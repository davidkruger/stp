<?php

App::uses('AppController', 'Controller');

class ComplainsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show_read(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('complains.show_read',$page); 
        $this->render('show');
    }
    
    public function show_unread(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('complains.show_unread',$page); 
        $this->render('show');
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('complains',$id);    
    }
}