<?php

App::uses('AppController', 'Controller');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class BranchesController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('branches',[
                'formdata' => $this->data,
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'branches',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Sucursal editada con éxito',
                    'error' => 'No se pudo editar la Sucursal'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('branches',$id,[
                'data' => [
                    'company' => $company,
                    'company_id' => $company
                ]
            ]);      
        }
        
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
        
        $this->KlezkaffoldReader->detail('branches',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['name']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
    
    public function edit_xhr(){
        $id = $this->route('company');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('branches', [
            'company_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('company');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['company_id'] = $id;
            $this->params->data['company'] = $id;
            
            $this->KlezkaffoldWeb->add('branches',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'branches',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Sucural creada con éxito',
                    'error' => 'No se pudo crear la Sucursal'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('branches',0,[
                'company' => $id,
                'company_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
    }
    
    public function add_xhr(){
        $id = $this->route('company');
        $this->KlezkaffoldWeb->requestAutocompleteForm('branches', [
            'company_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('branches',[
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'branches',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Sucursal eliminada con éxito',
                    'error' => 'No se pudo eliminar la Sucursal'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('branches',$id,[
                'company' => $company,
                'company_id' => $company
            ]);      
        }        
        
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
        
        $this->KlezkaffoldReader->detail('branches',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['name']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
}