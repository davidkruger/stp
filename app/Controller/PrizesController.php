<?php

App::uses('BreadCrumbAppController', 'Controller');

class PrizesController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('prizes');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('prizes.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('prizes.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('prizes',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('prizes',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'prizes',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Premio creado con éxito',
                    'error' => 'No se pudo crear el Premio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('prizes');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('prizes');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('prizes',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'prizes',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Premio editado con éxito',
                    'error' => 'No se pudo editar el Premio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('prizes',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('prizes');    
    }

    public function prize_photos(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('prizes.prize_photos',$page,[
            'prize' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['prize'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
    
    public function photo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('photo.prizes',$id);    
    }
    
    public function pdf(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->file('pdf.prizes',$id);    
    }

    public function provideModule() {
        return 'prizes';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}