<?php
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('AppController', 'Controller');

class SegmentsController extends AppController {    
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];

    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $subdivision = $this->route('subdivision');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('segments',[
                'formdata' => $this->data,
                'subdivision' => $subdivision,
                'subdivision_id' => $subdivision,
                'id' => $id,
                'redirect' => [
                    'controller' => 'subdivisions',
                    'action' => 'segments',
                    'id' => $subdivision,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Segmento editado con éxito',
                    'error' => 'No se pudo editar el Segmento'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('segments',$id,[
                'data' => [
                    'subdivision' => $subdivision,
                    'subdivision_id' => $subdivision
                ]
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('subdivisions',$this->route('subdivision'));   
        $detail = $this->KlezkaffoldReader->getData();
        $subdivision = $detail['payload']['data']['name'];  
        $division = $detail['payload']['data']['division_id'];  
        
        $this->KlezkaffoldReader->detail('divisions',$division);   
        $detail = $this->KlezkaffoldReader->getData();
        $div = $detail['payload']['data']['name'];  
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $division,'slug' => KlezkaffoldComponent::resolvParamSlug($div)], $div, 'after');
        $this->pushBreadcrumb([ 'controller' => 'subdivisions', 'action' => 'segments','id' => $this->route('subdivision'),'slug' => KlezkaffoldComponent::resolvParamSlug($div)], $subdivision, 'after');

    }
    
    public function edit_xhr(){
        $id = $this->route('subdivision');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('segments', [
            'subdivision_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('subdivision');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['subdivision_id'] = $id;
            $this->params->data['subdivision'] = $id;
            
            $this->KlezkaffoldWeb->add('segments',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'subdivisions',
                    'action' => 'segments',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Segmento creado con éxito',
                    'error' => 'No se pudo crear el Segmento'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('segments',0,[
                'subdivision' => $id,
                'subdivision_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('subdivisions',$this->route('subdivision'));   
        $detail = $this->KlezkaffoldReader->getData();
        $subdivision = $detail['payload']['data']['name'];  
        $division = $detail['payload']['data']['division_id'];  
        
        $this->KlezkaffoldReader->detail('divisions',$division);   
        $detail = $this->KlezkaffoldReader->getData();
        $div = $detail['payload']['data']['name'];  
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $division,'slug' => KlezkaffoldComponent::resolvParamSlug($div)], $div, 'after');
        $this->pushBreadcrumb([ 'controller' => 'subdivisions', 'action' => 'segments','id' => $this->route('subdivision'),'slug' => KlezkaffoldComponent::resolvParamSlug($div)], $subdivision, 'after');

    }
    
    public function add_xhr(){
        $id = $this->route('subdivision');
        $this->KlezkaffoldWeb->requestAutocompleteForm('segments', [
            'subdivision_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $subdivision = $this->route('subdivision');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('segments',[
                'subdivision' => $subdivision,
                'subdivision_id' => $subdivision,
                'id' => $id,
                'redirect' => [
                    'controller' => 'subdivisions',
                    'action' => 'segments',
                    'id' => $subdivision,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Segmento eliminado con éxito',
                    'error' => 'No se pudo eliminar el Segmento'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('segments',$id,[
                'subdivision' => $subdivision,
                'subdivision_id' => $subdivision
            ]);      
        }
    }
}