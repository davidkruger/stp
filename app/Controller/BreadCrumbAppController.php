<?php

App::uses('AppController', 'Controller');

abstract class BreadCrumbAppController extends AppController {
    abstract function provideModule();
    abstract function provideName($data);
    
    private $ignore = false;
    
    protected function setIgnoreBreadcrumb($ignore) {
        $this->ignore = $ignore;
    }
    
    public function beforeRender() {
        if($this->ignore === false){
            $ctrl = $this->params['controller'];
            $actn = $this->params['action'];

            if($actn !== 'dashboard'){
                $this->pushBreadcrumb([ 'controller' => $ctrl, 'action' => 'dashboard'], Configure::read("Sitemap.{$ctrl}.dashboard.h1"), 'before');
            }

            $detail = $this->resolvBreadcrumbDetail();
            $url = $this->resolvBreadcrumbDetailUrl($detail);

            if(is_null($url) === false){
                $this->pushBreadcrumb($url, $this->provideName($detail), 'after');
            }
        }

        return parent::beforeRender();
    }
    
    protected function resolvBreadcrumbDetail(){
        $actn = $this->params['action'];
        
        if($actn === 'detail'){
            $detail = $this->viewVars[API_DATA];
        }
        else{
            $id = $this->route('id',false);
            
            if(is_null($id) === false){
                $this->KlezkaffoldReader->detail($this->provideModule(),$id);   
                $detail = $this->KlezkaffoldReader->getData();
            }
            else{
                $detail = null;
            }
        }
        
        return $detail;        
    }

    protected function resolvBreadcrumbDetailUrl($detail){
        $url = null;
        
        if(isset($detail['payload']['links'])){
            foreach($detail['payload']['links'] as $link){
                if($link['action'] === 'detail'){
                    $url = $link;
                    break;
                }
            }
        }
        
        return $url;
    }
}
