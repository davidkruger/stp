<?php

App::uses('AppController', 'Controller');

class RedeemsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb', 'Backoffice'];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Backoffice'
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show_pending(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_show_pending', $this->request->query);
        }
        
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('redeems.show_pending',$page);   
        $this->render('show');     
    }
    
    public function show_approved(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_show_approved', $this->request->query);
        }
        
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('redeems.show_approved',$page);    
        $this->render('show');     
    }
    
    public function show_rejected(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_show_rejected', $this->request->query);
        }
        
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('redeems.show_rejected',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('redeems',$id);    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('redeems',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'redeems',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Moderacion procesada exitosamente',
                    'error' => 'No se pudo moderar el Canje'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('redeems',$this->route('id'));    
        }
    }
    
    public function massive(){
        if($this->request->is('POST')){
            $check = isset($this->data['check']) ? $this->data['check'] : [];
            
            if(empty($check)){
                $this->invalidMassive();
            }
            else if(isset($this->data['confirm'])){
                $this->Backoffice->redeemsMassiveProcess($this->data);
            }
            
            $this->set('check', $check);
            $this->KlezkaffoldWeb->requestForm('redeems');    
        }
        else{
            $this->invalidMassive();
        }
    }
    
    private function invalidMassive(){
        $referer = $this->request->referer();
        $this->flashToast('warning','No se han seleccionado Canjes');
        $this->redirect($referer ? $referer : [
            'controller' => 'redeems',
            'action' => 'show_pending'
        ]);
    }
}