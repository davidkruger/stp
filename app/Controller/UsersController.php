<?php

App::uses('BreadCrumbAppController', 'Controller');

class UsersController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb',
        'KlezBackend.KlezkaffoldReader'
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('users');
    }
        
//    public function show_romis(){
//        $page = $this->get('p',1);
//        $this->KlezkaffoldWeb->show('users.show_romis',$page);     
//        $this->render('show');      
//    }
    public function show_encargados(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_encargados',$page);      
        $this->render('show');     
    }
    
    public function show_samsung(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_samsung',$page);      
        $this->render('show');     
    }
    
    public function show_lamoderna(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_lamoderna',$page);      
        $this->render('show');     
    }
    
    public function show_responsibles(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_responsibles',$page);      
        $this->render('show');     
    }
    
    public function show_sellers(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_sellers',$page);      
        $this->render('show');     
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('users.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('users',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('users',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'users',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Usuario creado con éxito',
                    'error' => 'No se pudo crear el Usuario'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('users');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('users');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('users',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'users',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Usuario editado con éxito',
                    'error' => 'No se pudo editar el Usuario'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('users',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('users');    
    }
    
    public function picture(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('picture.users',$id);    
    }
    
    public function massive(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->massive('users',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'users',
                    'action' => 'show_sellers'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Revise la planilla generada y confirme la Carga de Vendedores',
                    'error' => 'No se pudo procesar la Carga de Vendedores',
                    'empty' => 'No se han encontrado registros CSV validos',
                ],
                'confirm' => [
                    'success' => 'Se han cargado %d Vendedores',
                    'error' => 'Ha ocurrido un error en la Carga de Vendedores',
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestMassive('users');    
        }
    }

    public function provideModule() {
        return 'users';
    }

    public function provideName($data) {
        return $data['payload']['data']['first_name'] . ' ' . $data['payload']['data']['last_name'] ;
    }
}