<?php

App::uses('BreadCrumbAppController', 'Controller');

class AchievementsController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('achievements.show',$page); 
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('achievements',$id);    
    }
        
    public function draw(){ 
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('achievements',$id);
        
        if($this->request->is('POST')){
            $this->Draw->write($id);    
        }
        
        $this->Draw->read($id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('achievements',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'achievements',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Logro cargado con éxito',
                    'error' => 'No se pudo crear el Logro'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('achievements');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('achievements');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('achievements',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'achievements',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Logro editado con éxito',
                    'error' => 'No se pudo editar el Logro'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('achievements',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('achievements');    
    }
    
    public function icon_0(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('icon_0.achievements',$id);    
    }
    
    public function icon_1(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('icon_1.achievements',$id);    
    }
    
    public function xlsx(){
        $this->KlezkaffoldWeb->downloadReport('xlsx.achievements',[
            'achievement_id' => $this->route('id')
        ]);
    }

    public function provideModule() {
        return 'achievements';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
    
    public function dashboard(){
        $this->redirect([
            'controller' => 'achievements',
            'action' => 'show'
        ]);
    }

    public function achievement_products(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('achievements.achievement_products',$page,[
            'achievement' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['achievement'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
}