<?php

App::uses('BreadCrumbAppController', 'Controller');

class CompaniesController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('companies');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('companies.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('companies.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('companies',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('companies',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Empresa creada con éxito',
                    'error' => 'No se pudo crear la Empresa'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('companies');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('companies');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('companies',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Empresa editada con éxito',
                    'error' => 'No se pudo editar la Empresa'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('companies',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('companies');    
    }

    public function company_contacts(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('companies.company_contacts',$page,[
            'company' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['company'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
    
    public function massive(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->massive('companies',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'show'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Revise la planilla generada y confirme la Carga de Empresas',
                    'error' => 'No se pudo procesar la Carga de Empresas',
                    'empty' => 'No se han encontrado registros CSV validos',
                ],
                'confirm' => [
                    'success' => 'Se han cargado %d Empresas',
                    'error' => 'Ha ocurrido un error en la Carga de Empresas',
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestMassive('companies');    
        }
    }
    
    public function prize_photo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('prize_photo.run_sales',$id);    
    }
    
    public function logo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('logo.companies',$id);    
    }
    
    public function branches(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('companies.branches',$page,[
            'company' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['company'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
    
    public function stock(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('companies.stock',$page,[
            'company' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['company'] = $id;
            }
        }  
        
        $this->render('show'); 
    }

    public function provideModule() {
        return 'companies';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }

}