<?php

App::uses('AppController', 'Controller');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class SubdivisionsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $division = $this->route('division');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('subdivisions',[
                'formdata' => $this->data,
                'division' => $division,
                'division_id' => $division,
                'id' => $id,
                'redirect' => [
                    'controller' => 'divisions',
                    'action' => 'subdivisions',
                    'id' => $division,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Subdivision editada con éxito',
                    'error' => 'No se pudo editar la Subdivision'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('subdivisions',$id,[
                'data' => [
                    'division' => $division,
                    'division_id' => $division
                ]
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('divisions',$this->route('division'));   
        $detail = $this->KlezkaffoldReader->getData();
        $division = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $this->route('division'),'slug' => KlezkaffoldComponent::resolvParamSlug($division)], $division, 'after');
    
    }
    
    public function edit_xhr(){
        $id = $this->route('division');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('subdivisions', [
            'division_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('division');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['division_id'] = $id;
            $this->params->data['division'] = $id;
            
            $this->KlezkaffoldWeb->add('subdivisions',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'divisions',
                    'action' => 'subdivisions',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Subdivision creada con éxito',
                    'error' => 'No se pudo crear la Subdivision'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('subdivisions',0,[
                'division' => $id,
                'division_id' => $id
            ]);    
        }
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('divisions',$this->route('division'));   
        $detail = $this->KlezkaffoldReader->getData();
        $division = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $this->route('division'),'slug' => KlezkaffoldComponent::resolvParamSlug($division)], $division, 'after');
    
    }
    
    public function add_xhr(){
        $id = $this->route('division');
        $this->KlezkaffoldWeb->requestAutocompleteForm('subdivisions', [
            'division_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $division = $this->route('division');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('subdivisions',[
                'division' => $division,
                'division_id' => $division,
                'id' => $id,
                'redirect' => [
                    'controller' => 'divisions',
                    'action' => 'subdivisions',
                    'id' => $division,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Subdivision eliminada con éxito',
                    'error' => 'No se pudo eliminar la Subdivision'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('subdivisions',$id,[
                'division' => $division,
                'division_id' => $division
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('divisions',$this->route('division'));   
        $detail = $this->KlezkaffoldReader->getData();
        $division = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $this->route('division'),'slug' => KlezkaffoldComponent::resolvParamSlug($division)], $division, 'after');
    }

    public function segments(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('subdivisions.segments',$page,[
            'subdivision' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['subdivision'] = $id;
            }
        }  
        
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'dashboard'], Configure::read("Sitemap.divisions.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('subdivisions',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $subdivision = $detail['payload']['data']['name'];  
        $division = $detail['payload']['data']['division_id'];  
        
        $this->KlezkaffoldReader->detail('divisions',$division);   
        $detail = $this->KlezkaffoldReader->getData();
        $div = $detail['payload']['data']['name'];  
        
        $this->pushBreadcrumb([ 'controller' => 'divisions', 'action' => 'detail','id' => $division,'slug' => KlezkaffoldComponent::resolvParamSlug($div)], $div, 'after');
        $this->pushBreadcrumb(null, $subdivision, 'after');
    
        $this->render('show'); 
    }
}