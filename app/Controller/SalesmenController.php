<?php

App::uses('AppController', 'Controller');
App::uses('AppModel', 'Model');
App::uses('AppModel', 'Model');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class SalesmenController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('salesmen');
    }
    
    public function by_branches(){      
        $this->pushBreadcrumb([ 'controller' => 'salesmen', 'action' => 'dashboard'], Configure::read("Sitemap.salesmen.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];
        $this->write('h1',$company);
        
        $this->KlezkaffoldWeb->dashboard('salesmen_by_branches', $this->route('company'));
        $this->render('dashboard');
    }
    
    public function show_by_branches(){
        $this->KlezkaffoldReader->detail('branches',$this->route('branch'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['name']; 
        $company_id = $detail['payload']['data']['company_id'];

        $this->pushBreadcrumb([ 'controller' => 'salesmen', 'action' => 'dashboard'], Configure::read("Sitemap.salesmen.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$company_id);   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name']; 
        
        $this->pushBreadcrumb([ 'controller' => 'salesmen', 'action' => 'by_branches','company' => $company_id, 'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'before');
        $this->write('h1',$branch);
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('salesmen_by_branches',$page, [
            'branch' => $this->route('branch')
        ]);      
        $this->render('show');     
    }
    
    public function detail(){
        $this->redirect([
            'controller' => 'users',
            'action' => 'detail',
            'id' => $this->route('id'),
            'slug' => $this->route('slug'),
        ]);
    }
}