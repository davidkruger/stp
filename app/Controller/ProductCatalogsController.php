<?php
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('AppController', 'Controller');

class ProductCatalogsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $product = $this->route('product');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('product_catalogs',[
                'formdata' => $this->data,
                'product' => $product,
                'product_id' => $product,
                'id' => $id,
                'redirect' => [
                    'controller' => 'products',
                    'action' => 'product_catalogs',
                    'id' => $product,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Catalogo de Producto editado con éxito',
                    'error' => 'No se pudo editar el Catalogo del Producto'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('product_catalogs',$id,[
                'data' => [
                    'product' => $product,
                    'product_id' => $product
                ]
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'dashboard'], Configure::read("Sitemap.products.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('products',$this->route('product'));   
        $detail = $this->KlezkaffoldReader->getData();
        $product = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'detail','id' => $this->route('product'),'slug' => KlezkaffoldComponent::resolvParamSlug($product)], $product, 'after');

    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('product_catalogs');    
    }
    
    public function add(){
        $id = $this->route('product');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['product_id'] = $id;
            $this->params->data['product'] = $id;
            
            $this->KlezkaffoldWeb->add('product_catalogs',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'products',
                    'action' => 'product_catalogs',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Catalogo de Producto creado con éxito',
                    'error' => 'No se pudo crear el Catalogo del Producto'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('product_catalogs',0,[
                'product' => $id,
                'product_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'dashboard'], Configure::read("Sitemap.products.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('products',$this->route('product'));   
        $detail = $this->KlezkaffoldReader->getData();
        $product = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'detail','id' => $this->route('product'),'slug' => KlezkaffoldComponent::resolvParamSlug($product)], $product, 'after');

    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('product_catalogs');    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $product = $this->route('product');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('product_catalogs',[
                'product' => $product,
                'product_id' => $product,
                'id' => $id,
                'redirect' => [
                    'controller' => 'products',
                    'action' => 'product_catalogs',
                    'id' => $product,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Catalogo del Producto eliminado con éxito',
                    'error' => 'No se pudo eliminar el Catalogo del Producto'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('product_catalogs',$id,[
                'product' => $product,
                'product_id' => $product
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'dashboard'], Configure::read("Sitemap.products.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('products',$this->route('product'));   
        $detail = $this->KlezkaffoldReader->getData();
        $product = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'products', 'action' => 'detail','id' => $this->route('product'),'slug' => KlezkaffoldComponent::resolvParamSlug($product)], $product, 'after');

    }
    
    public function image(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('image.product_catalogs',$id);    
    }
}