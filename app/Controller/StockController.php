<?php

App::uses('AppController', 'Controller');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class StockController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('stock',[
                'formdata' => $this->data,
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'stock',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Stock editado con éxito',
                    'error' => 'No se pudo editar el Stock'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('stock',$id,[
                'data' => [
                    'company' => $company,
                    'company_id' => $company
                ]
            ]);      
        }
        
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
        
    }
    
    public function edit_xhr(){
        $id = $this->route('company');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('stock', [
            'company_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('company');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['company_id'] = $id;
            $this->params->data['company'] = $id;
            
            $this->KlezkaffoldWeb->add('stock',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'stock',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Stock creado con éxito',
                    'error' => 'No se pudo crear el Stock'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('stock',0,[
                'company' => $id,
                'company_id' => $id
            ]);    
        }
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
    
    }
    
    public function add_xhr(){
        $id = $this->route('company');
        $this->KlezkaffoldWeb->requestAutocompleteForm('stock', [
            'company_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('stock',[
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'stock',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Stock eliminado con éxito',
                    'error' => 'No se pudo eliminar el Stock'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('stock',$id,[
                'company' => $company,
                'company_id' => $company
            ]);      
        }
        
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'dashboard'], Configure::read("Sitemap.companies.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('companies',$this->route('company'));   
        $detail = $this->KlezkaffoldReader->getData();
        $company = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'companies', 'action' => 'detail','id' => $this->route('company'),'slug' => KlezkaffoldComponent::resolvParamSlug($company)], $company, 'after');
        
    }

}