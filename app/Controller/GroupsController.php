<?php

App::uses('BreadCrumbAppController', 'Controller');

class GroupsController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('groups');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('groups.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('groups.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('groups',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('groups',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'groups',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Grupo creado con éxito',
                    'error' => 'No se pudo crear el Grupo'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('groups');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('groups');    
    }
    
    public function edit(){
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('groups',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'groups',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Grupo editado con éxito',
                    'error' => 'No se pudo editar el Grupo'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('groups',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('groups');    
    }

    public function provideModule() {
        return 'groups';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}