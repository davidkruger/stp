<?php

App::uses('BreadCrumbAppController', 'Controller');

class LotteriesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',
        'DrawLottery','PrintLottery'
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
        'DrawLottery'
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('lotteries.show',$page); 
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('lotteries',$id);    
    }
        
    public function draw(){ 
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('lotteries',$id);
        
        if($this->request->is('POST')){
            $this->DrawLottery->write($id);    
        }
        
        $this->DrawLottery->read($id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('lotteries',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'lotteries',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Loteria cargada con éxito',
                    'error' => 'No se pudo crear la Loteria'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('lotteries');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('lotteries');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('lotteries',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'lotteries',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Loteria editada con éxito',
                    'error' => 'No se pudo editar la Loteria'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('lotteries',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('lotteries');    
    }
    
    public function prize_photo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('prize_photo.lotteries',$id);    
    }
    
    public function xlsx(){
        $this->KlezkaffoldWeb->downloadReport('xlsx.lotteries',[
            'lottery_id' => $this->route('id')
        ]);
    }

    public function provideModule() {
        return 'lotteries';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
    
    public function dashboard(){
        $this->redirect([
            'controller' => 'lotteries',
            'action' => 'show'
        ]);
    }
    
    public function tickets(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('lotteries',$id); 
        $this->PrintLottery->get($id);
        $this->layout = 'printable';
    }
}