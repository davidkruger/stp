<?php

App::uses('BreadCrumbAppController', 'Controller');

class ProductsController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb', 'Backoffice','KlezBackend.KlezkaffoldReader'  ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('products');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('products.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('products.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('products',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('products',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'products',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto creado con éxito',
                    'error' => 'No se pudo crear el Producto'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('products');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('products');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('products',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'products',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto editado con éxito',
                    'error' => 'No se pudo editar el Producto'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('products',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('products');    
    }

    public function product_promotions(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('products.product_promotions',$page,[
            'product' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['product'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
    
    public function picture(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('picture.products',$id);    
    }

    public function product_specs(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('products.product_specs',$page,[
            'product' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['product'] = $id;
            }
        }  
        
        $this->render('show'); 
    }

    public function product_catalogs(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('products.product_catalogs',$page,[
            'product' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['product'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
    
    public function search(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('products_search',[
                'formdata' => $this->data,
                'dryrun' => true,
            ]);    
        
            if($this->viewVars[API_DATA]['payload']['success']){
                $this->Backoffice->productSearch($this->data);
                if(empty($this->Backoffice->getData()['products'])){
                    $this->flashToast('warning','No se han encontrado productos');
                }
            }
            else{
                $this->flashToast('error','Revise errores en los filtros');
            }
        }
        
        if(!isset($this->viewVars[API_DATA]['payload'])){
            $this->KlezkaffoldWeb->requestForm('products_search');    
        }
    }

    public function product_points(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('products.product_points',$page,[
            'product' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['product'] = $id;
            }
        }  
        
        $this->render('show'); 
    }

    public function provideModule() {
        return 'products';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}