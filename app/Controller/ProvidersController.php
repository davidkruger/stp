<?php

App::uses('BreadCrumbAppController', 'Controller');

class ProvidersController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('providers');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('providers.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('providers.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('providers',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('providers',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'providers',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Proveedor creado con éxito',
                    'error' => 'No se pudo crear Proveedor'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('providers');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('providers');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('providers',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'providers',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Proveedor editado con éxito',
                    'error' => 'No se pudo editar Proveedor'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('providers',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('providers');    
    }

    public function provideModule() {
        return 'providers';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}