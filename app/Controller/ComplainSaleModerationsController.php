<?php

App::uses('AppController', 'Controller');

class ComplainSaleModerationsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Detail',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('complain_sale_moderations',$id);    
    }
}