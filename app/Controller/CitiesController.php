<?php

App::uses('BreadCrumbAppController', 'Controller');

class CitiesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('cities');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('cities.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('cities.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('cities',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('cities',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'cities',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Ciudad creada con éxito',
                    'error' => 'No se pudo crear Ciudad'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('cities');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('cities');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('cities',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'cities',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Ciudad editada con éxito',
                    'error' => 'No se pudo editar Ciudad'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('cities',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('cities');    
    }

    public function provideModule() {
        return 'cities';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}