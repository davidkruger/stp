<?php

App::uses('BreadCrumbAppController', 'Controller');

class SalesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb',
        'Stock',
        'Approval', 'Backoffice','KlezBackend.KlezkaffoldReader'
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
        'Klezkaffold.Massive',
        'Backoffice'
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('sales');
    }
    
    public function approve_massive(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->samsungMassive($id);
        }
        
        $this->KlezkaffoldWeb->show('sales.detail_pending_massive',$page, [
            'massive_sale' => $id
        ]); 
    }
    
    public function reject_massive(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->samsungMassiveReject($id);
        }
        
        $this->KlezkaffoldWeb->show('sales.detail_pending_massive',$page, [
            'massive_sale' => $id
        ]); 
    }
    
    public function detail_massive(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        
        $this->KlezkaffoldWeb->show('sales.detail_massive',$page, [
            'massive_sale' => $id
        ]); 
        $this->render('show');
    }
    
    public function show(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_sales_show', $this->request->query);
        }
        
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('sales.show',$page); 
        $this->render('show');
    }
    
    public function show_massive(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('sales.show_massive',$page); 
        $this->render('show');
    }
        
    public function show_massive_pending(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('sales.show_massive_pending',$page); 
        $this->render('show');
    }
        
    public function show_manual(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_sales_show_manual', $this->request->query);
        }
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('sales.show_manual',$page); 
        $this->render('show');
    }
        
    public function show_manual_pending(){
        if(isset($this->request->query['excel'])){
            $this->KlezkaffoldWeb->downloadReport('xlsx_sales_show_manual_pending', $this->request->query);
        }
        
        $this->Backoffice->customFiltersFeed();
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('sales.show_manual_pending',$page); 
        $this->render('show');
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('sales',$id);    
    }

    public function delete() {
    	$id = $this->route('id');
        
        
    }
        
    public function approve_manual(){
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->samsung($id);
        }
        
        $this->KlezkaffoldWeb->detail('sales',$id);   
        $this->Stock->check($this->viewVars[API_DATA]['payload']['data']);
        $stock = $this->Stock->getData();
        
        if($stock['warning']){
            $this->flashToast('warning','Empresa no posee el Stock necesario para esta venta');
            $this->set('warning', $stock);
        }
    }
        
    public function reject_manual(){
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->samsungReject($id, $this->data);
        }
        
        $this->KlezkaffoldWeb->detail('sales',$id);   
    }
    
    public function massive(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->massive('sales',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'sales',
                    'action' => 'show_massive'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Revise la planilla generada y confirme la Carga de Ventas',
                    'error' => 'No se pudo procesar la Carga de Ventas',
                    'empty' => 'No se han encontrado registros CSV validos',
                ],
                'confirm' => [
                    'success' => 'Se han cargado %d Ventas',
                    'error' => 'Ha ocurrido un error en la Carga de Ventas',
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestMassive('sales');    
        }
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('sales',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'sales',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Venta cargada con éxito',
                    'error' => 'No se pudo crear la Venta'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('sales');    
        }
    }
    
    public function add_xhr(){
        if(isset($this->data['stock'])){
            $this->Stock->check($this->data);
            $stock = $this->Stock->getData();
            $this->Stock->jsonDataDump($stock);
        }
        else{
            $this->KlezkaffoldWeb->requestAutocompleteForm('sales');    
        }
    }
    
    public function photo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('photo.sales',$id);    
    }

    public function provideModule() {
        return 'sales';
    }

    public function provideName($data) {
        return $data['payload']['data']['id'];
    }
}