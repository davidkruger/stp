<?php
App::uses('BreadCrumbAppController', 'Controller');

class SpecsController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('specs');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('specs.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('specs.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('specs',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('specs',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'specs',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Caracteristica creada con éxito',
                    'error' => 'No se pudo crear la Caracteristica'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('specs');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('specs');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('specs',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'specs',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Caracteristica editada con éxito',
                    'error' => 'No se pudo editar la Caracteristica'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('specs',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('specs');    
    }

    public function provideModule() {
        return 'specs';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}