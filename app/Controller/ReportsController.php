<?php

App::uses('AppController', 'Controller');

class ReportsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb', 'Reports' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Reports'
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function summary(){
        $this->KlezkaffoldWeb->dashboard('reports');
    }
    
    public function total(){
        $this->KlezkaffoldWeb->requestForm('reports_summary');
        
        if(empty($this->data)){
            $this->params->data['a'] = '2019-01-01';
            $this->params->data['b'] = date('Y-m-d');
        }
        else{
            $a = explode('/', $this->params->data['a']);
            $b = explode('/', $this->params->data['b']);
            $this->params->data['a'] = $a[2] . '-' . $a[1] . '-' . $a[0];
            $this->params->data['b'] = $b[2] . '-' . $b[1] . '-' . $b[0];
        }
        
        $this->viewVars[API_DATA]['payload']['data']['a'] = $this->data['a'];
        $this->viewVars[API_DATA]['payload']['data']['b'] = $this->data['b'];
        $this->Reports->summary($this->data);
    }
    
    public function companies(){
        $this->KlezkaffoldWeb->requestForm('reports_companies');
        
        if(empty($this->data)){
            $this->params->data['a'] = '2019-01-01';
            $this->params->data['b'] = date('Y-m-d');
            $this->viewVars[API_DATA]['payload']['data'] = $this->params->data;
        }
        else{
            $a = explode('/', $this->params->data['a']);
            $b = explode('/', $this->params->data['b']);
            $this->params->data['a'] = $a[2] . '-' . $a[1] . '-' . $a[0];
            $this->params->data['b'] = $b[2] . '-' . $b[1] . '-' . $b[0];
            $this->Reports->companies($this->data);
            
            if(isset($this->viewVars[REPORTS_DATA]['messages'])){
                $this->viewVars[API_DATA]['payload']['messages'] = $this->viewVars[REPORTS_DATA]['messages'];
            }
            
            $this->viewVars[API_DATA]['payload']['data'] = $this->viewVars[REPORTS_DATA]['data'];
        }
    }
    
    public function top10(){
        $this->KlezkaffoldWeb->dashboard('top10');
    }
    
    public function top10division(){
        $this->KlezkaffoldWeb->dashboard('top10');
        $this->params->data['mod'] = 'division';
        $this->Reports->top10($this->data);
        $this->render('top10general');
    }
    
    public function top10divisionlastmonth(){
        $this->KlezkaffoldWeb->dashboard('top10');
        $this->params->data['mod'] = 'divisionlastmonth';
        $this->Reports->top10($this->data);
        $this->render('top10general');
    }
    
    public function top10lastmonth(){
        $this->KlezkaffoldWeb->dashboard('top10');
        $this->params->data['mod'] = 'lastmonth';
        $this->Reports->top10($this->data);
        $this->render('top10general');
    }
    
    public function top10general(){
        $this->KlezkaffoldWeb->dashboard('top10');
        $this->Reports->top10($this->data);
    }
    
    public function products(){
        $this->KlezkaffoldWeb->requestForm('reports_products');
        
        if(empty($this->data)){
            $this->params->data['a'] = '2019-01-01';
            $this->params->data['b'] = date('Y-m-d');
            $this->viewVars[API_DATA]['payload']['data'] = $this->params->data;
        }
        else{
            $a = explode('/', $this->params->data['a']);
            $b = explode('/', $this->params->data['b']);
            $this->params->data['a'] = $a[2] . '-' . $a[1] . '-' . $a[0];
            $this->params->data['b'] = $b[2] . '-' . $b[1] . '-' . $b[0];
            $this->Reports->products($this->data);
            
            if(isset($this->viewVars[REPORTS_DATA]['messages'])){
                $this->viewVars[API_DATA]['payload']['messages'] = $this->viewVars[REPORTS_DATA]['messages'];
            }
            
            $this->viewVars[API_DATA]['payload']['data'] = $this->viewVars[REPORTS_DATA]['data'];
            
            if(empty($this->viewVars[REPORTS_DATA]['report'])){
                $this->flashToast('warning','No hay informacion');
            }
        }
    }
    
    public function sales_xlsx(){
        if(!empty($this->data)){
            $this->KlezkaffoldWeb->downloadReport('sales_xlsx', $this->data);
        }
        else {
            $this->KlezkaffoldWeb->reportFilter('sales_xlsx');
        }
    }
    
    public function products_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('sales');
    }
    
    public function companies_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('sales');
    }
    
    public function sales_xlsx_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('sales');
    }
    
    public function achievements_xlsx(){
        if(!empty($this->data)){
            $this->KlezkaffoldWeb->downloadReport('achievements_xlsx', $this->data);
        }
        else {
            $this->KlezkaffoldWeb->reportFilter('achievements_xlsx');
        }
    }
    
    public function achievements_xlsx_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('achievements_xlsx');
    }
    
    public function challenges_xlsx(){
        if(!empty($this->data)){
            $this->KlezkaffoldWeb->downloadReport('challenges_xlsx', $this->data);
        }
        else {
            $this->KlezkaffoldWeb->reportFilter('challenges_xlsx');
        }
    }
    
    public function challenges_xlsx_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('challenges_xlsx');
    }

    public function companies_xlsx(){
        if(!empty($this->data)){
            $this->KlezkaffoldWeb->downloadReport('companies_xlsx', $this->data);
        }
        else {
            $this->KlezkaffoldWeb->reportFilter('companies_xlsx');
        }
    }
    
    public function users_xlsx(){
        if(!empty($this->data)){
            $this->KlezkaffoldWeb->downloadReport('users_xlsx', $this->data);
        }
        else {
            $this->KlezkaffoldWeb->reportFilter('users_xlsx');
        }
    }
    
    public function users_xlsx_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('users_xlsx');
    }
    
}