<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

if(!defined('BACKOFFICE_DATA')){
    define('BACKOFFICE_DATA',  'backoffice_data' . substr(md5(uniqid()),0,8));
}

class BackofficeComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function redeemsMassiveProcess($data){
        $url = $this->getApi('backoffice_redeems_massive_process');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function customFiltersFeed(){
        $url = $this->getApi('backoffice_custom_filters_feed');
        $this->setUrl($url);
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function productSearch($data){
        $url = $this->getApi('backoffice_product_search');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    protected function feed($format = false){
        $this->flowControl($format);
        $this->data = $this->getHttpOkResponse($format);
        $this->Controller->set(BACKOFFICE_DATA,$this->getData());
    }    
    
    public function getData() {
        return $this->data;
    }
}