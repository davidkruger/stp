<?php

App::uses('KlezkaffoldWebComponent', 'KlezBackend.Controller/Component');

if(!defined('REPORTS_DATA')){
    define('REPORTS_DATA',  'reports_data' . substr(md5(uniqid()),0,8));
}
class ReportsComponent extends KlezkaffoldWebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function summary($data){
        $url = $this->getApi('reports_summary');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        
        if(isset($data['excel'])){
            $this->reportDump('json');
        }
        else{
            $this->feed('json');
        }
    }
    
    public function companies($data){
        $url = $this->getApi('reports_companies');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        
        if(isset($data['excel'])){
            $this->reportDump('json');
        }
        else{
            $this->feed('json');
        }
    }
    
    public function top10($data){
        $url = $this->getApi('reports_top10');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        
        if(isset($data['excel'])){
            $this->reportDump('json');
        }
        else{
            $this->feed('json');
        }
    }
    
    public function products($data){
        $url = $this->getApi('reports_products');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        
        if(isset($data['excel'])){
            $this->reportDump('json');
        }
        else{
            $this->feed('json');
        }
    }
    
    protected function feed($format = false){
        $this->data = $this->getHttpOkResponse($format);
        $this->Controller->set(REPORTS_DATA,$this->getData());
    }    
    
    public function getData() {
        return $this->data;
    }
}