<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

if(!defined('DRAW_DATA')){
    define('DRAW_DATA',  'draw_data' . substr(md5(uniqid()),0,8));
}
class DrawComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function write($id){
        $url = $this->getApi('draw');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'run_sale' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function read($id){
        $url = $this->getApi('draw');
        $this->setUrl($url);
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'run_sale' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    protected function feed($format = false){
        $this->data = $this->getHttpOkResponse($format);
        $this->Controller->set(DRAW_DATA,$this->getData());
    }    
    
    public function getData() {
        return $this->data;
    }
}