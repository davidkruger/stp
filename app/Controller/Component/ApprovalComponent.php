<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

if(!defined('APPROVAL_DATA')){
    define('APPROVAL_DATA',  'approval_data' . substr(md5(uniqid()),0,8));
}

class ApprovalComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function samsungMassive($id){
        $url = $this->getApi('samsung_massive_approval');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function samsungMassiveReject($id){
        $url = $this->getApi('samsung_massive_reject');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function samsung($id){
        $url = $this->getApi('samsung_approval');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function samsungReject($id,$data){
        $data['id'] = $id;
        
        $url = $this->getApi('samsung_reject');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function responsable($id){
        $url = $this->getApi('responsable_approval');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function responsableReject($id,$data){
        $data['id'] = $id;
        $url = $this->getApi('responsable_reject');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    protected function feed($format = false){
        $this->flowControl($format);
        $this->data = $this->getHttpOkResponse($format);
        $this->Controller->set(APPROVAL_DATA,$this->getData());
    }    
    
    public function getData() {
        return $this->data;
    }
}