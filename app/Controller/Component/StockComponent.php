<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

if(!defined('STOCK_DATA')){
    define('STOCK_DATA',  'stock_data_' . substr(md5(uniqid()),0,8));
}

class StockComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function check($data){
        $url = $this->getApi('stock_check');
        $this->setUrl($url);
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData($data);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    protected function feed($format = false){
        $this->data = $this->getHttpOkResponse($format);
        $this->Controller->set(STOCK_DATA,$this->getData());
    }    
    
    public function getData() {
        return $this->data;
    }
}