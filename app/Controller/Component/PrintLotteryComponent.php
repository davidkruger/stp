<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

class PrintLotteryComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function get($id){
        $this->setUrl($this->getApi('print_lottery'));
        $this->setVerb('GET');
        $this->cookieStorage();
                
        $this->setData([
            'lottery' => $id
        ]);
        
        $this->jsonPayload();
        $this->shared('json');
    }
}