<?php

App::uses('KlezkaffoldWebComponent', 'KlezBackend.Controller/Component');

class FrontendComponent extends KlezkaffoldWebComponent{
    
    /**
     * 
     * Esta es la magia del subclassing...
     * 
     */
    
    public function getApi($key){
        return parent::getApi('frontend_' . $key);
    }
    
    public function assignPoints($data){
        $this->setUrl($this->getApi('assign_points'));
        $this->setVerb('POST');
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function company(){
        $this->setUrl($this->getApi('company'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');        
    }
    
    public function deleteSeller($data){
        $this->setUrl($this->getApi('delete_seller'));
        $this->setVerb('POST');
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function seller($id){
        $this->setUrl($this->getApi('seller'));
        $this->setVerb('GET');
        $this->setData([
            'id' => $id
        ]);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function pendings(){
        $this->setUrl($this->getApi('pendings'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function sellers(){
        $this->setUrl($this->getApi('sellers'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function products(){
        $this->setUrl($this->getApi('products'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function salesmen(){
        $this->setUrl($this->getApi('salesmen'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function redeems(){
        $this->setUrl($this->getApi('redeems'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function ranking(){
        $this->setUrl($this->getApi('ranking'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function acceptChallenge($id){
        $this->setUrl($this->getApi('challenges'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id,
        ]);
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function redeem($id,$count=0){
        $this->setUrl($this->getApi('redeem'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'id' => $id,
            'count' => $count
        ]);
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function prizes(){
        $this->setUrl($this->getApi('prizes'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function runSales(){
        $this->setUrl($this->getApi('run_sales'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function challenges(){
        $this->setUrl($this->getApi('challenges'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function achievements(){
        $this->setUrl($this->getApi('achievements'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function branches(){
        $this->setUrl($this->getApi('branches'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function catalogs($data){
        $this->setUrl($this->getApi('catalogs'));
        $this->setVerb('GET');
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->shared('json');
    }
    
    public function lotteries($data = [], $verb = 'GET'){
        $this->setUrl($this->getApi('lotteries'));
        $this->setVerb($verb);
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        
        if($verb === 'POST'){
            $this->dump('json');
        } else {
            $this->shared('json');
        }
    }
    
    public function roulettes($data = [], $verb = 'GET'){
        $this->setUrl($this->getApi('roulettes'));
        $this->setVerb($verb);
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        
        if($verb === 'POST'){
            $this->dump('json');
        } else {
            $this->shared('json');
        }
    }
    
    public function injection(&$apiData){
        Configure::load('frontend');
        
        if(empty($apiData['auth']['id']) === false){
            $this->injectUserMenu($apiData);
        }
        else{
            $this->injectAnonMenu($apiData);
        }
        
        $this->injectCurrentClass($apiData);
    }
    
    public function injectCurrentClass(&$apiData){
        if(is_array($apiData['menu']) === false){
            return;
        }
        
        $current = Router::url();
        
        foreach($apiData['menu'] as &$menu){
            if(empty($menu['class']) === false){
                continue;
            }
            
            if(isset($menu['dropdown'])){
                foreach($menu['dropdown'] as $m){
                    if($current === $m['url']){
                        $menu['class'] = ' m_index';
                    }
                }
            }
            else if($current === $menu['url']){
                $menu['class'] = ' m_index';
            }
        }
    }
    
    public function injectAnonMenu(&$apiData){
        $apiData['menu'] = Configure::read('Frontend.anon_menu');
    }
    
    public function injectUserMenu(&$apiData){
        switch ($apiData['auth']['data']['role']){
            case 'responsable':
            case 'encargado':
                $apiData['menu'] = Configure::read('Frontend.responsable_menu');
                break;
            case 'vendedor':
                $apiData['menu'] = Configure::read('Frontend.vendedor_menu');
                break;
            default:
                $apiData['menu'] = [];
        }
    }
}