<?php

App::uses('AppController', 'Controller');

class ComplainSalesController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show_pending(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('complain_sales.show_pending',$page);   
        $this->render('show');     
    }
    
    public function show_approved(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('complain_sales.show_approved',$page);    
        $this->render('show');     
    }
    
    public function show_rejected(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('complain_sales.show_rejected',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('complain_sales',$id);    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('complain_sales',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'complain_sales',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Moderacion procesada exitosamente',
                    'error' => 'No se pudo moderar el Reclamo de Venta'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('complain_sales',$this->route('id'));    
        }
    }
}