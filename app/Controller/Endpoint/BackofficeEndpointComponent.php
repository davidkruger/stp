<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('Redeem','Model');
App::uses('Company','Model');
App::uses('Product','Model');
App::uses('RedeemModeration','Model');
App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');

class BackofficeEndpointComponent extends BackendComponent{
    private $Auth;
    private $Scaffold;
    private $Controller;
    private $Company;
    private $Product;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        $this->Auth = $controller->Auth;
        $this->Controller = $controller;
    }
    
    public function customFiltersFeed(){
        $this->Company = new Company();
        $this->write('companies', $this->Company->feedCustomFilters());
        $this->setSimple(true);
    }
    
    public function redeemsMassiveProcess($payload){
        $this->Redeem = new Redeem();
        $this->RedeemModeration = new RedeemModeration();
        
        Configure::load('scaffold_redeems','default',false);
        $check = isset($payload['check']) ? $payload['check'] : [];
        $this->Redeem->begin();
        
        try{
            foreach($check as $id){
                $config = Configure::read('Klezkaffold.edit.redeems');
                $output = $this->edit($id, $config, $payload);
                
                if(!$output['success']){
                    throw new Exception('CANNOT PROCESS REDEEM='.$id);
                }
            }
            
            $this->setFlash('Se ha'.(count($check)>1?'n':'').' procesado ' . count($check) . ' canje' . (count($check)>1?'s':''), 'success');
            $this->Redeem->commit();
        }
        catch (Exception $e){
            $this->Redeem->rollback();
            error_log("Exception @ BackofficeEndpointComponent::redeemsMassiveProcess() - {$e->getMessage()}");
            $this->setFlash('Ha ocurrido un error inesperado', 'error');
        }
        
        $this->setRedirect([
            'controller' => 'redeems',
            'action' => 'show_pending'
        ]);
    }
    
    
    final public function edit($id,$config,$payload){
        App::uses('EditKlezkaffoldComponent', 'Klezkaffold.Controller/Klezkaffold');
        App::uses('ComponentCollection', 'Controller');
        
        $this->Scaffold = new EditKlezkaffoldComponent(new ComponentCollection());
        $this->Scaffold->setTransactional(false);
        $this->Scaffold->initialize($this->Controller);
        $this->Scaffold->input($config, [
            'data'=> [
                'formdata' => $payload,
                'redirect' => null,
                'id' => $id
                
            ]
        ]);
        
        $this->Scaffold->process();
        $output = $this->Scaffold->output();
        
        unset($output['schema']);
        unset($output['data']);
        unset($output['id']);
        
        if(array_key_exists('messages',$output)){
            $messages = $output['messages'];
            
            if(empty($messages) === false){
                $output['validation'] = $messages;
            }
            
            unset($output['messages']);
        }
        
        $this->overwrite($output);
        return $output;
    }
    
    public function productSearch($payload){
        $this->Product = new Product();
        $this->overwrite($this->Product->complexSearch($payload));
        $this->setSimple(true);
    }
}