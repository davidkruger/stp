<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('AuthComponent', 'KlezApi.Controller/Component');
App::uses('Prize', 'Model');
App::uses('User', 'Model');
App::uses('Redeem', 'Model');
App::uses('Product', 'Model');
App::uses('Seller', 'Model');
App::uses('Company', 'Model');
App::uses('Sale', 'Model');
App::uses('Assignment', 'Model');
App::uses('RunSale', 'Model');
App::uses('Semaphore', 'Model');
App::uses('FullUser', 'Model');
App::uses('Branch', 'Model');
App::uses('Roulette', 'Model');
App::uses('Lottery', 'Model');
App::uses('RouletteIntent', 'Model');
App::uses('LotteryIntent', 'Model');
App::uses('Achievement', 'Model');
App::uses('Challenge', 'Model');
App::uses('ChallengeUser', 'Model');


class FrontendEndpointComponent extends EndpointComponent{
    private $data = [];
    private $Auth;
    private $Redeem;
    private $Prize;
    private $User;
    private $Product;
    private $Seller;
    private $Company;
    private $Assignment;
    private $RunSale;
    private $FullUser;
    private $Branch;
    private $Sale;
    private $Roulette;
    private $Lottery;
    private $RouletteIntent;
    private $LotteryIntent;
    private $Challenge;
    
    public function initialize(\Controller $controller) {
        $this->Auth = $controller->Auth;
        
        parent::initialize($controller);
    }
    
    public function company(){
        $this->Company = new Company();
        $this->data['company'] = $this->Company->fetchFrontend($this->Auth->getData());
    }
    
    public function sellers(){
        $this->Seller = new Seller();
        $this->data['sellers'] = $this->Seller->fetchFrontend($this->Auth->getData());
    }
    
    public function seller($payload){
        $this->Seller = new Seller();
        $this->data['seller'] = $this->Seller->loadFrontend($payload['id'],$this->Auth->getData());
    }
    
    public function products(){
        $this->Product = new Product();
        $this->data['products'] = $this->Product->fetchFrontend();
    }
    
    public function branches(){
        $this->Branch = new Branch();
        $this->data['branches'] = $this->Branch->fetchFrontend($this->Auth->getData()['data']['company_id']);
    }
    
    public function pendings(){
        $this->Sale = new Sale();
        $this->data['pendings'] = $this->Sale->fetchFrontendPendings($this->Auth->getData()['data']['id']);
    }
    
    public function salesmen(){
        $this->FullUser = new FullUser();
        $this->data['salesmen'] = $this->FullUser->fetchSalesmenFrontend($this->Auth->getData()['data']);
    }
    
    public function catalogs($payload){
        $this->Product = new Product();
        $this->data['catalogs'] = $this->Product->fetchCatalogs($payload,$this->Auth->getData()['data']['company_id']);
    }
    
    public function prizes(){
        $this->Prize = new Prize();
        $this->data['prizes'] = $this->Prize->fetchFrontend($this->Auth->getData()['data']);
        $this->updateAuthData();
        $this->data['auth'] = $this->Auth->getData();
    }
    
    public function runSales(){
        $this->RunSale = new RunSale();
        $this->data['run_sales'] = $this->RunSale->fetchFrontend($this->Auth->getData());
    }
    
    public function achievements(){
        $this->Achievement = new Achievement();
        $this->data['achievements'] = $this->Achievement->fetchFrontend($this->Auth->getData());
    }
    
    public function assignPoints($payload){
        throw new Exception('Deprecated');
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
        $response = [
            'messages' => [
                
            ],
            'data' => $payload
        ];
        
        if($semaphore){
            $this->Assignment = new Assignment();
            $this->Company = new Company();
            $this->Seller = new Seller();
            $this->Company->begin();
            
            sem_acquire($semaphore);
            $company = $this->Auth->getData()['data']['company_id'];
            $responsible = $this->Auth->getData()['id'];
            $this->Company->loadById($company);
            $current = $this->Company->readField('points');
            $success = true;
            $total = 0;
            
            foreach($payload as $field => $value){
                $total += (int) $value;
            }
            
            if($total > $current){
                $this->data['message'] = 'La sumatoria de Puntos que desea Asignar supera los ' . number_format($current,0,',','.') . ' puntos';
                $success = false;
            }
            else{
                foreach($payload as $field => $value){
                    $seller = (int) str_replace('seller_', '',$field);
                    $points = (int) $value;

                    $data = [
                        'company_id' => $company,
                        'responsible_id' => $responsible,
                        'seller_id' => $seller,
                        'points' => $value
                    ];

                    $this->Assignment->prepareForStore($data, null);
                    $this->Assignment->set($this->Assignment->getWritableData());

                    if($this->Assignment->validates()){
                        if($points === 0 || $success === false){
                            continue;
                        }

                        if($this->Assignment->saveData()){
                            if($this->Seller->assignPoints($company,$seller,$points)){
                                $this->data['message'] = 'Asignacion de Puntos realizada satisfactoriamente';
                            }
                            else{
                                $this->data['message'] = 'Ha ocurrido un error no previsto al guardar la Asignacion de Puntos';
                                $success = false;
                                break;
                            }
                        }
                        else{
                            $this->data['message'] = 'Ha ocurrido un error no previsto al guardar la Asignacion de Puntos';
                            $success = false;
                            break;
                        }
                    }
                    else{
                        $this->data['message'] = 'No se pudo guardar la Asignacion de Puntos, revise errores en el formulario';
                        $response['messages'][$field] = [];

                        foreach($this->Assignment->validationErrors as $f => $m){
                           $response['messages'][$field] += $m;
                        }

                        $success = false;
                    }
                }
            }
            
            if($total <= 0){
                $this->data['message'] = 'Debe asignar al menos un Punto';
                $success = false;
            }
        
            if($success){
                $this->Assignment->commit();
            }
            else{
                $this->Assignment->rollback();
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
            $this->data['message'] = 'Servidor ocupado';
        }
        
        $this->data['success'] = $success;
        $this->data['payload'] = $response;
        sem_release($semaphore);
    }
    
    public function redeem($payload){
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
        
        if($semaphore && sem_acquire($semaphore)){
            $this->Prize = new Prize();
            $auth = $this->Auth->getData();

            if($this->Prize->loadForRole($payload['id'],$auth['data']['role'])){
                $this->updateAuthData();
                $points = (int) $auth['data']['points'];
                $required = (int) $this->Prize->readField('points');


                $prizes_filter_max = $this->Prize->find('all_pipe',[
                    'conditions' => ['Prize.id'=>$payload['id']],
                    'fields' => 'Prize.max_prizes_month,Prize.control_prize,Redee.total_use',
                    'joins' => [
                        "LEFT JOIN (
                            select 
                            prize_id,
                            COUNT(*) as total_use
                            FROM redeems
                            WHERE `status` in ('pendiente','aprobado') 
                            AND MONTH(created) = MONTH(CURRENT_DATE) 	
                            AND YEAR(created) = YEAR(CURRENT_DATE)
                            GROUP BY prize_id,MONTH(created),YEAR(created)
        
                        ) as Redee ON Redee.prize_id = Prize.id"
                    ]
                ]);

                 $max_canje = 0;
                 $total_use = 0;
                 $control_prize = 0;

                foreach ($prizes_filter_max as $key => $value) {
                    $max_canje = (int) $value['Prize']['max_prizes_month'];
                    $total_use = (int)$value['Redee']['total_use'];
                    $control_prize = (int)$value['Prize']['control_prize'];
                }
    
     
                 if($total_use >= $max_canje && $control_prize){
                      $this->data['message'] = 'El premio ya no puede ser canjeado, se alcanzo el limite mensual';

                } else if($points >= $required){


                    $prize = $this->Prize->getData();

                    $this->Redeem = new Redeem();

                    $invalidRedeem=false;
                    if(isset($payload['count']) && $payload['count']>0){
                        
                        $prize['amount']=$payload['count']*$prize['amount'];
                        $prize['total'] = $payload['count'];
                        
                        if($payload['count']>$points){
                            $this->data['message'] = 'No posee los puntos necesarios';
                            $invalidRedeem=true;
                        }
                    }

                    if(!$invalidRedeem && $this->Redeem->pushRedeem($prize,$auth)){
                        $this->updateAuthData();
                        $this->data['message'] = 'Tu premio ha sido solicitado';
                        $success = true;
                    }
                    else{
                        $this->data['message'] = 'Ha ocurrido un error imprevisto';
                    }
                    
                
                }
                else{
                    $this->data['message'] = 'No posee los puntos necesarios';
                }
                
                $this->data['auth'] = $this->Auth->getData();
            }
            else{
                $this->data['message'] = 'Premio no existente';
            }
            
            sem_release($semaphore);
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
            $this->data['message'] = 'Servidor ocupado';
        }
        
        $this->data['success'] = $success;
    }
    
    public function deleteSeller($payload){
        $success = false;
        $id = $payload['id'];
        $authdata = $this->Auth->getData();
        $this->Seller = new Seller();

        try{
            if($this->Seller->findFrontend($id,$authdata)){
                if($this->Seller->disableFrontend($id)){
                    $this->data['message'] = 'Vendedor eliminado satisfactoriamente';
                    $success = true;
                }
                else{
                    $this->data['message'] = 'Ha ocurrido un problema inesperado al eliminar Vendedor';
                }
            }
            else{
                $this->data['message'] = 'Vendedor no existente';
            }
        }
        catch (Exception $e){
            error_log("Exception @ FrontendEndpointComponent::deleteSeller($id) - {$e->getMessage()}");
            $this->data['message'] = 'Error inesperado en el Servidor';
        }
        
        $this->data['success'] = $success;
    }
    
    public function redeems(){
        $id = $this->Auth->getId();
        $this->Redeem = new Redeem();
        $data = $this->Redeem->fetchFrontend($id);
        
        $this->data = [
            'redeems' => []
        ];
        
        foreach($data as $redeem){
            $red    = $redeem['Redeem'];
            $pri    = $redeem['Prize'];
            $status = $red['status'];
            
            if(!isset($this->data['redeems'][$status])){
                $this->data['redeems'][$status] = [];
            }
            
            $red['prize'] = $pri;
            $this->data['redeems'][$status][] = $red;
        }
    }
    
    public function ranking(){
        $company_id = $this->Auth->getData()['data']['company_id'];
        $id = $this->Auth->getId();
        $this->Sale = new Sale();
        $month = date('m', strtotime("first day of previous month"));
        $year = date('Y', strtotime("first day of previous month"));
        $timestamp = time();
        $days = 1 + (int) date('t', $timestamp) - (int) date('j', $timestamp);
        $ranking = $this->Sale->getRanking($company_id, $month, $year);
        $incl = false;
        $rank = 0;

        foreach($ranking as $r) {
            $rank++;

            if ($r['ranking']['salesman_id'] == $id) {
                $incl = true;
                break;
            }
        }

        $total = count($ranking);

        if ( ! $incl ) {
            $total += 1;
            $rank  += 1;
        }

        $this->data['ranking'] = [
            'chart' => $this->Sale->getMonthly($id, $year),
            'rank'  => $rank,
            'total' => $total,
            'days'  => $days,
        ];
    }
    
    public function lotteries($payload){
        $this->LotteryIntent = new LotteryIntent();
        $this->Lottery = new Lottery();
        
        if($this->isPost()){
            $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);

            if($semaphore && sem_acquire($semaphore)){
                $id = $payload['lottery'];      
                $count = (int)$payload['count'];
                $this->updateAuthData();
                $auth = $this->Auth->getData();
                $status = 0;    

                if ($lottery = $this->Lottery->fetchFrontend($auth, $id)){
                    if ($lottery['updated'] === $payload['timestamp']){
                        $requiredPoints = $lottery['points'] * $count;
                        
                        if ($auth['data']['points'] >= $requiredPoints){
                            if($this->LotteryIntent->intent($auth, $lottery, $count)){
                                $this->updateAuthData();
                                $auth = $this->Auth->getData();
                                $status = 1;                                
                            } else {
                                throw new InternalErrorException('Cannot make lottery intent!');
                            }
                        } else {
                            $status = 3;
                        }
                    } else {
                        $status = 2;
                    }
                }

                $this->data['status'] = $status;
                $this->data['auth'] = [
                    'points' => $auth['data']['points'],
                    'updated' => $auth['data']['updated'],
                    'first_name' => $auth['data']['first_name'],
                    'last_name' => $auth['data']['last_name'],
                ];
                        
                sem_release($semaphore);
            } else {
                throw new InternalErrorException('Block expired lottery!');
            }
            
        } else {
            $this->data['lotteries'] = $this->Lottery->fetchFrontend($this->Auth->getData());
        }
    }
    
    public function roulettes($payload){
        $this->RouletteIntent = new RouletteIntent();
        $this->Roulette = new Roulette();
        
        if($this->isPost()){        
            $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);

            if($semaphore && sem_acquire($semaphore)){
                $id = $payload['roulette'];                            
                $this->updateAuthData();
                $auth = $this->Auth->getData();
                $status = 0;    

                if ($roulette = $this->Roulette->fetchFrontend($auth, $id)){
                    if ($roulette['updated'] === $payload['timestamp']){
                        if ($auth['data']['points'] >= $roulette['points']){
                            if($this->RouletteIntent->intent($auth, $roulette)){
                                $this->updateAuthData();
                                $auth = $this->Auth->getData();
                                $status = 1;                                
                                $this->data['prize'] = $this->RouletteIntent->readField('roulette_prize_id');
                            } else {
                                throw new InternalErrorException('Cannot make roulette intent!');
                            }
                        } else {
                            $status = 3;
                        }
                    } else {
                        $status = 2;
                    }
                }

                $this->data['status'] = $status;
                $this->data['auth'] = [
                    'points' => $auth['data']['points'],
                    'updated' => $auth['data']['updated'],
                    'first_name' => $auth['data']['first_name'],
                    'last_name' => $auth['data']['last_name'],
                ];
                        
                sem_release($semaphore);
            } else {
                throw new InternalErrorException('Block expired roulette!');
            }
        } else {
            $this->data['roulettes'] = $this->Roulette->fetchFrontend($this->Auth->getData());
        }
    }

    public function challenges($payload) {
        $this->ChallengeUser = new ChallengeUser();
        $this->Challenge = new Challenge();
        $this->User = new User();
        
        if($this->isPost()){    
            $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);

            if($semaphore && sem_acquire($semaphore)){
                $id = $payload['id'];                            
                $this->updateAuthData();
                $auth = $this->Auth->getData();
                $message = 'Desafio aceptado';

                if ($challenge = $this->Challenge->loadById($id)){
                    $this->Challenge->begin();
                    $status = true;

                    $points = (int) $auth['data']['points'];
                    $required = (int) $this->Challenge->readField('cost');

                    if ($points < $required) {
                        $message = 'No posee los puntos para aceptar el desafio';
                        $status = false;
                        $this->Challenge->rollback();
                    } else {
                        $accepted = $this->ChallengeUser->find('first', [
                            'conditions' => [
                                'ChallengeUser.challenge_id' => $id,
                                'ChallengeUser.user_id' => $auth['id'],
                            ]
                        ]);

                        if ($accepted) {
                            $message = 'Ya has aceptado este desafio';
                            $status = false;
                            $this->Challenge->rollback();
                        } else {
                            $p = $points - $required;
                            $this->User->setPoints($auth['id'],$p);
                            $this->ChallengeUser->upsert($auth['id'], $id);
                            $this->updateAuthData();
                            $auth = $this->Auth->getData();
                            $this->Challenge->commit();
                        }
                    }
                }
                
                $this->data['message'] = $message;
                $this->data['success'] = $status;
                $this->data['auth'] = [
                    'points' => $auth['data']['points'],
                    'updated' => $auth['data']['updated'],
                    'first_name' => $auth['data']['first_name'],
                    'last_name' => $auth['data']['last_name'],
                ];
                        
                sem_release($semaphore);
            } else {
                throw new InternalErrorException('Block expired roulette!');
            }
        }
        else {
            $this->data['challenges'] = $this->Challenge->fetchFrontend($this->Auth->getData());
        }
    }
    
    private function updateAuthData(){
        $id = $this->Auth->getId();
        
        if($id > 0){
            $this->User = new User();
            
            if($this->User->loadById($id)){
                $data = $this->User->getData();
                $this->Auth->setFrontendData($id,$data);
            }
        }
    }
    
    public function json(){
        return json_encode($this->data);
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}