<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('RunSale','Model');
App::uses('RunSaleCompany','Model');

class DrawEndpointComponent extends EndpointComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }

    private $RunSaleCompany;
    private $RunSale;
    private $output = [];

    public function main_GET($payload){
        $this->RunSale = new RunSale();
        $this->RunSaleCompany = new RunSaleCompany();
        
        if($this->RunSale->loadById($payload['run_sale']) === false){
            throw new NotFoundException("Cannot find RunSale({$payload['run_sale']})");
        }
        
        $winner = $this->RunSaleCompany->getCompany($this->RunSale->readField('winner_id'));
        
        $this->output = [
            'run_sale' => $this->RunSale->getData(),
            'winner' => $winner
        ];
    }

    public function main_POST($payload){
        $this->RunSale = new RunSale();
        
        if($this->RunSale->loadById($payload['run_sale']) === false){
            throw new NotFoundException("Cannot find RunSale({$payload['run_sale']})");
        }
        
        $data = $this->RunSale->getData();
        
        if(is_null($data['draw_at'])){
            if($data['draw'] === 'manual'){
                $score = $data['score'];
                $goal = $data['goal'];
                
                if($score >= $goal){
                    $this->output = $this->RunSale->draw($payload['run_sale']);
                }
            }
        }
    }
    
    public function json(){
        $json = json_encode($this->output);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}