<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('Lottery','Model');
App::uses('LotteryCompany','Model');

class DrawLotteryEndpointComponent extends EndpointComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }

    private $LotteryCompany;
    private $Lottery;
    private $output = [];

    public function main_GET($payload){
        $this->Lottery = new Lottery();
        $this->LotteryCompany = new LotteryCompany();
        
        if($this->Lottery->loadById($payload['lottery']) === false){
            throw new NotFoundException("Cannot find Lottery({$payload['lottery']})");
        }
        
        $this->output = [
            'lottery' => $this->Lottery->getData(),
            'winner' => $this->Lottery->winner()
        ];
    }

    public function main_POST($payload){
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);

        if($semaphore && sem_acquire($semaphore)){
            $this->Lottery = new Lottery();

            if($this->Lottery->loadById($payload['lottery']) === false){
                throw new NotFoundException("Cannot find Lottery({$payload['lottery']})");
            }

            $data = $this->Lottery->getData();
            if($data['draw'] === 'electronic'){
                if(! $this->Lottery->winner()){
                    $this->Lottery->draw();
                }

                $this->output = [
                    'lottery' => $this->Lottery->getData(),
                    'winner' => $this->Lottery->winner()
                ];
            } else {
                throw new InternalErrorException('Lottery is not electronic drawable');
            }
            
            sem_release($semaphore);
        } else {
            throw new InternalErrorException('Block expired lottery draws!');
        }
    }
    
    public function json(){
        $json = json_encode($this->output);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}