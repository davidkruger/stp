<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('LotteryIntent','Model');
App::uses('Lottery','Model');

class PrintLotteryEndpointComponent extends EndpointComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }

    private $LotteryIntent;
    private $Lottery;

    public function main($payload){
        $this->Lottery = new Lottery();
        
        if($this->Lottery->loadById($payload['lottery']) === false){
            throw new NotFoundException("Cannot find Lottery({$payload['lottery']})");
        }
    }
    
    private function output() {
        $this->LotteryIntent = new LotteryIntent();
        
        return $this->LotteryIntent->find('all',[
            'fields' => [
                'LotteryIntent.*',
                'Seller.*',
                'Company.*'
            ],
            'conditions' => [
                'LotteryIntent.lottery_id' => $this->Lottery->id
            ],
            'joins' => [
                'INNER JOIN sellers AS Seller ON Seller.id=LotteryIntent.user_id',
                'INNER JOIN companies AS Company ON Company.id=LotteryIntent.company_id',
            ],
            #'limit' => 20
        ], true);
    }
    
    public function json(){
        $json = json_encode([
            'tickets' => $this->output()
        ]);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}