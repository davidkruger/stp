<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('Stock','Model');

class StockEndpointComponent extends EndpointComponent{
    private $Stock;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->Stock = new Stock();
    }
    
    public function check($payload){
        $company = (int) $payload['company_id'];
        $product = (int) $payload['product_id'];
        $q = (int) $payload['quantity'];
        
       $current = $this->Stock->resolvCurrent($company,$product);
        
        $this->output = [
            'current' => $current,
            'needed' => $q,
            'warning' => $current < $q
        ];
    }
    
    public function json(){
        $json = json_encode($this->output);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}