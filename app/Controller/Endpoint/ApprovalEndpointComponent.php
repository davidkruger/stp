<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('Sale','Model');
App::uses('MassiveSale','Model');

class ApprovalEndpointComponent extends EndpointComponent{
    private $MassiveSale;
    private $Sale;
    private $Auth;
    private $output = [];
    private $id;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        $this->Auth = $controller->Auth;
        $this->MassiveSale = new MassiveSale();
        $this->Sale = new Sale();
    }
    
    public function responsableReject($payload){
        if($this->Auth->getData()['data']['role'] !== 'encargado'){
            throw new ForbiddenException();
        }
        
        $this->id = $payload['id'];
        
        $this->output['redirect'] = [
            'controller' => 'frontend',
            'action' => 'reject',
            'id' => $this->id
        ];
        
        $cnd = [];
        $cnd['Sale.company_id'] = $this->Auth->getData()['data']['company_id'];
        $cnd['Sale.approval'] = 'ninguna';
        $cnd['Sale.id'] = $this->id;
        $cnd['Sale.rejected'] = 0;
        
        if(!$this->Sale->loadByConditions($cnd)){
            return $this->error('Venta no valida');
        }
        
        try{
            $this->Sale->begin();
            
            if(!$this->Sale->saveField('rejected',1)){
                throw new Exception("Cannot save Sale@{$this->id}<rejected,1>");
            }
            
            if(!$this->Sale->saveField('rejection_comments',$payload['rejection_comments'])){
                throw new Exception("Cannot save Sale@{$this->id}<rejection_comments,1>");
            }
            
            if(!$this->Sale->saveField('rejected_at',date('Y-m-d H:i:s'))){
                throw new Exception("Cannot save Sale@{$this->id}<rejected_at,1>");
            }
            
            $this->Sale->commit();
        
            $this->output['redirect'] = [
                'controller' => 'frontend',
                'action' => 'pending_sales',
            ];
            
            return $this->success('Venta rechazada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::responsableReject() - {$e->getMessage()}");
            return $this->error('Error en el Rechazo de la Venta');
        }
    }
    
    public function responsable($payload){
        if($this->Auth->getData()['data']['role'] !== 'encargado'){
            throw new ForbiddenException();
        }
        
        $this->id = $payload['id'];
        
        $this->output['redirect'] = [
            'controller' => 'frontend',
            'action' => 'approve',
            'id' => $this->id
        ];
        
        $cnd = [];
        $cnd['Sale.company_id'] = $this->Auth->getData()['data']['company_id'];
        $cnd['Sale.approval'] = 'ninguna';
        $cnd['Sale.id'] = $this->id;
        $cnd['Sale.rejected'] = 0;
        
        if(!$this->Sale->loadByConditions($cnd)){
            return $this->error('Venta no valida');
        }
        
        try{
            $this->Sale->begin();
            $this->Sale->writeField('approval', 'responsable');
            
            if(!$this->Sale->saveField('approval','responsable')){
                throw new Exception("Cannot save Sale@{$this->id}<approval,responsable>");
            }
            
            if(!$this->Sale->saveField('extra_target',$this->Auth->getData()['id'])){
                throw new Exception("Cannot save Sale@{$this->id}<extra_target,responsable>");
            }
            
//            if(!$this->Sale->updateExtraTargetPoints(null,null)){
//                throw new Exception("Cannot exec Sale@{$this->id}<updateExtraTargetPoints>");
//            }
            
            $this->Sale->commit();
        
            $this->output['redirect'] = [
                'controller' => 'frontend',
                'action' => 'pending_sales',
            ];
            
            return $this->success('Venta aprobada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::responsable() - {$e->getMessage()}");
            return $this->error('Error en la Aprobacion de la Venta');
        }
    }
    
    public function samsung($payload){
        $this->id = $payload['id'];
        
        $this->output['redirect'] = [
            'controller' => 'sales',
            'action' => 'detail',
            'id' => $this->id
        ];
        
        $cnd = [];
        $cnd['Sale.approval'] = 'responsable';
        $cnd['Sale.id'] = $this->id;
        $cnd['Sale.rejected'] = 0;
        
        if(!$this->Sale->loadByConditions($cnd)){
            return $this->error('Venta no valida');
        }
        
        try{
            $this->Sale->begin();
            
            if(!$this->Sale->saveField('approval','samsung')){
                throw new Exception("Cannot save Sale@{$this->id}<approval,samsung>");
            }
            
            $this->Sale->writeField('approval', 'samsung');
            
            if(!$this->Sale->updateSalesmanPoints(null,null)){
                throw new Exception("Cannot exec Sale@{$this->id}<updateSalesmanPoints>");
            }
            
            if(!$this->Sale->updateExtraTargetPoints(null,null)){
                throw new Exception("Cannot exec Sale@{$this->id}<updateExtraTargetPoints>");
            }
            
            if(!$this->Sale->updateResponsiblePoints(null,null)){
                throw new Exception("Cannot exec Sale@{$this->id}<updateResponsiblePoints>");
            }
            
            if(!$this->Sale->updateEncargadosPoints(null,null)){
                throw new Exception("Cannot exec Sale@{$this->id}<updateEncargadosPoints>");
            }
            
            if(!$this->Sale->updateRunSales(null,null)){
                throw new Exception("Cannot exec Sale@{$this->id}<updateRunSales>");
            }
            
            $this->Sale->commit();
            
            return $this->success('Venta aprobada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::samsung() - {$e->getMessage()}");
            return $this->error('Error en la Aprobacion de la Venta');
        }
    }
    
    public function samsungReject($payload){
        $this->id = $payload['id'];
        
        $this->output['redirect'] = [
            'controller' => 'sales',
            'action' => 'show_manual_pending',
        ];
        
        $cnd = [];
        $cnd['Sale.approval'] = 'responsable';
        $cnd['Sale.id'] = $this->id;
        $cnd['Sale.rejected'] = 0;
        
        if(!$this->Sale->loadByConditions($cnd)){
            return $this->error('Venta no valida');
        }
        
        try{
            $this->Sale->begin();
            
            if(!$this->Sale->saveField('rejected',1)){
                throw new Exception("Cannot save Sale@{$this->id}<rejected,1>");
            }
            
            if(!$this->Sale->saveField('rejection_comments', $payload['rejection_comments'])){
                throw new Exception("Cannot save Sale@{$this->id}<rejection_comments,1>");
            }
            
            if(!$this->Sale->saveField('rejected_at',date('Y-m-d H:i:s'))){
                throw new Exception("Cannot save Sale@{$this->id}<rejected_at,1>");
            }
            
            
            $this->Sale->commit();
            
            return $this->success('Venta rechazada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::samsungReject() - {$e->getMessage()}");
            return $this->error('Error en el Rechazo de la Venta');
        }
    }
    
    public function samsungMassive($payload){
        $this->id = $payload['id'];
        
        $cnd = [];
        $cnd['MassiveSale.approval'] = 'responsable';
        $cnd['MassiveSale.id'] = $this->id;
        $cnd['MassiveSale.rejected'] = 0;
        
        if(!$this->MassiveSale->loadByConditions($cnd)){
            return $this->error('Venta Masiva no valida');
        }
        
        $cnd2 = [];
        $cnd2['Sale.approval'] = 'responsable';
        $cnd2['Sale.massive_sale_id'] = $this->id;
        
        $sales = $this->Sale->find('all', [
            'conditions' => $cnd2
        ]);
        
        try{
            
            $this->Sale->begin();
            
            foreach($sales as $sale){
                $saleId = $sale['id'];
                
                if(!$this->Sale->loadById($saleId)){
                    throw new Exception("Cannot load Sale@{$saleId}");
                }
                
                if(!$this->Sale->saveField('approval','samsung')){
                    throw new Exception("Cannot save Sale@{$saleId}<approval,samsung>");
                }
            
                $this->Sale->writeField('approval', 'samsung');

                if(!$this->Sale->updateSalesmanPoints(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateSalesmanPoints>");
                }

                if(!$this->Sale->updateExtraTargetPoints(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateExtraTargetPoints>");
                }

                if(!$this->Sale->updateResponsiblePoints(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateExtraTargetPoints>");
                }

                if(!$this->Sale->updateEncargadosPoints(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateEncargadosPoints>");
                }

                if(!$this->Sale->updateRunSales(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateRunSales>");
                }
            }
                
            if(!$this->MassiveSale->saveField('approval','samsung')){
                throw new Exception("Cannot save MassiveSale{$this->id}<approval,samsung>");
            }

            $this->output['redirect'] = [
                'controller' => 'sales',
                'action' => 'detail_massive',
                'id' => $this->id
            ];
            
            $this->Sale->commit();
            return $this->success('Venta Masiva aprobada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::samsungMassive() - {$e->getMessage()}");
            return $this->error('Error en la Aprobacion de la Venta Masiva');
        }
    }
    
    public function samsungMassiveReject($payload){
        $this->id = $payload['id'];
        
        $cnd = [];
        $cnd['MassiveSale.approval'] = 'responsable';
        $cnd['MassiveSale.id'] = $this->id;
        $cnd['MassiveSale.rejected'] = 0;
        
        if(!$this->MassiveSale->loadByConditions($cnd)){
            return $this->error('Venta Masiva no valida');
        }
        
        $cnd2 = [];
        $cnd2['Sale.approval'] = 'responsable';
        $cnd2['Sale.rejected'] = 0;
        $cnd2['Sale.massive_sale_id'] = $this->id;
        
        $sales = $this->Sale->find('all', [
            'conditions' => $cnd2
        ]);
        
        try{
            
            $this->Sale->begin();
            
            foreach($sales as $sale){
                $saleId = $sale['id'];
                
                if(!$this->Sale->loadById($saleId)){
                    throw new Exception("Cannot load Sale@{$saleId}");
                }
                
                if(!$this->Sale->saveField('rejected',1)){
                    throw new Exception("Cannot save Sale@{$saleId}<rejected,1>");
                }
            }
                
            if(!$this->MassiveSale->saveField('rejected',1)){
                throw new Exception("Cannot save MassiveSale{$this->id}<rejected,1>");
            }

            $this->output['redirect'] = [
                'controller' => 'sales',
                'action' => 'show_massive_pending',
            ];
            
            $this->Sale->commit();
            return $this->success('Venta Masiva rechazada correctamente');
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::samsungMassiveReject() - {$e->getMessage()}");
            return $this->error('Error en el Rechazo de la Venta Masiva');
        }
    }
    
    private function success($message){
        $this->output['flash'] = [
            'kind' => 'success',
            'message' => $message
        ];
    }
    
    private function error($message){
        $this->output['flash'] = [
            'kind' => 'error',
            'message' => $message
        ];
    }
    
    public function json(){
        $json = json_encode($this->output);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}