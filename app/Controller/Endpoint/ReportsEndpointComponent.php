<?php

Configure::load('scaffold_reports','default',false);    
App::uses('ExcelComponent','KlezData.Controller/DataDrivers');
App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('Company','Model');
App::uses('Salesman','Model');
App::uses('Redeem','Model');
App::uses('Sale','Model');

class ReportsEndpointComponent extends EndpointComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }

    private $Salesman;
    private $Company;
    private $Redeem;
    private $Sale;
    private $output = [];

    public function summary($payload){
        $this->Company = new Company();
        $this->Salesman = new Salesman();
        $this->Redeem = new Redeem();

        $companies = $this->Company->summary($payload);
        $salesmen = $this->Salesman->summary($payload);
        $redeems = $this->Redeem->summary($payload);
        
        foreach($companies as $company){
            $id = $company['id'];
            
            if(!isset($this->output['report']["#{$id}"])){
                $this->output['report']["#{$id}"] = $company;
                $this->output['report']["#{$id}"]['salesmen'] = [];
                $this->output['report']["#{$id}"]['total'] = 0;
            }
        }
        
        foreach($salesmen as $salesman){
            $id = $salesman['id'];
            $company = $salesman['company_id'];
            
            if(!isset($this->output['report']["#{$company}"])){
                continue;
            }
            
            unset($salesman['company_id']);
            $salesman['redeemed'] = 0;
            $this->output['report']["#{$company}"]['salesmen']["#{$id}"] = $salesman;
            $this->output['report']["#{$company}"]['total'] += $salesman['acum'];
        }
        
        foreach($redeems as $redeem){
            $salesman = $redeem['user_id'];
            $company = $redeem['company_id'];
            
            if(!isset($this->output['report']["#{$company}"])){
                continue;
            }
            
            if(!isset($this->output['report']["#{$company}"]['salesmen']["#{$salesman}"])){
                continue;
            }
            
            $this->output['report']["#{$company}"]['salesmen']["#{$salesman}"]['acum'] += $redeem['redeemed'];
            $this->output['report']["#{$company}"]['salesmen']["#{$salesman}"]['redeemed'] = $redeem['redeemed'];
        }
        
        foreach($this->output['report'] as &$compdata){
            $compdata['totals'] = [
                'acum' => 0,
                'redeemed' => 0,
                'delta' => 0
            ];
            
            foreach($compdata['salesmen'] as $hash => &$salesman){
                $salesman['acum'] = $salesman['acum'] - $salesman['redeemed'];
                
                $compdata['totals']['redeemed'] += $salesman['redeemed'];
                $compdata['totals']['delta'] += ($salesman['acum'] - $salesman['redeemed']);
                $compdata['totals']['acum'] += $salesman['acum'];
            }
            
            uasort($compdata['salesmen'], [ 'ReportsEndpointComponent', 'sortSalesmen' ]);
        }
        
        uasort($this->output['report'], [ 'ReportsEndpointComponent', 'sortCompanies' ]);
        $this->output['filter']['a'] = $payload['a'];
        $this->output['filter']['b'] = $payload['b'];
        
        if(isset($payload['excel'])){
            $this->summaryExcel($payload);
        }
    }
    
    private function summaryExcel($payload){        
        $this->Sale = new Sale();
        $this->Sale->shapeshift('summary');
        $schema = $this->Sale->provideReadableSchema();
        
        if(@$payload['no_redeem']){
            unset($schema['redeemed']);
            unset($schema['delta']);
        }
        
        $collection = new ComponentCollection();
        $this->Excel = new ExcelComponent($collection);
        $this->Excel->initialize(new Controller());
        $this->Excel->setStyles(Configure::read('Klezkaffold.report.sales_xlsx.excel.styles'));
        
        foreach($this->output['report'] as $compdata){
            $this->Excel->newSheet();
            $this->Excel->sheet($compdata['name']);
            $this->Excel->table($schema,$compdata['salesmen']);
            $this->Excel->rowJump();
            $this->Excel->col();
            $this->Excel->cell('Totales');
            $this->Excel->style('value');
            $this->Excel->columnFit(true);
            $this->Excel->colJump();

            $this->Excel->cell($compdata['totals']['acum']);
            $this->Excel->style('value');
            $this->Excel->columnFit(true);
            $this->Excel->colJump();

            if(!@$payload['no_redeem']){
                $this->Excel->cell($compdata['totals']['redeemed']);
                $this->Excel->style('value');
                $this->Excel->columnFit(true);
                $this->Excel->colJump();

                $this->Excel->cell($compdata['totals']['delta']);
                $this->Excel->style('value');
                $this->Excel->columnFit(true);
                $this->Excel->colJump();
            }
        }
        
        $output = $this->Excel->output();
        $output['blob'] = base64_encode($output['blob']);
        $output['file'] = 'reporte.xlsx';
        $output['timestamp'] = time();
        
        $this->output['payload']['report'] = $output;
    }
    
    public function products($payload){
        $this->Sale = new Sale();
        $this->Sale->shapeshift('reportProductsFilters');
        $this->Sale->validate = [];
        $this->Sale->prepareForStore($payload, null);
        
        if($this->Sale->validateData()){ 
            $payload['no_redeem'] = true;
            $this->summary($payload);
        }
        else{
            $this->output['messages'] = $this->Sale->validationErrors;
        }
        
        $this->output['data'] = $this->Sale->getData();
    }
    
    public function companies($payload){
        $this->Sale = new Sale();
        $this->Sale->shapeshift('reportCompaniesFilters');
        $this->Sale->validate = [];
        $this->Sale->prepareForStore($payload, null);
        
        if($this->Sale->validateData()){        
            $this->summary($payload);
        }
        else{
            $this->output['messages'] = $this->Sale->validationErrors;
        }
        
        $this->output['data'] = $this->Sale->getData();
    }
    
    public function top10($payload){
        $this->Salesman = new Salesman();
        $this->Salesman->shapeshift('top10');
        
        if(isset($payload['mod'])){
            switch ($payload['mod']){
                case 'lastmonth':
                    $report =  $this->Salesman->top10lastmonth($payload);
                    $this->output['report']['lastmonth']['data'] = $report;
                    $this->output['report']['lastmonth']['schema'] =  $this->Salesman->provideReadableSchema();
                    $this->output['report']['lastmonth']['title'] =  'Top 10 Mes Pasado';
                    break;
                case 'divisionlastmonth':
                    $divisions =  $this->Salesman->divisionlastmonth($payload);
                case 'division':
                    if(!isset($divisions)){
                        $divisions =  $this->Salesman->division($payload);
                    }
                    
                    foreach($divisions as $division){
                        $index = '#' . $division['division']['id'];
                        $this->output['report'][$index]['data'] = $division['data'];
                        $this->output['report'][$index]['schema'] =  $this->Salesman->provideReadableSchema();
                        $this->output['report'][$index]['title'] =  $division['division']['name'];
                        $this->output['report'][$index]['total'] = 0;
                        
                        foreach($division['data'] as $d){
                            $this->output['report'][$index]['total'] += $d['acum'];
                        }
                    }
                    
                    uasort($this->output['report'], [ 'ReportsEndpointComponent', 'sortTop10']);
                    break;
            }
        }
        else{
            $report =  $this->Salesman->top10general($payload);
            $this->output['report']['general']['data'] = $report;
            $this->output['report']['general']['schema'] =  $this->Salesman->provideReadableSchema();
            $this->output['report']['general']['title'] =  'Top 10 General';
        }
        
        
        if(isset($payload['excel'])){
            $this->top10Excel();
        }
    }
    
    public static function sortTop10($a,$b){
        return $b['total'] - $a['total'];
    }
    
    private function top10Excel(){        
        $this->Salesman = new Salesman();
        $this->Salesman->shapeshift('top10');
        
        $collection = new ComponentCollection();
        $this->Excel = new ExcelComponent($collection);
        $this->Excel->initialize(new Controller());
        $this->Excel->setStyles(Configure::read('Klezkaffold.report.sales_xlsx.excel.styles'));
        
        foreach($this->output['report'] as $report){
            $report['totals'] = [
                'acum' => 0,
                'redeemed' => 0,
                'delta' => 0,
            ];
            
            foreach ($report['data'] as $r){
                $report['totals']['acum'] += $r['acum'];
            }
            
            $this->Excel->newSheet();
            $this->Excel->sheet($report['title']);
            $this->Excel->table($this->Salesman->provideReadableSchema(),$report['data']);
            $this->Excel->rowJump();
            $this->Excel->col();
            $this->Excel->cell('Totales');
            $this->Excel->style('value');
            $this->Excel->columnFit(true);
            $this->Excel->colJump();

            $this->Excel->cell($report['totals']['acum']);
            $this->Excel->style('value');
            $this->Excel->columnFit(true);
            $this->Excel->colJump();
        }
        
        $output = $this->Excel->output();
        $output['blob'] = base64_encode($output['blob']);
        $output['file'] = 'top10.xlsx';
        $output['timestamp'] = time();
        
        $this->output['payload']['report'] = $output;
    }
    
    public static function sortSalesmen($a,$b){
        return $b['acum'] - $a['acum'];
    }
    
    public static function sortCompanies($a,$b){
        return $b['total'] - $a['total'];
    }
    
    public function json(){
        $json = json_encode($this->output);
        
        if(json_last_error() > 0){
            error_log("JSON-ERROR : " . json_last_error_msg());
        }
        
        return $json;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}