<?php

App::uses('BreadCrumbAppController', 'Controller');

class RunSalesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',
        'Draw'
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
        'Draw'
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('run_sales.show',$page); 
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('run_sales',$id);    
    }
        
    public function draw(){ 
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('run_sales',$id);
        
        if($this->request->is('POST')){
            $this->Draw->write($id);    
        }
        
        $this->Draw->read($id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('run_sales',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'run_sales',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Competencia cargada con éxito',
                    'error' => 'No se pudo crear la Competencia'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('run_sales');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('run_sales');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('run_sales',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'run_sales',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Competencia editada con éxito',
                    'error' => 'No se pudo editar la Competencia'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('run_sales',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('run_sales');    
    }
    
    public function prize_img(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('prize_img.run_sales',$id);    
    }
    
    public function xlsx(){
        $this->KlezkaffoldWeb->downloadReport('xlsx.run_sales',[
            'run_sale_id' => $this->route('id')
        ]);
    }

    public function provideModule() {
        return 'run_sales';
    }

    public function provideName($data) {
        return $data['payload']['data']['description'];
    }
    
    public function dashboard(){
        $this->redirect([
            'controller' => 'run_sales',
            'action' => 'show'
        ]);
    }
}