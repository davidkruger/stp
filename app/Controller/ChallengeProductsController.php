<?php

App::uses('AppController', 'Controller');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ChallengeProductsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $challenge = $this->route('challenge');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('challenge_products',[
                'formdata' => $this->data,
                'challenge' => $challenge,
                'challenge_id' => $challenge,
                'id' => $id,
                'redirect' => [
                    'controller' => 'challenges',
                    'action' => 'challenge_products',
                    'id' => $challenge,
                    'title' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto del Desafio editado con éxito',
                    'error' => 'No se pudo editar el Producto del Desafio'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('challenge_products',$id,[
                'data' => [
                    'challenge' => $challenge,
                    'challenge_id' => $challenge
                ]
            ]);      
        }
        
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'dashboard'], Configure::read("Sitemap.challenges.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('challenges',$this->route('challenge'));   
        $detail = $this->KlezkaffoldReader->getData();
        $challenge = $detail['payload']['data']['title'];       
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'detail','id' => $this->route('challenge'),'slug' => KlezkaffoldComponent::resolvParamSlug($challenge)], $challenge, 'after');
        
        $this->KlezkaffoldReader->detail('challenge_products',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['title']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
    
    public function edit_xhr(){
        $id = $this->route('challenge');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('challenge_products', [
            'challenge_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('challenge');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['challenge_id'] = $id;
            $this->params->data['challenge'] = $id;
            
            $this->KlezkaffoldWeb->add('challenge_products',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'challenges',
                    'action' => 'challenge_products',
                    'id' => $id,
                    'title' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto asignado al logro con éxito',
                    'error' => 'No se pudo asignar el Producto al Desafio',
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('challenge_products',0,[
                'challenge' => $id,
                'challenge_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'dashboard'], Configure::read("Sitemap.challenges.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('challenges',$this->route('challenge'));   
        $detail = $this->KlezkaffoldReader->getData();
        $challenge = $detail['payload']['data']['title'];       
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'detail','id' => $this->route('challenge'),'slug' => KlezkaffoldComponent::resolvParamSlug($challenge)], $challenge, 'after');
    }
    
    public function add_xhr(){
        $id = $this->route('challenge');
        $this->KlezkaffoldWeb->requestAutocompleteForm('challenge_products', [
            'challenge_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $challenge = $this->route('challenge');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('challenge_products',[
                'challenge' => $challenge,
                'challenge_id' => $challenge,
                'id' => $id,
                'redirect' => [
                    'controller' => 'challenges',
                    'action' => 'challenge_products',
                    'id' => $challenge,
                    'title' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto eliminado del Desafio con éxito',
                    'error' => 'No se pudo eliminar el Producto del Desafio'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('challenge_products',$id,[
                'challenge' => $challenge,
                'challenge_id' => $challenge
            ]);      
        }        
        
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'dashboard'], Configure::read("Sitemap.challenges.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('challenges',$this->route('challenge'));   
        $detail = $this->KlezkaffoldReader->getData();
        $challenge = $detail['payload']['data']['title'];       
        $this->pushBreadcrumb([ 'controller' => 'challenges', 'action' => 'detail','id' => $this->route('challenge'),'slug' => KlezkaffoldComponent::resolvParamSlug($challenge)], $challenge, 'after');
        
        $this->KlezkaffoldReader->detail('challenge_products',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['title']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
}