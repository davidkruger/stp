<?php

App::uses('BreadCrumbAppController', 'Controller');

class RoulettesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('roulettes.show',$page); 
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('roulettes',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('roulettes',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'roulettes',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Ruleta cargada con éxito',
                    'error' => 'No se pudo crear la Ruleta'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('roulettes');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('roulettes');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('roulettes',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'roulettes',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Ruleta editada con éxito',
                    'error' => 'No se pudo editar la Ruleta'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('roulettes',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('roulettes');    
    }
    
    public function xlsx(){
        $this->KlezkaffoldWeb->downloadReport('xlsx.roulettes',[
            'roulette_id' => $this->route('id')
        ]);
    }

    public function provideModule() {
        return 'roulettes';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
    
    public function dashboard(){
        $this->redirect([
            'controller' => 'roulettes',
            'action' => 'show'
        ]);
    }

    public function roulette_prizes(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('roulettes.roulette_prizes',$page,[
            'roulette' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['roulette'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
}