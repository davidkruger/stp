<?php

App::uses('AppController', 'Controller');

class RedeemModerationsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Detail',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('redeem_moderations',$id);    
    }
}