<?php

App::uses('AppController', 'Controller');
    
class FrontendController extends AppController {
    public $components = [ 'Frontend','Approval' ];
    public $helpers = [ 'Frontend', 'KlezBackend.Login', 'FrontendShow' ];
    private $layoutData = [];
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->layout = 'frontend';
        $this->Frontend->dashboard('frontend');
        $this->Frontend->injection($this->viewVars[API_DATA]);
        $this->layoutData = $this->viewVars[API_DATA];
        
        if((@$this->layoutData['auth']['id'])){
            $this->Frontend->pendings();
            $this->set('pendings',(int) $this->viewVars[API_DATA]['pendings']);
        }
        else{
            $this->set('pendings',0 );
        }
    }
    
    public function beforeRender() {

        if(isset($this->viewVars[API_DATA]['payload'])){
            $this->layoutData['payload'] = $this->viewVars[API_DATA]['payload'];
        }
        
        $this->viewVars[API_DATA] = array_merge($this->viewVars[API_DATA],$this->layoutData);
        parent::beforeRender();
    
    }
    
    private function errorRequest(){
        $code = $this->response->statusCode();
        $codes = $this->response->httpCodes();
        
        $error = [
            'code' => $code,
            'name' => $codes[$code]
        ];
        
        $this->write('error',$error);
        $this->layout = 'error';
    }
    
    public function home(){

        if(@$this->layoutData['auth']['id']){
            switch (@$this->layoutData['auth']['data']['role']){
                case 'vendedor':
                case 'responsable':
                case 'encargado':
                    $this->runSales();
                    $this->homePrizes();
                    $this->achievements();
                    $this->Frontend->redeems();
                    $this->Frontend->roulettes();
                    $this->Frontend->lotteries();
                    $this->Frontend->ranking();
                    $this->challenges();
//                case 'responsable':
                    break;
            }
        }
    }

    private function challenges() {
        if($this->request->is('POST')){
            if(isset($this->data['challenge_id'])){
                $this->Frontend->acceptChallenge($this->data['challenge_id']);
                $this->injectAuth();
                $this->injectMessage();
                if($this->viewVars[API_DATA]['success']){
                    $this->redirect([
                        'controller' => 'frontend',
                        'action' => 'home',
                    ]);
                }
            }
        }

        $this->Frontend->challenges();
    }
    
    public function home_xhr(){
        switch($this->data['context']){
            case 'roulette':
                $this->Frontend->roulettes($this->data, 'POST');
                break;
            case 'lottery':
                $this->Frontend->lotteries($this->data, 'POST');
                break;
            default:
                throw new NotFoundException();
        }
    }
    
    private function achievements(){
        $this->Frontend->achievements();
        $this->layoutData['achievements'] = $this->viewVars[API_DATA]['achievements'];
    }
    
    private function runSales(){
        $this->Frontend->runSales();
        $this->layoutData['run_sales'] = $this->viewVars[API_DATA]['run_sales'];
    }
    
    private function homePrizes(){
        
        if($this->request->is('POST')){
            if(isset($this->data['id'])){
                $this->Frontend->redeem($this->data['id'],@$this->data['count']);
                $this->injectAuth();
                $this->injectMessage();
                if($this->viewVars[API_DATA]['success']){
                    $this->redirect([
                        'controller' => 'frontend',
                        'action' => 'prizes',
                    ]);
                }
            }
        }
        
        $this->Frontend->prizes();
        $this->injectAuth();
        
        $this->view = 'home_vendedor';

        if(empty($this->viewVars[API_DATA]['prizes'])){
            $this->flashToast('warning','No hay Premios disponibles');
        }
    }
    public function achievement_icon_0(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('icon_0.achievements',$id);   
        exit;
    }

    public function achievement_icon_1(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('icon_1.achievements',$id);   
        exit;
    }
    
    public function lottery_prize_photo(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('prize_photo.lotteries',$id);   
        exit;
    }
    
    public function catalog_photo(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('image.product_catalogs',$id);   
        exit;
    }
    
    public function product_photo(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('picture.products',$id);   
        exit;
    }

    public function challenge_prize_photo() {
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('prize_img.challenges',$id);   
        exit;
    }
    
    public function run_sale_prize(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('prize_img.run_sales',$id);   
        exit;
    }
    
    public function prize_photo(){
        ob_clean();
        $id = $this->route('id');
        $this->Frontend->image('photo.prizes',$id);   
        exit;
    }
    
    private function injectMessage(){
        if(isset($this->viewVars[API_DATA]['success'])){
            if(isset($this->viewVars[API_DATA]['message'])){
                $message = $this->viewVars[API_DATA]['message'];
                
                if($this->viewVars[API_DATA]['success']){
                    $this->flashToast('success',$message);

                }
                else{
                    $this->flashToast('error',$message);
                }
            }
        }
    }
    
    private function injectAuth(){
        if(isset($this->viewVars[API_DATA]['auth'])){
            $this->layoutData['auth'] = $this->viewVars[API_DATA]['auth'];
        }
    }
    
    public function login(){
        if($this->request->is('POST')){
            $this->Frontend->login($this->data);
        }
        
        $this->Frontend->loginConf();
    }
    
    public function logout(){
        $this->Frontend->logout();
    }
    
    public function byc(){
        $file = __DIR__ . '/../../streamable/bases.pdf';
        $data = [
            'blob' => base64_encode(file_get_contents($file)),
            'file' => 'bases_y_condiciones.pdf',
            'size' => filesize($file),
            'type' => mime_content_type($file),
            'timestamp' => filemtime($file)
        ];
        
        $this->Frontend->rawFileDump([
            'payload' => $data
        ]);
    }
    
    public function prizes(){
        $this->Frontend->redeems();
        
        if(empty($this->viewVars[API_DATA]['redeems'])){
            $this->flashToast('warning','No hay Premios Solicitados');
        }
    }
    
    public function complains(){
        if($this->request->is('POST')){
            $this->params->data['user_id'] = $this->layoutData['auth']['id'];
            
            $this->Frontend->add('complains',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'home'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Su Consulta ha sido enviada',
                    'error' => 'No se pudo enviar la Consulta, revise errores en el formulario'
                ]
            ]);    
        }   
        else{
            $this->Frontend->requestForm('complains');
        }
    }
    
    public function sellers(){
        if($this->request->is('POST')){
            $this->Frontend->deleteSeller($this->data);
            $this->injectMessage();
        }
        
        $this->Frontend->sellers();
        
        if(empty($this->viewVars[API_DATA]['sellers'])){
            if(empty($this->Session->read('Backend.flash'))){
                $this->flashToast('warning','No hay Vendedores registrados');
            }
        }
    }
    
    private function add_seller_responsable(){
        if($this->request->is('POST')){
            $this->params->data['company_id'] = $this->layoutData['auth']['data']['company_id'];
            $this->params->data['role'] = 'vendedor';
            $this->params->data['status'] = 1;
            $this->params->data['extra'] = 5;

            $this->Frontend->add('sellers',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'sellers'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Vendedor agregado satisfactoriamente',
                    'error' => 'No se pudo agregar al Vendedor, revise errores en el formulario'
                ]
            ]);    
            
            $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        }   
        else{
            $this->Frontend->requestForm('sellers');
            $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        }        
        
        $this->Frontend->branches();
    }
    
    private function add_seller_encargado(){
        if($this->request->is('POST')){
            $this->params->data['company_id'] = $this->layoutData['auth']['data']['company_id'];     
            $this->params->data['branch_id'] = $this->layoutData['auth']['data']['branch_id'];
            $this->params->data['role'] = 'vendedor';
            $this->params->data['status'] = 1;
            $this->params->data['extra'] = 5;

            $this->Frontend->add('sellers',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'sellers'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Vendedor agregado satisfactoriamente',
                    'error' => 'No se pudo agregar al Vendedor, revise errores en el formulario'
                ]
            ]);    
            
            $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        }   
        else{
            $this->Frontend->requestForm('sellers');
            $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        }        
    }
    
    public function add_seller(){
        switch ($this->layoutData['auth']['data']['role']){
            case 'responsable':
                $this->add_seller_responsable();
                $this->render('add_seller_responsable');
                break;
            case 'encargado':
                $this->add_seller_encargado();
                $this->render('add_seller_encargado');
                break;
        }
    }
    
    public function assign_points(){
        if($this->request->is('POST')){
            $this->Frontend->assignPoints($this->data);
            $this->injectMessage();
            
            if($this->viewVars[API_DATA]['success']){
                unset($this->viewVars[API_DATA]['payload']);
            }
            else{
                $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
            }
        }
        
        $this->Frontend->company();
        $this->layoutData = array_merge($this->viewVars[API_DATA],$this->layoutData);
        $this->Frontend->sellers();
        
        if(empty($this->viewVars[API_DATA]['sellers'])){
            if(empty($this->Session->read('Backend.flash'))){
                $this->flashToast('warning','No hay Vendedores registrados');
            }
        }
    }
    
    public function rejected(){
        $page = $this->get('p',1);
        $this->Frontend->show('sales.show_rejected',$page); 
    }
    
    public function sales(){
        $page = $this->get('p',1);
        $this->Frontend->show('sales.show_frontend',$page); 
    }
    
    public function pending_sales(){
        $page = $this->get('p',1);
        $this->Frontend->show('sales.show_pending_frontend',$page); 
    }
    
    public function add_sale(){
        if($this->request->is('POST')){
            $this->params->data['user_id'] = $this->layoutData['auth']['id'];
            
            if($this->layoutData['auth']['data']['role'] !== 'vendedor'){
                $this->params->data['extra_target'] = $this->layoutData['auth']['id'];
            }
            
            if(!isset($this->params->data['salesman_id'])){
                $this->params->data['salesman_id'] = $this->layoutData['auth']['id'];
                $this->params->data['branch_id'] = $this->layoutData['auth']['data']['branch_id'];
            }
            else{
                $this->Frontend->seller($this->params->data['salesman_id']);
                $this->params->data['branch_id'] = $this->viewVars[API_DATA]['seller']['branch_id'];
            }
            
            $this->params->data['company_id'] = $this->layoutData['auth']['data']['company_id'];
            $this->Frontend->add('sales',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'sales'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Su Venta ha sido enviada, queda pendiente de aprobacion',
                    'error' => 'No se pudo agregar la Venta, revise errores en el formulario'
                ]
            ]);    
            
            $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        }   
        else{
            $this->Frontend->requestForm('sales');
        }        
        
        if($this->layoutData['auth']['data']['role'] !== 'vendedor'){
            $this->Frontend->salesmen();
        }
        
        $this->Frontend->products();
    }
    
    public function reject(){
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->responsableReject($id, $this->data);
        }
        
        $this->Frontend->detail('frontend_sales',$id);    
    }
    
    public function approve(){
        $id = $this->route('id');
        
        if($this->request->is('POST')){
            $this->Approval->responsable($id);
        }
        
        $this->Frontend->detail('frontend_sales',$id);    
    }
    
    public function add_massive_sale(){
        if($this->request->is('POST')){
            $this->Frontend->massive('sales',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'sales'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Revise la planilla generada y confirme la Carga de Ventas',
                    'error' => 'No se pudo procesar la Carga de Ventas',
                    'empty' => 'No se han encontrado registros CSV validos',
                ],
                'confirm' => [
                    'success' => 'Se han cargado %d Ventas',
                    'error' => 'Ha ocurrido un error en la Carga de Ventas',
                ]
            ]);    
        }
        else{
            $this->Frontend->requestMassive('sales');
        }
    }
    
    public function edit_profile(){
        if($this->request->is('POST')){
            $this->Frontend->edit('frontend_profile',[
                'id' => $this->layoutData['auth']['id'],
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'frontend',
                    'action' => 'home'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Perfil editado satisfactoriamente',
                    'error' => 'No se pudo editar el Perfil, revise errores en el formulario'
                ]
            ]);    
        }   
        else{
            $this->Frontend->requestForm('frontend_profile');
        }    
        
        $this->layoutData['payload'] = array_merge($this->viewVars[API_DATA]['payload'],$this->layoutData['payload']);
        $this->viewVars[API_DATA]['payload']['data'] = array_merge($this->viewVars[API_DATA]['auth']['data'],$this->data);
        
        $this->viewVars[API_DATA]['payload']['data']['picture'] = "";
    }

    public function catalog(){
        $this->Frontend->catalogs($this->data);
    }
    
    public function lottery(){
        $this->Frontend->lotteries();
    }
    
    public function roulette(){
        $this->Frontend->roulettes();
    }
    
    public function roulette_xhr(){
        $this->Frontend->roulettes($this->data, 'POST');
    }
    
    public function lottery_xhr(){
        $this->Frontend->lotteries($this->data, 'POST');
    }
}