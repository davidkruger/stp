<?php

App::uses('AppController', 'Controller');

class BackendController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function home(){
        $this->KlezkaffoldWeb->dashboard('backend');
    }
    
    public function profile_edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->profileEdit($this->data,[
                'messages' => [
                    'success' => Configure::read('Sitemap.backend.profile_edit.flash.success'),
                    'error' => Configure::read('Sitemap.backend.profile_edit.flash.error')
                ]
            ],[
                'controller' => 'backend',
                'action' => 'home',
                'plugin' => null
            ]);
        }
        else{
            $this->KlezkaffoldWeb->requestProfileForm();    
        }
    }
    
    public function profile_edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteProfileForm('profile'); 
    }
    
    public function profile_photo(){
        $this->KlezkaffoldWeb->profilePhoto('photo.profile',[
            'transform' => 'resize',
            'w' => 512,
            'h' => 512
        ]);    
    }
}