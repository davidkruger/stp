<?php

App::uses('BreadCrumbAppController', 'Controller');

class ChallengesController extends BreadCrumbAppController {
    public $components = [ 
        'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader',
    ];
    
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('challenges.show',$page); 
    }
        
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('challenges',$id);    
    }
        
    public function draw(){ 
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('challenges',$id);
        
        if($this->request->is('POST')){
            $this->Draw->write($id);    
        }
        
        $this->Draw->read($id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('challenges',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'challenges',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Desafio cargado con éxito',
                    'error' => 'No se pudo crear el Desafio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('challenges');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('challenges');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('challenges',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'challenges',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Desafio editado con éxito',
                    'error' => 'No se pudo editar el Desafio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('challenges',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('challenges');    
    }
    
    public function prize_img(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('prize_img.challenges',$id);    
    }
    
    public function provideModule() {
        return 'challenges';
    }

    public function provideName($data) {
        return $data['payload']['data']['title'];
    }
    
    public function dashboard(){
        $this->redirect([
            'controller' => 'challenges',
            'action' => 'show'
        ]);
    }

    public function challenge_products(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('challenges.challenge_products',$page,[
            'challenge' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['challenge'] = $id;
            }
        }  
        
        $this->render('show'); 
    }
}