<?php

App::uses('AppController', 'Controller');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class AchievementProductsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $achievement = $this->route('achievement');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('achievement_products',[
                'formdata' => $this->data,
                'achievement' => $achievement,
                'achievement_id' => $achievement,
                'id' => $id,
                'redirect' => [
                    'controller' => 'achievements',
                    'action' => 'achievement_products',
                    'id' => $achievement,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto del Logro editado con éxito',
                    'error' => 'No se pudo editar el Producto del Logro'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('achievement_products',$id,[
                'data' => [
                    'achievement' => $achievement,
                    'achievement_id' => $achievement
                ]
            ]);      
        }
        
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'dashboard'], Configure::read("Sitemap.achievements.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('achievements',$this->route('achievement'));   
        $detail = $this->KlezkaffoldReader->getData();
        $achievement = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'detail','id' => $this->route('achievement'),'slug' => KlezkaffoldComponent::resolvParamSlug($achievement)], $achievement, 'after');
        
        $this->KlezkaffoldReader->detail('achievement_products',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['name']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
    
    public function edit_xhr(){
        $id = $this->route('achievement');
        
        $this->KlezkaffoldWeb->requestAutocompleteForm('achievement_products', [
            'achievement_id' => $id
        ]);    
    }
    
    public function add(){
        $id = $this->route('achievement');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['achievement_id'] = $id;
            $this->params->data['achievement'] = $id;
            
            $this->KlezkaffoldWeb->add('achievement_products',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'achievements',
                    'action' => 'achievement_products',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto asignado al logro con éxito',
                    'error' => 'No se pudo asignar el Producto al Logro',
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('achievement_products',0,[
                'achievement' => $id,
                'achievement_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'dashboard'], Configure::read("Sitemap.achievements.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('achievements',$this->route('achievement'));   
        $detail = $this->KlezkaffoldReader->getData();
        $achievement = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'detail','id' => $this->route('achievement'),'slug' => KlezkaffoldComponent::resolvParamSlug($achievement)], $achievement, 'after');
    }
    
    public function add_xhr(){
        $id = $this->route('achievement');
        $this->KlezkaffoldWeb->requestAutocompleteForm('achievement_products', [
            'achievement_id' => $id
        ]);    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $achievement = $this->route('achievement');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('achievement_products',[
                'achievement' => $achievement,
                'achievement_id' => $achievement,
                'id' => $id,
                'redirect' => [
                    'controller' => 'achievements',
                    'action' => 'achievement_products',
                    'id' => $achievement,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Producto eliminado del Logro con éxito',
                    'error' => 'No se pudo eliminar el Producto del Logro'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('achievement_products',$id,[
                'achievement' => $achievement,
                'achievement_id' => $achievement
            ]);      
        }        
        
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'dashboard'], Configure::read("Sitemap.achievements.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('achievements',$this->route('achievement'));   
        $detail = $this->KlezkaffoldReader->getData();
        $achievement = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'achievements', 'action' => 'detail','id' => $this->route('achievement'),'slug' => KlezkaffoldComponent::resolvParamSlug($achievement)], $achievement, 'after');
        
        $this->KlezkaffoldReader->detail('achievement_products',$this->route('id'));   
        $detail = $this->KlezkaffoldReader->getData();
        $branch = $detail['payload']['data']['name']; 
        $this->pushBreadcrumb(null, $branch, 'after');
    }
}