<?php

App::uses('AuthEntityComponent','KlezApi.Controller/AuthEntity');
App::uses('KlezModel','KlezData.Model');

class BackendAuthEntityComponent extends AuthEntityComponent{
    private $Model;
    private $Schema;
    private $hooks = [];
    
    public function parseConf() {
        $this->loadModel();
        $this->parseFields();
        $this->loadSchema();
    }
    
    private function loadSchema(){
        $conf = $this->getConfig();
        $fields = $conf['entity']['fields'];
        $schema = $this->Model->provideSanitizedSchema();
        $authSchema = [];    
        
        foreach($fields as $field){        
            $authSchema[$field] = $schema[$field];
        }
        
        $this->Schema = $authSchema;
    }
    
    private function parseFields(){
        $conf = $this->getConfig();
        
        if(isset($conf['entity']['fields']) === false){           
            throw new ConfigureException("No Conf<DatabaseAuthEntity.loginField> in DatabaseAuth Config");
        }
        
        if(is_array($conf['entity']['fields']) === false){        
            $field = $conf['entity']['fields'];
            $conf['entity']['fields'] = [ $field ];
        }
        
        $fields = $conf['entity']['fields'];
        $schema = $this->Model->provideSchema();
        $class = get_class($this->Model);        
        
        foreach($fields as $field){        
            if(isset($schema[$field]) === false){           
                throw new ConfigureException("No Field<model:{$class},field:{$field}> in DatabaseAuth Model");
            }
        }
    }
    
    private function loadModel(){
        $conf = $this->getConfig();
        
        if(isset($conf['entity']['model']) === false){           
            throw new ConfigureException("No Conf<DatabaseAuthEntity.model> in Auth Config");
        }
        
        if(isset($conf['entity']['location']) === false){           
            throw new ConfigureException("No Conf<DatabaseAuthEntity.location> in Auth Config");
        }
        
        if(isset($conf['entity']['hooks'])){     
            $this->hooks = is_array($conf['entity']['hooks']) ?
                    $conf['entity']['hooks'] : [  $conf['entity']['hooks'] ];
        }
    
        $model = $conf['entity']['model'];
        $location = $conf['entity']['location'];
        
        App::uses($model, $location);
        
        if(class_exists($model) === false){
            throw new ConfigureException("No Class<$model> for DatabaseAuth");
        }
        
        $this->Model = new $model();
        
        if(($this->Model instanceof KlezModel) === false){
            throw new ConfigureException("Not a KlezModel<Class:$model> in DatabaseAuth Config");
        }        
    }
    
    public function getModel() {
        return $this->Model;
    }

    public function getSchema() {
        return $this->Schema;
    }

    public function provideData() {
        return [
            'schema' => $this->Schema
        ];
    }

    public function resolvAuthentication($data) {
        $this->Model->loadArray($data);
        $alias = $this->Model->alias;
        $cnd = [];
        
        foreach($this->Schema as $field => $meta){
            if(isset($data[$field]) === false){
                throw new BadRequestException("Data Required<field:$field> in DatabaseAuth::resolvAuthentication()");
            }
            
            $key = "{$alias}.{$field}";
            $cnd[$key] = $this->Model->readWritable($field);
        }
        
        $cnd['OR'][0]['User.role'] = 'lamoderna';
//        $cnd['OR'][1]['User.role'] = 'romis';
        $cnd['OR'][2]['User.role'] = 'samsung';
        $cnd['User.status'] = 1;
//        error_log(serialize($cnd));
        $loaded = $this->Model->loadByConditions($cnd);
        
        if($loaded){
            return $this->hooksProcessor();
        }
        
        return false;
    }
    
    private function hooksProcessor(){
        if(empty($this->hooks)){
            return true;
        }
        
        foreach ($this->hooks as $method){
            if(method_exists($this->Model,$method) === false){
                throw new ConfigureException("Hook Not Found in Model<method:$method> for DatabaseAuth");
            }
            
            if($this->Model->{$method}() === false){
                return false;
            }
        }
        
        return true;
    }

    public function provideCommitData() {
        $data = $this->Model->getData();
        return $data;
    }

    public function provideUniqueIdentificator() {
        return $this->Model->id;
    }

    public function loadData($data) {
        $array = $data['data'];
        $id = $data['id'];
        
        $this->Model->id = $id;
        $this->Model->loadArray($array);
    }
}