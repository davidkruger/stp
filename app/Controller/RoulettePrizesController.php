<?php
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('AppController', 'Controller');

class RoulettePrizesController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $roulette = $this->route('roulette');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('roulette_prizes',[
                'formdata' => $this->data,
                'roulette' => $roulette,
                'roulette_id' => $roulette,
                'id' => $id,
                'redirect' => [
                    'controller' => 'roulettes',
                    'action' => 'roulette_prizes',
                    'id' => $roulette,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Premio editado con éxito',
                    'error' => 'No se pudo editar el Premio'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('roulette_prizes',$id,[
                'data' => [
                    'roulette' => $roulette,
                    'roulette_id' => $roulette
                ]
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'dashboard'], Configure::read("Sitemap.roulettes.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('roulettes',$this->route('roulette'));   
        $detail = $this->KlezkaffoldReader->getData();
        $roulette = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'detail','id' => $this->route('roulette'),'slug' => KlezkaffoldComponent::resolvParamSlug($roulette)], $roulette, 'after');

    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('roulette_prizes');    
    }
    
    public function add(){
        $id = $this->route('roulette');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['roulette_id'] = $id;
            $this->params->data['roulette'] = $id;
            
            $this->KlezkaffoldWeb->add('roulette_prizes',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'roulettes',
                    'action' => 'roulette_prizes',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Premio creado con éxito',
                    'error' => 'No se pudo crear el Premio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('roulette_prizes',0,[
                'roulette' => $id,
                'roulette_id' => $id
            ]);    
        }
        
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'dashboard'], Configure::read("Sitemap.roulettes.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('roulettes',$this->route('roulette'));   
        $detail = $this->KlezkaffoldReader->getData();
        $roulette = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'detail','id' => $this->route('roulette'),'slug' => KlezkaffoldComponent::resolvParamSlug($roulette)], $roulette, 'after');

    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('roulette_prizes');    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $roulette = $this->route('roulette');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('roulette_prizes',[
                'roulette' => $roulette,
                'roulette_id' => $roulette,
                'id' => $id,
                'redirect' => [
                    'controller' => 'roulettes',
                    'action' => 'roulette_prizes',
                    'id' => $roulette,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Premio eliminado con éxito',
                    'error' => 'No se pudo eliminar el Premio'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('roulette_prizes',$id,[
                'roulette' => $roulette,
                'roulette_id' => $roulette
            ]);      
        }
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'dashboard'], Configure::read("Sitemap.roulettes.dashboard.h1"), 'before');
        $this->KlezkaffoldReader->detail('roulettes',$this->route('roulette'));   
        $detail = $this->KlezkaffoldReader->getData();
        $roulette = $detail['payload']['data']['name'];       
        $this->pushBreadcrumb([ 'controller' => 'roulettes', 'action' => 'detail','id' => $this->route('roulette'),'slug' => KlezkaffoldComponent::resolvParamSlug($roulette)], $roulette, 'after');
    }
}