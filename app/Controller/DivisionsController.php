<?php

App::uses('BreadCrumbAppController', 'Controller');

class DivisionsController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('divisions');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('divisions.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('divisions.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('divisions',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('divisions',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'divisions',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Division creada con éxito',
                    'error' => 'No se pudo crear la Division'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('divisions');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('divisions');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('divisions',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'divisions',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Division editada con éxito',
                    'error' => 'No se pudo editar la Division'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('divisions',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('divisions');    
    }
    
    public function subdivisions(){
        $page = $this->get('p',1);
        $id = $this->route('id');
        $slug = $this->route('slug');
        
        $this->KlezkaffoldWeb->show('divisions.subdivisions',$page,[
            'division' => $id,
            'slug' => $slug,
        ]);   
        
        $vars = &$this->viewVars;
        
        if(isset($vars[API_DATA]['payload']['links'])){
            $links = &$vars[API_DATA]['payload']['links'];
            
            foreach($links as &$link){
                $link['slug'] = $slug;
                $link['division'] = $id;
            }
        }  
        
        $this->render('show'); 
    }

    public function provideModule() {
        return 'divisions';
    }

    public function provideName($data) {
        return $data['payload']['data']['name'];
    }
}