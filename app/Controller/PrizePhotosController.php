<?php

App::uses('AppController', 'Controller');

class PrizePhotosController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $prize = $this->route('prize');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('prize_photos',[
                'formdata' => $this->data,
                'prize' => $prize,
                'prize_id' => $prize,
                'id' => $id,
                'redirect' => [
                    'controller' => 'prizes',
                    'action' => 'prize_photos',
                    'id' => $prize,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Foto del Premio eliminada con éxito',
                    'error' => 'No se pudo eliminar la Foto del Premio'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('prize_photos',$id,[
                'data' => [
                    'prize' => $prize,
                    'prize_id' => $prize
                ]
            ]);      
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('prize_photos');    
    }
    
    public function add(){
        $id = $this->route('prize');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            $this->params->data['prize_id'] = $id;
            $this->params->data['prize'] = $id;
            
            $this->KlezkaffoldWeb->add('prize_photos',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'prizes',
                    'action' => 'prize_photos',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Foto del Premio creada con éxito',
                    'error' => 'No se pudo crear la Foto del Premio'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('prize_photos',0,[
                'prize' => $id,
                'prize_id' => $id
            ]);    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('prize_photos');    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $prize = $this->route('prize');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('prize_photos',[
                'prize' => $prize,
                'prize_id' => $prize,
                'id' => $id,
                'redirect' => [
                    'controller' => 'prizes',
                    'action' => 'prize_photos',
                    'id' => $prize,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Foto del Premio eliminada con éxito',
                    'error' => 'No se pudo eliminar la Foto del Premio'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('prize_photos',$id,[
                'prize' => $prize,
                'prize_id' => $prize
            ]);      
        }
    }
    
    public function photo(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->image('photo.prize_photos',$id);    
    }
}