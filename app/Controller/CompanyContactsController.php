<?php

App::uses('AppController', 'Controller');

class CompanyContactsController extends AppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb'];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
     public function edit(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('company_contacts',[
                'formdata' => $this->data,
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'company_contacts',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Contacto de la Empresa eliminado con éxito',
                    'error' => 'No se pudo eliminar el Contacto de la Empresa'
                ]
            ]);        
        }
        else{
            $this->KlezkaffoldWeb->requestForm('company_contacts',$id,[
                'data' => [
                    'company' => $company,
                    'company_id' => $company
                ]
            ]);      
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('company_contacts');    
    }
    
    public function add(){
        $id = $this->route('company');
        $slug = $this->route('slug');
        
        if($this->request->is('POST')){
            //error_log(serialize($this->params->data));
            $this->params->data['company_id'] = $id;
            $this->params->data['company'] = $id;
            
            $this->KlezkaffoldWeb->add('company_contacts',[
                'formdata' => $this->params->data,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'company_contacts',
                    'id' => $id,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Contacto de empresa creado con éxito',
                    'error' => 'No se pudo crear el Contacto de empresa'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('company_contacts',0,[
                'company' => $id,
                'company_id' => $id
            ]);    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('company_contacts');    
    }
    
     public function delete(){
        $id = $this->route('id');
        $slug = $this->route('slug');
        $company = $this->route('company');
        
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->delete('company_contacts',[
                'company' => $company,
                'company_id' => $company,
                'id' => $id,
                'redirect' => [
                    'controller' => 'companies',
                    'action' => 'company_contacts',
                    'id' => $company,
                    'name' => $slug
                ]
            ],
            [
                'messages' => [
                    'success' => 'Contacto de la Empresa eliminado con éxito',
                    'error' => 'No se pudo eliminar el Contacto de la Empresa'
                ]
            ]);      
        }
        else{
            $this->KlezkaffoldWeb->requestDelete('company_contacts',$id,[
                'company' => $company,
                'company_id' => $company
            ]);      
        }
    }

}