<?php

App::uses('BreadCrumbAppController', 'Controller');

class DisabledGroupsController extends BreadCrumbAppController {
    public $components = [ 'KlezBackend.KlezkaffoldWeb','KlezBackend.KlezkaffoldReader' ];
    public $helpers = [
        'Klezkaffold.Dashboard', 
        'Klezkaffold.Show',
        'Klezkaffold.Detail',
        'Klezkaffold.Form',
        'Klezkaffold.Massive',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function dashboard(){
        $this->KlezkaffoldWeb->dashboard('disabled_groups');
    }
        
    public function show(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('disabled_groups.show',$page); 
    }
    
    public function show_disabled(){
        $page = $this->get('p',1);
        $this->KlezkaffoldWeb->show('disabled_groups.show_disabled',$page);    
        $this->render('show');     
    }
    
    public function detail(){
        $id = $this->route('id');
        $this->KlezkaffoldWeb->detail('disabled_groups',$id);    
    }
    
    public function add(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->add('disabled_groups',[
                'formdata' => $this->data,
                'redirect' => [
                    'controller' => 'disabled_groups',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Grupo creado con éxito',
                    'error' => 'No se pudo crear el Grupo'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('disabled_groups');    
        }
    }
    
    public function add_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('disabled_groups');    
    }
    
    public function edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->edit('disabled_groups',[
                'formdata' => $this->data,
                'id' => $this->route('id'),
                'redirect' => [
                    'controller' => 'disabled_groups',
                    'action' => 'detail'
                ]
            ],
            [
                'messages' => [
                    'success' => 'Grupo editado con éxito',
                    'error' => 'No se pudo editar el Grupo'
                ]
            ]);    
        }
        else{
            $this->KlezkaffoldWeb->requestForm('disabled_groups',$this->route('id'));    
        }
    }
    
    public function edit_xhr(){
        $this->KlezkaffoldWeb->requestAutocompleteForm('disabled_groups');    
    }

    public function provideModule() {
        return 'disabled_groups';
    }

    public function provideName($data) {
        return 'Grupos Restringidos';
    }
}