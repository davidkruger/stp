<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Product extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre del Producto',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre'
        ],
        'code' => [
            'type' => 'text',
            'required' => true,
            'searchable' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Codigo del Producto',
            'unique-message' => 'Este codigo ya ha sido registrado',
            'required-message' => 'Debe ingresar codigo'
        ],
        'points' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Puntos Asignados',
        ],
        'picture' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Foto del Producto',
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'products',
                'action' => 'picture',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'division_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'listable' => true,
            'label' => 'Division',
            'autocomplete' => [
                'class' => 'Division',
                'path' => 'Model',
                'type' => 'LEFT',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Division.id, Division.name',
                    'order' => 'Division.name ASC',
                    'conditions' => [
                        'Division.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Division',
            'autocomplete-message' => 'Debe especificar Division',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Division'
        ],
        'subdivision_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Subdivision',
            'autocomplete_dep' => 'division_id',
            'autocomplete_dep_condition' => 'Subdivision.division_id',
            'autocomplete' => [
                'class' => 'Subdivision',
                'path' => 'Model',
                'type' => 'LEFT',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Subdivision.id, Subdivision.name',
                    'order' => 'Subdivision.name ASC',
                    'conditions' => [
                        'Subdivision.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Subdivision',
            'autocomplete-message' => 'Debe especificar Subdivision',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Subdivision'
        ],
        'segment_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Segmento 1',
            'autocomplete_dep' => 'subdivision_id',
            'autocomplete_dep_condition' => 'Segment.subdivision_id',
            'autocomplete' => [
                'class' => 'Segment',
                'path' => 'Model',
                'type' => 'LEFT',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Segment.id, Segment.name',
                    'order' => 'Segment.name ASC',
                    'conditions' => [
                        'Segment.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Segmento',
            'autocomplete-message' => 'Debe especificar Segmento',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Segmento'
        ],
        'segment_id2' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'listable' => true,
            'label' => 'Segmento 2',
            'autocomplete_dep' => 'subdivision_id',
            'autocomplete_dep_condition' => 'Segment.subdivision_id',
            'autocomplete' => [
                'class' => 'Segment',
                'path' => 'Model',
                'alias' => 'Segment2',
                'type' => 'LEFT',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Segment2.id, Segment2.name',
                    'order' => 'Segment2.name ASC',
                    'conditions' => [
                        'Segment2.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Segmento',
            'autocomplete-message' => 'Debe especificar Segmento',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Segmento'
        ],
        'segment_id3' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => false,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Segmento 3',
            'autocomplete_dep' => 'subdivision_id',
            'autocomplete_dep_condition' => 'Segment.subdivision_id',
            'autocomplete' => [
                'class' => 'Segment',
                'path' => 'Model',
                'type' => 'LEFT',
                'alias' => 'Segment3',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Segment3.id, Segment3.name',
                    'order' => 'Segment3.name ASC',
                    'conditions' => [
                        'Segment3.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Segmento',
            'autocomplete-message' => 'Debe especificar Segmento',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Segmento'
        ],
        'capacity' => [
            'type' => 'text',
            'searchable' => true,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'label' => 'Capacidad',
            'null' => 'NO APLICA',
            'blank' => 'NO APLICA'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Estado del Producto',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    private $serachSchema = [
        'name' => [
            'type' => 'text',
            'weight' => 12,
            'searchable' => true,
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Terminos de Busqueda del Producto',
        ],
    ];
    
    public function provideSchema() {
        switch ($this->getShape()){
            case 'search': return $this->serachSchema;
        }
        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function fetchFrontend(){
        return $this->find('all',[
            'conditions' => [
                'Product.status' => true
            ],
            'fields' => 'Product.id, Product.name, Product.code',
            'order' => 'Product.name ASC'
        ]);
    }
    
    private function sanitizeSearchTerm($payload){
        return trim(preg_replace("/[^A-Za-z0-9 ]/", '', isset($payload['name']) ? $payload['name'] : ''));
    }
    
    public function fetchCatalogs($payload, $myCompany){
        $q = $this->sanitizeSearchTerm($payload);
        
        $cnd = [];
        $cnd['Product.status'] = 1;
        
        $joins = [];
        $joins[] = 'LEFT JOIN product_points AS ProductPoint ON ProductPoint.product_id=Product.id AND ProductPoint.status=1 AND ProductPoint.company_id = ' . $myCompany;
        $joins[] = 'LEFT JOIN divisions AS Division ON Division.id=Product.division_id AND Division.status=1';
        $joins[] = 'LEFT JOIN product_catalogs AS ProductCatalog ON ProductCatalog.product_id=Product.id AND ProductCatalog.status=1';
        $joins[] = 'LEFT JOIN product_specs AS ProductSpec ON ProductSpec.product_id=Product.id AND ProductSpec.status=1';
        $joins[] = 'LEFT JOIN specs AS Spec ON Spec.id=ProductSpec.spec_id AND Spec.status=1';

        if($q !== ''){
            $cnd['OR'][] = "Product.name LIKE '%{$q}%'";
            $cnd['OR'][] = "ProductCatalog.title LIKE '%{$q}%'";
            $cnd['OR'][] = "ProductCatalog.description LIKE '%{$q}%'";
            $cnd['OR'][] = "ProductSpec.value LIKE '%{$q}%'";
            $cnd['OR'][] = "Spec.name LIKE '%{$q}%'";
            $cnd['OR'][] = "Division.name LIKE '%{$q}%'";
        }
        
        $this->virtualFields['division'] = 'Division.name';
        $this->virtualFields['points'] = 'COALESCE(ProductPoint.points,Product.points)';
        $products = $this->find('all', [
            'conditions' => $cnd,
            'joins' => $joins,
            //'fields' => 'Product.id, Product.code, Product.name, Product.picture, Product.division_id',
            'group' => 'Product.id',
            'order' => 'COALESCE(Division.name,"zzz") ASC',
//            'order' => 'COALESCE(ProductPoint.points,Product.points) DESC'
        ],false);
        
        $response = [
            'q' => $q,
            'divisions' => []
        ];
        
        App::uses('ProductSpec', 'Model');
        $Spec = new ProductSpec();
        
        App::uses('ProductCatalog', 'Model');
        $Catalog = new ProductCatalog();
        
        foreach($products as $product){
            $p = $product['id'];
            $d = '#' . $product['division_id'];
            
            if(!isset($response['divisions'][$d])){
                $response['divisions'][$d] = [
                    'products' => [],
                    'division' => $product['division']
                ];
            }
            
            $product['catalogs'] = $Catalog->fetchForProduct($p);
            $product['specs'] = $Spec->fetchForProduct($p);
            $response['divisions'][$d]['products'][] = $product;
        }
        
        return $response;
    }
    
    public function complexSearch($payload){
        $q = $this->sanitizeSearchTerm($payload);
        
        $response = [
            'q' => $q,
            'products' => []
        ];
        
        if($q === ''){
            return $response;
        }
                    
        foreach($this->complexSpecSearch($q) as $spec){
            $p = $spec['product_id'];
            $key = '#' . $p;
            
            if(!isset($response['products'][$key])){
                $response['products'][$key] = $this->complexProductMalloc($spec);
            }
            
            $response['products'][$key]['specs'][] = $spec;
        }
        
        foreach($this->complexCatalogSearch($q) as $catalog){
            $p = $catalog['product_id'];
            $key = '#' . $p;
            
            if(!isset($response['products'][$key])){
                $response['products'][$key] = $this->complexProductMalloc($catalog);
            }
            
            $response['products'][$key]['catalogs'][] = $catalog;
        }
        
        return $response;
    }
    
    private function complexProductMalloc($obj){
        return [
            'product' => [
                'id' => $obj['product_id'],
                'name' => $obj['product'],
                'code' => $obj['code'],
                'picture' => $obj['picture'],
                'division' => $obj['division'],
            ],
            'specs' => [],
            'catalogs' => []
        ];
    }
    
    private function complexSpecSearch($q){
        App::uses('ProductSpec', 'Model');
        $Spec = new ProductSpec();
        
        $Spec->virtualFields['product'] = 'Product.name';
        $Spec->virtualFields['code'] = 'Product.code';
        $Spec->virtualFields['picture'] = 'Product.picture';
        $Spec->virtualFields['division'] = 'Division.name';
        $Spec->virtualFields['spec'] = 'Spec.name';
        return $Spec->find('all',[
            'joins' => [
                'INNER JOIN specs AS Spec ON Spec.id = ProductSpec.spec_id',
                'INNER JOIN products AS Product ON Product.id = ProductSpec.product_id',
                'LEFT JOIN divisions AS Division ON Division.id = Product.division_id',
            ],
            'order' => 'ProductSpec.spec_order ASC',
            'conditions' => [
                'OR' => [
                    "ProductSpec.value LIKE '%{$q}%'",
                    "Spec.name LIKE '%{$q}%'"
                ],
                'ProductSpec.status' => 1,
                'Spec.status' => 1,
                'Product.status' => 1,
            ]
        ],false);
    }

    public function complexCatalogSearch($q) {
        App::uses('ProductCatalog', 'Model');
        $Catalogs = new ProductCatalog();
        
        $Catalogs->virtualFields['product'] = 'Product.name';
        $Catalogs->virtualFields['code'] = 'Product.code';
        $Catalogs->virtualFields['picture'] = 'Product.picture';
        $Catalogs->virtualFields['division'] = 'Division.name';
        return $Catalogs->find('all',[
            'joins' => [
                'INNER JOIN products AS Product ON Product.id = ProductCatalog.product_id',
                'LEFT JOIN divisions AS Division ON Division.id = Product.division_id',
            ],
            'order' => 'ProductCatalog.catalog_order ASC',
            'conditions' => [
                'OR' => [
                    "ProductCatalog.title LIKE '%{$q}%'",
                    "ProductCatalog.description LIKE '%{$q}%'"
                ],
                'ProductCatalog.status' => 1,
                'Product.status' => 1,
            ]
        ],false);
    }
}