<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Stock extends KlezBackendAppModel{
    public $useTable = 'stock';
    
    private $schema = [
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'FullProduct',
                'path' => 'Model',
                'label' => 'product',
                'identifier' => 'id',
                'full' => true,
                'query' => [
                    'fields' => 'FullProduct.id, FullProduct.product',
                    'order' => 'FullProduct.product ASC',
                    'conditions' => [
                        'FullProduct.status' => 1,
                    ]
                ]   
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-cart',
            'placeholder' => 'Buscar Producto'
        ],
        'provider_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'listable' => true,
            'label' => 'Proveedor',
            'autocomplete' => [
                'class' => 'Provider',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Provider.id, Provider.name',
                    'order' => 'Provider.name ASC',
                    'conditions' => [
                        'Provider.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Proveedor',
            'autocomplete-message' => 'Debe especificar Proveedor',
            'icon' => 'truck',
            'placeholder' => 'Buscar Proveedor'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Cantidad',
            'required-message' => 'Debe ingresar Cantidad',
            'int-message' => 'Debe ingresar una Cantidad valida',
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'companyless':
                return $this->companyless();
            default:
                return $this->schema;
        }
    }

    private function companyless(){
        $schema = $this->schema;
        $schema['company_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'STOCK-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function companyOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['company'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.company_id"] = $id;
        
        return $query;
    } 
    
    public function filterCompanyUsers($query,$authdata,$payload){
        $id = $payload['extra']['company_id'];
        
        switch($payload['data']['field']){
            case 'responsible_id':
                $query['conditions']['FullUser.company_id'] = $id;
                $query['conditions']['FullUser.role'] = 'responsable';
                break;
        }
        
        return $query;
    }
    
    public function resolvCurrent($company,$product){
        $this->virtualFields = array(
            'sum' => 'SUM(Stock.quantity)'
        );
        
        $raw = parent::rawFind('first', [
            'conditions' => [
                'Stock.company_id' => $company,
                'Stock.product_id' => $product,
            ]
        ]);
        
        return (int) @$raw['Stock']['sum'];
    }
}