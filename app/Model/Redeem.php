<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('User', 'Model');
App::uses('Prize', 'Model');
App::uses('Salesman', 'Model');

class Redeem extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Solicitud',
            'writable' => false,
            'readable' => true,
            'required' => true,
            'orderable' => true,
        ],
        'user_id' => [
            'searchable' => true,
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Vendedor',
            'autocomplete' => [
                'class' => 'Seller',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Seller.id, Seller.full_name',
                    'order' => 'Seller.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Vendedor',
            'autocomplete-message' => 'Debe especificar Vendedor',
            'icon' => 'account',
            'placeholder' => 'Buscar Vendedor'
        ],
        'group_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Grupo',
            'autocomplete' => [
                'class' => 'Group',
                'path' => 'Model',
                'alias' => 'Wroup',
                'label' => 'name',
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Wroup.id, Wroup.name',
                    'order' => 'Wroup.name ASC',
                    'conditions' => [
                        'Wroup.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Premio',
            'autocomplete-message' => 'Debe especificar Premio',
            'icon' => 'account',
            'placeholder' => 'Buscar Premio'
        ],
        'prize_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Premio a Canjear',
            'autocomplete' => [
                'class' => 'Prize',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Prize.id, Prize.name',
                    'order' => 'Prize.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Premio',
            'autocomplete-message' => 'Debe especificar Premio',
            'icon' => 'account',
            'placeholder' => 'Buscar Premio'
        ],
        'points' => [
            'type' => 'int',
            'label' => 'Puntos Utilizados',
            'writable' => false,
            'readable' => true,
            'required' => true,
        ],
        'amount' => [
            'type' => 'int',
            'label' => 'Monto Canjeado (Gs.)',
            'writable' => false,
            'readable' => true,
            'required' => false,
        ],
        'telephone_zimple' => [
            'type' => 'text',
            'label' => 'Telefono Zimple',
            'writable' => false,
            'readable' => true,
            'required' => false,
        ],
        
        'restriction' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Restricción',
        ],
    ];
    
    private $moderationSchema = [
        'status' => [
            'type' => 'options',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'label' => 'Moderar Estado del Canje',
            'options' => [
                'aprobado' => 'Aprobado',
                'rechazado' => 'Rechazado',
            ],
        ],
        'observations' => [
            'weight' => 8,
            'type' => 'text',
            'subtype' => 'textarea',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'placeholder' => 'Comentarios acerca de esta Moderacion',
            'label' => 'Observaciones',
        ],
    ];
    
    public function provideSchema() {
        switch ($this->getShape()){
            case 'moderation':
                return $this->moderationSchema();
            case 'show':
                return $this->showSchema();
        }
        
        return $this->schema;
    }
    
    private function moderationSchema(){
        return $this->moderationSchema;
    }
    
    private function showSchema(){
        $this->virtualFields['company'] = 'Company.name';
        $this->virtualFields['branch'] = 'Branch.name';
        $schema = $this->schema;
        $schema['company'] = [
            'type' => 'text',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'required' => false,
            'label' => 'Empresa',
        ];
        $schema['branch'] = [
            'type' => 'text',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'required' => false,
            'label' => 'Sucursal',
        ];  
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'REDEEM-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    private $Moderation;
    
    public $virtualFields = [
        'observations' => '""'
    ];
    
    public function pushModeration($data,$authdata){
        App::uses('RedeemModeration','Model');
        $this->Moderation = new RedeemModeration();
        $this->Moderation->id = null;
        
        $mod = [
            'RedeemModeration' => [
                'redeem_id' => $this->id,
                'user_id' => $authdata['id'],
                'moderation' => $data['status'],
                'observations' => $data['observations'],
            ]
        ];
        
        if($this->Moderation->save($mod)){
            return true;
        }
        
        return false;
    }
    
    private $Salesman;
    private $Prize;
    
    public function notifySalesman($data,$authdata){
        if($data['status'] !== 'aprobado'){
            return true;
        }
        
        $this->Redeem = new Redeem();
        $this->Prize = new Prize();
        $this->Salesman = new Salesman();
        
        if(!$this->Redeem->loadById($this->id)){
            return true;
        }
        
        if(!$this->Salesman->loadById($this->Redeem->readField('user_id'))){
            return true;
        }
        
        if(!$this->Prize->loadById($this->Redeem->readField('prize_id'))){
            return true;
        }
        
        try{
            App::uses('CakeEmail', 'Network/Email');                            
            $email = new CakeEmail('default');

            if(!Configure::read('Mail.tamper')){
                $email->addTo($this->Salesman->readField('email'));
            }
            else{
                $email->addTo(Configure::read('Mail.tamper'));
            }

            $email->viewVars([
                'prize' => $this->Prize->getData(),
                'salesman' => $this->Salesman->getData(),
            ]);
        
            $email->template('redeem');
            $email->emailFormat('html');  
            $email->from(array(Configure::read('Mail.rcpt') => Configure::read('Mail.from')));
            $email->subject('STP Canje Aprobado');
            $email->send();
        }
        catch(Exception $e){
            error_log("Exception @ Redeem::notifySalesman({$this->Salesman->id},{$this->Salesman->readField('email')}) - {$e->getMessage()}");
        }
        
        return true;
    }
    
    public function resolvPending($id){
        $cnd = [];
        $cnd['Redeem.id'] = $id;
        $cnd['Redeem.status'] = 'pendiente';
        
        return $this->find('all', [
            'conditions' => $cnd
        ]);
    }
    
    public function pendingFilter($query,$authdata,$payload){
        $query['conditions']['Redeem.status'] = 'pendiente';
        return $query;
    }
    
    public function notPendingFilter($query,$authdata,$payload){
        $query['conditions']['Redeem.status != '] = 'pendiente';
        return $query;
    }
    
    private $User;
    
    public function pushRedeem($prize,$auth){

        $this->User = new User();
        $this->begin();
        
        
        try{
            $data = [];
            $data['Redeem']['user_id'] = $auth['id'];
            $data['Redeem']['prize_id'] = $prize['id'];
            $data['Redeem']['group_id'] = $prize['group_id'];

            if($prize['restriction'] == ''){
                $data['Redeem']['restriction'] = null;
            }else{
                $data['Redeem']['restriction'] = $prize['restriction'];
            }

            if($auth['data']['telephone_zimple'] == ''){
                $data['Redeem']['telephone_zimple'] = null;
            }else{
                $data['Redeem']['telephone_zimple'] = $auth['data']['telephone_zimple'];
            }
            
            
            if($prize['amount'] > 0){

                $data['Redeem']['points'] = (int) $prize['total'];
                $p = ((int) $auth['data']['points']) - $prize['total'];

            }else{
                
                $data['Redeem']['points'] = (int) $prize['points'];
                $p = ((int) $auth['data']['points']) - $prize['points'];
            }
            
            $data['Redeem']['amount'] = (int) $prize['amount'];

            if($p >= 0){
                $result = $this->save($data,false) && $this->User->setPoints($auth['id'],$p);
            }
            else{
                $result = false;
            }
        }
        catch (Exception $e){
            error_log("Exception @ Redeem::pushRedeem() - {$e->getMessage()}");
            $result = false;
        }
        
        if($result){
            $this->commit();
        }
        else{
            $this->rollback();
        }
        
        return $result;
    }
    
    public function fetchFrontend($id){
        $cnd = [];
        $cnd['Redeem.user_id'] = $id;
//        $cnd['Redeem.status'] = 'pendiente';
        
        return $this->rawFind('all',[
            'conditions' => $cnd,
            'joins' => [
                'INNER JOIN prizes AS Prize ON Prize.id=Redeem.prize_id',
                'LEFT JOIN redeem_moderations AS RedeemModeration ON RedeemModeration.redeem_id=Redeem.id'
            ],
            'fields' => 'Redeem.*, Prize.*, RedeemModeration.observations AS Redeem__obs',
            'order' => 'Redeem.created DESC'
        ]);
    }
    
    private $Seller;
    private $Redeem;
    
    public function returnUserPoints($data,$authdata){
        if($data['status'] !== 'rechazado'){
            return true;
        }
        
        $this->Redeem = new Redeem();
        
        if($this->Redeem->loadById($this->getData()['id']) === false){
            return false;
        }
        
        App::uses('Seller','Model');
        $this->Seller = new Seller();
        $redeem = $this->Redeem->getData();
        return $this->Seller->semUpdatePoints($redeem['user_id'], $redeem['points']);
    }
    
    public function summary($payload){
        $cnd = [];
        $cnd['Redeem.status'] = [ 'aprobado', 'pendiente' ];
        $cnd['Redeem.created >= '] = $payload['a'];
        $cnd['Redeem.created <= '] = $payload['b'];
        
        if(isset($payload['company_id']) && $payload['company_id']){
            $cnd['Salesman.company_id'] = $payload['company_id'];
        }
        
        $joins = [];
        $joins[] = 'LEFT JOIN salesmen AS Salesman ON Salesman.id = Redeem.user_id';
        
        $group = [];
        $group[] = 'Salesman.id';
        
        $fields = [];
        $fields[] = 'Redeem.user_id';
        $fields[] = 'Salesman.company_id AS Redeem__company_id';
        $fields[] = 'SUM(COALESCE(Redeem.points,0)) AS Redeem__redeemed';
        
        $order = [];
        $order[] = 'Redeem__company_id ASC';
        $order[] = 'Redeem.user_id ASC';
        $order[] = 'Redeem__redeemed DESC';
        
        $query = [
            'fields' => $fields,
            'conditions' => $cnd,
            'joins' => $joins,
            'group' => $group,
            'order' => $order
        ];
        
        return $this->find('all', $query, false);
    }
    
    public function customFiltersPrequery($query,$authdata,$payload){
        if(isset($payload['filter'])){
            $payload = $payload['filter'];
        }
        
        $company = (int) $payload['company_id'];
        $branch = (int) $payload['branch_id'];
        
        if($company){
            $query['conditions']["Seller.company_id"] = $company;
        }
        
        if($branch){
            $query['conditions'][0]['OR']["Seller.branch_id"] = $branch;
            $query['conditions'][0]['OR']["Seller.branch_id2"] = $branch;
            $query['conditions'][0]['OR']["Seller.branch_id3"] = $branch;
       }
        
        if(isset($payload['a'])){
            if($payload['a']){
                $aa = explode('/', $payload['a']);
                $a = (int) @$aa[2] . '-' . @$aa[1] . '-' . @$aa[0];
                $query['conditions']["DATE(Redeem.created) >= "] = $a;
            }
            
            if($payload['b']){
                $bb = explode('/', $payload['b']);
                $b = (int) @$bb[2] . '-' . @$bb[1] . '-' . @$bb[0];
                $query['conditions']["DATE(Redeem.created) <= "] = $b;
            }
        }
        
        return $query;
    }
}