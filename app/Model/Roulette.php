<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('RouletteIntent', 'Model');
App::uses('RouletteCompany', 'Model');
App::uses('RoulettePrize', 'Model');

class Roulette extends KlezBackendAppModel{
    const ROULETTE_ZERO_COMPANIES = 'No ha asignado ninguna Empresa';

    private $schema = [
        'active' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Activa?',
            'boolean' => [
                0 => 'No',
                1 => 'Si'
            ],
            'default' => 0,
        ],
        'updated' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Versión',
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Nombre Ruleta',
        ],
        'starts_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Inicio',
        ],
        'ends_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Final',
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Puntos',
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Participantes',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'RouletteCompany',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'roulette_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas asignadas',
            'placeholder' => 'Empresas Participantes'
        ],
    ];
    
    private $xlsxSchema = [
        'roulette_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Ruleta',
            'autocomplete' => [
                'class' => 'Roulette',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Roulette.id, Roulette.name',
                    'order' => 'Roulette.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Ruleta',
            'autocomplete-message' => 'Debe especificar Ruleta',
            'icon' => 'gears',
            'placeholder' => 'Buscar Ruleta'
        ],
    ];
    
    public $validate = [
        'goal' => [
            'range' => [
                'rule' => [ 'pointsRangeValidator' ],
                'message' => 'Debe ser mayor a 0'
            ]
        ]
    ];
    
    public function pointsRangeValidator(){
        $goal = (int) $this->readField('points');
        
        return $goal > 0;
    }
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxFilter':
                return $this->xlsxSchema;
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ROULETTE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function habtmValidator($data,$field){
        $array = $data[$field];
        
        foreach($array as $raw){
            if(strlen($raw) > 0){
                return true;
            }
        }
        
        return $this->getHabtmMessage($field);
    }
    
    private function getHabtmMessage($field){
        switch($field){
            case 'companies':
                return self::ROULETTE_ZERO_COMPANIES;
        }
        
        return false;
    }
    
    /**
     * Obtiene las competencias vigentes para el frontend
     */
    
    private $RoulettePrize;
    
    public function fetchFrontend($auth, $id = null){
        $date = date('Y-m-d H:i:s');
        $conditions = [
            'RouletteCompany.company_id' => $auth['data']['company_id'],
            '(Roulette.starts_at) <=' => $date,
            '(Roulette.ends_at) >='  => $date,
            'Roulette.status' => 1,
            'Roulette.active' => 1
        ];
        
        if ($id) {
            $conditions['Roulette.id'] = $id;
        }
        
        $roulettes = $this->find('all',[
            'conditions' => $conditions,
            'joins' => [
                'INNER JOIN roulette_companies AS RouletteCompany ON RouletteCompany.roulette_id = Roulette.id'
            ],
            'order' => 'Roulette.created DESC'
        ], false);
        
        if(!$roulettes){
            return [];
        }
        
        $this->RoulettePrize = new RoulettePrize();
        
        foreach($roulettes as $i => &$roulette){
            $roulette['prizes'] = $this->RoulettePrize->find('all', [
                'conditions' => [
                    'RoulettePrize.roulette_id' => $roulette['id'],
                ]
            ], false);
            
            if(empty($roulette['prizes'])) {
                unset($roulettes[$i]);
            }
        }
        
        if ($id) {
            return array_shift($roulettes);
        }
        
        return $roulettes;
    }
    
    public function activeRoulette($data, $authdata) {
        if($data['active']) {
            $sql = 'UPDATE roulettes SET active = 0';
            $this->query($sql);
        }
        
        return true;
    }
}