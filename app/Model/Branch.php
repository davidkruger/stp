<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Branch extends KlezBackendAppModel{
    private $schema = [
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'searchable' => true,
            'compound_unique'=> [ 'company_id' ],
            'readable' => true,
            'label' => 'Nombre Sucursal',
            'compound_unique-message' => 'Nombre de Sucursal existente',
            'required-message' => 'Debe ingresar nombre'
        ],
        'city_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'listable' => true,
            'label' => 'Ciudad',
            'autocomplete' => [
                'class' => 'City',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'City.id, City.name',
                    'order' => 'City.name ASC',
                    'conditions' => [
                        'City.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Ciudad',
            'autocomplete-message' => 'Debe especificar Ciudad',
            'icon' => 'map-marker',
            'placeholder' => 'Buscar Ciudad'
        ],
        'telephone' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Telefono',
            'required-message' => 'Debe ingresar telefono'
        ],
        'email' => [
            'type' => 'text',
            'subtype' => 'email',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Email',
            'required-message' => 'Debe ingresar direccion de email'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado de la Sucursal',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'companyless':
                return $this->companyless();
            default:
                return $this->schema;
        }
    }

    private function companyless(){
        $schema = $this->schema;
        $schema['company_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ID-SUCURSAL',
            'listable' => true,
            'readable' => true,
            'hidden' => false
        ];
    }

    public function companyOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['company'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.company_id"] = $id;
        
        return $query;
    } 
    
    public function filterCompanyUsers($query,$authdata,$payload){
        $id = $payload['extra']['company_id'];
        
        switch($payload['data']['field']){
            case 'responsible_id':
                $query['conditions']['FullUser.company_id'] = $id;
                $query['conditions']['FullUser.role'] = 'responsable';
                break;
        }
        
        return $query;
    }
    
    public function fetchFrontend($company_id){
        $cnd = [];
        $cnd['Branch.status'] = true;
        $cnd['Branch.company_id'] = $company_id;
        
        return $this->find('all',[
            'conditions' => $cnd,
            'order' => 'Branch.name ASC'
        ]);
    }
    
}