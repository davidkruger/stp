<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Company', 'Model');

class LotteryCompany extends KlezBackendAppModel{
    private $schema = [
        'lottery_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Loteria',
            'autocomplete' => [
                'class' => 'Lottery',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Lottery.id, Lottery.name',
                    'order' => 'Lottery.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Loteria',
            'autocomplete-message' => 'Debe especificar Loteria',
            'icon' => 'gears',
            'placeholder' => 'Buscar Loteria'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'LOTTERY-COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}