<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Subdivision extends KlezBackendAppModel{
    private $schema = [
        'division_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Division',
            'autocomplete' => [
                'class' => 'Division',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Division.id, Division.name',
                    'order' => 'Division.name ASC',
                    'conditions' => [
                        'Division.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Division',
            'autocomplete-message' => 'Debe especificar Division',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Division'
        ],
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'compound_unique'=> [ 'division_id' ],
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre de la Subdivision',
            'compound_unique-message' => 'Nombre de subdivision existente',
            'required-message' => 'Debe ingresar nombre',
            'searchable' => true
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado de la Subdivision',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'divisionless':
                return $this->divisionless();
            default:
                return $this->schema;
        }
    }

    private function divisionless(){
        $schema = $this->schema;
        $schema['division_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ID-SUBDIVISION',
            'listable' => true,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function divOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['division'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.division_id"] = $id;
        
        return $query;
    } 
}