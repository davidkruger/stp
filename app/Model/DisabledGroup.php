<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class DisabledGroup extends KlezBackendAppModel{
    private $schema = [

        'group_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Grupo',
            'autocomplete' => [
                'class' => 'Group',
                'path' => 'Model',
                'alias' => 'Grou',
                'label' => 'name',
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Grou.id, Grou.name',
                    'order' => 'Grou.name ASC',
                    'conditions' => [
                        'Grou.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Grupo',
            'autocomplete-message' => 'Debe especificar Grupo',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Grupo'
        ],

        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'alias' => 'Company',
                'label' => 'name',
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado de la Division',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
      
    ];
    
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'DISABLED-GROUP-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}