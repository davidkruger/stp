<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('AchievementLog', 'Model');
App::uses('AchievementUser', 'Model');
App::uses('AchievementProduct', 'Model');

class Achievement extends KlezBackendAppModel{
    private $schema = [
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Logro',
        ],
        'class_group' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Clase',
        ],
        'target' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Meta',
        ],
        'products' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Productos Target',
            'habtm' => [
                'foreign' => [
                    'class' => 'FullProduct',
                    'path' => 'Model',
                    'label' => 'product',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'AchievementProduct',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'achievement_id',
                        'foreign' => 'product_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Productos asignados',
            'placeholder' => 'Productos asignados al Logro'
        ],
        'valid_from' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Valido desde',
        ],
        'valid_to' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Valido hasta',
        ],
        'icon_0' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Icono 0',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'achievements',
                'action' => 'icon_0',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'icon_1' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Icono 1',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'achievements',
                'action' => 'icon_1',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'prio' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'label' => 'Orden',
            'default' => 1,
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Excluidas',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'AchievementCompany',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'achievement_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas excluidas',
            'placeholder' => 'Empresas Excluidas'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado del Logro',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo',
            ],
            'default' => 1,
        ],
    ];
    
    public $validate = [
        'target' => [
            'range' => [
                'rule' => [ 'targetRangeValidator' ],
                'message' => 'Debe ser mayor a 0'
            ]
        ]
    ];
    
    public function targetRangeValidator(){
        $target = (int) $this->readField('target');
        
        return $target > 0;
    }

    public function xlsxFilterSchema(){
       
        set_time_limit(0);
        ini_set('memory_limit', -1);
 
        return [
            'achievement_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Logro',
                'autocomplete' => [
                    'class' => 'Achievement',
                    'alias' => 'Ach',
                    'path' => 'Model',
                    'label' => 'name',
                    'identifier' => 'id',
                    'query' => [
                        'fields' => 'Ach.id, Ach.name',
                        'order' => 'Ach.name ASC',
                        'conditions' => [
                            
                        ],
                    ]
                ],          
                'required-message' => 'Debe especificar Logro',
                'autocomplete-message' => 'Debe especificar Logro',
                'icon' => 'gears',
                'placeholder' => 'Buscar Logro'
            ],
            'company_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'writable' => true,
                'readable' => true,
                'required' => false,
                'weight' => 4,
                'label' => 'Empresa',
                'autocomplete' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'identifier' => 'id',
                    'type' => 'LEFT',
                    'query' => [
                        'fields' => 'Company.id, Company.name',
                        'order' => 'Company.name ASC',
                        'conditions' => [
                            'Company.status' => 1,
                        ]
                    ]
                ],
                'required-message' => 'Debe especificar Empresa',
                'autocomplete-message' => 'Debe especificar Empresa',
                'icon' => 'shopping-bag',
                'placeholder' => 'Buscar Empresa'
            ],
            'user_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'writable' => true,
                'readable' => true,
                'required' => false,
                'label' => 'Vendedor',
                'autocomplete_dep' => 'company_id',
                'autocomplete_dep_condition' => 'Salesman.company_id',
                'autocomplete' => [
                    'class' => 'Salesman',
                    'path' => 'Model',
                    'label' => 'full_name',
                    'identifier' => 'id',
                    'query' => [
                        'fields' => 'Salesman.id, Salesman.full_name',
                        'order' => 'Salesman.full_name ASC',
                        'conditions' => [
                            
                        ],
                    ]
                ],          
                'required-message' => 'Debe especificar Usuario',
                'autocomplete-message' => 'Debe especificar Usuario',
                'icon' => 'gears',
                'placeholder' => 'Buscar Usuario'
            ],
            'valid_from' => [
                'type' => 'date',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Fecha desde',
            ],
            'valid_to' => [
                'type' => 'date',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Fecha hasta',
            ],
            'complete' => [
                'type' => 'boolean',
                'writable' => true,
                'readable' => false,
                'required' => false,
	                'label' => 'Solo completados?',
            ],
        ];
    }

    public function xlsxSchema(){
        $schema = $this->xlsxFilterSchema();
        $schema['achievement_id']['readable'] = false;
        $schema['company_id']['readable'] = false;
        $schema['user_id']['readable'] = false;

        $schema['valid_from'] = $this->schema['valid_from'];
        $schema['valid_to'] = $this->schema['valid_to'];
        $schema['name'] = $this->schema['name'];
        $schema['class_group'] = $this->schema['class_group'];
        $schema['target'] = $this->schema['target'];
        $schema['salesman'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Vendedor',
        ];
        $schema['document'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Documento',
        ];
        $schema['company'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Empresa',
        ];
        $schema['branch'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Sucursal',
        ];
        $schema['p'] = [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Progreso',
        ];

        $this->virtualFields['salesman'] = 'Salesman.full_name';
        $this->virtualFields['document'] = 'Salesman.document';
        $this->virtualFields['company'] = 'Company.name';
        $this->virtualFields['branch'] = 'Branch.name';
        $this->virtualFields['p'] = 'AchievementUser.points';


        return $schema;
    }

    public function xlsxFilter($query,$authdata,$payload){
        $query['joins'] =  [
            "INNER JOIN achievement_users AS AchievementUser ON AchievementUser.achievement_id=Achievement.id",
            "INNER JOIN salesmen AS Salesman ON Salesman.id=AchievementUser.user_id",
            "INNER JOIN companies AS Company ON Company.id=Salesman.company_id",
            "INNER JOIN branches AS Branch ON Branch.id=Salesman.branch_id",
        ];
        

        $cnd = [];

        if ($company = (int)@$payload['filter']['company_id']) {
            $cnd['Company.id'] = $company;
        }

        if ($user = (int)@$payload['filter']['user_id']) {
            $cnd['AchievementUser.user_id'] = $user;
        }

        if ($achievement = (int)@$payload['filter']['achievement_id']) {
            $cnd['Achievement.id'] = $achievement;
        }

        if (@$payload['filter']['complete']) {
            $cnd[] = 'AchievementUser.points >= Achievement.target';
        }
        if ($from = @$payload['filter']['valid_from']) {
            $chunks = split('/', $from);
           	$cnd['Achievement.valid_from >='] = $chunks[2] . '-' . $chunks[1] . '-' . $chunks[0] . ' 00:00:00';
		}
        if ($to = @$payload['filter']['valid_to']) {
            $chunks = split('/', $to);
         	$cnd['Achievement.valid_to <='] = $chunks[2] . '-' . $chunks[1] . '-' . $chunks[0] . ' 23:59:59';
		}

        $query['conditions'] = $cnd;

        $query['order'] = [
            'Achievement.name ASC',
            'Salesman.full_name ASC'
        ];

        $this->log('-------- xlsxFilter ----------');
        $this->log(json_encode($query));

        return $query;
    }
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxFilter':
                return $this->xlsxFilterSchema();
            case 'xlsx':
                return $this->xlsxSchema();
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ACHIEVEMENT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    /**
     * Determina los Achievement activos segun detalle de ventas.
     * 
     * Si hay coincidencia de productos se asignan los valores correspondientes.
     */
    
    private $AchievementLog = null;
    private $AchievementProduct = null;
    private $AchievementUser = null;
    const SEM = 'ACH::updateScore()';
    
    public function allocAchievement($sale){
        if(is_null($this->AchievementLog)){
            $this->AchievementLog = new AchievementLog();
        }
        
        if(is_null($this->AchievementProduct)){
            $this->AchievementProduct = new AchievementProduct();
        }
        
        if(is_null($this->AchievementUser)){
            $this->AchievementUser = new AchievementUser();
        }

        $now         = $sale['sale_date'];
        $sale_id     = $sale['id'];
        $salesman_id = $sale['salesman_id'];
        $quantity    = (int) $sale['quantity'];
        $current     = $this->AchievementProduct->find('all', [
                        'conditions' => [
                            'AchievementProduct.product_id' => $sale['product_id'],
                            'Achievement.status'            => 1,
                            'Achievement.valid_from <='     => $now,
                            'Achievement.valid_to >='       => $now,
                        ],
                        'joins' => [
                            'INNER JOIN achievements AS Achievement ON Achievement.id = AchievementProduct.achievement_id',
                        ],
                    ]);
        

        foreach($current as $curr) {
            $achievement_id = $curr['achievement_id'];
            $product_id     = $curr['product_id'];

            $record = $this->AchievementUser->upsert($salesman_id, $achievement_id);
            // $record = $this->AchievementUser->find('first', [
            //     'conditions' => [
            //         'AchievementUser.user_id' => $salesman_id,
            //         'AchievementUser.achievement_id' => $achievement_id,
            //     ],
            // ]);
    
            if($this->block(self::SEM) === false){
                return false;
            }
        
            if($this->AchievementUser->loadById($record['id']) === false){
                $this->unblock(self::SEM);
                return false;
            }

            $former    = (int) $this->AchievementUser->readField('points');
            $delta     = $quantity;
            $updPoints = $former + $delta;
            error_log("Updating archievement=$achievement_id user=$salesman_id former=$former delta=$delta updated=$updPoints");
            
            if($this->AchievementUser->saveField('points', $updPoints) === false){
                $this->unblock(self::SEM);
                return false;
            }
            
            $this->unblock(self::SEM);

            $this->AchievementLog->append($salesman_id, $achievement_id, $product_id, $sale_id, $delta);
        }

        return true;
    }

    public function fetchFrontend($auth) {
        $now = date('Y-m-d H:i:s');
        $achievements = $this->rawFind('all', [
            'conditions' => [
                'Achievement.status'        => 1,
                'Achievement.valid_from <=' => $now,
                'Achievement.valid_to >='   => $now,
                'AchievementCompany.id'     => null,
            ],
            'order' => 'Achievement.`prio` ASC, Achievement.name ASC',
            'joins' => [
                'LEFT JOIN achievement_users AS AchievementUser ON AchievementUser.achievement_id = Achievement.id AND AchievementUser.user_id = ' . $auth['id'],
                'LEFT JOIN achievement_companies AchievementCompany ON AchievementCompany.achievement_id=Achievement.id AND AchievementCompany.company_id=' . $auth['data']['company_id']

            ],
            'fields' => [
                'Achievement.*',
                'AchievementUser.*',
            ]
        ]); 

        return $achievements;
    }
}
