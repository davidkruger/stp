<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ProductCatalog extends KlezBackendAppModel{
    private $schema = [
        'catalog_order' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'orderable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar un numero valido',
            'int-message' => 'Debe ingresar un numero valido',
            'label' => 'Orden Lista',
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto'
        ],
        'title' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar titulo',
            'label' => 'Titulo',
        ],
        'description' => [
            'type' => 'text',
            'subtype' => 'textarea',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar contenido',
            'label' => 'Contenido',
        ],
        'image' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Foto del Catalogo',
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'product_catalogs',
                'action' => 'image',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Estado del Catalogo',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'productless':
                return $this->productless();
            default:
                return $this->schema;
        }
    }

    private function productless(){
        $schema = $this->schema;
        $schema['product_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-CATALOG-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function productOwnerPrequery($query,$authdata,$payload){
        if(isset($payload['data']['product'])){
            $id = $payload['data']['product'];
        }
        else if(isset($payload['product'])){
            $id = $payload['product'];
        }
        else{
            return false;
        }
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $alias = $this->alias;
        $query['conditions']["{$alias}.product_id"] = $id;
        $this->product = $id;
        
        return $query;
    }
    
    public function fetchForProduct($p){
        return $this->find('all',[
            'order' => 'ProductCatalog.catalog_order ASC',
            'conditions' => [
                'ProductCatalog.product_id' => $p,
                'ProductCatalog.status' => 1,
            ]
        ],false);
    }
}