<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Assignment extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'type' => 'INNER',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'responsible_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Responsable',
            'autocomplete' => [
                'class' => 'FullUser',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'type' => 'INNER',
                'query' => [
                    'fields' => 'FullUser.id, FullUser.full_name',
                    'order' => 'FullUser.full_name ASC',
                    'conditions' => [
                        'FullUser.status' => true,
                        'FullUser.role' => 'responsable'
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'seller_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Vendedor',
            'autocomplete' => [
                'class' => 'Seller',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'type' => 'INNER',
                'query' => [
                    'fields' => 'Seller.id, Seller.full_name',
                    'order' => 'Seller.full_name ASC',
                    'conditions' => [
                        'Seller.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Vendedor',
            'autocomplete-message' => 'Debe especificar Vendedor',
            'icon' => 'account',
            'placeholder' => 'Buscar Vendedor'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Puntos',
            'int-message' => 'Cantidad de Puntos no valida'
        ],
    ];
        
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ASSIGNMENT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}