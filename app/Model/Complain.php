<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Complain extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Recibido',
            'date-message' => 'Recibido'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Remitente',
            'autocomplete' => [
                'class' => 'FullUser',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'FullUser.id, FullUser.full_name',
                    'order' => 'FullUser.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Usuario no encontrado',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'subject' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Asunto',
        ],
        'message' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Mensaje',
            'weight' => 12
        ],
    ];
    
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPLAIN-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function flagRead($query,$authdata,$payload){
        $id = $payload['id'];
        $Complain = new Complain();
        
        if($Complain->loadById($id)){
            if($Complain->readField('status') === 'unread'){
                $Complain->saveField('status', 'read');
            }
        }
        
        return $query;
    }
    
    public function injectUser($data,$authdata){
        return (int) $data['user_id'] === (int) $authdata['id'];
    }
}