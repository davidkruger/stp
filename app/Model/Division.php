<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Semaphore', 'Model');

class Division extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre de la Division',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre',
            'searchable' => true
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado de la Division',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'DIVISION-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}