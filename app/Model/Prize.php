<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('AllowedCompanies', 'Model');


class Prize extends KlezBackendAppModel{


    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'label' => 'Nombre del Premio',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre'
        ],
        'description' => [
            'type' => 'text',
            'subtype' => 'textarea',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'label' => 'Descripcion',
        ],
        'points' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Puntos Requeridos',
        ],
        'control_prize' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Control Cantidad Maxima',
            'boolean' => [
                0 => 'Desactivado',
                1 => 'Activo'
            ],
            'default' => 0,
        ],
        'max_prizes_month' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Cantidad Maxima de Canje al mes',
        ],
        'photo' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Foto del Premio',
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'prizes',
                'action' => 'photo',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'expiration' => [
            'type' => 'date',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Validez',
            'orderable' => true
        ],
        'eo' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => '#EO Relacionado',
            'searchable' => true
        ],
        'restriction' => [
            'type' => 'text',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Indicar Restricciones de Canje',
            'searchable' => true
        ],
        'group_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Grupo',
            'autocomplete' => [
                'class' => 'Group',
                'path' => 'Model',
                'alias' => 'Wroup',
                'label' => 'name',
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Wroup.id, Wroup.name',
                    'order' => 'Wroup.name ASC',
                    'conditions' => [
                        'Wroup.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Grupo',
            'autocomplete-message' => 'Debe especificar Grupo',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Grupo',
        ],
        'pdf' => [
            'type' => 'file',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'PDF',
            'mimes' => [
                'application/pdf',
                'application/x-pdf',
                'applications/vnd.pdf',
            ],
            'url' => [
                'controller' => 'prizes',
                'action' => 'pdf',
                'plugin' => null
            ],
            'placeholder' => 'Seleccione archivo PDF',
            'xlsx-message' => 'Debe ser un documento valido en formato PDF',
            'file-message' => 'Debe ser un documento valido en formato PDF '
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado del Premio',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
        'amount' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Monto GS (Zimple)',
        ],
        'minimal' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Pts. Minimo Zimple',
        ],
        'interval' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Pts. Intervalo Zimple',
        ]
    ];
    
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRIZE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function fetchFrontend($auth){

        $this->AllowedCompanies = new AllowedCompanies();
        
        $role = $auth['role'];
        $company = $auth['company_id'];
        
        $data = [];
        $cnd = [
            'Prize.status' => true,
            'Prize.expiration >= ' => date('Y-m-d'),
            'DG.id' => null
        ];
        
        if($role === 'responsable'){
            $cnd['Wroup.responsible_deny'] = 0;
        }

        $prizes = $this->find('all_pipe',[
            'conditions' => $cnd,
            'fields' => 'Prize.*,Wroup.*,Redee.*',
            'joins' => [
                'LEFT JOIN groups AS Wroup ON Wroup.id=Prize.group_id AND Wroup.status=1',
                'LEFT JOIN disabled_groups AS DG ON DG.group_id=Prize.group_id AND DG.company_id="' . $company . '"',
                "LEFT JOIN (
                    select 
                    prize_id,
                    COUNT(*) as total_use
                    FROM redeems
                    WHERE `status` in ('pendiente','aprobado') 
                    AND MONTH(created) = MONTH(CURRENT_DATE) 	
                    AND YEAR(created) = YEAR(CURRENT_DATE)
                    GROUP BY prize_id,MONTH(created),YEAR(created)

                ) as Redee ON Redee.prize_id = Prize.id"
            ],
            'order' => 'Prize.points desc'
        ]);

        //FILTRAR PREMIOS SEGUN CANTIDAD MAXIMA POR MES
        $data_new = [];
        foreach ($prizes as $key => $value) {
            $total_use = (int)$value['Redee']['total_use'];
            $max_use = (int)$value['Prize']['max_prizes_month'];
            $control_prize = (int)$value['Prize']['control_prize'];

            if($control_prize){
                if($total_use < $max_use){
                    $data_new[$key] = $value['Prize'];
                    $data_new[$key]['***foreign***'] = array('group_id' => $value['Wroup']['name']);
                }
            } else {
                $data_new[$key] = $value['Prize'];
                $data_new[$key]['***foreign***'] = array('group_id' => $value['Wroup']['name']);
            }


          
        } 
        $prizes = $data_new;

        
        foreach($prizes as $prize){

            $g = $prize['group_id'] ?: null;
            $c = $prize[self::FOREIGN_MARK]['group_id'] ?: null;
            $d = '#' . $g;
            
            if(!isset($data[$d])){
                $data[$d] = [
                    'prizes' => [],
                    'category' => $c
                ];
            }
            
            $data[$d]['prizes'][] = $prize;
        }
        
        if(isset($data['#11'])){
            $a = $data['#11'];
            unset($data['#11']);
            array_unshift($data, $a);
        }
        
        if(isset($data['#18'])){
            $a = $data['#18'];
            unset($data['#18']);
            array_unshift($data, $a);
        }

        
        if($this->AllowedCompanies->allowedCount() > 0){
            if($this->AllowedCompanies->allowedExits($company)){
                return $data;
            }else{
                if(isset($data['#20'])){
                    unset($data['#20']);
                    return $data;
                }
            }
        }else{
            return $data;
        }        
    }
    
    public function loadForRole($id, $role){
        $cnd = [
            'Prize.id' => $id,
            'Prize.status' => true,
            'Prize.expiration >= ' => date('Y-m-d')
        ];
        
        if($role === 'responsable'){
            $cnd['Wroup.responsible_deny'] = 0;
        }
        
        $prize = $this->find('first',[
            'conditions' => $cnd,
            'fields' => 'Prize.id',
            'joins' => [
                'LEFT JOIN groups AS Wroup ON Wroup.id=Prize.group_id AND Wroup.status=1'
            ],
        ]);
        
        
        if($prize){
            return $this->loadById($id);
        }
        
        return false;
    }
}