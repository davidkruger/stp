<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('ChallengeLog', 'Model');
App::uses('ChallengeUser', 'Model');
App::uses('ChallengeProduct', 'Model');

class Challenge extends KlezBackendAppModel{
    private $schema = [
        'title' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Titulo',
        ],
        'description' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Description',
        ],
        'target' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Meta',
        ],
        'products' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Productos Target',
            'habtm' => [
                'foreign' => [
                    'class' => 'FullProduct',
                    'path' => 'Model',
                    'label' => 'product',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'ChallengeProduct',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'challenge_id',
                        'foreign' => 'product_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Productos asignados',
            'placeholder' => 'Productos asignados al Desafio'
        ],
        'prize_img' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto del Premio',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'challenges',
                'action' => 'prize_img',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'valid_from' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Valido desde',
        ],
        'valid_to' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Valido hasta',
        ],
        'cost' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'label' => 'Costo',
            'orderable' => true,
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Participantes',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'ChallengeCompany',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'challenge_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas asignadas',
            'placeholder' => 'Empresas Participantes'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado del Desafio',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo',
            ],
            'default' => 1,
        ],
    ];


    public function xlsxFilterSchema(){
       
        set_time_limit(0);
        ini_set('memory_limit', -1);
 
        return [
            'challenge_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Desafio',
                'autocomplete' => [
                    'class' => 'Challenge',
                    'alias' => 'Cha',
                    'path' => 'Model',
                    'label' => 'title',
                    'identifier' => 'id',
                    'query' => [
                        'fields' => 'Cha.id, Cha.name',
                        'order' => 'Cha.name ASC',
                        'conditions' => [
                            
                        ],
                    ]
                ],          
                'required-message' => 'Debe especificar Desafio',
                'autocomplete-message' => 'Debe especificar Desafio',
                'icon' => 'gears',
                'placeholder' => 'Buscar Desafio'
            ],
            'company_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'writable' => true,
                'readable' => true,
                'required' => false,
                'weight' => 4,
                'label' => 'Empresa',
                'autocomplete' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'identifier' => 'id',
                    'type' => 'LEFT',
                    'query' => [
                        'fields' => 'Company.id, Company.name',
                        'order' => 'Company.name ASC',
                        'conditions' => [
                            'Company.status' => 1,
                        ]
                    ]
                ],
                'required-message' => 'Debe especificar Empresa',
                'autocomplete-message' => 'Debe especificar Empresa',
                'icon' => 'shopping-bag',
                'placeholder' => 'Buscar Empresa'
            ],
            'user_id' => [
                'type' => 'foreign',
                'subtype' => 'autocomplete',
                'writable' => true,
                'readable' => true,
                'required' => false,
                'label' => 'Vendedor',
                'autocomplete_dep' => 'company_id',
                'autocomplete_dep_condition' => 'Salesman.company_id',
                'autocomplete' => [
                    'class' => 'Salesman',
                    'path' => 'Model',
                    'label' => 'full_name',
                    'identifier' => 'id',
                    'query' => [
                        'fields' => 'Salesman.id, Salesman.full_name',
                        'order' => 'Salesman.full_name ASC',
                        'conditions' => [
                            
                        ],
                    ]
                ],          
                'required-message' => 'Debe especificar Usuario',
                'autocomplete-message' => 'Debe especificar Usuario',
                'icon' => 'gears',
                'placeholder' => 'Buscar Usuario'
            ],
            'valid_from' => [
                'type' => 'date',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Fecha desde',
            ],
            'valid_to' => [
                'type' => 'date',
                'required' => false,
                'writable' => true,
                'readable' => true,
                'label' => 'Fecha hasta',
            ],
        ];
    }

    public function xlsxSchema(){
        $schema = $this->xlsxFilterSchema();
        $schema['challenge_id']['readable'] = false;
        $schema['company_id']['readable'] = false;
        $schema['user_id']['readable'] = false;

        $schema['title'] = $this->schema['title'];
        $schema['title']['label'] ='Desafio';
        $schema['target'] = $this->schema['target'];
        $schema['valid_from'] = $this->schema['valid_from'];
        $schema['valid_to'] = $this->schema['valid_to'];
        $schema['salesman'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Vendedor',
        ];
        $schema['document'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Documento',
        ];
        $schema['company'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Empresa',
        ];
        $schema['branch'] = [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Sucursal',
        ];
        $schema['p'] = [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Progreso',
        ];

        $this->virtualFields['salesman'] = 'Salesman.full_name';
        $this->virtualFields['document'] = 'Salesman.document';
        $this->virtualFields['company'] = 'Company.name';
        $this->virtualFields['branch'] = 'Branch.name';
        $this->virtualFields['p'] = 'ChallengeUser.points';


        return $schema;
    }

    public function xlsxFilter($query,$authdata,$payload){
        $query['joins'] =  [
            "INNER JOIN challenge_users AS ChallengeUser ON ChallengeUser.challenge_id=Challenge.id",
            "INNER JOIN salesmen AS Salesman ON Salesman.id=ChallengeUser.user_id",
            "INNER JOIN companies AS Company ON Company.id=Salesman.company_id",
            "INNER JOIN branches AS Branch ON Branch.id=Salesman.branch_id",
        ];
        

        $cnd = [];

        if ($company = (int)@$payload['filter']['company_id']) {
            $cnd['Company.id'] = $company;
        }

        if ($user = (int)@$payload['filter']['user_id']) {
            $cnd['ChallengeUser.user_id'] = $user;
        }

        if ($challenge = (int)@$payload['filter']['challenge_id']) {
            $cnd['Challenge.id'] = $challenge;
        }

        if ($from = @$payload['filter']['valid_from']) {
            $chunks = split('/', $from);
            $cnd['Challenge.valid_from >= '] =  $chunks[2] . '-' . $chunks[1] . '-' . $chunks[0];
        }

        if ($to = @$payload['filter']['valid_to']) {
            $chunks = split('/', $to);
            $cnd['Challenge.valid_to <= '] =   $chunks[2] . '-' . $chunks[1] . '-' . $chunks[0];
        }

        $query['conditions'] = $cnd;

        $query['order'] = [
            'Challenge.title ASC',
            'Salesman.full_name ASC'
        ];

        return $query;
    }
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxFilter':
                return $this->xlsxFilterSchema();
            case 'xlsx':
                return $this->xlsxSchema();
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CHALLENGE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    /**
     * Determina los Challenges activos segun detalle de ventas.
     * 
     * Si hay coincidencia de productos se asignan los valores correspondientes.
     */
    
    private $ChallengeLog = null;
    private $ChallengeProduct = null;
    private $ChallengeUser = null;
    const SEM = 'CHA::update()';
    
    public function updateChallenge($sale){
        if(is_null($this->ChallengeLog)){
            $this->ChallengeLog = new ChallengeLog();
        }
        
        if(is_null($this->ChallengeProduct)){
            $this->ChallengeProduct = new ChallengeProduct();
        }
        
        if(is_null($this->ChallengeUser)){
            $this->ChallengeUser = new ChallengeUser();
        }

        $now          = $sale['sale_date'];
        $sale_id      = $sale['id'];
        $salesman_id  = $sale['salesman_id'];
        $quantity     = (int) $sale['quantity'];
        $current      = $this->ChallengeProduct->find('all', [
                        'conditions' => [
                            'ChallengeProduct.product_id' => $sale['product_id'],
                            'Challenge.status'            => 1,
                            'Challenge.valid_from <='     => $now,
                            'Challenge.valid_to >='       => $now,
                        ],
                        'joins' => [
                            'INNER JOIN challenges AS Challenge ON Challenge.id=ChallengeProduct.challenge_id'
                        ]
                    ]);

        foreach($current as $curr) {
            $challenge_id = $curr['challenge_id'];
            $product_id   = $curr['product_id'];

            $record = $this->ChallengeUser->find('first', [
                'conditions' => [
                    'ChallengeUser.user_id'      => $salesman_id,
                    'ChallengeUser.challenge_id' => $challenge_id,
                ]
            ]);
    
            if($this->block(self::SEM) === false){
                return false;
            }

            if (! $this->loadById($challenge_id)) {
                error_log("Cannot load challenge=$challenge_id user=$salesman_id");
                $this->unblock(self::SEM);
                continue;
            }

            $target = (int) $this->readField('target');
        
            if (!$record || $this->ChallengeUser->loadById($record['id']) === false){
                error_log("Challenge n/a challenge=$challenge_id user=$salesman_id");
                $this->unblock(self::SEM);
                continue;
            }

            $sql       = "SELECT COALESCE(SUM(quantity), 0) AS sum FROM challenge_logs cl WHERE " .
                          "cl.challenge_id = '$challenge_id' AND " .
                          "cl.product_id   = '$product_id' AND " .
                          "cl.user_id      = '$salesman_id'";
            $logsSum   = $this->query($sql);            
            $sold      = (int) @$logsSum[0][0]['sum'];
            $remaining = $target - $sold;

            if ($remaining <= 0) {
                # vendio todos los productos
                error_log("Challenge sold out challenge=$challenge_id user=$salesman_id product=$product_id target=$target sold=$sold remaining=$remaining");
                $this->unblock(self::SEM);
                continue;
            }
   
            $apply_quantity     = $quantity > $remaining ? $remaining : $quantity;
            $totalPoints        = $target;
            $former             = (int) $this->ChallengeUser->readField('progress');
            $formerPoints       = (int) $this->ChallengeUser->readField('points');
            $delta              = ceil(100 * $apply_quantity / $totalPoints);
            $updProgress        = $former + $delta;    
            $updPoints          = $formerPoints + $apply_quantity;

            error_log("Updating challenge=$challenge_id user=$salesman_id former=$former delta=$delta updated=$updProgress");

            
            $this->ChallengeUser->id = $record['id'];
            if($this->ChallengeUser->saveField('progress', $updProgress) === false){
                $this->unblock(self::SEM);
                return false;
            }
            if($this->ChallengeUser->saveField('points', $updPoints) === false){
                $this->unblock(self::SEM);
                return false;
            }
            
            $this->unblock(self::SEM);

            $this->ChallengeLog->append($salesman_id, $challenge_id, $product_id, $sale_id, $apply_quantity);
        }

        return true;
    }

    public function fetchFrontend($auth) {
        $now = date('Y-m-d H:i:s');
        return $this->rawFind('all', [
            'conditions' => [
                'Challenge.status'            => 1,
                'Challenge.valid_from <='     => $now,
                'Challenge.valid_to >='       => $now,
            ],
            'joins' => [
                'LEFT JOIN challenge_users ChallengeUser ON ChallengeUser.challenge_id=Challenge.id AND ChallengeUser.user_id=' . $auth['id'] ,
                'INNER JOIN challenge_companies ChallengeCompany ON ChallengeCompany.challenge_id=Challenge.id AND ChallengeCompany.company_id=' . $auth['data']['company_id']
            ],
            'fields' => [
                'Challenge.*', 'ChallengeUser.*',
            ],
            'order' => 'Challenge.id DESC',
        ]);
    }
}