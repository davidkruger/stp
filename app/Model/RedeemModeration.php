<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class RedeemModeration extends KlezBackendAppModel{    
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Moderacion',
            'writable' => true,
            'required' => true,
            'readable' => false,
        ],
        'user_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Moderador',
        ],
        'redeem_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Canje',
        ],
        'observations' => [
            'type' => 'text',
            'label' => 'Observaciones',
            'writable' => true,
            'readable' => false,
            'required' => false,
            'null' => 'No hay observaciones',
            'blank' => 'No hay observaciones',
        ],
        'moderation' => [
            'type' => 'options',
            'writable' => true,
            'readable' => false,
            'required' => true,
            'label' => 'Moderacion del Canje',
            'options' => [
                'aprobado' => 'Aprobado',
                'rechazado' => 'Rechazado',
            ],
        ],
    ];
      
    private $readableSchema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Moderacion',
            'writable' => true,
            'required' => true,
            'readable' => true,
        ],
        'user_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Moderador',
        ],
        'moderation' => [
            'type' => 'options',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'label' => 'Moderacion del Canje',
            'options' => [
                'aprobado' => 'Aprobado',
                'rechazado' => 'Rechazado',
            ],
        ],
        'observations' => [
            'null' => 'No hay observaciones',
            'blank' => 'No hay observaciones',
            'type' => 'text',
            'label' => 'Observaciones',
            'writable' => true,
            'readable' => true,
            'required' => false,
        ],
    ];
    
    public function provideSchema() {
        switch ($this->getShape()){
            case 'readable';
                return $this->readableSchema;
        }
        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'REDEEM-MOD-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function injectRedeem($query,$authdata,$payload){
        $query['conditions']['RedeemModeration.redeem_id'] = $query['conditions']['RedeemModeration.id'];
        unset($query['conditions']['RedeemModeration.id']);
        
        return $query;
    }
}