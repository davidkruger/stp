<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class AchievementProduct extends KlezBackendAppModel{
    private $schema = [
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        'Product.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto',
            'compound_unique'=> [ 'achievement_id' ],
            'compound_unique-message' => 'Producto ya ha sido asignado a este Logro',
        ],
        'achievement_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Logro',
            'autocomplete' => [
                'class' => 'Achievement',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Achievement.id, Achievement.name',
                    'order' => 'Achievement.name ASC',
                    'conditions' => [
                        'Achievement.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Logro'
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'achievementless':
                return $this->achievementless();
            default:
                return $this->schema;
        }
    }

    private function achievementless(){
        $schema = $this->schema;
        $schema['achievement_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ACHIEVEMENT-PRODUCT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function achievementOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['achievement'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.achievement_id"] = $id;
        
        return $query;
    } 
}