<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class CompanyContact extends KlezBackendAppModel{
    private $schema = [
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'email' => [
            'type' => 'text',
            'subtype' => 'email',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Email',
            'required-message' => 'Debe ingresar direccion de email'
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'companyless':
                return $this->companyless();
            default:
                return $this->schema;
        }
    }

    private function companyless(){
        $schema = $this->schema;
        $schema['company_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPANY-CONTACT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function companyOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['company'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.company_id"] = $id;
        
        return $query;
    } 
}