<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('RunSale', 'Model');
App::uses('Responsible', 'Model');
App::uses('Achievement', 'Model');
App::uses('Challenge', 'Model');
App::uses('User', 'Model');

class Sale extends KlezBackendAppModel{
    private $schema = [
        'sale_date' => [
            'type' => 'date',
            'orderable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Fecha Venta',
            'date-message' => 'Fecha no valida'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'searchable' => true,
            'like' => '%%%s%%',
            'autocomplete' => [
                'class' => 'FullCompany',
                'path' => 'Model',           
                'label' => 'company',
                'full' => true,
                'identifier' => 'id',
                'query' => [
                    'fields' => 'FullCompany.id, FullCompany.company',
                    'order' => 'FullCompany.company ASC',
                    'conditions' => [
                        'FullCompany.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'RUC no encontrado',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar RUC'
        ],
        'branch_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Sucursal',
            'orderable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'autocomplete_dep' => 'company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',           
                'label' => 'name',
                'full' => true,
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Branch.id, Branch.name',
                    'order' => 'Branch.name ASC',
                    'conditions' => [
                        'Branch.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Sucursal no encontrada',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'salesman_id' => [
            'type' => 'foreign',
            'searchable' => true,
            'like' => '%%%s%%',
            'subtype' => 'autocomplete',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'hidden' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Vendedor',
            'autocomplete_dep' => 'branch_id',
            'autocomplete' => [
                'class' => 'Salesman',
                'path' => 'Model',
                'label' => 'full_name',
                'full' => true,
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Salesman.id, Salesman.full_name',
                    'order' => 'Salesman.full_name ASC',
                    'conditions' => [
                        'Salesman.status' => 1                        
                    ]
                ]
            ],
            'massive_foreign_resolver' => 'salesmanMassiveResolver',
            'required-message' => 'Debe especificar Vendedor',
            'autocomplete-message' => 'Debe especificar Vendedor',
            'icon' => 'user',
            'placeholder' => 'Buscar Vendedor'
        ],
        'photo' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'sales',
                'action' => 'photo',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'invoice' => [
            'type' => 'text',
            'searchable' => true,
            'like' => '%%%s%%',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Factura',
        ],
        'product_id' => [
            'searchable' => true,
            'like' => '%%%s%%',
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'FullProduct',
                'path' => 'Model',
                'label' => 'product',
                'identifier' => 'id',
                'full' => true,
                'query' => [
                    'fields' => 'FullProduct.id, FullProduct.product',
                    'order' => 'FullProduct.product ASC',
                    'conditions' => [
                        'FullProduct.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Codigo de Producto no encontrado',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Codigo'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Cantidad de Productos',
            'int-message' => 'Cantidad no valida'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Puntos',
        ],
        'created' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Fecha Carga',
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Cargador',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'email',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.email',
                    'order' => 'User.email ASC',
                    'conditions' => [
                        'User.status' => 1                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'approval' => [
            'type' => 'options',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'hidden' => true,
            'listable' => false,
            'label' => 'Aprobacion',
            'options' => [
                'ninguna' => 'Ninguna',
                'responsable' => 'Encargado',
                'samsung' => 'Samsung'
            ]
        ],
        'massive_sale_id' => [
            'type' => 'int',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'hidden' => true,
            'listable' => false,
            'label' => 'Masivo',
        ],
        'extra_target' => [
            'type' => 'foreign',
            'searchable' => true,
            'like' => '%%%s%%',
            'subtype' => 'autocomplete',
            'required' => false,
            'writable' => true,
            'hidden' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Responsable/Encargado',
            'autocomplete_dep' => 'company_id',
            'autocomplete' => [
                'class' => 'Responsible',
                'path' => 'Model',
                'label' => 'full_name',
                'full' => true,
                'type' => 'LEFT',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Responsible.id, Responsible.full_name',
                    'order' => 'Responsible.full_name ASC',
                    'conditions' => [
                        'Responsible.status' => 1                        
                    ]
                ]
            ],
            'massive_foreign_resolver' => 'responsibleMassiveResolver',
            'autocomplete-message' => 'Debe especificar Responsable/Encargado',
            'icon' => 'user',
            'placeholder' => 'Buscar Responsable/Encargado'
        ],
    ];
    
    private $summarySchema = [
        'full_name' => [
            'label' => 'Vendedor',
            'type' => 'text',
            'readable' => true
        ],
        'acum' => [
            'label' => 'Acumulado',
            'type' => 'text',
            'readable' => true
        ],
        'redeemed' => [
            'label' => 'Canjeado',
            'type' => 'text',
            'readable' => true
        ],
        'delta' => [
            'label' => 'Restante',
            'type' => 'text',
            'readable' => true
        ],
    ];
    
    private $massiveSchema = [
        'document' => [
            'type' => 'file',
            'subtype' => 'xlsx',
            'weight' => 12,
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => false,
            'label' => 'Carga Masiva',
            'mimes' => [
//                'application/csv',
//                'application/lotus123',
//                'application/msexcel',
//                'application/vnd.lotus-1-2-3',
//                'application/vnd.ms-excel',
//                'application/vnd.ms-excel [official]',
//                'application/vnd.ms-works',
//                'application/vnd.msexcel',
//                'application/wks',
//                'application/x-dos_ms_excel',
//                'application/x-excel',
//                'application/x-lotus123',
//                'application/x-msexcel',
//                'application/x-msworks',
//                'application/x-wks',
//                'application/xlc',
//                'application/xlt',
//                'text/anytext',
//                'text/comma-separated-values',
//                'text/csv',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ],
            'url' => [
                'controller' => 'sales',
                'action' => 'document',
                'plugin' => null
            ],
            'placeholder' => 'Seleccione archivo XLSX',
            'xlsx-message' => 'Debe ser un documento valido en formato XLSX',
            'file-message' => 'Debe ser un documento valido en formato XLSX '
//            'placeholder' => 'Seleccione archivo CSV',
//            'csv-message' => 'Debe ser un documento valido en formato CSV',
//            'file-message' => 'Debe ser un documento valido en formato CSV '
        ],
    ];
    
    public $validate = [
        'branch_id' => [
            'companyCheck' => [
                'rule' => [
                    'branchCompanyCheck'
                ]
            ]
        ],
        'extra_target' => [
            'companyCheck' => [
                'rule' => [
                    'extraTargetCompanyCheck'
                ]
            ]
        ],
        'salesman_id' => [
            'companyCheck' => [
                'rule' => [
                    'salesmanCompanyCheck'
                ]
            ]
        ],
    ];
    
    public function fetchFrontendPendings($uid){
        $cnd = [];
        $cnd['Sale.salesman_id'] = $uid;
        $cnd['Sale.approval != '] = 'samsung';
        $cnd['Sale.rejected'] = 0;
        
        return $this->find('count', [
            'conditions' => $cnd
        ]);
    }
    
    public function branchCompanyCheck(){
        App::uses('Branch','Model');
        $Branch = new Branch();
        $branch = $this->readField('branch_id');
        $branch2 = $this->readField('branch_id2');
        $branch3 = $this->readField('branch_id3');
        $company = $this->readField('company_id');
        
        if(!$branch){
            return true;
        }
        
        $cnd = [];
        $cnd['Branch.id'] = [$branch,$branch2,$branch3];
        $cnd['Branch.company_id'] = $company;
        
        if($Branch->loadByConditions($cnd)){
            return true;
        }
        
        $this->writeField('branch_id', null);
        return 'Sucursal no corresponde a Empresa';
    }
    
    public function salesmanCompanyCheck(){
        App::uses('Salesman','Model');
        $Salesman = new Salesman();
        $salesman = $this->readField('salesman_id');
        $company = $this->readField('company_id');
        
        if(!$salesman){
            return true;
        }
        
        $cnd = [];
        $cnd['Salesman.id'] = $salesman;
        $cnd['Salesman.company_id'] = $company;
        
        if($Salesman->loadByConditions($cnd)){
            if(!$Salesman->readField('branch_id')){
                return 'Vendedor no posee Sucursal asociada';
            }
            
            return true;
        }
            
        $this->writeField('salesman_id', null);
        return 'Vendedor no corresponde a Empresa';
    }
    
    public function extraTargetCompanyCheck(){
        App::uses('Responsible','Model');
        $Responsible = new Responsible();
        $responsible = $this->readField('extra_target');
        $company = $this->readField('company_id');
        $branch = $this->readField('branch_id');
        
        if(!$responsible){
            return true;
        }
        
        $cnd = [];
        $cnd['Responsible.id'] = $responsible;
        
        if($Responsible->loadByConditions($cnd)){
            switch ($Responsible->readField('role')){
                case 'encargado':
                    if($Responsible->readField('branch_id') == $branch){
                        return true;
                    }
                    
                    if($Responsible->readField('branch_id2') == $branch){
                        return true;
                    }
                    
                    if($Responsible->readField('branch_id3') == $branch){
                        return true;
                    }
                    
                    $this->writeField('extra_target', null);
                    return 'Encargado no corresponde a Sucursal';
                case 'responsable':
                    if($Responsible->readField('company_id') == $company){
                        return true;
                    }
                    
                    $this->writeField('extra_target', null);
                    return 'Responsable no corresponde a Empresa';
            }
        }
            
        $this->writeField('extra_target', null);
        return 'Responsable/Encargado no valido';
    }
    
    public function readWithStock(){
        return [];
    }
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxRunSale':
                return $this->xlsxRunSaleSchema();
            case 'read':
                return $this->readSchema();
            case 'frontend':
                return $this->frontendSchema();
            case 'rejected':
                return $this->rejectedSchema();
            case 'massive':
                return $this->massiveSchema();
            case 'xlsxSaleFilter':
                return $this->xlsxSaleFilterSchema();
            case 'xlsxSale':
                return $this->xlsxSaleSchema();
            case 'reportSummaryFilters':
                return $this->reportSummaryFiltersSchema();
            case 'reportCompaniesFilters':
                return $this->reportCompaniesFiltersSchema();
            case 'reportProductsFilters':
                return $this->reportProductsFiltersSchema();
            case 'summary':
                return $this->summarySchema;
            case 'empty':
                return [];
            default:
                return $schema;
        }
    }
    
    private function reportProductsFiltersSchema(){
        return [
            'product_id' => $this->schema['product_id'],
            'a' => [
                'label' => 'Fecha Desde',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'date-message' => 'Fecha no valida'
            ],
            'b' => [
                'label' => 'Fecha Hasta',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'date-message' => 'Fecha no valida'
            ],
        ];
    }
    
    private function reportCompaniesFiltersSchema(){
        return [
            'company_id' => $this->schema['company_id'],
            'a' => [
                'label' => 'Fecha Desde',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'date-message' => 'Fecha no valida'
            ],
            'b' => [
                'label' => 'Fecha Hasta',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'date-message' => 'Fecha no valida'
            ],
        ];
    }
    
    private function reportSummaryFiltersSchema(){
        return [
            'a' => [
                'label' => 'Fecha Desde',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'weight' => 6
            ],
            'b' => [
                'label' => 'Fecha Hasta',
                'type' => 'date',
                'writable' => true,
                'readable' => true,
                'weight' => 6
            ],
        ];
    }
    
    private $division = [
        'label' => 'Division',
        'type' => 'text',
        'orderable' => true,
        'required' => true,
        'writable' => true,
        'readable' => true,
        'listable' => true,
    ];
    
    private $subdivision = [
        'label' => 'Subdivision',
        'type' => 'text',
        'orderable' => true,
        'required' => true,
        'writable' => true,
        'readable' => true,
        'listable' => true,
    ];
    
    private function xlsxSaleSchema(){
        $this->useTable = 'full_sales';
        $points =  $this->schema['points'];
        $points['hidden'] = false;
       
        set_time_limit(0);
        ini_set('memory_limit', -1);
 
        $schema = [
            'sale_date' => $this->schema['sale_date'],
            'company_id' => $this->schema['company_id'],
            'branch_id' => $this->schema['branch_id'],
            'extra_target' => $this->schema['extra_target'],
            'salesman_id' => $this->schema['salesman_id'],
            'division' => $this->division,
	   // 'points_rate' => $this->schema['points_rate'],
            'subdivision' => $this->subdivision,
            'product_id' => $this->schema['product_id'],
            'points' => $points,
        ];
        
        return $schema;
    }
    
    private function xlsxSaleFilterSchema(){
        $schema = [
            'company_id' => $this->schema['company_id'],
            'branch_id' => $this->schema['branch_id'],
        ];
        
        $schema['company_id']['required'] = false;
        $schema['branch_id']['required'] = false;
        
        return $schema;
    }
        
    private function rejectedSchema(){
        $schema = $this->schema;
        $schema['rejection_comments'] = [
            'type' => 'text',
            'searchable' => true,
            'like' => '%%%s%%',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Motivo de Rechazo',
        ];
        
        return $schema;
    }
    
    public function xlsxSaleFilter($query,$authdata,$payload){
        $company = (int) @$payload['filter']['company_id'];
        $branch = (int) @$payload['filter']['branch_id'];
        
        if($company > 0){
            $query['conditions']['Sale.company_id'] = $company;
        }
        
        if($branch > 0){
            $query['conditions']['Sale.branch_id'] = $branch;
        }
        
        return $query;
    }
    
    private function xlsxRunSaleSchema(){
        $schema = $this->schema;  
        $this->virtualFields['points'] = 'RunSaleLog.points';
        return $schema;
    }
    
    private function frontendSchema(){
        $schema = $this->schema;  
        $schema['product_id']['autocomplete']['label'] = 'code';
        unset($schema['company_id']);
        unset($schema['user_id']);
        return $schema;
    }
    
    private function readSchema(){
        $schema = $this->schema;  
        $schema['company_id']['autocomplete']['label'] = 'company';
        $schema['product_id']['autocomplete']['label'] = 'product';
        $schema['points']['hidden'] = false;
        
        return $schema;
    }
    
    private function massiveSchema(){
        $schema = $this->massiveSchema; 
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => '#VENTA',
            'listable' => true,
            'readable' => true,
            'hidden' => in_array($this->getShape(), [
                'massive','manual','xlsxSaleFilter', 'xlsxSale', 
                'reportSummaryFilters', 'summary','reportCompaniesFilters',
                'reportProductsFilters'
            ])
        ];
    }
    
    public function injectUser(&$data,$authdata){
        $uid = $authdata['id'];
        $this->writeField('user_id', $uid);
        $data['user_id'] = $uid;
        
        if($authdata['data']['role'] === 'encargado'){
            if(!$this->readField('extra_target')){
                $this->writeField('extra_target', $uid);
                $data['extra_target'] = $uid;
            }
        }
        
        return true;
    }
    
    public function injectCompany(&$data,$authdata){
        $uid = $data['salesman_id'];
        
        if(is_null($this->User)){
            App::uses('User','Model');
            $this->User = new User();
        }
        
        if(!$this->User->loadById($uid)){
            return false;
        }
        
        $salesmanCompany = (int) $this->User->readField('company_id');
        $myCompany = (int) $authdata['data']['company_id'];
        
        switch($authdata['data']['role']){
            case 'vendedor':
                return false;
            case 'responsable':
            case 'encargado':
                if($salesmanCompany !== $myCompany){
                    $this->writeField('salesman_id', null);
                    $data['salesman_id'] = null;
                    $this->writeField('company_id', null);
                    $data['company_id'] = null;
                }
                else{
                    $this->writeField('company_id', $myCompany);
                    $data['company_id'] = $myCompany;
                }
                
                break;
            case 'samsung':
            case 'lamoderna':
//            case 'romis':
                $this->writeField('company_id', $salesmanCompany);
                $data['company_id'] = $salesmanCompany;
                break;
                
        }
        
        return true;
    }
    
    private $MassiveSale = null;
    
    public function injectMassiveSale(&$data,$authdata){
        if(is_null($this->MassiveSale)){
            App::uses('MassiveSale', 'Model');
            $this->MassiveSale = new MassiveSale();
            
            if(!$this->MassiveSale->alloc($authdata)){
                return false;
            }
        }
        
        $this->writeField('massive_sale_id', $this->MassiveSale->id);
        $this->writeField('approval', $this->MassiveSale->readField('approval'));
        $data['massive_sale_id'] = $this->MassiveSale->id;
        $data['approval'] = $this->MassiveSale->readField('approval');
        return true;
    }
    
    private $Product;
    private $ProductPromotion;
    private $ProductPoint;
    private $ExtraPoint;
    
    private function resolvPoints($data){
        App::uses('Product', 'Model');
        App::uses('ProductPoint', 'Model');
        App::uses('ExtraPoint', 'Model');
        App::uses('ProductPromotion', 'Model');
        App::uses('Company', 'Model');
        
        $this->Product = new Product();
        $this->ProductPoint = new ProductPoint();
        $this->ProductPromotion = new ProductPromotion();
        $this->Company = new Company();
        $multiply = 100;

        if($this->Product->loadById($data['product_id']) === false){
            return false;
        }

        if($this->Company->loadById($data['company_id'])){
            $multiply = $this->Company->readField('multiply') ?: 100;
        }
        
        $date = $data['sale_date'];
        $base = $this->ProductPoint->getBase($data, $this->Product->readField('points'));
        $extra = $this->ProductPromotion->resolvExtraPoints($date,$data['product_id']);
        $q = $data['quantity'];
        $points = 
                ( $base + $extra ) * $q * ($multiply / 100);
        
        return round($points);
    }
    
    public function filterAutocomplete($query,$authdata,$payload){
        $id = $payload['data']['dep'];
        
        switch($payload['data']['field']){
            case 'salesman_id':
                $query['conditions']['Salesman.branch_id'] = $id;
//                $query['conditions']['Salesman.role'] = [ 'vendedor', 'responsable', 'encargado' ];
                break;
            case 'branch_id':
                $query['conditions']['Branch.company_id'] = $id;
                $query['conditions']['Branch.status'] = 1;
                break;
            case 'extra_target':
                $query['conditions']['Responsible.company_id'] = $id;
                $query['conditions']['Responsible.status'] = 1;
                break;
        }
        
        return $query;
    }
    
    public function injectApproval($data,$authdata){
        switch($authdata['data']['role']){
            case 'vendedor':
                $this->writeField('approval', 'ninguna');
                break;
            case 'responsable':
            case 'encargado':
                $this->writeField('approval', 'responsable');
                break;
            case 'samsung':
            case 'lamoderna':
//            case 'romis':
                $this->writeField('approval', 'samsung');
                break;
                
        }
        return true;
    }
    
    public function injectPoints($data,$authdata){
        $points = $this->resolvPoints($data);
        
        if($points === false){
            return false;
        }
        
        $this->writeField('points', $points);
        return true;
    }
    
    public function injectSalesman($data,$authdata){
        $role = $authdata['data']['role'];
        
        if($role === 'vendedor'){
            $this->writeField('salesman_id', $authdata['data']['id']);
        }
        else if($role === 'responsable' || $role === 'encargado'){
            if(is_null($this->User)){
                $this->User = new User();
            }
            
            $salesman = $this->readField('salesman_id');
            
            if(!$this->User->loadById($salesman)){
                return false;
            }
            
            return (int) $this->User->readField('company_id') === (int) $authdata['data']['company_id'];
        }
        
        return true;
    }
    
    public function calcPoints($Parent,$data){
        $points = $this->resolvPoints($data);
        
        if($points === false){
            return 0;
        }
        
        return $points;
    }
    
    private $Stock = null;
    
    
    public function calcStockWarning($Parent,$data){
        if(is_null($this->Stock)){
            App::uses('Stock','Model');
            $this->Stock = new Stock();
        }
        
        if(is_null($this->Product)){
            App::uses('Product','Model');
            $this->Product = new Stock();
        }
        
        if(is_null($this->Company)){
            App::uses('Company','Model');
            $this->Company = new Company();
        }
        
        if(is_null($this->User)){
            App::uses('User','Model');
            $this->User = new Stock();
        }
        
        $salesman = $data['salesman_id'];
        
        if(!$this->User->loadById($salesman)){
            return 'Vendedor no valido';
        }
        
        $company = $this->User->readField('company_id');
        $product = $data['product_id'];
        
        if(!$this->Company->loadById($company)){
            return 'Empresa no valida';
        }
        
        if(!$this->Product->loadById($product)){
            return 'Producto no valido';
        }
        
        $needed = $data['quantity'];
        
        if(!is_numeric($needed)){
            return 'Cantidad no valida';
        }
        
        $current = $this->Stock->resolvCurrent($company, $product);
                
        return $current < $needed ? 'No' : 'Si';
    }
    
    private $Branch;
    
    public function resolvMassiveBranch($Parent,$data,$authdata){
        $uid = $data['salesman_id'];
        $myCompany = (int) $authdata['data']['company_id'];
        $myBranch = (int) $authdata['data']['branch_id'];
        
        if(is_null($this->User)){
            App::uses('User','Model');
            $this->User = new User();
        }
        
        if(!$this->User->loadById($uid)){
            return false;
        }
        
        $salesmanCompany = (int) $this->User->readField('company_id');
        $salesmanBranch = (int) $this->User->readField('branch_id');
        
        if(is_null($this->Branch)){
            App::uses('Branch','Model');
            $this->Branch = new Branch();
        }
        
        switch ($authdata['data']['role']){
            case 'vendedor': 
                return false;
            case 'responsable':
                if($salesmanCompany !== $myCompany){
                    return false;
                }
                break;
            case 'encargado':
                $myBranch2 = (int) $authdata['data']['branch_id2'];
                $myBranch3 = (int) $authdata['data']['branch_id3'];
                
                if($salesmanBranch !== $myBranch &&  $salesmanBranch !== $myBranch2 && $salesmanBranch !== $myBranch3){
                    return false;
                }
                
                break;
        }
        
        if(!$this->Branch->loadById($salesmanBranch)){
            return false;
        }
        
        return [
            'id' => $this->Branch->id,
            'label' => $this->Branch->readField('name')
        ];
    }
    
    private $Company;
    
    public function resolvMassiveCompany($Parent,$data,$authdata){
        $uid = $data['salesman_id'];
        $myCompany = (int) $authdata['data']['company_id'];
        
        if(is_null($this->User)){
            App::uses('User','Model');
            $this->User = new User();
        }
        
        if(!$this->User->loadById($uid)){
            return false;
        }
        
        $salesmanCompany = (int) $this->User->readField('company_id');
        
        if(is_null($this->Company)){
            App::uses('Company','Model');
            $this->Company = new Company();
        }
        
        switch ($authdata['data']['role']){
            case 'vendedor': 
                return false;
            case 'encargado':
            case 'responsable':
                if($salesmanCompany !== $myCompany){
                    return false;
                }
                
                break;
        }
        
        if(!$this->Company->loadById($salesmanCompany)){
            return false;
        }
        
        return [
            'id' => $this->Company->id,
            'label' => $this->Company->readField('name')
        ];
    }
    
    private $User = null;
    
    public function updateSalesmanPoints($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $salesman_id = $this->readField('salesman_id');
        
        return $this->User->semUpdatePoints($salesman_id,$this->readField('points'));
    }
    
    public function updateExtraTargetPoints($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $extraTarget = $this->readField('extra_target');
        
        if(!$extraTarget){
            return true;
        }
        
        return $this->User->semUpdateExtraPoints($this->id,$extraTarget,$this->readField('points'));
    }
    
    public function updateResponsiblePoints($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $responsibles = $this->User->fetchResponsibles($this->readField('company_id'));
        
        if(empty($responsibles)){
            return true;
        }
        
        return $this->User->semUpdateResponsiblesPoints($this->id,$responsibles,$this->readField('points'));
    }
    
    public function updateEncargadosPoints($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $encargados = $this->User->fetchEncargados($this->readField('branch_id'));
        if(empty($encargados)){
            return true;
        }
        
        return $this->User->semUpdateResponsiblesPoints($this->id,$encargados,$this->readField('points'));
    }
    
    public function updateMassiveSalesmanPoints($data,$authdata){
        if($data['approval'] !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $salesman_id = $data['salesman_id'];
        
        return $this->User->semUpdatePoints($salesman_id,$data['points']);
    }
    
    public function updateMassiveExtraPoints($data,$authdata){
        if($data['approval'] !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $extraTarget = $data['extra_target'];
        
        if(!$extraTarget){
            return true;
        }
        
        return $this->User->semUpdateExtraPoints($data['id'],$extraTarget,$data['points']);
    }
    
    public function updateMassiveResponsiblePoints($data,$authdata){
        if($data['approval'] !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $responsibles = $this->User->fetchResponsibles($data['company_id']);
        
        if(empty($responsibles)){
            return true;
        }
        
        return $this->User->semUpdateResponsiblesPoints($data['id'],$responsibles,$data['points']);
    }
    
    public function updateMassiveEncargadosPoints($data,$authdata){
        if($data['approval'] !== 'samsung'){
            return true;
        }
        
        App::uses('User','Model');
        $this->User = new User();
        $encargados = $this->User->fetchEncargados($data['branch_id']);
        
        if(empty($encargados)){
            return true;
        }
        return $this->User->semUpdateResponsiblesPoints($data['id'],$encargados,$data['points']);
    }
    
    public function updateMassiveCompanyPoints($data,$authdata){
        App::uses('Company','Model');
        $this->Company = new Company();
        $company = $data['company_id'];
        
        return $this->Company->semUpdatePoints($company,$data['points']);
    }
    
    public function filterCompany($query,$authdata,$payload){
        if(isset($authdata['data']['company_id'])){
            $query['conditions']['Sale.company_id'] = $authdata['data']['company_id'];
        }
        
        if($authdata['data']['role'] === 'encargado'){
            $query['conditions']['OR'][]['Sale.branch_id'] = $authdata['data']['branch_id'];
            if($authdata['data']['branch_id2']) $query['conditions']['OR'][]['Sale.branch_id'] = $authdata['data']['branch_id2'];
            if($authdata['data']['branch_id3']) $query['conditions']['OR'][]['Sale.branch_id'] = $authdata['data']['branch_id3'];
        }
        
        return $query;
    }
    
    public function filterResponsableApproval($query,$authdata,$payload){
        $query['conditions']['Sale.approval'] = 'ninguna';
        return $query;
    }
    
    public function filterVendedor($query,$authdata,$payload){
        $role = $authdata['data']['role'];
        
        if($role === 'vendedor'){
            $query['conditions']['Sale.salesman_id'] = $authdata['data']['id'];
        }
        
        return $query;
    }
    
    public function filterMassiveSale($query,$authdata,$payload){
        $query['conditions']['Sale.massive_sale_id'] = $payload['massive_sale'];
        return $query;
    }
    
    public function filterPendingMassiveSale($query,$authdata,$payload){
        $query['conditions']['Sale.massive_sale_id'] = $payload['massive_sale'];
        $query['conditions']['Sale.approval'] = 'responsable';
        return $query;
    }
    
    public function injectRunSale($query,$authdata,$payload){
        $query['conditions']['RunSaleLog.run_sale_id'] = $payload['filter']['run_sale_id'];
        return $query;
    }
    
    /**
     * TODO: Alimentar tabla run_sale_logs segun run_sales vigentes y releventes
     * segun empresa/producto configurados.
     */
    
    private $RunSale = null;
    
    public function updateRunSales($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->RunSale)){
            $this->RunSale = new RunSale();
        }
        
        return $this->RunSale->allocRunSale($this->getWritableData() + [
            'id' => $this->id
        ]);
    }
    
    public function updateMassiveRunSales($data){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->RunSale)){
            $this->RunSale = new RunSale();
        }
        
        if($this->RunSale->allocRunSale($data) === false){
            throw new Exception("RunSale::allocRunSales() === false");
        }

        return true;
    }
    
    private $Achievement = null;
    
    public function updateAchievements($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->Achievement)){
            $this->Achievement = new Achievement();
        }
        
        return $this->Achievement->allocAchievement($this->getWritableData() + [
            'id' => $this->id
        ]);
    }
    
    public function updateMassiveAchievements($data){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->Achievement)){
            $this->Achievement = new Achievement();
        }
        
        if($this->Achievement->allocAchievement($data) === false){
            throw new Exception("Achievement::allocAchievement() === false");
        }
        return true;
    }
    
    private $Challenge = null;
    
    public function updateChallenges($data,$authdata){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->Challenge)){
            $this->Challenge = new Challenge();
        }
        
        return $this->Challenge->updateChallenge($this->getWritableData() + [
            'id' => $this->id
        ]);
    }
    
    public function updateMassiveChallenges($data){
        if($this->readField('approval') !== 'samsung'){
            return true;
        }
        
        if(is_null($this->Challenge)){
            $this->Challenge = new Challenge();
        }
        
        if($this->Challenge->updateChallenge($data) === false){
            throw new Exception("Challenge::updateChallenge() === false");
        }
        return true;
    }
    
    public function checkSamsungApprove($data){
        return $data['approval'] === 'responsable';// && is_null($data['massive_sale_id']);
    }
    
    private $Salesman;
    
    public function salesmanMassiveResolver($value,$authdata){
        if(is_null($this->Salesman)){
            App::uses('Salesman','Model');
            $this->Salesman = new Salesman();
        }
        
        $cnd = [];
        $cnd['Salesman.status'] = 1;
        $cnd['Salesman.document'] = $value;
        
        switch ($authdata['data']['role']){
            case 'vendedor': return false;
            case 'responsable': 
                $cnd['Salesman.company_id'] = $authdata['data']['company_id'];
                break;
            case 'encargado': 
                $cnd['Salesman.company_id'] = $authdata['data']['company_id'];
                $cnd['OR'][]['Salesman.branch_id'] = $authdata['data']['branch_id'];
                if($authdata['data']['branch_id2']) $cnd['OR'][]['Salesman.branch_id'] = $authdata['data']['branch_id2'];
                if($authdata['data']['branch_id3']) $cnd['OR'][]['Salesman.branch_id'] = $authdata['data']['branch_id3'];
                break;
//            case 'romis':
            case 'samsung':
            case 'lamoderna':
                break;
        }
        
        if(!$this->Salesman->loadByConditions($cnd)){
            return false;
        }
        
        $array = [
            'value' => $value,
            'label' => $this->Salesman->readField('full_name'),
            'id' => $this->Salesman->id
        ];
        
        return $array;
    }
    
    private $Responsible;
    
    public function responsibleMassiveResolver($value,$authdata){
        if(is_null($this->Responsible)){
            App::uses('Responsible','Model');
            $this->Responsible = new Responsible();
        }
        
        $cnd = [];
        $cnd['Responsible.status'] = 1;
        $cnd['Responsible.document'] = $value;
        
        switch ($authdata['data']['role']){
            case 'vendedor': return false;
            case 'responsable': 
            case 'encargado': 
                $cnd['Responsible.company_id'] = $authdata['data']['company_id'];
                break;
//            case 'romis':
            case 'samsung':
            case 'lamoderna':
                break;
        }
        
        if(!$this->Responsible->loadByConditions($cnd)){
            return false;
        }
        
        $array = [
            'value' => $value,
            'label' => $this->Responsible->readField('full_name'),
            'id' => $this->Responsible->id
        ];
        
        return $array;
    }
    
    public function customFiltersPrequery($query,$authdata,$payload){
        if(isset($payload['filter'])){
            $payload = $payload['filter'];
        }
        
        $company = (int) $payload['company_id'];
        $branch = (int) $payload['branch_id'];
        
        if($company){
            $query['conditions']["Salesman.company_id"] = $company;
        }
        
        if($branch){
            $query['conditions'][0]['OR']["Salesman.branch_id"] = $branch;
            $query['conditions'][0]['OR']["Salesman.branch_id2"] = $branch;
            $query['conditions'][0]['OR']["Salesman.branch_id3"] = $branch;
        }
        
        if(isset($payload['a'])){
            if($payload['a']){
                $aa = explode('/', $payload['a']);
                $a = (int) @$aa[2] . '-' . @$aa[1] . '-' . @$aa[0];
                $query['conditions']["(Sale.sale_date) >= "] = $a;
            }
            
            if($payload['b']){
                $bb = explode('/', $payload['b']);
                $b = (int) @$bb[2] . '-' . @$bb[1] . '-' . @$bb[0];
                $query['conditions']["(Sale.sale_date) <= "] = $b;
            }
        }
        
        return $query;
    }

    public function getRanking($company_id, $mo, $ye) {
        $sql = 
            'SELECT * FROM ranking ' .
            'WHERE ' .
            'company_id  = "' . $company_id  . '" AND ' .
            'month       = "' . $mo          . '" AND ' .
            'year        = "' . $ye          . '"' .
            'ORDER BY points DESC';

        return $this->query($sql);
    }

    public function getMonthly($user_id, $ye) {
        $sql = 
            'SELECT * FROM monthly ' .
            'WHERE ' .
            'salesman_id = "' . $user_id . '" AND ' .
            'year        = "' . $ye .      '"';

            $sql = 
            'SELECT * FROM monthly ' .
            'WHERE ' .
            'salesman_id = "' . $user_id . '"';

        $monthly = $this->query($sql);
        $data = [];

        foreach($monthly as $m) {
            $mo = str_pad($m['monthly']['month'], 2, 0, STR_PAD_LEFT);
            $x = $m['monthly']['year'] . '-' . $mo . '-01';
            $y = (int) $m['monthly']['points'];

            $data[$x] = $y;
        }

        $chartjs = [];
        for($i = 1; $i <= date("m");$i++) {
            $mo = str_pad($i, 2, 0, STR_PAD_LEFT);
            $x = "$ye-$mo-01";

            if(!isset($data[$x])) {
                $data[$x] = 0;
            }

            $chartjs[] = [
                'x' => $x,
                'y' => $data[$x],
            ];
        }
        
        return $chartjs;
    }
}
