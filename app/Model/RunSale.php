<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('RunSaleLog', 'Model');
App::uses('RunSaleCompany', 'Model');
App::uses('RunSaleTarget', 'Model');

class RunSale extends KlezBackendAppModel{
    const RUN_SALE_ZERO_PRODUCTS = 'No ha asignado ningun Producto';
    const RUN_SALE_ZERO_COMPANIES = 'No ha asignado ninguna Empresa';

    private $schema = [
        'description' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Descripcion',
        ],
        'goal' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Meta',
        ],
        'score' => [
            'type' => 'int',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Progreso',
        ],
        'starts_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Inicio',
        ],
        'ends_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Final',
        ],
        'prize_title' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Premio',
        ],
        'prize_img' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto del Premio',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'run_sales',
                'action' => 'prize_img',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'draw' => [
            'type' => 'options',
            'options' => [
                'manual' => 'Manual',
                'auto' => 'Automatico'
            ],
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Sorteo',
        ],
        'products' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Productos Target',
            'habtm' => [
                'foreign' => [
                    'class' => 'FullProduct',
                    'path' => 'Model',
                    'label' => 'product',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'RunSaleTarget',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'run_sale_id',
                        'foreign' => 'product_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Productos asignados',
            'placeholder' => 'Productos asignados a la Competencia'
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Participantes',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'RunSaleCompany',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'run_sale_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas asignadas',
            'placeholder' => 'Empresas Participantes'
        ],
        'draw_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'null' => 'Sin sortear',
            'label' => 'Fecha Sorteo',
        ],
    ];
    
    private $xlsxSchema = [
        'run_sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Competencia',
            'autocomplete' => [
                'class' => 'RunSale',
                'path' => 'Model',
                'label' => 'description',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'RunSale.id, RunSale.description',
                    'order' => 'RunSale.description ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Competencia',
            'autocomplete-message' => 'Debe especificar Competencia',
            'icon' => 'gears',
            'placeholder' => 'Buscar Competencia'
        ],
    ];
    
    public $validate = [
        'goal' => [
            'range' => [
                'rule' => [ 'goalRangeValidator' ],
                'message' => 'Debe ser mayor a 0'
            ]
        ]
    ];
    
    public function goalRangeValidator(){
        $goal = (int) $this->readField('goal');
        
        return $goal > 0;
    }
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxFilter':
                return $this->xlsxSchema;
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'RUN-SALE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function habtmValidator($data,$field){
        $array = $data[$field];
        
        foreach($array as $raw){
            if(strlen($raw) > 0){
                return true;
            }
        }
        
        return $this->getHabtmMessage($field);
    }
    
    private function getHabtmMessage($field){
        switch($field){
            case 'products':
                return self::RUN_SALE_ZERO_PRODUCTS;
            case 'companies':
                return self::RUN_SALE_ZERO_COMPANIES;
        }
        
        return false;
    }
    
    /**
     * Determina los RunSales participantes de la data proveniente de un Sale.
     * 
     * Si hay coincidencia de fechas y productos, se agrega el campo RunSaleLog.
     */
    
    private $RunSaleLog = null;
    private $RunSaleCompany = null;
    
    public function allocRunSale($sale){
        if(is_null($this->RunSaleLog)){
            $this->RunSaleLog = new RunSaleLog();
        }
        
        if(is_null($this->RunSaleCompany)){
            $this->RunSaleCompany = new RunSaleCompany();
        }
        
        
        $running = $this->find('all',[
            'conditions' => [
                'DATE(RunSale.starts_at) <=' => $sale['sale_date'],
                'DATE(RunSale.ends_at) >='  => $sale['sale_date'],
                'RunSaleTarget.product_id' => $sale['product_id'],
                'RunSaleCompany.company_id' => $sale['company_id'],
                'RunSale.draw_at' => null
            ],
            'joins' => [
                'INNER JOIN run_sale_targets AS RunSaleTarget ON RunSaleTarget.run_sale_id=RunSale.id',
                'INNER JOIN run_sale_companies AS RunSaleCompany ON RunSaleCompany.run_sale_id=RunSale.id'
            ],
        ]);


        if(is_array($running)){
            foreach($running as $run){
                if($this->RunSaleLog->allocRunSale($sale,$run) === false){
                    return false;
                }
                
                if($this->RunSaleCompany->updatePoints($sale,$run) === false){
                    return false;
                }
                
                if($this->updateScore($sale,$run) === false){
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /**
     * Actualiza el campo score del run sale afectado.
     */
    
    const SEM_UPDATE_SCORE = 'RunSale::updateScore()';
    
    private function updateScore($sale,$run){
        if($this->block(self::SEM_UPDATE_SCORE) === false){
            return false;
        }
        
        if($this->loadById($run['id']) === false){
            return false;
        }
        
        $score = (int) $this->readField('score') + (int) $sale['quantity'];
        
        if($this->saveField('score', $score) === false){
            return false;
        }
        
        return $this->unblock(self::SEM_UPDATE_SCORE);
    }
    
    /**
     * Ejecuta el sorteo de la competencia.
     */
    
    const SEM_DRAW = 'RunSale::draw()';
    
    public function draw($id, $dontBlock = false){
        if($dontBlock === false){
            if($this->block(self::SEM_DRAW) === false){
                return false;
            }
        }
        
        $sql = "
            SELECT 
                Company.*, RunSaleCompany.id
            FROM
                run_sale_companies AS RunSaleCompany
            INNER JOIN companies AS Company ON Company.id=RunSaleCompany.company_id 
            WHERE 
                RunSaleCompany.points > 0 AND
                RunSaleCompany.run_sale_id = ?
            ORDER BY -LOG(1.0 - rand()) / RunSaleCompany.points
            LIMIT 1 ";

        $this->begin();
        $db = ConnectionManager::getDataSource('default');
        $data = $db->query($sql, [ $id ]);
        
        if(isset($data[0]['RunSaleCompany']['id'])){
            $run_sale_company = $data[0]['RunSaleCompany']['id'];
            $this->id = $id;
            
            if($this->saveField('draw_at', date('Y-m-d H:i:s')) === false){
                $this->rollback();
                return false;
            }
            
            if($this->saveField('winner_id', $run_sale_company) === false){
                $this->rollback();
                return false;
            }
        }
        
        $this->commit();
        
        if($dontBlock === false){
            if($this->unblock(self::SEM_DRAW) === false){
                return false;
            }
        }
        
        
        return $data;
    }
    
    /**
     * Obtiene competencias automaticas pendientes de sorteo
     */
    
    public function pickForAutoDraw(){
        $run_sales = $this->find('all',[
            'conditions' => [
                'RunSale.score >= RunSale.goal',
                'RunSale.draw'  => 'auto',
                'RunSale.draw_at' => null
            ],
        ]);
        
        if($run_sales === false){
            return [];
        }
        
        return $run_sales;
    }
    
    /**
     * Obtiene los run sales vigentes para el frontend
     */
    
    private $RunSaleTarget;
    
    public function fetchFrontend($auth){
        $date = date('Y-m-d');
        
        $run_sales = $this->find('all',[
            'conditions' => [
                'RunSaleCompany.company_id' => $auth['data']['company_id'],
                'RunSale.draw_at' => NULL,
                'DATE(RunSale.starts_at) <=' => $date,
                'DATE(RunSale.ends_at) >='  => $date,
            ],
            'joins' => [
                'INNER JOIN run_sale_companies AS RunSaleCompany ON RunSaleCompany.run_sale_id = RunSale.id'
            ]
        ]);
        
        if(!$run_sales){
            return [];
        }
        
        $this->RunSaleTarget = new RunSaleTarget();
        
        foreach($run_sales as &$run_sale){
            $run_sale['products'] = $this->RunSaleTarget->find('all', [
                'fields' => [
                    'RunSaleTarget.product_id',
                    'Product.name',
                ],
                'joins' => [
                    'INNER JOIN products AS Product ON Product.id=RunSaleTarget.product_id'
                ],
                'conditions' => [
                    'RunSaleTarget.run_sale_id' => $run_sale['id'],
                ]
            ]);
        }
        
        return $run_sales;
    }
}