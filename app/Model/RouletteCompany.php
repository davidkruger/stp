<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Company', 'Model');

class RouletteCompany extends KlezBackendAppModel{
    private $schema = [
        'roulette_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Ruleta',
            'autocomplete' => [
                'class' => 'Roulette',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Roulette.id, Roulette.name',
                    'order' => 'Roulette.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Ruleta',
            'autocomplete-message' => 'Debe especificar Ruleta',
            'icon' => 'gears',
            'placeholder' => 'Buscar Ruleta'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ROULETTE-COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}