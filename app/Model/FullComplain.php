<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Complain', 'Model');

class FullComplain extends Complain{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Recibido',
            'orderable' => true,
            'date-message' => 'Recibido'
        ],
        'full_name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Remitente',
            'searchable' => true,
        ],
        'email' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'E-mail',
        ],
        'subject' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Asunto',
        ],
        'message' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Mensaje',
            'weight' => 8
        ],
    ];
    
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPLAIN-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}