<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ChallengeUser extends KlezBackendAppModel{
    private $schema = [
        'challenge_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Logro',
            'autocomplete' => [
                'class' => 'Challenge',
                'path' => 'Model',
                'label' => 'title',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Challenge.id, Challenge.name',
                    'order' => 'Challenge.title ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'gears',
            'placeholder' => 'Buscar Logro'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Usuario',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.name',
                    'order' => 'User.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'gears',
            'placeholder' => 'Buscar Usuario'
        ],
        'progress' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Progreso %',
            'weight' => 6,
        ],
    ];
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CHALLENGE-USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function upsert($user_id, $challenge_id) {
        $c = $this->find('first', [
            'conditions' => [
                'ChallengeUser.user_id' => $user_id,
                'ChallengeUser.challenge_id' => $challenge_id,
            ],
        ]);

        if ($c) {
            return $c;
        }

        $this->id = null;
        $data = [
            'user_id' => $user_id,
            'challenge_id' => $challenge_id,
            'progress' => 0,
        ];
        
        $this->prepareForStore($data);
        
        if($this->saveData($data)) {
            return $this->upsert($user_id, $challenge_id);
        }

        throw new Exception("Cannot alloc $user_id, $challenge_id");
    }
}