<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ProductPromotion extends KlezBackendAppModel{
    private $schema = [
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto'
        ],
        'promotion_date_alice' => [
            'type' => 'date',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Fecha de Arranque',     
            'date-message' => 'Fecha no valida',
            'required-message' => 'Debe ingresar esta Fecha'
        ],
        'promotion_date_bob' => [
            'type' => 'date',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Fecha de Fin',
            'date-message' => 'Fecha no valida',
            'required-message' => 'Debe ingresar esta Fecha'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar una cantidad valida',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Puntos Extra',
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'productless':
                return $this->productless();
            default:
                return $this->schema;
        }
    }

    private function productless(){
        $schema = $this->schema;
        $schema['product_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-PROMO-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function productOwnerPrequery($query,$authdata,$payload){
        if(isset($payload['data']['product'])){
            $id = $payload['data']['product'];
        }
        else if(isset($payload['product'])){
            $id = $payload['product'];
        }
        else{
            return false;
        }
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $alias = $this->alias;
        $query['conditions']["{$alias}.product_id"] = $id;
        $this->product = $id;
        
        return $query;
    }
    
    public function resolvExtraPoints($date,$product_id){
        $cnd = [];
        $cnd['ProductPromotion.product_id'] = $product_id;
        $cnd['ProductPromotion.promotion_date_alice <= '] = $date;
        $cnd['ProductPromotion.promotion_date_bob >= '] = $date;
        
        $r = $this->loadByConditions($cnd);
        
        if($r){
            return $this->readField('points');
        }
        
        return 0;
    }

    public $validate = [
        'promotion_date_alice' => [
            'weight_check' => [
                'rule' => [ 'weightValidator' ],
                'message' => 'Fecha de Arranque debe ser igual o menor a Fecha de Fin'
            ],
            'range_check' => [
                'rule' => [ 'rangeValidator' ],
                'message' => 'El Rango de Fechas coincide con otra Promoción de este Producto'
            ],
        ]
    ];
    
    private $product = false;
    
    public function weightValidator($data){
        $a = strtotime($this->readWritable('promotion_date_alice'));
        $b = strtotime($this->readWritable('promotion_date_bob'));
        
        return $a <= $b;
    }
    
    public function rangeValidator($data,$field=null){
        $product = $this->readField('product_id');
        
        if(!$product){
            $product = $this->product;
        }
        
        if(!$product){
            return false;
        }
        
        $a = $this->readWritable('promotion_date_alice');
        $b = $this->readWritable('promotion_date_bob');
        
        $sql = "
                
                SELECT * FROM product_promotions AS ProductPromotion
                WHERE 
                    ProductPromotion.product_id={$product}
                    AND (
                        ( 
                            ProductPromotion.promotion_date_alice <= '{$a}' AND
                            ProductPromotion.promotion_date_bob >= '{$a}'
                        )
                        
                        OR
                        
                        ( 
                            ProductPromotion.promotion_date_alice <= '{$b}' AND
                            ProductPromotion.promotion_date_bob >= '{$b}'
                        )
                        
                        OR
                        
                        ( 
                            ProductPromotion.promotion_date_alice >= '{$a}' AND
                            ProductPromotion.promotion_date_bob <= '{$b}'
                        )
                    )
        ";
                            
        if($this->id){
            $sql .= " AND ProductPromotion.id != {$this->id}";
        }
                            
        $sql .= ' LIMIT 1';
        
        if($this->query($sql)){
            return false;
        }
        
        return true;
    }
}