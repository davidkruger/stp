<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Lottery', 'Model');
App::uses('User', 'Model');

class LotteryIntent extends KlezBackendAppModel{
    private $schema = [
        'lottery_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Loteria',
            'autocomplete' => [
                'class' => 'Lottery',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Lottery.id, Lottery.name',
                    'order' => 'Lottery.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Loteria',
            'autocomplete-message' => 'Debe especificar Loteria',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Loteria'
        ],
        'number' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar numero',
            'int-message' => 'Numero no validos',
            'label' => 'Numero',
        ],
        'user_id' => [
            'searchable' => true,
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Nombre',
            'autocomplete' => [
                'class' => 'Seller',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Seller.id, Seller.full_name',
                    'order' => 'Seller.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'weight' => 4,
            'searchable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'branch_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Sucursal',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Branch.id, Branch.name',
                    'order' => 'Branch.name ASC',
                    'conditions' => [
                        'Branch.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha y Hora',
            'orderable' => true,
            'searchable' => true,
            'required' => false,
            'writable' => false,
            'readable' => true,
            'listable' => true
        ],
        'points' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar puntos',
            'int-message' => 'Puntos no validos',
            'label' => 'Puntos',
        ],
    ];
    
    public function provideSchema() {        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'LOTTERY-INTENT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function intent($auth, $lottery, $count){
        if($count < 0){
            $count = 0;
        } else if($count > 100){
            $count = 100;
        }
        
        $Lottery = new Lottery();
        $User = new User();
        $Lottery->begin();
        
        for($i = 0;$i < $count;$i++){
            try{
                #$roulette['acum'] = $roulette['acum'];
                $auth['data']['points'] = $auth['data']['points'] - $lottery['points'];
                $number = $this->pick($lottery);

                if($auth['data']['points'] < 0){
                    throw new Exception('User points cannot be negative');
                }

                $User->id = $auth['id'];
                if(!$User->saveField('points', $auth['data']['points'])){
                    throw new Exception('Cannot update users points');
                }

                $this->id = null;
                $data = [
                    'number' => $number,
                    'lottery_id' => $lottery['id'],
                    'user_id' => $auth['id'],
                    'company_id' => $auth['data']['company_id'],
                    'branch_id' => $auth['data']['branch_id'],
                    'points' => $lottery['points'],
                ];

                $this->prepareForStore($data);
                if(!$this->saveData()){
                    error_log(serialize($this->validationErrors));
                    throw new Exception('Cannot create lotteries intent');
                }

            } catch (Exception $ex) {
                error_log('Exception @ LotteryIntent::intent() - ' . $ex->getMessage());
                $Lottery->rollback();
                return false;
            }
        }

        $Lottery->commit();
        return true;
    }
    
    private function pick($lottery) {
        $id = $lottery['id'];
        $count = $this->find('count', [
            'conditions' => [
                'LotteryIntent.lottery_id' => $id
            ]
        ]);
        
        return 1 + $count;
    }
    
    public function injectLottery($query,$authdata,$payload){
        $query['conditions']['LotteryIntent.lottery_id'] = $payload['filter']['lottery_id'];
        return $query;
    }
    
}