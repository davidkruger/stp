<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ExtraPoint extends KlezBackendAppModel{
    private $schema = [
        'points' => [
            'type' => 'int',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Puntos',
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Usuario',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'email',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.email',
                    'order' => 'User.email ASC',
                    'conditions' => [
                        'User.status' => 1                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'extra' => [
            'type' => 'double',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'default' => 5,
            'double-message' => 'Debe ser un valor numerico (use punto para decimales)',
            'label' => '% de puntos x venta',
        ],
    ];
    
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => '#EXTRAS',
            'listable' => true,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function alloc($id,$p,$perc,$sale){
        $data = [];
        $data['ExtraPoint'] = [
            'user_id' => $id,
            'points' => $p,
            'extra' => $perc,
            'sale_id' => $sale,
        ];
        
        $this->id = null;
        return $this->save($data,false);
    }
}
