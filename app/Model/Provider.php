<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Provider extends KlezBackendAppModel{
    private $schema = [
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'unique' => true,
            'readable' => true,
            'searchable' => true,
            'label' => 'Nombre Proveedor',
            'unique-message' => 'Nombre de Proveedor existente',
            'required-message' => 'Debe ingresar Proveedor'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado del Proveedor',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            default:
                return $this->schema;
        }
    }

    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PROVIDER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}