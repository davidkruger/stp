<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Semaphore', 'Model');
App::uses('ExtraPoint', 'Model');

class User extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
//        'document_type' => [
//            'type' => 'options',
//            'options' => [
//                'ci' => 'CI',
//                'ruc' => 'RUC',
//                'passport' => 'Pasaporte',
//            ],
//            'required' => true,
//            'writable' => true,
//            'readable' => true,
//            'label' => 'Tipo de Documento',
//            'options-message' => 'Debe seleccionar Tipo de Documento'
//        ],
        'document' => [
            'searchable' => true,
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
//            'compound_unique' => ['document_type','document'],
            'unique' => true,
            'unique-message' => 'Documento ya registrado',
            'label' => 'Nro. de Documento',
        ],
        'password' => [
            'type' => 'text',
            'subtype' => 'password',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'hash' => [
                'algorithm' => 'md5',
                'salt' => [
                    'entropy' => '',
                    'function' => 'sprintf'
                ]
            ],
            'label' => 'Password',
        ],
        'first_name' => [
            'searchable' => true,
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre',
        ],
        'last_name' => [
            'searchable' => true,
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Apellido',
        ],
        'email' => [
            'searchable' => true,
            'type' => 'text',
            'nullable' => true,
            'subtype' => 'email',
            'required' => false,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Email',
            'unique-message' => 'Email ya ha sido registrado',
            'required-message' => 'Debe ingresar direccion de email'
        ],
        'telephone' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Telefono',
            'listable' => false
        ],
        'telephone_zimple' => [
            'type' => 'text',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Telefono Zimple',
            'listable' => false
        ],
        'birthday' => [
            'orderable' => true,
            'type' => 'date',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Fec. Nacimiento',
            'date-message' => 'Debe especificar Fec. Nacimiento'
        ],
        'picture' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'users',
                'action' => 'picture',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado de la Cuenta',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
        'role' => [
            'group' => [
                'title' => 'Datos del Perfil',
                'fields' => [
                    'company_id'
                ],
                'dep' => [
                    'vendedor' => [ 
                        'company_id',
                        'branch_id',
                        'points_rate',
                    ],
                    'encargado' => [ 
                        'company_id',
                        'branch_id',
                        'branch_id2',
                        'branch_id3',
                        'extra'
                    ],
                    'responsable' => [ 
                        'company_id',
//                        'branch_id',
                        'extra'
                    ],
                ]
            ],
            'label' => 'Perfil del Usuario',
            'type' => 'options',
            'options' => [
//                'romis' => 'Romis',
                'samsung' => 'Samsung',
                'lamoderna' => 'LaModerna',
                'responsable' => 'Responsable',
                'vendedor' => 'Vendedor',
                'encargado' => 'Encargado',
            ],
            'listable' => false,
            'writable' => true,
            'readable' => true,
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'weight' => 4,
            'searchable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'type' => 'LEFT',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'branch_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'searchable' => true,
            'label' => 'Sucursal',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'type' => 'LEFT',
                'query' => [
                    'fields' => 'Branch.id, Branch.name',
                    'order' => 'Branch.name ASC',
                    'conditions' => [
                        'Branch.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'branch_id2' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'searchable' => true,
            'label' => 'Sucursal 2',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch2.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'alias' => 'Branch2',
                'identifier' => 'id',
                'type' => 'LEFT',
                'query' => [
                    'fields' => 'Branch2.id, Branch2.name',
                    'order' => 'Branch2.name ASC',
                    'conditions' => [
                        'Branch2.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'branch_id3' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'searchable' => true,
            'label' => 'Sucursal 3',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch3.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'alias' => 'Branch3',
                'type' => 'LEFT',
                'query' => [
                    'fields' => 'Branch3.id, Branch3.name',
                    'order' => 'Branch3.name ASC',
                    'conditions' => [
                        'Branch3.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'points_rate' => [
            'type' => 'double',
            'orderable' => true,
            'required' => false,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Porcentaje de conversión de puntos',
        ],
        'points' => [
            'type' => 'int',
            'orderable' => true,
            'required' => false,
            'writable' => false,
            'readable' => false,
            'listable' => false,
            'label' => 'Puntos',
        ],
        'extra' => [
            'type' => 'double',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'default' => 5,
            'double-message' => 'Debe ser un valor numerico (use punto para decimales)',
            'label' => '% de puntos x venta',
        ],
//        'created' => [
//            'type' => 'datetime',
//            'required' => false,
//            'writable' => false,
//            'readable' => true,
//            'listable' => false,
//            'label' => 'CREATED',
//        ],
    ];
    
    public $validate = [
        'branch_id' => [
            'rule' => [
                'branchCompanyCheck'
            ]
        ]
    ];
    
    public function branchCompanyCheck(){
        if(preg_match('/^xlsx/',$this->getShape())) {
            return true;
        }

        $role = $this->readField('role');
        $roles = [ 'responsable', 'romis', 'samsung', 'lamoderna' ];
        
        if(in_array($role, $roles)){
            return true;
        }
        
        App::uses('Branch','Model');
        $Branch = new Branch();
        $branch = $this->readField('branch_id');
        $company = $this->readField('company_id');
        
        if(!$company){
            return true;
        }
        
        $cnd = [];
        $cnd['Branch.id'] = $branch;
        $cnd['Branch.company_id'] = $company;
        
        if($Branch->loadByConditions($cnd)){
            return true;
        }
            
        $this->writeField('branch_id', null);
        return 'Sucursal no valida';
    }
    
    private $massiveSchema = [
        'document' => [
            'type' => 'file',
            'subtype' => 'xlsx',
            'weight' => 12,
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => false,
            'label' => 'Carga Masiva',
            'mimes' => [
//                'application/csv',
//                'application/lotus123',
//                'application/msexcel',
//                'application/vnd.lotus-1-2-3',
//                'application/vnd.ms-excel',
//                'application/vnd.ms-excel [official]',
//                'application/vnd.ms-works',
//                'application/vnd.msexcel',
//                'application/wks',
//                'application/x-dos_ms_excel',
//                'application/x-excel',
//                'application/x-lotus123',
//                'application/x-msexcel',
//                'application/x-msworks',
//                'application/x-wks',
//                'application/xlc',
//                'application/xlt',
//                'text/anytext',
//                'text/comma-separated-values',
//                'text/csv'
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ],
            'url' => [
                'controller' => 'companies',
                'action' => 'document',
                'plugin' => null
            ],
            'placeholder' => 'Seleccione archivo XLSX',
            'xlsx-message' => 'Debe ser un documento valido en formato XLSX',
            'file-message' => 'Debe ser un documento valido en formato XLSX'
        ],
    ];
    
    public function massiveRole(){
        return 'vendedor';
    }
        
    public function provideSchema() {
        $schema = $this->fixedSalt();
                
        switch($this->getShape()){
            case 'login':
                return $this->loginSchema();
//            case 'romis':
            case 'samsung':
            case 'lamoderna':
                return $this->adminSchema();
            case 'readable':
                return $this->readableSchema();
            case 'responsibles':
                return $this->responsiblesSchema();
            case 'sellers':
                return $this->sellersSchema();
            case 'writable':
                return $this->writableSchema();
            case 'disabled':
                return $this->disabledSchema();
            case 'profile':
                return $this->profileSchema();
            case 'frontendProfile':
                return $this->frontendProfile();
            case 'massive':
                return $this->massiveSchema;
            case 'xlsxFilter':
                return $this->xlsxFilterSchema($schema);
            case 'xlsx':
                return $this->xlsxSchema($schema);
            default:
                return $schema;
        }
    }

    private function xlsxSchema($schema) {
        $schema['company_id']['listable'] = true;
        $schema['branch_id']['listable'] = true;
        $schema['role']['listable'] = true;
        unset($schema['company_id']['required']);
        unset($schema['branch_id']['required']);
        unset($schema['role']['group']);
        return $schema;
    }

    private function xlsxFilterSchema($schema) {
        unset($schema['company_id']['required']);
        unset($schema['branch_id']['required']);
        unset($schema['role']['group']);

        return [
            'role' => $schema['role'],
            'company_id' => $schema['company_id'],
            'branch_id' => $schema['branch_id'],
            'status' => $schema['status'],
        ];
    }

    public function xlsxFilter($query,$authdata,$payload){
        $status = $payload['filter']['status'] ? 1 : 0;
        $role = '' . @$payload['filter']['role'];
        $company = (int) @$payload['filter']['company_id'];
        $branch = (int) @$payload['filter']['branch_id'];
        
        if($company > 0){
            $query['conditions']['User.company_id'] = $company;
        }
        
        if($branch > 0){
            $query['conditions']['User.branch_id'] = $branch;
        }
        

        $query['conditions']['User.status'] = $status;
        $query['conditions']['User.role'] = $role;

        return $query;
    }
    
    private function frontendProfile(){
        $schema = $this->fixedSalt();
        unset($schema['status']);
        unset($schema['role']);
        unset($schema['company_id']);
        unset($schema['extra']);
        unset($schema['points']);
        unset($schema['branch_id']);
        
        return $schema;
    }
    
    private $pendingPoints = [
        'type' => 'int',
        'orderable' => true,
        'required' => false,
        'writable' => false,
        'readable' => true,
        'listable' => true,
        'label' => 'Puntos Pendientes Canje',
    ];
    
    private function sellersSchema(){
        $this->useTable = 'salesmen';
        $schema = $this->schema;
        $schema['status']['listable'] = false;
        $schema['company_id']['listable'] = true;
        $schema['branch_id']['listable'] = true;
        $schema['email']['listable'] = false;
        $schema['points']['listable'] = true;
        $schema['points']['readable'] = true;
        $schema['extra']['readable'] = false;
        $schema['points']['group']['dep']['vendedor'][] = 'points';
        $schema['points']['group']['fields'][] = 'points';
        $schema['pending_points'] = $this->pendingPoints;
        return $schema;
    }
    
    private function responsiblesSchema(){
        $schema = $this->schema;
        $schema['status']['listable'] = false;
        $schema['company_id']['listable'] = true;
        $schema['extra']['readable'] = true;
        $schema['extra']['listable'] = true;
        $schema['points']['listable'] = true;
        $schema['points']['readable'] = true;
        return $schema;
    }
    
    private function disabledSchema(){
        $schema = $this->schema;
        $schema['status']['listable'] = false;
        $schema['role']['listable'] = true;
        $schema['extra']['readable'] = false;
        return $schema;
    }
    
    private function adminSchema(){
        $schema = $this->schema;
        $schema['status']['listable'] = false;
        $schema['extra']['readable'] = false;
        $schema['role']['group']['dep']['vendedor'][] = 'points';
        $schema['role']['group']['fields'][] = 'points';
        return $schema;
    }
        
    private function readableSchema(){
        $schema = $this->schema;
        $schema['status']['listable'] = false;
        $schema['points']['listable'] = true;
        $schema['points']['readable'] = true;
        $schema['extra']['readable'] = false;
        $schema['role']['group']['dep']['vendedor'][] = 'points';
        $schema['role']['group']['fields'][] = 'points';
        return $schema;
    }
    
    private function writableSchema(){
        $schema = $this->fixedSalt();
        unset($schema['points']);
        return $schema;
    }
    
    private function profileSchema(){
        $schema = $this->fixedSalt();
        unset($schema['role']);
        unset($schema['company_id']);
        unset($schema['points']);
        unset($schema['status']);
        return $schema;
    }
    
    public function massiveDefaults(&$data,$authdata){
        $this->writeField('status', true);
        $data['status'] = true;
        $data['extra'] = 5;
        return true;
    }
    
    public function fetchResponsibles($company){
        $cnd = [];
        $cnd['User.role'] = 'responsable';
        $cnd['User.company_id'] = $company;
        
        return $this->find('all', [
            'conditions' => $cnd
        ], false) ?: [];
    }
    
    public function fetchEncargados($branch){
        $cnd = [];
        $cnd['User.role'] = 'encargado';
        $cnd['OR']['User.branch_id'] = $branch;
        $cnd['OR']['User.branch_id2'] = $branch;
        $cnd['OR']['User.branch_id3'] = $branch;
        
        return $this->find('all', [
            'conditions' => $cnd
        ], false) ?: [];
    }
    
    private $ExtraPoint;
    
    public function semUpdateResponsiblesPoints($sale,$responsibles,$points){
        if($points <= 0){
            return true;
        }
        
        $this->ExtraPoint = new ExtraPoint();
        
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
                
        if($semaphore){
            sem_acquire($semaphore);
            
            foreach($responsibles as $responsible){
                $id = $responsible['id'];
                
                if($this->loadById($id)){
                    $current = $this->readField('points');
                    $perc = (double) $this->readField('extra');
                    $extra = ceil($points * $perc / 100);
                    $p =  $extra + $current;
                    
                    $this->ExtraPoint->alloc($id,$extra,$perc,$sale);
                    
//                    error_log('==============================');
//                    error_log("user=$id");
//                    error_log("curr=$current");
//                    error_log("points=$points");
//                    error_log("perc=$perc");
//                    error_log("new=$p");
//                    error_log('==============================');

                    $this->id = $id;
                    $success = (bool) $this->saveField('points', $p);
                    
                    if(!$success){
                        break;
                    }
                }
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
        }
        
        sem_release($semaphore);
        return $success;
    }
    
    public function semUpdateExtraPoints($sale,$id,$points){
        if($points <= 0){
            return true;
        }
        $this->ExtraPoint = new ExtraPoint();
        
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
                
        if($semaphore){
            sem_acquire($semaphore);
            
            if($this->loadById($id)){
                $current = $this->readField('points');
                $perc = (double) $this->readField('extra');
                $extra = ceil($points * $perc / 100);
                $p =  $extra + $current;
                
                $this->id = $id;
                $this->ExtraPoint->alloc($id,$extra,$perc,$sale);
                $success = (bool) $this->saveField('points', $p);
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
        }
        
        sem_release($semaphore);
        return $success;
    }
    
    public function semUpdatePoints($id,$points){
        if($points <= 0){
            return true;
        }
        
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
                
        if($semaphore){
            sem_acquire($semaphore);
            
            if($this->loadById($id)){
                $current = $this->readField('points');
                $rate = $this->readField('points_rate');
                $gains = round($rate * $points / 100);
                $p = $gains + $current;
                $this->id = $id;
                error_log("Giving $gains points to $id, sale=$points at rate=$rate former=$current new=$p");
                $success = (bool) $this->saveField('points', $p);
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
        }
        
        sem_release($semaphore);
        return $success;
    }
    
    public function setPoints($id,$points){
        $this->id = $id;
        return $this->saveField('points', $points);
    }
    
    private function fixedSalt(){
        $schema = $this->schema;
        $schema['password']['hash']['salt']['entropy'] = Configure::read('User.password.salt');
        return $schema;
    }
    
    private function loginSchema(){
        $loginSchema = [];
        $schema = $this->fixedSalt();
        $fields = [ 'email','password' ];
        
        foreach($fields as $field){
            $loginSchema[$field] = $schema[$field];
        }
        
        return $loginSchema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function filterFrontend($data,$authdata){
        $dataCompany = (int) $data['company_id'];
        $authCompany = (int) $authdata['data']['company_id'];
        
        if($dataCompany !== $authCompany){
            return false;
        }
        
        $dataRole = $data['role'];
        
        if($dataRole !== 'vendedor'){
            return false;
        }
        
        return true;
    }
}