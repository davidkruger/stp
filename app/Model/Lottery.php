<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('LotteryIntent', 'Model');
App::uses('User', 'Model');
App::uses('Company', 'Model');
App::uses('Branch', 'Model');

class Lottery extends KlezBackendAppModel{
    const LOTTERY_ZERO_COMPANIES = 'No ha asignado ninguna Empresa';

    public $virtualFields = [
        'tickets' => 'COALESCE((select count(*) as tickets from lottery_intents as i where i.lottery_id=Lottery.id group by i.lottery_id), 0)'
    ];
    
    private $schema = [
        'active' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Activa?',
            'boolean' => [
                0 => 'No',
                1 => 'Si'
            ],
            'default' => 0,
        ],
        'updated' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Versión',
        ],
        'tickets' => [
            'type' => 'int',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Tickets Vendidos',
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'like' => '%%%s%%',
            'label' => 'Nombre Loteria',
        ],
        'starts_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Inicio',
        ],
        'ends_at' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Final',
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Puntos',
        ],
        'prize' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Premio',
        ],
        'prize_photo' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto del Premio',
            'listable' => false,
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'lotteries',
                'action' => 'prize_photo',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'draw' => [
            'type' => 'options',
            'options' => [
                'physical' => 'Físico',
                'electronic' => 'Electrónico'
            ],
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Sorteo',
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Participantes',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'LotteryCompany',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'lottery_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas asignadas',
            'placeholder' => 'Empresas Participantes'
        ],
        'winner_id' => [
            'searchable' => true,
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Ganador',
            'autocomplete' => [
                'class' => 'Seller',
                'path' => 'Model',
                'type' => 'LEFT',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Seller.id, Seller.full_name',
                    'order' => 'Seller.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'null' => '<i>No sorteado</i>',
            'required-message' => 'Debe especificar Ganador',
            'autocomplete-message' => 'Debe especificar Ganador',
            'icon' => 'account',
            'placeholder' => 'Buscar Ganador'
        ],
    ];
    
    private $xlsxSchema = [
        'run_sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Loteria',
            'autocomplete' => [
                'class' => 'Lottery',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Lottery.id, Lottery.name',
                    'order' => 'Lottery.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Loteria',
            'autocomplete-message' => 'Debe especificar Loteria',
            'icon' => 'gears',
            'placeholder' => 'Buscar Loteria'
        ],
    ];
    
    public $validate = [
        'points' => [
            'range' => [
                'rule' => [ 'pointsRangeValidator' ],
                'message' => 'Debe ser mayor a 0'
            ]
        ]
    ];
    
    public function pointsRangeValidator(){
        $points = (int) $this->readField('points');
        
        return $points > 0;
    }
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxFilter':
                return $this->xlsxSchema;
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'LOTTERY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function habtmValidator($data,$field){
        $array = $data[$field];
        
        foreach($array as $raw){
            if(strlen($raw) > 0){
                return true;
            }
        }
        
        return $this->getHabtmMessage($field);
    }
    
    private function getHabtmMessage($field){
        switch($field){
            case 'products':
                return self::LOTTERY_ZERO_COMPANIES;
        }
        
        return false;
    }
    
    /**
     * Ejecuta el sorteo de la loteria.
     */
    
    const SEM_DRAW = 'Lottery::draw()';
    private $LotteryIntent;
    
    public function draw(){
        $this->LotteryIntent = new LotteryIntent();
        $sql = "
            SELECT 
                User.*, LotteryIntent.*
            FROM
                lottery_intents AS LotteryIntent
            INNER JOIN users AS User ON User.id=LotteryIntent.user_id 
            WHERE LotteryIntent.lottery_id = ?
            ORDER BY rand()
            LIMIT 1 ";

        $this->begin();
        $db = ConnectionManager::getDataSource('default');
        $data = $db->query($sql, [ $this->id ]);
        
        if(isset($data[0]['User']['id'])){
            $user = $data[0]['User']['id'];
            $intent = $data[0]['LotteryIntent']['id'];
            $this->LotteryIntent->id = $intent;
            
            if($this->saveField('winner_id', $user) === false){
                $this->rollback();
                return false;
            } else {
                $this->writeField('winner_id', $user);
            }
            
            if($this->LotteryIntent->saveField('winner', 1) === false){
                $this->rollback();
                return false;
            }
        }
        
        $this->commit();
        return $data;
    }
    
    public function winner() {
        $winner = $this->readField('winner_id');
        $User = new User();
        
        if($User->loadById($winner)){
            $Company = new Company();
            $Company->loadById($User->readField('company_id'));
            
            $Branch = new Branch();
            $Branch->loadById($User->readField('branch_id'));
            
            $data = $User->getData();
            $data['company'] = $Company->getData();
            $data['branch'] = $Branch->getData();
            return $data;
        }
        
        return null;
    }
    
    /**
     * Obtiene las loterias vigentes para el frontend
     */
    
    public function fetchFrontend($auth, $id = null){
        $date = date('Y-m-d H:i:s');
        
        $conditions = [
            'LotteryCompany.company_id' => $auth['data']['company_id'],
            '(Lottery.starts_at) <=' => $date,
            '(Lottery.ends_at) >='  => $date,
            'Lottery.active'  => true,
        ];
        
        if ($id) {
            $conditions['Lottery.id'] = $id;
        }
        
        $lotteries = $this->find('all',[
            'conditions' => $conditions,
            'joins' => [
                'INNER JOIN lottery_companies AS LotteryCompany ON LotteryCompany.lottery_id = Lottery.id'
            ]
        ], false);
        
        if(!$lotteries){
            return [];
        }
        
        if ($id) {
            return array_shift($lotteries);
        }
        
        return $lotteries;
    }
    
    public function activeLottery($data, $authdata) {
        if($data['active']) {
            $sql = 'UPDATE lotteries SET active = 0';
            $this->query($sql);
        }
        
        return true;
    }
}