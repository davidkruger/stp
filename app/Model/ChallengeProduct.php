<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ChallengeProduct extends KlezBackendAppModel{
    private $schema = [
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        'Product.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto',
            'compound_unique'=> [ 'challenge_id' ],
            'compound_unique-message' => 'Producto ya ha sido asignado a este Logro',
        ],
        'challenge_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Logro',
            'autocomplete' => [
                'class' => 'Challenge',
                'path' => 'Model',
                'label' => 'title',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Challenge.id, Challenge.title',
                    'order' => 'Challenge.title ASC',
                    'conditions' => [
                        'Challenge.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Logro'
        ],
    ];
    
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'challengeless':
                return $this->challengeless();
            default:
                return $this->schema;
        }
    }

    private function challengeless(){
        $schema = $this->schema;
        $schema['challenge_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CHALLENGE-PRODUCT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function challengeOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['challenge'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.challenge_id"] = $id;
        
        return $query;
    } 
}