<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Division','Model');

class Salesman extends KlezBackendAppModel{
    public $useTable = 'salesmen';
    
    private $schema = [
        'full_name' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Nombre Completo',
        ],
		'points_rate' => [
            'type' => 'double',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Porcentaje de conversión de puntos',
        ],
    ];
    
    private $top10schema = [
        'full_name' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Vendedor',
        ],
        'acum' => [
            'type' => 'int',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Acumulado',
        ],
    ];
        
    public function provideSchema() {
        switch ($this->getShape()){
            case 'top10': return $this->top10schema;
        }
        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    private $Division;
    
    public function divisionlastmonth($payload){
        $this->Division = new Division();
        $report = [];       
        $salesCnd = 'YEAR(Sale.sale_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Sale.sale_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)';

        
        $divisions = $this->Division->find('all', [
           'conditions' => [
               'Division.status' => true
           ] 
        ]);
        
        foreach($divisions as $division){
            $cnd = [];
            $cnd['Salesman.status'] = 1;
            $sales = "SELECT COALESCE(SUM(Sale.points),0) FROM sales AS Sale INNER JOIN products AS Product ON Product.id=Sale.product_id INNER JOIN divisions AS Division ON Division.id=Product.division_id WHERE {$salesCnd} AND Sale.salesman_id=Salesman.id AND Sale.approval='samsung' AND Sale.rejected = 0 AND Division.id={$division['id']}";

            $fields = [];
            $fields[] = 'Salesman.id';
            $fields[] = 'Salesman.company_id';
            $fields[] = 'Salesman.full_name';
            $fields[] = "($sales) AS Salesman__acum";

            $order = [];
            $order[] = 'Salesman__acum DESC';

            $query = [
                'fields' => $fields,
                'conditions' => $cnd,
                'order' => $order,
                'limit' => 10
            ];

            $report[] = [
                'division' => $division,
                'data' => $this->find('all', $query, false)
            ];
        }
        
        return $report;
    }
    
    public function division($payload){
        $this->Division = new Division();
        $report = [];
        
        $divisions = $this->Division->find('all', [
           'conditions' => [
               'Division.status' => true
           ] 
        ]);
        
        foreach($divisions as $division){
            $cnd = [];
            $cnd['Salesman.status'] = 1;
            $sales = "SELECT COALESCE(SUM(Sale.points),0) FROM sales AS Sale INNER JOIN products AS Product ON Product.id=Sale.product_id INNER JOIN divisions AS Division ON Division.id=Product.division_id WHERE Sale.salesman_id=Salesman.id AND Sale.approval='samsung' AND Sale.rejected = 0 AND Division.id={$division['id']}";

            $fields = [];
            $fields[] = 'Salesman.id';
            $fields[] = 'Salesman.company_id';
            $fields[] = 'Salesman.full_name';
            $fields[] = "($sales) AS Salesman__acum";

            $order = [];
            $order[] = 'Salesman__acum DESC';

            $query = [
                'fields' => $fields,
                'conditions' => $cnd,
                'order' => $order,
                'limit' => 10
            ];

            $report[] = [
                'division' => $division,
                'data' => $this->find('all', $query, false)
            ];
        }
        
        return $report;
    }
    
    public function top10lastmonth($payload){
        $salesCnd = 'YEAR(Sale.sale_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Sale.sale_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)';
        $redeemsCnd = 'YEAR(Redeem.created) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Redeem.created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)';

        $cnd = [];
        $cnd['Salesman.status'] = 1;
        $sales = "SELECT COALESCE(SUM(Sale.points),0) FROM sales AS Sale WHERE {$salesCnd} AND Sale.salesman_id=Salesman.id AND Sale.approval='samsung' AND Sale.rejected = 0";
        $redeems = "SELECT COALESCE(SUM(Redeem.points),0) FROM redeems AS Redeem WHERE {$redeemsCnd} AND Redeem.user_id=Salesman.id AND Redeem.status IN ( 'aprobado', 'pendiente' )";

        $fields = [];
        $fields[] = 'Salesman.id';
        $fields[] = 'Salesman.company_id';
        $fields[] = 'Salesman.full_name';
        $fields[] = "($sales) AS Salesman__acum";
        $fields[] = "($redeems) AS Salesman__redeemed";
        
        $order = [];
        $order[] = 'Salesman__acum DESC';
        
        $query = [
            'fields' => $fields,
            'conditions' => $cnd,
            'order' => $order,
            'limit' => 10
        ];
        
        return $this->find('all', $query, false);
    }
    
    public function top10general($payload){
        $cnd = [];
        $cnd['Salesman.status'] = 1;
        $sales = "SELECT COALESCE(SUM(Sale.points),0) FROM sales AS Sale WHERE Sale.salesman_id=Salesman.id AND Sale.approval='samsung' AND Sale.rejected = 0";
        $redeems = "SELECT COALESCE(SUM(Redeem.points),0) FROM redeems AS Redeem WHERE Redeem.user_id=Salesman.id AND Redeem.status IN ( 'aprobado', 'pendiente' )";

        $fields = [];
        $fields[] = 'Salesman.id';
        $fields[] = 'Salesman.company_id';
        $fields[] = 'Salesman.full_name';
        $fields[] = "($sales) AS Salesman__acum";
        $fields[] = "($redeems) AS Salesman__redeemed";
        
        $order = [];
        $order[] = 'Salesman__acum DESC';
        
        $query = [
            'fields' => $fields,
            'conditions' => $cnd,
            'order' => $order,
            'limit' => 10
        ];
        
        return $this->find('all', $query, false);
    }
    
    public function summary($payload){
        $cnd = [];
        $cnd['Salesman.status'] = 1;
        
        if(isset($payload['company_id']) && $payload['company_id']){
            $cnd['Salesman.company_id'] = $payload['company_id'];
        }
        
        $salesCnd = '1=1';
        
        if(isset($payload['product_id']) && $payload['product_id']){
            $salesCnd = 'Sale.product_id = ' . $payload['product_id'];
        }
        
        $sales = "SELECT COALESCE(SUM(Sale.points),0) FROM sales AS Sale WHERE $salesCnd AND Sale.salesman_id=Salesman.id AND Sale.sale_date >= '{$payload['a']}' and Sale.sale_date <= '{$payload['b']}' AND Sale.approval='samsung' AND Sale.rejected = 0";
        $extra = "SELECT COALESCE(SUM(ExtraPoint.points),0) FROM extra_points AS ExtraPoint INNER JOIN sales AS Sale ON Sale.id=ExtraPoint.sale_id  WHERE $salesCnd AND ExtraPoint.user_id=Salesman.id AND Sale.sale_date >= '{$payload['a']}' and Sale.sale_date <= '{$payload['b']}' AND Sale.approval='samsung' AND Sale.rejected = 0";

        $fields = [];
        $fields[] = 'Salesman.id';
        $fields[] = 'Salesman.company_id';
        $fields[] = 'Salesman.full_name';
        $fields[] = "($sales) + ($extra) AS Salesman__acum";
        
        $order = [];
        $order[] = 'Salesman.company_id ASC';
        $order[] = 'Salesman__acum DESC';
        
        $query = [
            'fields' => $fields,
            'conditions' => $cnd,
//            'joins' => $joins,
//            'group' => $group,
            'order' => $order
        ];
        
        $data = $this->find('all', $query, false);
        #error_log($this->getLastQuery());
        return $data;
    }
    
    public function pickToDisable(){
        $sql = 'select * from (select salesmen.id, max(COALESCE(sales.created,salesmen.created)) AS maxcreated from salesmen left join sales on sales.salesman_id=salesmen.id where salesmen.status=1 and salesmen.role="vendedor" group by salesmen.id) as Salesman where maxcreated  < DATE_SUB(NOW(),INTERVAL 1 YEAR) or maxcreated is null';
        $data = $this->query($sql);
        return $data;
    }
	
	
	public function pointsRate($id_salesmen){
		
		$points_rate = 0;
		
		$cnd = [
            'id' => $id_salesmen,
        ];
		
		$sales = $this->find('all_pipe',[
            'conditions' => $cnd
        ]);
		
		foreach ($sales as $key => $value) {
            $points_rate = (int)$value['Salesman']['points_rate'];
		} 

        return $points_rate;
    }
	
	

}