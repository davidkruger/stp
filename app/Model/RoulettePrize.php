<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Roulette', 'Model');

class RoulettePrize extends KlezBackendAppModel{
    private $schema = [
        'roulette_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Ruleta',
            'autocomplete' => [
                'class' => 'Roulette',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Roulette.id, Roulette.name',
                    'order' => 'Roulette.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Ruleta',
            'autocomplete-message' => 'Debe especificar Ruleta',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Ruleta'
        ],
        'prize' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'like' => '%%%s%%',
            'required-message' => 'Debe ingresar premio',
            'label' => 'Premio',
        ],
        'points' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar puntos',
            'int-message' => 'Puntos no validos',
            'label' => 'Puntos',
            'hint' => 'Especifique cero puntos para ignorar stock al girar la ruleta'
        ],
        'stock' => [
            'type' => 'int',
            'orderable' => true,
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe especificar stock',
            'int-message' => 'Stock no valido',
            'label' => 'Stock',
        ],
        'redeemable' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Canjeable?',
            'boolean' => [
                0 => 'No',
                1 => 'Si'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'rouletteless':
                return $this->rouletteless();
            default:
                return $this->schema;
        }
    }

    private function rouletteless(){
        $schema = $this->schema;
        $schema['roulette_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ROULETTE-PRIZE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function rouletteOwnerPrequery($query,$authdata,$payload){
        if(isset($payload['data']['roulette'])){
            $id = $payload['data']['roulette'];
        }
        else if(isset($payload['roulette'])){
            $id = $payload['roulette'];
        }
        else{
            return false;
        }
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $alias = $this->alias;
        $query['conditions']["{$alias}.roulette_id"] = $id;
        $this->roulette = $id;
        
        return $query;
    }
    
    public function updateRouletteTimestamp($data, $authdata) {
        $Prize = new RoulettePrize();
        $id = null;
        
        if($Prize->loadById($this->id)){
            $id = $Prize->readField('roulette_id');
        } else if(isset($data['roulette_id'])){
            $id = $data['roulette_id'];
        }
        
        if(isset($id)){
            $sql = "UPDATE roulettes SET updated=NOW() WHERE id='{$id}'";            
            $this->query($sql);
            return true;
        }
        
        return false;
    }
}