<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Group extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre del Grupo',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre',
            'searchable' => true
        ],
        'companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Restringidas',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'DisabledGroup',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'group_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas Restringidas',
            'placeholder' => 'Empresas Restringidas'
        ],
        
        'allowed_companies' => [
            'type' => 'foreign',
            'subtype' => 'habtm',
            'listable' => false,
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'maxlength' => 40,
            'label' => 'Empresas Permitidas',
            'habtm' => [
                'foreign' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'label' => 'name',
                    'counter' => null,
                    'identifier' => 'id',
                ],
                'intermediate' => [
                    'class' => 'AllowedCompanies',
                    'path' => 'Model',
                    'identifier' => [
                        'me' => 'group_id',
                        'foreign' => 'company_id'
                    ]
                ],
            ],
            'blank' => 'No tiene Empresas Permitidas',
            'placeholder' => 'Empresas Permitidas'
        ],
         
        'responsible_deny' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Responsables',
            'boolean' => [
                0 => 'No restringir',
                1 => 'Restringido'
            ],
            'default' => 1,
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado del Grupo',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {

        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'GROUP-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}