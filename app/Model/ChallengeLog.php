<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ChallengeLog extends KlezBackendAppModel{
    private $schema = [
        'challenge_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Desafio',
            'autocomplete' => [
                'class' => 'Challenge',
                'path' => 'Model',
                'label' => 'title',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Challenge.id, Challenge.title',
                    'order' => 'Challenge.title ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Desafio',
            'autocomplete-message' => 'Debe especificar Desafio',
            'icon' => 'gears',
            'placeholder' => 'Buscar Desafio'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Usuario',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.name',
                    'order' => 'User.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'gears',
            'placeholder' => 'Buscar Usuario'
        ],
        'sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Venta',
            'autocomplete' => [
                'class' => 'Sale',
                'path' => 'Model',
                'label' => 'id',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Sale.id',
                    'order' => 'Sale.id ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Venta',
            'autocomplete-message' => 'Debe especificar Venta',
            'icon' => 'gears',
            'placeholder' => 'Buscar Venta'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'id',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id',
                    'order' => 'Product.id ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'gears',
            'placeholder' => 'Buscar Producto'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Cantidad',
        ]
    ];
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CHALLENGE-LOG-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function append($user_id, $challenge_id, $product_id, $sale_id, $quantity) {
        $this->id = null;
        $data = [
            'user_id'      => $user_id,
            'challenge_id' => $challenge_id,
            'product_id'   => $product_id,
            'sale_id'      => $sale_id,
            'quantity'     => $quantity,
        ];
        
        $this->prepareForStore($data);
        
        return $this->saveData($data);
    }
}