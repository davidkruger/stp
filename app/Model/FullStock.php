<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class FullStock extends KlezBackendAppModel{
    public $useTable = 'full_stock';
    
    private $schema = [
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'searchable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'FullProduct',
                'path' => 'Model',
                'label' => 'product',
                'identifier' => 'id',
                'full' => true,
                'query' => [
                    'fields' => 'FullProduct.id, FullProduct.product',
                    'order' => 'FullProduct.product ASC',
                    'conditions' => [
                        'FullProduct.status' => 1,
                    ]
                ]   
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-cart',
            'placeholder' => 'Buscar Producto'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Cantidad',
            'required-message' => 'Debe ingresar Cantidad',
            'int-message' => 'Debe ingresar una Cantidad valida',
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            default:
                return $this->schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'STOCK-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}