<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Company', 'Model');

class RunSaleCompany extends KlezBackendAppModel{
    private $schema = [
        'run_sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Competencia',
            'autocomplete' => [
                'class' => 'RunSale',
                'path' => 'Model',
                'label' => 'description',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'RunSale.id, RunSale.description',
                    'order' => 'RunSale.description ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Competencia',
            'autocomplete-message' => 'Debe especificar Competencia',
            'icon' => 'gears',
            'placeholder' => 'Buscar Competencia'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
        'status' => [
            'type' => 'boolean',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Estado',
            'weight' => 6,
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Puntos',
            'weight' => 6,
            'default' => 0
        ],
    ];
    
    private $xlsxRunSaleSchema = [
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
        'id' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Frecuencia',
        ],
    ];
    
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxRunSale':
                return $this->xlsxRunSaleSchema();
            default:
                return $schema;
        }
    }
    
    private function xlsxRunSaleSchema(){
        $schema = $this->xlsxRunSaleSchema;
        $this->virtualFields['id'] = 'COUNT(Sale.id)';
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'RUN-SALE-COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function injectRunSale($query,$authdata,$payload){
        $query['conditions']['RunSaleLog.run_sale_id'] = $payload['filter']['run_sale_id'];
        return $query;
    }
    
    /**
     * Actualiza los puntos del RunSaleCompany, bloqueo con semaforo
     */
    
    const SEM_UPDATE_POINTS = 'RunSaleCompany::updatePoints()';
    
    public function updatePoints($sale,$run){
        if($this->block(self::SEM_UPDATE_POINTS) === false){
            return false;
        }
        
        $cnd = [];
        $cnd['RunSaleCompany.run_sale_id'] = $run['id'];
        $cnd['RunSaleCompany.company_id'] = $sale['company_id'];
        
        if($this->loadByConditions($cnd) === false){
            return false;
        }
        
        $points = (int) $this->readField('points') + (int) $sale['quantity'];
        
        if($this->saveField('points', $points) === false){
            return false;
        }
        
        return $this->unblock(self::SEM_UPDATE_POINTS);
    }
    
    /**
     * Obtiene el ganador (empresa) del ID especificado.
     */
    private $Company;
    
    public function getCompany($id){
        if($this->loadById($id)){
            $this->Company = new Company();
            $company = $this->readField('company_id');
            
            if($this->Company->loadById($company)){
                return $this->Company->getData();
            }
        }
        
        return null;
    }
}