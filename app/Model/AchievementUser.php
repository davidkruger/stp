<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class AchievementUser extends KlezBackendAppModel{
    private $schema = [
        'achievement_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Logro',
            'autocomplete' => [
                'class' => 'Achievement',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Achievement.id, Achievement.name',
                    'order' => 'Achievement.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'gears',
            'placeholder' => 'Buscar Logro'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Usuario',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.name',
                    'order' => 'User.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'gears',
            'placeholder' => 'Buscar Usuario'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Puntos',
            'weight' => 6,
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ACHIEVEMENT-USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function upsert($user_id, $achievement_id) {
        $c = $this->find('first', [
            'conditions' => [
                'AchievementUser.user_id' => $user_id,
                'AchievementUser.achievement_id' => $achievement_id,
            ],
        ]);

        if ($c) {
            return $c;
        }

        $this->id = null;
        $data = [
            'user_id' => $user_id,
            'achievement_id' => $achievement_id,
            'points' => 0,
        ];
        
        $this->prepareForStore($data);
        
        if($this->saveData($data)) {
            return $this->upsert($user_id, $achievement_id);
        }

        throw new Exception("Cannot alloc $user_id, $achievement_id");
    }
}