<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class AchievementLog extends KlezBackendAppModel{
    private $schema = [
        'achievement_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Logro',
            'autocomplete' => [
                'class' => 'Achievement',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Achievement.id, Achievement.name',
                    'order' => 'Achievement.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'gears',
            'placeholder' => 'Buscar Logro'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Usuario',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.name',
                    'order' => 'User.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'gears',
            'placeholder' => 'Buscar Usuario'
        ],
        'sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Venta',
            'autocomplete' => [
                'class' => 'Sale',
                'path' => 'Model',
                'label' => 'id',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Sale.id',
                    'order' => 'Sale.id ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Venta',
            'autocomplete-message' => 'Debe especificar Venta',
            'icon' => 'gears',
            'placeholder' => 'Buscar Venta'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'id',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id',
                    'order' => 'Product.id ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'gears',
            'placeholder' => 'Buscar Producto'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Puntos',
        ],
    ];
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ACHIEVEMENT-LOG-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function append($user_id, $achievement_id, $product_id, $sale_id, $points) {
        $this->id = null;
        $data = [
            'user_id' => $user_id,
            'achievement_id' => $achievement_id,
            'product_id' => $product_id,
            'sale_id' => $sale_id,
            'points' => $points,
        ];
        
        $this->prepareForStore($data);
        
        return $this->saveData($data);
    }
}