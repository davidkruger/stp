<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Company', 'Model');

class ChallengeCompany extends KlezBackendAppModel{
    private $schema = [
        'challenge_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Desafio',
            'autocomplete' => [
                'class' => 'Challenge',
                'path' => 'Model',
                'label' => 'title',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Challenge.id, Challenge.title',
                    'order' => 'Challenge.title ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Desafio',
            'autocomplete-message' => 'Debe especificar Desafio',
            'icon' => 'gears',
            'placeholder' => 'Buscar Desafio'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CHALLENGE-COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}