<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ComplainSaleModeration extends KlezBackendAppModel{    
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Moderacion',
            'writable' => true,
            'required' => true,
            'readable' => false,
        ],
        'user_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Moderador',
        ],
        'complain_sale_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Reclamo de Venta',
        ],
        'observations' => [
            'type' => 'text',
            'label' => 'Observaciones',
            'writable' => true,
            'readable' => false,
            'required' => false,
        ],
        'moderation' => [
            'type' => 'options',
            'writable' => true,
            'readable' => false,
            'required' => true,
            'label' => 'Moderacion del Reclamo de Venta',
            'options' => [
                'accepted' => 'Aprobado',
                'rejected' => 'Rechazado',
            ],
        ],
    ];
      
    private $readableSchema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Moderacion',
            'writable' => true,
            'required' => true,
            'readable' => true,
        ],
        'user_id' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Moderador',
        ],
        'moderation' => [
            'type' => 'options',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'label' => 'Moderacion del Reclamo de Venta',
            'options' => [
                'accepted' => 'Aprobado',
                'rejected' => 'Rechazado',
            ],
        ],
        'observations' => [
            'type' => 'text',
            'label' => 'Observaciones',
            'writable' => true,
            'readable' => true,
            'required' => false,
        ],
    ];
    
    public function provideSchema() {
        throw new Exception('Deprecated');
        switch ($this->getShape()){
            case 'readable';
                return $this->readableSchema;
        }
        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPLAIN-SALE-MOD-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function injectComplainSale($query,$authdata,$payload){
        $query['conditions']['ComplainSaleModeration.complain_sale_id'] = $query['conditions']['ComplainSaleModeration.id'];
        unset($query['conditions']['ComplainSaleModeration.id']);
        
        return $query;
    }
}