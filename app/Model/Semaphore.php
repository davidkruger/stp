<?php

class Semaphore{
    const SEM_KEY = 7654;
    const SEM_MAX = 1;
    const SEM_PER = 0666;
    const SEM_REL = 1;
}
