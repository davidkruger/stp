<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class FullSale extends KlezBackendAppModel{
    private $schema = [
        'sale_date' => [
            'type' => 'date',
            'orderable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Fecha Venta',
            'date-message' => 'Fecha no valida'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'searchable' => true,
            'like' => '%%%s%%',
            'autocomplete' => [
                'class' => 'FullCompany',
                'path' => 'Model',           
                'label' => 'company',
                'full' => true,
                'identifier' => 'id',
                'query' => [
                    'fields' => 'FullCompany.id, FullCompany.company',
                    'order' => 'FullCompany.company ASC',
                    'conditions' => [
                        'FullCompany.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'RUC no encontrado',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar RUC'
        ],
        'salesman_id' => [
            'type' => 'foreign',
            'searchable' => true,
            'like' => '%%%s%%',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Vendedor',
            'autocomplete_dep' => 'company_id',
            'autocomplete' => [
                'class' => 'Salesman',
                'path' => 'Model',
                'label' => 'full_name',
                'full' => true,
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Salesman.id, Salesman.full_name',
                    'order' => 'Salesman.full_name ASC',
                    'conditions' => [
                        'Salesman.status' => 1                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Vendedor',
            'autocomplete-message' => 'Debe especificar Vendedor',
            'icon' => 'user',
            'placeholder' => 'Buscar Vendedor'
        ],
        'invoice' => [
            'type' => 'text',
            'searchable' => true,
            'like' => '%%%s%%',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Factura',
        ],
        'division' => [
            'type' => 'text',
            'searchable' => true,
            'like' => '%%%s%%',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Division',
        ],
        'product_id' => [
            'searchable' => true,
            'like' => '%%%s%%',
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'FullProduct',
                'path' => 'Model',
                'label' => 'product',
                'identifier' => 'id',
                'full' => true,
                'query' => [
                    'fields' => 'FullProduct.id, FullProduct.product',
                    'order' => 'FullProduct.product ASC',
                    'conditions' => [
                        'FullProduct.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Codigo de Producto no encontrado',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Codigo'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Cantidad de Productos',
            'int-message' => 'Cantidad no valida'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Puntos',
        ],
        'created' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Fecha Carga',
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => false,
            'listable' => false,
            'label' => 'Cargador',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'email',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.email',
                    'order' => 'User.email ASC',
                    'conditions' => [
                        'User.status' => 1                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'approval' => [
            'type' => 'options',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'hidden' => true,
            'listable' => false,
            'label' => 'Aprobacion',
            'options' => [
                'ninguna' => 'Ninguna',
                'responsable' => 'responsable',
                'samsung' => 'Samsung'
            ]
        ],
        'massive_sale_id' => [
            'type' => 'int',
            'required' => false,
            'writable' => true,
            'readable' => true,
            'hidden' => true,
            'listable' => false,
            'label' => 'Masivo',
        ],
        'has_stock' => [
            'type' => 'boolean',
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'En Stock?',
            'boolean' => [
                0 => 'No',
                1 => 'Si'
            ],
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'read':
                return $this->readSchema();
        }
        
        return $schema;
    }
    
    private function readSchema(){
        $schema = $this->schema;  
        $schema['company_id']['autocomplete']['label'] = 'company';
        $schema['product_id']['autocomplete']['label'] = 'product';
        $schema['points']['hidden'] = false;
        
        return $schema;
    }
        
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => '#VENTA',
            'listable' => true,
            'readable' => true,
            'hidden' => in_array($this->getShape(), ['massive','manual'])
        ];
    }
    
    
    public function filterPendingMassiveSale($query,$authdata,$payload){
        $query['conditions']['FullSale.massive_sale_id'] = $payload['massive_sale'];
        $query['conditions']['FullSale.approval'] = 'responsable';
        return $query;
    }
    
    public function checkSamsungApprove($data){
        return $data['approval'] === 'responsable';// && is_null($data['massive_sale_id']);
    }
    
    public function filterCompany($query,$authdata,$payload){
        if(isset($authdata['data']['company_id'])){
            $query['conditions']['FullSale.company_id'] = $authdata['data']['company_id'];
        }
        
        return $query;
    }
    
    public function filterResponsableApproval($query,$authdata,$payload){
        $query['conditions']['FullSale.approval'] = 'ninguna';
        return $query;
    }
    
    public function filterVendedor($query,$authdata,$payload){
        $role = $authdata['data']['role'];
        
        if($role === 'vendedor'){
            $query['conditions']['FullSale.salesman_id'] = $authdata['data']['id'];
        }
        
        return $query;
    }
}