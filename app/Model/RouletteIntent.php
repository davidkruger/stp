<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('RoulettePrize', 'Model');
App::uses('Roulette', 'Model');
App::uses('User', 'Model');

class RouletteIntent extends KlezBackendAppModel{
    private $schema = [
        'roulette_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Ruleta',
            'autocomplete' => [
                'class' => 'Roulette',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Roulette.id, Roulette.name',
                    'order' => 'Roulette.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Ruleta',
            'autocomplete-message' => 'Debe especificar Ruleta',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Ruleta'
        ],
        'user_id' => [
            'searchable' => true,
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Nombre',
            'autocomplete' => [
                'class' => 'Seller',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Seller.id, Seller.full_name',
                    'order' => 'Seller.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'weight' => 4,
            'searchable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'branch_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'searchable' => true,
            'label' => 'Sucursal',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Branch.id, Branch.name',
                    'order' => 'Branch.name ASC',
                    'conditions' => [
                        'Branch.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'prize' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'like' => '%%%s%%',
            'required-message' => 'Debe ingresar premio',
            'label' => 'Premio',
        ],
        'value' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar valor del premio',
            'int-message' => 'Valor no valido',
            'label' => 'Valor Premio',
        ],
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha y Hora',
            'orderable' => true,
            'searchable' => true,
            'required' => false,
            'writable' => false,
            'readable' => true,
            'listable' => true
        ],
        'points' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar puntos',
            'int-message' => 'Puntos no validos',
            'label' => 'Puntos',
        ],
        'acum' => [
            'type' => 'int',
            'searchable' => true,
            'required' => true,
            'orderable' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar pozo',
            'int-message' => 'Pozo no valido',
            'label' => 'Pozo',
        ],
        'roulette_prize_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Premio',
            'autocomplete' => [
                'class' => 'RoulettePrize',
                'path' => 'Model',
                'label' => 'prize',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'RoulettePrize.id, RoulettePrize.prize',
                    'order' => 'RoulettePrize.prize ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Premio',
            'autocomplete-message' => 'Debe especificar Premio',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Premio'
        ],
    ];
    
    public function provideSchema() {        
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ROULETTE-INTENT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function intent($auth, $roulette){
        $Roulette = new Roulette();
        $User = new User();
        $Roulette->begin();
        
        try{
            #$roulette['acum'] = $roulette['acum'];
            $userPoints = $auth['data']['points'] - $roulette['points'];
            
            if($userPoints < 0){
                throw new Exception('User points cannot be negative');
            }
            
            $User->id = $auth['id'];
            if(!$User->saveField('points', $userPoints)){
                throw new Exception('Cannot update users points');
            }
            
            $prize = $this->draw($roulette);
            $this->id = null;
            $data = [
                'roulette_id' => $roulette['id'],
                'user_id' => $auth['id'],
                'company_id' => $auth['data']['company_id'],
                'branch_id' => $auth['data']['branch_id'],
                'prize' => $prize['prize'],
                'value' => $prize['points'],
                'roulette_prize_id' => $prize['id'],
                'points' => $roulette['points'],
                'acum' => $roulette['acum'],
            ];
            
            $this->prepareForStore($data);
            if(!$this->saveData()){
                error_log(serialize($this->validationErrors));
                throw new Exception('Cannot create roulettes intent');
            }
            
            $roulette['acum'] = (int) $roulette['acum'] - (int) $prize['points'] + (int) $roulette['points'];
            $sql = "UPDATE roulettes SET acum='{$roulette['acum']}' where id='{$roulette['id']}'";
            $Roulette->query($sql);
            $Roulette->commit();
            
            return true;
        } catch (Exception $ex) {
            error_log('Exception @ RouletteIntent::intent() - ' . $ex->getMessage());
            $Roulette->rollback();
            return false;
        }
    }
    
    private function draw($roulette){
        $acum = $roulette['acum'];
        
        $prizes = array_values(array_filter($roulette['prizes'], function($prize) use ($acum) {
            return (int) $prize['points'] <= $acum && (
                (int) $prize['stock'] > 0 || (int) $prize['points'] === 0
            ) && ( $prize['redeemable'] || (int) $prize['points'] === 0 );
        }));
        error_log(serialize($prizes));
        $c = count($prizes);
        
        if($c <= 0){
            throw new Exception('No prize available at this time!');
        }
        
        $rand = rand(0, $c - 1);
        $p = $prizes[$rand];
        $Prize = new RoulettePrize();
        
        if($Prize->loadById($p['id'])){
            $stock = $Prize->readField('stock');
            
            if($stock > 0){
                if(!$Prize->saveField('stock', $stock - 1)){
                    throw new Exception('Cannot update prizes stock');
                }
            }
        }
        
        return $p;
    }
    
    public function injectRoulette($query,$authdata,$payload){
        $query['conditions']['RouletteIntent.roulette_id'] = $payload['filter']['roulette_id'];
        return $query;
    }
    
}