<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Company', 'Model');

class AchievementCompany extends KlezBackendAppModel{
    private $schema = [
        'achievement_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Desafio',
            'autocomplete' => [
                'class' => 'Challenge',
                'path' => 'Model',
                'label' => 'title',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Achievement.id, Achievement.name',
                    'order' => 'Achievement.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Logro',
            'autocomplete-message' => 'Debe especificar Logro',
            'icon' => 'gears',
            'placeholder' => 'Buscar Logro'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ACHIEVEMENT-COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}