<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class City extends KlezBackendAppModel{
    private $schema = [
        'name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'unique' => true,
            'readable' => true,
            'searchable' => true,
            'label' => 'Nombre Ciudad',
            'unique-message' => 'Nombre de Ciudad existente',
            'required-message' => 'Debe ingresar Ciudad'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado de la Ciudad',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            default:
                return $this->schema;
        }
    }

    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'CITY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}