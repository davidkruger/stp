<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class FullProduct extends KlezBackendAppModel{
    private $schema = [
        'product' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Producto',
        ],
    ];
        
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}