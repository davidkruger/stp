<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class FullCompany extends KlezBackendAppModel{
    private $schema = [
        'company' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Empresa',
        ],
    ];
        
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}