<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ComplainSale extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'label' => 'Fecha Reclamo',
            'writable' => false,
            'readable' => true,
            'required' => true,
            'orderable' => true
        ],
        'sale_date' => [
            'type' => 'date',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'orderable' => true,
            'label' => 'Fecha Venta',
            'date-message' => 'Fecha no valida'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'searchable' => true,
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'RUC no encontrado',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar RUC'
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Responsable',
            'autocomplete' => [
                'class' => 'FullUser',
                'path' => 'Model',
                'label' => 'full_name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'FullUser.id, FullUser.full_name',
                    'order' => 'FullUser.full_name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Vendedor',
            'autocomplete-message' => 'Debe especificar Vendedor',
            'icon' => 'account',
            'placeholder' => 'Buscar Vendedor'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'code',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.code',
                    'order' => 'Product.code ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'account',
            'placeholder' => 'Buscar Producto'
        ],
        'quantity' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Cantidad de Productos',
            'int-message' => 'Cantidad no valida'
        ],
        'status' => [
            'type' => 'options',
            'writable' => false,
            'listable' => false,
            'readable' => true,
            'label' => 'Estado del Canje',
            'options' => [
                'pending' => 'Pendiente',
                'accepted' => 'Aprobado',
                'rejected' => 'Rechazado',
            ],
        ],
    ];
    
    private $moderationSchema = [
        'status' => [
            'type' => 'options',
            'writable' => true,
            'readable' => true,
            'required' => true,
            'label' => 'Moderar Estado del Canje',
            'options' => [
                'accepted' => 'Aprobado',
                'rejected' => 'Rechazado',
            ],
        ],
        'observations' => [
            'weight' => 8,
            'type' => 'text',
            'subtype' => 'textarea',
            'writable' => true,
            'readable' => true,
            'required' => false,
            'placeholder' => 'Comentarios acerca de esta Moderacion',
            'label' => 'Observaciones',
        ],
    ];
    
    public function provideSchema() {
        throw new Exception('Deprecated');
        
        switch ($this->getShape()){
            case 'moderation':
                return $this->moderationSchema();
            case 'frontend':
                return $this->frontendSchema();
        }
        
        return $this->schema;
    }
    
    private function moderationSchema(){
        return $this->moderationSchema;
    }
    
    private function frontendSchema(){
        $schema = $this->schema;
        $schema['product_id']['writable'] = true;
        $schema['user_id']['writable'] = true;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPLAIN-SALE-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    private $Moderation;
    
    public $virtualFields = [
        'observations' => '""'
    ];
    
    public function pushModeration($data,$authdata){
        App::uses('ComplainSaleModeration','Model');
        $this->Moderation = new ComplainSaleModeration();
        $this->Moderation->id = null;
        
        $mod = [
            'ComplainSaleModeration' => [
                'complain_sale_id' => $this->id,
                'user_id' => $authdata['id'],
                'moderation' => $data['status'],
                'observations' => $data['observations'],
            ]
        ];
        
        if($this->Moderation->save($mod)){
            return true;
        }
        
        return false;
    }
    
    public function pendingFilter($query,$authdata,$payload){
        $query['conditions']['ComplainSale.status'] = 'pending';
        return $query;
    }
    
    public function notPendingFilter($query,$authdata,$payload){
        $query['conditions']['ComplainSale.status != '] = 'pending';
        return $query;
    }
    
    private $Sale;
    private $ComplainSale;
    
    public function injectSale($data,$authdata){
        if($data['status'] === 'rejected'){
            return true;
        }
        
        App::uses('Sale','Model');
        $this->Sale = new Sale();
        $this->Sale->id = null;
        
        App::uses('ComplainSale','Model');
        $this->ComplainSale = new ComplainSale();
        
        if($this->ComplainSale->loadById($this->id)){            
            $moderation = $this->ComplainSale->getData();
            $moderation['id'] = null;
            $moderation['user_id'] = $authdata['id'];
            unset($moderation['created']);
            $moderation['points'] = $this->Sale->calcPoints(null, $moderation);
            $sale = [ 'Sale' => $moderation ];
            $this->Sale->writeData($moderation);

            if($this->Sale->save($sale)){
                return true;
            }
        }

        return false;
    }
    
    private $Company;
    
    public function updateCompanyPoints($data,$authdata){
        if($data['status'] === 'rejected'){
            return true;
        }
        
        App::uses('Company','Model');
        $this->Company = new Company();
        $company = $data['company_id'];
        
        return $this->Company->semUpdatePoints($this->Sale->readField('company_id'),$this->Sale->readField('points'));
        
    }
    
    public function injectUser($data,$authdata){
        $dataUser = (int) $data['user_id'];
        $authUser = (int) $authdata['id'];
        
        if($dataUser !== $authUser){
            return false;
        }
        
        $dataCompany = (int) $data['company_id'];
        $authCompany = (int) $authdata['data']['company_id'];
        
        if($dataCompany !== $authCompany){
            return false;
        }
        
        return true;
    }
}