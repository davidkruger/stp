<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class RunSaleTarget extends KlezBackendAppModel{
    private $schema = [
        'run_sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Competencia',
            'autocomplete' => [
                'class' => 'RunSale',
                'path' => 'Model',
                'label' => 'description',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'RunSale.id, RunSale.description',
                    'order' => 'RunSale.description ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Competencia',
            'autocomplete-message' => 'Debe especificar Competencia',
            'icon' => 'gears',
            'placeholder' => 'Buscar Competencia'
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'gears',
            'placeholder' => 'Buscar Producto'
        ],
        'status' => [
            'type' => 'boolean',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Estado',
            'weight' => 6,
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    private $xlsxRunSaleSchema = [
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'gears',
            'placeholder' => 'Buscar Producto'
        ],
        'id' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Frecuencia',
        ],
    ];
        
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'xlsxRunSale':
                return $this->xlsxRunSaleSchema();
            default:
                return $schema;
        }
    }
    
    private function xlsxRunSaleSchema(){
        $schema = $this->xlsxRunSaleSchema;
        $this->virtualFields['id'] = 'COUNT(Sale.id)';
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'RUN-SALE-TARGET-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function injectRunSale($query,$authdata,$payload){
        $query['conditions']['RunSaleLog.run_sale_id'] = $payload['filter']['run_sale_id'];
        return $query;
    }
}