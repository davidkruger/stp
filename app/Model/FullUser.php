<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class FullUser extends KlezBackendAppModel{
    private $schema = [
        'full_name' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Nombre Completo',
        ],
    ];
        
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function fetchSalesmenFrontend($authdata){
        $company = $authdata['company_id'];
        $cnd =  [
            'FullUser.status' => true,
            'FullUser.company_id' => $company,
            'FullUser.role' => [ 'vendedor','encargado' ]
        ];
        
        if($authdata['role'] === 'encargado'){
            $cnd['OR'][]['FullUser.branch_id'] = $authdata['branch_id'];
            
            if($authdata['branch_id2']) $cnd['OR'][]['FullUser.branch_id'] = $authdata['branch_id2'];
            if($authdata['branch_id3']) $cnd['OR'][]['FullUser.branch_id'] = $authdata['branch_id3'];
        }
        
        return $this->find('all',[
            'conditions' => $cnd,
            'fields' => 'FullUser.id, FullUser.full_name, FullUser.document',
            'order' => 'FullUser.full_name ASC'
        ]);
    }
}