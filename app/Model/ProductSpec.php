<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ProductSpec extends KlezBackendAppModel{
    private $schema = [
        'spec_order' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'orderable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar un numero valido',
            'int-message' => 'Debe ingresar un numero valido',
            'label' => 'Orden Lista',
        ],
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto'
        ],
        'spec_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Caracteristica',
            'autocomplete' => [
                'class' => 'Spec',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Spec.id, Spec.name',
                    'order' => 'Spec.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'compound_unique'=> [ 'product_id' ],
            'compound_unique-message' => 'Producto ya posee esta Caracteristica',
            'required-message' => 'Debe especificar Caracteristica',
            'autocomplete-message' => 'Debe especificar Caracteristica',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Caracteristica'
        ],
        'value' => [
            'type' => 'text',
            'searchable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar contenido',
            'label' => 'Contenido',
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Estado de la Caracteristica',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'productless':
                return $this->productless();
            default:
                return $this->schema;
        }
    }

    private function productless(){
        $schema = $this->schema;
        $schema['product_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-SPEC-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function productOwnerPrequery($query,$authdata,$payload){
        if(isset($payload['data']['product'])){
            $id = $payload['data']['product'];
        }
        else if(isset($payload['product'])){
            $id = $payload['product'];
        }
        else{
            return false;
        }
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $alias = $this->alias;
        $query['conditions']["{$alias}.product_id"] = $id;
        $this->product = $id;
        
        return $query;
    }
    
    public function fetchForProduct($p){
        $this->virtualFields['spec'] = 'Spec.name';
        return $this->find('all',[
            'joins' => [
                'INNER JOIN specs AS Spec ON Spec.id = ProductSpec.spec_id',
            ],
            'order' => 'ProductSpec.spec_order ASC',
            'conditions' => [
                'ProductSpec.status' => 1,
                'ProductSpec.product_id' => $p,
                'Spec.status' => 1,
            ]
        ],false);
    }
}