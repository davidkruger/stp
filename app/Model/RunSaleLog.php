<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class RunSaleLog extends KlezBackendAppModel{
    private $schema = [
        'run_sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Competencia',
            'autocomplete' => [
                'class' => 'RunSale',
                'path' => 'Model',
                'label' => 'description',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'RunSale.id, RunSale.description',
                    'order' => 'RunSale.description ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Competencia',
            'autocomplete-message' => 'Debe especificar Competencia',
            'icon' => 'gears',
            'placeholder' => 'Buscar Competencia'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'gears',
            'placeholder' => 'Buscar Empresa'
        ],
        'sale_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => true,
            'readable' => true,
            'label' => 'Venta',
            'autocomplete' => [
                'class' => 'Sale',
                'path' => 'Model',
                'label' => 'id',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Sale.id',
                    'order' => 'Sale.id ASC',
                    'conditions' => [
                        
                    ],
                ]
            ],          
            'required-message' => 'Debe especificar Venta',
            'autocomplete-message' => 'Debe especificar Venta',
            'icon' => 'gears',
            'placeholder' => 'Buscar Venta'
        ],
        'points' => [
            'type' => 'int',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Puntos',
            'weight' => 6,
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'RUN-SALE-LOG-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function allocRunSale($sale,$run){
        $data = [
            'run_sale_id' => $run['id'],
            'sale_id' => $sale['id'],
            'points' => $sale['quantity'],
            'company_id' => $sale['company_id'],
        ];
        
        $this->prepareForStore($data);
        return $this->saveData($data);
    }
}