<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Semaphore', 'Model');

class Company extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre de la Empresa',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre',
            'searchable' => true
        ],
        'ruc' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'RUC',
            'unique-message' => 'Este RUC ya ha sido registrado',
            'required-message' => 'Debe ingresar RUC',
            'searchable' => true
        ],
        'social_reason' => [
            'type' => 'text',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Razon Social',
            'required-message' => 'Debe ingresar Razon Social',
            'searchable' => true
        ],
        'telephone' => [
            'type' => 'text',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Telefono',
            'required-message' => 'Debe ingresar Telefono'
        ],
        'email' => [
            'type' => 'text',
            'subtype' => 'email',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Email',
            'required-message' => 'Debe ingresar Direccion de Email'
        ],
        'logo' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Logo',
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'companies',
                'action' => 'logo',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una imagen válida en formato PNG o JPG'
        ],
        'multiply' => [
            'type' => 'int',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Multiplicador (en %)',
            'required-message' => 'Debe ingresar multiplicador',
            'orderable' => true,
            'default' => 100,
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado de la Empresa',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
//        'points' => [
//            'type' => 'int',
//            'writable' => false,
//            'readable' => true,
//            'label' => 'Puntos Acumulados',
//        ],
    ];
    
    private $massiveSchema = [
        'document' => [
            'type' => 'file',
            'subtype' => 'xlsx',
            'weight' => 12,
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => false,
            'label' => 'Carga Masiva',
            'mimes' => [
//                'application/csv',
//                'application/lotus123',
//                'application/msexcel',
//                'application/vnd.lotus-1-2-3',
//                'application/vnd.ms-excel',
//                'application/vnd.ms-excel [official]',
//                'application/vnd.ms-works',
//                'application/vnd.msexcel',
//                'application/wks',
//                'application/x-dos_ms_excel',
//                'application/x-excel',
//                'application/x-lotus123',
//                'application/x-msexcel',
//                'application/x-msworks',
//                'application/x-wks',
//                'application/xlc',
//                'application/xlt',
//                'text/anytext',
//                'text/comma-separated-values',
//                'text/csv'
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ],
            'url' => [
                'controller' => 'companies',
                'action' => 'document',
                'plugin' => null
            ],
            'placeholder' => 'Seleccione archivo XLSX',
            'xlsx-message' => 'Debe ser un documento valido en formato XLSX',
            'file-message' => 'Debe ser un documento valido en formato XLSX'
        ],
    ];
    
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            case 'massive':
                return $this->massiveSchema;
            case 'xlsxFilter':
                return $this->xlsxFilterSchema($schema);
            case 'xlsx':
                return $this->xlsxSchema($schema);
            default:
                return $schema;
        }
    }

    private function xlsxSchema($schema) {
        $schema['telephone']['listable']=true;
        $schema['email']['listable']=true;
        return $schema;
    }

    private function xlsxFilterSchema($schema) {
        unset($schema['name']['required']);
        unset($schema['name']['unique']);
        unset($schema['social_reason']['required']);
        unset($schema['social_reason']['unique']);
        unset($schema['ruc']['required']);
        unset($schema['ruc']['unique']);

        return [
            'name' => $schema['name'],
            'ruc' => $schema['ruc'],
            'social_reason' => $schema['social_reason'],
            'status' => $schema['status']
        ];
    }

    public function xlsxFilter($query,$authdata,$payload){
        $status = $payload['filter']['status'];
        $name = $payload['filter']['name'];
        $ruc = $payload['filter']['ruc'];
        $social_reason = $payload['filter']['social_reason'];

        $query['conditions']['Company.status'] = $status;

        if ($name) {
            $query['conditions']['OR'][] = "Company.name LIKE '%{$name}%'";
        }

        if ($ruc) {
            $query['conditions']['OR'][] = "Company.ruc LIKE '%{$ruc}%'";
        }

        if ($social_reason) {
            $query['conditions']['OR'][] = "Company.social_reason LIKE '%{$social_reason}%'";
        }

        return $query;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'COMPANY-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function massiveDefaults(&$data,$authdata){
        $this->writeField('status', true);
        $data['status'] = true;
        return true;
    }
    
    public function fetchFrontend($authdata){
        $cnd = [];
        $cnd['Company.id'] = $authdata['data']['company_id'];
        
        return $this->find('first',[
            'conditions' => $cnd
        ]);
    }
    
    public function semUpdatePoints($id,$points){
        if($points <= 0){
            return true;
        }
        
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
                
        if($semaphore){
            sem_acquire($semaphore);
            
            if($this->loadById($id)){
                $current = $this->readField('points');
                $p = $points + $current;
                $this->id = $id;
                $success = (bool) $this->saveField('points', $p);
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
        }
        
        sem_release($semaphore);
        return $success;
    }
    
    public function summary($payload){
        $cnd = [];
        $cnd['Company.status'] = 1;
        
        $cnd['Sale.rejected'] = 0;
        $cnd['Sale.approval'] = 'samsung';
        $cnd['Salesman.status'] = 1;
        
        if(isset($payload['company_id']) && $payload['company_id']){
            $cnd['Company.id'] = $payload['company_id'];
        }
        
        if(isset($payload['product_id']) && $payload['product_id']){
            $cnd['Sale.product_id'] = $payload['product_id'];
        }
        
        if(isset($payload['a']) && $payload['b']){
            $cnd['Sale.sale_date >= '] = $payload['a'];
            $cnd['Sale.sale_date <= '] = $payload['b'];
        }
        
        if(isset($payload['product_id']) && $payload['product_id']){
            $cnd['Sale.product_id'] = $payload['product_id'];
        }
        
        $joins = [];
        $joins[] = 'LEFT JOIN sales AS Sale ON Sale.company_id = Company.id';
        $joins[] = 'LEFT JOIN salesmen AS Salesman ON Salesman.id = Sale.salesman_id';
        
        $group = [];
        $group[] = 'Company.id';
        
        $fields = [];
        $fields[] = 'Company.id';
        $fields[] = 'Company.name';
        $fields[] = 'SUM(COALESCE(Sale.points,0)) AS Company__acum';
        
        $order = [];
        $order[] = 'Company__acum DESC';
        $order[] = 'Company.id ASC';
        
        $query = [
            'fields' => $fields,
            'conditions' => $cnd,
            'joins' => $joins,
            'group' => $group
        ];
        
        return $this->find('all', $query, false);
    }
    
    public function feedCustomFilters(){
        $cnd = [];
        $cnd['Company.status'] = 1;
        $cnd['Branch.status'] = 1;
        
        $joins = [];
        $joins[] = 'INNER JOIN branches AS Branch ON Branch.company_id = Company.id';
        
        $fields = [];
        $fields[] = 'Company.id';
        $fields[] = 'Company.name';
        
        $this->virtualFields = [];
        $this->virtualFields['branch_id'] = 'Branch.id';
        $this->virtualFields['branch'] = 'Branch.name';
        
        $order = [];
        $order[] = 'Company.id ASC';
        
        $query = [
//            'fields' => $fields,
            'conditions' => $cnd,
            'joins' => $joins,
        ];
        
        return $this->find('all', $query, true);
    }
}