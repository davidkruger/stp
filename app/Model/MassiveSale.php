<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class MassiveSale extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'orderable' => true,
            'type' => 'datetime',
            'required' => true,
            'writable' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Fecha Carga',
        ],
        'user_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'hidden' => false,
            'readable' => true,
            'listable' => true,
            'label' => 'Cargador',
            'autocomplete' => [
                'class' => 'User',
                'path' => 'Model',
                'label' => 'email',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'User.id, User.email',
                    'order' => 'User.email ASC',
                    'conditions' => [
                        'User.status' => 1
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Usuario',
            'autocomplete-message' => 'Debe especificar Usuario',
            'icon' => 'account',
            'placeholder' => 'Buscar Usuario'
        ],
        'approval' => [
            'type' => 'options',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'hidden' => true,
            'listable' => false,
            'label' => 'Aprobacion',
            'options' => [
                'ninguna' => 'Ninguna',
                'responsable' => 'responsable',
                'samsung' => 'Samsung'
            ]
        ],
    ];
    
    public function provideSchema() {
        $schema = $this->schema;
                
        switch($this->getShape()){
            default:
                return $schema;
        }
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => '#VENTA-MASIVA',
            'listable' => true,
            'readable' => true,
            'hidden' => false
        ];
    }
    
    public function alloc($authdata){
        $data = [];
        $data['user_id'] = $authdata['id'];
        $approval = 'ninguna';
        
        switch ($authdata['data']['role']){
            case 'samsung':
            case 'lamoderna':
//            case 'romis':
                $approval = 'samsung';
                break;
            case 'responsable':
            case 'encargado':
                $approval = 'responsable';
                break;
        }
        
        
        $data['approval'] = $approval;
        
        $this->prepareForStore($data,null);
        return $this->saveData();
    }
    
    public function checkSamsungApprove($data){
        return $data['approval'] === 'responsable';
    }
}