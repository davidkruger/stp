<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class ProductPoint extends KlezBackendAppModel{
    private $schema = [
        'product_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Producto',
            'autocomplete' => [
                'class' => 'Product',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Product.id, Product.name',
                    'order' => 'Product.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Producto',
            'autocomplete-message' => 'Debe especificar Producto',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Producto'
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'orderable' => true,
            'listable' => true,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'searchable' => true,
            'placeholder' => 'Buscar Empresa',
            'compound_unique'=> [ 'product_id' ],
            'compound_unique-message' => 'Empresa ya posee Puntos asignados',
        ],
        'points' => [
            'type' => 'int',
            'searchable' => true,
            'orderable' => true,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'required-message' => 'Debe ingresar punots',
            'int-message' => 'Debe ingresar una cantidad valida',
            'label' => 'Puntos',
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => true,
            'label' => 'Estado del Catalogo',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'productless':
                return $this->productless();
            default:
                return $this->schema;
        }
    }

    private function productless(){
        $schema = $this->schema;
        $schema['product_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'PRODUCT-CATALOG-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function productOwnerPrequery($query,$authdata,$payload){
        if(isset($payload['data']['product'])){
            $id = $payload['data']['product'];
        }
        else if(isset($payload['product'])){
            $id = $payload['product'];
        }
        else{
            return false;
        }
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $alias = $this->alias;
        $query['conditions']["{$alias}.product_id"] = $id;
        $this->product = $id;
        
        return $query;
    }
    
    public function getBase($data, $defa){
        $cnd = [];
        $cnd['ProductPoint.company_id'] = $data['company_id'];
        $cnd['ProductPoint.product_id'] = $data['product_id'];
        
        if($this->loadByConditions($cnd)){
            return $this->readField('points');
        }
        
        return $defa;
    }
}