<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');
App::uses('Semaphore', 'Model');

class Seller extends KlezBackendAppModel{
    private $schema = [
        'email' => [
            'type' => 'text',
            'subtype' => 'email',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Email',
            'unique-message' => 'Email ya ha sido registrado',
            'required-message' => 'Debe ingresar direccion de email'
        ],
        'password' => [
            'type' => 'text',
            'subtype' => 'password',
            'required' => true,
            'writable' => true,
            'readable' => false,
            'hash' => [
                'algorithm' => 'md5',
                'salt' => [
                    'entropy' => '',
                    'function' => 'sprintf'
                ]
            ],
            'label' => 'Password',
        ],
        'first_name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre',
        ],
        'last_name' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Apellido',
        ],
        'telephone' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Telefono',
        ],
//        'document_type' => [
//            'type' => 'options',
//            'options' => [
//                'ci' => 'CI',
//                'ruc' => 'RUC',
//                'passport' => 'Pasaporte',
//            ],
//            'required' => true,
//            'writable' => true,
//            'readable' => true,
//            'label' => 'Tipo de Documento',
//            'options-message' => 'Debe seleccionar Tipo de Documento'
//        ],
        'document' => [
            'type' => 'text',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'Nro. de Documento',
        ],
        'picture' => [
            'type' => 'file',
            'subtype' => 'image',
            'required' => false,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Foto',
            'mimes' => [
                'image/jpeg',
                'image/png'
            ],
            'url' => [
                'controller' => 'users',
                'action' => 'picture',
                'plugin' => null
            ],
            'image-message' => 'Debe ser una foto válida en formato PNG o JPG'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Estado de la Cuenta',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
        'role' => [
            'group' => [
                'title' => 'Datos del Perfil',
                'fields' => [
                    'company_id'
                ],
                'dep' => [
                    'vendedor' => [ 
                        'company_id'
                    ],
                    'responsable' => [ 
                        'company_id'
                    ],
                ]
            ],
            'label' => 'Perfil del Usuario',
            'type' => 'options',
            'options' => [
//                'romis' => 'Romis',
                'samsung' => 'Samsung',
                'lamoderna' => 'LaModerna',
                'responsable' => 'Responsable',
                'vendedor' => 'Vendedor',
            ],
            'listable' => false,
            'writable' => true,
            'readable' => true,
        ],
        'company_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Empresa',
            'autocomplete' => [
                'class' => 'Company',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Company.id, Company.name',
                    'order' => 'Company.name ASC',
                    'conditions' => [
                        'Company.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Empresa',
            'autocomplete-message' => 'Debe especificar Empresa',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Empresa'
        ],
        'branch_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'weight' => 4,
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'searchable' => true,
            'label' => 'Sucursal',
            'autocomplete_dep' => 'company_id',
            'autocomplete_dep_condition' => 'Branch.company_id',
            'autocomplete' => [
                'class' => 'Branch',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'type' => 'LEFT',
                'query' => [
                    'fields' => 'Branch.id, Branch.name',
                    'order' => 'Branch.name ASC',
                    'conditions' => [
                        'Branch.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Sucursal',
            'autocomplete-message' => 'Debe especificar Sucursal',
            'icon' => 'home',
            'placeholder' => 'Buscar Sucursal'
        ],
        'points' => [
            'type' => 'int',
            'required' => false,
            'writable' => false,
            'readable' => false,
            'listable' => false,
            'label' => 'Puntos',
        ],
        'full_name' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => false,
            'listable' => false,
            'label' => 'Nombre Completo',
        ],
        'extra' => [
            'type' => 'double',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'default' => 5,
            'double-message' => 'Debe ser un valor numerico (use punto para decimales)',
            'label' => '% de puntos x venta',
        ],
    ];
        
    public function provideSchema() {
        switch ($this->getShape()){
            case 'frontend':
                return $this->frontendSchema();
        }
        
        return $this->schema;
    }
    
    private function frontendSchema(){
        $schema = $this->schema;
        $schema['password']['hash']['salt']['entropy'] = Configure::read('User.password.salt');
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
    
    public function loadFrontend($id,$authdata){
        $cnd = [];
        $cnd['Seller.id'] = $id;
        $cnd['Seller.status'] = true;
        $cnd['Seller.company_id'] = $authdata['data']['company_id'];
        
        switch ($authdata['data']['role']){
            case 'encargado':
                $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id'];
                if($authdata['data']['branch_id2']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id2'];
                if($authdata['data']['branch_id3']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id3'];
                break;
        }
        
        if(!$this->loadByConditions($cnd)){
            throw new NotFoundException();
        }
        
        return $this->getData();
    }
    
    public function fetchFrontend($authdata){
        $cnd = [];
        $cnd['Seller.status'] = true;
        $cnd['Seller.role'] = 'vendedor';
        $cnd['Seller.company_id'] = $authdata['data']['company_id'];
        
        switch ($authdata['data']['role']){
            case 'encargado':
                $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id'];
                if($authdata['data']['branch_id2']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id2'];
                if($authdata['data']['branch_id3']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id3'];
                break;
        }
        
        return $this->find('all',[
            'conditions' => $cnd,
            'order' => 'Seller.full_name ASC'
        ]);
    }
    
    public function findFrontend($id,$authdata){
        $cnd = [];
        $cnd['Seller.id'] = $id;
        $cnd['Seller.status'] = true;
        $cnd['Seller.role'] = 'vendedor';
        $cnd['Seller.company_id'] = $authdata['data']['company_id'];
        
        switch ($authdata['data']['role']){
            case 'encargado':
                $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id'];
                if($authdata['data']['branch_id2']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id2'];
                if($authdata['data']['branch_id3']) $cnd['OR'][]['Seller.branch_id'] = $authdata['data']['branch_id3'];
                break;
        }
        
        return $this->find('first',[
            'conditions' => $cnd
        ]);
    }
    
    public function disableFrontend($id){
        $this->id = $id;
        
        return $this->saveField('status',false);
    }
    
    private $Company;
    
    public function assignPoints($company,$seller,$points){
        App::uses('Company','Model');
        $this->Company = new Company();
        
        if($this->Company->loadById($company) === false){
            return false;
        }
        
        $cnd = [];
        $cnd['Seller.company_id'] = $company;
        $cnd['Seller.id'] = $seller;
            
        if($this->loadByConditions($cnd)){
            $current = (int) $this->readField('points');
            $p = $points + $current;
            
            $this->Company->id = $company;
            $currentCompany = (int) $this->Company->readField('points');
            $pCompany = $currentCompany - $points;
            
            return
                $this->saveField('points', $p) &&
                $this->Company->saveField('points', $pCompany);
        }
        
        return false;
    }
    
    public function semUpdatePoints($id,$points){
        if($points <= 0){
            return true;
        }
        
        $semaphore = sem_get(Semaphore::SEM_KEY, Semaphore::SEM_MAX, Semaphore::SEM_PER, Semaphore::SEM_REL);
        $success = false;
                
        if($semaphore){
            sem_acquire($semaphore);
            
            if($this->loadById($id)){
                $current = $this->readField('points');
                $rate = $this->readField('points_rate');
                $gains = round($rate * $points / 100);
                $p = $gains + $current;
                $this->id = $id;
                $success = (bool) $this->saveField('points', $p);
                error_log("Giving $gains points to $id, sale=$points at rate=$rate former=$current new=$p");
            }
        }
        else{
            error_log("Cannot sem_get(" . Semaphore::SEM_KEY . "," . Semaphore::SEM_MAX ."," . Semaphore::SEM_PER ."," . Semaphore::SEM_REL. ")");
        }
        
        sem_release($semaphore);
        return $success;
    }
}