<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Spec extends KlezBackendAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'searchable' => true,
            'label' => 'Nombre de la Caracteristica',
            'unique-message' => 'Este nombre ya ha sido registrado',
            'required-message' => 'Debe ingresar nombre'
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado de la Caracteristica',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'SPEC-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}