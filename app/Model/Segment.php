<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Segment extends KlezBackendAppModel{
    private $schema = [
        'subdivision_id' => [
            'type' => 'foreign',
            'subtype' => 'autocomplete',
            'required' => true,
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Subdivision',
            'autocomplete' => [
                'class' => 'Subdivision',
                'path' => 'Model',
                'label' => 'name',
                'identifier' => 'id',
                'query' => [
                    'fields' => 'Subdivision.id, Subdivision.name',
                    'order' => 'Subdivision.name ASC',
                    'conditions' => [
                        'Subdivision.status' => 1,
                    ]
                ]
            ],
            'required-message' => 'Debe especificar Subdivision',
            'autocomplete-message' => 'Debe especificar Subdivision',
            'icon' => 'shopping-bag',
            'placeholder' => 'Buscar Subdivision'
        ],
        'created' => [
            'type' => 'datetime',
            'writable' => false,
            'readable' => true,
            'label' => 'Fecha Alta',
            'orderable' => true
        ],
        'name' => [
            'type' => 'text',
            'required' => true,
            'compound_unique'=> [ 'subdivision_id' ],
            'writable' => true,
            'readable' => true,
            'label' => 'Nombre del Segmento',
            'compound_unique-message' => 'Nombre de Segmento existente',
            'required-message' => 'Debe ingresar nombre',
            'searchable' => true
        ],
        'status' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'listable' => false,
            'label' => 'Estado del Segmento',
            'boolean' => [
                0 => 'Suspendida',
                1 => 'Activa'
            ],
            'default' => 1,
        ],
    ];
    
    public function provideSchema() {
        switch($this->getShape()){
            case 'subdivisionless':
                return $this->subdivisionless();
            default:
                return $this->schema;
        }
    }

    private function subdivisionless(){
        $schema = $this->schema;
        $schema['subdivision_id']['writable'] = false;
        
        return $schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ID-SEGMENTO',
            'listable' => true,
            'readable' => true,
            'hidden' => true
        ];
    }

    public function subOwnerPrequery($query,$authdata,$payload){
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        if(isset($query['joins']) === false){
            $query['joins'] = [];            
        }
        
        $id = $payload['subdivision'];
        $alias = $this->alias;
        $query['conditions']["{$alias}.subdivision_id"] = $id;
        
        return $query;
    } 
}