<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Responsible extends KlezBackendAppModel{
    public $useTable = 'responsibles';
    
    private $schema = [
        'full_name' => [
            'type' => 'text',
            'required' => false,
            'writable' => false,
            'readable' => true,
            'label' => 'Nombre Completo',
        ],
    ];
        
    public function provideSchema() {
        return $this->schema;
    }
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'USER-ID',
            'listable' => false,
            'readable' => true,
            'hidden' => true
        ];
    }
}