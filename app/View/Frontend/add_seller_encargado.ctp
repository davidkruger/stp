<?php $this->Frontend->js('Frontend/js/seller'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Vendedores</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de agregar este vendedor?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="seller btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="col-sm-12">
    <h2>Agregar Vendedor</h2>
    
    <form id="seller_form" method="post">
        <div class="row">
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('first_name')?>">
                <label for="exampleInputEmail1">Nombre</label>
                <input value="<?=$this->Frontend->inputVal('first_name')?>" name="first_name" type="text" class="form-control" id="exampleInputEmail1" >
                <span class="label label-danger"><?=$this->Frontend->errors('first_name')?></span>
            </div>
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('last_name')?>">
                <label for="exampleInputPassword1">Apellido</label>
                <input value="<?=$this->Frontend->inputVal('last_name')?>" name="last_name" type="text" class="form-control" id="exampleInputPassword1" >
                <span class="label label-danger"><?=$this->Frontend->errors('last_name')?></span>
            </div>
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('telephone')?>">
                <label for="exampleInputPassword1">Teléfono</label>
                <input value="<?=$this->Frontend->inputVal('telephone')?>" name="telephone" type="text" class="form-control" id="exampleInputPassword1" >
                <span class="label label-danger"><?=$this->Frontend->errors('telephone')?></span>
            </div>
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('document')?>">
                <label for="exampleInputPassword1">N&deg; de documento</label>
                <input value="<?=$this->Frontend->inputVal('document')?>" name="document" type="text" class="form-control" id="exampleInputPassword1" >
                <span class="label label-danger"><?=$this->Frontend->errors('document')?></span>
            </div>
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('password')?>">
                <label for="exampleInputPassword1">Contraseña</label>
                <input value="<?=$this->Frontend->inputVal('password')?>" name="password" type="password" class="form-control" id="exampleInputPassword1" >
                <span class="label label-danger"><?=$this->Frontend->errors('password')?></span>
            </div>  
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('birthday')?>">
                <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                <input data-date-language="es" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-provide="datepicker" type="text" value="<?=$this->Frontend->inputVal('birthday')?>" name="birthday" class="form-control" id="exampleInputEmail1" >
                <span class="label label-danger"><?=$this->Frontend->errors('birthday')?></span>
            </div>
            <div class="form-group col-sm-4 col-md-3 col-lg-3 <?=$this->Frontend->errorField('email')?>">
                <label for="exampleInputPassword1">Correo electrónico</label>
                <input value="<?=$this->Frontend->inputVal('email')?>" name="email" type="text" class="form-control" id="exampleInputPassword1" >
                <span class="label label-danger"><?=$this->Frontend->errors('email')?></span>
            </div>
        </div><!-- row -->

        <div class="btn_cont big_form clearfix">
            <a class="btn btn-default boton_login" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i> AGREGAR VENDEDOR</a><br>
        </div><!-- btn_cont -->
    </form>
</div> <!-- col 12 -->