<?php $this->Frontend->js('Frontend/js/profile'); ?>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
        <h2>Editar Perfil</h2>
      
        <form id="sale_form" method="POST" class="form_agregar_ventas">
            <div class="row">
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('document')?>">
                    <label for="exampleInputEmail1">Nro. de Documento</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('document')?>" name="document" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('document')?></span>
                </div>
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('password')?>">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password" value="<?=$this->Frontend->inputVal('password')?>" name="password" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('password')?></span>
                </div>
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('first_name')?>">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('first_name')?>" name="first_name" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('first_name')?></span>
                </div>
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('last_name')?>">
                    <label for="exampleInputEmail1">Apellido</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('last_name')?>" name="last_name" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('last_name')?></span>
                </div>
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('email')?>">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('email')?>" name="email" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('email')?></span>
                </div>
                
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('telephone')?>">
                    <label for="exampleInputEmail1">Telefono</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('telephone')?>" name="telephone" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('telephone')?></span>
                </div>

                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('birthday')?>">
                    <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                    <input data-date-language="es" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-provide="datepicker" type="text" value="<?=$this->Frontend->inputVal('birthday')?>" name="birthday" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('birthday')?></span>
                </div>
		<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('telephone_zimple')?>">
                    <label for="exampleInputEmail1" id='zimple_phone'>Telefono con Zimple</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('telephone_zimple')?>" name="telephone_zimple" class="form-control"  >
                    <span class="label label-danger"><?=$this->Frontend->errors('telephone_zimple')?></span>
                </div>
		<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('picture')?>">
                    <label for="exampleInputEmail1">Foto</label>
                    <input type="hidden" value="<?=$this->Frontend->inputVal('picture')?>" name="picture" class="form-control" id="exampleInputEmail1" >
                    <input style="color:#000;cursor:pointer;" type="text" placeholder="Seleccionar Archivo" readonly name="picture_facade" class="form-control" id="exampleInputEmail1" >
                    <input style="position:absolute;top:-1000px;left:-1000px" type="file" name="picture_trigger" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('picture')?></span>
                </div>
            </div><!-- row -->
        </form>
    </div><!-- col -->
</div><!-- row -->
  
<div class="row">
    <div class="btn_cont">
        <a class="btn btn-default editbtn"><i class="fa fa-edit" aria-hidden="true"></i> ACTUALIZAR PERFIL</a><br>
    </div><!-- btn_cont -->
</div><!-- row -->