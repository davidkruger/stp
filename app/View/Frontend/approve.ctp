<?php $this->Frontend->js('Frontend/js/approve'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Aprobar Venta</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de aprobar esta venta?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="sale btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>


<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
        <h2>Aprobar Venta</h2>
      
        <form id="approve_form" method="POST" class="form_agregar_ventas">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">Codigo</label>
                    <?=$this->Frontend->feed('payload.data.***foreign***.product_id')?>
                </div>
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">Vendedor</label>
                    <?=$this->Frontend->feed('payload.data.***foreign***.salesman_id')?>
                    
                </div>
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">Factura</label>
                    <?=$this->Frontend->feed('payload.data.invoice')?>
                </div>
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">Cantidad</label>
                    <?=$this->Frontend->feed('payload.data.quantity')?>
                </div>
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">Fecha</label>
                    <?=$this->Frontend->feed('payload.data.sale_date')?>
                </div>
                <div class="col-lg-10 col-lg-offset-1 ">
                    <label for="exampleInputPassword1">En Stock?</label>
                    <?=$this->Frontend->feed('payload.data.has_stock') ? 'Si' : 'No'?> 
                </div>
            </div><!-- row -->
        </form>
    </div><!-- col -->
</div><!-- row -->

<div class="row">
    <div class="btn_cont">
        <a class="btn btn-default boton_login" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-check-circle" aria-hidden="true"></i> APROBAR VENTA</a><br>
    </div><!-- btn_cont -->
</div><!-- row -->