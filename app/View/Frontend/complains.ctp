<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
        <h2>Consultas</h2>
        <form method="POST" class="form_consultas">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('subject')?>">
                    <label for="exampleInputEmail1">Asunto</label>
                    <input type="text" value="<?=$this->Frontend->inputVal('subject')?>" name="subject" class="form-control" id="exampleInputEmail1" >
                    <span class="label label-danger"><?=$this->Frontend->errors('subject')?></span>
                </div>
                <div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('message')?>">
                    <label for="exampleInputEmail1">Mensaje</label>
                    <textarea class="form-control" name="message" rows="4"><?=$this->Frontend->inputVal('message')?></textarea>
                    <span class="label label-danger"><?=$this->Frontend->errors('message')?></span>
                </div>
            </div><!-- row -->
        </form>
    </div><!-- col -->
  </div><!-- row -->
  
  <div class="row">
    <div class="btn_cont">
        <a class="btn btn-default boton_login"><i class="fa fa-check" aria-hidden="true"></i> ENVIAR MENSAJE</a><br>
        <!--<a href="vendedores_vista.php" class="linkblanco">Cancelar y volver</a>-->
    </div><!-- btn_cont -->
  </div><!-- row -->
  
  <?php $this->Frontend->js('Frontend/js/complains')?>