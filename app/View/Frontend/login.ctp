<div class="col-sm-6 col-sm-offset-3 form_container">
    <h1>Iniciar Sesión</h1>
    <form method="post" action="<?=$this->Api->feedUrl('gateway.clients.login')?>">
        
        <div class="form-group">
            <div class="col-xs-12">
                <label><?=$this->Login->resolvInputPlaceholder($this->Api->feed('entity.schema'), 'document')?></label>
            </div>
            <div style="margin-bottom: 20px" class="col-xs-12">
                <input 
                    class="form-control"
                    name="<?=$this->Login->resolvInputName($this->Api->feed('entity.schema'), 'document')?>"
                    type="<?=$this->Login->resolvInputType($this->Api->feed('entity.schema'), 'document')?>"
                    required="<?=$this->Login->resolvInputRequired($this->Api->feed('entity.schema'), 'document')?>">
            </div>
        </div>

        <div class="form-group"">
            <div class="col-xs-12">
                <label><?=$this->Login->resolvInputPlaceholder($this->Api->feed('entity.schema'), 'password')?></label>
            </div>
            <div style="margin-bottom: 20px" class="col-xs-12">
                <input 
                    class="form-control"
                    name="<?=$this->Login->resolvInputName($this->Api->feed('entity.schema'), 'password')?>"
                    type="<?=$this->Login->resolvInputType($this->Api->feed('entity.schema'), 'password')?>"
                    required="<?=$this->Login->resolvInputRequired($this->Api->feed('entity.schema'), 'password')?>">
            </div>
        </div>

        <div class="btn_cont">
            <button type="submit" class="btn btn-default boton_login">INGRESAR</button>
        </div><!-- btn_cont -->

    </form>
</div> 