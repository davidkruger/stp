<?php $this->Frontend->js('Frontend/js/sale'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ventas</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de agregar esta venta?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="sale btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
        <h2>Agregar Venta</h2>
      
        <form id="sale_form" method="POST" class="form_agregar_ventas">
            <div class="row">
                <?php $this->Frontend->addSale(); ?>
            </div><!-- row -->
        </form>
    </div><!-- col -->
</div><!-- row -->

  
<div class="row">
    <div class="btn_cont">
        <a class="btn btn-default boton_login" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> AGREGAR VENTA</a><br>
    </div><!-- btn_cont -->
</div><!-- row -->