<?php $this->Frontend->js('Frontend/js/sellers'); ?>

<form id="delete_form" method="post">
    <input type="hidden" name="id" />
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Vendedor</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de eliminar este Vendedor?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="seller btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="col-sm-12">
    <h2><?=$this->Frontend->sellersTitle()?></h2>
    <div class="vendedores_lista clearfix">
        <div class="row">
            <?=$this->Frontend->sellers()?> 
        </div><!-- row -->
        
        <div class="row">
            <div class="btn_cont">
                <a class="btn btn-default boton_login" href="<?=Router::url([ 'controller' => 'frontend', 'action' => 'add_seller' ])?>"><i class="fa fa-plus" aria-hidden="true"></i> AGREGAR VENDEDOR</a>
            </div><!-- btn_cont -->
        </div>
    </div><!-- vendedores_lista -->
</div> 