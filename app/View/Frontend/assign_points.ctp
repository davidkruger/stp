<?php $this->Frontend->js('Frontend/js/assign_points'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Canjear</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de asignar estos puntos?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="assign_points btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->


<div class="col-sm-12">
    <div class="big_info text-center">
        <h2>Asignación de puntos</h2>
        <h3>Cantidad de puntos hasta <?=$this->Frontend->feedDatetime('company.updated')?></h3>
        <h1><strong><?=$this->Frontend->feedInteger('company.points')?></strong></h1>
    </div><!-- big_info -->

    <?php $this->Frontend->assignPoints(); ?>
</div> <!-- col 12 -->