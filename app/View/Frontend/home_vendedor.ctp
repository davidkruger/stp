<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Canjear</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de realizar este canje?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Aceptar Desafio</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de aceptar este desafio?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="challenge_title modal-title" id="myModalLabel"></h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <img style="max-width:200px;" class="challenge_img" />
                <h2 class="challenge_description "></h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<?php $this->Frontend->welcome(); ?>

<div class="contenedor_juegos">
    <?php $this->Frontend->roulettes(false); ?>
    <?php $this->Frontend->lotteries(false); ?>
</div>
<div class="col-sm-12">
    <?php $this->Frontend->prizes(); ?>
</div>

<form id="redeem_form" method="post">
    <input type="hidden" name="id" id="redeem_id" />
    <input type="hidden" name="count" id="redeem_count"/>
    <input type="hidden" name="total" id="redeem_total"/>
</form>

<form id="challenge_form" method="post">
    <input type="hidden" name="challenge_id" id="challenge_id" />
</form>

<?php $this->Frontend->js('Frontend/js/roulette'); ?>
<?php $this->Frontend->js('Frontend/js/lottery'); ?>