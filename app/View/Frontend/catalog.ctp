
<!-- Modal -->
<div class="modal fade" id="modaldetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
            <img src="./frontend/gfx/samsung_azul.svg" class="img-responsive">
        </h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <div class="row rowtopmodal">
                <div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
                    <div class="modaltitulo">
                        <h5>Producto</h5>
                        <h2 class="modal-product">TV 55" MU6100</h2> 
                    </div><!-- nodaltitulo --> 
                </div><!-- col  -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-xl-4 text-center">
                    <div class="modalthumb">
                        <div class="modalescudo">
                            <img src="./frontend/gfx/escudo.png" class="img-responsive"/>
                        </div><!-- modal escudo -->
                        <img class="modal-photo img-responsive" src="" />
                    </div><!-- modalthum -->
                </div><!-- col  -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
                    <div class="catalogo_moneda_container">
                        <div class="catalogo_moneda">
                            <img src="./frontend/gfx/plus.svg" class="img-responsive plusverde" />
                            <h3>
                                <span class="spanacu">ACUMULÁS</span>
                                <strong class="modal-points">300</strong>
                                <span class="spanpun">PUNTOS</span>
                            </h3>
                        </div><!-- catalogo moneda -->
                    </div><!-- catalogo_moneda_container -->
                </div><!-- col  -->
            </div><!-- row -->

            <div class="row">
                <div class="col-sm-12 prodtitle2">
                    <h3>Detalles del Producto</h3>
                </div><!-- col -->
            </div><!-- row -->
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 listabeneficios">
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong class="modal-spec">Tamaño</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p class="modal-value">5"</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Resolución</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>1840 x 2160</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>USB</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>2</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>HDMI</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>3</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Motion rate</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>120 Hz</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Salida de sonido</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>20W</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Salida de audio</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6">
                            <p>1 Óptical</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Ethernet (LAN)</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>Sí</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>WIFI</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>Sí</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Bluethooth</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>Sí</p>
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-6 listbene">
                            <p><strong>Tamaño con base</strong></p>
                        </div><!-- col -->
                        <div class="col-xs-6 listvalue">
                            <p>111x71x31,1 cm</p>
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- col -->
                <div class="col-sm-12 col-md-6 col-lg-6 listabeneficios2">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <img class="modal-photo img-responsive" src="./frontend/gfx/bene1.jpg"  />
                        </div><!-- col -->
                        <div class="col-xs-8 col-sm-8">
                            <h5 class="modal-title">La verdadera TV UHD 4K</h5>
                            <p class="modal-desc">Experimenta los detalles más nítidos con cuatro veces más resolución y colores más reales.</p>
                        </div><!-- col -8 -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <img src="./frontend/gfx/bene2.jpg" class="img-responsive" />
                        </div><!-- col -->
                        <div class="col-xs-8 col-sm-8">
                            <h5>Sistema operativo TIZEN</h5>
                            <p>Acceso rápido y simultáneo a las apps mientras sigue viendo su programa favorito.</p>
                        </div><!-- col -8 -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <img src="./frontend/gfx/bene3.jpg" class="img-responsive" />
                        </div><!-- col -->
                        <div class="col-xs-8 col-sm-8">
                            <h5>Smart View</h5>
                            <p>Conecta tu teléfono a la TV y automáticamente reproducí tus contenidos en la TV.</p>
                        </div><!-- col -8 -->
                    </div><!-- row -->
                </div><!-- col -->
            </div><!-- row -->
        </div><!-- container -->
      </div><!-- modal body -->
      
    </div>
  </div>
</div>



<div id="p_catalogo">
    <div class="big_info text-center">
        <div class="container">
            <h1><strong>Catálogo de productos Samsung</strong></h1>
            <h3>Aquí encontrás productos con sus detalles y cantidad de puntos por venta.</h3>
        </div><!-- container -->
    </div><!-- bighero -->

    <div class="row formrow">
        <div class="col-sm-12 text-center">
            <form method="POST">
                <input value="<?=@$this->request->data['name']?>" type="text" class="form-control inputbusqueda" name="name" id="exampleInputName2" placeholder="Búsqueda de producto">
                <button type="submit" class="btnbusqueda"><img src="./frontend/gfx/whitezoom.svg" class="img-responsive" /></button>
            </form>
        </div><!-- col -->
    </div><!-- row -->
    
    <?php $this->Frontend->catalogs(); ?>
    
    <div class="row">
        <div class="col-sm-12 text-center">
            <a href="" class="btnlimpiar">
                Limpiar resultados
            </a>
        </div><!-- col -->
    </div><!-- row -->
  
</div><!-- p_catalogo -->

<script>
    var Catalog = <?=json_encode($this->Frontend->feed('catalogs'))?>;
    <?php $this->Frontend->js('Frontend/catalog_modal'); ?>
</script>