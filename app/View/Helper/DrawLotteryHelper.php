<?php

App::uses('ApiHelper', 'KlezBackend.View/Helper');

class DrawLotteryHelper extends ApiHelper{
    public function getData(){
        return $this->_View->viewVars[DRAW_DATA];
    }
    
    public function deploy(){
        $data = $this->getData();
        $lottery = $data['lottery'];
        echo $this->_View->element('DrawLottery/status', $lottery);
        echo $this->_View->element('DrawLottery/' . $lottery['draw'], $lottery);
    }
    
    public function form(){
        $data = $this->getData();
        
        if(isset($data['winner'])){
            $winner = $data['winner'];
            echo $this->_View->element('DrawLottery/winner', $winner);
        } else {
            $lottery = $data['lottery'];
            echo $this->_View->element('DrawLottery/' . $lottery['draw'] . '_form', $lottery);
        }
    }
    
    public function dt($iso){
        return date('d/m/Y H:i:s',strtotime($iso));
    }
    
    public function tickets(){
        $tickets = $this->feed('tickets');
        $lottery = $this->feed('payload.data');
        
        if(empty($tickets)){
            echo $this->_View->element('DrawLottery/empty_tickets');
        } else {
            echo $this->_View->element('DrawLottery/tr');
            
            foreach($tickets as $tr => $ticket) {                
                $ticket['lottery'] = $lottery;
                $ticket['foreign'] = $ticket['***foreign***'];
                echo $this->_View->element('DrawLottery/ticket', $ticket);
                
                if((1 + $tr) % 3 === 0){
                    echo $this->_View->element('DrawLottery/tr_close');
                    echo $this->_View->element('DrawLottery/tr');
                }
            }
                    
            echo $this->_View->element('DrawLottery/tr_close');
        }
    }
}