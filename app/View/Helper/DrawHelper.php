<?php

App::uses('ApiHelper', 'KlezBackend.View/Helper');

class DrawHelper extends ApiHelper{
    public function getData(){
        return $this->_View->viewVars[DRAW_DATA];
    }
    
    public function deploy(){
        $data = $this->getData();
        $run = $data['run_sale'];
        $winner = $data['winner'];
        echo $this->_View->element('Draw/status', $run);
        
        if(isset($run['draw_at']) === false){
            if($run['draw'] === 'auto'){
                echo $this->_View->element('Draw/auto', $run);
            }
            else if($run['draw'] === 'manual'){
                $score = $run['score'];
                $goal = $run['goal'];
                
                if($score < $goal){
                    echo $this->_View->element('Draw/score_not_reached', $run);
                }
                else{
                    echo $this->_View->element('Draw/manual', $run);
                }
            }
        }
        else if(isset($winner['id'])){
            echo $this->_View->element('Draw/winner', [
                'winner' => $winner,
                'run' => $run
            ]);
        }
        else{
            echo $this->_View->element('Draw/no_winner', [
                'run' => $run
            ]);
        }
    }
    
    public function dt($iso){
        return date('d/m/Y H:i:s',strtotime($iso));
    }
}