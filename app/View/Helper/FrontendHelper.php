<?php

App::uses('ApiHelper', 'KlezBackend.View/Helper');

class FrontendHelper extends ApiHelper{
    private $delayed = [];
    
    public function remaining($points){
        $current = $this->feed('auth.data.points');
        $remaining = $points - $current;
        
        if($remaining > 0){        
            echo $this->_View->element('Frontend/remaining',[
                'points' => $remaining
            ]);
        }
    }
    
    public function timestamp($iso){
        return strtotime($iso);
    }
    
    public function renderMenu(){
        $this->renderThisMenu($this->feed('menu'));
    }
    
    public function renderThisMenu($config){
        foreach($config as $menu){
            if(isset($menu['dropdown'])){
                echo $this->_View->element('Frontend/dropdown',$menu);
            }
            else{
                echo $this->_View->element('Frontend/menu',$menu);
            }
        }
    }
    
    public function integer($number){   
        return number_format($number,0,',','.');
    }
    
    public function timepad($number){
        return str_pad($number, 2, '0', STR_PAD_LEFT);
    }
    
    public function catalogs(){
        $catalogs = $this->feed('catalogs');
        
        if(empty($catalogs['divisions']) === false){
            foreach($catalogs['divisions'] as $i => $catalog){
                if(!isset($catalog['division'])){
                    $catalog['division'] = 'Otros Productos';
                }
                
                $catalog['index'] = $i;
                echo $this->_View->element('Frontend/catalog', $catalog);
            }
        }
    }
    
    public function catalogProducts($products){
        foreach($products as $i => $product){
            $product['index'] = $i;
            echo $this->_View->element('Frontend/catalog_product', $product);
        }
    }
    
    public function welcome() {
        echo $this->_View->element('Frontend/welcome');
    }
    
    public function roulettes($showEmpty = true){
        $roulettes = array_filter($this->feed('roulettes'), function($roulette) {
           return !empty($roulette['prizes']) ;
        });
        
        if(empty($roulettes) === false){
            foreach($roulettes as $roulette){
                echo $this->_View->element('Frontend/roulette', $roulette);
            }
        } else if($showEmpty) {
            echo $this->_View->element('Frontend/empty_roulettes');
        }
    }
    
    public function lotteries($showEmpty = true){
        $lotteries = $this->feed('lotteries');
        
        if(empty($lotteries) === false){
            foreach($lotteries as $lottery){
                echo $this->_View->element('Frontend/lottery', $lottery);
            }
        } else if($showEmpty) {
            echo $this->_View->element('Frontend/empty_lotteries');
        }
    }

    public function challenges() {
        $ch = $this->feed('challenges');
        $challenges =  array_filter(is_array($ch) ? $ch : [], function($a) {
            return !isset($a['ChallengeUser']['id']);
        });

        if (! empty($challenges)) {
            echo $this->_View->element('Frontend/challenge_list', [
                'challenges' => $challenges,
            ]);
        }
    }

    public function challengePrinter($challenges) {
        foreach($challenges as $challenge) {
            echo $this->_View->element('Frontend/desafio', $challenge['Challenge']);
        }
    }
    
    public function prizes(){

        $prizes = $this->feed('prizes');
        $zimplePhone = $this->feed('auth.data.telephone_zimple');
        
        if(empty($prizes) === false){
            echo $this->_View->element('Frontend/prizes_top');
            
            foreach($prizes as $c){
                if(!isset($c['category'])){
                    $c['category'] = 'Otros Premios';
                }

                if($c['category'] == 'Billetera Electronica'){
                    if($zimplePhone != ''){
                        echo $this->_View->element('Frontend/prize_category', $c);
                    }else{
                        echo $this->_View->element('Frontend/modal_notice');
                        $this->js('Frontend/js/wallet_modal');
                    }
                }else{
                    echo $this->_View->element('Frontend/prize_category', $c);
                    
                }
              
            }
            
            echo $this->_View->element('Frontend/prizes_bottom');
        }
    }
    
    public function categoryPrizes($prizes){

        $current = $this->feed('auth.data.points');

        foreach($prizes as $prize){

            $prize['current']=$current;

            if($prize['amount']>0 && $current >= $prize['minimal']){

                echo $this->_View->element('Frontend/wallet',$prize);

            }else if($prize['amount']>0 && $current<$prize['minimal']){

                echo $this->_View->element('Frontend/wallet_cant_redeem',$prize);

            }else if($current >= $prize['points']){

                echo $this->_View->element('Frontend/prize',$prize);

            }else{

                echo $this->_View->element('Frontend/prize_cant_redeem',$prize);
            }
        }
    }
    
    public function runSales(){
        $runSales = $this->feed('run_sales');
        
        if(empty($runSales)){
            return;
        }
        
        echo $this->_View->element('Frontend/run_sales',[
            'runSales' => $runSales
        ]);
        
        $this->js('Frontend/js/run_sales_time');
    }
    
    public function runSalesPrinter($runSales){
        foreach($runSales as $runSale){
            echo $this->_View->element('Frontend/run_sale', [
                'runSale' => $runSale,
                'time' => $this->resolvRunSaleTime($runSale),
            ]);
        }
    }
    
    public function runSaleSlider($runSale){
        foreach($runSale['products'] as $p){
            echo $this->_View->element('Frontend/run_sale_slider', [
                'id' => $p['product_id'],
                'name' => $p['***foreign***']['product_id'],
            ]);
        }
    }
    
    private function resolvRunSaleTime($runSale){
        $endTS = strtotime($runSale['ends_at']);
        $curTS = time();
        $difTS = $endTS - $curTS;
        return $difTS;
    }
    
    public function runSaleProgress($runSale){
        $score = $runSale['score'];
        $goal = $runSale['goal'];
        
        $perc = floor($score / $goal * 100);
        
        if($perc > 100){
            return 100;
        }
        
        return $perc;
    }
    
    public function challengeButton($challenge){
        $points = $this->feed('auth.data.points');

        if($points >= $challenge['cost']){
            echo $this->_View->element('Frontend/challenge_button',[
                'challenge' => $challenge
            ]);
        }
        else{
            echo $this->_View->element('Frontend/challenge_button_disabled');
        }
    }
    
    public function redeemButton($prize){

        $points = $this->feed('auth.data.points');

        if(isset($prize['amount'])){

            echo $this->_View->element('Frontend/redeem_button_disabled');

        }else{

            if($points >= $prize['points']){
                echo $this->_View->element('Frontend/redeem_button',[
                    'prize' => $prize
                ]);
            }
            else{
                echo $this->_View->element('Frontend/redeem_button_disabled');
            }

        }
    }
    
    public function datable($array){
        foreach($array as $field => $value){
            echo "data-{$field}=\"{$value}\" ";
        }
    }
    
    public function js($path){
        $this->delayed[] = $path;
    }
    
    public function delayed(){
        foreach($this->delayed as $path){
            echo $this->_View->element($path);
        }
    }
    
    public function prizePhotoUrl($id){
        return Router::url([
            'controller' => 'frontend',
            'action' => 'prize_photo',
            'id' => $id
        ]);
    }

    public function challengePrizeImgUrl($id) {
        return Router::url([
            'controller' => 'frontend',
            'action' => 'challenge_prize_photo',
            'id' => $id
        ]);
    }
    
    public function redeemsDeploy(){
        $redeems = $this->feed('redeems');
        $deploy = false;
        
        foreach($redeems as $r){
            if(!empty($r)){
                $deploy = true;
                break;
            }
        }
        
        if($deploy){
            echo $this->_View->element('Frontend/redeems');
        }
        else{
            echo $this->_View->element('Frontend/empty_redeems');
        }
    }
    
    public function redeemsTitle($status){
        $redeems = $this->feed('redeems');
        
        if(empty($redeems[$status]) === false){
            return 'Premios ' . ucfirst($status) . 's';
        }
        else{
            return '';
        }
    }
    public function redeemPrinter($redeems) {
        $prizes = [];

        foreach ($redeems as $redeem) {
            $prizes[] = $redeem['prize'];
        }

        $this->categoryPrizes($prizes);
    }

    public function redeemsWelcome() {
        $redeems = $this->feed('redeems');
        
        if(empty($redeems['aprobado'])){
            echo $this->_View->element('Frontend/redeems_welcome_empty');
            return;
        }

        echo $this->_View->element('Frontend/redeems_welcome',[
            'redeems' => $redeems['aprobado']
        ]);
    }
    
    public function redeems($status){
        $redeems = $this->feed('redeems');
        
        if(isset($redeems[$status]) === false){
            return;
        }
        
        if(is_array($redeems[$status]) === false){
            return;
        }
        
        foreach($redeems[$status] as $redeem){
            echo $this->_View->element('Frontend/detail_redeem',[
                'redeem' => $redeem
            ]);
        }
    }
    
    public function errorField($field){
        $messages = $this->feed("payload.messages.{$field}",false);
        
        if(isset($messages)){
            return 'muestraerror';
        }
        
        return '';
    }
    
    public function errors($field){
        $messages = $this->feed("payload.messages.{$field}",false);
        
        if(is_array($messages)){
            $text = '';
            
            foreach($messages as $message){
                $text .= $message . ' ';
            }
            
            return $text;
        }
        
        return '&nbsp;';
    }
    
    public function inputVal($field){
        $val = $this->feed("payload.data.{$field}",false);
        
        if(isset($val)){
            return $val;
        }
        
        return '';
    }
    
    public function inputIntegerVal($field){
        $val = $this->inputVal($field);
        
        if($val === ''){
            return 0;
        }
        
        return $val;
    }
    
    public function salesTitle(){
        $c = $this->feed('payload.count');
        
        if($c > 0){
            return 'Ventas';
        }
        else{
            return 'No hay Ventas';
        }
    }
    
    public function pendingSalesTitle(){
        $c = $this->feed('payload.count');
        
        if($c > 0){
            return 'Ventas Pendientes';
        }
        else{
            return 'No hay Ventas Pendientes';
        }
    }
    
    public function pendingSales(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        if(empty($data) === false){
            echo $this->_View->element('Frontend/pending_sales_' . $role,$this->feed('payload'));
        }
        else if($this->feed('payload.count') > 0){
            $payload = $this->feed('payload');
            $this->_View->FrontendShow->pagination($payload['page'],$payload['count'],0,$payload['limit']);
        }
        
        echo $this->_View->element('Frontend/sales_button');
    }
    
    public function rejected(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        if(empty($data) === false){
            echo $this->_View->element('Frontend/rejected_' . $role,$this->feed('payload'));
        }
        else if($this->feed('payload.count') > 0){
            $payload = $this->feed('payload');
            $this->_View->FrontendShow->pagination($payload['page'],$payload['count'],0,$payload['limit']);
        }
        
        echo $this->_View->element('Frontend/sales_button');
    }
    
    public function sales(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        if(empty($data) === false){
            echo $this->_View->element('Frontend/sales_' . $role,$this->feed('payload'));
        }
        else if($this->feed('payload.count') > 0){
            $payload = $this->feed('payload');
            $this->_View->FrontendShow->pagination($payload['page'],$payload['count'],0,$payload['limit']);
        }
        
        echo $this->_View->element('Frontend/sales_button');
    }
    
    public function pendingSalesData(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        foreach($data as $datarow){
            $product = $datarow['data']['***foreign***']['product_id'];
            $salesman = $datarow['data']['***foreign***']['salesman_id'];
            $points = $datarow['data']['points'];
            $date = date('d/m/Y',strtotime($datarow['data']['sale_date']));
            $quantity = $datarow['data']['quantity'];
            $approval = $this->resolvApproval($datarow['data']['approval']);
            $invoice = $datarow['data']['invoice'];
            $id = $datarow['data']['id'];
            $perc = $auth['extra'];
            
            echo $this->_View->element('Frontend/pending_sale_row_' . $role,[
                'date' => $date,
                'product' => $product,
                'quantity' => $quantity,
                'salesman' => $salesman,
                'invoice' => $invoice,
                'approval' => $approval,
                'points' => $points,
                'perc' => $perc,
                'id' => $id
            ]); 
        }
    }
    
    public function salesData(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        foreach($data as $datarow){
            $product = $datarow['data']['***foreign***']['product_id'];
            $salesman = $datarow['data']['***foreign***']['salesman_id'];
            $date = date('d/m/Y',strtotime($datarow['data']['sale_date']));
            $quantity = $datarow['data']['quantity'];
            $approval = $this->resolvApproval($datarow['data']['approval']);
            $invoice = $datarow['data']['invoice'];
            
            echo $this->_View->element('Frontend/sale_row_' . $role,[
                'date' => $date,
                'product' => $product,
                'quantity' => $quantity,
                'salesman' => $salesman,
                'invoice' => $invoice,
                'approval' => $approval
            ]); 
        }
    }
    public function rejectedData(){
        $data = $this->feed('payload.data');
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        foreach($data as $datarow){
            $product = $datarow['data']['***foreign***']['product_id'];
            $salesman = $datarow['data']['***foreign***']['salesman_id'];
            $date = date('d/m/Y',strtotime($datarow['data']['sale_date']));
            $quantity = $datarow['data']['quantity'];
            $rejectionComments = $datarow['data']['rejection_comments'];
            $invoice = $datarow['data']['invoice'];
            
            echo $this->_View->element('Frontend/rejected_row_' . $role,[
                'date' => $date,
                'product' => $product,
                'quantity' => $quantity,
                'salesman' => $salesman,
                'invoice' => $invoice,
                'rejectionComments' => $rejectionComments
            ]); 
        }
    }
    
    public function addSale(){
        $auth = $this->feed('auth.data');
        $role = $auth['role'];
        
        echo $this->_View->element('Frontend/add_sale_' . $role);        
    }
    
    public function salesmen($data){
        $currentVal = $this->inputVal('salesman_id');    
        
        foreach($data as $op){
            $value = $op['id'];
            $selected = $currentVal === $value;
            $label = "{$op['full_name']} ({$op['document']})";
            
            echo $this->_View->element('Frontend/option',[
                'value' => $value,
                'label' => $label,
                'selected' => $selected ? 'selected' : ''
            ]);
        }
    }
    
    public function branches($data){
        $currentVal = $this->inputVal('branch_id');    
        
        foreach($data as $op){
            $value = $op['id'];
            $selected = $currentVal === $value;
            $label = "{$op['name']}";
            
            echo $this->_View->element('Frontend/option',[
                'value' => $value,
                'label' => $label,
                'selected' => $selected ? 'selected' : ''
            ]);
        }
    }
    
    public function products($data){
        $currentVal = $this->inputVal('product_id');    
        
        foreach($data as $op){
            $value = $op['id'];
            $selected = $currentVal === $value;
            $label = "{$op['name']} ({$op['code']})";
            
            echo $this->_View->element('Frontend/option',[
                'value' => $value,
                'label' => $label,
                'selected' => $selected ? 'selected' : ''
            ]);
        }
    }
    
    public function documentTypes($types){
        $currentVal = $this->inputVal('document_type');    
        
        foreach($types as $type => $label){
            $selected = $currentVal === $type;
            
            echo $this->_View->element('Frontend/option',[
                'value' => $type,
                'label' => $label,
                'selected' => $selected ? 'selected' : ''
            ]);
        }
    }
    
    public function sellersTitle(){
        $sellers = $this->feed('sellers');
        
        
        if(empty($sellers) === false){
            return 'Vendedores';
        }
        else{
            return 'No hay Vendedores';
        }
    }
    
    public function sellers(){
        $sellers = $this->feed('sellers');
        
        if(empty($sellers) === false){
            foreach($sellers as $seller){
                echo $this->_View->element('Frontend/seller',$seller);
            }
        }
    }
    
    public function assignPoints(){
        $sellers = $this->feed('sellers');
        
        if(empty($sellers) === false){
            echo $this->_View->element('Frontend/assign_points');
        }
        else{
            echo $this->_View->element('Frontend/assign_points_empty');
        }
    }
    
    public function sellerPoints(){
        $sellers = $this->feed('sellers');
        
        if(empty($sellers) === false){
            foreach($sellers as $seller){
                $seller['form_id'] = "seller_{$seller['id']}";
                echo $this->_View->element('Frontend/seller_points',$seller);
            }
        }
    }
    
    public function massive(){
        $config = $this->feed('payload');
        $massive = [];
        
        if(isset($config['massive'])){
            $massive = $config['massive'];
        }
        
//        print_r($massive);exit;
        
        if(!empty($massive)){
            echo $this->_View->element('Frontend/massive_csv', [
                'csv' => $massive
            ]);
        }
        else{
            echo $this->_View->element('Frontend/massive_form');
        }
    }
    
    public function csvData($csv){
        foreach($csv as $datarow){
            $product = 'N/D';
            $company = 'N/D';
            $salesman = 'N/D';
            
            if(isset($datarow['data']['***foreign***']['product_id'])){
                $product = $datarow['data']['***foreign***']['product_id'];
            }
            
            if(isset($datarow['data']['***foreign***']['salesman_id'])){
                $salesman = $datarow['data']['***foreign***']['salesman_id'];
            }
            
            if(isset($datarow['data']['***foreign***']['company_id'])){
                $company = $datarow['data']['***foreign***']['company_id'];
            }
            
            $quantity = $datarow['data']['quantity'];
            $invoice = $datarow['data']['invoice'];
            $date = date('d/m/Y',strtotime($datarow['data']['sale_date']));
            $points = $datarow['data']['points'];
            $stock = $datarow['data']['stock_warning'];
            
            echo $this->_View->element('Frontend/massive_row',[
                'date' => $date,
                'product' => $product,
                'quantity' => (int)$quantity,
                'salesman' => $salesman,
                'invoice' => $invoice,
                'points' => (int)$points,
                'company' => $company,
                'stock' => $stock,
                'icon' => empty($datarow['validation']) ? 'check' : 'ban',
                'validation' => $datarow['validation']
            ]); 
        }
    }
    
    public function massiveValidation($validations,$field){
        if(isset($validations[$field])){
            $message = '';
            
            foreach($validations[$field] as $msg){
                $message .= $msg . '. ';
            }
            
            echo $this->_View->element('Frontend/massive_validation',[
                'message' => trim($message),
            ]); 
        }
    }
    
    private function resolvApproval($appr){
        if($appr === 'responsable') return 'Encargado';
        return ucfirst($appr);
    }
    
    public function achievements(){
        $achievements = $this->feed('achievements');
        
        if(empty($achievements)){
            return;
        }

        echo $this->_View->element('Frontend/achievements',[
            'achievements' => $achievements,
        ]);
    }
    
    public function printAchievements($achievements){

        $class_groups = array();
        foreach($achievements as $a) {
            $group = $a['Achievement']['class_group'];
         
            if(!isset($class_groups[$group])){
                $class_groups[$group]=array();
            }
            $class_groups[$group][]=$a;
            
        }
        foreach($class_groups as $group => $desc ){
        
            echo "<div class='achievementSlide'>";
            echo '<div style="width:100%;maring-bottom:10px;">Logros '.$group.'</div>';

            foreach($class_groups[$group] as $a){
                echo $this->_View->element('Frontend/achievement',[
                    'achievement' => $a,
                ]);
            }
            echo "</div>";
        }
        echo '</div>';

      
    }

    public function achievementPerc($achievement){
        $target = (int) $achievement['Achievement']['target'];
        $points = (int) $achievement['AchievementUser']['points'];
        $perc   = $points < $target ? (
            100 * ($points / $target)
        ) : 100;

        return floor($perc);
    }
    public function achievementName($achievement){
        $name = $achievement['Achievement']['name'];
        return $name;        
    }

    public function achievementIconUrl($achievement){
        $id   = $achievement['Achievement']['id'];
        $perc = $this->achievementPerc($achievement);
        $icon = $perc >= 100 ? 1 : 0;

        return Router::url([
            'controller' => 'frontend',
            'action' => "achievement_icon_$icon",
            'id' => $id,
            '?' => [
                't' => strtotime($achievement['Achievement']['updated']),
            ]
        ]);
    }

    public function company() {
        $company = $this->feed('auth.data.company', false);
        $branch = $this->feed('auth.data.branch', false);

        if ($company) {
            echo $this->_View->element('Frontend/company', [
                'company' => $company,
                'branch' => $branch,
            ]);
        }
    }

    public function challenge() {
        $ch = $this->feed('challenges');
        $challenges =  array_filter(is_array($ch) ? $ch : [], function($a) {
            return isset($a['ChallengeUser']['id']);
        });

        if (empty($challenges)) {
            echo $this->_View->element('Frontend/challenge_empty');
        } else {
            foreach($challenges as $challenge) {
                echo $this->_View->element('Frontend/challenge', [
                    'challenge' => $challenge['Challenge'],
                    'progress'  => $challenge['ChallengeUser'],
                ]);
            }
        }
    }
}