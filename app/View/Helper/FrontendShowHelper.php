<?php

App::uses('ShowHelper', 'Klezkaffold.View/Helper');

class FrontendShowHelper extends ShowHelper{
    protected function element($name,$vars = []){
        return $this->_View->element("Frontend/show/{$name}",$vars);
    }
}
