<?php

App::uses('ApiHelper', 'KlezBackend.View/Helper');

class ReportsHelper extends ApiHelper{
    private $totals = [
        'acum' => 0,
        'redeemed' => 0,
        'delta' => 0
    ];
    
    public function getData(){
        return isset($this->_View->viewVars[REPORTS_DATA]) ? $this->_View->viewVars[REPORTS_DATA] : [];
    }
    
    public function deploy(){
        $data = $this->getData();
        if(!isset($data['report'])){
            return;
        }
        
        switch ($this->request['action']){
            case 'total':
            case 'companies':
                echo $this->_View->element('Reports/summary', $data);
                break;
            case 'products':
                echo $this->_View->element('Reports/products', $data);
                break;
            case 'top10general':
            case 'top10lastmonth':
            case 'top10division':
            case 'top10divisionlastmonth':
                echo $this->_View->element('Reports/top10', $data);
                break;
        }
        
    }
    
    public function date($iso){
        return date('d/m/Y',strtotime($iso));
    }
    
    public function integer($c){
        return number_format($c,0,',','.');
    }
    
    public function top10($report){
        foreach($report as $data){
            $this->totals = [
                'acum' => 0,
                'redeemed' => 0,
                'delta' => 0
            ];
            echo $this->_View->element('Reports/cards', $data);
        }
    }
    
    public function products($report){
        foreach($report as $data){
            $this->totals = [
                'acum' => 0,
                'redeemed' => 0,
                'delta' => 0
            ];
            echo $this->_View->element('Reports/product_cards', $data);
        }
    }
    
    public function summaryCards($data){
        foreach($data as $company){
            $this->totals = [
                'acum' => 0,
                'redeemed' => 0,
                'delta' => 0
            ];
            
            echo $this->_View->element('Reports/summary_cards', $company);
        }
    }
    
    public function productRows($salesmen){
        foreach($salesmen as $salesman){
            $this->totals['acum'] += $salesman['acum'];
            
            echo $this->_View->element('Reports/product_rows', $salesman);
        }
    }
    
    public function summaryRows($salesmen){
        foreach($salesmen as $salesman){
            $this->totals['redeemed'] += $salesman['redeemed'];
            $this->totals['acum'] += $salesman['acum'];
            
            echo $this->_View->element('Reports/summary_rows', $salesman);
        }
        
        $this->totals['delta'] = $this->totals['acum'] - $this->totals['redeemed'];
    }
    
    public function top10Rows($salesmen){
        foreach($salesmen as $salesman){
            $this->totals['acum'] += $salesman['acum'];
            
            echo $this->_View->element('Reports/top10_rows', $salesman);
        }
    }
    
    public function getTotal($key){
        return $this->totals[$key];
    }
}