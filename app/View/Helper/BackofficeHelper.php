<?php

App::uses('ApiHelper', 'KlezBackend.View/Helper');

class BackofficeHelper extends ApiHelper{
    public function getData(){
        return $this->_View->viewVars[BACKOFFICE_DATA];
    }
    
    public function productSearchResults(){
        $data = $this->getData();
        
        if(isset($data['products']) && !empty($data['products'])){
            echo $this->_View->element('Products/results', $this->getData());
        }
    }
    
    public function products($products){
        foreach($products as $product){
            echo $this->_View->element('Products/product', $product);
        }
    }
    
    public function productSpecs($specs,$catalogs){
        if(empty($specs)){
            return;
        }
        
        echo $this->_View->element('Products/product_specs', [
            'weight' => 6 * (empty($catalogs) ? 2 : 1),
            'specs' => $specs
        ]);
    }
    
    public function productSpecsPrinter($specs){
        foreach($specs as $spec){
            echo $this->_View->element('Products/product_spec', $spec);
        }
    }
    
    public function productCatalogs($specs,$catalogs){
        if(empty($catalogs)){
            return;
        }
        
        echo $this->_View->element('Products/product_catalogs', [
            'weight' => 6 * (empty($specs) ? 2 : 1),
            'catalogs' => $catalogs
        ]);
    }
    
    public function productCatalogsPrinter($catalogs){
        foreach($catalogs as $catalog){
            echo $this->_View->element('Products/product_catalog', $catalog);
        }
    }
}