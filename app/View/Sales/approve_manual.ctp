<?php if(isset($warning)){ ?>
    <div class="row">
        <div class="row warning-stock">
            <div class="col-md-12">
                <div class="card-box">

                    <h4 class="header-title m-t-0 m-b-30">Advertencia de Stock</h4>

                    <p>
                        La cantidad requerida <b>(<?=$warning['needed']?>)</b> es superior al Stock reportado <b>(<?=$warning['current']?>)</b>
                                        en la empresa requerida.
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php $detail = $this->Api->feed('payload'); $detail['links'] = []; ?>
<?php $this->Detail->deploy($detail); ?>

<form method="POST">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php $this->Form->submit(); ?>
                        </div>
                    </div>
                </div>   
            </div>             
        </div>
    </div>
</FORM>