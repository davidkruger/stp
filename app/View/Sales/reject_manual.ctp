
<?php $detail = $this->Api->feed('payload'); $detail['links'] = []; ?>
<?php $this->Detail->deploy($detail); ?>

<form method="POST">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12 form-holder" id="holder_for_name">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Motivo del Rechazo</h3>
                            </div>
                            <div class="panel-body">
                                <p></p>
                                
                                <div class="input-group m-t-10">
                                    <textarea type="text" class="form-control" value="<?=@$this->data['rejection_comments']?>" name="rejection_comments"></textarea>
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                </div>                            
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php $this->Form->submit(); ?>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</FORM>