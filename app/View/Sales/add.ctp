<?php $this->Form->deploy($this->Api->feed('payload')); ?>

<script>
    $(function(){
        var quantity = $('input[name="quantity"]');
        var product = $('input[name="product_id"]');
        var company = $('input[name="company_id"]');
        
        var Stocking = {
            xhr : null,
            trigger : function(){
                var p = product.val();
                var q = quantity.val();
                var c = company.val();
                var data = {
                    stock : true,
                    product_id : p,
                    company_id : c,
                    quantity : q
                };
                
                if(p == "" || q == "" || c == ""){
                    toastr.clear();
                    return;
                }

                if(Stocking.xhr !== null){
                    Stocking.xhr.abort();
                }
                
                Stocking.xhr = $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    data : data,
                    success : function(data){
                        if(data.warning){
                            toastr.clear();
                            toastr.warning('La empresa especificada no cuenta con el stock suficiente','Advertencia de Stock', {
                                timeOut: 0,
                                extendedTimeOut: 0
                            });
                        }
                        else{
                            toastr.clear();
                        }
                    }
                });
            }
        };
        
        quantity.on('input',Stocking.trigger);
        product.on('change', Stocking.trigger);
        company.on('change', Stocking.trigger);
        
        Stocking.trigger();
    });
</script>
