<div class="account-pages"></div>

<div class="clearfix"></div>

<div class="wrapper-page">
    <div class="text-center">
        <a href="<?=$this->Api->feedUrl('gateway.clients.login')?>" class="logo"><img src="<?=$this->Html->url('/img/logo_samsung.png')?>" /></a>
        <h5 class="text-muted m-t-0 font-600">Acceso al Backoffice</h5>
    </div>
        <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Inicio de Sesión</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="post" action="<?=$this->Api->feedUrl('gateway.clients.login')?>">
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input 
                                class="form-control"
                                name="<?=$this->Login->resolvInputName($this->Api->feed('entity.schema'), 'document')?>"
                                type="<?=$this->Login->resolvInputType($this->Api->feed('entity.schema'), 'document')?>"
                                required="<?=$this->Login->resolvInputRequired($this->Api->feed('entity.schema'), 'document')?>"
                                placeholder="<?=$this->Login->resolvInputPlaceholder($this->Api->feed('entity.schema'), 'document')?>">
                        </div>
                    </div>
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input 
                                class="form-control"
                                name="<?=$this->Login->resolvInputName($this->Api->feed('entity.schema'), 'password')?>"
                                type="<?=$this->Login->resolvInputType($this->Api->feed('entity.schema'), 'password')?>"
                                required="<?=$this->Login->resolvInputRequired($this->Api->feed('entity.schema'), 'password')?>"
                                placeholder="<?=$this->Login->resolvInputPlaceholder($this->Api->feed('entity.schema'), 'password')?>">
                        </div>
                    </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Ingresar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>