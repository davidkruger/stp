<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="google" content="notranslate">
        <meta http-equiv="Content-Language" content="es">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?=$this->Html->url('/klez_backend/assets/images/favicon.ico')?>">

        <title><?=$this->Backend->feed('title')?></title>
        
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')?>" rel="stylesheet" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/multiselect/css/multi-select.css')?>"  rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/css/select2.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/css/select2-bootstrap.css')?>" rel="stylesheet" type="text/css">
        
        <link href="<?=$this->Html->url('/klez_backend/assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/core.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/components.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/icons.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/menu.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/responsive.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klezkaffold/css/dashboard.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/switchery/switchery.min.css')?>" rel="stylesheet" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/jquery.dataTables.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/buttons.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/responsive.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/scroller.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/css/custom.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/custombox/dist/custombox.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-sweetalert/sweet-alert.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="<?=$this->Html->url('/klez_backend/assets/js/modernizr.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/chart.js/chart.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/custombox/dist/custombox.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/custombox/dist/legacy.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery-resizer.js')?>"></script>

    </head>

    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <a href="<?=$this->Backend->feedUrl('home')?>" class="logo"><img src="<?=$this->Html->url('/img/logo_samsung.png')?>" /><i class="zmdi zmdi-layers"></i></a>
                </div>

                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h1 class="page-title"><?php $this->Backend->breadcrumb()?></h1>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="hidden-xs">
                                    <h5><b>Perfil:</b> <?=$this->Api->feed('auth.data.role')?></h5>

<!--                                <form role="search" class="app-search" method="post" action="<?=$this->Html->url([ 'controller' => 'backend', 'action' => 'search' ])?>">
                                    <input name="q" placeholder="Búsqueda..." class="form-control" type="text">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <div class="user-img">
                            <img src="<?=$this->Backend->feedUrl('avatar')?>?t=<?=$this->Api->feed('auth.data.picture')?>&a=<?=md5(Configure::read('Security.salt') . $this->Api->feed('auth.data.id'))?>" title="<?=$this->Api->feed('auth.data.email')?>" class="img-circle img-thumbnail img-responsive">
                        </div>
                        <h5><a href="#"><?=$this->Api->feed('auth.data.first_name')?> <?=$this->Api->feed('auth.data.last_name')?></a> </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="<?=$this->Backend->feedUrl('profile_edit')?>" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="<?=$this->Backend->feedUrl('logout')?>" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                    <div id="sidebar-menu">
                        <?php $this->Menu->deploy($this->Api->feed('menu')); ?>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <?=$this->fetch('content'); ?>
                    </div>
                </div>

                <footer class="footer text-right">
                    <?=$this->Backend->feed('footer') ?>
                </footer>
            </div>

        </div>
        
        <script>
            var resizefunc = [];
        </script>
        
        <script src="<?=$this->Html->url('/klez_backend/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/detect.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/fastclick.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/waves.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.nicescroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.js')?>"></script>

        <!--[if IE]>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-knob/excanvas.js')?>"></script>
        <![endif]-->
                
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')?>"></script>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/multiselect/js/jquery.multi-select.js')?>"></script>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/js/select2.min.js')?>" type="text/javascript"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/switchery/switchery.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/waypoints/lib/jquery.waypoints.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/counterup/jquery.counterup.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-knob/jquery.knob.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.core.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.app.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')?>" type="text/javascript"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/timepicker/bootstrap-timepicker.min.js')?>"></script>
        
        <?php $this->Flash->deploy(); ?>
    </body>
</html>