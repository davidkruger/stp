<div class="data-card-created col-lg-12">
    <div class="panel panel-color panel-inverse">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$name?></h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive" data-pattern="priority-columns">
                <table class="table summary table-striped">
                        <thead>
                            <tr>
                                <th id="tech-companies-1-col-0-clone">Vendedor</th>
                                <th width="10%" style="text-align:right">Acumulado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $this->Reports->productRows($salesmen); ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><b>Totales</b></td>
                                <td width="10%" style="text-align:right"><?=$this->Reports->integer($this->Reports->getTotal('acum'))?></td>
                            </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>