<div class="row">
    <?php $this->Reports->top10($report); ?>

    <form method="post">
        <input type="hidden" name="excel" value="1" />
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <button id="excel" class="btn btn-success waves-effect waves-light" type="submit">
                                <i class="fa fa-download m-r-5"></i> <span>Exportar a Excel</span>
                            </button>
                        </div>
                    </div>
                </div>   
            </div>             
        </div>
    </form>
</div>