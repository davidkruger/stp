<div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Anuncio</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>No olvide registrar su número de billetera Zimple</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary" onclick="window.location.href='/mi_cuenta/editar.html/#zimple_phone'">registrar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
