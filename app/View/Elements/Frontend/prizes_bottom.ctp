</div><!-- catalogo_premios -->


<script>

function zimpleUpdatePoints(amount,interval,id,points,minimal){

    var qty = $("#qty_"+id).val();

    if(qty<=0){
        $("#qty_"+id).val(minimal);
        return;
    }

    if(qty == interval){
        $("#qty_"+id).val(minimal);
    }

    if(qty > points){
        $("#qty_"+id).val(points);
    }

    var actual = (qty*amount);
    var val = (actual).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    $("#total_"+id).html('₲ '+val);
    
    $("#btn_"+id).attr('data-total',val);
    $("#btn_"+id).attr('data-count',qty);
}

</script>


