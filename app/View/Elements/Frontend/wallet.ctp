
<style>
input::-webkit-input-placeholder {
    font-size: 11px;
}
</style>
<div class="col-md-6">
    <div class="catalogo_uno clearfix catalogo_canjeable">
        <div class="catalogo_thumb">
            <img src="<?=$this->Frontend->prizePhotoUrl($id)?>" class="img-responsive" />
        </div>
        <div class="catalogo_info">
        <h4 style="margin-bottom: 5px"><?=$name?></h4>
            <div class="input-group">
                <span class="input-group-addon"style="font-size: 10px;">Puntos</span>
                <input type="number" id="qty_<?=$id?>" class="form-control" placeholder="ingrese cantidad"  onchange="zimpleUpdatePoints(<?=$amount?>,<?=$interval?>,<?=$id?>,<?=$current?>,<?=$minimal?>)" step="<?=$interval?>" min="<?=$minimal?>" max="<?=$current?>">
            </div>

            <div class="subt ch" style="font-size: 12px;">1 punto = ₲ <?=number_format($amount,0,'','.')?></div>

            <div class="subt gr text-center"><p id="total_<?=$id?>" style="font-size: 22px;"></p></div>
            <?php
                if(strlen($restriction)>0){
                ?>
                <small style='color: #70be44;font-size:11px'>Solo valido para <b><?=$restriction?></b></small>
                <?php
                }
           
                if ($current >= $points) {
                    echo '<a href="#" class="btn_canje redeem" data-id="'.$id.'" id="btn_'.$id.'" data-name="'.$name.'" data-photo="'.$photo.'" data-total="" data-count="" >CANJEAR</a>';
                } else {
                    echo '<a href="javascript:void(0)" class="btn_canje" data-toggle="modal" disabled="disabled">CANJEAR</a>';
                }
            ?>
        </div><!-- catalogo_info -->
    </div>
</div>