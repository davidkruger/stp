<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="catalogo_uno clearfix catalogo_canjeable">
        <div class="catalogo_thumb">
            <img src="<?=$this->Frontend->challengePrizeImgUrl($id)?>" class="img-responsive" />
        </div>
        <div class="catalogo_info">
            <h4><?=$title?></h4>
            <div class="catalogo_moneda_container">
                <div class="catalogo_moneda">
                    <h3>
                        <strong><?=$this->Frontend->integer($cost)?></strong>
                        <br><span>puntos</span>
                    </h3>
                </div><!-- catalogo moneda -->
                <div class="catalogo_faltan">
                    <?=$this->Frontend->remaining($cost)?>
                </div><!-- catalogo faltan -->
            </div><!-- CAtalogo moda -->

            <?php $this->Frontend->challengeButton([
                'id' => $id,
                'title' => $title,
                'cost' => $cost,
            ]); ?>
            
        </div><!-- catalogo_info -->
    </div><!-- catalogo_uno -->
</div><!-- col 4 --> 