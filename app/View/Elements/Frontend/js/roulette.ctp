<script>
    var Roulette = function(settings){
        this.modals = {
            intent: settings.modals.intent,
            winner: settings.modals.winner,
        };
        this.canvas = settings.canvas || document.getElementById('canvas');
        this.button = settings.button || document.getElementById('spin');
        this.options = settings.data.prizes || ["$100", "$10", "$25", "$250", "$30", "$1000", "$1", "$200", "$45", "$500", "$5", "$20", "Lose", "$1000000", "Lose", "$350", "$5", "$99"];
        this.startAngle = 0;
        this.arc = Math.PI / (this.options.length / 2);
        this.spinTimeout = null;

        this.spinArcStart = 10;
        this.spinTime = 0;
        this.spinTimeTotal = 0;
        this.freq = 30;
        this.ctx = null;
        this.xhr = null;
        this.font = "bold 14px 'SamsungSharpRegular', Arial";
        this.innerCircle = {
            padding: 12,
            color: '#000000',
            text: '#ffffff'
        };


        this.byte2Hex = function(n) {
          var nybHexString = "0123456789ABCDEF";
          return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
        };

        this.RGB2Color = function(r,g,b) {
                return '#' + this.byte2Hex(r) + this.byte2Hex(g) + this.byte2Hex(b);
        };

        this.getColor = function(item, maxitem) {
          var phase = 0;
          var center = 128;
          var width = 127;
          var frequency = Math.PI*2/maxitem;

          var red   = Math.sin(frequency*item+2+phase) * width + center;
          var green = Math.sin(frequency*item+0+phase) * width + center;
          var blue  = Math.sin(frequency*item+4+phase) * width + center;

          return this.RGB2Color(red,green,blue);
        };

        this.drawRouletteWheel = function() {
          if (this.canvas.getContext) {
            var outsideRadius = 220;
            var textRadius = 176;
            var insideRadius = 138;

            this.ctx = this.canvas.getContext("2d");
            this.ctx.clearRect(0,0,500,500);

            this.ctx.beginPath();
            this.ctx.fillStyle = this.innerCircle.color;
            this.ctx.arc(250, 250, outsideRadius + this.innerCircle.padding, 0, Math.PI * 2, false);
            this.ctx.stroke();
            this.ctx.fill();

            this.ctx.strokeStyle = "white";
            this.ctx.lineWidth = 1;
            this.ctx.font = "bold 14px  Arial";

            for(var i = 0; i < this.options.length; i++) {
              var angle = this.startAngle + i * this.arc;
              //ctx.fillStyle = colors[i];
              this.ctx.fillStyle = this.getColor(i, this.options.length);

              this.ctx.beginPath();
              this.ctx.arc(250, 250, outsideRadius, angle, angle + this.arc, false);
              this.ctx.arc(250, 250, insideRadius, angle + this.arc, angle, true);
              //this.ctx.stroke();
              this.ctx.fill();

              this.ctx.save();
              this.ctx.shadowOffsetX = 0;
              this.ctx.shadowOffsetY = 0;
              this.ctx.shadowBlur    = 0;
              this.ctx.shadowColor   = "rgb(225,225,225)";
              this.ctx.fillStyle = "black";
              this.ctx.translate(250 + Math.cos(angle + this.arc / 2) * textRadius, 250 + Math.sin(angle + this.arc / 2) * textRadius);
              this.ctx.rotate(angle + this.arc / 2 + Math.PI / 2);
              var text = this.options[i].prize;
              this.ctx.fillText(text, -this.ctx.measureText(text).width / 2, 0);
              this.ctx.restore();
            } 

            //Arrow
            this.ctx.fillStyle = "#070d97";
            this.ctx.beginPath();
            this.ctx.moveTo(250 - 4, 250 - (outsideRadius + 5));
            this.ctx.lineTo(250 + 4, 250 - (outsideRadius + 5));
            this.ctx.lineTo(250 + 4, 250 - (outsideRadius - 5));
            this.ctx.lineTo(250 + 9, 250 - (outsideRadius - 5));
            this.ctx.lineTo(250 + 0, 250 - (outsideRadius - 13));
            this.ctx.lineTo(250 - 9, 250 - (outsideRadius - 5));
            this.ctx.lineTo(250 - 4, 250 - (outsideRadius - 5));
            this.ctx.lineTo(250 - 4, 250 - (outsideRadius + 5));
            this.ctx.fill();
          }
        };
        
        this.phasing = function(plant){
            const arcd = this.arc * 180 / Math.PI;
            const target = plant * arcd + arcd / 2;
            const cycles = Math.ceil(this.spinTimeTotal / this.freq);
            let rotation = this.startAngle;
            let spinTime = 30;
            
            for(var i = 0;i < cycles;i++){
                rotation += (this.spinAngleStart - this.easeOut(spinTime, 0, this.spinAngleStart, this.spinTimeTotal)) * Math.PI / 180;
                spinTime += 30;
            }
            
            const degrees = 360 - (rotation * 180 / Math.PI + 90) % 360;
            const rindex = degrees / arcd;
            const index = Math.floor(rindex); 
            const epsilon = 0.05;
            
            if(index === plant) {
                if((Math.trunc(rindex + epsilon) <= index) && (Math.trunc(rindex - epsilon) >= index)) {
                    return this.rotateWheel();
                }
            }
            
            const spinAngle = this.spinAngleStart;
            this.startAngle += (spinAngle * Math.PI / 180);
            this.drawRouletteWheel();
            this.spinTimeout = setTimeout(this.phasing.bind(this, plant), this.freq);

        };

        this.spin = function(plant) {
            //this.spinTimeTotal = 5000;//Math.random() * 3 + 4 * 1000;
            //this.spinAngleStart = 20;//Math.random() * 10 + 10;
            const baseSpeed = 40;
            const baseTime = 4000;
            this.spinAngleStart = (Math.random() * baseSpeed/4 + 1) + baseSpeed;
            this.spinTimeTotal = baseTime + Math.random() * baseTime/2 + 1;
            
            this.spinTime = 0;
            
            console.log(this.spinAngleStart, this.spinTimeTotal);
            this.phasing(plant);
        };

        this.rotateWheel = function() {
          this.spinTime += this.freq;
          
          if(this.spinTime >= this.spinTimeTotal) {
            this.stopRotateWheel();
            return;
          }
          
          var spinAngle = this.spinAngleStart - this.easeOut(this.spinTime, 0, this.spinAngleStart, this.spinTimeTotal);
          this.startAngle += (spinAngle * Math.PI / 180);
          this.drawRouletteWheel();
          this.spinTimeout = setTimeout(this.rotateWheel.bind(this), this.freq);
        };

        this.stopRotateWheel = function() {
          clearTimeout(this.spinTimeout);
          var degrees = 360 - (this.startAngle * 180 / Math.PI + 90) % 360;
          var arcd = this.arc * 180 / Math.PI;
          var index = Math.floor((degrees) / arcd);
          this.ctx.save();
          this.ctx.font = "bold 22px 'SamsungSharpRegular', Arial";
          var text = this.options[index].prize;
          this.ctx.fillStyle = this.innerCircle.text;
          this.ctx.fillText(text, 250 - this.ctx.measureText(text).width / 2, 250 + 10, this.ctx.measureText(text).width);
          this.ctx.restore();
          
          this.winner(index);
        };
        
        this.winner = function(index){
            const prize = this.options[index];
            
            if(prize.redeemable && prize.points > 0){
                const pr = this.modals.winner.find('.prize');
                const redeem = this.modals.winner.find('.redeem');
                pr.html(prize.prize);

                redeem.off('click').on('click', () => {
                    //this.endpoint();
                    this.modals.winner.modal('hide');
                });
                
                this.modals.winner.modal('show');
                this.unlock();
            } else {
                this.unlock();
            }
        }

        this.easeOut = function(t, b, c, d) {
          var ts = (t/=d) * t;
          var tc = ts * t;
          return b + c * (tc + -3 * ts + 3 * t);
        };
        
        this.click = function(){
            if (this.xhr) {
                return;
            }
            
            const points = this.modals.intent.find('.points');
            const redeem = this.modals.intent.find('.redeem');
            points.html(settings.data.points);
            
            redeem.off('click').on('click', () => {
                this.endpoint();
                this.modals.intent.modal('hide');
            });
            this.modals.intent.modal('show');
        };
        
        this.endpoint = function(){
            var self = this;
            
            if (this.xhr) {
                return;
            }
            
            const unlockFn = this.unlock.bind(this);
            
            this.xhr = $.ajax({
                method : 'POST',
                dataType: "json",
                data: {
                    roulette: settings.data.id,
                    timestamp: settings.data.updated,
                    context: 'roulette'
                },
            }).success(function(data){
                const status = data.status || -1;
                
                switch(status){
                    case -1: 
                        toastr.clear();
                        toastr.error('Su sesion ha expirado');
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                        break;
                    case 0: 
                        toastr.clear();
                        toastr.error('Ruleta no vigente');
                        unlockFn();
                        break;
                    case 1:
                        const successFn = self.intent.bind(self, data);
                        successFn();
                        break;
                    case 2:
                        toastr.clear();
                        toastr.error('La ruleta ha sido actualizada');
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                        break;
                    case 3:
                        toastr.clear();
                        toastr.error('No posee los puntos necesarios para participar');
                        unlockFn();
                        break;   
                    default:
                        toastr.clear();
                        toastr.error('Ha ocurrido un error en la conexion al servidor');
                        unlockFn();
                        break;
                }
                
                const authFn = self.updateAuth.bind(self,data.auth);
                authFn();
            }).fail(function(xhr,status,error){
                toastr.clear();
                toastr.error('Ha ocurrido un error en la conexion al servidor');
                unlockFn();
            });
            //this.spin(14);
        }
        
        this.unlock = function(){
            this.xhr = null;
        }
        
        this.intent = function(data){
            const prize = parseInt(data.prize);
            const internal = settings.data.prizes.findIndex(element => {
                return parseInt(element.id) === prize;
            });
            
            if(internal >= 0){
                console.log('Premio:', settings.data.prizes[internal].prize);
                this.spin(internal);
            } else {
                toastr.clear();
                toastr.error('Ha ocurrido un error en la conexion al servidor');
                unlockFn();
            }
        }
        
        this.updateAuth = function(auth) {
            const dt = auth.updated.split(' ');
            const d = dt[0].split('-');
            const ds = d[2] + '/' + d[1] + '/' + d[0] + ' ' + dt[1];
            
            $('#auth_points').html(auth.points.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $('#auth_updated').html(ds);
            $('#auth_name').html(auth.first_name + ' ' + auth.last_name);
        }

        this.drawRouletteWheel();
        this.button.addEventListener("click", this.click.bind(this));
    };
    
    $(function(){
        var roulettes = <?=json_encode($this->Frontend->feed('roulettes'))?>;
        
        roulettes.forEach(roulette => {
            const wheel = $('#wheel_' + roulette.id);
            const canvas = wheel.find('canvas');
            const button = wheel.find('button');
            
            const engine = new Roulette({
                canvas: canvas[0],
                button: button[0],
                data : roulette,
                modals : {
                    intent: $('#rouletteIntentModal'),
                    winner:  $('#rouletteWinnerModal'),
                }
            });
        });
    });
</script>

<!-- Modal -->
<div class="modal fade" id="rouletteIntentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Canjear Ruleta</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Esta seguro que desea realizar este canje por <span class="points"></span> puntos?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<!-- Modal -->
<div class="modal fade" id="rouletteWinnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Felicitaciones!!! Te ganaste: <span class="prize"></span></h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Deseas reclamar este premio?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary">Si</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->