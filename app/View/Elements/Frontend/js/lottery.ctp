<script>
    var Lottery = function(settings){
        this.modals = {
            intent: settings.modals.intent,
        };
        this.click = function(){
            if (this.xhr) {
                return;
            }
            
            const count = parseInt(settings.input.value);
            
            if(isNaN(count)) {
                toastr.clear();
                toastr.error('Debes especificar una cantidad valida de tickets');
                return;
            } else if(count > 100){
                toastr.clear();
                toastr.error('Debes especificar 100 o menos tickets');
                return;
            }
            
            const points = this.modals.intent.find('.points');
            const counter = this.modals.intent.find('.count');
            const redeem = this.modals.intent.find('.redeem');
            points.html(settings.data.points * count);
            counter.html(count + ' ticket' + (count === 1 ? '' : 's'));
            
            redeem.off('click').on('click', () => {
                this.endpoint();
                this.modals.intent.modal('hide');
            });
            this.modals.intent.modal('show');
        };
        
        this.endpoint = function(){
            var self = this;
            
            if (this.xhr) {
                return;
            }
            
            const count = parseInt(settings.input.value);
            const unlockFn = this.unlock.bind(this);
            
            this.xhr = $.ajax({
                method : 'POST',
                dataType: "json",
                data: {
                    lottery: settings.data.id,
                    timestamp: settings.data.updated,
                    count: count,
                    context: 'lottery'
                },
            }).success(function(data){
                const status = data.status || -1;
                
                switch(status){
                    case -1: 
                        toastr.clear();
                        toastr.error('Su sesion ha expirado');
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                        break;
                    case 0: 
                        toastr.clear();
                        toastr.error('Loteria no vigente');
                        unlockFn();
                        break;
                    case 1:
                        toastr.clear();
                        
                        if(count > 1){
                            toastr.success('Has comprado ' + count + ' tickets');
                        } else {
                            toastr.success('Has comprado un ticket');
                        }
                        
                        settings.data.tickets = parseInt(settings.data.tickets) + parseInt(count);
                        settings.tickets.innerHTML = self.ticketsPrint();
                        unlockFn();
                        break;
                    case 2:
                        toastr.clear();
                        toastr.error('La loteria ha sido actualizada');
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                        break;
                    case 3:
                        toastr.clear();
                        toastr.error('No posee los puntos necesarios para participar');
                        unlockFn();
                        break;   
                    default:
                        toastr.clear();
                        toastr.error('Ha ocurrido un error en la conexion al servidor');
                        unlockFn();
                        break;
                }
                
                const authFn = self.updateAuth.bind(self,data.auth);
                authFn();
            }).fail(function(xhr,status,error){
                toastr.clear();
                toastr.error('Ha ocurrido un error en la conexion al servidor');
                unlockFn();
            });
            //this.spin(14);
        };
        
        this.unlock = function(){
            this.xhr = null;
        };
        
        this.updateAuth = function(auth) {
            const dt = auth.updated.split(' ');
            const d = dt[0].split('-');
            const ds = d[2] + '/' + d[1] + '/' + d[0] + ' ' + dt[1];
            
            $('#auth_points').html(auth.points.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $('#auth_updated').html(ds);
            $('#auth_name').html(auth.first_name + ' ' + auth.last_name);
        };
        
        this.input = function(){
            const value = parseInt(settings.input.value);
            
            if(isNaN(value)){
                settings.total.innerHTML = 'Cantidad no valida';
            } else {
                const total = value * parseInt(settings.data.points);
                settings.total.innerHTML = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' puntos';
            }
        };
        
        this.date = function(){
            const endsAt = new Date(settings.data.ends_at);
            const now = new Date();
            
            const delta = endsAt.getTime() - now.getTime();
            const days = Math.floor(delta / 86400000);
            
            if(days <= 1){
                if(endsAt.getFullYear() === now.getFullYear() && endsAt.getMonth() === now.getMonth() && endsAt.getDate() === now.getDate()){
                    return 'Termina hoy';
                } else {
                    return 'Termina mañana';
                }
            } else {
                return 'Faltan ' + days + ' dias';
            }
            
            return endsAt;
        };  
        
        this.ticketsPrint = function(){
            settings.data.tickets = parseInt(settings.data.tickets);
             return settings.data.tickets + (settings.data.tickets === 1 ? ' ticket vendido' : ' tickets vendidos');
        };
        
        settings.tickets.innerHTML = this.ticketsPrint();
        settings.date.innerHTML = this.date();
        settings.button.addEventListener("click", this.click.bind(this));
        settings.input.addEventListener("input", this.input.bind(this));
        this.input();
    };
    
    $(function(){
        var lotteries = <?=json_encode($this->Frontend->feed('lotteries'))?>;
        
        lotteries.forEach(lottery => {
            const container = $('#lottery_' + lottery.id);
            const button = container.find('button');
            const input = container.find('input');
            const total = container.find('.total');
            const date = container.find('.date');
            const tickets = container.find('.tickets');
            
            
            const engine = new Lottery({
                button: button[0],
                input: input[0],
                total: total[0],
                date: date[0],
                tickets: tickets[0],
                data : lottery,
                modals : {
                    intent: $('#lotteryIntentModal'),
                }
            });
        });
    });
</script>

<!-- Modal -->
<div class="modal fade" id="lotteryIntentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Comprar Tickets</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Esta seguro que deseas comprar <span class="count"></span> por <span class="points"></span> puntos?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="redeem btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
