<script>
    $(function(){
        $('div.vendedores_lista a.delete').off('click').on('click',function(e){
            e.preventDefault();
            
            $('#delete_form input').val($(this).attr('data-id'));
            $('#myModal').modal('show');
        });     
        
        $('#myModal button.seller').off('click').on('click',function(){
            $('#delete_form').submit();
            
            console.debug($('#delete_form'));
        });     
    });
</script>