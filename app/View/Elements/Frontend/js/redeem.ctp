<script>
    $(function(){
        $('div.catalogo_premios a.redeem').not('.challenge').off('click').on('click',function(){
            $('#redeem_id').val($(this).attr('data-id'));
            $('#redeem_count').val($(this).attr('data-count'));
            $('#redeem_total').val($(this).attr('data-total'));
            $('#myModal').modal('show');
        });     
        
        $('#myModal button.redeem').off('click').on('click',function(){
            $('#redeem_form').submit();
        });     
  
    });

    $(function(){
        $('div.catalogo_premios a.redeem.challenge').off('click').on('click',function(){
            $('#challenge_id').val($(this).attr('data-id'));
            $('#myModal2').modal('show');
        });     
        
        $('#myModal2 button.redeem').off('click').on('click',function(){
            $('#challenge_form').submit();
        });     
  
    });

    $(function(){
        $('a.modal-challenge').off('click').on('click',function(){
            console.log($(this).attr('challenge-title'))
            console.log($('.challenge_title')   )
            $('.challenge_title').html($(this).attr('challenge-title'));
            $('.challenge_description').html($(this).attr('challenge-description'));
            $('.challenge_img').attr('src',$(this).attr('challenge-img'));
            $('#myModal3').modal('show');
        });      

    });
</script>