
<script>
    $(function(){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        };
        
        $('.klez-datepicker').datepicker({
            format: "dd/mm/yyyy",
            language : 'es',
            autoclose : true
        });
    });
    
    var Engine = {
        delayed : null,
        imagePickerCallback : function(param,request){
            if(request == this.code){
                var image = Android.getImageBase64(param);
                
                if(this.loader === null){
                    this.delayed = {
                        image: image,
                        request: request
                    };
                }
                else{
                    this.loader(image,request);
                }
            }
        },
        loader : null,
        code : 1001
    };
    
    $(function(){
        var facade = $("input[name='picture_facade']");
        var trigger = $("input[name='picture_trigger']");
        var content = $("input[name='picture']");
        
        var message = "Debe ser una foto v\u00e1lida en formato PNG o JPG";
        var mimes = ["image\/jpeg","image\/png"];
        
        facade.on('click', function(){
            if(typeof Android !== 'undefined'){
                Android.imagePicker('photo', Engine.code);
            }
            else{
                trigger.trigger('click');
            }
        });
        
        trigger.on('change',function(){
            var file = document.getElementsByName('picture_trigger');
            getBase64(file[0].files[0]);
        });
                
        Engine.loader = function(image,request){
            content.val(image);
            facade.val('Archivo seleccionado desde Android');
        };
        
        if(Engine.delayed !== null){
            Engine.loader(Engine.delayed.image, Engine.delayed.request);
            Engine.delayed = null;
        }
        
        function getBase64(file){
            if(validateFile(file,mimes)){
                var reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onload = function () {
                    content.val(reader.result);
                    facade.val(file.name + " (" + file.type + ")");
                };

                reader.onerror = function (error) {
                    var kind = 'error';
                    var message = 'Error al recuperar Archivo';
                    toastr[kind](message);
                    content.val('');
                    facade.val('');
                };
            }
            else{
                var kind = 'warning';
                toastr[kind](message);
                content.val('');
                facade.val('');
            }
        }
        
        function validateFile(file,mimes){
            var type = file.type;
            
            for(var i in mimes){
                if(mimes[i] == type){
                    return true;
                }
            }
            
            return false;
        }
    });
    
    $(function(){
        var btn = $('.editbtn');
        var form = $('form');
        
        btn.click(function(){
            form.submit();
        });
    });
</script> 