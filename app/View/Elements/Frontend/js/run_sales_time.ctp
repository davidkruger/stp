<script>
    $(function(){
        var now = new Date();
        
        function strpad(n){
            if(n < 10){
                return '0' + n;
            }
            else if(n < 0){
                return '00';
            }
            
            return n;
        }
        
        $('[data-run-sale]').each(function(){
            var config = {
                day : null,
                hour : null,
                minute : null,
                second : null,
                ends : parseInt($(this).attr('data-ends-at')),
                now : Math.floor(now / 1000)
            };

            config.day = $(this).find('.c_dia h3');
            config.hour = $(this).find('.c_hora h3');
            config.minute = $(this).find('.c_minuto h3');
            config.second = $(this).find('.c_segundo h3');

            setInterval(function(){
                var now = new Date();
                var diff = config.now - Math.floor(now.getTime() / 1000);
                var ts = config.ends + diff;
                
                var day = Math.floor(ts / 86400);
                config.day.html(strpad(day));
                ts -= day * 86400;

                var hour = Math.floor(ts / 3600);
                config.hour.html(strpad(hour));
                ts -= hour * 3600;

                var minute = Math.floor(ts / 60);
                config.minute.html(strpad(minute));
                ts -= minute * 60;

                var second = ts;
                config.second.html(strpad(second));
        
            }, 1000);
        });
    });
</script>