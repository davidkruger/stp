<script>
    var Engine = {
        delayed : null,
        filePickerCallback : function(param,request){
            if(request == this.code){
                var file = 'data:' + this.mime + ';base64,' +Android.getFileBase64(param);
                
                if(this.loader === null){
                    this.delayed = {
                        file: file,
                        request: request
                    };
                }
                else{
                    this.loader(file,request);
                }
            }
        },
        mime : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        loader : null,
        code : 1001
    };
    $(function(){
        $('#myModal button.sale').off('click').on('click',function(){
            $('#massive_form').submit();
        });     
        
        var field = "document";
        var message = <?=json_encode($this->Frontend->feed('payload.schema.document.xlsx-message'))?>;
        var mimes = <?=json_encode($this->Frontend->feed('payload.schema.document.mimes'))?>;

        var fake = $('input[name="fake_' + field + '"]');
        var content = $('input[name="' + field + '"]');
        var file = $('input[name="trigger_' + field + '"]');
        var submit = $('a.submit');

        file.off('change').on('change',function(){
            var file = document.getElementsByName('trigger_' + field);
            getBase64(file[0].files[0]);
            submit.trigger('click');
        });
                
        Engine.loader = function(file,request){
            content.val(file);
            fake.val('Archivo seleccionado desde Android');
        };
        
        if(Engine.delayed !== null){
            Engine.loader(Engine.delayed.file, Engine.delayed.request);
            Engine.delayed = null;
        }

        fake.off('click').on('click',function(){
            if(typeof Android !== 'undefined'){
                Android.filePicker('excel', Engine.code, Engine.mime);
            }
            else{
                file.trigger('click');
            }
        });

        content.val('');
        fake.val('');

        function getBase64(file){
            if(validateFile(file,mimes)){
                var reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onload = function () {
                    content.val(reader.result);
                    fake.val(file.name + " (" + file.type + ")");
                };

                reader.onerror = function (error) {
                    var kind = 'error';
                    var message = 'Error al recuperar Archivo';
                    toastr[kind](message);
                    content.val('');
                    fake.val('');
                };
            }
            else{
                var kind = 'warning';
                toastr[kind](message);
                content.val('');
                fake.val('');
            }
        }

        function validateFile(file,mimes){
            var type = file.type;

            for(var i in mimes){
                if(mimes[i] == type){
                    return true;
                }
            }

            return false;
        }
    });
</script>