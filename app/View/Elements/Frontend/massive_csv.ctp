<?php $this->Frontend->js('Frontend/js/massive'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Venta Masiva</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de agregar esta venta masiva?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="sale btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-xs-12">
        <h2>Agregar Venta Masiva</h2>
        <h3><center>Confirmar datos extraidos del CSV</center></h3>
        <h4><center>Las filas con errores no se procesarán.</center></h4>
        
        <form id="massive_form" method="POST" class="form_agregar_ventas">
            <div class="row" style="margin-top:42px;margin-bottom:42px;">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>&nbsp;</td>
                            <td class="col_cantidad">Producto</td>
                            <td class="col_cantidad">Cantidad</td>
                            <td class="col_cantidad">Vendedor</td>
                            <td class="col_cantidad">Factura</td>
                            <td class="col_cantidad">Fecha</td>
                            <td class="col_cantidad">Puntos Calculados</td>
                            <td class="col_cantidad">En Stock?</td>
                        </tr>

                        <?php $this->Frontend->csvData($csv); ?>
                    </table>
                </div><!-- table responsive -->


            </div><!-- row -->
        </form>
    </div><!-- col -->
</div><!-- row -->
  
<div class="row">
    <div class="btn_cont">
        <a class="btn btn-default boton_login submit" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> CONFIRMAR VENTA MASIVA</a><br>
    </div><!-- btn_cont -->
</div><!-- row -->