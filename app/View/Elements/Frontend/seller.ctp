<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="vendedor_uno clearfix">
        <div class="row">
            <div class="col-xs-9 col-sm-9 col-md-10 col-lg-8">
                <div class="vendedor_info">
                    <h1><strong><?=$full_name?></strong></h1>
                    <div class="info_mail"><i class="fa fa-envelope" aria-hidden="true"></i><?=$email?></div>
                    <div class="info_cel"><i class="fa fa-phone" aria-hidden="true"></i><?=$telephone?></div>
                    <div class="info_doc"><i class="fa fa-id-card-o" aria-hidden="true"></i><?=$document?></div>
                </div><!-- vendedor_info -->
            </div>
            <div class="col-xs-3 col-sm-3 col-md-2 col-lg-4">
                <div class="ico_eliminar"><a href="" class="delete" data-id="<?=$id?>"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>
            </div>
        </div><!-- row -->
    </div><!-- vendedor_uno -->
</div><!-- col 6 -->