
<div class="big_info">
	<div class="row">
		<div class="col-md-6">
			<h1><strong>Bienvenido <span id="auth_name"><?=$this->Frontend->feed('auth.data.first_name')?> <?=$this->Api->feed('auth.data.last_name')?></span></strong></h1>
			<?php $this->Frontend->company(); ?>
			<!-- 
			<h2>Novedades</h2>
			<p>Tienes <b>1 nuevo</b> Desafío! del mes.</p>
			<p>Esta semana sumaste <b>300</b> puntos.</p>
			<p>Cantidad de puntos hasta <span id="auth_updated"><?=$this->Frontend->feedDatetime('auth.data.updated')?></span> <span id="auth_points"><?=$this->Frontend->feedInteger('auth.data.points')?></span> puntos</p>

		-->
		</div><!-- col -->
		<div class="col-md-6">
			<?php $this->Frontend->achievements(); ?>
		</div><!-- col -->
	</div><!-- row -->
</div><!-- big_info -->
<?php $this->Frontend->runSales(); ?>

<div class="container">
<div class="row desafiopuntoscanje">
	<div class="col-md-4 challenges">
		<?php $this->Frontend->challenge(); ?>
	</div>
	<div class="col-md-8">
		<div class="fondoceleste mispuntosycanje">
			<div class="row">
				<div class="col-md-4">
					<h2>Mis Puntos</h2>
					<div class="blueborder">
						<div class="blackcircle">
							<b><?=$this->Frontend->feedInteger('auth.data.points')?></b><br>
							 puntos
						</div>
					</div>
				</div><!-- col -->
				<?= $this->Frontend->redeemsWelcome()?>
			</div><!-- row  -->
		</div><!-- -->
		
	</div><!-- col -->
</div><!-- row -->
</div><!-- container -->

<?php $this->Frontend->challenges(); ?>

<section class="fondoceleste ventasyranking">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="whitecard">
					<h2 class="text-left">Mis ventas</h2>
					<canvas id="monthly"></canvas>

				</div><!-- whitecard -->
			</div><!-- col 8 -->
			<div class="col-md-4" style="margin-left: 9px !important;">
				<div class="whitecard text-center">
					<h2>Ranking de vendedores</h2>
					<div class="rankingboard">
						<h5>Posición</h5>
						<div class="rankingnumbers">
							<b><?=$this->Frontend->feedInteger('ranking.rank')?></b> / <?=$this->Frontend->feedInteger('ranking.total')?>
						</div><!-- rankingnumbers -->
					</div><!-- rankingboard -->
					<p>Quedan <?=$this->Frontend->feedInteger('ranking.days')?> días<br/>para terminar <br/>el mes corriente</p>
					
				</div><!-- whitecard -->
			</div><!-- col -->
		</div><!-- ow -->
	</div><!-- container -->
</section><!-- fondoceleste -->

<script>moment.locale('es');
	const data = <?=json_encode($this->Frontend->feed('ranking.chart'))?>;
	const ctx = document.querySelector("#monthly").getContext('2d');
	const config = {
	type: 'line',
	data: {
		labels: [],
		datasets: [{
		data: data,
		label: "Mis Ventas",
		borderColor: "#3e95cd",
		fill: false
		}]
	},
	options: {
		legend: {
			display: false,
			labels: {
				display: false
			}
		},
		scales: {
		xAxes: [{
			type: 'time',
			distribution: 'series',
			time: {
				unit: 'month'
			},
			ticks: {
				source: 'labels',
			},
		}],
		title: {
			display: false,
		}
		}
	}
	};
	new Chart(ctx, config);
</script>


<div class="container">