<div class="row vendedor_row">
    <div class="col-lg-6 col-sm-6 col-xs-6 vendedor_label">
        <?=$full_name?>
    </div><!-- col -->

    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 vendedor_input <?=$this->Frontend->errorField($form_id)?>">
        <div class="form-group">
            <label for="exampleInputEmail1">Cantidad de puntos</label>
            <input value="<?=$this->Frontend->inputIntegerVal($form_id)?>" name="<?=$form_id?>" type="text" class="form-control" id="exampleInputEmail1">
            <span class="label label-danger"><?=$this->Frontend->errors($form_id)?></span>
        </div><!-- form group -->
    </div><!-- col -->
</div><!-- row -->