<div class="col-sm-12  col-sm-12 col-md-5 col-lg-5 lottery" id="lottery_<?=$id?>">
    <h3><?=$name?></h3>
    <span class="date"></span>
    <span class="tickets"></span>
    
    <div class="days"></div>
    
    <div class="input">
        <p>Quiero comprar <input type="number" max="100" value="1" min="1" /> tickets por <span class="total"></span></p>
    </div>
    
    <div class="photo">
        <img src="<?=$this->Html->url([ 'controller' => 'frontend', 'action' => 'lottery_prize_photo', 'id' => $id])?>" />
        <span><?=$prize?></span>
    </div>
    
    <div class="submit">
        <button class="jugarahora">Comprar</button>
    </div>

</div>
