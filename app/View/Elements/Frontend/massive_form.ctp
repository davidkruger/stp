<?php $this->Frontend->js('Frontend/js/massive'); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Venta Masiva</h4>
            </div><!-- modal-header -->
            <div class="modal-body">
                <h2>¿Está seguro de agregar esta venta masiva?</h2>
            </div><!-- modal body -->
            <div class="modal-footer">
                <button type="button" class="sale btn btn-primary">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div><!-- modal footer -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
        <h2>Agregar Venta Masiva</h2>
        
        <form id="massive_form" method="POST" class="form_agregar_ventas">
            <div class="row" style="margin-top:42px;margin-bottom:42px;">
                <div class="input-group m-t-10">
                    <input 
                        style="cursor: pointer;"
                        type="text"
                        readonly="readonly"
                        placeholder="Subir XLSX"
                        class="form-control"
                        value=""
                        name="fake_document">

                    <span class="input-group-addon"><i class="fa fa-file-excel-o"></i></span>
                </div>

                <input 
                    style="position:absolute;left:-1000px;top:-1000px;"
                    type="file"
                    class="form-control"
                    name="trigger_document">

                <input 
                    style="display:none"
                    type="text"
                    readonly="readonly"
                    class="form-control"
                    name="document">
            </div><!-- row -->
        </form>
    </div><!-- col -->
</div><!-- row -->
  
<div class="row">
    <div class="btn_cont">
        <a class="btn btn-default boton_login submit" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> AGREGAR VENTA MASIVA</a><br>
    </div><!-- btn_cont -->
</div><!-- row -->