<div class="col-sm-12 col-md-6">
    <div class="tarjeta_catalogo">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">

                <div class="catalogo_moneda_container dellistado">
                    <div class="catalogo_moneda">
                        <img src="./frontend/gfx/plus.svg" class="img-responsive plusverde" />
                        <h3 class="dellistado">
                            <span class="spanacu">ACUMULÁS</span>
                            <strong class="modal-points"><?=$points?></strong>
                            <span class="spanpun">puntos</span>
                        </h3>
                    </div><!-- catalogo moneda -->
                </div><!-- catalogo_moneda_container -->

            </div><!-- col -->

            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="catalogo_thumb">
                    <img src="<?=$this->Html->url([ 'controller' => 'frontend', 'action' => 'product_photo', 'id' => $id])?>" class="img-responsive" />
                </div>
            </div><!-- col -->

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="catalogo_info">
                    <h4><?=$name?></h4>  
                    <a href="" data-index="<?=$index?>" class="btndetalle">VER DETALLES</a>  
                </div><!-- catalogo_info -->
            </div><!-- col -->
        </div><!-- row -->
    </div><!-- tarjeta_catalogo -->
</div><!-- col -->