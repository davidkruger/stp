<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="catalogo_uno clearfix">
        <div class="catalogo_thumb">
            <img src="<?=$this->Frontend->prizePhotoUrl($id)?>" class="img-responsive" />
        </div>
        <div class="catalogo_info">
            <h4><?=$name?></h4>
            <div class="catalogo_moneda_container">
                <div class="catalogo_moneda">
                    <h3>
                        <strong><?=$this->Frontend->integer($points)?></strong>
                        <br><span>puntos</span>
                    </h3>
                </div><!-- catalogo moneda -->
                <div class="catalogo_faltan">
                    <?=$this->Frontend->remaining($points)?>
                </div><!-- catalogo faltan -->
            </div><!-- CAtalogo moda -->
            
            
            <?php $this->Frontend->redeemButton([
                'id' => $id,
                'name' => $name,
                'photo' => $photo,
                'points' => $points
            ]); ?>
        </div><!-- catalogo_info -->
    </div><!-- catalogo_uno -->
</div><!-- col 4 --> 