<div class="fondoceleste desafio_home">
    <h2  style="margin-bottom: 20px"><?=$challenge['title']?></h2>
        <div class="row">
            <div class="col-12">
                <div class="circlebar loader" data-circle-startTime="0" data-circle-maxValue="<?=$progress['progress']?>" data-circle-dialWidth="20" data-circle-size="200px" data-circle-type="progress" >
                    <div class="loader-bg" >
                        <div class="text" style="color: black"></div>
                    </div>
            </div>
        </div>
    </div>


    <p style="margin-top: 20px"><?=$challenge['description']?></p>
    <a class="modal-challenge" href="#" challenge-title="<?=$challenge['title']?>" challenge-description="<?=$challenge['description']?>" challenge-img="<?=$this->Frontend->challengePrizeImgUrl($challenge['id'])?>" class="btn btn-primary btn-small">ver más</a>
</div>