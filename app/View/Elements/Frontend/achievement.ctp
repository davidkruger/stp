<div class="una_moneda"data-toggle="tooltip" data-placement="top" title="<?=$this->Frontend->achievementName($achievement)?>">
    <img src="<?=$this->Frontend->achievementIconUrl($achievement)?>" class="img-fluid imgmoneda"/>
    <div class="moneda_inferior" >
            <div class="circlebar" data-circle-startTime=0 data-circle-maxValue="<?=$this->Frontend->achievementPerc($achievement)?>" data-circle-dialWidth=10 data-circle-size="100px" data-circle-type="progress">
                <div class="loader-bg">
                    <div class="text">
                        
                    </div>
                </div>
            </div>  
    </div> 
</div><!-- una_moneda -->