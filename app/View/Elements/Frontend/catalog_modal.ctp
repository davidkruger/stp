<script>
    $(function(){           
        var modal = $('#modaldetalle');
        var link = $('a.btndetalle');
        var specs = modal.find('.listabeneficios');
        var catalogs = modal.find('.listabeneficios2');
        var spec = specs.find('div.row:first').clone();
        var catalog = catalogs.find('div:first').clone();
        
        link.click(function(e){
            e.preventDefault();
            
            var j = $(this).attr('data-index');
            var i = $(this).closest('.grupoproductos').attr('data-index');
            var product = Catalog.divisions[i].products[j];
            
            var catalogPhoto = <?=json_encode($this->Html->url([ 'controller' => 'frontend', 'action' => 'catalog_photo', 'id' => '{id}']))?>;
            var productPhoto = <?=json_encode($this->Html->url([ 'controller' => 'frontend', 'action' => 'product_photo', 'id' => '{id}']))?>;
            modal.find('.modal-product').html(product.name);
            modal.find('.modal-points').html(product.points);
            
            console.log(modal.find('.plusverde'))
            
            if(Catalog.divisions[i].division === 'TV'){
                modal.find('.modalescudo img').show(product.name);
            }
            else{
                modal.find('.modalescudo img').hide(product.name);
            }
            
            modal.find('.modal-photo').attr('src',productPhoto.replace('{id}',product.id));
            specs.html('');
            catalogs.html('');
            
            
            
            var scount = 0;
            
            for(var i in product.specs){
                var s = product.specs[i];
                var sdom = spec.clone();
                var svalue = sdom.find('.modal-value');
                var sspec = sdom.find('.modal-spec');
                svalue.html(s.value);
                sspec.html(s.spec);
                specs.append(sdom);
                scount++;
            }
            
            if(scount === 0){
                var sdom = spec.clone();
                var sspec = sdom.find('.modal-spec');
                var svalue = sdom.find('.modal-value');
                sspec.html('Sin caracteristicas');
                svalue.html('');
                specs.append(sdom);
            }
            
            var ccount = 0;
            
            for(var i in product.catalogs){
                var c = product.catalogs[i];
                var cdom = catalog.clone();
                var ctitle = cdom.find('.modal-title');
                var cdesc = cdom.find('.modal-desc');
                cdom.find('.modal-photo').attr('src',catalogPhoto.replace('{id}',c.id));
                ctitle.html(c.title);
                cdesc.html(c.description);
                catalogs.append(cdom);
                ccount++;
            }
            
            if(ccount === 0){
                var c = product.catalogs[i];
                var cdom = catalog.clone();
                var ctitle = cdom.find('.modal-title');
                var cdesc = cdom.find('.modal-desc');
                cdom.find('.modal-photo').detach();
                cdesc.html('Sin catalogos');
                ctitle.detach();
                catalogs.append(cdom);
            }
            
            modal.modal('show');
        });
    });
</script>