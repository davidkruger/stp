  
<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('product_id')?>">
    <label for="exampleInputPassword1">Producto</label>
    <select name="product_id" class="form-control">
        <option>Seleccione Producto</option>
        <?php $this->Frontend->products($this->Frontend->feed('products'))?>
    </select>
    <span class="label label-danger"><?=$this->Frontend->errors('product_id')?></span>
</div>

<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('invoice')?>">
    <label for="exampleInputEmail1">Factura</label>
    <input type="text" value="<?=$this->Frontend->inputVal('invoice')?>" name="invoice" class="form-control" id="exampleInputEmail1" >
    <span class="label label-danger"><?=$this->Frontend->errors('invoice')?></span>
</div>

<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('photo')?>">
    <label for="exampleInputEmail1">Foto</label>
    <input type="hidden" value="<?=$this->Frontend->inputVal('photo')?>" name="photo" class="form-control" id="exampleInputEmail1" >
    <input style="color:#000;cursor:pointer;" type="text" placeholder="Seleccionar Archivo" readonly name="photo_facade" class="form-control" id="exampleInputEmail1" >
    <input style="position:absolute;top:-1000px;left:-1000px" type="file" name="photo_trigger" class="form-control" id="exampleInputEmail1" >
    <span class="label label-danger"><?=$this->Frontend->errors('photo')?></span>
</div>

<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('quantity')?>">
    <label for="exampleInputEmail1">Cantidad de Productos</label>
    <input type="text" value="<?=$this->Frontend->inputVal('quantity')?>" name="quantity" class="form-control" id="exampleInputEmail1" >
    <span class="label label-danger"><?=$this->Frontend->errors('quantity')?></span>
</div>

<div class="col-lg-10 col-lg-offset-1 <?=$this->Frontend->errorField('sale_date')?>">
    <label for="exampleInputEmail1">Fecha de la Venta</label>
    <input data-date-language="es" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-provide="datepicker" type="text" value="<?=$this->Frontend->inputVal('sale_date')?>" name="sale_date" class="form-control" id="exampleInputEmail1" >
    <span class="label label-danger"><?=$this->Frontend->errors('sale_date')?></span>
</div>