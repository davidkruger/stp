<nav class="text-center">
    <ul class="pagination">
        <?php $this->FrontendShow->backButton($page,$count,$limit)?>
        <?php $this->FrontendShow->numbers($page,$count,$limit)?>
        <?php $this->FrontendShow->nextButton($page,$count,$limit)?>
    </ul>
</nav>

<?=$this->Frontend->js('Frontend/js/disable_page_links')?>