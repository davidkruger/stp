<li class="<?=$disabled?>" aria-controls="datatable-responsive" tabindex="0" id="datatable-responsive_previous">
    <a href="<?=$url?>" aria-label="<?=$this->Backend->feed('pagination.back')?>" class="<?=$disabled?>">
        <span aria-hidden="true">&laquo;</span>
    </a>
</li>