<li class="dropdown <?=$class?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$label?> <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <?php $this->Frontend->renderThisMenu($dropdown); ?>
    </ul>
</li>