<tr>
    <td><?=$product?></td>
    <td class="col_salesman_id"><?=$salesman?></td>
    <td class="col_factura"><?=$invoice?></td>
    <td class="col_cantidad"><?=$this->Frontend->integer($quantity)?></td>
    <td class="col_fecha"><?=$date?></td>
    <td class="col_cantidad"><?=$this->Frontend->integer($points)?></td>
    <td class="col_cantidad"><?=$this->Frontend->integer(ceil($points * $perc / 100))?></td>
    <td class="col_approval"><?=$approval?></td>
    <td class="col_cantidad">
        <a title="Aprobar" href="<?=$this->Html->url([ 'controller' => 'frontend','action' => 'approve','id' => $id])?>"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
        &nbsp;<a title="Rechazar" href="<?=$this->Html->url([ 'controller' => 'frontend','action' => 'reject','id' => $id])?>"><i class="fa fa-ban" aria-hidden="true"></i></a>
    </td>
</tr>