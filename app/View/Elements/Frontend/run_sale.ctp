<div class="homesliderOne">
    
    <div class="c_item" data-ends-at="<?=$time?>" data-run-sale="<?=$runSale['id']?>">
    <div class="c_header">
        <div class="row">
            <div class="col-sm-6">
                <h2><?=$runSale['description']?></h2>
            </div><!-- col sm 8 -->
            <div class="col-sm-6 col_tiempo">
                <div class="tiempo">Tiempo restante</div><!-- tiempo -->
                <div class="c_dia"><h3></h3><span>días</span></div><!-- c_dia -->
                <div class="c_hora"><h3></h3><span>horas</span></div><!-- c_hora -->
                <div class="c_minuto"><h3></h3><span>min.</span></div><!-- c_minuto -->
                <div class="c_segundo"><h3></h3><span>seg.</span></div><!-- c_segundo -->
            </div><!-- col sm 8 -->
        </div><!-- row -->
    </div><!-- c_header -->
    <div class="c_body">
        <div class="row">
            <div class="col-sm-8">
                <h3>Productos Objetivo</h3>
                <div class="second-slider">
                    <?php $this->Frontend->runSaleSlider($runSale)?>
                </div><!-- second-slider -->
                <h5>Progreso de ventas realizadas</h5>
                <div class="progresshome">
                <div class="progress_container">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$this->Frontend->runSaleProgress($runSale)?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$this->Frontend->runSaleProgress($runSale)?>%;">
                        <?=$this->Frontend->runSaleProgress($runSale)?>%
                        </div>
                    </div>
                </div><!-- progressbar_container -->
            </div>

            </div><!-- col sm 8 -->
            <div class="col-sm-4">
                <h3>Premio</h3>
                <div class="cont_premio">
                    <div class="icon_gift">
                        <img src="<?=$this->Html->url('/frontend/gfx/regalito.png')?>" class="img-responsive" />
                    </div><!-- icon gift -->
                    <img src="<?=$this->Html->url([ 'controller' => 'frontend', 'action' => 'run_sale_prize', 'id' => $runSale['id']])?>" class="img-responsive" /><br>
                        <h6><?=$runSale['prize_title']?></h6>
                </div><!-- con_premio -->
            </div><!-- col sm 8 -->
        </div><!-- row -->
    </div><!-- c_body -->
</div><!-- c_item -->

</div><!-- homesliderOne -->