<div class="col-sm-4 col-lg-4 col-md-4 natural personal">
    <div class="gal-detail thumb">
        <a href="<?=$this->Html->url([ 'controller' => 'products', 'action' => 'picture', 'id' => $product['id']])?>" target="_blank" class="image-popup">
            <img src="<?=$this->Html->url([ 'controller' => 'products', 'action' => 'picture', 'id' => $product['id']])?>" class="thumb-img" alt="Imagen Producto" >
        </a>
        <h4><?=$product['name']?><br/><span class="text-muted"><small class="text-muted"><?=$product['code']?></small></span></h4>
        <div class="row">
            <?php $this->Backoffice->productSpecs($specs,$catalogs)?>
            <?php $this->Backoffice->productCatalogs($specs,$catalogs)?>
        </div>
    </div>
</div>