<div style="padding:0px 5px;" class="text-muted">
    <a target="_blank" href="<?=$this->Html->url([ 'controller' => 'product_catalogs', 'action' => 'image', 'id' => $id])?>">
        <img alt="Imagen" src="<?=$this->Html->url([ 'controller' => 'product_catalogs', 'action' => 'image', 'id' => $id])?>" class="thumb-img">
    </a>
    <div><b><?=$title?></b></div>
    <div><?=$description?></div>
</div>