<div class="row port m-b-20">
    <div class="portfolioContainer">
        <?php $this->Backoffice->products($products); ?>
    </div>
</div> 

<script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/isotope/dist/isotope.pkgd.min.js')?>"></script>
<script type="text/javascript">
    $(window).load(function(){
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
    });
</script>