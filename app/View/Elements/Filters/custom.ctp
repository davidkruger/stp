<div class="col-md-12">
    <div id="datatable_filter" class="">     
        <label>Empresa:
            <div class="input-group">
                <input style="width:200px;" value="<?=isset($company_id) ? $company_id : ''?>" class="form-control input-sm" name="company_id" placeholder="Buscar..." aria-controls="datatable" type="search">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </label>
    </div>
</div>
<div class="col-md-12">
    <div id="datatable_filter" class="">      
        <label>Sucursal:
            <div class="input-group">
                <input style="width:200px;" value="<?=isset($branch_id) ? $branch_id : ''?>" class="form-control input-sm" name="branch_id" placeholder="Seleccione Empresa..." aria-controls="datatable" type="search">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </label>
    </div>
</div>
<div class="col-md-12">
    <div id="datatable_filter" class="">      
        <label>Periodo: 
            
            <div class="input-group">
                <input style="width:200px;" 
                    class="klez-datepicker form-control input-sm" name="a" aria-controls="datatable" type="search"
                    placeholder="dd/mm/aaaa" value="<?=isset($a) ? $a : ''?>">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            
            <div class="input-group">
                <input style="width:200px;" 
                    class="klez-datepicker form-control input-sm" name="b" aria-controls="datatable" type="search"
                    placeholder="dd/mm/aaaa" value="<?=isset($b) ? $b : ''?>">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </label>
    </div>
</div>
<div class="col-md-12" style="text-align:left">
    <div id="" class="">   
        <button class="btn btn-success"><i class="fa fa-send"></i> Aplicar Filtros</button>
        <button class="clear btn btn-warning"><i class="fa fa-ban"></i> Limpiar</button>
        <button name='excel' value='1' class="excel clear btn btn-primary"><i class="fa fa-download"></i> Descargar</button>
    </div>
</div>

<script>
    var data = <?=json_encode($this->Backoffice->getData())?>;
    var companies = [];
    var comp = {};
    var branches = {};
    var companyInput = $("input[name='company_id']");
    var branchInput = $("input[name='branch_id']");
    var currentCompany = null;
    var currentBranch = null;
    
    for(var i in data.companies){
        var reg = data.companies[i];
        var company = reg.id;
        var key = '#' + company;
        
        if(typeof comp[key] === 'undefined'){
            comp[key] = true;
            companies.push({
                id : parseInt(company),
                text : reg.name
            });
            
            branches[key] = [];
        }
        
        if(parseInt(companyInput.val()) === parseInt(company)){
            currentCompany = reg.name;

            if(parseInt(branchInput.val()) === parseInt(reg.branch_id)){
                currentBranch = reg.branch;
            }
        }
        
        branches[key].push({
            id : parseInt(reg.branch_id),
            text : reg.branch
        });
    }
    
    $(function(){
        function branchInit(collection){
            branchInput.select2({
                maximumSelectionLength: 1,
                maximumSelectionSize: 1,
                formatNoMatches: function() {
                    return 'No hay resultados';
                },
                formatSelectionTooBig: function (limit) {
                    return 'Seleccion completa';
                },
                data:{ results: collection}                    
            });
        }
        
        $('button.clear').click(function(e){
            window.location.href = window.location.href.replace(/\?.*$/,'');
            e.preventDefault();
        });
        
        $('button.excel').click(function(e){
            e.preventDefault();
            
            ShowFiltersEngineHooks.push(function(form,params){
                params.excel = 1;
                return params;
            });
            
            $('#quicksearch').submit();
            
            ShowFiltersEngineHooks.push(function(form,params){
                delete params.excel;
                return params;
            });
        });
        
        companyInput.select2({
            maximumSelectionLength: 1,
            maximumSelectionSize: 1,
            formatNoMatches: function() {
                return 'No hay resultados';
            },
            formatSelectionTooBig: function (limit) {
                return 'Seleccion completa';
            },
            data:{ results: companies },                     
        });
        
        companyInput.off('change').on('change',function(){
            var key = '#' + $(this).val();
            var collection = [];
            
            if(typeof branches[key] !== 'undefined'){
                collection = branches[key];
            }
        
            branchInit(collection);
            
            if(currentBranch !== null){
                branchInput.select2('data', { id : parseInt(branchInput.val()) , text: currentBranch });
                currentBranch = null;
            }
            else{
                branchInput.select2('data', { id : null , text: '' });
            }
        });

        if(currentCompany !== null){
            companyInput.select2('data', { id : parseInt(companyInput.val()) , text: currentCompany });
            companyInput.trigger('change');
        }
        else{
            branchInit([]);
        }
        
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        };
        
        $('.klez-datepicker').datepicker({
            format: "dd/mm/yyyy",
            language : 'es',
            autoclose : true
        });
    });
    
    ShowFiltersEngineHooks.push(function(form,params){
        params.branch_id = form.find('input[name="branch_id"]').val();
        params.company_id = form.find('input[name="company_id"]').val();
        params.a = form.find('input[name="a"]').val();
        params.b = form.find('input[name="b"]').val();
        params.p = 1;

        return params;
    });
</script>