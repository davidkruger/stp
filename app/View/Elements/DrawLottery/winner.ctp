<div class="profile-info-name">
    <div class="profile-info-detail">
        <h4 class="m-t-0 m-b-0">Ganador</h4>
        <p class="text-muted m-b-20"><i><?=$first_name?> <?=$last_name?></i></p>
        <h4 class="m-t-0 m-b-0">Documento</h4>
        <p class="text-muted m-b-20"><i><?=$document?></i></p>  
        <h4 class="m-t-0 m-b-0">Telefono</h4>
        <p class="text-muted m-b-20"><i><?=$telephone?></i></p>  
        <h4 class="m-t-0 m-b-0">Email</h4>
        <p class="text-muted m-b-20"><i><?=$email?></i></p>  
        <h4 class="m-t-0 m-b-0">Empresa</h4>
        <p class="text-muted m-b-20"><i><?=$company['name']?></i></p>  
        <h4 class="m-t-0 m-b-0">Sucursal</h4>
        <p class="text-muted m-b-20"><i><?=$branch['name']?></i></p>  
    </div>
    <div class="clearfix"></div>
</div>