<td>
    <div class="photo">
        <img src="<?=$this->Html->url([ 'controller' => 'lotteries', 'action' => 'prize_photo', 'id' => $lottery['id'] , '?' => [
            't' => md5($lottery['updated'])
        ]])?>">
        
        <p><?=$lottery['prize']?></p>
    </div>
    <div class="main">
        <h1><?=$lottery['name']?></h1>
        <h2>#<?=str_pad($number, 6, 0, STR_PAD_LEFT)?></h2>
        <h3><?=$foreign['user_id']?></h3>
        <h4><?=$foreign['company_id']?></h4>
    </div>
</td>