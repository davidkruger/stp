<div style="text-align: center" class="profile-info-name">
    <a href="<?=$this->Html->url([
        'controller' => 'lotteries',
        'action' => 'tickets',
        'id' => $id,
        'slug' => Inflector::slug($name)
    ])?>" target="_blank" class="btn btn-trans btn-primary"><i class="fa fa-print"></i> Imprimir Tickets</a>
    <div class="clearfix"></div>
</div>