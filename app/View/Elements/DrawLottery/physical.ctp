<div class="col-md-4">
    <div class="portlet card-draggable ui-sortable-handle">
        <div class="portlet-heading bg-purple">
            <h3 class="portlet-title">
                Sorteo Fisico
            </h3>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="bg-picture panel-collapse collapse in">
            <?php $this->DrawLottery->form(); ?>
        </div>
    </div>
</div>