<div class="col-md-4">
    <div class="portlet card-draggable ui-sortable-handle">
        <div class="portlet-heading bg-purple">
            <h3 class="portlet-title">
                Loteria: <?=$name?>
            </h3>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="bg-picture panel-collapse collapse in">
            <div class="profile-info-name">
                <div>
                    <img class="img-thumbnail" src="<?=$this->Html->url([ 'controller' => 'lotteries', 'action' => 'prize_photo', 'id' => $id , '?' => [
                        't' => md5($updated)
                    ]])?>">
                </div>
                    
                <div class="profile-info-detail">
                    <h4 class="m-t-0 m-b-0">Inicio</h4>
                    <p class="text-muted m-b-20"><i><?=$this->DrawLottery->dt($starts_at)?></i></p>
                    <h4 class="m-t-0 m-b-0">Fin</h4>
                    <p class="text-muted m-b-20"><i><?=$this->DrawLottery->dt($ends_at)?></i></p>
                    <h4 class="m-t-0 m-b-0">Premio</h4>
                    <p class="text-muted m-b-20"><i><?=$prize?></i></p>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>