<div class="col-md-4">
    <div class="portlet card-draggable ui-sortable-handle">
        <div class="portlet-heading bg-purple">
            <h3 class="portlet-title">
                Sorteo Automatico
            </h3>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="bg-picture panel-collapse collapse in">
            <div class="profile-info-name">
                El Sorteo se realizara de forma automatica cuando se alcance la Meta.

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>