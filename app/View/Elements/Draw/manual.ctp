<div class="col-md-4">
    <div class="portlet card-draggable ui-sortable-handle">
        <div class="portlet-heading bg-purple">
            <h3 class="portlet-title">
                Sorteo Manual
            </h3>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="bg-picture panel-collapse collapse in">
            <div style="text-align: center" class="profile-info-name">
                <form method="post">
                    <button class="btn btn-trans btn-primary"><i class="fa fa-send"></i> Realizar Sorteo</button>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>