<div class="col-md-4">
    <div class="portlet card-draggable ui-sortable-handle">
        <div class="portlet-heading bg-purple">
            <h3 class="portlet-title">
                Resultado del Sorteo
            </h3>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="bg-picture panel-collapse collapse in">
            <div class="profile-info-name">
                <div class="profile-info-detail">
                    <h4 class="m-t-0 m-b-0">Empresa Ganadora</h4>
                    <p class="text-muted m-b-20"><i><?=$winner['name']?></i></p>
                    <h4 class="m-t-0 m-b-0">Sorteo Realizado</h4>
                    <p class="text-muted m-b-20"><i><?=$this->Draw->dt($run['draw_at'])?></i></p>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>