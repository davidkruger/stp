<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Samsung te Premia</title>
        
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/frontend/fonts/stylesheet.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/frontend/css/font-awesome.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/frontend/css/bootstrap.min.css')?>" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="<?=$this->Html->url('/frontend/slick/slick.css')?>"/>
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="frontend/gfx/favicon.ico" type="image/gif" >

        <link href="<?=$this->Html->url('/frontend/circleprogress/assets/circle.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/frontend/circleprogress/assets/skins/simplecircle.css')?>" rel="stylesheet" type="text/css" />

        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/moment/min/moment-with-locales.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/chart.js/chart.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/slide/jssor.slider.min.js')?>"></script>
        <link href="<?=$this->Html->url('/frontend/css/samsung.css?v=5')?>" rel="stylesheet">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->  
    </head>
  
    <body class="pag_index">
        <nav class="navbar navbar-default"> 
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="<?=Router::url('/',true)?>"><img src="<?=$this->Html->url('/frontend/gfx/samsung_logo.png')?>" class="img-responsive samsung_logo" />
                      <?php /* <img src="<?=$this->Html->url('/frontend/gfx/ribbon.png')?>" class="img-responsive ribbonn"> */ ?>
                    </a>
                </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <?php $this->Frontend->renderMenu()?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        
        <div class="container">
            <?php if($pendings > 0): ?>
            <div id="pendings" class="pendings">Posees <?=$pendings?> canjes pendientes de aprobacion.</div>
            <script>//setTimeout(function(){
//                $('#pendings').fadeOut();
//            },5000)</script>
            <?php endif; ?>
            <?=$this->fetch('content'); ?>
        </div><!-- container -->
        
        <footer>
            <div class="container">
                <div class="row foot_text">
                  <div class="col-md-6 primerfoot">
                    Samsung te premia &copy; 2020
                  </div><!-- col -->
                  <div class="col-md-6 segundofoot">
                    <i class="fa fa-envelope" aria-hidden="true"></i>  <a href="mailto:info@stp.com.py">info@stp.com.py</a>
                  </div><!-- col -->
                </div><!-- foot_text -->
            </div><!-- container -->
        </footer>

         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?=$this->Html->url('/frontend/js/bootstrap.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>

        <script type="text/javascript" src="<?=$this->Html->url('/frontend/slick/slick.min.js')?>"></script>

        <script src="<?=$this->Html->url('/frontend/circleprogress/assets/circle.js')?>"type="text/javascript"></script>

        <?php $this->Flash->deploy()?>
        <?php $this->Frontend->delayed()?>

        <script>

            $( document ).ready(function() {
                $(".concurso_slide").on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {

                  var $elSlide = $(slick.$slides[currentSlide]);

                  var sliderObj = $elSlide.closest('.slick-slider');

                  if (sliderObj.hasClass('second-slider')) {
                    return;
                  }

                  var pager = (currentSlide ? currentSlide : 0) + 1 + "/6";
                  $('.page-nav').text("CURRENT SLIDE : " + pager);
                });

                $(".concurso_slide").slick({
                  autoplay: false,
                  dots: false,
                  arrows: true,
                  prevArrow: '<button type="button" class="slick-prev">Anterior</button>',
                  nextArrow: '<button type="button" class="slick-next">Siguiente</button>',
                  responsive: [{
                    breakpoint: 500,
                    settings: {
                      dots: false,
                      arrows: true,
                      infinite: false,
                      slidesToShow: 1,
                      slidesToScroll: 1
                    }
                  }]
                });

                $(".second-slider").slick({
                  autoplay: false,
                  dots: false,
                  adaptiveHeight: true,
                  arrows: false,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  responsive: [{
                    breakpoint: 500,
                    settings: {
                      dots: false,
                      arrows: false,
                      infinite: false,
                      slidesToShow: 1,
                      slidesToScroll: 1
                    }
                  }]
                });

                $(".homeslider").slick({
                  autoplay: true,
                  dots: false,
                  arrows: true,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });
                $(".challenges").slick({
                  autoplay: true,
                  dots: false,
                  arrows: true,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

                $(".contenedor_logros").slick({
                  autoplay: true,
                  dots: false,
                  arrows: false,
                  speed: 6000,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

                  

                $('.second-slider').on('touchstart touchmove mousemove mouseenter', function(e) {
                  $('.concurso_slide').slick('slickSetOption', 'swipe', false, false);
                });

                $('.second-slider').on('touchend mouseover mouseout', function(e) {
                  $('.concurso_slide').slick('slickSetOption', 'swipe', true, false);
                });


                if(typeof Android !== 'undefined'){
                    Android.splashRemoval('stp');
                }
            });


          $(document).ready(function() {

           
            
            $('#vercanjesbtn').click(function(){
              $('html, body').animate({
                  scrollTop: $( ".quetegustaria" ).offset().top-10
              }, 1000);
            });


        });

        </script>
        <script>
          function hoverImg(x) {
            x.style.display = "none";
          }

          /*function outImg(x) {
            x.style.display = "block";
            
          }*/
          function outImg(x) {
            x.style.display = "block";
          }
          
</script>

    </body>
</html>