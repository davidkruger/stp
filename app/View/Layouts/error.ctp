<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Samsung te Premia</title>

        <link href="<?=$this->Html->url('/frontend/fonts/stylesheet.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/frontend/css/font-awesome.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/frontend/css/bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/frontend/css/samsung.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->  
    </head>
  
    <body class="pag_index">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="<?=Router::url('/',true)?>"><img src="<?=$this->Html->url('/frontend/gfx/samsung_logo.png')?>" class="img-responsive"></a>
                </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <?php #$this->Frontend->renderMenu()?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        
        <div class="container">
            <?=$this->fetch('content'); ?>
        </div><!-- container -->
        
        <footer>
            <div class="container">
                <div class="row foot_text">
                  <div class="col-md-6 primerfoot">
                    Samsung te premia &copy; 2020
                  </div><!-- col -->
                  <div class="col-md-6 segundofoot">
                      <i class="fa fa-envelope" aria-hidden="true"></i>  <a href="mailto:info@stp.com.py">info@stp.com.py</a>
                  </div><!-- col -->
                </div><!-- foot_text -->
            </div><!-- container -->
        </footer>

         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?=$this->Html->url('/frontend/js/bootstrap.min.js')?>"></script>
    </body>
</html>