<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Samsung te Premia</title>
        
        <link href="<?=$this->Html->url('/css/printable.css')?>" rel="stylesheet" type="text/css" />
    </head>
  
    <body>
        <?=$this->fetch('content'); ?>
    </body>
</html>