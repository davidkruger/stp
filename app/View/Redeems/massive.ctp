<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="header-title m-t-0 m-b-0">Se ha<?=count($check)>1?'n':''?> seleccionado <?=count($check)?> canje<?=count($check)>1?'s':''?></h4>

        </div>
    </div><!-- end col -->
</div>

<?php $this->Form->deploy($this->Api->feed('payload')); ?>

<script>
    $(function(){
        var form = $("form");
        var check = <?=json_encode($check)?>;
        
        for(var i in check){
            var id = check[i];
            form.append($('<input name="check[]" value="' + id + '" type="hidden" />'));
        }
            form.append($('<input name="confirm" value="1" type="hidden" />'));
    });
</script>