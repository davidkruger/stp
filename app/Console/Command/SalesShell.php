<?php

App::uses('Salesman', 'Model');
App::uses('Sale', 'Model');

class SalesShell extends AppShell{
    private $Salesman, $Sale;
    
    public function initialize() {
        $this->Salesman = new Salesman();
        $this->initializeLog();
        
        return parent::initialize();
    }
    
    private function initializeLog(){
        $file = 'salesmen';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'salesmen' ],
            'file' => $file
        ));    
    }
    
    protected function verbose($message){
        $fullMessage = "{$message}";
        CakeLog::write('debug', $fullMessage, 'salesmen');
    }
    
    public function main(){
        echo "USAGE\n";
        echo "\tComando: crontab, ejecucion unitaria para usar con crontab\n";
    }

    public function run_sale() {
        $this->Sale = new Sale();
        $since = null;
        $to = null;
        if(isset($this->args[0])) $since = $this->args[0];
        if(isset($this->args[1])) $to = $this->args[1];

        $cnd = [];
        if($since) $cnd['Sale.sale_date >= '] = $since;
        if($to) $cnd['Sale.sale_date <= '] = $to;

        $sales = $this->Sale->find('all', [
            'conditions' => $cnd
        ]);
        
        try{
            
            $this->Sale->begin();
            
            foreach($sales as $sale){
                $saleId = $sale['id'];
                
                if(!$this->Sale->loadById($saleId)){
                    throw new Exception("Cannot load Sale@{$saleId}");
                }
                if(!$this->Sale->updateRunSales(null,null)){
                    throw new Exception("Cannot exec Sale@{$saleId}<updateRunSales>");
                }
            }

            $this->Sale->commit();
        } 
        catch (Exception $e) {
            $this->Sale->rollback();
            error_log("Exception @ ApprovalEndpointComponent::samsungMassive() - {$e->getMessage()}");
        }
    }
    
    const SEM_DRAW = 'SalesShell::crontab()';
    
    public function crontab(){
        $this->verbose('CRONTAB TICK');
        
        if($this->Salesman->block(self::SEM_DRAW) === false){
            $this->verbose("\t -> Cannot block");
            return false;
        }
        
        $salesmen = $this->Salesman->pickToDisable();
        $c = count($salesmen);
        $this->verbose("\t -> Salesmen: {$c}");
        
        foreach($salesmen as $salesman){
            $id = $salesman['Salesman']['id'];
            $this->verbose("\t\t\ -> Salesman [{$id}]");
            $this->Salesman->id = $id;
            
            if($this->Salesman->saveField('status',0)){
                $this->verbose("\t\t\t -> Disabled [{$id}]");
            }
            else{
                $this->verbose("\t\t\t -> Cant disable [{$id}]");
            }
        }
        
        if($this->Salesman->unblock(self::SEM_DRAW) === false){
            $this->verbose("\t -> Cannot unblock");
            return false;
        }
    }
    
    public function extras(){
        $this->loadModel('ExtraPoint');
        $this->loadModel('Sale');
        $this->loadModel('User');
        $this->ExtraPoint->query('TRUNCATE TABLE extra_points;');
        
        foreach($this->Sale->find('all') as $sale){
            $encargados = $this->User->fetchEncargados($sale['branch_id']);
            if(! empty($encargados)){
                foreach($encargados as $encargado){
                    if(strtotime($encargado['created']) <= strtotime($sale['created'])){
                        $perc = (double) $encargado['extra'];
                        $extra = ceil($sale['points'] * $perc / 100);
                        $this->ExtraPoint->alloc($encargado['id'],$extra,$perc,$sale['id']);
                    }
                }
            }
            
            $responsibles = $this->User->fetchResponsibles($sale['company_id']);

            if(!empty($responsibles)){
                foreach($responsibles as $responsible){
                    if(strtotime($encargado['created']) <= strtotime($sale['created'])){
                        $perc = (double) $responsible['extra'];
                        $extra = ceil($sale['points'] * $perc / 100);
                        $this->ExtraPoint->alloc($responsible['id'],$extra,$perc,$sale['id']);
                    }
                }
            }
            
            $extraTarget = $sale['extra_target'];
            
            if($this->User->loadById($extraTarget)){
                if(strtotime($this->User->readField['created']) <= strtotime($sale['created'])){
                    $perc = (double) $this->User->readField('extra');
                    $extra = ceil($sale['points'] * $perc / 100);
                    $this->ExtraPoint->alloc($extraTarget,$extra,$perc,$sale['id']);
                }
            }
        }
    }
}