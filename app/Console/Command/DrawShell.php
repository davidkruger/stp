<?php

App::uses('RunSale', 'Model');

class DrawShell extends AppShell{
    private $RunSale;
    
    public function initialize() {
        $this->RunSale = new RunSale();
        $this->initializeLog();
        
        return parent::initialize();
    }
    
    private function initializeLog(){
        $file = 'draw_auto';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'draw' ],
            'file' => $file
        ));    
    }
    
    protected function verbose($message){
        $fullMessage = "{$message}";
        CakeLog::write('debug', $fullMessage, 'draw');
    }
    
    public function main(){
        echo "USAGE\n";
        echo "\tComando: crontab, ejecucion unitaria para usar con crontab\n";
    }
    
    const SEM_DRAW = 'DrawShell::crontab()';
    
    public function crontab(){
        $this->verbose('CRONTAB TICK');
        
        if($this->RunSale->block(self::SEM_DRAW) === false){
            $this->verbose("\t -> Cannot block");
            return false;
        }
        
        $run_sales = $this->RunSale->pickForAutoDraw();
        $c = count($run_sales);
        $this->verbose("\t -> Run Sales: {$c}");
        
        foreach($run_sales as $run_sale){
            $id = $run_sale['id'];
            $this->verbose("\t\t -> Run Sale [{$id}]");
            $winner = $this->RunSale->draw($id, true);
            
            if(isset($winner[0]['Company']['id'])){
                $this->verbose("\t\t  -> Winner: {$winner[0]['Company']['name']} [{$winner[0]['Company']['id']}]");
            }
            else{
                $this->verbose("\t\t  -> No Winner");
            }
        }
        
        if($this->RunSale->unblock(self::SEM_DRAW) === false){
            $this->verbose("\t -> Cannot unblock");
            return false;
        }
    }
}