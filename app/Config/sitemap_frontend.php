<?php

$config = [];

#################################################### FRONTEND

$config['Sitemap.frontend.home'] = [
    
];

$config['Sitemap.frontend.login'] = [
    
];

$config['Sitemap.frontend.byc'] = [
    
];

$config['Sitemap.frontend.logout'] = [
    
];

$config['Sitemap.frontend.prize_photo'] = [
    
];

$config['Sitemap.frontend.prizes'] = [
    
];

$config['Sitemap.frontend.complains'] = [
    
];

$config['Sitemap.frontend.sellers'] = [
    
];

$config['Sitemap.frontend.add_seller'] = [
    
];

$config['Sitemap.frontend.assign_points'] = [
    
];

$config['Sitemap.frontend.sales'] = [
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
];

$config['Sitemap.frontend.add_sale'] = [
    
];

$config['Sitemap.frontend.run_sale_prize'] = [
    
];

$config['Sitemap.frontend.product_photo'] = [
    
];

$config['Sitemap.frontend.pending_sales'] = [
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas Pendientes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
];

$config['Sitemap.frontend.approve'] = [
    
];

$config['Sitemap.frontend.add_massive_sale'] = [
    
];

$config['Sitemap.frontend.reject'] = [
    
];

$config['Sitemap.frontend.rejected'] = [
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas Rechazadas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    
];

$config['Sitemap.frontend.edit_profile'] = [
    
];

$config['Sitemap.frontend.catalog'] = [
    
];

$config['Sitemap.frontend.catalog_photo'] = [
    
];

$config['Sitemap.frontend.roulette'] = [
    
];

$config['Sitemap.frontend.lottery'] = [
    
];

$config['Sitemap.frontend.lottery_prize_photo'] = [
    
];

$config['Sitemap.frontend.achievement_icon_0'] = [
    
];

$config['Sitemap.frontend.achievement_icon_1'] = [
    
];

$config['Sitemap.frontend.challenge_prize_photo'] = [
    
];