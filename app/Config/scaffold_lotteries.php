<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actions = [
    'detail.lotteries',
    'edit.lotteries',
    'draw.lotteries',
    'xlsx.lotteries'
];

############################################################# LISTAS

$config['Klezkaffold.show.lotteries.show'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'conditions' => [
                
            ],
            'joins' => [
                
            ],
            'order' => 'Lottery.starts_at DESC',
            'limit' => $limit
        ],
        'prequery' => [
            
        ]
    ],
    'actions' => $actions
];

############################################################# DRAW

$config['Klezkaffold.draw.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];


############################################################# DETALLE

$config['Klezkaffold.detail.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# CREAR

$config['Klezkaffold.add.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'beforeSave' => [
            'activeLottery'
        ]
    ],
];

$config['Klezkaffold.request_form.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
    ],
];

############################################################# EDITAR

$config['Klezkaffold.edit.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'schema' => 'writable',
        'beforeSave' => [
            'activeLottery'
        ]
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FOTO PREMIO

$config['Klezkaffold.image.prize_photo.lotteries'] = [
    'data' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'field' => 'prize_photo',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'imagen'
    ],
    'params' => [
        'id' => 'id',
    ],
];

############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL

$config['Klezkaffold.xlsx.lotteries'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Lottery',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'download' => [
        'file' => "reporte.xlsx",
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Loteria',
                'data' => [
                    'class' => 'LotteryIntent',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                        'order' => ['LotteryIntent.created DESC','LotteryIntent.number DESC']
                    ],
                    'prequery' => [ 'injectLottery' ],
                ],
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx.lotteries'] = $config['Klezkaffold.xlsx.lotteries'];