<?php

$config = [];

#################################################### CITIES

$config['Sitemap.providers.dashboard'] = [
    'h1' => 'Módulo de Proveedores',
    'empty' => [
        'title' => 'No hay Resúmenes de Proveedores',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.providers.show'] = [
    'h1' => 'Proveedores Activos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Proveedores',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Proveedores',
        'text' => 'No se han encontrado Proveedores en esta página'
    ],
    'search' => [
        'label' => 'Buscar Proveedores',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'proveedores',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.providers.show_disabled'] = [
    'h1' => 'Proveedores Suspendidos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Proveedores',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Proveedores',
        'text' => 'No se han encontrado Proveedores en esta página'
    ],
    'search' => [
        'label' => 'Buscar Proveedores',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'proveedores',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.providers.detail'] = [
    'h1' => 'Detalle del Proveedor',
    'icon' => 'info-outline'
];

$config['Sitemap.providers.edit'] = [
    'h1' => 'Editar Proveedor',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Proveedor',
        'icon' => 'send'
    ]
];

$config['Sitemap.providers.add'] = [
    'h1' => 'Crear Proveedor',
    'submit' => [
        'label' => 'Crear Proveedor',
        'icon' => 'send'
    ]
];