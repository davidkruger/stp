<?php

$config = [];
$limit = 10;

# CHALLENGES
############################################################# ACTIONS

$actions = [
    'detail.challenges',
    'edit.challenges',
    // 'challenge_produects.challenges',
];

############################################################# LISTAS

$config['Klezkaffold.show.challenges.show'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'conditions' => [
                
            ],
            'joins' => [
                
            ],
            'limit' => $limit
        ],
        'prequery' => [
            
        ]
    ],
    'actions' => $actions
];


############################################################# DETALLE

$config['Klezkaffold.detail.challenges'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'title' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# CREAR

$config['Klezkaffold.add.challenges'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
    ],
];

$config['Klezkaffold.request_form.challenges'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
    ],
];

############################################################# EDITAR

$config['Klezkaffold.edit.challenges'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'schema' => 'writable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'title' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# imagenes

$config['Klezkaffold.image.prize_img.challenges'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'field' => 'prize_img',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'premio'
    ],
    'params' => [
        'id' => 'id',
    ],
];