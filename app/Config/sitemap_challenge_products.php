<?php

$config = [];

#################################################### challenge x products

$config['Sitemap.challenge_products.add'] = [
    'h1' => 'Asignar Producto a Desafio',
    'submit' => [
        'label' => 'Asignar Producto',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.challenge_products.delete'] = [
    'h1' => 'Eliminar Producto del Desafio',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Producto del Desafio',
        'icon' => 'trash'
    ]
];

$config['Sitemap.challenge_products.edit'] = [
    'h1' => 'Editar Producto del Desafio',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Producto del Desafio',
        'icon' => 'send'
    ]
];
