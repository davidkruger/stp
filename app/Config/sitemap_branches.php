<?php

$config = [];

#################################################### SUCURSALES DE EMPRESA


$config['Sitemap.branches.add'] = [
    'h1' => 'Crear Sucursal',
    'submit' => [
        'label' => 'Crear Sucursal',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.branches.delete'] = [
    'h1' => 'Eliminar Sucursal',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Sucursal',
        'icon' => 'trash'
    ]
];

$config['Sitemap.branches.edit'] = [
    'h1' => 'Editar Sucursal',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Sucursal',
        'icon' => 'send'
    ]
];
