<?php

$config = [];

#################################################### USERS

$config['Sitemap.companies.dashboard'] = [
    'h1' => 'Módulo de Empresas',
    'empty' => [
        'title' => 'No hay Resúmenes de Empresas',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.companies.show'] = [
    'h1' => 'Empresas Activas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Empresas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Empresas',
        'text' => 'No se han encontrado Empresas en esta página'
    ],
    'search' => [
        'label' => 'Buscar Empresas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'empresas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.companies.show_disabled'] = [
    'h1' => 'Empresas Suspendidas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Empresas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Empresas',
        'text' => 'No se han encontrado Empresas en esta página'
    ],
    'search' => [
        'label' => 'Buscar Empresas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'empresas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.companies.detail'] = [
    'h1' => 'Detalle de la Empresa',
    'icon' => 'info-outline'
];

$config['Sitemap.companies.edit'] = [
    'h1' => 'Editar Empresa',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Empresa',
        'icon' => 'send'
    ]
];

$config['Sitemap.companies.add'] = [
    'h1' => 'Crear Nueva Empresa',
    'submit' => [
        'label' => 'Crear Empresa',
        'icon' => 'send'
    ]
];

$config['Sitemap.companies.company_contacts'] = [
    'h1' => 'Lista de Contactos',
    'icon' => 'accounts',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Contactos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Contactos',
        'text' => 'No se han encontrado Contactos para esta Empresa',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'contactos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.companies.massive'] = [
    'h1' => 'Carga Masiva de Empresas',
    'submit' => [
        'label' => 'Cargar Empresas',
        'icon' => 'send'
    ],
    'confirm' => [
        'label' => 'Confirmar Empresas',
        'icon' => 'send',
        'title' => 'Confirmar datos extraidos del CSV'
    ],
];

$config['Sitemap.companies.logo'] = [
    'h1' => 'Logo',
    'icon' => 'camera'
];

$config['Sitemap.companies.branches'] = [
    'h1' => 'Lista de Sucursales',
    'icon' => 'home',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Sucursales',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Sucursales',
        'text' => 'No se han encontrado Sucursales para esta Empresa'
    ],
    'search' => [
        'label' => 'Buscar Sucursales',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'sucursales',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.companies.stock'] = [
    'h1' => 'Stock',
    'icon' => 'truck',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Stocks',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Stocks',
        'text' => 'No se han encontrado Stocks para esta Empresa'
    ],
    'search' => [
        'label' => 'Buscar Stocks',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'stocks',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];