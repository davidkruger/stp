<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actions = [
    'detail.roulettes',
    'edit.roulettes',
    'xlsx.roulettes',
    'roulette_prizes.roulettes',
];

############################################################# LISTAS

$config['Klezkaffold.show.roulettes.show'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'conditions' => [
                
            ],
            'joins' => [
                
            ],
            'order' => 'Roulette.starts_at DESC',
            'limit' => $limit
        ],
        'prequery' => [
            
        ]
    ],
    'actions' => $actions
];

############################################################# DRAW

$config['Klezkaffold.draw.roulettes'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];


############################################################# DETALLE

$config['Klezkaffold.detail.roulettes'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# CREAR

$config['Klezkaffold.add.roulettes'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'beforeSave' => [
            'activeRoulette'
        ]
    ],
];

$config['Klezkaffold.request_form.roulettes'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
    ],
];

############################################################# EDITAR

$config['Klezkaffold.edit.roulettes'] = [
    'data' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'schema' => 'writable',
        'beforeSave' => [
            'activeRoulette'
        ]
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL

$config['Klezkaffold.xlsx.roulettes'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Roulette',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'download' => [
        'file' => "reporte.xlsx",
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ruleta',
                'data' => [
                    'class' => 'RouletteIntent',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                        'order' => 'RouletteIntent.created DESC',
                    ],
                    'prequery' => [ 'injectRoulette' ],
                ],
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx.roulettes'] = $config['Klezkaffold.xlsx.roulettes'];