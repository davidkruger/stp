<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.product_promotions',
    'edit.product_promotions',
];

############################################################# PRODUCT x PROMOCION 

$config['Klezkaffold.product_promotions.products'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'ProductPromotion.id DESC'
        ],
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'product' => [ 'payload', 'product' ],
    ],
    'links' => [
        [ 'controller' => 'product_promotions', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.products.product_promotions'] = $config['Klezkaffold.product_promotions.products'];

############################################################# EDITAR

$config['Klezkaffold.edit.product_promotions'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
        'schema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.product_promotions'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.product_promotions'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.product_promotions'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.product_promotions'] = [
    'data' => [
        'class' => 'ProductPromotion',
        'path' => 'Model',
        'formSchema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
];
