<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.disabled_groups'] = [
    'show' => [
        'title' => 'Grupos Activos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Grupos Restrigidos',
        'url' => [
            'controller' => 'disabled_groups', 'action' => 'show'
        ],
        'data' => [
            'class' => 'DisabledGroup',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'DisabledGroup.status' => true
                ],
                'order' => 'DisabledGroup.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Grupos Suspendidos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Grupos',
        'url' => [
            'controller' => 'disabled_groups', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'DisabledGroup',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'DisabledGroup.status' => false
                ],
                'order' => 'DisabledGroup.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.disabled_groups',
    'edit.disabled_groups',
];

############################################################# LISTAS

$config['Klezkaffold.show.disabled_groups.show'] = [
    'data' => $config['Klezkaffold.dashboard.disabled_groups']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.disabled_groups.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.disabled_groups']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.disabled_groups'] = [
    'data' => [
        'class' => 'DisabledGroup',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.disabled_groups'] = [
    'data' => [
        'class' => 'DisabledGroup',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.disabled_groups'] = [
    'data' => [
        'class' => 'DisabledGroup',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.disabled_groups'] = [
    'data' => [
        'class' => 'DisabledGroup',
        'path' => 'Model',
    ],
];