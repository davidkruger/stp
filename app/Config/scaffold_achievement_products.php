<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.achievement_products',
    'edit.achievement_products',
];

############################################################# PRODUCTOS X LOGRO

$config['Klezkaffold.achievement_products.achievements'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'AchievementProduct.id DESC'
        ],
        'prequery' => [ 'achievementOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'achievement' => [ 'payload', 'achievement' ],
    ],
    'links' => [
        [ 'controller' => 'achievement_products', 'action' => 'add']
    ],
    'actions' => $actions
];
$config['Klezkaffold.show.achievements.achievement_products'] = $config['Klezkaffold.achievement_products.achievements'];


############################################################# EDITAR

$config['Klezkaffold.edit.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
        'schema' => 'achievementless',
    ],
    'params' => [
        'id' => 'id',
        'achievement' => 'achievement',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'achievement' => 'achievement',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
        'schema' => 'achievementless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
        'schema' => 'achievementless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
        'formSchema' => 'achievementless',
        'prequery' => [ 'achievementOwnerPrequery' ],
    ],
];


############################################################# DETALLE

$config['Klezkaffold.detail.achievement_products'] = [
    'data' => [
        'class' => 'AchievementProduct',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => []
];