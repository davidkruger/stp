<?php

$config = [];

#################################################### COMPLAINS

$config['Sitemap.complains.show_read'] = [
    'h1' => 'Consultas Leidas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Consultas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Consultas',
        'text' => 'No se han encontrado Consultas en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Remitente',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'consultas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.complains.show_unread'] = [
    'h1' => 'Consultas sin Leer',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Consultas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Consultas',
        'text' => 'No se han encontrado Consultas en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Remitente',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'consultas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.complains.detail'] = [
    'h1' => 'Leyendo Consulta',
    'icon' => 'eye'
];
