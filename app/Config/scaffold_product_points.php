<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.product_points',
    'edit.product_points',
];

############################################################# POINTS x PRODUCT 

$config['Klezkaffold.product_points.products'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'ProductPoint.points DESC'
        ],
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'product' => [ 'payload', 'product' ],
    ],
    'links' => [
        [ 'controller' => 'product_points', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.products.product_points'] = $config['Klezkaffold.product_points.products'];

############################################################# EDITAR

$config['Klezkaffold.edit.product_points'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
        'schema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.product_points'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.product_points'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.product_points'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.product_points'] = [
    'data' => [
        'class' => 'ProductPoint',
        'path' => 'Model',
        'formSchema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
];
