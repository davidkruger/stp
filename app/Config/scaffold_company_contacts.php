<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.company_contacts',
    'edit.company_contacts',
];

############################################################# CONTACTO x EMPRESA 

$config['Klezkaffold.company_contacts.companies'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'CompanyContact.id DESC'
        ],
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'company' => [ 'payload', 'company' ],
    ],
    'links' => [
        [ 'controller' => 'company_contacts', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.companies.company_contacts'] = $config['Klezkaffold.company_contacts.companies'];

############################################################# EDITAR

$config['Klezkaffold.edit.company_contacts'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.company_contacts'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.company_contacts'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.company_contacts'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.company_contacts'] = [
    'data' => [
        'class' => 'CompanyContact',
        'path' => 'Model',
        'formSchema' => 'companyless',
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
];
