<?php

$config = [];

#################################################### CANJES


$config['Sitemap.redeems.show_pending'] = [
    'h1' => 'Canjes Pendientes de Moderacion',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Vendedor',
        'weight' => 3,
    ],
    'filters' => [
        'Filters/custom'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'canjes',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            30, 50, 100, 200
        ]
    ],
    'check' => [
        'field' => 'id',
        'label' => 'Procesar Canjes Seleccionados',
        'icon' => 'check',
        'url' => Router::url([
            'controller' => 'redeems',
            'action' => 'massive'
        ])
    ]
];

$config['Sitemap.redeems.show_rejected'] = [
    'h1' => 'Canjes Rechazados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Vendedor',
        'weight' => 3,
    ],
    'filters' => [
        'Filters/custom'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'canjes',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.redeems.show_approved'] = [
    'h1' => 'Canjes Aprobados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ],
    'filters' => [
        'Filters/custom'
    ],
    'search' => [
        'label' => 'Buscar por Vendedor',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'canjes',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.redeems.detail'] = [
    'h1' => 'Detalle del Canje',
    'icon' => 'info-outline'
];
$config['Sitemap.redeem_moderations.detail'] = [
    'h1' => 'Detalle de la Moderacion',
    'icon' => 'eye'
];

$config['Sitemap.redeems.edit'] = [
    'h1' => 'Moderar Canje',
    'icon' => 'badge-check',
    'submit' => [
        'label' => 'Procesar Moderacion',
        'icon' => 'send'
    ]
];

$config['Sitemap.redeems.massive'] = [
    'h1' => 'Procesar Canjes Seleccionados',
    'submit' => [
        'label' => 'Procesar Canjes Seleccionados',
        'icon' => 'send'
    ]
];
