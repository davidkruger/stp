<?php

$config = [];

#################################################### ROULETTES

$config['Sitemap.roulettes.dashboard'] = [
    'h1' => 'Módulo de Ruletas',
];

$config['Sitemap.roulettes.show'] = [
    'h1' => 'Listado de Ruletas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ruletas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ruletas',
        'text' => 'No se han encontrado Ruletas en esta página'
    ],
    'search' => [
        'label' => 'Filtrar Ruleta',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ruletas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.roulettes.detail'] = [
    'h1' => 'Detalle de la Ruleta',
    'icon' => 'info-outline'
];

$config['Sitemap.roulettes.edit'] = [
    'h1' => 'Editar Ruleta',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Ruleta',
        'icon' => 'send'
    ]
];

$config['Sitemap.roulettes.add'] = [
    'h1' => 'Crear Nueva Ruleta',
    'submit' => [
        'label' => 'Crear Ruleta',
        'icon' => 'send'
    ]
];

$config['Sitemap.roulettes.xlsx'] = [
    'h1' => 'Reporte',
    'icon' => 'download'
];

$config['Sitemap.roulettes.roulette_prizes'] = [
    'h1' => 'Lista de Premios',
    'icon' => 'assignment-o',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Premios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Premios',
        'text' => 'No se han encontrado Premios para esta Ruleta'
    ],
    'search' => [
        'label' => 'Buscar Premios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'premios',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];