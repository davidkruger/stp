<?php

Cache::config('default', array('engine' => 'File'));

Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

App::uses('CakeLog', 'Log');

CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
    'mask' => 0666,
));

CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
    'mask' => 0666,
));

Cache::config('default', array(
    'engine' => 'File',
    'mask' => 0666,
));

CakePlugin::load('KlezBackend', array('bootstrap' => false, 'routes' => false));
CakePlugin::load('KlezApi', array('bootstrap' => false, 'routes' => false));
CakePlugin::load('KlezData', array('bootstrap' => false, 'routes' => false));
CakePlugin::load('Klezkaffold', array('bootstrap' => false, 'routes' => false));
CakePlugin::load('KlezBackendApi', array('bootstrap' => false, 'routes' => false));

setlocale (LC_TIME, "es_ES.utf8");