<?php

$config = [];

#################################################### PREMIOS DE RULETA

$config['Sitemap.roulette_prizes.add'] = [
    'h1' => 'Asignar Premio',
    'submit' => [
        'label' => 'Asignar Premio',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.roulette_prizes.delete'] = [
    'h1' => 'Eliminar Premio',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Premio',
        'icon' => 'trash'
    ]
];

$config['Sitemap.roulette_prizes.edit'] = [
    'h1' => 'Editar Premio',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Premio',
        'icon' => 'send'
    ]
];