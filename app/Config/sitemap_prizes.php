<?php

$config = [];

#################################################### PREMIOS

$config['Sitemap.prizes.dashboard'] = [
    'h1' => 'Módulo de Premios',
    'empty' => [
        'title' => 'No hay Resúmenes de Premios',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.prizes.show'] = [
    'h1' => 'Premios Activos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Premios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Premios',
        'text' => 'No se han encontrado Premios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Premios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'premios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.prizes.show_disabled'] = [
    'h1' => 'Premios Suspendidos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Premios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Premios',
        'text' => 'No se han encontrado Premios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Premios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'premios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.prizes.detail'] = [
    'h1' => 'Detalle del Premio',
    'icon' => 'info-outline'
];

$config['Sitemap.prizes.edit'] = [
    'h1' => 'Editar Premio',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Premio',
        'icon' => 'send'
    ]
];

$config['Sitemap.prizes.add'] = [
    'h1' => 'Crear Nuevo Premio',
    'submit' => [
        'label' => 'Crear Premio',
        'icon' => 'send'
    ]
];

$config['Sitemap.prizes.photo'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];

$config['Sitemap.prizes.pdf'] = [
    'h1' => 'PDF',
    'icon' => 'document'
];