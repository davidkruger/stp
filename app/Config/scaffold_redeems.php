<?php

$config = [];
$limit = 10;
$limit_pending_redeems = 30;

############################################################# ACTIONS

$actionsPending = [
    'edit.redeems',
];

$actions = [
    'detail.redeems',
    'detail.redeem_moderations',
];

############################################################# LISTAS

$config['Klezkaffold.show.redeems.show_pending'] = [
    'data' => [
        'class' => 'Redeem',
        'schema' => 'show',
        'path' => 'Model',
        'prequery' => [ 'customFiltersPrequery' ],
        'query' => [
            'conditions' => [
                'Redeem.status' => 'pendiente'
            ],
            'joins' => [
                'INNER JOIN users AS User ON User.id=Redeem.user_id',
                'LEFT JOIN companies AS Company ON Company.id=User.company_id',
                'LEFT JOIN branches AS Branch ON Branch.id=User.branch_id',
            ],
            'order' => 'Redeem.created DESC',
            'limit' => $limit_pending_redeems
        ],
    ],
    'actions' => $actionsPending
];

$config['Klezkaffold.show.redeems.show_approved'] = [
    'data' => [
        'class' => 'Redeem',
        'path' => 'Model',
        'prequery' => [ 'customFiltersPrequery' ],
        'schema' => 'show',
        'query' => [
            'conditions' => [
                'Redeem.status' => 'aprobado'
            ],
            'joins' => [
                'INNER JOIN users AS User ON User.id=Redeem.user_id',
                'LEFT JOIN companies AS Company ON Company.id=User.company_id',
                'LEFT JOIN branches AS Branch ON Branch.id=User.branch_id',
            ],
            'order' => 'Redeem.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.redeems.show_rejected'] = [
    'data' => [
        'class' => 'Redeem',
        'path' => 'Model',
        'prequery' => [ 'customFiltersPrequery' ],
        'schema' => 'show',
        'query' => [
            'conditions' => [
                'Redeem.status' => 'rechazado'
            ],
            'joins' => [
                'INNER JOIN users AS User ON User.id=Redeem.user_id',
                'LEFT JOIN companies AS Company ON Company.id=User.company_id',
                'LEFT JOIN branches AS Branch ON Branch.id=User.branch_id',
            ],
            'order' => 'Redeem.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.redeems'] = [
    'data' => [
        'class' => 'Redeem',
        'path' => 'Model',
        'prequery' => [
            'notPendingFilter'
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# DETALLE MODERACION

$config['Klezkaffold.detail.redeem_moderations'] = [
    'data' => [
        'class' => 'RedeemModeration',
        'path' => 'Model',
        'schema' => 'readable',
        'prequery' => [
            'injectRedeem'
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => []
];

############################################################# EDITAR

$config['Klezkaffold.edit.redeems'] = [
    'data' => [
        'class' => 'Redeem',
        'path' => 'Model',
        'schema' => 'moderation',
        'query' => [
            'conditions' => [
                'Redeem.status' => 'pendiente'
            ],
        ],
        'beforeCommit' => [
            'pushModeration',
            'returnUserPoints',
            'notifySalesman'
        ],
        'prequery' => [
            'pendingFilter'
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.redeems'] = [
    'data' => [
        'class' => 'Redeem',
        'path' => 'Model',
        'schema' => 'moderation',
        'prequery' => [
            'pendingFilter'
        ]
    ],
];


############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL

$config['Klezkaffold.xlsx.show_pending'] = [
    'type' => 'excel',
    'download' => [
        'file' => "canjes_pendientes.xlsx",
    ],
    'filter' => [
        'class' => 'Redeem',
        'path' => 'Model'
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Canjes Pendientes',
                'data' => $config['Klezkaffold.show.redeems.show_pending']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_show_pending'] = $config['Klezkaffold.xlsx.show_pending'];
unset($config['Klezkaffold.report.xlsx_show_pending']['excel']['sheets'][0]['data']['query']['limit']);

$config['Klezkaffold.xlsx.show_approved'] = [
    'type' => 'excel',
    'download' => [
        'file' => "canjes_aprobados.xlsx",
    ],
    'filter' => [
        'class' => 'Redeem',
        'path' => 'Model'
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Canjes Aprobados',
                'data' => $config['Klezkaffold.show.redeems.show_approved']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_show_approved'] = $config['Klezkaffold.xlsx.show_approved'];
unset($config['Klezkaffold.report.xlsx_show_approved']['excel']['sheets'][0]['data']['query']['limit']);

$config['Klezkaffold.xlsx.show_rejected'] = [
    'type' => 'excel',
    'download' => [
        'file' => "canjes_rechazados.xlsx",
    ],
    'filter' => [
        'class' => 'Redeem',
        'path' => 'Model'
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Canjes Rechazados',
                'data' => $config['Klezkaffold.show.redeems.show_rejected']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_show_rejected'] = $config['Klezkaffold.xlsx.show_rejected'];
unset($config['Klezkaffold.report.xlsx_show_rejected']['excel']['sheets'][0]['data']['query']['limit']);