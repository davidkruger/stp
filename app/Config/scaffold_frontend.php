<?php

$actions = [];

############################################################# HOME

$config['Klezkaffold.dashboard.frontend'] = [
    
];

############################################################# VENDEDORES FORM

$config['Klezkaffold.request_form.sellers'] = [
    'data' => [
        'schema' => 'frontend',
        'class' => 'Seller',
        'path' => 'Model',
    ],
];

############################################################# VENDEDORES CREAR

$config['Klezkaffold.add.sellers'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'writable',
        'beforeSave' => [ 
            'filterFrontend'
        ]
    ],
];

############################################################# EDITAR PERFIL FRONTEND

$config['Klezkaffold.edit.frontend_profile'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'frontendProfile',
        'prequery' => [
            
        ]
    ], 
    'params' => [
        'slug' => [ 'slugify', 'document' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO FRONTEND

$config['Klezkaffold.request_form.frontend_profile'] = [
    'data' => [
        'schema' => 'frontendProfile',
        'class' => 'User',
        'path' => 'Model',
    ],
];