<?php

$config = [];

#################################################### DIVISIONES

$config['Sitemap.divisions.dashboard'] = [
    'h1' => 'Módulo de Divisiones',
    'empty' => [
        'title' => 'No hay Resúmenes de Divisiones',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.divisions.show'] = [
    'h1' => 'Divisiones Activas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Divisiones',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Divisiones',
        'text' => 'No se han encontrado Divisiones en esta página'
    ],
    'search' => [
        'label' => 'Buscar Divisiones',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'divisiones',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.divisions.show_disabled'] = [
    'h1' => 'Divisiones Suspendidas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Divisiones',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Divisiones',
        'text' => 'No se han encontrado Divisiones en esta página'
    ],
    'search' => [
        'label' => 'Buscar Divisiones',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'divisiones',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.divisions.detail'] = [
    'h1' => 'Detalle de la Division',
    'icon' => 'info-outline'
];

$config['Sitemap.divisions.edit'] = [
    'h1' => 'Editar Division',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Division',
        'icon' => 'send'
    ]
];

$config['Sitemap.divisions.add'] = [
    'h1' => 'Crear Nueva Division',
    'submit' => [
        'label' => 'Crear Division',
        'icon' => 'send'
    ]
];

$config['Sitemap.divisions.subdivisions'] = [
    'h1' => 'Lista de Subdivisiones',
    'icon' => 'puzzle-piece',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Subdivisiones',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Subdivisiones',
        'text' => 'No se han encontrado Subdivisiones para esta Division'
    ],
    'search' => [
        'label' => 'Buscar Subdivisiones',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'subdivisiones',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];