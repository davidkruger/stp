<?php

$config = [];

#################################################### PROMOCION DE PRODUCTO


$config['Sitemap.product_promotions.add'] = [
    'h1' => 'Asignar Puntaje Promocional',
    'submit' => [
        'label' => 'Asignar Promocion',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.product_promotions.delete'] = [
    'h1' => 'Eliminar Puntaje Promocional',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Promocion',
        'icon' => 'trash'
    ]
];

$config['Sitemap.product_promotions.edit'] = [
    'h1' => 'Editar Puntaje Promocional',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Promocion',
        'icon' => 'send'
    ]
];
