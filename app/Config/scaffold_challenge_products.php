<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.challenge_products',
    'edit.challenge_products',
];

############################################################# PRODUCTOS X LOGRO

$config['Klezkaffold.challenge_products.challenges'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'ChallengeProduct.id DESC'
        ],
        'prequery' => [ 'challengeOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'title' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'challenge' => [ 'payload', 'challenge' ],
    ],
    'links' => [
        [ 'controller' => 'challenge_products', 'action' => 'add']
    ],
    'actions' => $actions
];
$config['Klezkaffold.show.challenges.challenge_products'] = $config['Klezkaffold.challenge_products.challenges'];


############################################################# EDITAR

$config['Klezkaffold.edit.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
        'schema' => 'challengeless',
    ],
    'params' => [
        'id' => 'id',
        'challenge' => 'challenge',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'challenge' => 'challenge',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
        'schema' => 'challengeless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
        'schema' => 'challengeless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
        'formSchema' => 'challengeless',
        'prequery' => [ 'challengeOwnerPrequery' ],
    ],
];


############################################################# DETALLE

$config['Klezkaffold.detail.challenge_products'] = [
    'data' => [
        'class' => 'ChallengeProduct',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'title' ],
        'id' => 'id',
    ],
    'actions' => []
];