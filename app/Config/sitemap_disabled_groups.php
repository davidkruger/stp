<?php

$config = [];

#################################################### DIVISIONES

$config['Sitemap.disabled_groups.dashboard'] = [
    'h1' => 'Módulo de Grupos Restringidos',
    'empty' => [
        'title' => 'No hay Resúmenes de Grupos Restringidos',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.disabled_groups.show'] = [
    'h1' => 'Grupos Restringidos Activos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Grupos Restringidos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Grupos Restringidos',
        'text' => 'No se han encontrado Grupos Restringidos en esta página'
    ],
    'search' => [
        'label' => 'Buscar Grupos Restringidos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'grupos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.disabled_groups.show_disabled'] = [
    'h1' => 'Grupos Restringido Suspendidos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Grupos Restringido',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Grupos Restringido',
        'text' => 'No se han encontrado Grupos Restringido en esta página'
    ],
    'search' => [
        'label' => 'Buscar Grupos Restringido',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'grupos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.disabled_groups.detail'] = [
    'h1' => 'Detalle del Grupo Restringido',
    'icon' => 'info-outline'
];

$config['Sitemap.disabled_groups.edit'] = [
    'h1' => 'Editar Grupo Restringido',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Grupo Restringido',
        'icon' => 'send'
    ]
];

$config['Sitemap.disabled_groups.add'] = [
    'h1' => 'Crear Nuevo Grupo Restringido',
    'submit' => [
        'label' => 'Crear Grupo Restringido',
        'icon' => 'send'
    ]
];