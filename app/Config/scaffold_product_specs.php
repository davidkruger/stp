<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.product_specs',
    'edit.product_specs',
];

############################################################# SPEC x PRODUCT 

$config['Klezkaffold.product_specs.products'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'ProductSpec.spec_order ASC'
        ],
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'product' => [ 'payload', 'product' ],
    ],
    'links' => [
        [ 'controller' => 'product_specs', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.products.product_specs'] = $config['Klezkaffold.product_specs.products'];

############################################################# EDITAR

$config['Klezkaffold.edit.product_specs'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
        'schema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.product_specs'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.product_specs'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.product_specs'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.product_specs'] = [
    'data' => [
        'class' => 'ProductSpec',
        'path' => 'Model',
        'formSchema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
];
