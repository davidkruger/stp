<?php

$config = [];
$referer =  $_SERVER['HTTP_REFERER'];
$home = Router::url([ 'controller' => 'backend', 'action' => 'home', 'plugin' => null],true);

############################################################# HOME

Configure::load('scaffold_redeems');
Configure::load('scaffold_complains');
Configure::load('scaffold_sales');

$config['Klezkaffold.dashboard.backend'] = [];

if($referer === $home){
    $conditions = [];
    $recentLabels = [];
    $days = 10;
    
    $recentSeries = [
        'sales' => [
            'label' => 'Total de Ventas',
            'options' => [
                'fill' => true,
                'lineTension' => 0.3,
                'backgroundColor' => "#10c46922",
                'borderColor' => "#10c469",
                'borderDash' => [],
                'borderDashOffset' => 0.0,
                'pointBorderColor' => "#10c469",
                'pointBackgroundColor' => "#fff",
                'pointBorderWidth' => 1,
                'pointHoverRadius' => 5,
                'pointHoverBackgroundColor' => "#10c469",
                'pointHoverBorderColor' => "#eef0f2",
                'pointHoverBorderWidth' => 1,
                'pointRadius' => 3,
                'pointHitRadius' => 1,
            ],
            'points' => []
        ],
        'redeems' => [
            'label' => 'Total de Canjes',
            'options' => [
                'fill' => true,
                'fillColor' => '#188AE2',
                'lineTension' => 0.3,
                'backgroundColor' => "#188AE222",
                'borderColor' => "#188AE2",
                'borderCapStyle' => 'butt',
                'borderDash' => [ ],
                'borderDashOffset' => 0.0,
                'borderJoinStyle' => 'bevel',
                'pointBorderColor' => "#188AE2",
                'pointBackgroundColor' => "#fff",
                'pointBorderWidth' => 1,
                'pointHoverRadius' => 5,
                'pointHoverBackgroundColor' => "#188AE2",
                'pointHoverBorderColor' => "#eef0f2",
                'pointHoverBorderWidth' => 1,
                'pointRadius' => 3,
                'pointHitRadius' => 1,
            ],
            'points' => []
        ],
        'complains' => [
            'label' => 'Total de Consultas',
            'options' => [
                'fill' => true,
                'fillColor' => '#E8ADAD',
                'lineTension' => 0.3,
                'backgroundColor' => "#E8ADAD22",
                'borderColor' => "#E8ADAD",
                'borderCapStyle' => 'butt',
                'borderDash' => [ ],
                'borderDashOffset' => 0.0,
                'borderJoinStyle' => 'bevel',
                'pointBorderColor' => "#E8ADAD",
                'pointBackgroundColor' => "#fff",
                'pointBorderWidth' => 1,
                'pointHoverRadius' => 5,
                'pointHoverBackgroundColor' => "#E8ADAD",
                'pointHoverBorderColor' => "#eef0f2",
                'pointHoverBorderWidth' => 1,
                'pointRadius' => 3,
                'pointHitRadius' => 1,
            ],
            'points' => []
        ],
        'pending_sales' => [
            'label' => 'Ventas Pendientes',
            'options' => [
                'fill' => true,
                'lineTension' => 0.3,
                'backgroundColor' => "#EBAE4422",
                'borderColor' => "#EBAE44",
                'borderDash' => [],
                'borderDashOffset' => 0.0,
                'pointBorderColor' => "#EBAE44",
                'pointBackgroundColor' => "#fff",
                'pointBorderWidth' => 1,
                'pointHoverRadius' => 5,
                'pointHoverBackgroundColor' => "#EBAE44",
                'pointHoverBorderColor' => "#eef0f2",
                'pointHoverBorderWidth' => 1,
                'pointRadius' => 3,
                'pointHitRadius' => 1,
            ],
            'points' => []
        ],
    ];

    for($i = 0;$i < 10;$i++){
        $ts = strtotime("-{$i} days");
        $day = date('Y-m-d',$ts);
        $label = strtoupper(strftime('%d/%b',$ts));
        $recentLabels[] = $label;
        
        $recentSeries['sales']['data'] = [
            'class' => 'Sale',
            'path' => 'Model',
        ];

        $recentSeries['sales']['points'][] = [
            'query' => [
                'conditions' => [
                    'Sale.sale_date' => $day
                ]
            ],
        ];
        
        $recentSeries['redeems']['data'] = [
            'class' => 'Redeem',
            'path' => 'Model',
        ];

        $recentSeries['redeems']['points'][] = [
            'query' => [
                'conditions' => [
                    'DATE(Redeem.created)' => $day
                ]
            ],
        ];
        
        $recentSeries['complains']['data'] = [
            'class' => 'Complain',
            'path' => 'Model',
        ];

        $recentSeries['complains']['points'][] = [
            'query' => [
                'conditions' => [
                    'DATE(Complain.created)' => $day
                ]
            ],
        ];
        
        $recentSeries['pending_sales']['data'] = [
            'class' => 'Sale',
            'path' => 'Model',
        ];

        $recentSeries['pending_sales']['points'][] = [
            'query' => [
                'conditions' => [
                    'DATE(Sale.sale_date)' => $day,
                    'Sale.massive_sale_id' => null,
                    'Sale.approval' => 'responsable',
                    'Sale.rejected' => 0
                ]
            ],
        ];
    }

    $config['Klezkaffold.dashboard.backend']['recent'] = [
        'title' => "Actividad Reciente (últimos $days días)",
        'type' => 'line_graph',
        'graph' => [
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'min' => 0,
                            'stepSize' => 1
                        ]
                    ]
                ]
            ]
        ],    
        'url' => [ 'controller' => 'sales','action' => 'show'],
        'data' => [
            'labels' => $recentLabels,
            'series' => $recentSeries
        ]
    ];
    
    $config['Klezkaffold.dashboard.backend']['redeems'] = [
        'title' => 'Canjes Pendientes',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Canjes Pendientes',
        'url' => [
            'controller' => 'redeems', 'action' => 'show_pending'
        ],
        'data' => Configure::read('Klezkaffold.show.redeems.show_pending.data')
    ];
    
    $config['Klezkaffold.dashboard.backend']['complains'] = [
        'title' => 'Consultas sin Leer',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Consultas no Leidas',
        'url' => [
            'controller' => 'complains', 'action' => 'show_unread'
        ],
        'data' => Configure::read('Klezkaffold.show.complains.show_unread.data')
    ];
    
    $config['Klezkaffold.dashboard.backend']['pending_sales'] = [
        'title' => 'Ventas Manuales Pendientes',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas Manuales Pendientes de Aprobacion',
        'url' => [
            'controller' => 'sales', 'action' => 'show_manual_pending'
        ],
        'data' => Configure::read('Klezkaffold.show.sales.show_manual_pending.data')
    ];
    
//    $config['Klezkaffold.dashboard.backend']['complain_sales'] = [
//        'title' => 'Reclamos de Ventas Pendientes',
//        'type' => 'summary_pie',
//        'text' => 'Cantidad de Reclamos de Ventas Pendientes',
//        'url' => [
//            'controller' => 'complain_sales', 'action' => 'show_pending'
//        ],
//        'data' => Configure::read('Klezkaffold.show.complain_sales.show_pending.data')
//    ];
}

############################################################# PROFILE FORM

$config['Klezkaffold.request_form.profile'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'profile'
    ],
];

############################################################# PROFILE EDIT

$config['Klezkaffold.edit.profile'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'profile'
    ],
];

############################################################# FOTO PERFIL

$config['Klezkaffold.image.photo.profile'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'field' => 'picture',
        'format' => 'image/png',
        'file' => 'foto_perfil',
        'schema' => 'profile'
    ],
    'params' => [
        'id' => 'id',
    ],
];
