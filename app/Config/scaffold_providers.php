<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

    $config['Klezkaffold.dashboard.providers'] = [
    'show' => [
        'title' => 'Proveedores Activas',
        'type' => 'summary_pie',
        'text' => 'Proveedores de Ciudades',
        'url' => [
            'controller' => 'providers', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Provider',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Provider.status' => true
                ],
                'order' => 'Provider.id DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Proveedores Suspendidos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Provedores',
        'url' => [
            'controller' => 'providers', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Provider',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Provider.status' => false
                ],
                'order' => 'Provider.id DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.providers',
    'edit.providers',
];

############################################################# LISTAS

$config['Klezkaffold.show.providers.show'] = [
    'data' => $config['Klezkaffold.dashboard.providers']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.providers.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.providers']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.providers'] = [
    'data' => [
        'class' => 'Provider',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.providers'] = [
    'data' => [
        'class' => 'Provider',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.providers'] = [
    'data' => [
        'class' => 'Provider',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.providers'] = [
    'data' => [
        'class' => 'Provider',
        'path' => 'Model',
    ],
];
