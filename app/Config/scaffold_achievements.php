<?php

$config = [];
$limit = 10;

# ACHIEVEMENTS
############################################################# ACTIONS

$actions = [
    'detail.achievements',
    'edit.achievements',
    // 'achievement_products.achievements',
];

############################################################# LISTAS

$config['Klezkaffold.show.achievements.show'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'conditions' => [
                
            ],
            'joins' => [
                
            ],
            'limit' => $limit
        ],
        'prequery' => [
            
        ]
    ],
    'actions' => $actions
];


############################################################# DETALLE

$config['Klezkaffold.detail.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# CREAR

$config['Klezkaffold.add.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
    ],
];

$config['Klezkaffold.request_form.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
    ],
];

############################################################# EDITAR

$config['Klezkaffold.edit.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'schema' => 'writable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# iconos

$config['Klezkaffold.image.icon_0.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'field' => 'icon_0',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'icon0'
    ],
    'params' => [
        'id' => 'id',
    ],
];
$config['Klezkaffold.image.icon_1.achievements'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'field' => 'icon_1',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'icon1'
    ],
    'params' => [
        'id' => 'id',
    ],
];
