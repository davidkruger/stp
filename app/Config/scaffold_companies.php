<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.companies'] = [
    'show' => [
        'title' => 'Empresas Activas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Empresas',
        'url' => [
            'controller' => 'companies', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Company',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Company.status' => true
                ],
                'order' => 'Company.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Empresas Suspendidas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Empresas',
        'url' => [
            'controller' => 'companies', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Company',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Company.status' => false
                ],
                'order' => 'Company.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.companies',
    'edit.companies',
    'branches.companies',
    'stock.companies',
];

############################################################# LISTAS

$config['Klezkaffold.show.companies.show'] = [
    'data' => $config['Klezkaffold.dashboard.companies']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.companies.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.companies']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
    ],
];
############################################################# CARGA MASIVA

$config['Klezkaffold.massive.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
        'schema' => 'massive',
        'massive' => [
            'source' => 'document',
            'mode' => 'singles',
            'class' => 'Sale',
            'path' => 'Model',
            'cache' => 'massive',
            'beforeSave' => [
                'massiveDefaults'
            ],
            'map' => [
                'name' => [
                    'type' => 'date',
                    'label' => 'Nombre Empresa'
                ],
                'ruc' => [
                    'type' => 'text',
                    'label' => 'RUC Empresa',
                ],
                'social_reason' => [
                    'type' => 'text',
                    'label' => 'Razon Social',
                ],
                'telephone' => [
                    'type' => 'text',
                    'label' => 'Telefono',
                ],
                'email' => [
                    'type' => 'text',
                    'label' => 'E-mail',
                ],
            ]
        ]
    ],
];

$config['Klezkaffold.request_massive.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
        'schema' => 'massive',
    ],
];

############################################################# LOGO

$config['Klezkaffold.image.logo.companies'] = [
    'data' => [
        'class' => 'Company',
        'path' => 'Model',
        'field' => 'logo',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'logo'
    ],
    'params' => [
        'id' => 'id',
    ],
];
