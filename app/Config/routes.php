<?php

########################################### WEBSERVICE

Router::connect('/backoffice/webservice/:endpoint.:format', 
    [ 'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi' ],
    [ 'endpoint', 'format' ]
);

########################################### FRONTEND

Router::connect('/', [ 'controller' => 'frontend', 'action' => 'home' ]);
Router::connect('/login.html', [ 'controller' => 'frontend', 'action' => 'login' ]);
Router::connect('/catalogo.html', [ 'controller' => 'frontend', 'action' => 'catalog' ]);
Router::connect('/mi_cuenta/cerrar_sesion.html', [ 'controller' => 'frontend', 'action' => 'logout' ]);
Router::connect('/bases-y-condiciones.pdf', [ 'controller' => 'frontend', 'action' => 'byc' ]);
Router::connect('/premios-solicitados.html', [ 'controller' => 'frontend', 'action' => 'prizes' ]);
Router::connect('/consultas.html', [ 'controller' => 'frontend', 'action' => 'complains' ]);
Router::connect('/fotos/:id/premio.png', [ 'controller' => 'frontend', 'action' => 'prize_photo' ]);
Router::connect('/fotos/:id/producto.png', [ 'controller' => 'frontend', 'action' => 'product_photo' ]);
Router::connect('/fotos/:id/catalogo.png', [ 'controller' => 'frontend', 'action' => 'catalog_photo' ]);
Router::connect('/fotos/:id/competencia/premio.png', [ 'controller' => 'frontend', 'action' => 'run_sale_prize' ]);
Router::connect('/fotos/:id/logro/icono_0.png', [ 'controller' => 'frontend', 'action' => 'achievement_icon_0' ]);
Router::connect('/fotos/:id/logro/icono_1.png', [ 'controller' => 'frontend', 'action' => 'achievement_icon_1' ]);
Router::connect('/ventas.html', [ 'controller' => 'frontend', 'action' => 'sales' ]);
Router::connect('/vendedores.html', [ 'controller' => 'frontend', 'action' => 'sellers' ]);
Router::connect('/vendedores/agregar.html', [ 'controller' => 'frontend', 'action' => 'add_seller' ]);
Router::connect('/vendedores/asignar_puntos.html', [ 'controller' => 'frontend', 'action' => 'assign_points' ]);
Router::connect('/ventas/agregar.html', [ 'controller' => 'frontend', 'action' => 'add_sale' ]);
Router::connect('/ventas/pendientes.html', [ 'controller' => 'frontend', 'action' => 'pending_sales' ]);
Router::connect('/ventas/:id/aprobar.html', [ 'controller' => 'frontend', 'action' => 'approve' ]);
Router::connect('/ventas/masiva.html', [ 'controller' => 'frontend', 'action' => 'add_massive_sale' ]);
Router::connect('/ventas/:id/rechazar.html', [ 'controller' => 'frontend', 'action' => 'reject' ]);
Router::connect('/ventas/rechazadas.html', [ 'controller' => 'frontend', 'action' => 'rejected' ]);
Router::connect('/mi_cuenta/editar.html', [ 'controller' => 'frontend', 'action' => 'edit_profile' ]);
Router::connect('/loteria.html', [ 'controller' => 'frontend', 'action' => 'lottery' ]);
Router::connect('/ruleta.html', [ 'controller' => 'frontend', 'action' => 'roulette' ]);
Router::connect('/fotos/:id/loteria/premio.png', [ 'controller' => 'frontend', 'action' => 'lottery_prize_photo' ]);
Router::connect('/fotos/:id/desafios/premio.png', [ 'controller' => 'frontend', 'action' => 'challenge_prize_photo' ]);

########################################### BACKEND

Router::connect('/backoffice', [ 'controller' => 'backend', 'action' => 'home' ]);
Router::connect('/backoffice.html', [ 'controller' => 'backend', 'action' => 'home' ]);
Router::connect('/backoffice/login.html', [ 'controller' => 'gateway', 'action' => 'login', 'plugin' => 'KlezBackend' ]);
Router::connect('/backoffice/logout.html', [ 'controller' => 'gateway', 'action' => 'logout', 'plugin' => 'KlezBackend' ]);
Router::connect('/backoffice/mi-perfil/editar.html', [ 'controller' => 'backend', 'action' => 'profile_edit', 'plugin' => null ]);
Router::connect('/backoffice/mi-perfil/foto.png', [ 'controller' => 'backend', 'action' => 'profile_photo', 'plugin' => null ]);

########################################### USUARIOS

Router::connect('/backoffice/usuarios.html', [ 'controller' => 'users', 'action' => 'dashboard' ]);
Router::connect('/backoffice/usuarios/crear.html', [ 'controller' => 'users', 'action' => 'add' ]);
//Router::connect('/backoffice/usuarios/listar/romis.html', [ 'controller' => 'users', 'action' => 'show_romis' ]);
Router::connect('/backoffice/usuarios/listar/samsung.html', [ 'controller' => 'users', 'action' => 'show_samsung' ]);
Router::connect('/backoffice/usuarios/listar/lamoderna.html', [ 'controller' => 'users', 'action' => 'show_lamoderna' ]);
Router::connect('/backoffice/usuarios/listar/responsables.html', [ 'controller' => 'users', 'action' => 'show_responsibles' ]);
Router::connect('/backoffice/usuarios/listar/vendedores.html', [ 'controller' => 'users', 'action' => 'show_sellers' ]);
Router::connect('/backoffice/usuarios/listar/suspendidos.html', [ 'controller' => 'users', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/usuarios/detallar/:id/:slug.html', [ 'controller' => 'users', 'action' => 'detail' ]);
Router::connect('/backoffice/usuarios/foto/:id.png', [ 'controller' => 'users', 'action' => 'picture' ]);
Router::connect('/backoffice/usuarios/editar/:id/:slug.html', [ 'controller' => 'users', 'action' => 'edit' ]);
Router::connect('/backoffice/usuarios/listar/encargados.html', [ 'controller' => 'users', 'action' => 'show_encargados' ]);
Router::connect('/backoffice/vendedores/carga-masiva.html', [ 'controller' => 'users', 'action' => 'massive' ]);

########################################### VENDEDORES
Router::connect('/backoffice/vendedores.html', [ 'controller' => 'salesmen', 'action' => 'dashboard' ]);
Router::connect('/backoffice/vendedores/:company/:slug.html', [ 'controller' => 'salesmen', 'action' => 'by_branches',  ]);
Router::connect('/backoffice/vendedores/listar/:branch/:slug.html', [ 'controller' => 'salesmen', 'action' => 'show_by_branches',  ]);
Router::connect('/backoffice/vendedores/detallar/:id/:slug.html', [ 'controller' => 'salesmen', 'action' => 'detail' ]);

########################################### EMPRESAS

Router::connect('/backoffice/empresas.html', [ 'controller' => 'companies', 'action' => 'dashboard' ]);
Router::connect('/backoffice/empresas/crear.html', [ 'controller' => 'companies', 'action' => 'add' ]);
Router::connect('/backoffice/empresas/listar/activas.html', [ 'controller' => 'companies', 'action' => 'show' ]);
Router::connect('/backoffice/empresas/listar/suspendidas.html', [ 'controller' => 'companies', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/empresas/detallar/:id/:slug.html', [ 'controller' => 'companies', 'action' => 'detail' ]);
Router::connect('/backoffice/empresas/editar/:id/:slug.html', [ 'controller' => 'companies', 'action' => 'edit' ]);
Router::connect('/backoffice/empresas/contactos/:id/:slug.html', [ 'controller' => 'companies', 'action' => 'company_contacts' ]);
Router::connect('/backoffice/empresas/carga-masiva.html', [ 'controller' => 'companies', 'action' => 'massive' ]);
Router::connect('/backoffice/empresas/logo/:id.png', [ 'controller' => 'companies', 'action' => 'logo' ]);
Router::connect('/backoffice/empresas/sucursales/:id/:slug.html', [ 'controller' => 'companies', 'action' => 'branches' ]);
Router::connect('/backoffice/empresas/stock/:id/:slug.html', [ 'controller' => 'companies', 'action' => 'stock' ]);

########################################### STOCKS DE LA EMPRESA

Router::connect('/backoffice/empresas/:company/crear-stock/:slug.html', [ 'controller' => 'stock', 'action' => 'add' ]);
Router::connect('/backoffice/empresas/:company/eliminar-stock/:id/:slug.html.html', [ 'controller' => 'stock', 'action' => 'delete' ]);
Router::connect('/backoffice/empresas/:company/editar-stock/:id/:slug.html.html', [ 'controller' => 'stock', 'action' => 'edit' ]);

########################################### SUCURSALES DE LA EMPRESA

Router::connect('/backoffice/empresas/:company/crear-sucursal/:slug.html', [ 'controller' => 'branches', 'action' => 'add' ]);
Router::connect('/backoffice/empresas/:company/eliminar-sucursal/:id/:slug.html', [ 'controller' => 'branches', 'action' => 'delete' ]);
Router::connect('/backoffice/empresas/:company/editar-sucursal/:id/:slug.html', [ 'controller' => 'branches', 'action' => 'edit' ]);

########################################### CONTACTOS DE EMPRESA

Router::connect('/backoffice/empresas/:company/agregar-contacto/:slug.html', [ 'controller' => 'company_contacts', 'action' => 'add' ]);
Router::connect('/backoffice/empresas/:company/eliminar-contacto/:id/:slug.html', [ 'controller' => 'company_contacts', 'action' => 'delete' ]);
Router::connect('/backoffice/empresas/:company/editar-contacto/:id/:slug.html', [ 'controller' => 'company_contacts', 'action' => 'edit' ]);

########################################### DIVISIONES

Router::connect('/backoffice/divisiones.html', [ 'controller' => 'divisions', 'action' => 'dashboard' ]);
Router::connect('/backoffice/divisiones/crear.html', [ 'controller' => 'divisions', 'action' => 'add' ]);
Router::connect('/backoffice/divisiones/listar/activas.html', [ 'controller' => 'divisions', 'action' => 'show' ]);
Router::connect('/backoffice/divisiones/listar/suspendidas.html', [ 'controller' => 'divisions', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/divisiones/detallar/:id/:slug.html', [ 'controller' => 'divisions', 'action' => 'detail' ]);
Router::connect('/backoffice/divisiones/editar/:id/:slug.html', [ 'controller' => 'divisions', 'action' => 'edit' ]);
Router::connect('/backoffice/divisiones/subdivisiones/:id/:slug.html', [ 'controller' => 'divisions', 'action' => 'subdivisions' ]);

########################################### GRUPOS

Router::connect('/backoffice/grupos.html', [ 'controller' => 'groups', 'action' => 'dashboard' ]);
Router::connect('/backoffice/grupos/crear.html', [ 'controller' => 'groups', 'action' => 'add' ]);
Router::connect('/backoffice/grupos/listar/activas.html', [ 'controller' => 'groups', 'action' => 'show' ]);
Router::connect('/backoffice/grupos/listar/suspendidas.html', [ 'controller' => 'groups', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/grupos/detallar/:id/:slug.html', [ 'controller' => 'groups', 'action' => 'detail' ]);
Router::connect('/backoffice/grupos/editar/:id/:slug.html', [ 'controller' => 'groups', 'action' => 'edit' ]);

########################################### GRUPOS RESTRINGIDOS

Router::connect('/backoffice/restringir/grupos.html', [ 'controller' => 'disabled_groups', 'action' => 'dashboard' ]);
Router::connect('/backoffice/restringir/grupos/crear.html', [ 'controller' => 'disabled_groups', 'action' => 'add' ]);
Router::connect('/backoffice/restringir/grupos/listar/activas.html', [ 'controller' => 'disabled_groups', 'action' => 'show' ]);
Router::connect('/backoffice/restringir/grupos/listar/suspendidas.html', [ 'controller' => 'disabled_groups', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/restringir/grupos/:id/detalle.html', [ 'controller' => 'disabled_groups', 'action' => 'detail' ]);
Router::connect('/backoffice/restringir/grupos/:id/editar.html', [ 'controller' => 'disabled_groups', 'action' => 'edit' ]);

########################################### SUBDIVISIONES

Router::connect('/backoffice/subdivisiones/:division/crear/:slug.html', [ 'controller' => 'subdivisions', 'action' => 'add' ]);
Router::connect('/backoffice/subdivisiones/:division/eliminar/:id/:slug.html', [ 'controller' => 'subdivisions', 'action' => 'delete' ]);
Router::connect('/backoffice/subdivisiones/:division/editar/:id/:slug.html', [ 'controller' => 'subdivisions', 'action' => 'edit' ]);
Router::connect('/backoffice/subdivisiones/segmentos/:id/:slug.html', [ 'controller' => 'subdivisions', 'action' => 'segments' ]);

########################################### SEGMENTOS

Router::connect('/backoffice/segmentos/:subdivision/crear/:slug.html', [ 'controller' => 'segments', 'action' => 'add' ]);
Router::connect('/backoffice/segmentos/:subdivision/eliminar/:id/:slug.html', [ 'controller' => 'segments', 'action' => 'delete' ]);
Router::connect('/backoffice/segmentos/:subdivision/editar/:id/:slug.html', [ 'controller' => 'segments', 'action' => 'edit' ]);

########################################### PRODUCTOS

Router::connect('/backoffice/productos.html', [ 'controller' => 'products', 'action' => 'dashboard' ]);
Router::connect('/backoffice/productos/crear.html', [ 'controller' => 'products', 'action' => 'add' ]);
Router::connect('/backoffice/productos/listar/activos.html', [ 'controller' => 'products', 'action' => 'show' ]);
Router::connect('/backoffice/productos/listar/suspendidos.html', [ 'controller' => 'products', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/productos/detallar/:id/:slug.html', [ 'controller' => 'products', 'action' => 'detail' ]);
Router::connect('/backoffice/productos/editar/:id/:slug.html', [ 'controller' => 'products', 'action' => 'edit' ]);
Router::connect('/backoffice/productos/promociones/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_promotions' ]);
Router::connect('/backoffice/productos/fotos/:id.png', [ 'controller' => 'products', 'action' => 'picture' ]);
Router::connect('/backoffice/productos/caracteristicas/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_specs' ]);
Router::connect('/backoffice/productos/catalogos/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_catalogs' ]);
Router::connect('/backoffice/productos/buscar.html', [ 'controller' => 'products', 'action' => 'search' ]);

########################################### PROMOCIONES DE PRODUCTOS

Router::connect('/backoffice/productos/:product/agregar-promocion/:slug.html', [ 'controller' => 'product_promotions', 'action' => 'add' ]);
Router::connect('/backoffice/productos/:product/eliminar-promocion/:id/:slug.html', [ 'controller' => 'product_promotions', 'action' => 'delete' ]);
Router::connect('/backoffice/productos/editar/:id/:slug.html', [ 'controller' => 'products', 'action' => 'edit' ]);
Router::connect('/backoffice/productos/promociones/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_promotions' ]);
Router::connect('/backoffice/productos/fotos/:id.png', [ 'controller' => 'products', 'action' => 'picture' ]);
Router::connect('/backoffice/productos/caracteristicas/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_specs' ]);
Router::connect('/backoffice/productos/catalogos/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_catalogs' ]);
Router::connect('/backoffice/productos/buscar.html', [ 'controller' => 'products', 'action' => 'search' ]);
Router::connect('/backoffice/productos/puntos-para-empresa/:id/:slug.html', [ 'controller' => 'products', 'action' => 'product_points' ]);

########################################### PROMOCIONES DE PRODUCTOS

Router::connect('/backoffice/productos/:product/agregar-promocion/:slug.html', [ 'controller' => 'product_promotions', 'action' => 'add' ]);
Router::connect('/backoffice/productos/:product/eliminar-promocion/:id/:slug.html', [ 'controller' => 'product_promotions', 'action' => 'delete' ]);
Router::connect('/backoffice/productos/:product/editar-promocion/:id/:slug.html', [ 'controller' => 'product_promotions', 'action' => 'edit' ]);

########################################### PUNTOS PARA EMPRESAS

Router::connect('/backoffice/productos/:product/agregar-puntos-para-empresa/:slug.html', [ 'controller' => 'product_points', 'action' => 'add' ]);
Router::connect('/backoffice/productos/:product/eliminar-puntos-para-empresa/:id/:slug.html', [ 'controller' => 'product_points', 'action' => 'delete' ]);
Router::connect('/backoffice/productos/:product/editar-puntos-para-empresa/:id/:slug.html', [ 'controller' => 'product_points', 'action' => 'edit' ]);

########################################### SPECS DE PRODUCTOS

Router::connect('/backoffice/productos/:product/agregar-catacteristica/:slug.html', [ 'controller' => 'product_specs', 'action' => 'add' ]);
Router::connect('/backoffice/productos/:product/eliminar-catacteristica/:id/:slug.html', [ 'controller' => 'product_specs', 'action' => 'delete' ]);
Router::connect('/backoffice/productos/:product/editar-catacteristica/:id/:slug.html', [ 'controller' => 'product_specs', 'action' => 'edit' ]);

########################################### CATALOGO DE PRODUCTOS

Router::connect('/backoffice/productos/:product/agregar-catalogo/:slug.html', [ 'controller' => 'product_catalogs', 'action' => 'add' ]);
Router::connect('/backoffice/productos/:product/eliminar-catalogo/:id/:slug.html', [ 'controller' => 'product_catalogs', 'action' => 'delete' ]);
Router::connect('/backoffice/productos/:product/editar-catalogo/:id/:slug.html', [ 'controller' => 'product_catalogs', 'action' => 'edit' ]);
Router::connect('/backoffice/catalogos/:id.png', [ 'controller' => 'product_catalogs', 'action' => 'image' ]);

########################################### PREMIOS

Router::connect('/backoffice/premios.html', [ 'controller' => 'prizes', 'action' => 'dashboard' ]);
Router::connect('/backoffice/premios/crear.html', [ 'controller' => 'prizes', 'action' => 'add' ]);
Router::connect('/backoffice/premios/listar/activos.html', [ 'controller' => 'prizes', 'action' => 'show' ]);
Router::connect('/backoffice/premios/listar/suspendidos.html', [ 'controller' => 'prizes', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/premios/detallar/:id/:slug.html', [ 'controller' => 'prizes', 'action' => 'detail' ]);
Router::connect('/backoffice/premios/editar/:id/:slug.html', [ 'controller' => 'prizes', 'action' => 'edit' ]);
Router::connect('/backoffice/premios/fotos/:id/:slug.html', [ 'controller' => 'prizes', 'action' => 'prize_photos' ]);
Router::connect('/backoffice/premios/fotos/:id.png', [ 'controller' => 'prizes', 'action' => 'photo' ]);
Router::connect('/backoffice/premios/pdf/:id.pdf', [ 'controller' => 'prizes', 'action' => 'pdf' ]);

########################################### SPECS

Router::connect('/backoffice/caracteristicas.html', [ 'controller' => 'specs', 'action' => 'dashboard' ]);
Router::connect('/backoffice/caracteristicas/crear.html', [ 'controller' => 'specs', 'action' => 'add' ]);
Router::connect('/backoffice/caracteristicas/listar/activos.html', [ 'controller' => 'specs', 'action' => 'show' ]);
Router::connect('/backoffice/caracteristicas/listar/suspendidos.html', [ 'controller' => 'specs', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/caracteristicas/detallar/:id/:slug.html', [ 'controller' => 'specs', 'action' => 'detail' ]);
Router::connect('/backoffice/caracteristicas/editar/:id/:slug.html', [ 'controller' => 'specs', 'action' => 'edit' ]);

########################################### VENTAS

Router::connect('/backoffice/ventas.html', [ 'controller' => 'sales', 'action' => 'dashboard' ]);
Router::connect('/backoffice/ventas/todas.html', [ 'controller' => 'sales', 'action' => 'show' ]);
Router::connect('/backoffice/ventas/manuales/aprobadas.html', [ 'controller' => 'sales', 'action' => 'show_manual' ]);
Router::connect('/backoffice/ventas/manuales/pendientes.html', [ 'controller' => 'sales', 'action' => 'show_manual_pending' ]);
Router::connect('/backoffice/ventas/masivas/aprobadas.html', [ 'controller' => 'sales', 'action' => 'show_massive' ]);
Router::connect('/backoffice/ventas/masivas/pendientes.html', [ 'controller' => 'sales', 'action' => 'show_massive_pending' ]);
Router::connect('/backoffice/ventas/carga-manual.html', [ 'controller' => 'sales', 'action' => 'add' ]);
Router::connect('/backoffice/ventas/carga-masiva.html', [ 'controller' => 'sales', 'action' => 'massive' ]);
Router::connect('/backoffice/ventas/detallar/:id.html', [ 'controller' => 'sales', 'action' => 'detail' ]);
Router::connect('/backoffice/ventas/eliminar-venta/:id.html', [ 'controller' => 'sales', 'action' => 'delete' ]);
Router::connect('/backoffice/ventas/detalles/:id.html', [ 'controller' => 'sales', 'action' => 'sale_details' ]);
Router::connect('/backoffice/ventas/aprobar/:id.html', [ 'controller' => 'sales', 'action' => 'approve_manual' ]);
Router::connect('/backoffice/ventas-masivas/aprobar/:id.html', [ 'controller' => 'sales', 'action' => 'approve_massive' ]);
Router::connect('/backoffice/ventas-masivas/detalle/:id.html', [ 'controller' => 'sales', 'action' => 'detail_massive' ]);
Router::connect('/backoffice/ventas/rechazar/:id.html', [ 'controller' => 'sales', 'action' => 'reject_manual' ]);
Router::connect('/backoffice/ventas-masivas/rechazar/:id.html', [ 'controller' => 'sales', 'action' => 'reject_massive' ]);
Router::connect('/backoffice/ventas/foto/:id.png', [ 'controller' => 'sales', 'action' => 'photo' ]);

########################################### COMPETENCIAS
Router::connect('/backoffice/_competencias.html', [ 'controller' => 'run_sales', 'action' => 'dashboard' ]);
Router::connect('/backoffice/competencias.html', [ 'controller' => 'run_sales', 'action' => 'show' ]);
Router::connect('/backoffice/competencias/crear.html', [ 'controller' => 'run_sales', 'action' => 'add' ]);
Router::connect('/backoffice/competencias/detallar/:id/:slug.html', [ 'controller' => 'run_sales', 'action' => 'detail' ]);
Router::connect('/backoffice/competencias/editar/:id/:slug.html', [ 'controller' => 'run_sales', 'action' => 'edit' ]);
Router::connect('/backoffice/competencias/premio/:id.png', [ 'controller' => 'run_sales', 'action' => 'prize_img' ]);
Router::connect('/backoffice/competencias/reporte/:id/:slug.xlsx', [ 'controller' => 'run_sales', 'action' => 'xlsx' ]);
Router::connect('/backoffice/competencias/sorteo/:id/:slug.html', [ 'controller' => 'run_sales', 'action' => 'draw' ]);

########################################### CANJES

Router::connect('/backoffice/canjes/pendientes-de-moderacion.html', [ 'controller' => 'redeems', 'action' => 'show_pending' ]);
Router::connect('/backoffice/canjes/aprobados.html', [ 'controller' => 'redeems', 'action' => 'show_approved' ]);
Router::connect('/backoffice/canjes/rechazados.html', [ 'controller' => 'redeems', 'action' => 'show_rejected' ]);
Router::connect('/backoffice/canjes/detalle/:id.html', [ 'controller' => 'redeems', 'action' => 'detail' ]);
Router::connect('/backoffice/canjes/moderar/:id.html', [ 'controller' => 'redeems', 'action' => 'edit' ]);
Router::connect('/backoffice/canjes/moderacion/:id.html', [ 'controller' => 'redeem_moderations', 'action' => 'detail' ]);
Router::connect('/backoffice/canjes/moderacion-masiva.html', [ 'controller' => 'redeems', 'action' => 'massive' ]);

########################################### QUEJAS

Router::connect('/backoffice/consultas/leidas.html', [ 'controller' => 'complains', 'action' => 'show_read' ]);
Router::connect('/backoffice/consultas/no-leidas.html', [ 'controller' => 'complains', 'action' => 'show_unread' ]);
Router::connect('/backoffice/consultas/leer/:id/:slug.html', [ 'controller' => 'complains', 'action' => 'detail' ]);

########################################### RECLAMOS DE VENTAS

Router::connect('/backoffice/reclamos-de-ventas/pendientes-de-moderacion.html', [ 'controller' => 'complain_sales', 'action' => 'show_pending' ]);
Router::connect('/backoffice/reclamos-de-ventas/aprobados.html', [ 'controller' => 'complain_sales', 'action' => 'show_approved' ]);
Router::connect('/backoffice/reclamos-de-ventas/rechazados.html', [ 'controller' => 'complain_sales', 'action' => 'show_rejected' ]);
Router::connect('/backoffice/reclamos-de-ventas/detalle/:id.html', [ 'controller' => 'complain_sales', 'action' => 'detail' ]);
Router::connect('/backoffice/reclamos-de-ventas/moderar/:id.html', [ 'controller' => 'complain_sales', 'action' => 'edit' ]);
Router::connect('/backoffice/reclamos-de-ventas/moderacion/:id.html', [ 'controller' => 'complain_sale_moderations', 'action' => 'detail' ]);

########################################### CIUDADES

Router::connect('/backoffice/ciudades.html', [ 'controller' => 'cities', 'action' => 'dashboard' ]);
Router::connect('/backoffice/ciudades/crear.html', [ 'controller' => 'cities', 'action' => 'add' ]);
Router::connect('/backoffice/ciudades/listar/activas.html', [ 'controller' => 'cities', 'action' => 'show' ]);
Router::connect('/backoffice/ciudades/listar/suspendidas.html', [ 'controller' => 'cities', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/ciudades/detallar/:id/:slug.html', [ 'controller' => 'cities', 'action' => 'detail' ]);
Router::connect('/backoffice/ciudades/editar/:id/:slug.html', [ 'controller' => 'cities', 'action' => 'edit' ]);

########################################### PROVEEDORES

Router::connect('/backoffice/proveedores.html', [ 'controller' => 'providers', 'action' => 'dashboard' ]);
Router::connect('/backoffice/proveedores/crear.html', [ 'controller' => 'providers', 'action' => 'add' ]);
Router::connect('/backoffice/proveedores/listar/activos.html', [ 'controller' => 'providers', 'action' => 'show' ]);
Router::connect('/backoffice/proveedores/listar/suspendidos.html', [ 'controller' => 'providers', 'action' => 'show_disabled' ]);
Router::connect('/backoffice/proveedores/detallar/:id/:slug.html', [ 'controller' => 'providers', 'action' => 'detail' ]);
Router::connect('/backoffice/proveedores/editar/:id/:slug.html', [ 'controller' => 'providers', 'action' => 'edit' ]);

########################################### REPORTES

Router::connect('/backoffice/reportes/acumulado-de-puntos.html', [ 'controller' => 'reports', 'action' => 'summary' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/total.html', [ 'controller' => 'reports', 'action' => 'total' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/por-empresa.html', [ 'controller' => 'reports', 'action' => 'companies' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/top-10.html', [ 'controller' => 'reports', 'action' => 'top10' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/por-producto.html', [ 'controller' => 'reports', 'action' => 'products' ]);
Router::connect('/backoffice/reportes/ventas.html', [ 'controller' => 'reports', 'action' => 'sales_xlsx' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/top-10/general.html', [ 'controller' => 'reports', 'action' => 'top10general' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/top-10/mes-pasado.html', [ 'controller' => 'reports', 'action' => 'top10lastmonth' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/top-10/division.html', [ 'controller' => 'reports', 'action' => 'top10division' ]);
Router::connect('/backoffice/reportes/acumulado-de-puntos/top-10/division-mes-pasado.html', [ 'controller' => 'reports', 'action' => 'top10divisionlastmonth' ]);
Router::connect('/backoffice/reportes/logros.html', [ 'controller' => 'reports', 'action' => 'achievements_xlsx' ]);
Router::connect('/backoffice/reportes/desafios.html', [ 'controller' => 'reports', 'action' => 'challenges_xlsx' ]);
Router::connect('/backoffice/reportes/empresas.html', [ 'controller' => 'reports', 'action' => 'companies_xlsx' ]);
Router::connect('/backoffice/reportes/usuarios.html', [ 'controller' => 'reports', 'action' => 'users_xlsx' ]);

########################################### RULETAS
Router::connect('/backoffice/_ruletas.html', [ 'controller' => 'roulettes', 'action' => 'dashboard' ]);
Router::connect('/backoffice/ruletas.html', [ 'controller' => 'roulettes', 'action' => 'show' ]);
Router::connect('/backoffice/ruletas/crear.html', [ 'controller' => 'roulettes', 'action' => 'add' ]);
Router::connect('/backoffice/ruletas/detallar/:id/:slug.html', [ 'controller' => 'roulettes', 'action' => 'detail' ]);
Router::connect('/backoffice/ruletas/editar/:id/:slug.html', [ 'controller' => 'roulettes', 'action' => 'edit' ]);
Router::connect('/backoffice/ruletas/reporte/:id/:slug.xlsx', [ 'controller' => 'roulettes', 'action' => 'xlsx' ]);
Router::connect('/backoffice/ruletas/premios/:id/:slug.html', [ 'controller' => 'roulettes', 'action' => 'roulette_prizes' ]);

########################################### PREMIOS DE RULETA

Router::connect('/backoffice/ruletas/:roulette/agregar-premio/:slug.html', [ 'controller' => 'roulette_prizes', 'action' => 'add' ]);
Router::connect('/backoffice/ruletas/:roulette/eliminar-premio/:id/:slug.html', [ 'controller' => 'roulette_prizes', 'action' => 'delete' ]);
Router::connect('/backoffice/ruletas/:roulette/editar-premio/:id/:slug.html', [ 'controller' => 'roulette_prizes', 'action' => 'edit' ]);

########################################### LOTERIAS
Router::connect('/backoffice/_loterias.html', [ 'controller' => 'lotteries', 'action' => 'dashboard' ]);
Router::connect('/backoffice/loterias.html', [ 'controller' => 'lotteries', 'action' => 'show' ]);
Router::connect('/backoffice/loterias/crear.html', [ 'controller' => 'lotteries', 'action' => 'add' ]);
Router::connect('/backoffice/loterias/detallar/:id/:slug.html', [ 'controller' => 'lotteries', 'action' => 'detail' ]);
Router::connect('/backoffice/loterias/editar/:id/:slug.html', [ 'controller' => 'lotteries', 'action' => 'edit' ]);
Router::connect('/backoffice/loterias/premio/:id.png', [ 'controller' => 'lotteries', 'action' => 'prize_photo' ]);
Router::connect('/backoffice/loterias/reporte/:id/:slug.xlsx', [ 'controller' => 'lotteries', 'action' => 'xlsx' ]);
Router::connect('/backoffice/loterias/sorteo/:id/:slug.html', [ 'controller' => 'lotteries', 'action' => 'draw' ]);
Router::connect('/backoffice/loterias/tickets/:id/:slug.html', [ 'controller' => 'lotteries', 'action' => 'tickets' ]);

########################################### LOGROS
Router::connect('/backoffice/_logros.html', [ 'controller' => 'achievements', 'action' => 'dashboard' ]);
Router::connect('/backoffice/logros.html', [ 'controller' => 'achievements', 'action' => 'show' ]);
Router::connect('/backoffice/logros/crear.html', [ 'controller' => 'achievements', 'action' => 'add' ]);
Router::connect('/backoffice/logros/detallar/:id/:slug.html', [ 'controller' => 'achievements', 'action' => 'detail' ]);
Router::connect('/backoffice/logros/editar/:id/:slug.html', [ 'controller' => 'achievements', 'action' => 'edit' ]);
Router::connect('/backoffice/logros/icon_0/:id.png', [ 'controller' => 'achievements', 'action' => 'icon_0' ]);
Router::connect('/backoffice/logros/icon_1/:id.png', [ 'controller' => 'achievements', 'action' => 'icon_1' ]);
Router::connect('/backoffice/logros/productos/:id/:slug.html', [ 'controller' => 'achievements', 'action' => 'achievement_products' ]);

########################################### PRODUCTOS DEL LOGRO

Router::connect('/backoffice/logros/:achievement/crear-producto/:slug.html', [ 'controller' => 'achievement_products', 'action' => 'add' ]);
Router::connect('/backoffice/logros/:achievement/eliminar-producto/:id/:slug.html', [ 'controller' => 'achievement_products', 'action' => 'delete' ]);
Router::connect('/backoffice/logros/:achievement/editar-producto/:id/:slug.html', [ 'controller' => 'achievement_products', 'action' => 'edit' ]);

########################################### DESAFIOS
Router::connect('/backoffice/_desafios.html', [ 'controller' => 'challenges', 'action' => 'dashboard' ]);
Router::connect('/backoffice/desafios.html', [ 'controller' => 'challenges', 'action' => 'show' ]);
Router::connect('/backoffice/desafios/crear.html', [ 'controller' => 'challenges', 'action' => 'add' ]);
Router::connect('/backoffice/desafios/detallar/:id/:slug.html', [ 'controller' => 'challenges', 'action' => 'detail' ]);
Router::connect('/backoffice/desafios/editar/:id/:slug.html', [ 'controller' => 'challenges', 'action' => 'edit' ]);
Router::connect('/backoffice/desafios/premio/:id.png', [ 'controller' => 'challenges', 'action' => 'prize_img' ]);
Router::connect('/backoffice/desafios/productos/:id/:slug.html', [ 'controller' => 'challenges', 'action' => 'challenge_products' ]);

########################################### PRODUCTOS DEL DESAFIO

Router::connect('/backoffice/desafios/:challenge/crear-producto/:slug.html', [ 'controller' => 'challenge_products', 'action' => 'add' ]);
Router::connect('/backoffice/desafios/:challenge/eliminar-producto/:id/:slug.html', [ 'controller' => 'challenge_products', 'action' => 'delete' ]);
Router::connect('/backoffice/desafios/:challenge/editar-producto/:id/:slug.html', [ 'controller' => 'challenge_products', 'action' => 'edit' ]);

CakePlugin::routes();