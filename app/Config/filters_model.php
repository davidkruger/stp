<?php

################################# FILTERS

$filters = [];

$filters['any'] = [
    'data' => [
        'status' => true
    ]
];

$filters['users_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_write_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_massive_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array', 'lamoderna', 'samsung' ]
    ]
];

$filters['products_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['prizes_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['sales_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['sales_show'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung', 'responsable','vendedor','encargado' ]
    ]
];

$filters['sales_massive'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung', 'responsable','encargado' ]
    ]
];

$filters['complain_sales_moderation_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];
$filters['complain_sales_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung','vendedor' ]
    ]
];

$filters['complains_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complains_write_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','vendedor','responsable','encargado' ]
    ]
];

$filters['complain_sales_moderation_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complain_sales_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complain_sales_request_form'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung','responsable', 'vendedor','encargado' ]
    ]
];

$filters['redeems_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complain_sales_frontend_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','vendedor' ]
    ]
];

$filters['responsable_frontend'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','responsable','encargado' ]
    ]
];

$filters['frontend'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','responsable','encargado','vendedor' ]
    ]
];

$filters['sellers_request_form'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','vendedor','responsable','encargado' ]
    ]
];

################################# USERS

$config = [];

$config['KlezkaffoldFilter.User'] = [
    'filters' => $filters['users_abm']
];

$config['KlezkaffoldFilter.User@RequestForm::Profile'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.User@RequestForm::FrontendProfile'] = [
    'filters' => $filters['frontend']
];

$config['KlezkaffoldFilter.User@Edit::FrontendProfile'] = [
    'filters' => $filters['frontend']
];

$config['KlezkaffoldFilter.User@Edit::Profile'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.User@Image::Photo.profile'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.User@Add::Sellers'] = [
    'filters' => $filters['responsable_frontend']
];

################################# COMPANIES

$config['KlezkaffoldFilter.Company@Massive'] = [
    'filters' => $filters['companies_massive_abm']
];

$config['KlezkaffoldFilter.Company@RequestMassive'] = [
    'filters' => $filters['companies_massive_abm']
];

$config['KlezkaffoldFilter.Company@Dashboard'] = [
    'filters' => $filters['companies_read_abm']
];

$config['KlezkaffoldFilter.Company@RequestForm'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.Company@Add'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.Company@Show'] = [
    'filters' => $filters['companies_read_abm']
];

$config['KlezkaffoldFilter.Company@Edit'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.Company@Detail'] = [
    'filters' => $filters['companies_read_abm']
];

$config['KlezkaffoldFilter.CompanyContact@Show'] = [
    'filters' => $filters['companies_read_abm']
];

$config['KlezkaffoldFilter.CompanyContact@Edit'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.CompanyContact@RequestForm'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.CompanyContact@Delete'] = [
    'filters' => $filters['companies_write_abm']
];

$config['KlezkaffoldFilter.CompanyContact@RequestDelete'] = [
    'filters' => $filters['companies_write_abm']
];


################################# PRODUCTS

$config['KlezkaffoldFilter.Product@Image::Picture.products'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Product'] = [
    'filters' => $filters['products_abm']
];

$config['KlezkaffoldFilter.ProductPromotion'] = [
    'filters' => $filters['products_abm']
];

################################# PREMIOS

$config['KlezkaffoldFilter.Prize'] = [
    'filters' => $filters['prizes_abm']
];

$config['KlezkaffoldFilter.Prize@Image::Photo.prizes'] = [
    'filters' => $filters['any']
];

################################# VENTAS

$config['KlezkaffoldFilter.Sale'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.MassiveSale'] = [
    'filters' => $filters['sales_massive']
];

$config['KlezkaffoldFilter.Sale@RequestMassive'] = [
    'filters' => $filters['sales_massive']
];

$config['KlezkaffoldFilter.Sale@Massive'] = [
    'filters' => $filters['sales_massive']
];

$config['KlezkaffoldFilter.Sale@Show'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Sale@Detail'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Sale@Add'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Sale@RequestForm'] = [
    'filters' => $filters['any']
];

################################# COMPETENCIAS

$config['KlezkaffoldFilter.RunSale@Image::PrizeImg.runSales'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.RunSale'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.RunSaleCompany'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.RunSaleTarget'] = [
    'filters' => $filters['sales_abm']
];


$config['KlezkaffoldFilter.RunSaleLog'] = [
    'filters' => $filters['sales_abm']
];

################################# CANJES

$config['KlezkaffoldFilter.Redeem@Show'] = [
    'filters' => $filters['redeems_read_abm']
];
$config['KlezkaffoldFilter.Redeem@Detail'] = [
    'filters' => $filters['redeems_read_abm']
];

$config['KlezkaffoldFilter.RedeemModeration@Detail'] = [
    'filters' => $filters['redeems_read_abm']
];

################################# QUEJAS

$config['KlezkaffoldFilter.Complain'] = [
    'filters' => $filters['complains_write_abm']
];

$config['KlezkaffoldFilter.FullComplain@Show'] = [
    'filters' => $filters['complains_read_abm']
];

$config['KlezkaffoldFilter.FullComplain@Detail'] = [
    'filters' => $filters['complains_read_abm']
];

################################# RECLAMO DE VENTAS

$config['KlezkaffoldFilter.ComplainSale@RequestForm'] = [
    'filters' => $filters['complain_sales_request_form']
];

$config['KlezkaffoldFilter.ComplainSale@Add'] = [
    'filters' => $filters['responsable_frontend']
];

$config['KlezkaffoldFilter.ComplainSale@Show'] = [
    'filters' => $filters['complain_sales_read_abm']
];

$config['KlezkaffoldFilter.ComplainSale@Edit'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['KlezkaffoldFilter.ComplainSale@Detail'] = [
    'filters' => $filters['complain_sales_read_abm']
];

$config['KlezkaffoldFilter.ComplainSaleModeration@Detail'] = [
    'filters' => $filters['complain_sales_read_abm']
];

################################# VENDEDORES

$config['KlezkaffoldFilter.Seller@RequestForm'] = [
    'filters' => $filters['sellers_request_form']
];

################################# ROULETTES

$config['KlezkaffoldFilter.Roulette'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.RoulettePrize'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.RouletteCompany'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.RouletteIntent'] = [
    'filters' => $filters['sales_abm']
];

################################# LOTTERIES

$config['KlezkaffoldFilter.Lottery@Image::PrizePhoto.lotteries'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Lottery'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.LotteryCompany'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.LotteryIntent'] = [
    'filters' => $filters['sales_abm']
];

################################# LOGROS

$config['KlezkaffoldFilter.Achievement@Image::Icon0.achievements'] = [
    'filters' => $filters['any']
];
$config['KlezkaffoldFilter.Achievement@Image::Icon1.achievements'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Achievement'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.AchievementProduct'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.AchievementUser'] = [
    'filters' => $filters['sales_abm']
];


$config['KlezkaffoldFilter.AchievementLog'] = [
    'filters' => $filters['sales_abm']
];

################################# DESAFIOS

$config['KlezkaffoldFilter.Challenge@Image::PrizeImg.challenges'] = [
    'filters' => $filters['any']
];

$config['KlezkaffoldFilter.Challenge'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.ChallengeProduct'] = [
    'filters' => $filters['sales_abm']
];

$config['KlezkaffoldFilter.ChallengeUser'] = [
    'filters' => $filters['sales_abm']
];


$config['KlezkaffoldFilter.ChallengeLog'] = [
    'filters' => $filters['sales_abm']
];