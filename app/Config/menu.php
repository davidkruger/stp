<?php

$config = [];

$config['Menu.title'] = [
    'type' => 'text',
    'label' => 'Navegación'
];

$config['Menu.dashboard'] = [
    'type' => 'link',
    'icon' => 'home',
    'label' => 'Home',
    'url' => [ 'controller' => 'backend', 'action' => 'home', 'plugin' =>  null ]
];

$config['Menu.users'] = [
    'type' => 'parent',
    'icon' => 'accounts ',
    'label' => 'Usuarios',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'users', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'accounts-alt',
            'label' => 'Crear',
            'url' => [ 'controller' => 'users', 'action' => 'add' ]
        ],
        'massive' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Carga Masiva',
            'url' => [ 'controller' => 'users', 'action' => 'massive' ]
        ],
    ]
];
$config['Menu.salesmen'] = [
    'type' => 'link',
    'icon' => 'accounts ',
    'label' => 'Vendedores',
    'url' => [ 'controller' => 'salesmen', 'action' => 'dashboard' ]
];

$config['Menu.companies'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Empresas',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'companies', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'companies', 'action' => 'add' ]
        ],
        'massive' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Carga Masiva',
            'url' => [ 'controller' => 'companies', 'action' => 'massive' ]
        ],
    ]
];

$config['Menu.products'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Productos',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'products', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'products', 'action' => 'add' ]
        ],
        'search' => [
            'type' => 'link',
            'icon' => 'search',
            'label' => 'Busqueda',
            'url' => [ 'controller' => 'products', 'action' => 'search' ]
        ],
    ]
];

$config['Menu.divisions'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Divisiones',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'divisions', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'divisions', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.specs'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Caracteristicas',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'specs', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'specs', 'action' => 'add' ]
        ],
    ]
];
$config['Menu.prizes'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Premios',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'prizes', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'prizes', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.groups'] = [
    'type' => 'parent',
    'icon' => 'local-mall',
    'label' => 'Grupos',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'groups', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'groups', 'action' => 'add' ]
        ],
    ]
];

// $config['Menu.disabled_groups'] = [
//     'type' => 'parent',
//     'icon' => 'local-mall',
//     'label' => 'Grupos Restringidos',
//     'childs' => [
//         'dashboard' => [
//             'type' => 'link',
//             'icon' => 'view-dashboard',
//             'label' => 'Dashboard',
//             'url' => [ 'controller' => 'disabled_groups', 'action' => 'dashboard' ]
//         ],
//         'add' => [
//             'type' => 'link',
//             'icon' => 'plus-circle-o',
//             'label' => 'Crear',
//             'url' => [ 'controller' => 'disabled_groups', 'action' => 'add' ]
//         ],
//     ]
// ];

$config['Menu.sales'] = [
    'type' => 'parent',
    'icon' => 'file-plus',
    'label' => 'Ventas',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'sales', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Carga Manual',
            'url' => [ 'controller' => 'sales', 'action' => 'add' ]
        ],
        'massive' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Carga Masiva',
            'url' => [ 'controller' => 'sales', 'action' => 'massive' ]
        ],
    ]
];

$config['Menu.run_sales'] = [
    'type' => 'parent',
    'icon' => 'directions-walk',
    'label' => 'Competencias',
    'childs' => [
        'show' => [
            'type' => 'link',
            'icon' => 'view-list',
            'label' => 'Listado',
            'url' => [ 'controller' => 'run_sales', 'action' => 'show' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear Competencia',
            'url' => [ 'controller' => 'run_sales', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.achievements'] = [
    'type' => 'parent',
    'icon' => 'star-circle',
    'label' => 'Logros',
    'childs' => [
        'show' => [
            'type' => 'link',
            'icon' => 'view-list',
            'label' => 'Listado',
            'url' => [ 'controller' => 'achievements', 'action' => 'show' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear Logro',
            'url' => [ 'controller' => 'achievements', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.challenges'] = [
    'type' => 'parent',
    'icon' => 'star-circle',
    'label' => 'Desafios',
    'childs' => [
        'show' => [
            'type' => 'link',
            'icon' => 'view-list',
            'label' => 'Listado',
            'url' => [ 'controller' => 'challenges', 'action' => 'show' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear Desafio',
            'url' => [ 'controller' => 'challenges', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.roulettes'] = [
    'type' => 'parent',
    'icon' => 'directions-walk',
    'label' => 'Ruletas',
    'childs' => [
        'show' => [
            'type' => 'link',
            'icon' => 'view-list',
            'label' => 'Listado',
            'url' => [ 'controller' => 'roulettes', 'action' => 'show' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear Ruleta',
            'url' => [ 'controller' => 'roulettes', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.lotteries'] = [
    'type' => 'parent',
    'icon' => 'directions-walk',
    'label' => 'Loterias',
    'childs' => [
        'show' => [
            'type' => 'link',
            'icon' => 'view-list',
            'label' => 'Listado',
            'url' => [ 'controller' => 'lotteries', 'action' => 'show' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear Loteria',
            'url' => [ 'controller' => 'lotteries', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.redeems'] = [
    'type' => 'parent',
    'icon' => 'assignment-check',
    'label' => 'Canjes',
    'childs' => [
        'show_pending' => [
            'type' => 'link',
            'icon' => 'hourglass-outline',
            'label' => 'Pendientes',
            'url' => [ 'controller' => 'redeems', 'action' => 'show_pending' ]
        ],
        'show_approved' => [
            'type' => 'link',
            'icon' => 'check-all',
            'label' => 'Aprobados',
            'url' => [ 'controller' => 'redeems', 'action' => 'show_approved' ]
        ],
        'show_rejected' => [
            'type' => 'link',
            'icon' => 'minus-circle-outline',
            'label' => 'Rechazados',
            'url' => [ 'controller' => 'redeems', 'action' => 'show_rejected' ]
        ],
    ]
];

$config['Menu.complains'] = [
    'type' => 'parent',
    'icon' => 'alert-triangle',
    'label' => 'Consultas',
    'childs' => [
        'show_unread' => [
            'type' => 'link',
            'icon' => 'eye-off',
            'label' => 'No Leidas',
            'url' => [ 'controller' => 'complains', 'action' => 'show_unread' ]
        ],
        'show_read' => [
            'type' => 'link',
            'icon' => 'eye',
            'label' => 'Leidas',
            'url' => [ 'controller' => 'complains', 'action' => 'show_read' ]
        ],
    ]
];


//$config['Menu.complain_sales'] = [
//    'type' => 'parent',
//    'icon' => 'alert-triangle',
//    'label' => 'Reclamos de Ventas',
//    'childs' => [
//        'show_pending' => [
//            'type' => 'link',
//            'icon' => 'hourglass-outline',
//            'label' => 'Pendientes',
//            'url' => [ 'controller' => 'complain_sales', 'action' => 'show_pending' ]
//        ],
//        'show_approved' => [
//            'type' => 'link',
//            'icon' => 'check-all',
//            'label' => 'Aprobados',
//            'url' => [ 'controller' => 'complain_sales', 'action' => 'show_approved' ]
//        ],
//        'show_rejected' => [
//            'type' => 'link',
//            'icon' => 'minus-circle-outline',
//            'label' => 'Rechazados',
//            'url' => [ 'controller' => 'complain_sales', 'action' => 'show_rejected' ]
//        ],
//    ]
//];

$config['Menu.cities'] = [
    'type' => 'parent',
    'icon' => 'map',
    'label' => 'Ciudades',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'cities', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'cities', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.providers'] = [
    'type' => 'parent',
    'icon' => 'shopping-cart',
    'label' => 'Proveedores',
    'childs' => [
        'dashboard' => [
            'type' => 'link',
            'icon' => 'view-dashboard',
            'label' => 'Dashboard',
            'url' => [ 'controller' => 'providers', 'action' => 'dashboard' ]
        ],
        'add' => [
            'type' => 'link',
            'icon' => 'plus-circle-o',
            'label' => 'Crear',
            'url' => [ 'controller' => 'providers', 'action' => 'add' ]
        ],
    ]
];

$config['Menu.reports'] = [
    'type' => 'parent',
    'icon' => 'chart',
    'label' => 'Reportes',
    'childs' => [
        'summary' => [
            'type' => 'link',
            'icon' => 'sort-amount-desc',
            'label' => 'Acum. de Puntos',
            'url' => [ 'controller' => 'reports', 'action' => 'summary' ]
        ],
        'sales' => [
            'type' => 'link',
            'icon' => 'money',
            'label' => 'Ventas',
            'url' => [ 'controller' => 'reports', 'action' => 'sales_xlsx' ]
        ],
        'achievements' => [
            'type' => 'link',
            'icon' => 'star',
            'label' => 'Logros',
            'url' => [ 'controller' => 'reports', 'action' => 'achievements_xlsx' ]
        ],
        'challenges' => [
            'type' => 'link',
            'icon' => 'star',
            'label' => 'Desafios',
            'url' => [ 'controller' => 'reports', 'action' => 'challenges_xlsx' ]
        ],
        'companies_list' => [
            'type' => 'link',
            'icon' => 'download',
            'label' => 'Empresas',
            'url' => [ 'controller' => 'reports', 'action' => 'companies_xlsx' ]
        ],
        'users_list' => [
            'type' => 'link',
            'icon' => 'download',
            'label' => 'Usuarios',
            'url' => [ 'controller' => 'reports', 'action' => 'users_xlsx' ]
        ],
    ]
];