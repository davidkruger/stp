<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actions = [
    'detail.complains',
];

############################################################# LISTAS

$config['Klezkaffold.show.complains.show_read'] = [
    'data' => [
        'class' => 'FullComplain',
        'path' => 'Model',
        'query' => [
            'conditions' => [
                'FullComplain.status' => 'read'
            ],
            'order' => 'FullComplain.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.complains.show_unread'] = [
    'data' => [
        'class' => 'FullComplain',
        'path' => 'Model',
        'query' => [
            'conditions' => [
                'FullComplain.status' => 'unread'
            ],
            'order' => 'FullComplain.created DESC',
            'limit' => $limit
        ]
    ],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.complains'] = [
    'data' => [
        'class' => 'FullComplain',
        'path' => 'Model',
        'prequery' =>  [ 'flagRead' ],
        'query' => [
            
        ]
    ],
    'params' => [
        'slug' => [ 'slugify', 'subject' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.complains'] = [
    'data' => [
        'class' => 'Complain',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.complains'] = [
    'data' => [
        'class' => 'Complain',
        'path' => 'Model',
        'beforeSave' => [
            'injectUser'
        ]
    ],
];
