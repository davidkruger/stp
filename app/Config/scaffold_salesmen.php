<?php

$config = [];
$limit = 10;
$data = json_decode(file_get_contents("php://input"), true);

############################################################# DASHBOARD

$actions = [
    'detail.salesmen'
];

$config['Klezkaffold.dashboard.salesmen'] = [

];

if($data['module'] === 'salesmen'){
    App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
    App::uses('Company','Model');
    $Company = new Company();
    $companies = $Company->find('all', [
        'conditions' => [
            'Salesman.role' => 'vendedor',
            'Company.status' => true,
            'Salesman.status' => true,
        ],
        'joins' => [
            'INNER JOIN salesmen AS Salesman ON Salesman.company_id=Company.id'
        ],
        'group' => 'Company.id',
        'order' => 'count(*) desc'
    ]);

    foreach($companies as $company){
        $config['Klezkaffold.dashboard.salesmen']["company_{$company['id']}"] = [
            'title' => $company['name'],
            'type' => 'summary_pie',
            'text' => 'Cantidad de Vendedores',
            'url' => [
                'controller' => 'salesmen', 'action' => 'by_branches', 'company' => $company['id'], 'slug' => KlezkaffoldComponent::resolvParamSlug($company['name'])
            ],
            'data' => [
                'class' => 'Salesman',
                'path' => 'Model',
                'query' => [
                    'conditions' => [
                        'Salesman.role' => 'vendedor',
                        'Salesman.status' => true,
                        'Salesman.company_id' => $company['id']
                    ],
                    'order' => 'Salesman.created DESC',
                    'limit' => $limit
                ],
            ]
        ];
    }
}
else if($data['module'] === 'salesmen_by_branches'){
    App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
    App::uses('Branch','Model');
    $Branch = new Branch();
    $branches = $Branch->find('all', [
        'conditions' => [
            'Salesman.role' => 'vendedor',
            'Branch.status' => true,
            'Salesman.status' => true,
            'Branch.company_id' => $data['extras'],
        ],
        'joins' => [
            'INNER JOIN salesmen AS Salesman ON (Salesman.branch_id=Branch.id OR Salesman.branch_id2=Branch.id OR Salesman.branch_id3=Branch.id)'
        ],
        'group' => 'Branch.id',
        'order' => 'count(*) desc'
    ]);

    foreach($branches as $branch){
        $config['Klezkaffold.dashboard.salesmen_by_branches']["branch_{$branch['id']}"] = [
            'title' => $branch['name'],
            'type' => 'summary_pie',
            'text' => 'Cantidad de Vendedores',
            'url' => [
                'controller' => 'salesmen', 'action' => 'show_by_branches', 'branch' => $branch['id'], 'slug' => KlezkaffoldComponent::resolvParamSlug($branch['name'])
            ],
            'data' => [
                'class' => 'Salesman',
                'path' => 'Model',
                'query' => [
                    'conditions' => [
                        'Salesman.role' => 'vendedor',
                        'Salesman.status' => true,
                        'OR' => [
                            'Salesman.branch_id' => $branch['id'],
                            'Salesman.branch_id2' => $branch['id'],
                            'Salesman.branch_id3' => $branch['id']
                        ]
                    ],
                    'order' => 'Salesman.created DESC',
                    'limit' => $limit
                ],
            ]
        ];
    }
}

if(isset($data['payload']['branch'])){
    $config['Klezkaffold.show.salesmen_by_branches'] = [
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'sellers',
            'query' => [
                'conditions' => [
                    'User.role' => 'vendedor',
                    'User.status' => true,
                    'OR' => [
                        'User.branch_id' => $data['payload']['branch'],
                        'User.branch_id2' => $data['payload']['branch'],
                        'User.branch_id3' => $data['payload']['branch']
                    ]
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ],
        'actions' => $actions
    ];
}

############################################################# DETALLE

$config['Klezkaffold.detail.salesmen'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'readable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'document' ],
        'id' => 'id',
    ],
    'actions' => $actions
];