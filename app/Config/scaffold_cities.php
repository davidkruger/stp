<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

    $config['Klezkaffold.dashboard.cities'] = [
    'show' => [
        'title' => 'Ciudades Activas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ciudades',
        'url' => [
            'controller' => 'cities', 'action' => 'show'
        ],
        'data' => [
            'class' => 'City',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'City.status' => true
                ],
                'order' => 'City.id DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Ciudades Suspendidas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ciudades',
        'url' => [
            'controller' => 'cities', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'City',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'City.status' => false
                ],
                'order' => 'City.id DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.cities',
    'edit.cities',
];

############################################################# LISTAS

$config['Klezkaffold.show.cities.show'] = [
    'data' => $config['Klezkaffold.dashboard.cities']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.cities.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.cities']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.cities'] = [
    'data' => [
        'class' => 'City',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.cities'] = [
    'data' => [
        'class' => 'City',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.cities'] = [
    'data' => [
        'class' => 'City',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.cities'] = [
    'data' => [
        'class' => 'City',
        'path' => 'Model',
    ],
];
