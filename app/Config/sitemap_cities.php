<?php

$config = [];

#################################################### CITIES

$config['Sitemap.cities.dashboard'] = [
    'h1' => 'Módulo de Ciudades',
    'empty' => [
        'title' => 'No hay Resúmenes de Ciudades',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.cities.show'] = [
    'h1' => 'Ciudades Activas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ciudades',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ciudades',
        'text' => 'No se han encontrado Ciudades en esta página'
    ],
    'search' => [
        'label' => 'Buscar Ciudades',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ciudades',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.cities.show_disabled'] = [
    'h1' => 'Ciudades Suspendidas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ciudades',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ciudades',
        'text' => 'No se han encontrado Ciudades en esta página'
    ],
    'search' => [
        'label' => 'Buscar Ciudades',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ciudades',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.cities.detail'] = [
    'h1' => 'Detalle de la Ciudad',
    'icon' => 'info-outline'
];

$config['Sitemap.cities.edit'] = [
    'h1' => 'Editar Ciudad',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Ciudad',
        'icon' => 'send'
    ]
];

$config['Sitemap.cities.add'] = [
    'h1' => 'Crear Ciudad',
    'submit' => [
        'label' => 'Crear Ciudad',
        'icon' => 'send'
    ]
];