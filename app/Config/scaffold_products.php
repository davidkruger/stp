<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

    $config['Klezkaffold.dashboard.products'] = [
    'show' => [
        'title' => 'Productos Activos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Productos',
        'url' => [
            'controller' => 'products', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Product',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Product.status' => true
                ],
                'order' => 'Product.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Productos Suspendidos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Productos',
        'url' => [
            'controller' => 'products', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Product',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Product.status' => false
                ],
                'order' => 'Product.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.products',
    'edit.products',
    'product_promotions.products',
    'product_specs.products',
    'product_catalogs.products',
    'product_points.products'
];

############################################################# LISTAS

$config['Klezkaffold.show.products.show'] = [
    'data' => $config['Klezkaffold.dashboard.products']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.products.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.products']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.products'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.products'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.products'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.products'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
    ],
];

############################################################# FOTO

$config['Klezkaffold.image.picture.products'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
        'field' => 'picture',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'foto'
    ],
    'params' => [
        'id' => 'id',
    ],
];

############################################################# FORMULARIO BUSQ

$config['Klezkaffold.request_form.products_search'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
        'schema' => 'search'
    ],
];

$config['Klezkaffold.add.products_search'] = [
    'data' => [
        'class' => 'Product',
        'path' => 'Model',
        'schema' => 'search'
    ],
];
