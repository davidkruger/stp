<?php

$config = [];

#################################################### RECLAMOS DE VENTAS


$config['Sitemap.complain_sales.show_pending'] = [
    'h1' => 'Reclamos de Ventas Pendientes de Moderacion',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Reclamos de Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Reclamos de Ventas',
        'text' => 'No se han encontrado Reclamos de Ventas en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Empresa',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'reclamos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.complain_sales.show_rejected'] = [
    'h1' => 'Reclamos de Ventas Rechazados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Reclamos de Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Reclamos de Ventas',
        'text' => 'No se han encontrado Reclamos de Ventas en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Empresa',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'reclamos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.complain_sales.show_approved'] = [
    'h1' => 'Reclamos de Ventas Aprobados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Reclamos de Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Reclamos de Ventas',
        'text' => 'No se han encontrado Reclamos de Ventas en esta página'
    ],
    'search' => [
        'label' => 'Buscar por Empresa',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'reclamos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.complain_sales.detail'] = [
    'h1' => 'Detalle del Reclamo de Venta',
    'icon' => 'info-outline'
];
$config['Sitemap.complain_sale_moderations.detail'] = [
    'h1' => 'Detalle de la Moderacion',
    'icon' => 'eye'
];

$config['Sitemap.complain_sales.edit'] = [
    'h1' => 'Moderar Reclamo de Venta',
    'icon' => 'badge-check',
    'submit' => [
        'label' => 'Procesar Moderacion',
        'icon' => 'send'
    ]
];
