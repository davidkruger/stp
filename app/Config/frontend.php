<?php

$config = [];

########################################### MENU ANON (NO HAY USER LOGUEADO)

$config['Frontend.anon_menu'] = [];

$config['Frontend.anon_menu'][] = [
    'class' => '',
    'label' => 'Inicio',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'home',
        'plugin' => null
    ])
];

$config['Frontend.anon_menu'][] = [
    'class' => '',
    'label' => 'Bases y Condiciones',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'byc',
        'plugin' => null
    ])
];

$config['Frontend.anon_menu'][] = [
    'class' => 'btn_empresas',
    'label' => 'Area de Empresas',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'login',
        'plugin' => null
    ])
];

########################################### MENU VENDEDOR

$config['Frontend.vendedor_menu'] = [];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Inicio',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'home',
        'plugin' => null
    ])
];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Ventas',
    'dropdown' => [
//        [
//            'label' => 'Cargar Venta',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'add_sale',
//                'plugin' => null
//            ])
//        ],
        [
            'label' => 'Mis Ventas',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'sales',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Ventas Rechazadas',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'rejected',
                'plugin' => null
            ])
        ],
    ]
];


//$config['Frontend.vendedor_menu'][] = [
//    'class' => '',
//    'label' => 'Sorteos',
//    'dropdown' => [
//        [
//            'label' => 'Loteria',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'lottery',
//                'plugin' => null
//            ])
//        ],
//        [
//            'label' => 'Ruleta',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'roulette',
//                'plugin' => null
//            ])
//        ],
//    ]
//];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Premios Solicitados',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'prizes',
        'plugin' => null
    ])
];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Catálogo',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'catalog',
        'plugin' => null
    ])
];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Bases y Condiciones',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'byc',
        'plugin' => null
    ])
];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Consultas',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'complains',
        'plugin' => null
    ])
];

$config['Frontend.vendedor_menu'][] = [
    'class' => '',
    'label' => 'Mi Cuenta',
    'dropdown' => [
        [
            'label' => 'Editar Datos',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'edit_profile',
                'plugin' => null
            ])
        ],
        [
            'class' => '',
            'label' => 'Cerrar Sesión',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'logout',
                'plugin' => null
            ])
        ],
    ]
];

########################################### MENU RESPONSABLE

$config['Frontend.responsable_menu'] = [];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Inicio',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'home',
        'plugin' => null
    ])
];

//$config['Frontend.responsable_menu'][] = [
//    'class' => '',
//    'label' => 'Premios Solicitados',
//    'url' => Router::url([ 
//        'controller' => 'frontend',
//        'action' => 'prizes',
//        'plugin' => null
//    ])
//];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Vendedores',
    'dropdown' => [
        [
            'label' => 'Ver todos',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'sellers',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Agregar ',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'add_seller',
                'plugin' => null
            ])
        ],
//        [
//            'label' => 'Asignar Puntos',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'assign_points',
//                'plugin' => null
//            ])
//        ],
    ]
];

//$config['Frontend.responsable_menu'][] = [
//    'class' => '',
//    'label' => 'Sorteos',
//    'dropdown' => [
//        [
//            'label' => 'Loteria',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'lottery',
//                'plugin' => null
//            ])
//        ],
//        [
//            'label' => 'Ruleta',
//            'class' => '',
//            'url' => Router::url([ 
//                'controller' => 'frontend',
//                'action' => 'roulette',
//                'plugin' => null
//            ])
//        ],
//    ]
//];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Ventas',
    'dropdown' => [
        [
            'label' => 'Cargar Venta',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'add_sale',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Cargar Venta Masiva',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'add_massive_sale',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Ventas Pendientes',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'pending_sales',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Ver Ventas',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'sales',
                'plugin' => null
            ])
        ],
        [
            'label' => 'Ventas Rechazadas',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'rejected',
                'plugin' => null
            ])
        ],
    ]
];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Premios Solicitados',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'prizes',
        'plugin' => null
    ])
];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Catálogo',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'catalog',
        'plugin' => null
    ])
];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Bases y Condiciones',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'byc',
        'plugin' => null
    ])
];

$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Consultas',
    'url' => Router::url([ 
        'controller' => 'frontend',
        'action' => 'complains',
        'plugin' => null
    ])
];


$config['Frontend.responsable_menu'][] = [
    'class' => '',
    'label' => 'Mi Cuenta',
    'dropdown' => [
        [
            'label' => 'Editar Datos',
            'class' => '',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'edit_profile',
                'plugin' => null
            ])
        ],
        [
            'class' => '',
            'label' => 'Cerrar Sesión',
            'url' => Router::url([ 
                'controller' => 'frontend',
                'action' => 'logout',
                'plugin' => null
            ])
        ],
    ]
];