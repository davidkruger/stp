<?php

$config = [];

#################################################### LOTTERIES

$config['Sitemap.lotteries.dashboard'] = [
    'h1' => 'Módulo de Loterias',
];

$config['Sitemap.lotteries.show'] = [
    'h1' => 'Listado de Loterias',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Loterias',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Loterias',
        'text' => 'No se han encontrado Loterias en esta página'
    ],
    'search' => [
        'label' => 'Filtrar Loteria',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'loterias',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.lotteries.detail'] = [
    'h1' => 'Detalle de la Loteria',
    'icon' => 'info-outline'
];

$config['Sitemap.lotteries.edit'] = [
    'h1' => 'Editar Loteria',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Loteria',
        'icon' => 'send'
    ]
];

$config['Sitemap.lotteries.add'] = [
    'h1' => 'Crear Nueva Loteria',
    'submit' => [
        'label' => 'Crear Loteria',
        'icon' => 'send'
    ]
];

$config['Sitemap.lotteries.prize_photo'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];

$config['Sitemap.lotteries.xlsx'] = [
    'h1' => 'Reporte',
    'icon' => 'download'
];

$config['Sitemap.lotteries.draw'] = [
    'h1' => 'Sorteo de la Loteria',
    'icon' => 'star-half'
];

$config['Sitemap.lotteries.tickets'] = [
    
];