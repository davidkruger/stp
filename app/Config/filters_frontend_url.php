<?php

################################# FILTERS

$filters = [];

$filters['frontend'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','vendedor','responsable','encargado' ]
    ]
];

$filters['frontend_responsable_only'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','encargado','responsable' ]
    ]
];

$filters['frontend_vendedor_only'] = [
    'data' => [
        'status' => true,
        'role' => 'vendedor'
    ]
];

################################# CONFIG

$config = [];

################################# FRONTEND

$config['Filter.frontend.prizes'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.complains'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.sales'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.sellers'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.add_seller'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.assign_points'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.add_sale'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.pending_sales'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.approve'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.add_massive_sale'] = [
    'filters' => $filters['frontend_responsable_only'],
];

$config['Filter.frontend.rejected'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.edit_profile'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.catalog'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.lottery'] = [
    'filters' => $filters['frontend'],
];

$config['Filter.frontend.roulette'] = [
    'filters' => $filters['frontend'],
];