<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

    $config['Klezkaffold.dashboard.campaigns'] = [
    'show' => [
        'title' => 'Campañas Activas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Campañas',
        'url' => [
            'controller' => 'campaigns', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Campaign',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Campaign.status' => true
                ],
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Campañas Suspendidas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Campañas',
        'url' => [
            'controller' => 'campaigns', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Campaign',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Campaign.status' => false
                ],
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.campaigns',
    'edit.campaigns',
];

############################################################# LISTAS

$config['Klezkaffold.show.campaigns.show'] = [
    'data' => $config['Klezkaffold.dashboard.campaigns']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.campaigns.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.campaigns']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.campaigns'] = [
    'data' => [
        'class' => 'Campaign',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.campaigns'] = [
    'data' => [
        'class' => 'Campaign',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.campaigns'] = [
    'data' => [
        'class' => 'Campaign',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.campaigns'] = [
    'data' => [
        'class' => 'Campaign',
        'path' => 'Model',
    ],
];