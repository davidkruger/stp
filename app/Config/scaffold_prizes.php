<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

    $config['Klezkaffold.dashboard.prizes'] = [
    'show' => [
        'title' => 'Premios Activos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Premios',
        'url' => [
            'controller' => 'prizes', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Prize',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Prize.status' => true
                ],
                'order' => 'Prize.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Premios Suspendidos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Premios',
        'url' => [
            'controller' => 'prizes', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Prize',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Prize.status' => false
                ],
                'order' => 'Prize.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.prizes',
    'edit.prizes',
];

############################################################# LISTAS

$config['Klezkaffold.show.prizes.show'] = [
    'data' => $config['Klezkaffold.dashboard.prizes']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.prizes.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.prizes']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
    ],
];

############################################################# FOTO

$config['Klezkaffold.image.photo.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
        'field' => 'photo',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'foto'
    ],
    'params' => [
        'id' => 'id',
    ],
];

############################################################# FILE

$config['Klezkaffold.file.pdf.prizes'] = [
    'data' => [
        'class' => 'Prize',
        'path' => 'Model',
        'field' => 'pdf',
        'schema' => 'readable',
        'format' => 'application/pdf',
        'file' => 'condiciones_de_canje.pdf'
    ],
    'params' => [
        'id' => 'id',
    ],
];
