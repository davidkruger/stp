<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actions = [
    'detail.run_sales',
    'edit.run_sales',
    'draw.run_sales',
    'xlsx.run_sales'
];

############################################################# LISTAS

$config['Klezkaffold.show.run_sales.show'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'conditions' => [
                
            ],
            'joins' => [
                
            ],
            'order' => 'RunSale.starts_at DESC',
            'limit' => $limit
        ],
        'prequery' => [
            
        ]
    ],
    'actions' => $actions
];

############################################################# DRAW

$config['Klezkaffold.draw.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'description' ],
        'id' => 'id',
    ],
    'actions' => $actions
];


############################################################# DETALLE

$config['Klezkaffold.detail.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'slug' => [ 'slugify', 'description' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# CREAR

$config['Klezkaffold.add.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
    ],
];

$config['Klezkaffold.request_form.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
    ],
];

############################################################# EDITAR

$config['Klezkaffold.edit.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'schema' => 'writable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'description' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FOTO PREMIO

$config['Klezkaffold.image.prize_img.run_sales'] = [
    'data' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'field' => 'prize_img',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'imagen'
    ],
    'params' => [
        'id' => 'id',
    ],
];

############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL

$config['Klezkaffold.xlsx.run_sales'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'RunSale',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'download' => [
        'file' => "reporte.xlsx",
    ],
    'params' => [
        'slug' => [ 'slugify', 'description' ],
        'id' => 'id',
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ventas',
                'data' => [
                    'class' => 'Sale',
                    'path' => 'Model',
                    'schema' => 'xlsxRunSale',
                    'query' => [
                        'order' => 'Sale.sale_date DESC',
                        'joins' => [
                            'INNER JOIN run_sale_logs AS RunSaleLog ON RunSaleLog.sale_id=Sale.id'
                        ]
                    ],
                    'prequery' => [ 'injectRunSale' ],
                ],
            ],
            [
                'name' => 'Por Producto',
                'data' => [
                    'class' => 'RunSaleTarget',
                    'path' => 'Model',
                    'schema' => 'xlsxRunSale',
                    'query' => [
                        'joins' => [
                            'INNER JOIN run_sale_logs AS RunSaleLog ON RunSaleLog.run_sale_id=RunSaleTarget.run_sale_id',
                            'LEFT JOIN sales AS Sale ON Sale.id=RunSaleLog.sale_id AND Sale.product_id = RunSaleTarget.product_id',
                        ],
                        'group' => [
                            'RunSaleTarget.product_id'
                        ]
                    ],
                    'prequery' => [ 'injectRunSale' ],
                ],
            ],
            [
                'name' => 'Por Empresa',
                'data' => [
                    'class' => 'RunSaleCompany',
                    'path' => 'Model',
                    'schema' => 'xlsxRunSale',
                    'query' => [
                        'joins' => [
                            'INNER JOIN run_sale_logs AS RunSaleLog ON RunSaleLog.run_sale_id=RunSaleCompany.run_sale_id',
                            'LEFT JOIN sales AS Sale ON Sale.id=RunSaleLog.sale_id AND Sale.company_id = RunSaleCompany.company_id',
                        ],
                        'group' => [
                            'RunSaleCompany.company_id'
                        ]
                    ],
                    'prequery' => [ 'injectRunSale' ],
                ],
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx.run_sales'] = $config['Klezkaffold.xlsx.run_sales'];