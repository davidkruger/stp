<?php

$config = [];

#################################################### USERS

$config['Sitemap.users.dashboard'] = [
    'h1' => 'Módulo de Usuarios',
    'empty' => [
        'title' => 'No hay Resúmenes de Usuarios',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

//$config['Sitemap.users.show_romis'] = [
//    'h1' => 'Usuarios con Perfil Romis',
//    'pagination' => [
//        'summary' => 'Mostrando %d de %d Usuarios',
//        'next' => 'Siguiente',
//        'back' => 'Anterior',
//        'spread' => 3
//    ],
//    'empty' => [
//        'title' => 'No hay Usuarios',
//        'text' => 'No se han encontrado Usuarios en esta página'
//    ],
//    'search' => [
//        'label' => 'Buscar Usuarios'
//    ]
//];

$config['Sitemap.users.show_lamoderna'] = [
    'h1' => 'Usuarios con Perfil LaModerna',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.show_samsung'] = [
    'h1' => 'Usuarios con Perfil Samsung',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.show_responsibles'] = [
    'h1' => 'Usuarios con Perfil Responsables',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.show_sellers'] = [
    'h1' => 'Usuarios con Perfil Vendedores',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.show_disabled'] = [
    'h1' => 'Usuarios con Cuenta Suspendida',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.detail'] = [
    'h1' => 'Detalle del Usuario',
    'icon' => 'info-outline'
];

$config['Sitemap.users.edit'] = [
    'h1' => 'Editar Usuario',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Usuario',
        'icon' => 'send'
    ]
];

$config['Sitemap.users.add'] = [
    'h1' => 'Crear Nuevo Usuario',
    'submit' => [
        'label' => 'Crear Usuario',
        'icon' => 'send'
    ]
];

$config['Sitemap.users.picture'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];

$config['Sitemap.users.show_encargados'] = [
    'h1' => 'Usuarios con Perfil Encargado',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Usuarios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Usuarios',
        'text' => 'No se han encontrado Usuarios en esta página'
    ],
    'search' => [
        'label' => 'Buscar Usuarios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'usuarios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.users.massive'] = [
    'h1' => 'Carga Masiva de Vendedores',
    'submit' => [
        'label' => 'Cargar Vendedores',
        'icon' => 'send'
    ],
    'confirm' => [
        'label' => 'Confirmar Vendedores',
        'icon' => 'send',
        'title' => 'Confirmar datos extraidos del CSV'
    ],
];
