<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.specs'] = [
    'show' => [
        'title' => 'Caracteristicas Activas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Caracteristicas',
        'url' => [
            'controller' => 'specs', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Spec',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Spec.status' => true
                ],
                'order' => 'Spec.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Caracteristicas Suspendidas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Caracteristicas',
        'url' => [
            'controller' => 'specs', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Spec',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Spec.status' => false
                ],
                'order' => 'Spec.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.specs',
    'edit.specs',
];

############################################################# LISTAS

$config['Klezkaffold.show.specs.show'] = [
    'data' => $config['Klezkaffold.dashboard.specs']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.specs.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.specs']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.specs'] = [
    'data' => [
        'class' => 'Spec',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.specs'] = [
    'data' => [
        'class' => 'Spec',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.specs'] = [
    'data' => [
        'class' => 'Spec',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.specs'] = [
    'data' => [
        'class' => 'Spec',
        'path' => 'Model',
    ],
];