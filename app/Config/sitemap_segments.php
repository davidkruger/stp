<?php

$config = [];

#################################################### SEGMENTOS


$config['Sitemap.segments.add'] = [
    'h1' => 'Crear Segmentos',
    'submit' => [
        'label' => 'Crear Segmento',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.segments.delete'] = [
    'h1' => 'Eliminar Segmentos',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Segmentos',
        'icon' => 'trash'
    ]
];

$config['Sitemap.segments.edit'] = [
    'h1' => 'Editar Segmentos',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Segmentos',
        'icon' => 'send'
    ]
];
