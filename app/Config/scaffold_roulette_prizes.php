<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.roulette_prizes',
    'edit.roulette_prizes',
];

############################################################# PRIZES x ROULETTE 

$config['Klezkaffold.roulette_prizes.roulettes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
        ],
        'prequery' => [ 'rouletteOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'roulette' => [ 'payload', 'roulette' ],
    ],
    'links' => [
        [ 'controller' => 'roulette_prizes', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.roulettes.roulette_prizes'] = $config['Klezkaffold.roulette_prizes.roulettes'];

############################################################# EDITAR

$config['Klezkaffold.edit.roulette_prizes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'schema' => 'rouletteless',
        'prequery' => [ 'rouletteOwnerPrequery' ],
        'beforeCommit' => [ 'updateRouletteTimestamp' ]
    ],
    'params' => [
        'id' => 'id',
        'roulette' => 'roulette',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.roulette_prizes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'beforeCommit' => [ 'updateRouletteTimestamp' ]
    ],
    'params' => [
        'id' => 'id',
        'roulette' => 'roulette',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.roulette_prizes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'schema' => 'rouletteless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.roulette_prizes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'schema' => 'rouletteless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.roulette_prizes'] = [
    'data' => [
        'class' => 'RoulettePrize',
        'path' => 'Model',
        'formSchema' => 'rouletteless',
        'prequery' => [ 'rouletteOwnerPrequery' ],
        'beforeCommit' => [ 'updateRouletteTimestamp' ]
    ],
];