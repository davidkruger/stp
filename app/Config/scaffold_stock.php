<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.stock',
    'edit.stock',
];

############################################################# SUCURSAL x EMPRESA 

$config['Klezkaffold.stock.companies'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'Stock.id DESC'
        ],
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'company' => [ 'payload', 'company' ],
    ],
    'links' => [
        [ 'controller' => 'stock', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.companies.stock'] = $config['Klezkaffold.stock.companies'];

############################################################# EDITAR

$config['Klezkaffold.edit.stock'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.stock'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.stock'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.stock'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.stock'] = [
    'data' => [
        'class' => 'Stock',
        'path' => 'Model',
        'formSchema' => 'companyless',
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
];
