<?php

$config = [];

#################################################### SALESMEN

$config['Sitemap.salesmen.dashboard'] = [
    'h1' => 'Vendedores por Empresa',
    'empty' => [
        'title' => 'No hay Resúmenes de Vendedores',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

#################################################### BY BRANCHES

$config['Sitemap.salesmen.by_branches'] = [
    'h1' => 'Vendedores por Sucursal',
    'empty' => [
        'title' => 'No hay Resúmenes de Vendedores',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

#################################################### LIST

$config['Sitemap.salesmen.show_by_branches'] = [
    'h1' => 'Vendedores',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Vendedores',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Vendedores',
        'text' => 'No se han encontrado Vendedores en esta página'
    ],
    'search' => [
        'label' => 'Buscar Vendedores',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'vendedores',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

#################################################### DETALLE

$config['Sitemap.salesmen.detail'] = [
    'h1' => 'Detalle del Vendedor',
    'icon' => 'info-outline'
];