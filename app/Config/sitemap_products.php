<?php

$config = [];

#################################################### PRODUCTS

$config['Sitemap.products.dashboard'] = [
    'h1' => 'Módulo de Productos',
    'empty' => [
        'title' => 'No hay Resúmenes de Productos',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.products.show'] = [
    'h1' => 'Productos Activos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Productos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Productos',
        'text' => 'No se han encontrado Productos en esta página'
    ],
    'search' => [
        'label' => 'Buscar Productos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'productos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.products.show_disabled'] = [
    'h1' => 'Productos Suspendidos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Productos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Productos',
        'text' => 'No se han encontrado Productos en esta página'
    ],
    'search' => [
        'label' => 'Buscar Productos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'productos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.products.detail'] = [
    'h1' => 'Detalle del Producto',
    'icon' => 'info-outline'
];

$config['Sitemap.products.edit'] = [
    'h1' => 'Editar Producto',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Producto',
        'icon' => 'send'
    ]
];

$config['Sitemap.products.add'] = [
    'h1' => 'Crear Nuevo Producto',
    'submit' => [
        'label' => 'Crear Producto',
        'icon' => 'send'
    ]
];

$config['Sitemap.products.product_promotions'] = [
    'h1' => 'Lista de Puntajes Promocionales',
    'icon' => 'star',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Promociones',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Promociones',
        'text' => 'No se han encontrado Promociones para este Producto'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'puntajes promocionales',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.products.picture'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];

$config['Sitemap.products.product_specs'] = [
    'h1' => 'Lista de Caracteristicas',
    'icon' => 'attachment-alt',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Caracteristicas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Caracteristicas',
        'text' => 'No se han encontrado Caracteristicas para este Producto'
    ],
    'search' => [
        'label' => 'Buscar Caracteristicas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'caracteristicas',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.products.product_catalogs'] = [
    'h1' => 'Lista de Catalogos',
    'icon' => 'assignment-o',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Catalogos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Catalogos',
        'text' => 'No se han encontrado Catalogos para este Producto'
    ],
    'search' => [
        'label' => 'Buscar Catalogos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'catalogos',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.products.search'] = [
    'h1' => 'Buscar Productos',
    'icon' => 'search',
    'submit' => [
        'label' => 'Buscar Productos',
        'icon' => 'search'
    ]
];

$config['Sitemap.products.product_points'] = [
    'h1' => 'Lista de Puntos para Empresas',
    'icon' => 'eyedropper',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Puntos para Empresas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Puntos para Empresas',
        'text' => 'No se han encontrado Puntos para Empresas en este Producto'
    ],
    'search' => [
        'label' => 'Buscar Puntos para Empresas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'empresas',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];