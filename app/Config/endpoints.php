<?php

################## ACL

$acl = [];
$acl['session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'session'
];

$acl['no_session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'no_session'
];

$acl['none'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'none'
];

################### Endpoints

$config = [];

$config['Endpoint.FrontendLogin'] = [
    'component' => 'PagesComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'login',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['no_session']
];

$config['Endpoint.FrontendLoginConfig'] = [
    'component' => 'ConfigComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'fetchLogin',
    'input' => 'blank',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['no_session']
];

$config['Endpoint.FrontendLogout'] = [
    'component' => 'PagesComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'logout',
    'input' => 'blank',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendDashboard'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'dashboard',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['none']
];

$config['Endpoint.FrontendPrizes'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'prizes',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendBranches'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'branches',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRedeem'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'redeem',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendImage'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'image',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRedeems'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'redeems',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRequestForm'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'requestForm',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendEdit'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'edit',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendAdd'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'add',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendShow'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'show',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendMassive'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'massive',
    'input' => 'json_payload',
    'verb' => [ 'post','put' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRequestMassive'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'requestMassive',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendDetail'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'detail',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendSalesmen'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'salesmen',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendProducts'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'products',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];


$config['Endpoint.FrontendSeller'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'seller',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendSellers'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'sellers',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendDeleteSeller'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'deleteSeller',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendCompany'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'company',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendAssignPoints'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'assignPoints',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRunSales'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'runSales',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendPendings'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'pendings',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendCatalogs'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'catalogs',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendLotteries'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'lotteries',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRoulettes'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'roulettes',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendAchievements'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'achievements',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendRanking'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'ranking',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

$config['Endpoint.FrontendChallenges'] = [
    'component' => 'FrontendEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'challenges',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

######################## draw

$config['Endpoint.Draw'] = [
    'component' => 'DrawEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'post', 'get'],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.DrawLottery'] = [
    'component' => 'DrawLotteryEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'post', 'get'],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## print

$config['Endpoint.PrintLottery'] = [
    'component' => 'PrintLotteryEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get'],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## stock

$config['Endpoint.StockCheck'] = [
    'component' => 'StockEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'check',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## aprobar samsung

$config['Endpoint.SamsungApproval'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'samsung',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.SamsungMassiveApproval'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'samsungMassive',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## aprobar responsable

$config['Endpoint.ResponsableApproval'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'responsable',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

######################## rechazar samsung

$config['Endpoint.SamsungReject'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'samsungReject',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.SamsungMassiveReject'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'samsungMassiveReject',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## rechazar responsable

$config['Endpoint.ResponsableReject'] = [
    'component' => 'ApprovalEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'responsableReject',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'User',
    'acl' => $acl['session']
];

######################## reportes

$config['Endpoint.ReportsSummary'] = [
    'component' => 'ReportsEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'summary',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## proceso masivo canjes

$config['Endpoint.BackofficeRedeemsMassiveProcess'] = [
    'component' => 'BackofficeEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'redeemsMassiveProcess',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];
######################## feed custom filters

$config['Endpoint.BackofficeCustomFiltersFeed'] = [
    'component' => 'BackofficeEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'customFiltersFeed',
    'input' => 'json_payload',
    'verb' => [  'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## product search

$config['Endpoint.BackofficeProductSearch'] = [
    'component' => 'BackofficeEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'productSearch',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## reportes

$config['Endpoint.ReportsCompanies'] = [
    'component' => 'ReportsEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'companies',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## reportes

$config['Endpoint.ReportsTop10'] = [
    'component' => 'ReportsEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'top10',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

######################## reportes

$config['Endpoint.ReportsProducts'] = [
    'component' => 'ReportsEndpointComponent',
    'path' => 'Controller/Endpoint',
    'method' => 'products',
    'input' => 'json_payload',
    'verb' => [  'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];