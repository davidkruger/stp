<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.groups'] = [
    'show' => [
        'title' => 'Grupos Activos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Grupos',
        'url' => [
            'controller' => 'groups', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Group',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Group.status' => true
                ],
                'order' => 'Group.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Grupos Suspendidos',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Grupos',
        'url' => [
            'controller' => 'groups', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Group',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Group.status' => false
                ],
                'order' => 'Group.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.groups',
    'edit.groups',
];

############################################################# LISTAS

$config['Klezkaffold.show.groups.show'] = [
    'data' => $config['Klezkaffold.dashboard.groups']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.groups.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.groups']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.groups'] = [
    'data' => [
        'class' => 'Group',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.groups'] = [
    'data' => [
        'class' => 'Group',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.groups'] = [
    'data' => [
        'class' => 'Group',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.groups'] = [
    'data' => [
        'class' => 'Group',
        'path' => 'Model',
    ],
];