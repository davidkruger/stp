<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.users'] = [
    'show_samsung' => [
        'title' => 'Perfil Samsung',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_samsung'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'samsung',
            'query' => [
                'conditions' => [
                    'User.role' => 'samsung',
                    'User.status' => true
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_lamoderna' => [
        'title' => 'Perfil LaModerna',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_lamoderna'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'lamoderna',
            'query' => [
                'conditions' => [
                    'User.role' => 'lamoderna',
                    'User.status' => true
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_responsibles' => [
        'title' => 'Perfil Responsables',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_responsibles'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'responsibles',
            'query' => [
                'conditions' => [
                    'User.role' => 'responsable',
                    'User.status' => true
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_sellers' => [
        'title' => 'Perfil Vendedores',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_sellers'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'sellers',
            'query' => [
                'conditions' => [
                    'User.role' => 'vendedor',
                    'User.status' => true
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_encargados' => [
        'title' => 'Perfil Encargados',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_encargados'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'responsibles',
            'query' => [
                'conditions' => [
                    'User.role' => 'encargado',
                    'User.status' => true
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Usuarios con Cuenta Suspendida',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Usuarios',
        'url' => [
            'controller' => 'users', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'User',
            'path' => 'Model',
            'schema' => 'disabled',
            'query' => [
                'conditions' => [
                    'User.status' => false
                ],
                'order' => 'User.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.users',
    'edit.users',
];

############################################################# LISTAS

//$config['Klezkaffold.show.users.show_romis'] = [
//    'data' => $config['Klezkaffold.dashboard.users']['show_romis']['data'],
//    'actions' => $actions
//];

$config['Klezkaffold.show.users.show_lamoderna'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_lamoderna']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.users.show_samsung'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_samsung']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.users.show_responsibles'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_responsibles']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.users.show_sellers'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_sellers']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.users.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_disabled']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.users.show_encargados'] = [
    'data' => $config['Klezkaffold.dashboard.users']['show_encargados']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'readable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'document' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'writable',
    ],
    'params' => [
        'slug' => [ 'slugify', 'document' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.users'] = [
    'data' => [
        'schema' => 'writable',
        'class' => 'User',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'writable',
    ],
];

############################################################# FOTO

$config['Klezkaffold.image.picture.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'field' => 'picture',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'foto'
    ],
    'params' => [
        'id' => 'id',
    ],
];

############################################################# CARGA MASIVA

$config['Klezkaffold.massive.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'massive',
        'massive' => [
            'source' => 'document',
            'mode' => 'singles',
            'class' => 'User',
            'path' => 'Model',
            'cache' => 'massive',
            'beforeSave' => [
                'massiveDefaults'
            ],
            'map' => [
//                'document_type' => [
//                    'type' => 'text',
//                    'label' => 'Tipo de Documento'
//                ],
                'document' => [
                    'type' => 'text',
                    'label' => 'Documento',
                    'ensureUnique' => true
                ],
                'password' => [
                    'type' => 'text',
                    'label' => 'Password',
                    'hidden' => true
                ],
                'first_name' => [
                    'type' => 'text',
                    'label' => 'Nombre',
                ],
                'last_name' => [
                    'type' => 'text',
                    'label' => 'Apellido',
                ],
                'email' => [
                    'type' => 'text',
                    'label' => 'E-mail',
                    'ensureUnique' => true,
                    'nullable' => true
                ],
                'telephone' => [
                    'type' => 'text',
                    'label' => 'Telefono',
                ],
                'birthday' => [
                    'type' => 'date',
                    'label' => 'Fecha Nac.',
                ],
                'company_id' => [
                    'type' => 'foreign',
                    'label' => 'Empresa',
                    'resolver' => 'ruc'
                ],
                'branch_id' => [
                    'type' => 'foreign',
                    'label' => 'Sucursal',
                    'resolver' => 'id'
                ],
                'role' => [
                    'hidden' => true,
                    'type' => 'text',
                    'calc' => true,
                    'method' => 'massiveRole',
                    'label' => 'Rol',
                ],
            ]
        ]
    ],
];

$config['Klezkaffold.request_massive.users'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'massive',
    ],
];
