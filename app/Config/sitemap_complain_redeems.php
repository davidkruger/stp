<?php

$config = [];

#################################################### CANJES


$config['Sitemap.redeems.show_pending'] = [
    'h1' => 'Canjes Pendientes de Moderacion',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ]
];

$config['Sitemap.redeems.show_rejected'] = [
    'h1' => 'Canjes Rechazados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ]
];

$config['Sitemap.redeems.show_approved'] = [
    'h1' => 'Canjes Aprobados',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Canjes',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Canjes',
        'text' => 'No se han encontrado Canjes en esta página'
    ]
];

$config['Sitemap.redeems.detail'] = [
    'h1' => 'Detalle del Canje',
    'icon' => 'info-outline'
];
$config['Sitemap.redeem_moderations.detail'] = [
    'h1' => 'Detalle de la Moderacion',
    'icon' => 'eye'
];

$config['Sitemap.redeems.edit'] = [
    'h1' => 'Moderar Canje',
    'icon' => 'badge-check',
    'submit' => [
        'label' => 'Procesar Moderacion',
        'icon' => 'send'
    ]
];
