<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actions = [
    'detail.sales',
    'delete.sales',
    'approve_manual.sales',
    'reject_manual.sales'
];

$massiveActions = [
    'detail_massive.sales',
    'approve_massive.sales',
    'reject_massive.sales',
];

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.sales'] = [
    'show' => [
        'title' => 'Ventas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas',
        'url' => [
            'controller' => 'sales', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Sale',
            'path' => 'Model',
            'query' => [
                'order' => 'Sale.sale_date DESC, Sale.id DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_manual' => [
        'title' => 'Ventas Manuales Aprobadas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas',
        'url' => [
            'controller' => 'sales', 'action' => 'show_manual'
        ],
        'data' => [
            'class' => 'Sale',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Sale.massive_sale_id' => null,
                    'Sale.approval' => 'samsung',
                    'Sale.rejected' => 0
                ],
                'order' => 'Sale.sale_date DESC, Sale.id DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_manual_pending' => [
        'title' => 'Ventas Manuales Pendientes',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas',
        'url' => [
            'controller' => 'sales', 'action' => 'show_manual_pending'
        ],
        'data' => [
            'class' => 'Sale',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Sale.massive_sale_id' => null,
                    'Sale.approval' => 'responsable',
                    'Sale.rejected' => 0
                ],
                'order' => 'Sale.sale_date DESC, Sale.id DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_massive' => [
        'title' => 'Ventas Masivas Aprobadas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas Masivas',
        'url' => [
            'controller' => 'sales', 'action' => 'show_massive'
        ],
        'data' => [
            'class' => 'MassiveSale',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'MassiveSale.approval' => 'samsung',
                    'MassiveSale.rejected' => 0
                ],
                'order' => 'MassiveSale.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_massive_pending' => [
        'title' => 'Ventas Masivas Pendientes',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Ventas Masivas',
        'url' => [
            'controller' => 'sales', 'action' => 'show_massive_pending'
        ],
        'data' => [
            'class' => 'MassiveSale',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'MassiveSale.approval' => 'responsable',
                    'MassiveSale.rejected' => 0
                ],
                'order' => 'MassiveSale.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# LISTAS

$config['Klezkaffold.show.sales.detail_massive'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'class' => 'Sale',
            'path' => 'Model',
            'order' => 'Sale.id DESC',
            'limit' => $limit,
            'conditions' => [
                'Sale.rejected' => 0
            ],
        ],
        'prequery' => [
            'filterMassiveSale'
        ]
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.sales.detail_pending_massive'] = [
    'data' => [
        'class' => 'FullSale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => [
            'class' => 'FullSale',
            'path' => 'Model',
            'order' => 'FullSale.id DESC',
            'conditions' => [
                'FullSale.rejected' => 0
            ],
            'limit' => $limit,
        ],
        'prequery' => [
            'filterPendingMassiveSale'
        ]
    ],
    'actions' => $actions
];


$config['Klezkaffold.show.sales.show_manual'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => $config['Klezkaffold.dashboard.sales']['show_manual']['data']['query'],
        'prequery' => [
            'filterCompany','customFiltersPrequery'
        ]
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.sales.show_manual_pending'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => $config['Klezkaffold.dashboard.sales']['show_manual_pending']['data']['query'],
        'prequery' => [
            'filterCompany','customFiltersPrequery'
        ]
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.sales.show_rejected'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'rejected',
        'query' => [
            'conditions' => [
                'Sale.rejected' => 1,
            ],
            'order' => 'Sale.sale_date DESC, Sale.id DESC',
            'limit' => $limit
        ],
        'prequery' => [
            'filterCompany',
            'filterVendedor','customFiltersPrequery'
        ]
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.sales.show_massive'] = [
    'data' => [
        'class' => 'MassiveSale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => $config['Klezkaffold.dashboard.sales']['show_massive']['data']['query'],
        'prequery' => [
//            'filterCompany'
        ],
        'params' => []
    ],
    'actions' => $massiveActions
];

$config['Klezkaffold.show_massive.sales'] = $config['Klezkaffold.show.sales.show_massive'];


$config['Klezkaffold.show.sales.show_massive_pending'] = [
    'data' => [
        'class' => 'MassiveSale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => $config['Klezkaffold.dashboard.sales']['show_massive_pending']['data']['query'],
        'prequery' => [
//            'filterCompany'
        ]
    ],
    'actions' => $massiveActions
];

$config['Klezkaffold.show.sales.show'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
        'query' => $config['Klezkaffold.dashboard.sales']['show']['data']['query'],
        'prequery' => [
            'filterCompany','customFiltersPrequery'
        ]
    ],
    'actions' => $actions
];


############################################################# APPROVE MASSIVE

$config['Klezkaffold.approve_massive.sales'] = [
    'data' => [
        'class' => 'MassiveSale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'id' => 'id',
    ],
    'rule' => 'checkSamsungApprove',
    'actions' => $actions
];

############################################################# REJECT MASSIVE

$config['Klezkaffold.reject_massive.sales'] = [
    'data' => [
        'class' => 'MassiveSale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'id' => 'id',
    ],
    'rule' => 'checkSamsungApprove',
    'actions' => $actions
];

############################################################# APPROVE MANUAL

$config['Klezkaffold.approve_manual.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'id' => 'id',
    ],
    'rule' => 'checkSamsungApprove',
    'actions' => $actions
];

############################################################# APPROVE MANUAL

$config['Klezkaffold.reject_manual.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'id' => 'id',
    ],
    'rule' => 'checkSamsungApprove',
    'actions' => $actions
];

############################################################# DETALLE MASIVO

$config['Klezkaffold.detail_massive.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'read',
        'prequery' => [
            'filterCompany',
            'filterVendedor'
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

$config['Klezkaffold.detail.frontend_sales'] = [
    'data' => [
        'class' => 'FullSale',
        'path' => 'Model',
        'schema' => 'read',
        'prequery' => [
            'filterCompany',
            'filterVendedor',
            'filterResponsableApproval'
        ],
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id'
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'manual',
        'beforeSave' => [
            'injectUser',
            'injectPoints',
            'injectSalesman',
            'injectApproval',
        ],
        'beforeCommit' => [
            'updateSalesmanPoints',
            'updateExtraTargetPoints',
            'updateResponsiblePoints',
            'updateEncargadosPoints',
            'updateRunSales',
            'updateAchievements',
            'updateChallenges',
        ],
    ],
];

$config['Klezkaffold.request_form.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'manual',
        'autocomplete' => [
            'prequery' => [
                'filterAutocomplete'
            ]
        ]
    ],
];

############################################################# CARGA MASIVA

$config['Klezkaffold.massive.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'massive',
        'massive' => [
            'source' => 'document',
            'mode' => 'singles',
            'class' => 'Sale',
            'path' => 'Model',
            'cache' => 'massive',
            'beforeSave' => [
                'injectMassiveSale',
                'injectUser',
                'injectCompany',
            ],
            'afterSave' => [
                'updateMassiveSalesmanPoints',
                'updateMassiveExtraPoints',
                'updateMassiveResponsiblePoints',
                'updateMassiveEncargadosPoints',
                'updateMassiveAchievements',
                'updateMassiveRunSales',
                'updateMassiveChallenges',
            ],
            'map' => [
                'product_id' => [
                    'type' => 'foreign',
                    'label' => 'Producto',
                    'resolver' => 'code'
                ],
                'quantity' => [
                    'type' => 'int',
                    'label' => 'Cantidad',
                ],
                'salesman_id' => [
                    'type' => 'foreign',
                    'label' => 'Vendedor',
                    'resolver' => 'document'
                ],
                'company_id' => [
                    'type' => 'foreign',
                    'calc' => true,
                    'method' => 'resolvMassiveCompany',
                    'label' => 'Empresa',
                ],
                'branch_id' => [
                    'type' => 'foreign',
                    'calc' => true,
                    'method' => 'resolvMassiveBranch',
                    'label' => 'Sucursal',
                ],
                'invoice' => [
                    'type' => 'text',
                    'label' => 'Factura',
                ],
                'sale_date' => [
                    'type' => 'date',
                    'label' => 'Fecha'
                ],
                'extra_target' => [
                    'type' => 'foreign',
                    'label' => 'Responsable/Encargado',
                    'resolver' => 'document'
                ],
                'points' => [
                    'type' => 'int',
                    'calc' => true,
                    'method' => 'calcPoints',
                    'label' => 'Puntos Calculados',
                ],
                'stock_warning' => [
                    'type' => 'text',
                    'calc' => true,
                    'method' => 'calcStockWarning',
                    'label' => 'En Stock?',
                ],
            ]
        ]
    ],
];

$config['Klezkaffold.request_massive.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'massive',
    ],
];
$config['Klezkaffold.sales.frontend'] = [
    'params' => [
        
    ],
];
################################################## FRONTEND


$config['Klezkaffold.show.sales.show_frontend'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'frontend',
        'query' => [
            'conditions' => [
                'Sale.rejected' => 0
            ],
            'joins' => [
                #'INNER JOIN companies AS Company ON Company.id=Sale.company_id',
            ],
            'order' => 'Sale.id DESC',
            'limit' => $limit
        ],
        'prequery' => [
            'filterCompany',
            'filterVendedor'
        ]
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.sales.show_pending_frontend'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'frontend',
        'query' => [
            'conditions' => [
                'Sale.approval' => 'ninguna',
                'Sale.rejected' => 0
            ],
            'joins' => [
                
            ],
            'order' => 'Sale.id DESC',
            'limit' => $limit
        ],
        'prequery' => [
            'filterCompany',
            'filterVendedor'
        ]
    ],
    'actions' => $actions
];

############################################################# FOTO

$config['Klezkaffold.image.photo.sales'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'field' => 'photo',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'foto'
    ],
    'params' => [
        'id' => 'id',
    ],
];
############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL

$config['Klezkaffold.xlsx.sales_show'] = [
    'type' => 'excel',
    'download' => [
        'file' => "ventas.xlsx",
    ],
    'filter' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'empty' 
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ventas',
                'data' => $config['Klezkaffold.show.sales.show']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_sales_show'] = $config['Klezkaffold.xlsx.sales_show'];
unset($config['Klezkaffold.report.xlsx_sales_show']['excel']['sheets'][0]['data']['query']['limit']);

$config['Klezkaffold.xlsx.sales_show_manual'] = [
    'type' => 'excel',
    'download' => [
        'file' => "ventas_manuales.xlsx",
    ],
    'filter' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'empty' 
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ventas Manuales',
                'data' => $config['Klezkaffold.show.sales.show_manual']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_sales_show_manual'] = $config['Klezkaffold.xlsx.sales_show_manual'];
unset($config['Klezkaffold.report.xlsx_sales_show_manual']['excel']['sheets'][0]['data']['query']['limit']);

$config['Klezkaffold.xlsx.sales_show_pending'] = [
    'type' => 'excel',
    'download' => [
        'file' => "ventas_rechazadas.xlsx",
    ],
    'filter' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'empty' 
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ventas Rechazadas',
                'data' => $config['Klezkaffold.show.sales.show_manual_pending']['data']
            ],
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.report.xlsx_sales_show_manual_pending'] = $config['Klezkaffold.xlsx.sales_show_pending'];
unset($config['Klezkaffold.report.xlsx_sales_show_manual_pending']['excel']['sheets'][0]['data']['query']['limit']);