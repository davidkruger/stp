<?php

################################# FILTERS

$filters = [];

$filters['any'] = [
    'data' => [
        'status' => true
    ]
];

$filters['frontend'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','vendedor','responsable','encargado' ]
    ]
];

$filters['responsable'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','responsable','encargado' ]
    ]
];

$filters['vendedor'] = [
    'data' => [
        'status' => true,
        'role' => 'vendedor'
    ]
];

################### FILTER FOR ENDPOINTS

$config = [];

$config['Filter.Config'] = [
    'filters' => [
        
    ]
];

$config['Filter.Login'] = [
    'filters' => [
        
    ]
];

$config['Filter.LoginConfig'] = [
    'filters' => [
        
    ]
];

$config['Filter.Home'] = [
    'filters' => $filters['any']
];

$config['Filter.Logout'] = [
    'filters' => $filters['any']
];

$config['Filter.Profile'] = [
    'filters' => $filters['any']
];

$config['Filter.ProfileEdit'] = [
    'filters' => $filters['any']
];

########################################################### KLEZKAFFOLD

$config['Filter.Dashboard'] = [
    'filters' => $filters['any']
];

$config['Filter.Show'] = [
    'filters' => $filters['any']
];

$config['Filter.Detail'] = [
    'filters' => $filters['any']
];

$config['Filter.RequestForm'] = [
    'filters' => $filters['any']
];

$config['Filter.Add'] = [
    'filters' => $filters['any']
];

$config['Filter.Edit'] = [
    'filters' => $filters['any']
];

$config['Filter.Delete'] = [
    'filters' => $filters['any']
];

$config['Filter.RequestDelete'] = [
    'filters' => $filters['any']
];

$config['Filter.Massive'] = [
    'filters' => $filters['any']
];

$config['Filter.RequestMassive'] = [
    'filters' => $filters['any']
];

########################################################### FRONTEND

$config['Filter.FrontendShow'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendAdd'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendRequestForm'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendPendingPrizes'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendImage'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendRedeem'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendPrizes'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendBranches'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendLogout'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendSellers'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendSeller'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendProducts'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendCompany'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendAssignPoints'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendSalesmen'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendDetail'] = [
    'filters' => $filters['responsable']
];

$config['Filter.ResponsableApproval'] = [
    'filters' => $filters['responsable']
];

$config['Filter.FrontendRejected'] = [
    'filters' => $filters['frontend']
];

$config['Filter.FrontendCatalogs'] = [
    'filters' => $filters['frontend']
];