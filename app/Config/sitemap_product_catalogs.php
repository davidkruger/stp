<?php

$config = [];

#################################################### SPECS DE PRODUCTO


$config['Sitemap.product_catalogs.add'] = [
    'h1' => 'Asignar Catalogo',
    'submit' => [
        'label' => 'Asignar Catalogo',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.product_catalogs.delete'] = [
    'h1' => 'Eliminar Catalogo',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Catalogo',
        'icon' => 'trash'
    ]
];

$config['Sitemap.product_catalogs.edit'] = [
    'h1' => 'Editar Catalogo',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Catalogo',
        'icon' => 'send'
    ]
];

$config['Sitemap.product_catalogs.image'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];