<?php

$config = [];

#################################################### Desafios

$config['Sitemap.challenges.dashboard'] = [
    'h1' => 'Módulo de Desafios',
];

$config['Sitemap.challenges.show'] = [
    'h1' => 'Listado de Desafios',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Desafios',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Desafios',
        'text' => 'No se han encontrado Desafios en esta página'
    ],
    'search' => [
        'label' => 'Filtrar Desafios',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'Desafios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.challenges.detail'] = [
    'h1' => 'Detalle del Desafio',
    'icon' => 'info-outline'
];

$config['Sitemap.challenges.edit'] = [
    'h1' => 'Editar Desafio',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Logro',
        'icon' => 'send'
    ]
];

$config['Sitemap.challenges.add'] = [
    'h1' => 'Crear Nuevo Desafio',
    'submit' => [
        'label' => 'Crear Desafio',
        'icon' => 'send'
    ]
];

$config['Sitemap.challenges.prize_img'] = [
    'h1' => 'Imagen del Premio',
    'icon' => 'camera'
];

$config['Sitemap.challenges.challenge_products'] = [
    'h1' => 'Lista de Productos asociados al Desafio',
    'icon' => 'local-mall',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Productos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Productos',
        'text' => 'No se han encontrado Productos para este Desafio'
    ],
    'search' => [
        'label' => 'Buscar Productos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'productos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];