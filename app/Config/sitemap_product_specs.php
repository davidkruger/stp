<?php

$config = [];

#################################################### SPECS DE PRODUCTO


$config['Sitemap.product_specs.add'] = [
    'h1' => 'Asignar Caracteristica',
    'submit' => [
        'label' => 'Asignar Caracteristica',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.product_specs.delete'] = [
    'h1' => 'Eliminar Caracteristica',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Caracteristica',
        'icon' => 'trash'
    ]
];

$config['Sitemap.product_specs.edit'] = [
    'h1' => 'Editar Caracteristica',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Caracteristica',
        'icon' => 'send'
    ]
];
