<?php

$config = [];

#################################################### BACKEND

$config['Sitemap.gateway.login'] = [
    'title' => 'Acceso | Samsung Premia'
];

$config['Sitemap.gateway.logout'] = [
    
];

$config['Sitemap.backend.home'] = [
    'h1' => 'Home'
];

$config['Sitemap.backend.profile_edit'] = [
    'h1' => 'Editar Mi Perfil',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Mi Perfil',
        'icon' => 'send'
    ],
    'flash' => [
        'success' => 'Perfil editado éxitosamente',
        'error' => 'Error al editar Perfil',
    ]
];

$config['Sitemap.backend.profile_photo'] = [
    'h1' => 'Foto de Perfil',
    'icon' => 'camera'
];

#################################################### GLOBALES

$config['Sitemap.Globals'] = [    
    'avatar' => Router::url([ 'controller' => 'backend', 'action' => 'profile_photo', 'plugin' => null], true),
    'title' => 'Samsung Premia',
    'footer' => '2017 © Agencia Moderna',
    'home' => [ 'controller' => 'backend', 'action' => 'home', 'plugin' => null],
    'logout' => [ 'controller' => 'gateway', 'action' => 'logout', 'plugin' => 'KlezBackend'],
    'profile_edit' => [ 'controller' => 'backend', 'action' => 'profile_edit', 'plugin' => null],
];