<?php

$config = [];

#################################################### ACHIEVEMENTS

$config['Sitemap.achievements.dashboard'] = [
    'h1' => 'Módulo de Logros',
];

$config['Sitemap.achievements.show'] = [
    'h1' => 'Listado de Logros',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Logros',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Logros',
        'text' => 'No se han encontrado Logros en esta página'
    ],
    'search' => [
        'label' => 'Filtrar Logros',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'Logros',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.achievements.detail'] = [
    'h1' => 'Detalle del Logro',
    'icon' => 'info-outline'
];

$config['Sitemap.achievements.edit'] = [
    'h1' => 'Editar Logro',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Logro',
        'icon' => 'send'
    ]
];

$config['Sitemap.achievements.add'] = [
    'h1' => 'Crear Nuevo Logro',
    'submit' => [
        'label' => 'Crear Logro',
        'icon' => 'send'
    ]
];

$config['Sitemap.achievements.icon_0'] = [
    'h1' => 'Icono 0',
    'icon' => 'camera'
];


$config['Sitemap.achievements.icon_1'] = [
    'h1' => 'Icono 1',
    'icon' => 'camera'
];

$config['Sitemap.achievements.achievement_products'] = [
    'h1' => 'Lista de Productos asociados al Logro',
    'icon' => 'local-mall',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Productos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Productos',
        'text' => 'No se han encontrado Productos para este Logro'
    ],
    'search' => [
        'label' => 'Buscar Productos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'productos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];