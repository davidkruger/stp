<?php

$config = [];

#################################################### STOCK


$config['Sitemap.stock.add'] = [
    'h1' => 'Crear Stock',
    'submit' => [
        'label' => 'Crear Stock',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.stock.delete'] = [
    'h1' => 'Eliminar Stock',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Stock',
        'icon' => 'trash'
    ]
];

$config['Sitemap.stock.edit'] = [
    'h1' => 'Editar Stock',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Stock',
        'icon' => 'send'
    ]
];
