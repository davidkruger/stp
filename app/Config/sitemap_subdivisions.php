<?php

$config = [];

#################################################### SUBDIVISIONES


$config['Sitemap.subdivisions.add'] = [
    'h1' => 'Crear Subivision',
    'submit' => [
        'label' => 'Crear Subivision',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.subdivisions.delete'] = [
    'h1' => 'Eliminar Subivision',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Subivision',
        'icon' => 'trash'
    ]
];

$config['Sitemap.subdivisions.edit'] = [
    'h1' => 'Editar Subivision',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Subivision',
        'icon' => 'send'
    ]
];


$config['Sitemap.subdivisions.segments'] = [
    'h1' => 'Lista de Segmentos',
    'icon' => 'puzzle-piece',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Segmentos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Segmentos',
        'text' => 'No se han encontrado Segmentos para esta Subdivision'
    ],
    'search' => [
        'label' => 'Buscar Segmentos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'segmentos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];