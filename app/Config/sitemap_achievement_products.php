<?php

$config = [];

#################################################### SUCURSALES DE EMPRESA


$config['Sitemap.achievement_products.add'] = [
    'h1' => 'Asignar Producto a Logro',
    'submit' => [
        'label' => 'Asignar Producto',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.achievement_products.delete'] = [
    'h1' => 'Eliminar Producto del Logro',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Producto del Logro',
        'icon' => 'trash'
    ]
];

$config['Sitemap.achievement_products.edit'] = [
    'h1' => 'Editar Producto del Logro',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Producto del Logro',
        'icon' => 'send'
    ]
];
