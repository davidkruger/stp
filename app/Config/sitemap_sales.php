<?php

$config = [];

#################################################### SALES

$config['Sitemap.sales.dashboard'] = [
    'h1' => 'Módulo de Ventas',
    'empty' => [
        'title' => 'No hay Resúmenes de Ventas',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.sales.show'] = [
    'h1' => 'Listado de Ventas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ventas',
        'text' => 'No se han encontrado Ventas en esta página'
    ],
    'search' => [
        'label' => 'Busqueda',
        'weight' => 3,
    ],
    'filters' => [
        'Filters/custom'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.sales.show_manual'] = [
    'h1' => 'Listado de Ventas Manuales Aprobadas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ventas',
        'text' => 'No se han encontrado Ventas en esta página'
    ],
    'search' => [
        'label' => 'Busqueda',
        'weight' => 3,
    ],
    'filters' => [
        'Filters/custom'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.sales.show_manual_pending'] = [
    'h1' => 'Listado de Ventas Manuales Pendientes',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ventas',
        'text' => 'No se han encontrado Ventas en esta página'
    ],
    'search' => [
        'label' => 'Busqueda',
        'weight' => 3,
    ],
    'filters' => [
        'Filters/custom'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.sales.detail'] = [
    'h1' => 'Detalle de la Venta',
    'icon' => 'info-outline'
];

$config['Sitemap.sales.delete'] = [
    'h1' => 'Eliminar Venta',
    'icon' => 'delete',
    // 'submit' => [
    //     'label' => 'Eliminar Venta',
    //     'icon' => 'trash'
    // ]
];

$config['Sitemap.sales.detail_massive'] = [
    'h1' => 'Detalle de la Venta Masiva',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'icon' => 'info-outline',
    'empty' => [
        'title' => 'No hay Ventas',
        'text' => 'No se han encontrado Ventas en esta página'
    ],
    'search' => [
        'label' => 'Busqueda'
    ]
];

$config['Sitemap.sales.approve_massive'] = [
    'h1' => 'Aprobar Ventas pendientes de la Venta Masiva',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'icon' => 'check-circle',
    'empty' => [
        'title' => 'No hay Ventas pendientes',
        'text' => 'No se han encontrado Ventas pendientes en esta página'
    ],
    'search' => [
        'label' => 'Busqueda',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
    'submit' => [
        'label' => 'Confirmar Aprobacion de Venta',
        'icon' => 'check-circle'
    ],
];

$config['Sitemap.sales.reject_massive'] = [
    'h1' => 'Rechazar Venta Masiva',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'icon' => 'block',
    'empty' => [
        'title' => 'No hay Ventas pendientes',
        'text' => 'No se han encontrado Ventas pendientes en esta página'
    ],
    'search' => [
        'label' => 'Busqueda',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
    'submit' => [
        'label' => 'Confirmar Rechazo de Venta',
        'icon' => 'ban'
    ],
];

$config['Sitemap.sales.massive'] = [
    'h1' => 'Carga Masiva de Ventas',
    'submit' => [
        'label' => 'Cargar Ventas',
        'icon' => 'send'
    ],
    'confirm' => [
        'label' => 'Confirmar Ventas',
        'icon' => 'send',
        'title' => 'Confirmar datos extraidos del CSV'
    ],
];

$config['Sitemap.sales.add'] = [
    'h1' => 'Carga Manual de Venta',
    'submit' => [
        'label' => 'Cargar Venta',
        'icon' => 'send'
    ],
];

$config['Sitemap.sales.approve_manual'] = [
    'h1' => 'Aprobar Venta',
    'icon' => 'check-circle',
    'submit' => [
        'label' => 'Confirmar Aprobacion de Venta',
        'icon' => 'check-circle'
    ],
];

$config['Sitemap.sales.reject_manual'] = [
    'h1' => 'Rechazar Venta',
    'icon' => 'block',
    'submit' => [
        'label' => 'Confirmar Rechazo de Venta',
        'icon' => 'ban'
    ],
];

$config['Sitemap.sales.show_massive'] = [
    'h1' => 'Listado de Ventas Masivas Aprobadas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas Masivas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ventas Masivas',
        'text' => 'No se han encontrado Ventas Masivas en esta página'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.sales.show_massive_pending'] = [
    'h1' => 'Listado de Ventas Masivas Pendientes',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Ventas Masivas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Ventas Masivas',
        'text' => 'No se han encontrado Ventas Masivas en esta página'
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'ventas',
        'weight' => 12,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ]
];

$config['Sitemap.sales.photo'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];