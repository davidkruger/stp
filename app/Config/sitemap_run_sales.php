<?php

$config = [];

#################################################### RUN SALES

$config['Sitemap.run_sales.dashboard'] = [
    'h1' => 'Módulo de Competencias',
];

$config['Sitemap.run_sales.show'] = [
    'h1' => 'Listado de Competencias',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Competencias',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Competencias',
        'text' => 'No se han encontrado Competencias en esta página'
    ],
    'search' => [
        'label' => 'Filtrar Competencia',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'competencias',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.run_sales.detail'] = [
    'h1' => 'Detalle de la Competencia',
    'icon' => 'info-outline'
];

$config['Sitemap.run_sales.edit'] = [
    'h1' => 'Editar Competencia',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Competencia',
        'icon' => 'send'
    ]
];

$config['Sitemap.run_sales.add'] = [
    'h1' => 'Crear Nueva Competencia',
    'submit' => [
        'label' => 'Crear Competencia',
        'icon' => 'send'
    ]
];

$config['Sitemap.run_sales.prize_img'] = [
    'h1' => 'Foto',
    'icon' => 'camera'
];

$config['Sitemap.run_sales.xlsx'] = [
    'h1' => 'Reporte',
    'icon' => 'download'
];

$config['Sitemap.run_sales.draw'] = [
    'h1' => 'Sorteo de la Competencia',
    'icon' => 'star-half'
];