<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.product_catalogs',
    'edit.product_catalogs',
];

############################################################# CATALOG x PRODUCT 

$config['Klezkaffold.product_catalogs.products'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'ProductCatalog.catalog_order ASC'
        ],
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'product' => [ 'payload', 'product' ],
    ],
    'links' => [
        [ 'controller' => 'product_catalogs', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.products.product_catalogs'] = $config['Klezkaffold.product_catalogs.products'];

############################################################# EDITAR

$config['Klezkaffold.edit.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'schema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'product' => 'product',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'schema' => 'productless',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'formSchema' => 'productless',
        'prequery' => [ 'productOwnerPrequery' ],
    ],
];


############################################################# FOTO

$config['Klezkaffold.image.image.product_catalogs'] = [
    'data' => [
        'class' => 'ProductCatalog',
        'path' => 'Model',
        'field' => 'image',
        'schema' => 'readable',
        'format' => 'image/png',
        'file' => 'foto'
    ],
    'params' => [
        'id' => 'id',
    ],
];
