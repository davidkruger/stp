<?php

$config = [];

#################################################### CONTACTO DE EMPRESA


$config['Sitemap.company_contacts.add'] = [
    'h1' => 'Asignar Contacto',
    'submit' => [
        'label' => 'Asignar Contacto',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.company_contacts.delete'] = [
    'h1' => 'Eliminar Contacto',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Contacto',
        'icon' => 'trash'
    ]
];

$config['Sitemap.company_contacts.edit'] = [
    'h1' => 'Editar Contacto',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Contacto',
        'icon' => 'send'
    ]
];
