<?php

$config = [];

#################################################### SPECS DE PRODUCTO


$config['Sitemap.product_points.add'] = [
    'h1' => 'Asignar Puntos',
    'submit' => [
        'label' => 'Asignar Puntos',
        'icon' => 'send'
    ],
    'icon' => 'plus-circle'
];

$config['Sitemap.product_points.delete'] = [
    'h1' => 'Eliminar Puntos',
    'icon' => 'delete',
    'submit' => [
        'label' => 'Eliminar Puntos para Empresa',
        'icon' => 'trash'
    ]
];

$config['Sitemap.product_points.edit'] = [
    'h1' => 'Editar Puntos',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Puntos para Empresa',
        'icon' => 'send'
    ]
];