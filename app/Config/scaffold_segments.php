<?php

$config = [];

############################################################# ACTIONS

$actions = [
//    'delete.segments',
    'edit.segments',
];

############################################################# SEGMENTOS X SUBDIVISION

$config['Klezkaffold.segments.subdivisions'] = [
    'data' => [
            'class' => 'Segment',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'Segment.id DESC'
        ],
        'prequery' => [ 'subOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'subdivision' => [ 'payload', 'subdivision' ],
    ],
    'links' => [
        [ 'controller' => 'segments', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.subdivisions.segments'] = $config['Klezkaffold.segments.subdivisions'];

############################################################# EDITAR

$config['Klezkaffold.edit.segments'] = [
    'data' => [
        'class' => 'Segment',
        'path' => 'Model',
        'schema' => 'subdivisionless',
    ],
    'params' => [
        'id' => 'id',
        'subdivision' => 'subdivision',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.segments'] = [
    'data' => [
        'class' => 'Segment',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'subdivision' => 'subdivision',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.segments'] = [
    'data' => [
        'class' => 'Segment',
        'path' => 'Model',
        'schema' => 'subdivisionless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.segments'] = [
    'data' => [
        'class' => 'Segment',
        'path' => 'Model',
        'schema' => 'subdivisionless',
        'autocomplete' => [
            'prequery' => [
                
            ]
        ]
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.segments'] = [
    'data' => [
        'class' => 'Segment',
        'path' => 'Model',
        'formSchema' => 'subdivisionless',
        'prequery' => [ 'subdivisionOwnerPrequery' ],
    ],
];
