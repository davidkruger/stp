<?php

$config = [];

############################################################# ACTIONS

$actions = [
    'delete.branches',
    'edit.branches',
];

############################################################# SUCURSAL x EMPRESA 

$config['Klezkaffold.branches.companies'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'Branch.id DESC'
        ],
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'company' => [ 'payload', 'company' ],
    ],
    'links' => [
        [ 'controller' => 'branches', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.companies.branches'] = $config['Klezkaffold.branches.companies'];

############################################################# EDITAR

$config['Klezkaffold.edit.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'company' => 'company',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
        'schema' => 'companyless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
        'schema' => 'companyless',
        'autocomplete' => [
            'prequery' => [
                'filterCompanyUsers'
            ]
        ]
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
        'formSchema' => 'companyless',
        'prequery' => [ 'companyOwnerPrequery' ],
    ],
];


############################################################# DETALLE

$config['Klezkaffold.detail.branches'] = [
    'data' => [
        'class' => 'Branch',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => []
];