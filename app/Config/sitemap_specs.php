<?php

$config = [];

#################################################### PREMIOS

$config['Sitemap.specs.dashboard'] = [
    'h1' => 'Módulo de Caracteristicas',
    'empty' => [
        'title' => 'No hay Resúmenes de Caracteristicas',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.specs.show'] = [
    'h1' => 'Caracteristicas Activas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Caracteristicas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Caracteristicas',
        'text' => 'No se han encontrado Caracteristicas en esta página'
    ],
    'search' => [
        'label' => 'Buscar Caracteristicas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'premios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.specs.show_disabled'] = [
    'h1' => 'Caracteristicas Suspendidas',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Caracteristicas',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Caracteristicas',
        'text' => 'No se han encontrado Caracteristicas en esta página'
    ],
    'search' => [
        'label' => 'Buscar Caracteristicas',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'premios',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.specs.detail'] = [
    'h1' => 'Detalle de la Caracteristica',
    'icon' => 'info-outline'
];

$config['Sitemap.specs.edit'] = [
    'h1' => 'Editar Caracteristica',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Caracteristica',
        'icon' => 'send'
    ]
];

$config['Sitemap.specs.add'] = [
    'h1' => 'Crear Nueva Caracteristica',
    'submit' => [
        'label' => 'Crear Caracteristica',
        'icon' => 'send'
    ]
];
