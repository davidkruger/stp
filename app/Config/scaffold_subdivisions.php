<?php

$config = [];

############################################################# ACTIONS

$actions = [
//    'delete.subdivisions',
    'edit.subdivisions',
    'segments.subdivisions',
];

############################################################# SUBDIVISION X DIVISION

$config['Klezkaffold.subdivisions.divisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'query' => [
            'limit' => 10,
            'order' => 'Subdivision.id DESC'
        ],
        'prequery' => [ 'divOwnerPrequery' ],
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actionParams' => [
        'slug' => [ 'payload', 'slug' ],
        'division' => [ 'payload', 'division' ],
    ],
    'links' => [
        [ 'controller' => 'subdivisions', 'action' => 'add']
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.divisions.subdivisions'] = $config['Klezkaffold.subdivisions.divisions'];

############################################################# EDITAR

$config['Klezkaffold.edit.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'schema' => 'divisionless',
    ],
    'params' => [
        'id' => 'id',
        'division' => 'division',
        'slug' => 'slug'
    ],
];

############################################################# ELIMINAR

$config['Klezkaffold.delete.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
    ],
    'params' => [
        'id' => 'id',
        'division' => 'division',
        'slug' => 'slug'
    ],
];

$config['Klezkaffold.request_delete.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'schema' => 'divisionless',
    ],
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'schema' => 'divisionless',
        'autocomplete' => [
            'prequery' => [
                
            ]
        ]
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'formSchema' => 'divisionless',
        'prequery' => [ 'divisionOwnerPrequery' ],
    ],
];

############################################################# DETALLE

$config['Klezkaffold.detail.subdivisions'] = [
    'data' => [
        'class' => 'Subdivision',
        'path' => 'Model',
        'prequery' => [
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => []
];