<?php

$config = [];

#################################################### DIVISIONES

$config['Sitemap.groups.dashboard'] = [
    'h1' => 'Módulo de Grupos',
    'empty' => [
        'title' => 'No hay Resúmenes de Grupos',
        'text' => 'No se han encontrado resúmenes en esta página'
    ]
];

$config['Sitemap.groups.show'] = [
    'h1' => 'Grupos Activos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Grupos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Grupos',
        'text' => 'No se han encontrado Grupos en esta página'
    ],
    'search' => [
        'label' => 'Buscar Grupos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'grupos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];


$config['Sitemap.groups.show_disabled'] = [
    'h1' => 'Grupos Suspendidos',
    'pagination' => [
        'summary' => 'Mostrando %d de %d Grupos',
        'next' => 'Siguiente',
        'back' => 'Anterior',
        'spread' => 3
    ],
    'empty' => [
        'title' => 'No hay Grupos',
        'text' => 'No se han encontrado Grupos en esta página'
    ],
    'search' => [
        'label' => 'Buscar Grupos',
        'weight' => 3,
    ],
    'limits' => [
        'label' => 'Mostrar',
        'after' => 'grupos',
        'weight' => 9,
        'align' => 'right',
        'limits' => [
            10, 20, 30
        ]
    ],
];

$config['Sitemap.groups.detail'] = [
    'h1' => 'Detalle del Grupo',
    'icon' => 'info-outline'
];

$config['Sitemap.groups.edit'] = [
    'h1' => 'Editar Grupo',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Grupo',
        'icon' => 'send'
    ]
];

$config['Sitemap.groups.add'] = [
    'h1' => 'Crear Nuevo Grupo',
    'submit' => [
        'label' => 'Crear Grupo',
        'icon' => 'send'
    ]
];