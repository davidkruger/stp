<?php

################################# FILTERS

$filters = [];

$filters['any'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['samsung'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','samsung' ]
    ]
];

$filters['users_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_write_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['companies_massive_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array', 'lamoderna', 'samsung' ]
    ]
];

$filters['sales_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['products_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['prizes_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['redeems_moderation_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complains_read_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

$filters['complain_sales_moderation_abm'] = [
    'data' => [
        'status' => true,
        'role' => [ 'in_array','lamoderna','samsung' ]
    ]
];

################################# CONFIG

$config = [];

################################# BACKEND

$config['Filter.backend.home'] = [
    'filters' => $filters['any']
];

$config['Filter.backend.profile_photo'] = [
    'filters' => $filters['any']
];

$config['Filter.backend.profile_edit'] = [
    'filters' => $filters['any']
];

################################# USERS

$config['Filter.users.dashboard'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.add'] = [
    'filters' => $filters['users_abm']
];

//$config['Filter.users.show_romis'] = [
//    'filters' => $filters['users_abm']
//];

$config['Filter.users.show_samsung'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.show_disabled'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.show_lamoderna'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.show_responsibles'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.show_sellers'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.edit'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.detail'] = [
    'filters' => $filters['users_abm']
];

$config['Filter.users.picture'] = [
    'filters' => $filters['users_abm']
];

################################# COMPANIES

$config['Filter.companies.massive'] = [
    'filters' => $filters['companies_massive_abm']
];

$config['Filter.companies.dashboard'] = [
    'filters' => $filters['companies_read_abm']
];

$config['Filter.companies.add'] = [
    'filters' => $filters['companies_write_abm']
];

$config['Filter.companies.show'] = [
    'filters' => $filters['companies_read_abm']
];

$config['Filter.companies.show_disabled'] = [
    'filters' => $filters['companies_read_abm']
];

$config['Filter.companies.edit'] = [
    'filters' => $filters['companies_write_abm']
];

$config['Filter.companies.detail'] = [
    'filters' => $filters['companies_read_abm']
];

$config['Filter.company_contacts.show'] = [
    'filters' => $filters['companies_read_abm']
];

$config['Filter.company_contacts.edit'] = [
    'filters' => $filters['companies_write_abm']
];

$config['Filter.company_contacts.delete'] = [
    'filters' => $filters['companies_write_abm']
];

################################# PRODUCTS

$config['Filter.products.dashboard'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.products.add'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.products.show'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.products.show_disabled'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.products.edit'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.products.detail'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.product_promotions.show'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.product_promotions.edit'] = [
    'filters' => $filters['products_abm']
];

$config['Filter.product_promotions.delete'] = [
    'filters' => $filters['products_abm']
];

################################# PREMIOS

$config['Filter.prizes.dashboard'] = [
    'filters' => $filters['prizes_abm']
];

$config['Filter.prizes.add'] = [
    'filters' => $filters['prizes_abm']
];

$config['Filter.prizes.show'] = [
    'filters' => $filters['prizes_abm']
];

$config['Filter.prizes.show_disabled'] = [
    'filters' => $filters['prizes_abm']
];

$config['Filter.prizes.edit'] = [
    'filters' => $filters['prizes_abm']
];

$config['Filter.prizes.detail'] = [
    'filters' => $filters['prizes_abm']
];

################################# VENTAS

$config['Filter.sales.show_massive'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.show_massive_pending'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.massive'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.approve_massive'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.detail_massive'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.add'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.detail'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.sales.show'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.prizes.detail'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.prize_details.show_manual'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.prize_details.show_manual_pending'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.prize_details.approve_manual'] = [
    'filters' => $filters['samsung']
];

################################# COMPETENCIAS

$config['Filter.run_sales.add'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.run_sales.show'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.run_sales.detail'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.run_sales.edit'] = [
    'filters' => $filters['sales_abm']
];

################################# CANJES

$config['Filter.redeems.edit'] = [
    'filters' => $filters['redeems_moderation_abm']
];

$config['Filter.redeems.show_pending'] = [
    'filters' => $filters['redeems_moderation_abm']
];

$config['Filter.redeems.show_approved'] = [
    'filters' => $filters['redeems_moderation_abm']
];

$config['Filter.redeems.show_rejected'] = [
    'filters' => $filters['redeems_moderation_abm']
];

$config['Filter.redeems.detail'] = [
    'filters' => $filters['redeems_moderation_abm']
];

$config['Filter.redeem_moderations.detail'] = [
    'filters' => $filters['redeems_moderation_abm']
];

################################# COMPLAINS

$config['Filter.complains.dashboard'] = [
    'filters' => $filters['complains_read_abm']
];

$config['Filter.complains.show_unread'] = [
    'filters' => $filters['complains_read_abm']
];

$config['Filter.complains.show_read'] = [
    'filters' => $filters['complains_read_abm']
];

$config['Filter.complains.detail'] = [
    'filters' => $filters['complains_read_abm']
];

################################# RECLAMOS DE VENTAS

$config['Filter.complain_sales.edit'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['Filter.complain_sales.show_pending'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['Filter.complain_sales.show_approved'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['Filter.complain_sales.show_rejected'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['Filter.complain_sales.detail'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

$config['Filter.complain_sale_moderations.detail'] = [
    'filters' => $filters['complain_sales_moderation_abm']
];

#################################### REPORTES

$config['Filter.reports.summary'] = [
    'filters' => $filters['any']
];


################################# ROULETTES

$config['Filter.roulettes.add'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.roulettes.show'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.roulettes.detail'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.roulettes.edit'] = [
    'filters' => $filters['sales_abm']
];

################################# LOTTERIES

$config['Filter.lotteries.add'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.lotteries.show'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.lotteries.detail'] = [
    'filters' => $filters['sales_abm']
];

$config['Filter.lotteries.edit'] = [
    'filters' => $filters['sales_abm']
];