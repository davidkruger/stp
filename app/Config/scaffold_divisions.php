<?php

$config = [];
$limit = 10;

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.divisions'] = [
    'show' => [
        'title' => 'Divisiones Activas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Divisiones',
        'url' => [
            'controller' => 'divisions', 'action' => 'show'
        ],
        'data' => [
            'class' => 'Division',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Division.status' => true
                ],
                'order' => 'Division.created DESC',
                'limit' => $limit
            ],
        ]
    ],
    'show_disabled' => [
        'title' => 'Divisiones Suspendidas',
        'type' => 'summary_pie',
        'text' => 'Cantidad de Divisiones',
        'url' => [
            'controller' => 'divisions', 'action' => 'show_disabled'
        ],
        'data' => [
            'class' => 'Division',
            'path' => 'Model',
            'query' => [
                'conditions' => [
                    'Division.status' => false
                ],
                'order' => 'Division.created DESC',
                'limit' => $limit
            ],
        ]
    ],
];

############################################################# ACTIONS

$actions = [
    'detail.divisions',
    'edit.divisions',
    'subdivisions.divisions',
];

############################################################# LISTAS

$config['Klezkaffold.show.divisions.show'] = [
    'data' => $config['Klezkaffold.dashboard.divisions']['show']['data'],
    'actions' => $actions
];

$config['Klezkaffold.show.divisions.show_disabled'] = [
    'data' => $config['Klezkaffold.dashboard.divisions']['show_disabled']['data'],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.divisions'] = [
    'data' => [
        'class' => 'Division',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# EDITAR

$config['Klezkaffold.edit.divisions'] = [
    'data' => [
        'class' => 'Division',
        'path' => 'Model',
    ],
    'params' => [
        'slug' => [ 'slugify', 'name' ],
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.divisions'] = [
    'data' => [
        'class' => 'Division',
        'path' => 'Model',
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.divisions'] = [
    'data' => [
        'class' => 'Division',
        'path' => 'Model',
    ],
];