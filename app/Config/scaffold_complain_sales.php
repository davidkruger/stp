<?php

$config = [];
$limit = 10;

############################################################# ACTIONS

$actionsPending = [
    'edit.complain_sales',
];

$actions = [
    'detail.complain_sales',
    'detail.complain_sale_moderations',
];

############################################################# LISTAS

$config['Klezkaffold.show.complain_sales.show_pending'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'query' => [
            'conditions' => [
                'ComplainSale.status' => 'pending'
            ],
            'order' => 'ComplainSale.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actionsPending
];

$config['Klezkaffold.show.complain_sales.show_approved'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'query' => [
            'conditions' => [
                'ComplainSale.status' => 'accepted'
            ],
            'order' => 'ComplainSale.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actions
];

$config['Klezkaffold.show.complain_sales.show_rejected'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'query' => [
            'conditions' => [
                'ComplainSale.status' => 'rejected'
            ],
            'order' => 'ComplainSale.created DESC',
            'limit' => $limit
        ],
    ],
    'actions' => $actions
];

############################################################# DETALLE

$config['Klezkaffold.detail.complain_sales'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'prequery' => [
            'notPendingFilter',
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];
############################################################# DETALLE MODERACION

$config['Klezkaffold.detail.complain_sale_moderations'] = [
    'data' => [
        'class' => 'ComplainSaleModeration',
        'path' => 'Model',
        'schema' => 'readable',
        'prequery' => [
            'injectComplainSale',
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => []
];

############################################################# EDITAR

$config['Klezkaffold.edit.complain_sales'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'schema' => 'moderation',
        'query' => [
            'conditions' => [
                'ComplainSale.status' => 'pendiente'
            ],
        ],
        'beforeCommit' => [
            'pushModeration',
            'injectSale',
            'updateCompanyPoints'
        ],
        'prequery' => [
            'pendingFilter'
        ]
    ],
    'params' => [
        'id' => 'id',
    ],
    'actions' => $actions
];

############################################################# FORMULARIO

$config['Klezkaffold.request_form.complain_sales'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'schema' => 'moderation',
        'prequery' => [
            'pendingFilter'
        ]
    ],
];

############################################################# CREAR

$config['Klezkaffold.add.complain_sales'] = [
    'data' => [
        'class' => 'ComplainSale',
        'path' => 'Model',
        'schema' => 'frontend',
        'beforeSave' => [
            'injectUser'
        ]
    ],
];
