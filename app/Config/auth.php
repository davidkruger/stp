<?php

$config = [];

#################################################### BACKEND GW
#
// Puerta de acceso al autenticador

$admin = [];
$admin['gateway'] = [
    'driver' => 'login',
    'api' => Router::url([
        'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi',
        'endpoint' => 'login', 'format' => 'json'
    ], true),
    'clients' => [
       'login' => ['controller' => 'gateway','action' => 'login','plugin' => 'KlezBackend'],
       'logout' => ['controller' => 'gateway','action' => 'logout','plugin' => 'KlezBackend'],
    ],
    'bounces' => [
        'login' => ['controller' => 'backend','action' => 'home','plugin' => null],
        'logout' => ['controller' => 'gateway','action' => 'login','plugin' => 'KlezBackend'],
        'memento' => true
    ],
];

// Mecanismo de retencion de la autenticacion

$admin['mechanism'] = [
    'driver' => 'session',
    'key' => 'BackendUserSession'
];

// Entidad contra la cual autenticar

$admin['entity'] = [
    'driver' => 'backend',
    'driver-location' => 'Controller/AuthEntity',
    'model' => 'User',
    'location' => 'Model',
    'fields' => [ 'document', 'password' ],
];

$config['Auth.Admin'] = $admin;


#################################################### FRONTEND GW
#
// Puerta de acceso al autenticador

$user = [];
$user['gateway'] = [
    'driver' => 'login',
    'api' => Router::url([
        'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi',
        'endpoint' => 'login', 'format' => 'json'
    ], true),
    'clients' => [
       'login' => ['controller' => 'frontend','action' => 'login','plugin' => null],
       'logout' => ['controller' => 'frontend','action' => 'logout','plugin' => null],
    ],
    'bounces' => [
        'login' => ['controller' => 'frontend','action' => 'home','plugin' => null],
        'logout' => ['controller' => 'frontend','action' => 'home','plugin' => null],
        'memento' => true
    ],
];

// Mecanismo de retencion de la autenticacion

$user['mechanism'] = [
    'driver' => 'session',
    'key' => 'FrontendUserSession'
];

// Entidad contra la cual autenticar

$user['entity'] = [
    'driver' => 'frontend',
    'driver-location' => 'Controller/AuthEntity',
    'model' => 'User',
    'location' => 'Model',
    'fields' => [ 'document', 'password' ],
];

$config['Auth.User'] = $user;