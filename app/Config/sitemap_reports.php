<?php

$config = [];

#################################################### SUMMARY

$config['Sitemap.reports.summary'] = [
    'h1' => 'Acumulado de Puntos',
];
$config['Sitemap.reports.total'] = [
    'h1' => 'Reporte Total',
    'empty' => [
        'title' => 'No hay Empresas Activas',
        'text' => 'Debe ingresar al menos una empresa para ver el reporte'
    ],
    'submit' => [
        'label' => 'Generar Reporte',
        'icon' => 'send'
    ],
];

$config['Sitemap.reports.companies'] = [
    'h1' => 'Por Empresa',
    'submit' => [
        'label' => 'Generar Reporte',
        'icon' => 'send'
    ],
];

$config['Sitemap.reports.top10'] = [
    'h1' => 'Top 10',
];

$config['Sitemap.reports.top10general'] = [
    'h1' => 'Top 10 General',
];

$config['Sitemap.reports.top10lastmonth'] = [
    'h1' => 'Top 10 Mes Pasado',
];

$config['Sitemap.reports.top10division'] = [
    'h1' => 'Top 10 Por Division',
];

$config['Sitemap.reports.top10divisionlastmonth'] = [
    'h1' => 'Top 10 Por Division Mes Pasado',
];

$config['Sitemap.reports.products'] = [
    'h1' => 'Por Producto',
    'submit' => [
        'label' => 'Generar Reporte',
        'icon' => 'send'
    ],
];


#################################################### VENTAS

$config['Sitemap.reports.sales_xlsx'] = [
    'h1' => 'Descargar Excel',
    'icon' => 'download',
    'submit' => [
        'label' => 'Descargar Excel',
        'icon' => 'download'
    ]
];

#################################################### ACHIEVEMENTS

$config['Sitemap.reports.achievements_xlsx'] = [
    'h1' => 'Descargar Excel',
    'icon' => 'download',
    'submit' => [
        'label' => 'Descargar Excel',
        'icon' => 'download'
    ]
];
#################################################### CHALLENGES

$config['Sitemap.reports.challenges_xlsx'] = [
    'h1' => 'Descargar Excel',
    'icon' => 'download',
    'submit' => [
        'label' => 'Descargar Excel',
        'icon' => 'download'
    ]
];
#################################################### EMPRESAS

$config['Sitemap.reports.companies_xlsx'] = [
    'h1' => 'Descargar Excel',
    'icon' => 'download',
    'submit' => [
        'label' => 'Descargar Excel',
        'icon' => 'download'
    ],
];

#################################################### USERS

$config['Sitemap.reports.users_xlsx'] = [
    'h1' => 'Descargar Excel',
    'icon' => 'download',
    'submit' => [
        'label' => 'Descargar Excel',
        'icon' => 'download'
    ],
];