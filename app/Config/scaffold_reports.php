<?php

$config = [];

############################################################# DASHBOARD

$config['Klezkaffold.dashboard.reports'] = [
    'total' => [
        'title' => 'Reporte Total',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'table',
        'url' => [
            'controller' => 'reports', 'action' => 'total'
        ],
    ],
    'companies' => [
        'title' => 'Por Empresa',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'home',
        'url' => [
            'controller' => 'reports', 'action' => 'companies'
        ],
    ],
    'top10' => [
        'title' => 'Top 10',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'star',
        'url' => [
            'controller' => 'reports', 'action' => 'top10'
        ],
    ],
    'products' => [
        'title' => 'Por Producto',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'briefcase',
        'url' => [
            'controller' => 'reports', 'action' => 'products'
        ],
    ],
];

$config['Klezkaffold.dashboard.top10'] = [
    'top10general' => [
        'title' => 'Top 10 General',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'star',
        'url' => [
            'controller' => 'reports', 'action' => 'top10general'
        ],
    ],
    'top10lastmonth' => [
        'title' => 'Top 10 Mes Pasado',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'calendar',
        'url' => [
            'controller' => 'reports', 'action' => 'top10lastmonth'
        ],
    ],
    'top10division' => [
        'title' => 'Top 10 Por Division',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'star',
        'url' => [
            'controller' => 'reports', 'action' => 'top10division'
        ],
    ],
    'top10divisionlastmonth' => [
        'title' => 'Top 10 Por Division Mes Pasado',
        'type' => 'holders',
        'class' => 'success',
        'icon' => 'calendar',
        'url' => [
            'controller' => 'reports', 'action' => 'top10divisionlastmonth'
        ],
    ],
];


############################################################# EXCEL STYLE

$styleHead = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => 'FFFFFF' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'hair'
        ]
    ],
    'fill' => [
        'type' => 'solid',
        'color' => [ 'rgb' => '000000' ]
    ]
];

$styleValue = [
    'font' => [
        'bold' => true,
        'color' => [ 'rgb' => '000000' ]
    ],
    'borders' => [
        'allborders' => [
            'style' => 'solid'
        ]
    ],
];

########################################################### EXCEL VENTAS

$config['Klezkaffold.report.sales_xlsx'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'xlsxSaleFilter',
    ],
    'data' => [ 'class' => 'Sale'],
    'download' => [
        'file' => "ventas.xlsx",
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Ventas',
                'data' => [
                    'class' => 'Sale',
                    'path' => 'Model',
                    'schema' => 'xlsxSale',
                    'query' => [
                        'order' => 'Sale.sale_date ASC',
                    ],
                    'prequery' => [
                        'xlsxSaleFilter'
                    ],
                ],
            ]
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];
########################################################### EXCEL LOGROS

$config['Klezkaffold.report.achievements_xlsx'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'data' => [ 'class' => 'Achievement'],
    'download' => [
        'file' => "logros.xlsx",
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Logros',
                'data' => [
                    'class' => 'Achievement',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                    ],
                    'prequery' => [
                        'xlsxFilter'
                    ],
                ],
            ]
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];
########################################################### EXCEL DESAFIOS

$config['Klezkaffold.report.challenges_xlsx'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'data' => [ 'class' => 'Challenge'],
    'download' => [
        'file' => "desafios.xlsx",
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Desafios',
                'data' => [
                    'class' => 'Challenge',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                    ],
                    'prequery' => [
                        'xlsxFilter'
                    ],
                ],
            ]
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

################################################## FILTRO SUMMARY

$config['Klezkaffold.request_form.reports_summary'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'reportSummaryFilters',
    ],
];

################################################## FILTRO COMPANIES

$config['Klezkaffold.request_form.reports_companies'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'reportCompaniesFilters',
    ],
];

################################################## FILTRO PRODUCTS

$config['Klezkaffold.request_form.reports_products'] = [
    'data' => [
        'class' => 'Sale',
        'path' => 'Model',
        'schema' => 'reportProductsFilters',
    ],
];

$config['Klezkaffold.request_form.achievements_xlsx'] = [
    'data' => [
        'class' => 'Achievement',
        'path' => 'Model',
        'schema' => 'xlsxFilter'
    ], 
];
$config['Klezkaffold.request_form.challenges_xlsx'] = [
    'data' => [
        'class' => 'Challenge',
        'path' => 'Model',
        'schema' => 'xlsxFilter'
    ], 
];
########################################################### EXCEL COMPANIES

$config['Klezkaffold.report.companies_xlsx'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'Company',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'data' => [ 'class' => 'Company'],
    'download' => [
        'file' => "empresas.xlsx",
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Empresas',
                'data' => [
                    'class' => 'Company',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                    ],
                    'prequery' => [
                        'xlsxFilter'
                    ],
                ],
            ]
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];
########################################################### EXCEL USUARIOS

$config['Klezkaffold.report.users_xlsx'] = [
    'type' => 'excel',
    'filter' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
    'data' => [ 'class' => 'User'],
    'download' => [
        'file' => "usuarios.xlsx",
    ],
    'excel' => [
        'sheets' => [
            [
                'name' => 'Usuarios',
                'data' => [
                    'class' => 'User',
                    'path' => 'Model',
                    'schema' => 'xlsx',
                    'query' => [
                    ],
                    'prequery' => [
                        'xlsxFilter'
                    ],
                ],
            ]
        ],
        'styles' => [
            'head' => $styleHead,
            'value' => $styleValue
        ]
    ]
];

$config['Klezkaffold.request_form.users_xlsx'] = [
    'data' => [
        'class' => 'User',
        'path' => 'Model',
        'schema' => 'xlsxFilter',
    ],
];