<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?=$this->Html->url('/klez_backend/assets/images/favicon.ico')?>">
        <title><?=$this->Backend->feed('title')?></title>

        <link href="<?=$this->Html->url('/klez_backend/assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/core.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/components.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/icons.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/menu.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/responsive.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
    
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?=$this->Html->url('/klez_backend/assets/js/modernizr.min.js')?>"></script>
    </head>
    <body>
        <?=$this->fetch('content'); ?>
        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/detect.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/fastclick.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/waves.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/wow.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.nicescroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.js')?>"></script>

        <!-- App js -->
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.core.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.app.js')?>"></script>
        
        <?php $this->Flash->deploy(); ?>
    </body>
</html>