<script>
    $(function(){
        var kind = <?=  json_encode($class)?>;
        var message = <?=  json_encode($message)?>;

        toastr[kind](message);
    });
</script>