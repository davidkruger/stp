<?php

App::uses('KlezBackendHelper', 'KlezBackend.View/Helper');

class BackendHelper extends KlezBackendHelper {
    public function provideLogtag() {
        return 'Backend Feed';
    }

    public function provideVarname() {
        return BACKEND_DATA;
    }
    
    public function breadcrumb(){
        $breadcrumb = $this->feed('breadcrumb',false);
        
        if(isset($breadcrumb['before']) &&is_array($breadcrumb['before'])){
            foreach($breadcrumb['before'] as $conf){
                if(is_array($conf['url'])){
                    $conf['url'] = Router::url($conf['url']);
                }
                
                echo $this->_View->element('KlezBackend.Breadcrumb/before',$conf);
            }
        }
                
        echo $this->_View->element('KlezBackend.Breadcrumb/h1');
                
        if(isset($breadcrumb['after']) && is_array($breadcrumb['after'])){
            foreach($breadcrumb['after'] as $conf){
                if(is_array($conf['url'])){
                    $conf['url'] = Router::url($conf['url']);
                }
                
                echo $this->_View->element('KlezBackend.Breadcrumb/after',$conf);
            }
        }
    }
}