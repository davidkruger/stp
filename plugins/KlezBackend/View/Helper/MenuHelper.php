<?php

App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper {
    public function deploy($config,$class = ''){
        echo $this->element('main',[
            'config' => $config,
            'class' => $class
        ]);
    }
    
    private function element($name,$vars){
        return $this->_View->element("KlezBackend.Menu/{$name}",$vars);
    }
    
    public function main($config){
        if(is_array($config)){
            foreach($config as $ctrl => $conf){
                if(isset($conf['type']) === false){
                    continue;
                }
                
                $this->deployByType($conf,$ctrl);
            }
        }
    }
    
    private function deployByType($conf,$ctrl){
        $type = $conf['type'];
        unset($conf['type']);
        
        switch($type){
            case 'link':
                $conf = $this->configureLink($conf);
                break;
            case 'parent':
                $conf = $this->configureParent($conf,$ctrl);
                break;
        }
        
        echo $this->element("type_{$type}", $conf);
    }
    
    private function configureParent($conf,$ctrl){
        $controller = $this->_View->params['controller'];
        
        if(strcmp($controller, $ctrl) === 0){
            reset($conf['childs']);
            $key = key($conf['childs']);
            
            if(is_null($key) === false){
                $conf['childs'][$key]['active'] = 'active';
            }
        }
        
        return $conf;
    }
    
    private function configureLink($conf){
        if(isset($conf['url']) === false){
            return $conf;
        }
        
        $conf['url'] = $this->resolvFullUrl($conf['url']);
        
        if(isset($conf['active']) === false){
            $conf['active'] = $this->resolvActiveUrl($conf['url']);
        }
        
        return $conf;
    }
    
    private function resolvActiveUrl($url){
        $here = Router::url($this->_View->here,true);
        $active = '';
        
        if(strcmp($url, $here) === 0){
            $active = 'active';
        }
        
        return $active;
    }
    
    private function resolvFullUrl($url){
        if(isset($url['plugin']) === false){
            $url['plugin'] = null;
        }
        
        return Router::url($url,true);
    }
    
    public function icon($icon){
        if(is_null($icon)){
            return;
        }
        
        echo $this->element('icon', [
            'icon' => $icon
        ]);
    }
}