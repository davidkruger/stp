<?php

App::uses('AppHelper', 'View/Helper');

class FlashHelper extends AppHelper {
    public function deploy(){
        echo $this->element('main');
    }
    
    private function element($name,$vars = []){
        return $this->_View->element("KlezBackend.Flash/{$name}",$vars);
    }
    
    public function main(){
        $flashData = CakeSession::consume('Backend.flash');
        
        if(isset($flashData['class'])){
            $this->deployByClass($flashData);
        }
    }
    
    private function deployByClass($flashData){        
        if(isset($flashData['message']) === false){
            return;
        }
        
        $message = $flashData['message'];
        $class = $flashData['class'];
        
        switch($class){
            case 'error':
            case 'fail':
            case 'failure':
                $this->error($message);
                break;
            case 'success':
            case 'done':
                $this->success($message);
                break;
            case 'info':
                $this->info($message);
                break;
            case 'warning':
                $this->warning($message);
                break;
        }
    }
    
    private function error($message){
        echo $this->element('toast',[
            'message' => $message,
            'class' => 'error'
        ]);
    }
    
    private function success($message){
        echo $this->element('toast',[
            'message' => $message,
            'class' => 'success'
        ]);        
    }
    
    private function info($message){
        echo $this->element('toast',[
            'message' => $message,
            'class' => 'info'
        ]);        
    }
    
    private function warning($message){
        echo $this->element('toast',[
            'message' => $message,
            'class' => 'warning'
        ]);        
    }
}