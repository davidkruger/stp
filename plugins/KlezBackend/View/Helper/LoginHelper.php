<?php

App::uses('AppHelper', 'View/Helper');

class LoginHelper extends AppHelper {
    public function resolvInputType($schema,$field) {
        if(isset($schema[$field]['subtype'])){
            return $schema[$field]['subtype'];
        }
        
        return 'text';
    }
    
    public function resolvInputPlaceholder($schema,$field) {
        if(isset($schema[$field]['label'])){
            return $schema[$field]['label'];
        }
        
        return ucfirst($field);
    }
    
    public function resolvInputRequired($schema,$field) {
        if(isset($schema[$field]['required'])){
            return $schema[$field]['required'] ? 'required' : '';
        }
        
        return '';
    }
    
    public function resolvInputName($schema,$field) {
        return $field;
    }
}