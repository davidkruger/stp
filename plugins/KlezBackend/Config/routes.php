<?php

Router::connect('/', [ 'controller' => 'backend', 'action' => 'home', 'plugin' => 'KlezBackend' ]);
Router::connect('/login.html', [ 'controller' => 'gateway', 'action' => 'login', 'plugin' => 'KlezBackend' ]);
Router::connect('/logout.html', [ 'controller' => 'gateway', 'action' => 'logout', 'plugin' => 'KlezBackend' ]);
Router::connect('/mi-perfil/editar.html', [ 'controller' => 'backend', 'action' => 'profile_edit', 'plugin' => 'KlezBackend' ]);