<?php

$config = [];

$config['Menu.title'] = [
    'type' => 'text',
    'label' => 'Navegación'
];

$config['Menu.dashboard'] = [
    'type' => 'link',
    'icon' => 'home',
    'label' => 'Home',
    'url' => [ 'controller' => 'backend', 'action' => 'home', 'plugin' => 'KlezBackend' ]
];