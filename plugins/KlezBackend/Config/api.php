<?php

$config = [];
$config['Api.login'] = Router::url('/api/backend/login.json',true);
$config['Api.logout'] = Router::url('/api/backend/logout.json',true);
$config['Api.login_config'] = Router::url('/api/backend/login_config.json',true);
$config['Api.home'] = Router::url('/api/backend/home.json',true);
$config['Api.dashboard'] = Router::url('/api/backend/dashboard.json',true);
$config['Api.show'] = Router::url('/api/backend/show.json',true);