<?php

App::uses('KlezBackendAppController', 'KlezBackend.Controller');

class GatewayController extends KlezBackendAppController {
    public $components = [ 'KlezBackend.Web', 'Session' ];
    public $helpers = [ 'KlezBackend.Login', 'KlezBackend.Api', 'KlezBackend.Backend' ];
    
    public function login(){
        $this->layout = 'KlezBackend.login';
        
        if($this->request->is('POST')){
            $this->Web->login($this->data);
        }
        
        $this->Web->loginConf();
    }
    
    public function logout(){
        $this->Web->logout();
    }
    
    public function beforeRender() {
        parent::beforeRender();
        $this->Session->renew();
    }
}