<?php

App::uses('ClientComponent', 'KlezApi.Controller/Component');

if(!defined('API_DATA')){
    define('API_DATA',  'api_data_' . substr(md5(uniqid()),0,8));
}

class WebComponent extends ClientComponent{
    private $Controller;
    private $Api = [];
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->Controller = $controller;
        $this->loadConfig();
    }
    
    public function jsonDump($format){
        $this->flowControl($format);
        $data = $this->getHttpOkResponse($format);
        $this->jsonDataDump($data['payload']);
    }
    
    public function jsonDataDump($data){
        $this->jsonResponse($data);
    }
    
    public function jsonResponse($data){
        ob_start();
        header("Content-Type: application/json");
        echo json_encode($data);
        ob_end_flush();
        exit;
    }
    
    public function rawFileDump($data){
        if(array_key_exists('type',$data['payload']) === false){            
            $this->raiseConfigureException('Invalid Payload<missing.key:type> in Api Config');
        }
        
        if(array_key_exists('size',$data['payload']) === false){            
            $this->raiseConfigureException('Invalid Payload<missing.key:size> in Api Config');
        }
        
        if(array_key_exists('file',$data['payload']) === false){            
            $this->raiseConfigureException('Invalid Payload<missing.key:file> in Api Config');
        }
        
        if(array_key_exists('blob',$data['payload']) === false){            
            $this->raiseConfigureException('Invalid Payload<missing.key:blob> in Api Config');
        }
        
        ob_start();
        
        $lastModified = gmdate('D, d M Y H:i:s ', $data['payload']['timestamp']) . 'GMT';
        
        header("Last-Modified: {$lastModified}");
        header("Content-Type: {$data['payload']['type']}");
        header("Content-Length: {$data['payload']['size']}");
        header("Content-Disposition: filename=\"{$data['payload']['file']}\"");
        echo base64_decode($data['payload']['blob']);
        ob_end_flush();
        exit;
    }
    
    public function fileDump($format){
        $this->flowControl($format);
        $data = $this->getHttpOkResponse($format);
        $this->rawFileDump($data);
    }
    
    public function getApi($key){
        return $this->Api[$key];
    }
    
    private function loadConfig(){
        Configure::load('klezapi','default',false);
        
        $confiles = Configure::read('Api.confiles');
        
        if(is_null($confiles)){            
            $this->raiseConfigureException('No Conf<Api:confiles> in Api Cofig');
        }
        
        if(is_array($confiles) === false){
            $confiles = [ $confiles ];
        }
        
        foreach($confiles as $confile){
            Configure::load($confile,'default',false);
        }
        
        $this->parseConf();
    }
    
    private function parseConf(){
        Configure::load('KlezBackend.required','default',false);
        $conf = Configure::read('Api');
        $required = Configure::read('Required.WebComponent.api');
        
        foreach($required as $api){        
            if(isset($conf[$api]) === false){
                $this->raiseConfigureException("No Conf<Api.$api> in Api Config");
            }
        }
        
        foreach($conf as $api => $url){
            $this->Api[$api] = $url;
        }
    }

    private function raiseConfigureException($message){
        CakeLog::error($message);
        throw new ConfigureException($message);
    }
    
    private function exception($status){
        http_response_code($status);
        
        switch($status){
            case 404:
                throw new NotFoundException();
            case 401:
                throw new UnauthorizedException();
            case 403:
                throw new ForbiddenException();
            case 500:
                throw new InternalErrorException();
            case 304:
                exit;
        }
        
        throw new ConfigureException();
    }
    
    protected function flowControl($format = false){
        $data = $this->getHttpOkResponse($format);
        
        if(isset($data['exception'])){
            $this->exception($data['exception']);
        }
        
        if(isset($data['flash'])){
            $this->flash($data['flash']);
        }
        
        if(isset($data['redirect'])){
            $this->redirect($data['redirect']);
        }
    }
    
    public function flash($flash){
        if(is_null($flash)){
            return;
        }
        
        if(isset($flash['kind']) === false){
            return;
        }
        
        if(isset($flash['message']) === false){
            return;
        }
        
        $kind = $flash['kind'];
        $message = $flash['message'];
        $this->Controller->flashToast($kind,$message);
    }
    
    private $forceRedirect = null;
    
    public function setForceRedirect($url){
        $this->forceRedirect = $url;
    }
    
    private function redirect($url){
        if(is_null($this->forceRedirect) === false){
            $this->Controller->redirect($this->forceRedirect);
            return;
        }
        
        if(is_null($url)){
            return;
        }
        
        $this->Controller->redirect($url);
    }
    
    protected function dump($format = false){
        $data = $this->getHttpOkResponse($format);
        $this->jsonResponse($data);
    }
    
    protected function feed($format = false){
        $this->flowControl($format);
        
        $data = $this->getHttpOkResponse($format);
        $this->Controller->set(API_DATA,$data);
    }
    
    protected function shared($format = false){
        $this->flowControl($format);
        
        $data = $this->getHttpOkResponse($format);
        $shared = $this->Controller->viewVars[API_DATA];
        $share = array_merge_recursive($shared, $data);
        $this->Controller->set(API_DATA,$share);
    }
    
    public function loginConf(){
        $this->setUrl($this->getApi('login_config'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->request();
        $this->feed('json');
    }
    
    public function login($data = []){
        $this->setUrl($this->getApi('login'));
        $this->setVerb('POST');
        $this->setData($data);
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function home(){
        $this->setUrl($this->getApi('home'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->request();
        $this->feed('json');
    }
    
    public function logout(){
        $this->setUrl($this->getApi('logout'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->request();
        $this->feed('json');
    }
    
    public function backend(){
        $this->setUrl($this->getApi('home'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->request();
        $this->feed('json');
    }
    
    protected function interceptRequest($conf){
        $response = $this->getHttpOkResponse('json');
        
        if(isset($response['payload']['success']) === false){
            return;
        }
        
        if($response['payload']['success'] === true){
            if(isset($conf['messages']['success'])){
                $this->flashRewrite($conf['messages']['success'], 'success', 'json');
            }
        }
        else if($response['payload']['success'] === false){  
            if(isset($conf['messages']['error'])){          
                $this->flashRewrite($conf['messages']['error'], 'error', 'json');
            }
        }
    }
}