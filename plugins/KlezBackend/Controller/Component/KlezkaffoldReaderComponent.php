<?php

App::uses('KlezkaffoldWebComponent', 'KlezBackend.Controller/Component');

class KlezkaffoldReaderComponent extends KlezkaffoldWebComponent{
    private $Controller;
    private $Data;
    
    public function getData(){
        return $this->Data;
    }
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function feed($format='json'){
        $this->Data = $this->getResponse($format);
    }
}
