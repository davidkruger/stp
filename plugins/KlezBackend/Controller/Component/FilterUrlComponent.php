<?php

App::uses('Component','Controller');

class FilterUrlComponent extends Component{
    public function filter($auth,$config){
        foreach($config as $field => $matching){
            if(isset($auth[$field]) === false){
                return false;
            }

            $value = $auth[$field];
            
            if(is_array($matching)){
                $algo = array_shift($matching);
                
                if($this->filterAlgo($algo,$value,$matching) === false){
                    return false;
                }
            }
            else if($this->filterMatch($value,$matching) === false){
                return false;
            }
        }
        
        return true;
    }
    
    private function filterAlgo($algo,$value,$matching){
        switch ($algo){
            case 'in_array': return $this->filterInArrayAlgo($value,$matching);
        }
        
        return false;
    }
    
    private function filterInArrayAlgo($value,$matching){
        return in_array($value, $matching, true);
    }
    
    private function filterMatch($value,$matching){
        return $value === $matching;
    }
}