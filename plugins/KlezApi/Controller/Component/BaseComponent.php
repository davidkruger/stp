<?php

App::uses('Component', 'Controller');
App::uses('CakeSession', 'Model/Datasource');

class BaseComponent extends Component{
    private static $LogHash = null;
    private $post = false;
    private $put = false;
    private $get = false;
    protected $Acl;
    
    public function initialize(\Controller $controller) {
        $this->Acl = $controller->Acl;
        $this->post = $controller->request->is('POST');
        $this->put = $controller->request->is('PUT');
        $this->get = $controller->request->is('GET');
        
        parent::initialize($controller);
    }
    
    protected function isGet(){
        return $this->get;
    }
    
    protected function isPut(){
        return $this->put;
    }
    
    protected function isPost(){
        return $this->post;
    }
    
    protected function isBinary($type){
        if(preg_match('/^image\//', $type)){
            return true;
        }
        
        return false;
    }
    
    public function getLogHash(){
        if(is_null(self::$LogHash)){
            $this->generateLogHash();
        }
        
        return self::$LogHash;
    }
    
    private function generateLogHash(){
        $uniqid = uniqid('',true);
        $md5 = md5($uniqid);
        self::$LogHash = substr($md5,0,8);
    }
    
    public function isUrlAllowed($url){
        return $this->Acl->isUrlAllowed($url);
    }
}
