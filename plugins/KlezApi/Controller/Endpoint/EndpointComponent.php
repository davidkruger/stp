<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');

abstract class EndpointComponent extends BaseComponent{    
    public function initialize(\Controller $controller) {
        $this->initializeLog();
        
        parent::initialize($controller);
    }
    
    private function initializeLog(){
        $file = 'klezendpoint';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'endpoint' ],
            'file' => $file
        ));    
    }
    
    protected function logendpoint($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'endpoint');
    }
    
    abstract function getContentType($format);
}