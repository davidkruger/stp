<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');

class DemoComponent extends EndpointComponent{
    private $data = [];
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    public function foobar(){
        $this->data['current_timestamp'] = time();
    }
    
    public function json(){
        return json_encode($this->data);
    }
    
    public function png(){
        $font = WWW_ROOT . '../../plugins/KlezApi/webroot/fonts/OpenSans-Bold.ttf';
        $im = imagecreatetruecolor(300, 300);
        $color = imagecolorallocate($im, 0x30, 0x50, 0x74);
        imagerectangle($im, 10, 10, 290, 290, $color);
        imagettftext($im, 20, 0, 20, 40, $color, $font, 't = ' . $this->data['current_timestamp']);
        
        ob_start();
        imagejpeg($im); 
        $data = ob_get_clean();  
        imagedestroy($im);
        
        return $data;
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
            case 'png':
                return 'image/png';
        }
        
        return 'text/plain';
    }
}
