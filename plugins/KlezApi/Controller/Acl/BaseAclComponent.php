<?php

abstract class BaseAclComponent extends Component {
    private $Auth;
    
    public function initialize(\Controller $controller) {
        $this->Auth = $controller->Auth;
        return parent::initialize($controller);
    }

    public function getAuth(){
        return $this->Auth;
    }
    
    protected function raiseConfigureException($message){
        $this->Auth->logauth($message);
        throw new ConfigureException($message);
    }

    abstract function control($ctrl);

    public function isUrlAllowed($url) {
        $this->loadFilters();
        $filters = $this->resolvUrlFilters($url);
        $allowed = true;
        
        if(isset($filters['filters'])){
            $filters = $filters['filters'];
            
            if($this->aclFilters($filters) === false){
                $allowed = false;
            }
        }
        
        $purl = "{$url['controller']}.{$url['action']}";
        
        if($allowed){            
            $this->Auth->logauth("Acl<alllowed:YES,url:{$purl}>");
        }
        else{
            $this->Auth->logauth("Acl<alllowed:NO,url:{$purl}>");            
        }
        
        return $allowed;
    }
    
    public function aclFilters($filters){
        $allowed = true;
        
        if(isset($filters['data'])){
            $allowed = $this->aclFiltersData($filters);
        }
        
        return $allowed;
    }
    
    public function aclFiltersData($filters){
        $data = $this->getAuth()->getData();
        
        if(isset($filters['data']) === false){
            return null;
        }
                
        foreach($filters['data'] as $field => $value){
            if(array_key_exists($field,$data['data']) === false){
                return false;
            }
            
            $enitityValue = $data['data'][$field];
            
            if(is_array($value)){
                return $this->aclFiltersArray($enitityValue,$value);
            }
            else if(is_bool($enitityValue)){
                if($value != $enitityValue){
                    return false;
                }
            }
            else if(strcmp($enitityValue,$value) !== 0){
                return false;
            }
        }
        
        return true;
    }
    
    private function aclFiltersArray($entitiyValue,$array){
        $op = Inflector::camelize($array[0]);
        $method = "aclFiltersArray{$op}";
        unset($array[0]);
        
        if(method_exists($this, $method) === false){
            return false;
        }
        
        return $this->{$method}($entitiyValue,$array);
    }
        
    private function aclFiltersArrayInArray($entitiyValue,$array){
        return in_array($entitiyValue, $array);
    }
    
    private function aclFiltersArrayNotNull($entitiyValue,$array){
        return !is_null($entitiyValue);
    }
    
    private function resolvUrlFilters($url){
        $ctrl = $url['controller'];
        $actn = $url['action'];
        
        $key = "Filter.{$ctrl}.{$actn}";
        $conf = Configure::read($key);
        return $conf;
    }
    
    private $filtersLoaded = false;
    
    public function loadFilters(){
        if($this->filtersLoaded === false){
            Configure::load('klezfilters');

            $confiles = Configure::read('Filter.confiles');

            if(is_null($confiles)){            
                $this->raiseConfigureException('No Conf<Filter:confiles> in Filter Config');
            }

            if(is_array($confiles) === false){
                $confiles = [ $confiles ];
            }

            foreach($confiles as $confile){
                Configure::load($confile,'default',false);
            }
            
            $this->filtersLoaded = true;
        }
    }
}