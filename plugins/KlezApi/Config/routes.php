<?php

Router::connect('/api/:endpoint.:format', 
    [ 'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi' ],
    [ 'endpoint', 'format' ]
);