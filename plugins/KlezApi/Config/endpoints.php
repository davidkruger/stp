<?php

$config = [];

$config['Endpoint.Demo'] = [
    'component' => 'DemoComponent',
    'path' => 'KlezApi.Controller/Endpoint',
    'method' => 'foobar',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'acl' => [ 
        'auth' => false, 
        'control' => 'none',
        'component' => 'DemoAclComponent',
        'path' => 'KlezApi.Controller/Acl',
    ]
];