<?php

App::uses('ClientComponent', 'KlezApi.Controller/Component');

class FcmComponent extends ClientComponent{
    private $url;
    private $verb;
    
    private function initializeLog(){
        $file = 'fcm';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'apicli' ],
            'file' => $file
        ));    
    }
    
    public function __construct($collection) {
        parent::__construct($collection);
        
        $this->url = 'https://fcm.googleapis.com/fcm/send';
        $this->verb = 'POST';
        $this->initializeLog();
    }
    
    public function push($serverkey,$payload = []){
        Configure::write('BasicAuth.enabled',false);
        $this->putHeader('Authorization', "key={$serverkey}");
        $this->setUrl($this->url);
        $this->setVerb($this->verb);
        $this->setData($payload);
        $this->jsonPayload();
    }
    
    public function getVerb() {
        return $this->verb;
    }
    
    public function getUrl() {
        return $this->url;
    }
}