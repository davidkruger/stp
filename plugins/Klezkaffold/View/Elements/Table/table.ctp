<div class="col-sm-12">
    <div class="card-box table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">            
            <?php $this->Table->tableHead($schema); ?>
            <?php $this->Table->tableBody($schema,$data); ?>
        </table>
    </div>
</div>]

<script>
    $(function(){
        $('#datatable-responsive').dataTable({
            ordering: true,
            paging: false,
            bInfo : false,
            language: {
                search: <?=json_encode($this->Backend->feed('search.label'))?>,
                zeroRecords: <?=json_encode($this->Backend->feed('search.empty'))?>,
            },
            columnDefs: [{
                orderable: false,
                targets: "no-sort"
            }]
        });    
    });
</script>