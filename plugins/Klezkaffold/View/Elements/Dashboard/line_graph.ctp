<a class="dashboard-link" href="<?=$this->Dashboard->feedUrl('url')?>">
    <div class="col-lg-12 col-md-6">
        <div class="card-box">
            <h4 class="header-title m-t-0"><?=$this->Dashboard->feed('title')?></h4>
            <div class="widget-chart text-center">
                
                <canvas id="<?=$name?>_gfx" height="400"></canvas>
            </div>   
        </div>             
    </div>
</a>

<script>
$(function(){
    "use strict";
    
    var graph = <?= json_encode($this->Dashboard->feed('graph'))?>;
    var labels = <?= json_encode($this->Dashboard->feed('labels'))?>;
    var series = <?= json_encode($this->Dashboard->feed('series'))?>;
    var datasets = [];
    
    for(var i in series){
        datasets[i] = series[i].options;
        datasets[i].label = series[i].name;
        datasets[i].data = series[i].data;
    }

    var ChartJs = function() {};

    ChartJs.prototype.respChart = function(selector,type,data, options) {
        var ctx = selector.get(0).getContext("2d");
        var container = $(selector).parent();        
        $(window).resize( generateChart );

        function generateChart(){
            var ww = selector.attr('width', $(container).width() );
            
            switch(type){
                case 'Line':
                    new Chart(ctx, {type: 'line', data: data, options: options});
                    break;
                case 'Doughnut':
                    new Chart(ctx, {type: 'doughnut', data: data, options: options});
                    break;
                case 'Pie':
                    new Chart(ctx, {type: 'pie', data: data, options: options});
                    break;
                case 'Bar':
                    new Chart(ctx, {type: 'bar', data: data, options: options});
                    break;
                case 'Radar':
                    new Chart(ctx, {type: 'radar', data: data, options: options});
                    break;
                case 'PolarArea':
                    new Chart(ctx, {data: data, type: 'polarArea', options: options});
                    break;
            }

        };
        generateChart();
    },
            
    ChartJs.prototype.init = function() {
        var lineChart = {
            labels: labels,
            datasets: datasets
        };

        var lineOpts = graph;
        this.respChart($("#<?=$name?>_gfx"),'Line',lineChart, lineOpts);
    },
            
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs;
    $.ChartJs.init();
});


</script>