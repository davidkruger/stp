<a class="dashboard-link" href="<?=$this->Dashboard->feedUrl('url')?>">
    <div class="col-lg-12 col-md-6">
        <div class="card-box">
            <h4 class="header-title m-t-0"><?=$this->Dashboard->feed('data.title')?></h4>
            <div class="widget-chart text-center">

                <canvas id="<?=$name?>_gfx" height="400"></canvas>
            </div>   
        </div>             
    </div>
</a>

<script>
$(function(){
    "use strict";
    
    var graph = <?= json_encode($this->Dashboard->feed('data.graph'))?>;
    var data = <?= json_encode($this->Dashboard->feed('data.top'))?>;
    var options = <?= json_encode($this->Dashboard->feed('options',false))?>;
    
    
    var labels = [];
    var datasets = [];
    
    if(options !== null){
        datasets[0] = options;
    }
    else{
        datasets[0] = {};
    }
    
    datasets[0].data = [];
    datasets[0].label = <?= json_encode($this->Dashboard->feed('label'))?>;
    
    for(var i in data){
        var serie = data[i];
        labels.push(serie.label);
        datasets[0].data.push(parseFloat(serie.value));
    }

    var ChartJs = function() {};

    ChartJs.prototype.respChart = function(selector,type,data, options) {
        var ctx = selector.get(0).getContext("2d");
        var container = $(selector).parent();        
        $(window).resize( generateChart );

        function generateChart(){
            var ww = selector.attr('width', $(container).width() );
            
            switch(type){
                case 'Line':
                    new Chart(ctx, {type: 'line', data: data, options: options});
                    break;
                case 'Doughnut':
                    new Chart(ctx, {type: 'doughnut', data: data, options: options});
                    break;
                case 'Pie':
                    new Chart(ctx, {type: 'pie', data: data, options: options});
                    break;
                case 'Bar':
                    new Chart(ctx, {type: 'bar', data: data, options: options});
                    break;
                case 'HorizontalBar':
                    new Chart(ctx, {type: 'horizontalBar', data: data, options: options});
                    break;
                case 'Radar':
                    new Chart(ctx, {type: 'radar', data: data, options: options});
                    break;
                case 'PolarArea':
                    new Chart(ctx, {data: data, type: 'polarArea', options: options});
                    break;
            }

        };
        generateChart();
    },
            
    ChartJs.prototype.init = function() {
        var barChart = {
            labels: labels,
            datasets: datasets
        };

        var lineOpts = graph;
        this.respChart($("#<?=$name?>_gfx"),'HorizontalBar',barChart, lineOpts);
    },
            
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs;
    $.ChartJs.init();
});
</script>