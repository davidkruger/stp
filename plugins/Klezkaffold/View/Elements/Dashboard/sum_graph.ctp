<a class="dashboard-link" href="<?=$this->Dashboard->feedUrl('url')?>">
    <div class="col-lg-12 col-md-6">
        <div class="card-box">
            <h4 class="header-title m-t-0"><?=$this->Dashboard->feed('title')?></h4>
            <div class="widget-chart text-center">
                <div id="<?=$name?>_gfx" style="height:400px;" class="ct-chart"></div>
            </div>   
        </div>             
    </div>
</a>

<script>
    var labels = <?= json_encode($this->Dashboard->feed('labels'))?>;
    var series = <?= json_encode($this->Dashboard->feed('series'))?>;
    
    var chart = new Chartist.Line('#<?=$name?>_gfx', {
        labels: labels,
        series: series,
    },{
        low: 0,
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ],
        axisY: {
            onlyInteger: true
        }
    });
  
    var seq = 0, delays = 80, durations = 500;

    chart.on('created', function() {
        seq = 0;
    });

    chart.on('draw', function(data) {
        seq++;

        if(data.type === 'line') {
            data.element.animate({
                opacity: {
                    begin: seq * delays + 1000,
                    dur: durations,
                    from: 0,
                    to: 1
                }
            });
        } 
        else if(data.type === 'label' && data.axis === 'x') {
            data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    easing: 'easeOutQuart'
                }
            });
        } 
        else if(data.type === 'label' && data.axis === 'y') {
            data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
            });
        } 
        else if(data.type === 'point') {
            data.element.animate({
                x1: {
                  begin: seq * delays,
                  dur: durations,
                  from: data.x - 10,
                  to: data.x,
                  easing: 'easeOutQuart'
                },
                x2: {
                  begin: seq * delays,
                  dur: durations,
                  from: data.x - 10,
                  to: data.x,
                  easing: 'easeOutQuart'
                },
                opacity: {
                  begin: seq * delays,
                  dur: durations,
                  from: 0,
                  to: 1,
                  easing: 'easeOutQuart'
                }
            });
        }
        else if(data.type === 'grid') {
            var pos1Animation = {
              begin: seq * delays,
              dur: durations,
              from: data[data.axis.units.pos + '1'] - 30,
              to: data[data.axis.units.pos + '1'],
              easing: 'easeOutQuart'
            };

            var pos2Animation = {
              begin: seq * delays,
              dur: durations,
              from: data[data.axis.units.pos + '2'] - 100,
              to: data[data.axis.units.pos + '2'],
              easing: 'easeOutQuart'
            };

            var animations = {};
            animations[data.axis.units.pos + '1'] = pos1Animation;
            animations[data.axis.units.pos + '2'] = pos2Animation;
            animations['opacity'] = {
              begin: seq * delays,
              dur: durations,
              from: 0,
              to: 1,
              easing: 'easeOutQuart'
            };

            data.element.animate(animations);
        }
    });
</script>