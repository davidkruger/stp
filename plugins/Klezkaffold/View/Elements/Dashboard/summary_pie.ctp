<a class="dashboard-link" href="<?=$this->Dashboard->feedUrl('url')?>">
    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <h4 class="header-title m-t-0 m-b-30"><?=$this->Dashboard->feed('title')?></h4>

            <div class="widget-chart-1">
                <div class="widget-chart-box-1">
                    <input
                        autocomplete="off"
                        value="<?=(int)$this->Dashboard->feed('data.perc')?>"
                        data-plugin="knob"
                        data-width="80"
                        data-height="80" 
                        data-fgColor="<?=$this->Dashboard->feed('fgcolor')?>"
                        data-bgColor="<?=$this->Dashboard->feed('bgcolor')?>"
                        data-min="0"
                        data-max="100"
                        data-skin="tron" 
                        data-angleArc="360" 
                        data-angleOffset="0"
                        data-readOnly=true
                        data-thickness=".15"/>
                </div>
                <div class="widget-detail-1">
                    <h2 class="p-t-10 m-b-0" data-plugin="counterup"> <?=$this->Dashboard->feed('data.count')?> </h2>
                    <p class="text-muted"><?=$this->Dashboard->feed('text')?></p>
                </div>
            </div>
        </div>
    </div>
</a>