<div class="input-group m-t-10">
    <input 
        type="text"
        readonly="readonly"
        placeholder="<?=$label?>" 
        class="form-control"
        value="<?=$value?>"
        name="<?=$field?>">
    
    <input style="display:none;" type="text" name="<?=$lat?>" />
    <input style="display:none;" type="text" name="<?=$lon?>" />
    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
</div>

<script>
    $(function(){
        var field = $('input[name="<?=$field?>"]');
        var trigger = field.parent();
        var lat = $('input[name="<?=$lat?>"]');
        var lon = $('input[name="<?=$lon?>"]');
        var modal = $('#<?=$field?>-gmap-modal');
        var button = modal.find('button.btn');
        
        function getLat(){
            var v = lat.val();
            
            if(v === ''){
                return <?=json_encode($defaultLat); ?>;
            }
            
            return v;
        }
        
        function getLon(){
            var v = lon.val();
            
            if(v === ''){
                return <?=json_encode($defaultLon); ?>;
            }
            
            return v;
        }
        
        function field2coords(){
            var v = field.val();
            
            if(v === '') return;
            
            var coords = v.split(';');
            lat.val(coords[0]);
            lon.val(coords[1]);
        }
        
        trigger.off('click').on('click',function(e){
            e.preventDefault();
            modal.modal('show');
            
            if(typeof marker === 'undefined') return;
            
            var position = new google.maps.LatLng(getLat(), getLon());
            marker.setPosition(position);
        });
        
        button.off('click').on('click',function(e){
            e.preventDefault();
            modal.modal('hide');
            
            if(typeof marker === 'undefined') return;
            
            lon.val(marker.getPosition().lng());
            lat.val(marker.getPosition().lat());
            field.val(lat.val() + ';' + lon.val());
        });
        
        trigger.find('*').css({
            'cursor' : 'pointer'
        });
        
        var map;
        var marker;
        
        function loadMap(){
            var location = getLocation();

            var opts = {
                center: location,
                scrollwheel: false,
                zoom: <?=json_encode($zoom)?>
            };
            
            map = new google.maps.Map(document.getElementById('<?=$field?>-map'), opts);
            
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable:true
            });
            
            map.addListener('click', function(e) {
                marker.setPosition(e.latLng);
            });
        }
        
        modal.on('show.bs.modal', function() {
            setTimeout(function(){
                resizeMap();
            }, 1000);
        });
         
        function resizeMap() {
            if(typeof map === "undefined") return;
            
            google.maps.event.trigger(map, "resize");
            map.setCenter(getLocation()); 
        }
        
        function getLocation(){
            return {
                lat: parseFloat(getLat()), 
                lng: parseFloat(getLon())
            }
        }
        
        google.maps.event.addDomListener(window, "resize", resizeMap());
        google.maps.event.addDomListener(window, "load", loadMap());
        field2coords();
    });
</script>

<div id="<?=$field?>-gmap-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?=$label?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div style="height:400px;" id="<?=$field?>-map"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect waves-light">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=$key?>"></script>
