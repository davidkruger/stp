<script>
    $(function(){
        var master = <?=json_encode($master);?>;
        var dep = <?=json_encode($dep);?>;
        var control = $('[name="' + master + '"]');
        var holders = {};
        
        console.debug(dep)

        for(var group in dep){
            for(var i in dep[group]){
                var field = dep[group][i];
                var c = $("[name='" + field + "']");
                var holder;
                
                if(c.length === 0){
                    holder = $('#holder_for_' + field);
                }
                else{
                    holder = c.closest('#holder_for_' + field);
                }
                
                holders[field] = holder;
            }
        }
        
        console.debug(holders)

        control.off('change').on('change',function(){
            var v = $(this).val();
            var hideBuffer = {};
            var showBuffer = {};
            
            for(var g in dep){
                for(var i in dep[g]){
                    var f = dep[g][i];
                    
                    if(g == v){
                        showBuffer[f] = i;
                    }
                    else{
                        hideBuffer[f] = i;
                    }
                }
            }
            
            for(var f in hideBuffer){
                holders[f].hide();
            }
            
            for(var f in showBuffer){
                holders[f].show();
            }
            
        }).trigger('change');
    });
</script>