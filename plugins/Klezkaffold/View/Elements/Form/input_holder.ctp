<div class="col-lg-<?=$weight?> form-holder" id="holder_for_<?=$field?>">
    <div class="panel panel-color panel-inverse">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$label?></h3>
        </div>
        <div class="panel-body">
            <p>
                <?php $this->Form->input($schema,$data,$field); ?>
                <?php $this->Form->messages($field,$messages); ?>
            </p>
        </div>
    </div>
</div>