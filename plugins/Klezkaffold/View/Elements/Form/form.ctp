<form method="post" action="<?=$this->Form->resolvCurrentUrl(); ?>">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <?php $this->Form->inputs($schema, $data, $messages); ?>
                </div>
            </div>             
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php $this->Form->submit(); ?>
                        </div>
                    </div>
                </div>   
            </div>             
        </div>
    </div>
</form>