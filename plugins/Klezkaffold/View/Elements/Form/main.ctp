<div class="form-main">
    <?php $this->Form->main($config); ?>
</div>

<script>
    $(function(){

        <?php
        if(isset($config['data']['group_id']) && $config['data']['group_id'] == 20){
        ?>
        $('#holder_for_minimal').fadeIn();
        $('#holder_for_amount').fadeIn();
        $('#holder_for_interval').fadeIn();
        <?php
        }else{
        ?>
        $('#holder_for_amount').fadeOut();
        $('#holder_for_minimal').fadeOut();
        $('#holder_for_interval').fadeOut();
        <?php
        }
        ?>
        
        var holders = $('.form-holder').not('.col-lg-12');
        var mh = 0;
        
        holders.each(function(){
            var h = $(this).height()
            
            if(h > mh){
                mh = h;
            }
        });
        
        holders.each(function(){
            $(this).height(mh);
        });

        $('input[name ="group_id"]').change(function() {

            if( $( this ).val()==20 ){
                $('#holder_for_amount').fadeIn();
                $('#holder_for_minimal').fadeIn();
                $('#holder_for_interval').fadeIn();
            }else{
                $('#holder_for_amount').fadeOut();
                $('#holder_for_minimal').fadeOut();
                $('#holder_for_interval').fadeOut();
            }
        });
    });
</script>