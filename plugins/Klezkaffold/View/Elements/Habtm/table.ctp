<form method="post" action="<?=$this->Form->resolvCurrentUrl(); ?>">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="mainTable" class="table table-striped m-b-0">
                <thead>
                    <tr>
                        <th><?=$label['label']?></th>
                        <?php $this->Habtm->head($schema);?>
                    </tr>
                </thead>
                <tbody>
                    <?php $this->Habtm->body($schema,$data,$label);?>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <?php $this->Habtm->submit(); ?>
                            </div>
                        </div>
                    </div>   
                </div>             
            </div>
        </div>
    </div>
</form>

<script>
    $(function(){
        $('#mainTable').dataTable({
            ordering: true,
            paging: true,
            bInfo : false,
            language: {
                search: <?=json_encode($this->Backend->feed('search.label'))?>,
                zeroRecords: <?=json_encode($this->Backend->feed('search.empty'))?>,
            },
            columnDefs: [{
                orderable: false,
                targets: "no-sort"
            }]
        });    
    });
</script>