<script>
    var schema = <?=  json_encode($config['schema'])?>;
    var currentOrder = <?=  json_encode($config['order'])?>.split(' '); 
    var orderables = [];
    var currentOrderField = null;
    var currentOrderDir = null;
    
    if(typeof currentOrder[0] !== 'undefined'){
        currentOrderField = currentOrder[0];
    }
    
    if(typeof currentOrder[1] !== 'undefined'){
        currentOrderDir = currentOrder[1].toLowerCase();
    }
    
    for(var field in schema){
        var meta = schema[field];
        
        if(typeof meta.orderable !== 'undefined'){
            if(meta.orderable === true){
                orderables.push(field);
            }
        }
    }    
    
    $(function(){
        var query = window.location.href
                        .replace(/^((.*\?)|(.*))/,'')
                        .replace(/\#(.*)$/,'');

        var vars = query.split('&');
        var params = {};

        for(var i in vars){
            var pair = vars[i].split('=');

            if(pair.length !== 2){
                continue;
            }

            var k = pair[0];
            var v = decodeURIComponent(pair[1]).replace(/\+/g, ' ');
            params[k] = v;
        }
        
        if(typeof schema[currentOrderField] === 'undefined'){
            currentOrderField = params.of;
        }
        
        for(var i in orderables){
            var field = orderables[i];
            var th = $("#th_for_" + field);
            var thClass = 'sorting';
            
            if(currentOrderField === field){
                if(currentOrderDir === 'asc'){
                    thClass = 'sorting_asc';
                }
                else if(currentOrderDir === 'desc'){
                    thClass = 'sorting_desc';
                }
            }
                        
            th.attr('data-field',field);
            th.addClass(thClass);
            
            th.off('click').on('click',function(){
                var dir;
                
                $(this).siblings('.sorting_desc').removeClass('sorting_desc').addClass('sorting');
                $(this).siblings('.sorting_asc').removeClass('sorting_asc').addClass('sorting');

                if($(this).hasClass('sorting_desc')){
                    $(this).removeClass('sorting sorting_desc').addClass('sorting_asc');
                    dir = 'asc';
                }
                else{
                    $(this).removeClass('sorting sorting_asc').addClass('sorting_desc');
                    dir = 'desc';
                }
                
                params["of"] = $(this).attr('data-field');
                params["od"] = dir;
                
                var url =  window.location.href
                                .replace(/(\?.*)$/,'')
                                .replace(/\#(.*)$/,'')
                                + '?'
                                + jQuery.param(params);
                        
                window.location.href = url;
            });
        }
    });
</script>

<div class="row">
    <?php $this->Show->main($config); ?>
</div>

<?php $this->Show->actionsHolder($config); ?>