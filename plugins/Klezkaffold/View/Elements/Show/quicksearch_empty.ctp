<form style="margin-top:10px !important;" id="quicksearch" method="get" class="form-inline" action="<?=$this->Show->resolvCurrentUrl()?>">
    <div class="row">
        <div class="col-md-12">
            <div id="datatable_filter" class="dataTables_filter">
                <label style="margin-bottom:0px !important;"><?=$this->Backend->feed('search.label')?>: <input value="<?=$this->Show->getSearchTerm()?>" class="form-control input-sm" name="q" placeholder="" aria-controls="datatable" type="search"></label>
            </div>
        </div>
    </div>
</form>