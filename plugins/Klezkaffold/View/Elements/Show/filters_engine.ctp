<script>
    var ShowFiltersEngineHooks = [];
    
    $(function(){
        var query = window.location.href
                        .replace(/^((.*\?)|(.*))/,'')
                        .replace(/\#(.*)$/,'');

        var vars = query.split('&');
        var params = {};
        
        for(var i in vars){
            var pair = vars[i].split('=');

            if(pair.length !== 2){
                continue;
            }

            var k = pair[0];
            var v = pair[1];
            params[k] = v;
        }
                
        $('#quicksearch').off('submit').on('submit',function(e){
            e.preventDefault();
            
            for(var i in ShowFiltersEngineHooks){
                var fn = ShowFiltersEngineHooks[i];
                params = fn($(this), params);
            }
        
            var url =  window.location.href
                            .replace(/(\?.*)$/,'')
                            .replace(/\#(.*)$/,'')
                            + '?'
                            + jQuery.param(params);
            
            window.location.href = url;
            return false;
        });
        
        $('#quicksearch').find('select[name="lim"]').off('change').on('change', function(){
            $('#quicksearch').submit();
        });
        
        $('#quicksearch').find('input[name="q"]').off('keyup').on('keyup', function(e){
            if(e.keyCode === 13){
                $('#quicksearch').submit();
            }
        });
    });
</script>