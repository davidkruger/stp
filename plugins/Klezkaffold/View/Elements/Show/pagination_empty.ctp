<div class="col-sm-12">
    <div class="card-box table-responsive">
        <div class="row">
            <div class="col-sm-3">
                <div class="dataTables_info" id="datatable-responsive_info" role="status" aria-live="polite">
                    <?=$this->Show->resolvPaginationSummary($count,$c)?>
                </div>            
            </div>
            <div class="col-sm-9">
                <div class="dataTables_paginate paging_simple_numbers" id="datatable-responsive_paginate">
                    <ul style="margin-bottom:0px!important;" class="pagination">
                        <?php $this->Show->backButton($page,$count,$limit)?>
                        <?php $this->Show->numbers($page,$count,$limit)?>
                        <?php $this->Show->nextButton($page,$count,$limit)?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>