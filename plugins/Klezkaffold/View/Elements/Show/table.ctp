<div class="col-sm-12">
    <div class="card-box table-responsive">
        <?php $this->Show->filters($schema)?>
        
        <form method="post" action="<?=$this->Show->checkUrl('check')?>">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">            
                <?php $this->Show->tableHead($schema,$data[0]['links']); ?>
                <?php $this->Show->tableBody($schema,$data); ?>
            </table>
            <?php $this->Show->checkSubmit('check'); ?>
        </form>
        
        <div class="row">
            <?php $this->Show->pagination($page,$count,$c,$limit); ?>
        </div>
        
        <style>
            .resizer {
            position: absolute;
            top: 0;
            right: -8px;
            bottom: 0;
            left: auto;
            width: 16px;    
            cursor: col-resize;       
          }
        </style>
        <script>
            $(function(){
                $("table.dataTable td, table.dataTable th:not(.check-cell)")
                    .css({
                      /* required to allow resizer embedding */
                      position: "relative"
                    })
                    /* check .resizer CSS */
                    .prepend("<div class='resizer'></div>")
                    .resizable({
                      resizeHeight: false,
                      // we use the column as handle and filter
                      // by the contained .resizer element
                      handleSelector: "",
                      onDragStart: function(e, $el, opt) {
                        // only drag resizer
                        if (!$(e.target).hasClass("resizer"))
                          return false;
                        return true;
                      }
                });

                $('#check_all').change(function(){
                	$('input[name"check[]"]').prop('checked', $(this).prop("checked"));
                	console.log('hizo click');
                })
            })
        </script>
    </div>
</div>