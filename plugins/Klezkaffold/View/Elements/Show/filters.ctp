<form id="quicksearch" method="get" class="form-inline" action="<?=$this->Show->searchUrl()?>">
    <div class="row">
        <?php $this->Show->filtersEngine(); ?>
        <?php $this->Show->quickSearch($schema); ?>
        <?php $this->Show->limits($schema); ?>
        <?php $this->Show->customFiltersView(); ?>
    </div>
</form>