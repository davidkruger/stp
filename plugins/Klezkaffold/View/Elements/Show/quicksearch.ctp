<div class="col-md-<?=$this->Show->weight('search.weight',12)?>" style="text-align:<?=$this->Show->align('search.align','left')?>">
    <div id="datatable_filter" class="">
        <label><?=$this->Backend->feed('search.label')?>: <input value="<?=$this->Show->getSearchTerm()?>" class="form-control input-sm" name="q" placeholder="" aria-controls="datatable" type="search"></label>
    </div>
</div>