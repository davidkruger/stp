<div class="col-md-<?=$this->Show->weight('limits.weight',12)?>" style="text-align:<?=$this->Show->align('limits.align','left')?>">
    <div id="datatable_filter" class="">
        <label><?=$this->Backend->feed('limits.label')?>: 
            <select class="form-control input-sm" name="lim" placeholder="" aria-controls="datatable" type="search">
                <?php $this->Show->limOptions($this->Backend->feed('limits.limits')); ?>
            </select>
            <?=$this->Backend->feed('limits.after',false)?>
        </label>
    </div>
</div>