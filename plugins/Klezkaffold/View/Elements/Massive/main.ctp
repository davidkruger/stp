<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="header-title m-t-0 m-b-0"><?=$this->Backend->feed('confirm.title')?></h4>
            <p class="text-muted font-13 m-b-15">Las filas con errores no se procesarán.</p>
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                        <th>&nbsp;</th>
                        <?php $this->Massive->thead($map)?>
                    </thead>
                    <tbody>
                        <?php $this->Massive->rows($massive,$map)?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<form id="massiveform" method="post">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php $this->Massive->confirm(); ?>
                        </div>
                    </div>
                </div>   
            </div>             
        </div>
    </div>
</form>