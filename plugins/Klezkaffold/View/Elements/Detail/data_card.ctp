<div class="data-card-<?=$field?> col-lg-<?=$weight?>">
    <div class="panel panel-color panel-inverse">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$label?></h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-12"><?php $this->Detail->printValue($value,$meta); ?></div>
        </div>
    </div>
</div>