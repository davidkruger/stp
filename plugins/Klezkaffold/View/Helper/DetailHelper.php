<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class DetailHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function printValue($value,$meta){
        if(is_array($value)){
            if(empty($value)){
                echo $this->resolvBlankValue($meta);
            }
            else{
                foreach($value as $v){
                    echo $this->element('array_val',[
                        'value' => $v,
                    ]);
                }
            }
        }
        else{
            echo $value;
        }
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Detail:data> in DetailHelper");
        }
        
        if(isset($config['links']) === false){
            $this->raiseInternalServerError("No Conf<Detail:links> in DetailHelper");
        }
        
        if(isset($config['schema']) === false){
            $this->raiseInternalServerError("No Conf<Detail:schema> in DetailHelper");
        }
        
        echo $this->element('detail', $config);
    }
    
    private $buffer = [];
    
    public function dataCards($schema,$data){
        $x = count($this->buffer);
        $this->buffer[] = $schema;
        $i = -1;
        
        foreach($this->buffer[$x] as $field => $meta){
            $i++;
        
            if(isset($this->buffer[$x][$field]) === false){
                continue;
            }
            
            if($this->isReadable($meta) === false){
                continue;
            }
            
            if($this->isGroup($meta) === true){
                $this->group($this->buffer[$x],$data,$field,$x);
                $keys = array_keys($this->buffer[$x]);
                $last = count($keys) - 1;
                
                if($i <= $last){
                    $this->groupEnd();
                }
        
                continue;
            }
            
            $value = $this->resolvValue($field, $meta, $data);
            $label = $this->resolvLabel($meta);
            $weight = $this->resolvWeight($meta);
            
            echo $this->element('data_card',[
                'meta' => $meta,
                'value' => $value,
                'label' => $label,
                'field' => $field,
                'weight' => $weight
            ]);            
        }
    }
    
    private function groupDep($master,$dep,$value){
        echo $this->element('group_dep',[
            'dep' => $dep,
            'master' => $master,
            'value' => $value
        ]);
    }
    
    private function groupEnd(){
        echo $this->element('group_end');
    }
    
    private function groupStart($title,$master){
        echo $this->element('group_start',[
            'title' => $title,
            'master' => $master
        ]);
    }
    
    private $groupBuffer = null;
    
    private function group($schema,$data,$master,$x){
        if(is_null($this->groupBuffer)){
            $this->groupBuffer = $schema;
        }
        
        $meta = $schema[$master];
        
        $groupMeta = $meta['group'];
        $title = $groupMeta['title'];
        $fields = $groupMeta['fields'];
        $groupSchema = [];
        
        if(isset($groupMeta['dep'])){
            $this->groupDep($master,$groupMeta['dep'],$this->resolvRawValue($master, $data));
        }
        
        $this->groupStart($title,$master);
        $groupSchema[$master] = $schema[$master];
        unset($groupSchema[$master]['group']);
        
        foreach($fields as $field){
            $groupSchema[$field] = $this->groupBuffer[$field];
            unset($this->buffer[$x][$field]);
        }
        
        $this->dataCards($groupSchema,$data);
        unset($this->buffer[$x][$master]);
    }

    public function provideLogtag() {
        return "Detail";
    }
}