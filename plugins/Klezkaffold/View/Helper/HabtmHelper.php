<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class HabtmHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Habtm:data> in HabtmHelper");
        }
        
        if(isset($config['schema']) === false){
            $this->raiseInternalServerError("No Conf<Habtm:schema> in HabtmHelper");
        }
        
        if(isset($config['label']) === false){
            $this->raiseInternalServerError("No Conf<Habtm:label> in HabtmHelper");
        }
        
        if(empty($config['data'])){
            echo $this->element('empty');
        }
        else{
            echo $this->element('table', $config);
        }
    }
    
    public function head($schema){
        foreach($schema as $meta){
            if($this->isWritable($meta)){
                echo $this->element('head', $meta);
            }
        }
    }
    
    public function body($schema,$data,$label){
        foreach($data as $row){
            echo $this->element('row', [
                'data' => $row,
                'schema' => $schema,
                'label' => $label
            ]);
        }
    }
    public function submit(){
        echo $this->element('submit');
    }
    
    public function row($schema,$data,$label){
        $labelfield = $label['field'];
        $pkey = $label['pkey'];
        
        echo $this->element('labelcell', [
            'value' => $data[$labelfield]
        ]);
            
        foreach($schema as $field => $meta){
            if($this->isWritable($meta)){
                switch ($meta['type']){
                    case 'options':
                        $view = 'cell_options';
                        $value = $this->resolvRawValue($field, $data);
                        break;
                    default :
                        $view = 'cell';
                        $value = $this->resolvValue($field, $meta, $data);
                        break;
                }
                
                echo $this->element($view, [
                    'field' => $field,
                    'meta' => $meta,
                    'id' => $data[$pkey],
                    'value' => $value
                ]);
            }
        }
    }
    
    public function cellOptions($meta,$value){
        foreach($meta['options'] as $v => $f){
            echo $this->element('cell_option', [
                'value' => $v,
                'label' => $f,
                'selected' => ( $value === $v ? 'selected' : '' )
            ]);
        }
    }

    public function provideLogtag() {
        return 'Habtm';
    }

}