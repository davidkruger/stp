<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class ShowHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function searchUrl(){
        $this->params->query['p'] = 1;
        $query = Router::queryString($this->params->query);  
        
        $here = $this->_View->here;
        $url = Router::url($here,true) . $query;
        
        return $url;
    }
    
    public function align($search,$defa){
        $align = $this->_View->Backend->feed($search,false);
        
        if(!$align){
            $align = $defa;
        }
        
        return $align;
    }
    
    public function weight($search,$defa){
        $w = $this->_View->Backend->feed($search,false);
        
        if(!$w){
            $w = $defa;
        }
        
        return $w;
    }
    
    public function getSearchTerm(){
        if(isset($this->params->query['q'])){
            return $this->params->query['q'];
        }
        
        return '';
    }
    
    public function showActions($links){
        $metalinks = $this->resolvMetalinks($links);
        
        if(empty($metalinks) === false){
            $this->dataLinks($metalinks);
        }
    }
    
    public function actionsHolder($config){
        if(isset($config['links']) === false){
            $config['links'] = [];
        }
        
        if(empty($config['links'])){
            return;
        }
        
        echo $this->element('actions', $config);
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Show:data> in ShowHelper");
        }
        
        $data = $config['data'];
        
        if(empty($data)){
            $this->emptyData($config);
        }
        else{
            $this->tableData($config);
        }
    }
    
    private function tableData($config){
        if(isset($config['page']) === null){            
            $this->raiseInternalServerError("No Conf<Show:page> in ShowHelper");
        }
        
        if(isset($config['count']) === false){   
            $this->raiseInternalServerError("No Conf<Show:count> in ShowHelper");
        }
        
        if(isset($config['schema']) === false){   
            $this->raiseInternalServerError("No Conf<Show:schema> in ShowHelper");
        }
        
        if(isset($config['limit']) === false){   
            $this->raiseInternalServerError("No Conf<Show:schema> in ShowHelper");
        }
        
        $data = $config['data'];
        $schema = $config['schema'];
        $count = $config['count'];
        $page = $config['page'];
        $limit = $config['limit'];
        
        echo $this->element('table',[
            'data' => $data,
            'schema' => $schema,
            'page' => $page,
            'count' => $count,
            'limit' => $limit,
            'c' => count($data)
        ]);
    }
    
    public function customFiltersView(){
        $config = $this->_View->Backend->feed('filters',false);
        
        if(is_array($config)){
            foreach($config as $elem){
                echo $this->_View->element($elem, $this->_View->request->query);
            }
        }
    }
    
    public function limits($schema){
        $config = $this->_View->Backend->feed('limits',false);
        
        if($config){
            echo $this->element('limits_engine');
            echo $this->element('limits');
        }
    }
    
    public function limOptions($limits){
        $current = $this->params->query['lim'];
        
        foreach($limits as $lim){
            echo $this->element('lim_option',[
                'current' => $lim == $current,
                'lim' => $lim
            ]);
        }
    }
    
    public function getLimTerm(){
        if(isset($this->params->query['lim'])){
            return $this->params->query['lim'];
        }
        
        return '';
    }
    
    public function filtersEngine(){
        echo $this->element('filters_engine');
    }
    
    public function quickSearch($schema){
        if($this->isSearchable($schema) === false){
            return;
        }
        
                
        echo $this->element('quicksearch');
        echo $this->element('quicksearch_engine');
    }
    
    public function isSearchable($schema){
        $fields = [];
        
        foreach($schema as $field => $meta){
            if($this->resolvSearchable($meta) === false){
                continue;
            }
            
            $fields[] = $field;
        }
        
        return count($fields) > 0;
    }
    
    public function resolvSearchable($meta){
        if(isset($meta['searchable']) && $meta['searchable'] === true){
            return true;
        }
        
        return false;
    }
    
    public function resolvPaginationUrl($page){
        $url = $this->resolvCurrentUrl();
        $purl = parse_url($url);
        
        if(isset($purl['query'])){
            parse_str($purl['query'],$purl['query']);
        }
        else{
            $purl['query'] = [];
        }
                
        $purl['query']['p'] = $page;
        $paginationUrl = $this->buildUrl($purl);
        return $paginationUrl;
    }
    
    public function filters($schema){
        echo $this->element('filters',[
            'schema' => $schema,
        ]);
    }
        
    
    public function pagination($page,$count,$c,$limit){
        echo $this->element('pagination',[
            'page' => $page,
            'count' => $count,
            'c' => $c,
            'limit' => $limit
        ]);
    }
    
    public function paginationEmpty($page,$count,$c,$limit){
        echo $this->element('pagination_empty',[
            'page' => $page,
            'count' => $count,
            'c' => $c,
            'limit' => $limit
        ]);
    }
    
    public function numbers($page,$count,$limit){
        $last = $this->resolvLastPage($count, $limit);
        $activable = $page <= $last;
        
        if($page > $last){
            $page = $last;
        }
        
        $spread = $this->_View->Backend->feed('pagination.spread');
        $a = $page - $spread;
        
        if($a < 0){
            $spread += abs($a);
            $a = 1;
        }
        
        $b = $page + $spread;
        
        if($b > $last){
            $b = $last;
        }
        
        for($p = $a;$p <= $b;$p++){
            $url = $this->resolvPaginationUrl($p);
            $active = '';
            
            if($p === $page && $activable){
                $active = 'active';
            }
            
            if ($p == 0){
                continue;
            }
            
            echo $this->element('number',[
                'page' => $p,
                'url' => $url,
                'active' => $active,
            ]);
        }
    }
    
    public function nextButton($page,$count,$limit){
        $disabled = '';
        $url = '#';
        $lastPage = $this->resolvLastPage($count,$limit);
        
        if($page >= $lastPage){
            $disabled = 'disabled';
        }
        else{
            $url = $this->resolvPaginationUrl($page + 1);
        }
        
        echo $this->element('next',[
            'disabled' => $disabled,
            'url' => $url
        ]);
    }
    
    public function resolvLastPage($count,$limit){
        $last = ceil($count / $limit);
        return $last;
    }
    
    public function backButton($page,$count,$limit){
        $last = $this->resolvLastPage($count, $limit);
        $disabled = '';
        $url = '#';
        
        if($page === 1){
            $disabled = 'disabled';
        }
        else if($page > $last){
            $url = $this->resolvPaginationUrl($last);
        }
        else{
            $url = $this->resolvPaginationUrl($page - 1);
        }
        
        echo $this->element('back',[
            'disabled' => $disabled,
            'url' => $url
        ]);
    }
    
    public function resolvPaginationSummary($count,$c){
        $summary = $this->_View->Backend->feed('pagination.summary');
        return sprintf($summary, $c, $count);
    }
    
    private function emptyData($config){
        echo $this->element('empty',$config);
        
        if($config['count'] > 0){
            $this->paginationEmpty($config['page'], $config['count'],0,$config['limit']);
        }
    }
    
    public function provideLogtag() {
        return 'Show';
    }
    
    public function tableHead($schema,$links = []){
        echo $this->element('table_head',[
            'schema' => $schema,
            'links' => $links
        ]);
    }
    
    public function checkHead($path){
        $config = $this->_View->Backend->feed($path,false);
        
        if($config){
            echo $this->element('check_head', $config);
        }
    }
    
    public function checkSubmit($path){
        $config = $this->_View->Backend->feed($path,false);
        
        if($config){
            echo $this->element('check_submit', $config);
        }
    }
    
    public function checkCell($path,$row){
        $config = $this->_View->Backend->feed($path,false);
        
        if($config){
            $config['value'] = $row[$config['field']];
            echo $this->element('check_cell', $config);
        }
    }
    
    public function checkUrl($path){
        $config = $this->_View->Backend->feed($path,false);
        
        if($config){
            return $config['url'];
        }
        
        return '';
    }
    
    public function tableHeadCells($schema,$links){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $label = $this->resolvLabel($meta);
            
            echo $this->element('table_head_cell',[
                'field' => $field,
                'label' => $label,
            ]);
        }
            
        if(empty($links) === false){
            echo $this->element('table_head_cell',[
                'label' => 'Acciones',
                'field' => '___actions__'
            ]);
        }
    }
    
    public function tableBody($schema,$data){
        echo $this->element('table_body',[
            'data' => $data,
            'schema' => $schema
        ]);
    }
    
    public function tableRows($schema,$data){
        foreach($data as $datalink){
            echo $this->element('table_row',[
                'row' => $datalink['data'],
                'links' => $datalink['links'],
                'schema' => $schema
            ]);            
        }
    }
    
    public function actions($links){
        foreach($links as $link){
            $controller = $link['controller'];
            $action = $link['action'];
            $sitemap = $this->resolvSitemap($controller,$action);
            $url = Router::url($link);

            echo $this->element('action',[
                'title' => $sitemap['h1'],
                'icon' => $sitemap['icon'],
                'url' => $url
            ]);    
        }
    }
    
    public function tableCells($schema,$row,$links){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $value = $this->resolvValue($field,$meta,$row);
            
            echo $this->element('table_cell',[
                'field' => $field,
                'value' => $value
            ]);            
        }
        
        if(empty($links) === false){
            echo $this->element('table_cell_actions',[
                'links' => $links
            ]);            
        }
    }
}