<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class TopGraphDashboardComponent extends KlezkaffoldComponent{
    private $data = [];
    private $config;
    private $graph;
    
    public function output() {
        $this->data['graph'] = $this->graph;
        return $this->data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        $this->loadModel($this->config['data']['grouper']);
        $grouper = $this->getModel()->find('first',$this->config['data']['grouper']['query']);
        
        if(empty($grouper)){
            return;
        }
        
        $primary = $this->getModel()->primaryKey;
        $this->processTitle($grouper);
        $this->loadModel($this->config['data']['top']);
        $query = $this->resolvTopQuery($grouper,$primary);
        $alias = $this->getModel()->alias;
        $top = $this->getModel()->rawFind('all',$query);
        
        if(empty($top)){
            return;
        }
        
        $topData = [];
        
        foreach($top as $raw){
            $t = $raw[$alias];
            $topData[] = $t;
        }
        
        $this->data['top'] = $topData;
    }
    
    private function resolvTopQuery($grouper,$primary){
        $grouperColumn = $this->config['data']['top']['grouper'];
        $valueExpression = $this->config['data']['top']['value'];
        $labelExpression = $this->config['data']['top']['label'];
        $query = $this->config['data']['top']['query'];
        
        if(isset($query['conditions']) === false){
            $query['conditions'] = [];
        }
        
        $query['fields'] = [];
        $query['fields'][] = $valueExpression . ' AS value';
        $query['fields'][] = 'label';
        
        $this->getModel()->virtualFields['label'] = $labelExpression;
        
        $query['conditions'][$grouperColumn] = $grouper[$primary];            
        return $query;
    }
    
    private function processTitle($grouper){
        if(is_array($this->config['title'])){
            $string = array_shift($this->config['title']);
            $args = [];
            
            foreach($this->config['title'] as $arg){
                if(isset($grouper[$arg])){
                    $args[] = $grouper[$arg]; 
                }
                else{
                    $args[] = $arg;
                }
            }
            
            $title = vsprintf($string,$args);
        }
        else{
            $title = $this->config['title'];
        }
        
        $this->data['title'] = $title;
    }
    
    public function parseConfig($config){
        if(isset($config['data']['grouper']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.grouper> in Klezkaffold Config");
        }
        
        if(isset($config['data']['top']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.top> in Klezkaffold Config");
        }
        
        if(isset($config['data']['top']['grouper']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.top.grouper> in Klezkaffold Config");
        }
        
        if(isset($config['data']['top']['label']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.top.label> in Klezkaffold Config");
        }
        
        if(isset($config['data']['top']['value']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.top.value> in Klezkaffold Config");
        }
        
        if(isset($config['title']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:title> in Klezkaffold Config");
        }
        
        if(isset($config['graph']) === false){
            $this->graph = [];
        }
        else{
            $this->graph = $config['graph'];
        }
        
        if(isset($config['data']['grouper']['query']) === false){
            $config['data']['grouper']['query'] = [];
        }
        
        $this->config = $config;
    }
}