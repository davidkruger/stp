<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class SumGraphDashboardComponent extends KlezkaffoldComponent{
    private $series = [];
    private $data = [];
    
    public function output() {
        return $this->data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        $this->data['series'] = [];
        $this->data['labels'] = $this->labels;
        
        foreach($this->series as $name => $serie){
            if(isset($serie['data']) === false){
                $this->raiseConfigureException("No Conf <Serie:data> in Klezkaffold Config");
            }

            if(isset($serie['data']['field']) === false){
                $this->raiseConfigureException("No Conf <Serie:data.field> in Klezkaffold Config");
            }
            
            $this->loadModel($serie['data']);
            
            $this->data['series'][$name] = [
                'label' => $serie['label'],
                'points' => []
            ];
            
            foreach($serie['points'] as $point){
                $this->data['series'][$name]['points'][] = $this->processPoint($point,$serie['data']['field']);
            }
        }
    }
    
    private function processPoint($point,$field){
        if(isset($point['query']) === false){
            $this->raiseConfigureException("No Conf <Serie:query> in Klezkaffold Config");
        }
        
        if(isset($point['prequery']) === true){
            $this->prequery = $point['prequery'];
        }
        
        $this->query = $point['query'];
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        $alias = $this->getModel()->alias;
        $this->query['fields'] = "SUM({$alias}.{$field}) AS number";
        $result = $this->getModel()->rawFind('first',$this->query);
        return (int) @$result[0]['number'];
    }
    
    public function parseConfig($config){
        if(isset($config['data']['series']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.series> in Klezkaffold Config");
        }

        if(isset($config['data']['labels']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.labels> in Klezkaffold Config");
        }
        
        $this->series = $config['data']['series'];
        $this->labels = $config['data']['labels'];
    }
}