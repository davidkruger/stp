<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class SummaryPieDashboardComponent extends KlezkaffoldComponent{
    private $total;
    private $count;
    
    public function output() {
        return [
            'total' => $this->total,
            'count' => $this->count
        ];
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    private function processTotal(){
        $query = $this->query;
        $this->query = [];
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        $this->total = $this->getModel()->find('count',$this->query);
        $this->count = $this->total;
        $this->query = $query;
    }
    
    public function process(){
        $this->processTotal();
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        if(empty($this->query) === false){
            $this->count = $this->getModel()->find('count',$this->query);
        }
    }
    
    public function parseConfig($config){
        if(isset($config['type']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:type> in Klezkaffold Config");
        }
        
        if(isset($config['url']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:url> in Klezkaffold Config");
        }
        
        if(isset($config['data']['query']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.query> in Klezkaffold Config");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->query = $config['data']['query'];
        $this->loadModel($config['data']);
    }
}