<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('ExcelComponent','KlezData.Controller/DataDrivers');
App::uses('Salesman','Model');

class ExcelReportComponent extends KlezkaffoldComponent {
    private $Excel;
    private $sheets = [];
    private $fileName;
    private $styles = [];
    private $searchTerm = null;
    
    public function output() {
        $output = $this->Excel->output();
        $output['blob'] = base64_encode($output['blob']);
        $output['file'] = $this->fileName;
        $output['timestamp'] = time();
        
        return $output;
    }

    public function input($config,$payload = null) {
        $this->parsePayload($payload);
        $this->parseConfig($config);
        $this->loadExcel();
    }
    
    private function parsePayload($payload){
        $this->setPayload($payload);
        
        if(isset($payload['filter']['q']) === true){
            $this->searchTerm = $this->sanitizeInput($payload['filter']['q']);
        }
    }
    
    private function loadExcel(){
        $collection = new ComponentCollection();
        $this->Excel = new ExcelComponent($collection);
        $this->Excel->initialize(new Controller());
    }
    
    public function process(){
		$this->salesman = new Salesman();
        $this->schema = [];
        $this->Excel->setStyles($this->styles);
        
        foreach($this->sheets as $i => $config){
            if(isset($config['name']) === false){
                $this->raiseConfigureException("No Conf <Sheet:name> in Klezkaffold Config");
            }

            if(isset($config['data']) === false){
                $this->raiseConfigureException("No Conf <Sheet:data> in Klezkaffold Config");
            }

            if(isset($config['data']['query']) === false){
                $this->raiseConfigureException("No Conf <Sheet:data.query> in Klezkaffold Config");
            }
            
            $this->loadModel($config['data']);
            $this->query = $this->queryInjectSearch($this->searchTerm,$config['data']['query']);
            
            if(isset($config['data']['prequery']) === true){
                $this->prequery = $config['data']['prequery'];
            }
            
            if(empty($this->prequery) === false){
                $this->prequeryProcess();
            }
            
            $schema = $this->getModel()->provideReadableSchema();
            $data = $this->getModel()->findReadable('all',$this->query,true);
            error_log(serialize($this->getModel()->getLastQuery()));
            
            // foreach ($data as $key => $value) {
			// 	$id_salesmen = $value['salesman_id'];
			// 	$points_rate = $this->salesman->pointsRate($id_salesmen);
			// 	$data[$key]['points'] = round(($points_rate*(int)$value['points'])/100); 
			// } 
        
            $this->Excel->newSheet();
            $this->Excel->sheet($config['name'],$i);
            $this->Excel->table($schema,$data);
        }
    }
    
    public function parseConfig($config){
        if(isset($config['excel']['styles']) === false){
            $this->raiseConfigureException("No Conf <Report:excel.styles> in Klezkaffold Config");
        }
        
        if(isset($config['download']['file']) === false){
            $this->raiseConfigureException("No Conf <Report:download.file> in Klezkaffold Config");
        }
        
        if(isset($config['excel']['sheets']) === false){
            $this->raiseConfigureException("No Conf <Report:excel.sheets> in Klezkaffold Config");
        }
        
        $this->sheets = $config['excel']['sheets'];
        $this->fileName = $config['download']['file'];
        $this->styles = $config['excel']['styles'];
    }
}
