<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ShowKlezkaffoldComponent extends KlezkaffoldComponent{
    private $page;
    private $limit;
    private $count = 0;
    private $schema = [];
    private $data = [];
    private $dataLink = [];
    private $links = [];
    private $order = null;
    private $orderDir = null;
    private $orderField = null;
    private $searchTerm = null;
    private $injectors = [];
    
    public function output() {
        if(is_null($this->query) === true){
            return [ 
                'exception' => 404 
            ];
        }
        
        
        $blob = [ 
            'data' => $this->dataLink,
            'count' => $this->count,
            'page' => $this->page,
            'schema' => $this->schema,
            'limit' => $this->limit,
            'links' => $this->links,
            'order' => preg_replace('/^(.*\.)?/','',$this->order),
        ];
        
        return $blob;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->parseActions($config);
        $this->parsePayload($payload);
    }
    
    private function queryInjectors(){
        $this->queryInjectOrder();
        $this->query = $this->queryInjectSearch($this->searchTerm,$this->query);
        $this->queryInjectorLim();
    }
    
    private function queryInjectorLim(){
        $this->query['limit'] = $this->limit;
    }
    
    private function queryInjectOrder(){
        if(is_null($this->orderDir) || is_null($this->orderField)){
            return;
        }
        
        $schema = $this->getModel()->provideReadableSchema();
        $od = $this->orderDir;
        $of = null;
        
        foreach($schema as $field => $meta){
            if(isset($meta['orderable']) && $meta['orderable'] === true){
                if($field === $this->orderField){
                    $of = $field;
                    break;
                }
            }
        }
        
        if(is_null($of) === true){
            return;
        }
        
        switch ($od){
            case 'asc':
            case 'desc':
                if(isset($schema[$this->orderField]['autocomplete']['query']['order'])){
                    $oq = $schema[$this->orderField]['autocomplete']['query']['order'];
                    
                    if(is_array($oq)){
                        $ox = $oq[0];
                    }
                    else{
                        $ox = explode(' ' , $oq)[0];
                    }
                    
                    $this->query['order'] = "{$ox} {$od}";
                    break;
                }
                
                $alias = $this->getModel()->alias;
                $this->query['order'] = "{$alias}.{$of} {$od}";
                break;
        }
    }
    
    public function process(){
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }

        $this->queryInjectors();
        $this->resolvCount();
        $this->resolvData();
        $this->resolvDataLinks();
        
        $this->links = $this->resolvShowLinks();
    }
    
    private function resolvShowLinks(){
        $links = [];
        
        foreach($this->links as $link){
            if($this->isUrlAllowed($link)){
                $links[] = $link;
            }
        }
        
        return $links;
    }
    
    private function resolvDataLinks(){
        if(is_array($this->data) === false){
            return;
        }
        
        foreach($this->data as $row){
            $this->dataLink[] = [
                'data' => $row,
                'links' => $this->resolvLinks($row)
            ];
        }
    }
    
    private function resolvData(){
        if(is_null($this->query) === false){   
            if(isset($this->query['order']) === false){
                $alias = $this->getModel()->alias;
                $pkey = $this->getModel()->primaryKey;
                $this->query['order'] = "{$alias}.{$pkey} DESC";
            }
            
            $this->order = $this->query['order'];
            $offset = ($this->page - 1) * $this->limit;
            $query = $this->query;
            $query['offset'] = $offset;   
            
            $this->data = $this->getModel()->findReadable('all', $query, true); 
            error_log($this->getModel()->getLastQuery());

            foreach($this->injectors as $injector){
                if(method_exists($this->getModel(), $injector) === false){
                    $this->raiseConfigureException("No Injector <Method:$injector> in Klezkaffold");
                }
                
                $this->data = $this->getModel()->$injector($this->data);
            }
            
            $this->schema = $this->getModel()->provideReadableSchema();
        }
    }
    
    private function resolvCount(){
        $query = $this->query;
        unset($query['limit']);

        try{
            $q = $this->getModel()->findJoins('count',$query,true);
            unset($q['fields']);
            
            $this->count = $this->getModel()->find('count', $q);
//            error_log($this->getModel()->getLastQuery());
        }
        catch(\Exception $e){
            error_log($e->getMessage());
        }
    }
    
    private function parsePayload($payload){
        $this->setPayload($payload);
        
        if(isset($payload['page']) === false){
            $this->raiseConfigureException("No Data <Show:payload.page> in Klezkaffold");
        }
        
        $this->page = (int) $payload['page'];
        
        if($this->page < 1){
            $this->raiseBadRequestException("Invalid Data <Show:payload.page LESSER THAN 1> in Klezkaffold");            
        }
        
        if(isset($payload['od']) === true){
            $this->orderDir = $payload['od'];
        }
        
        if(isset($payload['of']) === true){
            $this->orderField = $payload['of'];
        }
        
        if(isset($payload['q']) === true){
            $this->searchTerm = $this->sanitizeInput($payload['q']);
        }
        
        if(isset($payload['lim']) === true){
            $this->limit = (int) $payload['lim'];
        }
    }
    
    private function parseConfig($config){
        if(isset($config['data']['query']) === false){
            $this->raiseConfigureException("No Conf <Show:data.query> in Klezkaffold Config");
        }
        
        if(isset($config['data']['query']['limit']) === false){
            $this->raiseConfigureException("No Conf <Show:data.query.limit> in Klezkaffold Config");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        if(isset($config['data']['injectors']) === true){
            $this->injectors = $config['data']['injectors'];
        }
        
        if(isset($config['links']) === true){
            $this->links = $config['links'];
        }
        
        $this->limit = $config['data']['query']['limit'];
        $this->query = $config['data']['query'];
        $this->loadModel($config['data']);
    }
}