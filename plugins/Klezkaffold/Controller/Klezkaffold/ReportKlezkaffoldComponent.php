<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ReportKlezkaffoldComponent extends KlezkaffoldComponent{
    private $Report;
    private $type;
    private $links = [];
    
    private $filterMode = false;
    private $filterError = false;
    private $schema;
    private $data = [];
    
    public function output() {
        $data = [
            'data' => $this->getModel()->getData(),
            'schema' => $this->schema,
            'messages' => $this->getModel()->validationErrors
        ];
        
        if($this->isGet() === false && $this->filterError === false){
            $data['report'] = $this->Report->injectOutput();

            if(is_array($data['report'])){
                $data['report']['links'] = $this->links;
            }
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseFilter($config,$payload);
        $this->parseConfig($config);
        
        if($this->isGet() === false){
            $this->loadReport($payload);
            $this->Report->input($config,$payload);
        }
    }
    
    private function loadReport($payload){
        $class = Inflector::camelize($this->type) . 'ReportComponent';
        $path = 'Klezkaffold.Controller/Report';
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Such Class <class:$class,path:$path> in Report Config");
        }
        
        $collection = new ComponentCollection();
        $this->Report = new $class($collection);
        
        if(($this->Report instanceof KlezkaffoldComponent) === false){
            $this->raiseConfigureException("Invalid Class <super.needed:KlezkaffoldComponent> in Report Config");
        }
        $this->Report->setPayload($payload);
        $this->Report->setAuth($this->getAuth());
        $this->Report->setAcl($this->getAcl());
    }
    
    public function process(){
        if($this->isGet() === false){
            if($this->filterMode === true){
                $this->getModel()->prepareForStore($this->data);
                
                if($this->getModel()->validateData() === false){
                    error_log(serialize($this->getModel()->validationErrors));
                    $this->filterError = true;
                    return;
                }
            }
            
            $this->links = $this->resolvReportLinks();
            $this->Report->process();
        }
    }
    
    private function resolvReportLinks(){
        $links = [];
        
        foreach($this->links as $link){
            if($this->isUrlAllowed($link)){
                $links[] = $link;
            }
        }
        
        return $links;
    }
    
    public function parseConfig($config){
        if(isset($config['type']) === false){
            $this->raiseConfigureException("No Conf <Report:type> in Klezkaffold Config");
        }
        
        if(isset($config['links']) === true){
            $this->links = $config['links'];
        }
        
        $this->type = $config['type'];
    }
    
    public function parseFilter($config,$payload){
        if(isset($payload['filter'])){
            $this->filterMode = true;
            $this->data = $payload['filter'];
            
            if(isset($config['filter']) === false){
                $this->raiseConfigureException("No Conf <Report:filter> in Klezkaffold Config");
            }
        }
        
        $this->loadModel($config['filter']);
        $this->schema = $this->getModel()->provideWritableSchema();
    }
}