<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class RequestHabtmKlezkaffoldComponent extends KlezkaffoldComponent{
    private $schema;
    private $data;
    protected $id = null;
    private $notFound = false;
    protected $intermediate;
    protected $habtm;
    private $label;
    private $configData = [];
    
    public function output() {        
        $data = [
            'schema' => $this->schema,
            'data' => $this->data,
            'label' => $this->label,
        ];
        
        if($this->notFound === true){
            $data = [
                'exception' => 404
            ];
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);

        if(isset($payload['id']) === false){
            $this->notFound = true;
        }
        else{
            $this->id = $payload['id'];
        }
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    public function process(){
        $this->loadModel($this->configData);
        
        if($this->notFound){
            return;
        }
        
        $this->resolvQuery();

        if(is_null($this->query)){
            $this->notFound = true;
        }
        else if($this->getModel()->findWritable('first',$this->query,false) === false){
            $this->notFound = true;
        }
        
        $this->processSchema();
        $this->processData();
    }
    
    private function processData(){
        $this->loadModel($this->habtm);
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        $id = $this->intermediate['id'];
        $label = $this->habtm['label'];
        $this->label['field'] = $label;
        $this->label['pkey'] = $pkey;
        
        $query = [
            'joins' => [
                "LEFT JOIN {$this->intermediate['table']} AS {$this->intermediate['alias']} ON " .
                    "{$this->intermediate['alias']}.{$this->intermediate['habtm']} = {$alias}.{$pkey} AND " .
                    "{$this->intermediate['alias']}.{$id} = {$this->id}"
            ],
            'fields' => [
                "{$this->intermediate['alias']}.*",
                "{$alias}.{$label}",
                "{$alias}.{$pkey}"
            ]
        ];
        
        $raw = $this->getModel()->rawFind('all',$query);
        $data = [];
        
        foreach($raw as $row){
            $data[] =  $row[$alias] + $row[$this->intermediate['alias']];
        }
        
        $this->data = $data;
    }
    
    public function processSchema(){
        $this->loadModel($this->intermediate);
        $this->schema = $this->getModel()->provideWritableSchema();
        $id = $this->intermediate['id'];
        $habtm = $this->intermediate['habtm'];
        $this->label = $this->schema[$habtm];
        
        $this->intermediate['table'] = $this->getModel()->useTable;
        $this->intermediate['alias'] = $this->getModel()->alias;
        
        unset($this->schema[$id]);
        unset($this->schema[$habtm]);
    }
    
    public function parseConfig($config){
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        if(isset($config['intermediate']) === false){
            throw new NotImplementedException("No Config <intermediate> in HabtmRequest");
        }
        
        if(isset($config['habtm']) === false){
            throw new NotImplementedException("No Config <habtm> in HabtmRequest");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->configData = $config['data'];        
        $this->intermediate = $config['intermediate'];
        $this->habtm = $config['habtm'];
        $this->id = $this->resolver($config);
    }
}