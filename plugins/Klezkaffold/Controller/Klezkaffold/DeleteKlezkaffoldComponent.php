<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class DeleteKlezkaffoldComponent extends KlezkaffoldComponent{
    private $success = false;
    private $schema = [];
    private $data = [];
    private $id = null;
    private $redirect = null;
    private $url;
    
    public function output() {
        $schema = $this->resolvFormSchema();
        
        if(empty($this->data) === true){
            return [ 'exception' => 404 ];
        }
        
        $data = [
            'schema' => $schema,
            'data' => $this->data,
            'id' => $this->id,
            'success' => $this->success,
        ];
        
        return $data;
    }
    
    public function isSuccess() {
        return $this->success;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function input($config,$payload = null) {
        $this->parseConfig($config);
                
        if(isset($payload['data']['redirect']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.redirect> in DeleteKlezkaffold");
        }
        
        if(isset($payload['data']['id'])){
            $this->id = $payload['data']['id'];
        }
        
        $this->url = $payload['data']['redirect'];
        //$this->data = $this->formData($payload['data']['formdata']);
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    private function loadData(){
        $this->resolvQuery();
        
        if(is_null($this->query) === false){
            $this->data = $this->getModel()->findReadable('first',$this->query,true);

            if($this->data === false){
                return false;
            }
        }
        
        return true;
    }
    
    public function process(){
        if($this->loadData() === false){
            return;
        }
        
        $this->schema = $this->getModel()->provideWritableSchema();
        $this->getModel()->prepareForStore($this->data,$this->id);
        
        if($this->deleteData() === false){
            $this->failureCallback();
            return;
        }
        
        $this->successCallback();
    }
    
    private function successCallback(){
        $this->success = true;
        $this->redirect = $this->resolvRedirect($this->getModel()->getData(), $this->url);
    }
    
    private function failureCallback(){
        $this->success = false;
        $this->redirect = null;
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->id = $this->resolver($config);
    }
}