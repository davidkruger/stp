<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class RequestDeleteKlezkaffoldComponent extends KlezkaffoldComponent{
    private $schema;
    private $data = false;
    private $id = null;
    private $notFound = false;
    
    public function output() {
        $data = [
            'schema' => $this->schema,
            'data' => $this->data
        ];
        
        if($this->notFound === true){
            $data = [
                'exception' => 404
            ];
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        
        if(isset($payload['id']) === false){
            $this->raiseConfigureException("Invalid Paylod<missing.key:id> in RequestDelete");
        }
        
        $this->id = $payload['id'];
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    public function process(){
        $this->resolvQuery();
        
        if(is_null($this->query) === false){ 
            $this->schema = $this->getModel()->provideReadableSchema();
            $this->data = $this->getModel()->findReadable('first',$this->query,true);
        }

        if($this->data === false){
            $this->notFound = true;
        }
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
    }
}