<?php

App::uses('RequestHabtmKlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class HabtmKlezkaffoldComponent extends RequestHabtmKlezkaffoldComponent{
    private $success = false;
    private $results = [];   
    private $formdata = [];
    
    public function output() {        
        $data = parent::output();
        
        if(isset($data['schema'])){
            $data['results'] = $this->results;
            $data['success'] = $this->success;
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        parent::input($config, $payload);
        
        if(array_key_exists('formdata',$payload) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:formdata> in HabtmKlezkaffold");
        }
        
        $this->formdata = $payload['formdata']['habtm'];
    }
    
    private function resolvRealId($habtmId,$intermediateId){
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        $intermediate = $this->intermediate['id'];
        $habtm = $this->intermediate['habtm'];
        
        $cnd = [];
        $cnd["{$alias}.{$intermediate}"] = $intermediateId;
        $cnd["{$alias}.{$habtm}"] = $habtmId;
        
        $result = $this->getModel()->rawFind('first',[
            'conditions' => $cnd,
            'fields' => [
                "{$alias}.{$pkey}"
            ]
        ]);
                
        if(isset($result[$alias][$pkey])){
            return $result[$alias][$pkey];
        }
        
        return null;
    }
    
    public function process(){
        try{
            $this->loadModel($this->intermediate);
            $this->getModel()->begin();
            $intermediate = $this->intermediate['id'];
            $habtm = $this->intermediate['habtm'];
            
            foreach($this->formdata as $blob => $data){
                $habtmId = (int) str_replace('habtm','',$blob);
                $data[$habtm] = $habtmId;
                $data[$intermediate] = $this->id;
                
                $this->getModel()->prepareForStore($data,$this->resolvRealId($habtmId,$this->id));
                $this->getModel()->set($this->getModel()->getWritableData());
                
                if($this->getModel()->saveData()){
                    $this->results[$blob] = true;
                }
                else{
                    $this->results[$blob] = $this->getModel()->validationErrors;
                }
            }
            
            $this->getModel()->commit();
            $this->success = true;
        } 
        catch (Exception $e) {
            $this->getModel()->rollback();
            error_log("Exception @ HabtmKlezkaffodlComponent::process() - {$e->getMessage()}");
            $this->success = false;
        }
        
        parent::process();
    }
    
    public function isSuccess(){
        return $this->success;
    }
}