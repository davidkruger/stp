<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');
App::uses('KlezModel', 'KlezData.Model');

abstract class KlezkaffoldComponent extends BaseComponent{
    private $Auth;
    private $Model;
    private $actions;
    private $payload;
    private $formSchema = null;
    private $actionParams = [];
    
    protected $prequery = [];
    protected $query = [];
    
    function getFlowControl() {
        return $this->flowControl;
    }
    
    function getAcl() {
        return $this->Acl;
    }

    function setAcl($Acl) {
        $this->Acl = $Acl;
    }

    function setPayload($payload) {
        $this->payload = $payload;
    }    
    
    function setAuth($Auth){
        $this->Auth = $Auth;
    }
    
    protected function resolvFormSchema(){
        if(is_null($this->formSchema) === false){
            $this->getModel()->shapeshift($this->formSchema);
        }
        
        $schema = $this->getModel()->provideWritableSchema();
        
        return $schema;
    }
    
    protected function prequeryProcess() {
        $Model = $this->getModel();
        $Auth = $this->getAuth();
        
        foreach($this->prequery as $method){
            if(method_exists($Model, $method)){
                $this->query = $Model->{$method}($this->query,$Auth->getData(),$this->payload);
                
                if(is_null($this->query)){
                    return;
                }
            }
            else{
                $alias = $Model->alias;
                $this->raiseConfigureException("No Prequery <Model:{$alias},Method:$method> in Klezkaffold Config");
            }
        }
    }
    
    public function __construct(\ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);
    }
    
    private function initializeLog(){
        $file = 'klezkaffold';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'scaffold' ],
            'file' => $file
        ));    
    }
    
    protected function logscaffold($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'scaffold');
    }

    abstract function input($config,$payload = null);
    abstract function process();
    abstract function output();
    
    private $injectors = [];

    public function injectOutput(){
        $output = $this->output();
        
        foreach($this->injectors as $injector){
            if(method_exists($this->Model, $injector) === false){
                throw new NotImplementedException("No such injector method<{$this->Model->alias},$injector}>");
            }
            
            $output = $this->Model->{$injector}($output);
        }
        
        return $output;
    }
    
    private $transactional = true;
    
    public function setTransactional($t){
        $this->transactional = $t;
    }
    
    protected function saveData(){
        try{
            if($this->transactional){
                $this->getModel()->begin();
            }
        
            if($this->beforeSave() === false){
                throw new Exception('Model::beforeSave() === false');
            }
                       
            if($this->getModel()->saveData() === false){
                error_log(serialize($this->getModel()->validationErrors));
                throw new Exception('Model::saveData() === false');
            }
            
            if($this->saveIntermediateData() === false){
                throw new Exception('Model::saveIntermediateData() === false');
            }
            
            if($this->beforeCommit() === false){
                throw new Exception('Model::beforeCommit() === false');
            }           
            
            if($this->transactional){
                $this->getModel()->commit();
            }
            
            return true;
        }
        catch(Exception $e){
            if($this->transactional){
                $this->getModel()->rollback();
            }
            
            $this->logException($e);
            $this->raiseInternalServerError("Exception @ KlezkaffoldComponent::saveData<message:{$e->getMessage()}>");

            return false;
        }
    }
    
    private function logException(Exception $e){
        $class = get_class($e);
        $line = $e->getLine();
        $file = $e->getFile();
        error_log("CATCHED-EXCEPTION {$e->getMessage()}");
        error_log("CATCHED {$class} @ $file:$line");
        
        foreach($e->getTrace() as $i => $message){
            $file = 'unknown';
            $line = 'unknown';
            
            if(isset($message['file'])){
                $file = $message['file'];
            }
            
            if(isset($message['line'])){
                $line = $message['line'];
            }
            
            error_log("TRACE #$i {$file}:{$line}");
        }
    }
    
    protected function deleteData(){
        try{
            $this->getModel()->begin();
            
            if($this->getModel()->deleteData() === false){
                throw new Exception('Model::saveData() === false');
            }
            
            if($this->beforeCommit() === false){
                throw new Exception('Model::beforeCommit() === false');
            }
            
            $this->getModel()->commit();
            return true;
        }
        catch(Exception $e){
            $this->getModel()->rollback();
            $this->logException($e);
            $this->raiseInternalServerError("Exception @ KlezkaffoldComponent::deleteData<message:{$e->getMessage()}>");
            return false;
        }
    }
    
    private $beforeSave = [];
    
    private function beforeSave(){
        if(empty($this->beforeSave) === true){
            return true;
        }
        
        $authdata = $this->Auth->getData();
        $data = $this->Model->getData();
        
        foreach($this->beforeSave as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in BeforeSave");
            }
            
            if($this->getModel()->{$method}($data,$authdata) !== true){
                return false;
            }
        }
        
        return true;
    }
    
    private $beforeCommit = [];
    
    private function beforeCommit(){
        if(empty($this->beforeCommit) === true){
            return true;
        }
        
        $authdata = $this->Auth->getData();
        $data = $this->Model->getData();
        
        foreach($this->beforeCommit as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in BeforeCommit");
            }
            
            if($this->getModel()->{$method}($data,$authdata) !== true){
                return false;
            }
        }
        
        return true;
    }
    
    private function saveIntermediateData(){
        $schema = $this->getModel()->provideSchema();   
        $status = true;     
        
        foreach($schema as $field => $meta){
            if(@$meta['type'] === 'foreign' && @$meta['subtype'] === 'habtm'){
                $status = $this->saveHabtmData($meta,$field);
            }
            
            if(@$meta['type'] === 'foreign' && @$meta['subtype'] === 'tags'){
                $status = $this->saveTagsData($meta,$field);
            }
            
            if($status === false){
                break;
            }
        }
        
        return $this->payloadProcess();
    }
    
    private $payloadProcessor = [];
    
    private function payloadProcess(){
        if(empty($this->payloadProcessor) === true){
            return true;
        }
        
        $authdata = $this->Auth->getData();
        $payload = $this->payload;
        
        foreach($this->payloadProcessor as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in PayloadProcess");
            }
            
            if($this->getModel()->{$method}($payload,$authdata) !== true){
                return false;
            }
        }
        
        return true;
        
    }
    
    private function saveTagsData($meta,$field){
        $tags = $this->tagsAllocation($meta,$field);
        $current = $this->getModel()->tagsCurrent($meta,$field);
        $counter = [];
        
        $class = $meta['habtm']['intermediate']['class'];
        $me = $meta['habtm']['intermediate']['identifier']['me'];
        $foreign = $meta['habtm']['intermediate']['identifier']['foreign'];
        $fclass = $meta['habtm']['foreign']['class'];
                
        if(array_key_exists('counter',$meta['habtm']['foreign']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:foreign.counter> in KlezModel');
        }
        
        $Tag = new $fclass();
        $Intermediate = new $class();
        $alias = $Intermediate->alias;
        $fcounter = $meta['habtm']['foreign']['counter'];
        
        foreach($tags as $tag => $id){
            if(isset($current[$tag]) === false){
                $Intermediate->id = null;
                $Intermediate->writeField($me,$this->getModel()->id);
                $Intermediate->writeField($foreign,$id);
                
                if($Intermediate->saveData() === false){
                    return false;
                }
                
                $counter[$id] = 1;
            }
            
            unset($current[$tag]);
        }
                
        foreach($current as $tag => $id){
            if(isset($tags[$tag]) === false){
                $cnd = [];
                $cnd["{$alias}.{$me}"] = $this->getModel()->id;
                $cnd["{$alias}.{$foreign}"] = $id;
                
                if($Intermediate->deleteAll($cnd,false) === false){
                    return false;
                }
                
                $counter[$id] = -1;
            }
        }
        
        if(is_null($fcounter) === false){
            foreach($counter as $id => $delta){
                if($Tag->loadById($id) === false){
                    return false;
                }

                $c = $Tag->readField($fcounter) + $delta;
                if($Tag->saveField($fcounter,$c) === false){
                    return false;
                }
            }
        }
        
        return true;
    }
    
    private function tagsCurrent($meta){
        if(isset($meta['habtm']['intermediate']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['me']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.me> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['foreign']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.foreign> in KlezModel');
        }
        
        $class = $meta['habtm']['intermediate']['class'];
        $path = $meta['habtm']['intermediate']['path'];
        $me = $meta['habtm']['intermediate']['identifier']['me'];
        $foreign = $meta['habtm']['intermediate']['identifier']['foreign'];
        
        $fclass = $meta['habtm']['foreign']['class'];
        $fpath = $meta['habtm']['foreign']['path'];
        $fidentifier = $meta['habtm']['foreign']['identifier'];
        $flabel = $meta['habtm']['foreign']['label'];
        
        App::uses($class,$path);
        App::uses($fclass,$fpath);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Intermediate = new $class();
        $alias = $Intermediate->alias;
        
        $FModel = new $fclass();
        $falias = $FModel->alias;
        $ftable = $FModel->useTable;
        
        $cnd = [
            "{$alias}.{$me}" => $this->getModel()->id
        ];
            
        $joins = [
            "INNER JOIN {$ftable} AS {$falias} ON {$falias}.{$fidentifier}={$alias}.{$foreign}"
        ];
            
        $raws = $Intermediate->find('all',[
            'fields' => "{$falias}.{$flabel},{$falias}.{$fidentifier}",
            'conditions' => $cnd,
            'joins' => $joins
        ]);
            
        $map = [];
        
        if(is_array($raws)){        
            foreach($raws as $raw){
                $tag = $raw[$falias][$flabel];
                $id = $raw[$falias][$fidentifier];
                $map[$tag] = $id;
            }
        }
        
        return $map;
    }
    
    private function tagsAllocation($meta,$field){
        $data = $this->getModel()->readField($field);
        $rawtags = trim($data[0]);
        $map = [];
        
        if(isset($rawtags) && ! empty($rawtags)){
            $tags = explode(',', $rawtags);
            
            foreach($tags as $tag){
                if(isset($meta['maxlength'])){
                    $tag = substr($tag, 0, $meta['maxlength']);
                }
                
                $map[$tag] = $this->tagAllocation($meta,$tag);
            }
        }
        
        return $map;
    }
    
    private function tagAllocation($meta,$tag){
        if(isset($meta['habtm']['foreign']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:foreign.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:foreign.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:foreign.identifier> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:foreign.label> in KlezModel');
        }
        
        $class = $meta['habtm']['foreign']['class'];
        $path = $meta['habtm']['foreign']['path'];
        $identifier = $meta['habtm']['foreign']['identifier'];
        $label = $meta['habtm']['foreign']['label'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Model = new $class();
        $alias = $Model->alias;
        
        $cnd = [];
        $cnd["{$alias}.{$label}"] = $tag;
        $raw = $Model->rawFind('first',[
            'fields' => "{$alias}.{$identifier}",
            'conditions' => $cnd
        ]);
        
        if($raw){
            return $raw[$alias][$identifier];
        }
        
        $Model->writeData([ $label => $tag, 'status' => 1 ]);
        
        if($Model->saveData()){
            return $Model->id;
        }
        
        $this->raiseInternalServerError("Cannot alloc tag<tag:$tag,alias:$alias> in KlezModel");

    }
    
    private function saveHabtmData($meta,$field){
        if(isset($meta['habtm']['intermediate']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['me']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.me> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['foreign']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.foreign> in KlezModel');
        }
        
        $class = $meta['habtm']['intermediate']['class'];
        $path = $meta['habtm']['intermediate']['path'];
        $me = $meta['habtm']['intermediate']['identifier']['me'];
        $foreign = $meta['habtm']['intermediate']['identifier']['foreign'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Intermediate = new $class();
        $alias = $Intermediate->alias;
        $pkey = $Intermediate->primaryKey;
        
        $data = $this->getModel()->readField($field);
        $status = true;
        $ids = [];
        $rawids = trim($data[0]);
        $protectionIds = [];
        
        if(isset($rawids) && ! empty($rawids)){
            $ids = explode(',', $rawids);
        }
        
        foreach($ids as $id){
            $cnd = [];
            $cnd["{$alias}.{$me}"] = $this->getModel()->id;
            $cnd["{$alias}.{$foreign}"] = $id;
            
            if($Intermediate->loadByConditions($cnd)){
                $protectionIds[] = $Intermediate->id;
                continue;
            }
            
            $intermediateData = [
                $me => $this->getModel()->id,
                $foreign => $id
            ];
            
            $Intermediate->writeData($intermediateData,null);
            
            if($Intermediate->saveData() === false){
                $status = false;
            }
            
            if($status === false){
                break;
            }
            
            $protectionIds[] = $Intermediate->id;
        }
        
        
        if(empty($protectionIds)){
            $deleteCnd = [
                "{$alias}.{$me}" => $this->getModel()->id
            ];
        }
        else if(count($protectionIds) === 1){
            $deleteCnd = [
                "{$alias}.{$pkey} != " => $protectionIds[0],
                "{$alias}.{$me}" => $this->getModel()->id
            ];
        }
        else{
            $deleteCnd = [
                "{$alias}.{$pkey} NOT IN " => $protectionIds,
                "{$alias}.{$me}" => $this->getModel()->id
            ];
        }
            
//        error_log(serialize($deleteCnd));
        
        if($Intermediate->deleteAll($deleteCnd,false) === false){
            return false;
        }
        
        return $status;
    }
    
    protected function raiseInternalServerError($message){
        $this->logscaffold($message);
        throw new InternalErrorException($message);
    }
    
    protected function raiseConfigureException($message){
        $this->logscaffold($message);
        throw new ConfigureException($message);
    }
    
    protected function raiseNotFoundException($message){
        $this->logscaffold($message);
        throw new NotFoundException($message);
    }
    
    protected function raiseBadRequestException($message){
        $this->logscaffold($message);
        throw new BadRequestException($message);
    }
    
    private function loadOptionalModel($config,$key){
        if(isset($config[$key]) === true){
            $this->$key = $config[$key];
            
            if(is_array($this->$key) === false){
                $this->$key = [ $this->$key ];
            }
        }
    }
    
    public function preinput($config,$payload){
        $this->parseFormSchema($config);
        $this->parseActionParams($config,$payload);
    }
    
    private function parseActionParams($config,$payload){
        if(isset($config['actionParams'])){
            $params = $config['actionParams'];
            
            foreach($params as $paramconf){
                $source = $paramconf[0];
                $param = $paramconf[1];
                $this->actionParams[$param] = $this->resolvActionParam($source,$param,$payload);
            }
        }
    }
    
    private function resolvActionParam($source,$param,$payload){
        switch($source){
            case 'payload':
                return $this->resolvPayloadActionParam($param,$payload);
        }
        
        $this->raiseConfigureException("No such Source<source:$source> in Klezkaffold");
    }
    
    private function resolvPayloadActionParam($param,$payload){
        if(isset($payload[$param]) === false){
            $this->raiseConfigureException("Invalid Payload <param:$param> in Klezkaffold");
        }
        
        return $payload[$param];
    }
    
    private function parseFormSchema($config){
        if(isset($config['data']['formSchema']) === true){
            $this->formSchema = $config['data']['formSchema'];
        }        
    }
    
    protected function loadModel($config){
        if(isset($config['class']) === false){
            $this->raiseConfigureException("No Conf <Model:data.class> in Klezkaffold Config");
        }
        
        if(isset($config['path']) === false){
            $this->raiseConfigureException("No Conf <Model:data.path> in Klezkaffold Config");
        }
        
        $this->loadOptionalModel($config,'beforeSave');
        $this->loadOptionalModel($config,'payloadProcessor');
        $this->loadOptionalModel($config,'beforeCommit');
        $this->loadOptionalModel($config,'afterLoad');
        $class = $config['class'];
        $path = $config['path']; 
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Class<$class> for Klezkaffold");
        }
        
        $this->Model = new $class();
        
        if(($this->Model instanceof KlezModel) === false){
            $this->raiseConfigureException("Not an KlezModel<Class:$class> in Klezkaffold");
        }
        
        if(isset($config['schema']) === true){
            $this->Model->shapeshift($config['schema']);
        }
        
        if(isset($config['injectors']) === true){
            $this->injectors = $config['injectors'];
        }
        
        $this->afterLoadExec();
    }
    
    private $afterLoad = [];
    
    private function afterLoadExec(){
        $authdata = $this->Auth->getData();
        $data = $this->Model->getData();
        
        foreach($this->afterLoad as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in AfterLoad");
            }
            
            if($this->getModel()->{$method}($data,$authdata) !== true){
                return false;
            }
        }
    }
    
    public function getModel(){
        return $this->Model;
    }
    
    protected function parseActions($config){
        if(isset($config['actions']) === false){
            $this->raiseConfigureException("No Conf <Klezkaffold:actions> in Klezkaffold Config");
        }
        
        foreach($config['actions'] as $action){
            $this->parseAction($action);
        }
    }
    
    protected function parseAction($action){
        $key = "Klezkaffold.{$action}";
        $conf = Configure::read($key);
        
        if(is_null($conf)){
            $this->logscaffold("No Conf @ parseAction() <Klezkaffold:{$action}> in Klezkaffold Config");            
            $this->actions[$action] = [];
            return;
        }
            
        if(isset($conf['params']) === false){
            $conf['params'] = [];
        }
            
        if(isset($conf['rule']) === false){
            $conf['rule'] = null;
        }
        
        $this->actions[$action] = [
            'params' => $conf['params'],
            'rule' => $conf['rule']
        ];
    }
    
    protected function getActions(){
        return $this->actions;
    }
    
    protected function resolvLinks($row){
        if(empty($row)){
            return [];
        }
        
        $actions = $this->getActions();
        $links = [];
        
        if(empty($actions)){
            return $links;
        }
        
        foreach($actions as $action => $conf){
            $params = $conf['params'];
            $link = $this->resolvLink($row,$params,$action,$this->actionParams);
            
            if(isset($conf['rule'])){
                $rule = $conf['rule'];
                
                if(method_exists($this->Model,$rule) === false){
                    $alias = $this->Model->alias;
                    throw new NotImplementedException("No such method <{$alias}:{$rule}> in Klezkaffold.actions Config");
                }
                
                if($this->Model->{$rule}($row) === false){
                    continue;
                }
            }
            
            if($this->isUrlAllowed($link)){
                $links[] = $link;
            }
        }
        
        return $links;
    }
    
    private function resolvLink($row,$params,$action, $urlarray = []){
        list($actn,$ctrl) = explode('.', $action);
        
        $url = [];
        $url['controller'] = $ctrl;
        $url['action'] = $actn;
            
        foreach($params as $param => $paramconf){
            $url[$param] = $this->resolvParam($row,$paramconf,$urlarray);
        }
              
        return $url;
    }
    
    public function resolvRedirect($row,$url){
        if(is_null($url)) return null;
        
        $ctrl = $url['controller'];
        $actn = $url['action'];
        $action = "$actn.$ctrl";
        $this->parseAction($action);
        $redirect = $this->resolvLink($row, $this->actions[$action]['params'], $action, $url);
        return $redirect;
    }
    
    private function resolvParam($row,$paramconf,$urlarray){        
        if(is_array($paramconf)){
            $value = $this->resolvParamConf($paramconf,$row,$urlarray);
        }
        else if(isset($urlarray[$paramconf])){
            $value = $urlarray[$paramconf];
        }
        else{
            $value = $row[$paramconf];
        }
        
        return $value;
    }
    
    private function resolvParamConf($paramconf,$row,$urlarray){
        $function = $paramconf[0];
        $field = $paramconf[1];
        
        if(isset($urlarray[$field])){
            return $urlarray[$field];
        }
        
        $value = $row[$field];
        
        switch($function){
            case 'slugify':
                return self::resolvParamSlug($value);
        }
        
        $this->raiseConfigureException("Invalid Param<function:{$function}> in Klezkaffold Config");  
    }
    
    public static function resolvParamSlug($value){
        $slug = strtolower(Inflector::slug($value,'-'));
        return $slug;
    }
    
    public function formData($data){
        $schema = $this->getModel()->provideWritableSchema();
        $formdata = [];
        
        foreach($schema as $field => $meta){
            $value = null;
            
            if(isset($meta['type']) === false){
                $this->raiseConfigureException("Invalid Schema<missing.key:type> in Klezkaffold Config");  
            }
            
            if(isset($data[$field])){
                $value = $data[$field];
            }
            
            $type = $meta['type'];
            $method = "{$type}FormData";
            
            if(method_exists($this, $method)){
                $value = $this->{$method}($value);
            }
            
            if(isset($meta['fields'])){
                $v = null;
                
                foreach($meta['fields'] as $f){
                    if(isset($data[$f])){
                        $v = $data[$f];
                    }
                    
                    $formdata[$f] = $v;
                }
            }
            
            $formdata[$field] = $value;
        }
        
        return $formdata;
    }
    
    private function booleanFormData($value){
        return is_null($value) ? false : true;
    }
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->initializeLog();
        $this->Auth = $controller->Auth;
    }
    
    public function getAuth() {
        return $this->Auth;
    }
    
    public function resolver($config){
        if(isset($config['data']['resolver']) === true){
            $method = $config['data']['resolver'];

            if(method_exists($this->getModel(), $method) === false){
                $this->raiseConfigureException("No such mehtod<$method> in RequestForm Resolver");
            }

            $authdata = $this->getAuth()->getData();
            return $this->getModel()->{$method}($authdata);
        }
        
        return null;
    }
    
    protected function sanitizeInput($q){
        $q = str_replace('%', '', trim($q)); 
        $q = str_replace('-', '', trim($q));
        $q = str_replace('\'', '', trim($q));
        $q = str_replace('\"', '', trim($q));
        $q = str_replace(';', '', trim($q));
        
        return $q;
    }
        
    protected function queryInjectSearch($searchTerm,$query){
        if(is_null($searchTerm)){
            return $query;
        }
        
        if($searchTerm === ''){
            return $query;
        }
        
        $searchables = [];
        $alias = $this->getModel()->alias;
        $schema = $this->getModel()->provideReadableSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['hidden']) && $meta['hidden'] === true){
                continue;
            }
            
            if(isset($meta['readable']) && $meta['readable'] === false){
                continue;
            }
            
            if(isset($meta['listable']) && $meta['listable'] === false){
                continue;
            }
            
            if(isset($meta['searchable']) && $meta['searchable'] === true){
                if($meta['type'] === 'foreign'){
                    $class = $meta['autocomplete']['class'];
                    $label = $meta['autocomplete']['label'];
                    
                    $searchables[] = [
                        'alias' => "{$class}.{$label}",
                        'like' => isset($meta['like']) ? $meta['like'] : null
                    ];
                }
                else{
                    if(isset($meta['alias'])){
                        $a = $meta['alias'];
                    }
                    else{
                        $a = "{$alias}.{$field}";
                    }
                    
                    $searchables[] = [
                        'alias' => $a,
                        'like' => isset($meta['like']) ? $meta['like'] : null
                    ];
                }
            }
        }
        
        if(empty($searchables)){
            return;
        }
        
        $query['conditions'][0]['OR'] = [];
                
        foreach($searchables as $meta){
            $field = $meta['alias'];
            $lookup = "{$searchTerm}%";
            
            if(isset($meta['like'])){
                $lookup = sprintf($meta['like'],$searchTerm);
            }
            
            $query['conditions'][0]['OR']["{$field} LIKE "] = $lookup;
        }
        
        return $query;
    }
}