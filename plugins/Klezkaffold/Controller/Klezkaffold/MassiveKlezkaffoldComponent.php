<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('ExcelComponent','KlezData.Controller/DataDrivers');
App::uses('ComponentCollection', 'Controller');

class MassiveKlezkaffoldComponent extends KlezkaffoldComponent{
    private $Data;
    private $success = false;
    private $schema = [];
    private $data = [];
    private $massive = [];
    private $id = null;
    private $url;
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function output() {
        if($this->isPut()){
            $this->deleteCache();
            
            return [
                'id' => $this->id,
                'success' => $this->success,
                'records' => $this->records,
                'data' => $this->data,
                'schema' => $this->schema,
            ];
        }
        
        $schema = $this->resolvFormSchema();
        $data = $this->getModel()->getData();
        unset($data[$this->source]);
        
        return [
            'map' => $this->map,
            'schema' => $schema,
            'massive' => $this->massive,
            'data' => $data,
            'id' => $this->id,
            'success' => $this->success,
            'messages' => $this->getModel()->validationErrors,
        ];
    }
    
    public function isSuccess() {
        return $this->success;
    }
    
    private $signature;
    
    private function resolvCacheKey(){
        $id = $this->getAuth()->getId();
        return "massive@$id";
    }
    
    private function resolvCacheSignature($config){
        if(is_null($this->signature)){
            $this->signature = md5(json_encode($config));
        }
        
        return $this->signature;
    }
    
    private function deleteCache(){
        Cache::delete($this->resolvCacheKey());
    }
    
    private function pushCache($config,$payload){
        $sign = $this->resolvCacheSignature($config);
        
        Cache::write($this->resolvCacheKey(), [
            'payload' => $payload,
            'sign' => $sign
        ]);
    }
    
    private function pullCache($config){
        $sign = $this->resolvCacheSignature($config);
        $data = Cache::read($this->resolvCacheKey());
        
        if(isset($data['sign'])){
            if($data['sign'] === $sign){
                return $data['payload'];
            }
        }
        
        return null;
    }
    
    public function input($config,$payload = null) {
        if($this->isPut($config)){
            $payload = $this->pullCache($config); 
        }
        
        $this->pushCache($config,$payload);
        $this->parseConfig($config);
        
        if(array_key_exists('redirect',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.redirect> in MassiveKlezkaffold");
        }
        
        if(array_key_exists('formdata',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.formdata> in MassiveKlezkaffold");
        }
        
        $this->url = $payload['data']['redirect'];
        $this->data = $this->formData($payload['data']['formdata']);
    }
    
    public function process(){
        set_time_limit(0);
        ini_set('memory_limit', -1);
        
        $this->schema = $this->getModel()->provideWritableSchema();
        $this->getModel()->prepareForStore($this->data);
        
        if($this->getModel()->validateData() === false){
            $this->failureCallback();
            return;
        }
        
        if($this->resolvMassive() === false){
            $this->failureCallback();
            return;
        }
        
        if($this->isPut()){
            if($this->saveMassive() === false){
                $this->failureCallback();
                return;
            }
        }
        
        $this->successCallback();
    }
    
    private function saveMassive(){
        switch ($this->mode){
            case 'singles':
                return $this->saveMassiveSingles();
            default:
                $this->logscaffold("No such mode<{$this->mode}>");
        }
        
        return false;
    }
    
    private $records = 0;
    
    private function saveMassiveSingles(){
        try{
            $this->Massive->begin();
            
            foreach($this->massive as $data){
                if($data['valid'] === true){
                    $store = $this->beforeSave($data['data']);
                    
                    if($store === false){
                        throw new Exception("Massive::beforeSave() === false");
                    }
                    
                    $this->Massive->prepareForStore($store);
                    
                    if($this->Massive->saveData() === false){
                        throw new Exception("Massive::saveData() === false");
                    }
                    
                    $this->afterSave($this->Massive->getData());
                    $this->records++;
                }
            }
            
            $this->Massive->commit();
            $this->success = true;
            return true;
        }
        catch (Exception $e){
            $this->Massive->rollback();
            $this->logscaffold("Exception @ Massive::saveMassiveSingles() - {$e->getMessage()}");
        }
        
        return false;
    }
    
    private $afterSave = [];
    
    private function afterSave($data){
        if(empty($this->afterSave) === true){
            return $data;
        }
        
        $authdata = $this->getAuth()->getData();
        
        foreach($this->afterSave as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in MassiveAfterSave");
            }
            
            if($this->getModel()->{$method}($data,$authdata) !== true){
                return false;
            }
        }
        
        return $data;
    }
    
    private $beforeSave = [];
    
    private function beforeSave($data){
        if(empty($this->beforeSave) === true){
            return $data;
        }
        
        $authdata = $this->getAuth()->getData();
        
        foreach($this->beforeSave as $method){
            if(method_exists($this->getModel(), $method) === false){
                $class = get_class($this->getModel());
                $this->logscaffold("No Such Method<class:$class,method:$method> in MassiveBeforeSave");
            }
            
            if($this->getModel()->{$method}($data,$authdata) !== true){
                return false;
            }
        }
        
        return $data;
    }
    
    private function resolvMassive(){
        $source = $this->source;
        $subtype = $this->schema[$source]['subtype'];
        
        switch ($subtype){
            case 'csv':
                return $this->resolvMassiveCsv();
            case 'xlsx':
                return $this->resolvMassiveXlsx();
        }
        
        $this->logscaffold("Unknown Massive type<$subtype>");
        return false;
    }
    
    private function resolvMassiveXlsx(){
        $source = $this->source;
        unset($this->data[$source]);
        
        $base64 = $this->getModel()->readField($source);
        $this->Data = new ExcelComponent(new ComponentCollection());
        $this->Data->loadFromString(base64_decode(explode(',', $base64)[1]));
        $i = 1;
        
        while (($data = $this->Data->nextRow()) !== FALSE) {
            $this->processMassiveRow($data,$i);
            $i++;
        }
        
        return true;
    }
    
    private function resolvMassiveCsv(){
        $source = $this->source;
        unset($this->data[$source]);
        
        $base64 = $this->getModel()->readField($source);
        $fd = fopen($base64,'r');
        
        if($fd === false){
            return false;
        }
        
        $i = 1;
        
        while (($data = fgetcsv($fd)) !== FALSE) {
            $this->processMassiveRow($data,$i);
            $i++;
        }
        
        fclose($fd);        
        return true;
    }
    
    private $uniques = [];
    
    private function processMassiveRow($data,$pointer){
        $buffer = $this->data;
        $calc = [];
        $ensureFailed = [];
        $j = 0;
        
        foreach($this->map as $field => $schema){
            $type = $schema['type'];
            $value = null;
            
            if(isset($schema['calc'])){
                if($schema['calc']){
                    $calc[] = $field;
                    continue;
                }
            }
            
            if(isset($data[$j])){
                switch ($type){
                    case 'custom':
                        $method = $schema['method'];
                        
                        if(method_exists($this->Massive, $method) === false){
                            $alias = $this->Massive->alias;
                            throw new NotImplementedException("No such method <{$alias}::{$method}()> in Massive Config");
                        }
                        
                        $value = $this->Massive->{$method}(trim($data[$j]));
                        break;
                    case 'date':
                        $latin = str_replace('/','-',trim($data[$j]));
                        $ts = strtotime($latin);
                        
                        if($ts !== false){                        
                            $value = date('Y-m-d',$ts);
                        }
                        else{
                            $value = trim($data[$j]);
                        }
                        
                        break;
                    case 'int':
                        $value = trim($data[$j]);
                        break;
                    case 'foreign';
                        $array = $this->resolvMassiveForeign($schema,$field,$data[$j]);
                        $buffer['***foreign***'][$field] = $array['label'];
                        $buffer['***foreignraw***'][$field] = $array['value'];
                        $value = $array['id'];
                        
                        break;
                    case 'text':
                        $value = trim($data[$j]);
                        
                        if(@$schema['nullable']){
                            if($value === ''){
                                $value = null;
                            }
                        }
                        break;
                    default:
                        $this->logscaffold("Unknown Massive Data Type<$type>");
                }
            }
            
            if(isset($schema['ensureUnique'])){
                
                if(isset($value)){
                    if($schema['ensureUnique']){
                        if(!isset($this->uniques[$field])){
                            $this->uniques[$field] = [];
                        }

                        if(!in_array($value,$this->uniques[$field])){
                            $this->uniques[$field][] = $value;
                        }
                        else{
                            $ensureFailed[$field] = 'Se repite en el documento';
                        }
                    }
                }
            }
            
            $buffer[$field] = $value;
            $j++;
        }

        foreach($calc as $field){
            $value = $this->resolvMassiveCalc($this->map[$field],$buffer);
            
            if(is_null($value) === false){
                if(is_array($value)){
                    $buffer['***foreign***'][$field] = $value['label'];
                    $buffer['***foreignraw***'][$field] = $value['id'];
                    $buffer[$field] = $value['id'];
                }
                else{
                    $buffer[$field] = $value;
                }
            }
        }
        
        if(empty($buffer) === false){
            $this->Massive->validate = [];
            $this->Massive->prepareForStore($buffer);
            $valid = $this->Massive->validateData();
            
            if(!empty($ensureFailed)){
                $valid = false;
                
                foreach($ensureFailed as $field => $message){
                    if(!isset($this->Massive->validationErrors[$field])){
                        $this->Massive->validationErrors[$field] = [];
                    }
                    
                    $this->Massive->validationErrors[$field][] = $message;
                }
            }
            
            $this->massive[$pointer] = [
                'data' => $this->Massive->getData() + $buffer,
                'validation' => $this->Massive->validationErrors,
                'valid' => $valid
            ];
            
            $this->Massive->validationErrors = [];
        }
        
    }
    
    private function resolvMassiveCalc($schema,$buffer){
        $method = $schema['method'];
        
        if(method_exists($this->Massive, $method)){
            return $this->Massive->{$method}($this->getModel(),$buffer, $this->getAuth()->getData());
        }
        else{
            $this->logscaffold("No such method<{$this->Massive->alias},{$method}>");
        }
        
        return null;
    }
    
    private function resolvMassiveForeign($schema,$field,$value){
        $massiveSchema = $this->Massive->provideSchema();
        
        if(isset($massiveSchema[$field]['massive_foreign_resolver'])){
            return $this->massiveForeginResolver($massiveSchema,$field,$value);
        }
        
        $conditions = [];
        
        if(isset($massiveSchema[$field]['autocomplete']['query']['conditions'])){
            $conditions += $massiveSchema[$field]['autocomplete']['query']['conditions'];
        }

        $column = $schema['resolver'];
        $foreign = $this->Massive->findPrimaryKeyForeign($field,$column,$value,$conditions);
        $pkey = $massiveSchema[$field]['autocomplete']['identifier'];
        $label = $massiveSchema[$field]['autocomplete']['label'];

        $array = [
            'value' => $value,
            'label' => $foreign[$label],
            'id' => $foreign[$pkey]
        ];
        
        return $array;
    }
    
    private function massiveForeginResolver($massiveSchema,$field,$value){
        $authdata = $this->getAuth()->getData();
        $method = $massiveSchema[$field]['massive_foreign_resolver'];
        
        if(method_exists($this->getModel(), $method) === false){
            $class = get_class($this->getModel());
            $this->logscaffold("No Such Method<class:$class,method:$method> in MassiveBeforeSave");
            return false;
        }
        
        return $this->getModel()->{$method}($value,$authdata);
    }
    
    private function successCallback(){
        $this->id = $this->getModel()->id;
        $this->success = true;       
        $this->redirect = $this->resolvRedirect($this->getModel()->getData(), $this->url);
    }
    
    private function failureCallback(){
        $this->id = null;
        $this->success = false;
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        $this->loadMassive($config['data']);
    }
    
    private $Massive;
    private $source;
    private $map;
    private $cache;
    private $mode;
    
    private function loadMassive($config){
        if(isset($config['massive']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive> in Klezkaffold Config");
        }
        
        if(isset($config['massive']['class']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive.class> in Klezkaffold Config");
        }
        
        if(isset($config['massive']['cache']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive.cache> in Klezkaffold Config");
        }
        
        if(isset($config['massive']['source']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive.source> in Klezkaffold Config");
        }
        
        if(isset($config['massive']['map']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive.map> in Klezkaffold Config");
        }
        
        if(isset($config['massive']['mode']) === false){
            $this->raiseConfigureException("No Conf <Model:data.massive.mode> in Klezkaffold Config");
        }
        
        $massiveClass = $config['class'];
        $massivePath = $config['path'];
        
        App::uses($massiveClass, $massivePath);
        $this->Massive = new $massiveClass();
        
        if(($this->Massive instanceof KlezModel) === false){
            $this->raiseConfigureException("Not an KlezModel<Class:$massiveClass> in Klezkaffold");
        }
        
        $this->source = $config['massive']['source'];
        $this->map = $config['massive']['map'];
        $this->cache = $config['massive']['cache'];
        $this->mode = $config['massive']['mode'];
        
        if(isset($config['massive']['beforeSave'])){
            if(is_array($config['massive']['beforeSave'])){
                $this->beforeSave = $config['massive']['beforeSave'];
            }
            else{
                $this->beforeSave = [ $config['massive']['beforeSave'] ];
            }
        }
        
        if(isset($config['massive']['afterSave'])){
            if(is_array($config['massive']['afterSave'])){
                $this->afterSave = $config['massive']['afterSave'];
            }
            else{
                $this->afterSave = [ $config['massive']['afterSave'] ];
            }
        }
    }
}