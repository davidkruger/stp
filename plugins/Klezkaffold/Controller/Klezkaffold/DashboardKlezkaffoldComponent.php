<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class DashboardKlezkaffoldComponent extends KlezkaffoldComponent{
    private $Dashboard;
    private $type;
    
    public function output() {
        return $this->Dashboard->output();
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->loadDashboard();
        
        return $this->Dashboard->input($config,$payload);
    }
    
    private function loadDashboard(){
        $class = Inflector::camelize($this->type) . 'DashboardComponent';
        $path = 'Klezkaffold.Controller/Dashboard';
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Such Class <class:$class,path:$path> in Dashboard Config");
        }
        
        $collection = new ComponentCollection();
        $this->Dashboard = new $class($collection);
        
        if(($this->Dashboard instanceof KlezkaffoldComponent) === false){
            $this->raiseConfigureException("Invalid Class <super.needed:KlezkaffoldComponent> in Dashboard Config");
        }
        
        $this->Dashboard->setAuth($this->getAuth());
        $this->Dashboard->setAcl($this->getAcl());
    }
    
    public function process(){
        $this->Dashboard->process();
    }
    
    public function parseConfig($config){
        if(isset($config['type']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:type> in Klezkaffold Config");
        }
        
        $this->type = $config['type'];
    }
}