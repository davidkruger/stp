<?php

$config = [];

############################################################# HOME

$config['Klezkaffold.home.backend'] = [
    
];

############################################################# PROFILE FORM

$config['Klezkaffold.request_form.profile'] = [
    'data' => [
        'class' => 'Administrator',
        'path' => 'KlezBackend.Model',
    ],
];

############################################################# PROFILE EDIT

$config['Klezkaffold.edit.profile'] = [
    'data' => [
        'class' => 'Administrator',
        'path' => 'KlezBackend.Model',
    ],
];
