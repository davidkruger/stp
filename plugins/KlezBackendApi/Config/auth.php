<?php

$config = [];

// Puerta de acceso al autenticador

$admin = [];
$admin['gateway'] = [
    'driver' => 'login',
    'api' => Router::url([
        'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi',
        'endpoint' => 'login', 'format' => 'json'
    ], true),
    'clients' => [
       'login' => ['controller' => 'gateway','action' => 'login','plugin' => 'KlezBackend'],
       'logout' => ['controller' => 'gateway','action' => 'logout','plugin' => 'KlezBackend'],
    ],
    'bounces' => [
        'login' => ['controller' => 'backend','action' => 'home','plugin' => 'KlezBackend'],
        'logout' => ['controller' => 'gateway','action' => 'login','plugin' => 'KlezBackend'],
        'memento' => true
    ],
];

// Mecanismo de retencion de la autenticacion

$admin['mechanism'] = [
    'driver' => 'session',
    'key' => 'BackendAdminSession'
];

// Entidad contra la cual autenticar

$admin['entity'] = [
    'driver' => 'database',
    'model' => 'Administrator',
    'location' => 'KlezBackend.Model',
    'fields' => [ 'email', 'password' ]
];

// Cargamos en la configuracion todos los logins a ser utilizados

$config['Auth.Admin'] = $admin;