<?php

App::uses('BaseAclComponent', 'KlezApi.Controller/Acl');

class BackendAclComponent extends BaseAclComponent{
    private $Server;
    
    public function initialize(\Controller $controller) {
        $this->Server = $controller->Server;
        return parent::initialize($controller);
    }

    public function control($ctrl) {
        $control = Inflector::camelize($ctrl);
        $method  = "acl{$control}";
        
        if(method_exists($this, $method) === false){
            $this->raiseConfigureException("No Acl<control:{$control}> in Acl Config");
        }
        
        $this->{$method}();
    }
    
    private function aclNoSession(){
        if($this->getAuth()->hasSession()){
            $this->Server->redirect('KlezBackendApi.Controller/Redirect','AclRedirectComponent','alreadyLogged');
        }
    }
    
    private function aclSession(){
        if($this->getAuth()->hasSession() === false){
            $this->Server->redirect('KlezBackendApi.Controller/Redirect','AclRedirectComponent','loginNeeded');
        }
    }
    
    private function aclNone(){
        
    }
}