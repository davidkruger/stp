<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');

class AclRedirectComponent extends BackendComponent{
    private $Auth;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->Auth = $controller->Auth;
    }
    
    public function alreadyLogged($data){
        $this->setFlash('Sesion iniciada', 'done');
        $this->setRedirect($this->Auth->resolvLoginBounce());
    }
    
    public function loginNeeded($data){
        $this->setFlash('Debe iniciar sesion', 'error');
        $this->setRedirect($this->Auth->resolvLogoutBounce());
    }
}
