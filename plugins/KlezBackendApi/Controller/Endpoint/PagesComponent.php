<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');

class PagesComponent extends BackendComponent{
    private $Auth;
    
    public function initialize(\Controller $controller) {
        $this->Auth = $controller->Auth;
        parent::initialize($controller);
    }
    
    public function login($data){
        $this->Auth->renew();
        $auth = $this->Auth->authentication($data);
        
        if($auth){
            $this->Auth->commit();
            $this->setFlash('Bienvenido', 'done');
            $this->setRedirect($this->Auth->resolvLoginBounce());
        }
        else{
            $this->setFlash('Datos de acceso incorrectos', 'error');
        }
    }
    
    public function logout(){
        $auth = $this->Auth->release();
        
        if($auth){
            $this->setFlash('Sesion finalizada correctamente', 'done');
            $this->setRedirect($this->Auth->resolvLogoutBounce());
        }
        else{
            $this->setFlash('No se pudo cerrar sesion', 'error');
            $this->setRedirect($this->Auth->resolvLoginBounce());
        }
    }
    
    public function home(){
        
    }
}
