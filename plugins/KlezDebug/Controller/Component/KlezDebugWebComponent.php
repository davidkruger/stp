<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

class KlezDebugWebComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function console(){
        $this->setUrl($this->getApi('debug_console'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function facebook(){
        $this->setUrl($this->getApi('debug_facebook'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function fcm(){
        $this->setUrl($this->getApi('debug_fcm'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function fcmRequest($data){       
        $url = $this->getApi('debug_fcm');
        $requestPayload = $data;
        $verb = 'POST';
        
        $this->setUrl($url);
        $this->setVerb($verb);
        $this->setData($requestPayload);
        $this->cookieStorage();
        $this->jsonPayload();
        
        $response = $this->getResponse('json');
        $this->jsonResponse($response);
    }
    
    public function consoleRequest($data){
        $headers = isset($data['header']) ? $data['header'] : [];
        
        foreach($headers as $headerName => $headerValue){
            $this->putHeader($headerName, $headerValue);
        }
        
        if($data['auth']['basic'] !== ''){
            $this->putHeader('Authorization', "Basic {$data['auth']['basic']}");
        }
        
        $url = $data['request']['server'] . $data['request']['endpoint'];
        $verb = $data['request']['verb'];
        $contentType = $data['request']['content_type'];
        $requestPayload = array_key_exists('payload', $data) ? $data['payload'] : [];
        
        $this->setUrl($url);
        $this->setVerb($verb);
        $this->setData($requestPayload);
        
        switch ($contentType){
            case 'application/json':
                $this->jsonPayload();
                break;
            case 'application/x-www-form-urlencoded':
                $this->postPayload();
                break;
            case 'querystring':
                $this->querystringPayload();
                break;
        }
        
        $payload = $this->getResponse('json');
        
        $response = [
            'response' => [
                'payload' => $payload,
                'headers' => $this->getResponseHeaders(),
                'status' => $this->getStatusCode(),
            ],
            'request' => [
                'payload' => $requestPayload,
                'verb' => $verb,
                'contentType' => $contentType,
                'url' => $this->getUrl(),
                'headers' => $this->getHeaders()
            ]
        ];
        
        $this->jsonResponse($response);
    }
}
