<?php

App::uses('KlezJsonAppController', 'KlezJson.Controller');

class DebugController extends KlezJsonAppController {
    public $components = [ 'KlezDebug.KlezDebugWeb' ];
    public $helpers = [ 'KlezDebug.Fcm' ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function console(){
        $this->KlezDebugWeb->console();
    }
    
    public function facebook(){
        $this->KlezDebugWeb->facebook();
    }
    
    public function fcm(){
        $this->KlezDebugWeb->fcm();
    }
    
    public function fcm_xhr(){
        $this->KlezDebugWeb->fcmRequest($this->data);
    }
    
    public function console_xhr(){
        $this->KlezDebugWeb->consoleRequest($this->data);
    }
}