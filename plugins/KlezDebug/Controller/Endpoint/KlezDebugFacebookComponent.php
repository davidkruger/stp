<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');
App::uses('Json', 'KlezJson.Model');

class KlezDebugFacebookComponent extends BackendComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    public function main($payload){
        $this->overwrite(Configure::read('Facebook'));
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        throw new NotImplementedException("Unknown format:$format");
    }
}