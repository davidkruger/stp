<?php

Router::connect('/debug/console.html', [ 
    'controller' => 'debug', 
    'action' => 'console',
    'plugin' => 'KlezDebug'
]);

Router::connect('/debug/webservice/console.json',  [ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'debug_console',
    'format' => 'json'
]);

Router::connect('/debug/facebook.html', [ 
    'controller' => 'facebook', 
    'action' => 'console',
    'plugin' => 'KlezDebug'
]);

Router::connect('/debug/webservice/facebook.json',  [ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'debug_facebook',
    'format' => 'json'
]);

Router::connect('/debug/fcm.html', [ 
    'controller' => 'fcm', 
    'action' => 'console',
    'plugin' => 'KlezDebug'
]);

Router::connect('/debug/webservice/fcm.json',  [ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'debug_fcm',
    'format' => 'json'
]);