<div class="col-sm-12">
    <div class="row">
        <div class='col-lg-6'>
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Request Builder</h4>

                <div class="row">
                    <div class="col-lg-12 form-horizontal" id='request-fields'>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Server KEY</label>
                            <div class="col-sm-9">
                                <input name="api_key" class="form-control" value="<?=$this->Api->feed('payload.serverkey')?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 control-label" style=""></label>

                    <div class="col-sm-8">
                        <button id="fcmsubmit" type="submit" class="btn btn-success waves-effect waves-light">PUSH</button>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-lg-6'>
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Payload Builder</h4>

                <div id="fcmroot" class="row">
                    <a class="fcmplus">[ + ]</a> {
                    <div class="container"></div>
                    <div>}</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class='col-lg-6' id='fcmrequest' style='display:none;'>
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Request</h4>

                <div class="row">
                    <div class="col-lg-12" id='fcmrequestcontainer'>

                    </div>
                </div>
            </div>
        </div>


        <div class='col-lg-6' style='display:none;' id='fcmresponse'>
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Response</h4>

                <div class="row">
                    <div class="col-lg-12" id='fcmresponsecontainer'>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fcmvalue" class="fcmline">
    <a class="fcmdelete">[ - ]</a>
    <label class="fcmlabel control-label">Label</label> : 
    <label class="fcmvalue control-label">""</label>,
</div>

<div id="fcmnode" class="fcmline">
    <a class="fcmplus">[ + ]</a>
    <a class="fcmdelete">[ - ]</a>
    <label class="fcmlabel control-label">Label</label> {
    <div class="container"></div>
    <div>},</div>
</div>

<input id="fcminput" class="fcminput form-control input-sm" />

<div id="fcmmore" class="fcmmore fcmline">
    <input class="fcminput form-control input-sm" /> : <select><option>Literal</option><option>Nodo</option></select>
</div>

<div class="modal fade fcmloading" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="json-loading" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <p align="center">
                    <i class="fa fa-4x fa-spin fa-spinner"></i><br/><br/>
                    Ejecutando Push...
                </p>
            </div>
        </div>
    </div>
</div>