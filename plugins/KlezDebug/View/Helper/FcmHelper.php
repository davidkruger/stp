<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class FcmHelper extends KlezkaffoldHelper {
    public function fcm($data){
        $this->deploy();
        $this->literal('to',$data['to']);
        
        $this->node('data',[
            [ 'name' => 'action', 'expandible' => false, 'default' => '' ],
        ]);
        
        $this->node('notification',[
            [ 'name' => 'title', 'expandible' => false, 'default' => '' ],
            [ 'name' => 'body', 'expandible' => false, 'default' => '' ],
        ]);
        
        $this->literal('priority','high');
    }
    
    public function node($name,$default=[]){
        $this->line($name,$default,true);
    }
    
    public function literal($name,$default=''){
        $this->line($name,$default,false);
    }
    
    private function line($name,$default=null,$expandible=false){
        echo $this->element('fcm/js/line',[
            'name'  => $name,
            'default'  => $default,
            'expandible'  => $expandible,
        ]);
    }
    
    private function deploy(){
        echo $this->element('fcm/js/head');
        echo $this->element('fcm/html/body');
    }
    
    protected function element($name,$vars = []){
        return $this->_View->element("KlezDebug.{$name}",$vars);
    }

    public function provideLogtag() {
        return 'Fcm';
    }
}