<p>
    <a href="#" id="login" style="display:none;" onClick="logInWithFacebook()">Obtener Access Token</a>
</p>

<pre style="display:none;" id="access_token"></pre>

<script>
    logInWithFacebook = function() {
        FB.login(function(response) {
            if (response.authResponse) {
                var token =   FB.getAuthResponse()['accessToken'];
                $('#access_token').html(token).show();
            } 
            else {
                $('#access_token').html('').hide();
            }
        });

        return false;
    };
  
    window.fbAsyncInit = function() {
        var appId = <?=  json_encode($this->Api->feed('payload.app_id')); ?>;
        var appSecret = <?=  json_encode($this->Api->feed('payload.app_secret')); ?>;
        
        FB.init({
            appId: appId,
            cookie: true,
            version: 'v2.2'
        });
        
        $('#login').show();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>