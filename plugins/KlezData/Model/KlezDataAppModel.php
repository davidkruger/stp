<?php

App::uses('AppModel', 'Model');

class KlezDataAppModel extends AppModel {
    private $LogHash;
    
    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id,$table,$ds);
        $this->initializeLog();
    }
    
    private function initializeLog(){
        $this->LogHash = $this->alias;
        $file = 'klezmodel';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'model' ],
            'file' => $file
        ));    
    }
    
    protected function logmodel($message){
        $fullMessage = "[{$this->LogHash}] {$message}";
        CakeLog::write('debug', $fullMessage, 'model');
    }
    
    protected function raiseBadRequestException($message){
        $this->logmodel($message);
        throw new BadRequestException($message);
    }
}
