<div class="col-sm-12" id="json-master" style="display:none;">
    <div class="card-box">
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fecha</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12" id="json-master-created"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Checksum</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12" id="json-master-etag"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tamaño</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12" id="json-master-size"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Descarga Directa</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12"><a id="json-master-link" target="_blank" href=""></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>              
</div>