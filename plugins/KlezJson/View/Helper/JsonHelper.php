<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class JsonHelper extends KlezkaffoldHelper {
    public function current($data){
        if(empty($data)){
            echo $this->element('empty');
        }
        
        echo $this->element('current');
    }
    
    public function generate($data){
        $json = [];
        $url = "";
        
        if(isset($data['json'])){
            $json['created'] = date('d/m/Y H:i:s',strtotime($data['json']['created']));
            $json['etag'] = $data['json']['etag'];
            $json['size'] = number_format($data['json']['size'],0,',','.') . ' bytes';
        }
        
        if(isset($data['url'])){
            $url = $data['url'];
        }
        
        echo $this->element('generate',[
            'json' => $json,
            'url' => $url
        ]);
    }
    
    protected function element($name,$vars = []){
        return $this->_View->element("KlezJson.{$name}",$vars);
    }

    public function provideLogtag() {
        return 'Json';
    }
}