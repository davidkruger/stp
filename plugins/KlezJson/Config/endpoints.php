<?php

################## ACL

$acl = [];
$acl['session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'session'
];

$acl['no_session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'no_session'
];

$acl['none'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'none'
];

################### Endpoints

$config = [];

$config['Endpoint.JsonMetadata'] = [
    'component' => 'KlezJsonMetadataComponent',
    'path' => 'KlezJson.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl[ 'session' ],
    'data' => [
        'class' => 'Json',
        'path' => 'KlezJson.Model',
    ]
];

$config['Endpoint.JsonGenerate'] = [
    'component' => 'KlezJsonGenerateComponent',
    'path' => 'KlezJson.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl[ 'session' ],
    'data' => [
        'class' => 'Json',
        'path' => 'KlezJson.Model',
    ]
];

$config['Endpoint.JsonMaster'] = [
    'component' => 'KlezJsonMasterComponent',
    'path' => 'KlezJson.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => false,
    'acl' => $acl['none'],
    'data' => [
        'class' => 'Json',
        'path' => 'KlezJson.Model',
    ]
];