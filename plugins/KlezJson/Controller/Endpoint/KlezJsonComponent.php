<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');

class KlezJsonComponent extends BackendComponent{
    private $Json;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->loadModel($controller);
    }
    
    private function loadModel($controller){
        $config = $controller->Server->getConfig();
        
        if(isset($config['data']) === false){
            $this->logendpoint('Invalid Config <missing:data> in JsonComponent');
        }
        
        if(isset($config['data']['class']) === false){
            $this->logendpoint('Invalid Config <missing:data.class> in JsonComponent');
        }
        
        
        if(isset($config['data']['path']) === false){
            $this->logendpoint('Invalid Config <missing:data.path> in JsonComponent');
        }
        
        $class = $config['data']['class'];  
        $path = $config['data']['path'];
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->logendpoint("No Such Class <class:$class> in JsonComponent");
        }
        
        $this->Json = new $class();
    }
    
    public function getJson(){
        return $this->Json;
    }
}