<?php

App::uses('KlezJsonAppController', 'KlezJson.Controller');

class JsonsController extends KlezJsonAppController {
    public $components = [ 'KlezJson.KlezJsonWeb' ];
    public $helpers = [ 'KlezJson.Json' ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function current(){
        $this->KlezJsonWeb->metadata();
    }
    
    public function generate_xhr(){
        $this->KlezJsonWeb->generate();
    }
    
    public function master(){
        $this->KlezJsonWeb->master(); 
    }
    
    public function recursive(){
        $this->KlezJsonWeb->recursive(); 
    }
}