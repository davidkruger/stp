class Opportunity {
    constructor(id = null) {
        // get products container
        this.container = document.querySelector('.products-container');
        // get product wrapper
        this.wrapper = document.querySelector('.product-wrapper#new');
        // add an empty one
        this.addEmptyWrapper();
    }

    addEmptyWrapper() {
        // get a new wrapper
        this.getNewWrapper().then(wrapper => {
            // append to container
            this.container.append(wrapper);
        });
    }

    getNewWrapper() { return new Promise(resolve => {
        // clone container
        let wrapper = this.wrapper.cloneNode(true);
        // remove id and class
        wrapper.removeAttribute('id');
        wrapper.classList.remove('d-none');
        // save old value
        wrapper.querySelector('select').old = null;
        // capture change
        wrapper.addEventListener('change', e => {
            // check already added
            if (wrapper.querySelector('select').old !== null) return;
            // update old value
            wrapper.querySelector('select').old = wrapper.querySelector('select').value;
            // add an empty one
            this.addEmptyWrapper();
        });
        // return wrapper
        resolve(wrapper);
    })}
}