$(_ => {
    class Preview {
        constructor(ele) {
            this.element = $(ele);
            this.preview = $(this.element.attr('preview'));
            this.change();
        }

        change() {
            // capture change event
            this.element.change(e => {
                // hide preview by default
                this.preview.hide();
                // process field type
                switch(e.target.type) {
                    case 'select-one':
                        // must have option with url attr
                        let option = this.element.find('>option:checked');
                        // validate url
                        if (option.attr('url') !== undefined) {
                            // set url on preview
                            this.preview.attr('src', option.attr('url'));
                            // show preview
                            this.preview.show();
                        }
                        break;
                    case 'file':
                        // validate selected file
                        if (!e.target.files || !e.target.files[0]) return;
                        let reader = new FileReader();
                        //
                        reader.onload = re => {
                            // set image src
                            this.preview.attr('src', re.target.result);
                            // show preview
                            this.preview.show();
                        }
                        // read image data
                        reader.readAsDataURL(e.target.files[0]);
                        break;
                    default: console.log(e.target.type);
                }
            });
            // fire change on select only
            if (this.element[0].type === 'select-one') this.element.change();
        }
    }

    // get all elements with preview
    $('[preview]').each((idx, ele) => {
        new Preview(ele);
    });
});