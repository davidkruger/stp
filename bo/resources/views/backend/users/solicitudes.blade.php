@extends('layouts.master')

@section('page-name', 'Usuarios')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Usuarios
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.rechazados') }}" class="btn btn-sm btn-danger" style="margin: 10px">Ver Rechazados</a>
                <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-success" style="margin: 10px">Añadir Usuario</a>
                <a href="{{ route('admin.companies.create') }}" class="btn btn-sm btn-primary" style="margin: 10px">Añadir Canal Integrador</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($users->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="w-100px">ID</th>
                            <th>Canal Integrador</th>
                            <th>Seleccionar Canal Integrador</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Tipo</th>
                            <th class="w-200px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->getKey() }}</td>
                            <td>{{ $user->company_text}}</td>
                            <td>
                                @if ($user->company)
                                    {{ $user->company->name }}
                                    <input type="hidden" name="company_id" form="aceptar{{ $user->id }}" value="{{ $user->company_id }}">
                                @else
                                <select class="form-control select2" name="company_id" id="company_id" form="aceptar{{ $user->id }}" required>
                                    <option value="" selected disabled>Seleccione el Canal Integrador</option>
                                    @foreach ($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                                @endif
                            </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type }}</td>
                            <td class="text-center d-flex justify-content-around">
                                @include('components.actions', [
                                    'resource'  => 'users',
                                    'title'     => $user->name,
                                    'record'    => $user->getKey(),
                                    'actions'   => [ 'aceptar', 'rechazar' ]
                                ])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No tienes solicitudes pendientes</h3>
            </div>
        @endif
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endpush