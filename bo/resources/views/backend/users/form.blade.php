<div class="form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Nombre" value="{{ isset($user) && !old('name') ? $user->name : old('name') }}">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Email</label>
    <div class="col-6">
        <input name="email" type="email" class="form-control {{ $errors->has('email') ? 'is-danger' : '' }}" required placeholder="Email" value="{{ isset($user) && !old('email') ? $user->email : old('email') }}">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Compañía</label>
    <div class="col-6">
        <select name="company_id" class="form-control {{ $errors->has('company_id') ? 'is-danger' : '' }}" required placeholder="Compañía" value="{{ isset($user) && !old('company_id') ? $user->company_id : old('company_id') }}">
            <option selected disabled hidden>-- Seleccione Compañía --</option>
            @foreach($companies as $company)
            <option value="{{ $company->id }}" {{ (isset($user) && !old('company_id') ? $user->company_id : old('company_id')) == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Habilitar Usuario?</label>
    <div class="col-6">
        <select name="state" class="form-control {{ $errors->has('state') ? 'is-danger' : '' }}" required placeholder="Habilitado?" value="{{ isset($user) && !old('state') ? $user->state : old('state') }}">
            <option value="1" {{ (isset($user) && !old('state') ? $user->state : old('state')) == '1' ? 'selected' : '' }}>No</option>
            <option value="2" {{ (isset($user) && !old('state') ? $user->state : old('state')) == '2' ? 'selected' : '' }}>Si</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Usuario de Tipo Gerente?</label>
    <div class="col-6">
        <select name="type" class="form-control {{ $errors->has('type') ? 'is-danger' : '' }}" required placeholder="Tipo de Reseller" value="{{ isset($user) && !old('type') ? $user->type : old('type') }}">
            <option value="reseller" {{ (isset($user) && !old('type') ? $user->type : old('type')) == 'reseller' ? 'selected' : '' }}>No</option>
            <option value="reseller_gerente" {{ (isset($user) && !old('type') ? $user->type : old('type')) == 'reseller_gerente' ? 'selected' : '' }}>Si</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Contraseña</label>
    <div class="col-6">
        <input name="password" type="password" class="form-control {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} placeholder="Contraseña (mínimo 6 caracteres)">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Confirmar Contraseña</label>
    <div class="col-6">
        <input name="password_confirmation" type="password" class="form-control {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} placeholder="Contraseña (mínimo 6 caracteres)">
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.users') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>