@extends('layouts.master')

@section('page-name', 'Ranking de Oportunidades Ganadas '.$fecha)

@section('content')

<div class="card mb-3">
    <form action="{{ url('backend/rank_oportunidades/periodo') }}" method="GET" id="rank">
        @csrf
    </form>
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Ranking de Oportunidades Ganadas
            </div>
                <div class="col-1">
                    <label for="">Periodo</label>
                </div>
                <div class="col-2">
                    <select name="ano" id="ano" class="form-control" form="rank" required>
                        <option value="">Seleccione el año</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                        <option value="2031">2031</option>
                        <option value="2032">2032</option>
                        <option value="2033">2033</option>
                        <option value="2034">2034</option>
                        <option value="2035">2035</option>
                    </select>
                </div>
                <div class="col-2">
                    <select name="mes" id="mes" class="form-control" form="rank" required>
                        <option value="">Seleccione el mes</option>
                        <option value="01">01-Enero</option>
                        <option value="02">02-febrero</option>
                        <option value="03">03-Marzo</option>
                        <option value="04">04-Abril</option>
                        <option value="05">05-Mayo</option>
                        <option value="06">06-Junio</option>
                        <option value="07">07-Julio</option>
                        <option value="08">08-Agosto</option>
                        <option value="09">09-Septiembre</option>
                        <option value="10">10-Octubre</option>
                        <option value="11">11-Noviembre</option>
                        <option value="12">12-Diciembre</option>
                    </select>
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-warning" form="rank">Buscar</button>
                </div>
        </div>
    </div>
    <div class="card-body">
        @if ($companies->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Posicion</th>
                            <th>Canal Integrador</th>
                            <th>Oportunidades ganadas</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($companies as $rank)
                        <tr>
                            <td>{{ $rank->position }}</td>
                            <td>{{ $rank->name }}</td>
                            <td>{{ $rank->opp_cant }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No se encontraron resultados</h3>
            </div>
        @endif
    </div>
</div>

@endsection