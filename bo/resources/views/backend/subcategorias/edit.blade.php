@extends('layouts.master')

@section('page-name', 'Editar Sub-Categoria')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Editar Sub-Categoria
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.subcategorias.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ url('bakend/subcategorias/'.$subCategoria->id.'/edit') }}">
            @method('PUT')
            @csrf
            @include('subcategorias.form')
        </form>
    </div>
</div>

@endsection