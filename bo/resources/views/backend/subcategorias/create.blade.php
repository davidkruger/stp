@extends('layouts.master')

@section('page-name', 'Crear nueva Sub-Categoria')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Añadir Sub-Categoria
            </div>
            <div class="col-6 d-flex justify-content-end">
                {{-- <a href="{{ route('admin.divisions.create') }}" class="btn btn-sm btn-primary">Añadir</a> --}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.subcategorias.store') }}">
            @csrf
            @include('subcategorias.form')
        </form>
    </div>
</div>

@endsection