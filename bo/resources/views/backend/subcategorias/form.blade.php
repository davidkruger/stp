<div class="form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Nombre" value="{{ isset($subCategoria) && !old('name') ? $subCategoria->name : old('name') }}">
    </div>
</div>
<div class="form-group">
    <label class="col-3 control-label">Categoria</label>
    <div class="col-6">
        <select name="division_id" class="form-control {{ $errors->has('division_id') ? 'is-danger' : '' }}" required placeholder="Categoria" value="{{ isset($subCategoria) && !old('division_id') ? $subCategoria->division_id : old('division_id') }}">
            <option selected disabled hidden>-- Seleccione Categoria --</option>
            @foreach($divisiones as $division)
            <option value="{{ $division->id }}" {{ (isset($subCategoria) && !old('division_id') ? $subCategoria->division_id : old('division_id')) == $division->id ? 'selected' : '' }}>{{ $division->name }}</option>
            @endforeach
        </select>
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.subcategorias') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>