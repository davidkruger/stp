<div class="form-row form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Nombre" value="{{ isset($product) && !old('name') ? $product->name : old('name') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Descripción</label>
    <div class="col-6">
        <input name="description" type="text" class="form-control {{ $errors->has('description') ? 'is-danger' : '' }}" required placeholder="Descripción" value="{{ isset($product) && !old('description') ? $product->description : old('description') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">SKU</label>
    <div class="col-6">
        <input name="sku" type="text" class="form-control {{ $errors->has('sku') ? 'is-danger' : '' }}" required placeholder="SKU" value="{{ isset($product) && !old('sku') ? $product->sku : old('sku') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Cantidad disponible en Stock</label>
    <div class="col-6">
        <input name="stock_cantidad" type="text" class="form-control {{ $errors->has('stock_cantidad') ? 'is-danger' : '' }}" required placeholder="Cantidad disponible en stock" value="{{ isset($product) && !old('stock_cantidad') ? $product->stock_cantidad : old('stock_cantidad') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Con stock?</label>
    <div class="col-6">
        <select name="stock" class="form-control {{ $errors->has('stock') ? 'is-danger' : '' }}" required placeholder="Stock" value="{{ isset($product) && !old('stock') ? $product->stock : old('stock') }}">
            <option value="1" {{ (isset($product) && !old('stock') ? $product->stock : old('stock')) == "1" ? 'selected' : '' }}>Si</option>
            <option value="0" {{ (isset($product) && !old('stock') ? $product->stock : old('stock')) == "0" ? 'selected' : '' }}>No</option>
        </select>
    </div>
</div>

{{-- <div class="form-row form-group">
    <label class="col-3 control-label">Categoria</label>
    <div class="col-6">
        <select name="division_id" class="form-control {{ $errors->has('division_id') ? 'is-danger' : '' }}" required placeholder="Categoria" value="{{ isset($product) && !old('division_id') ? $product->division_id : old('division_id') }}">
            <option selected disabled hidden>-- Seleccione Categoria --</option>
            @foreach($divisions as $division)
            <option value="{{ $division->id }}" {{ (isset($product) && !old('division_id') ? $product->division_id : old('division_id')) == $division->id ? 'selected' : '' }}>{{ $division->name }}</option>
            @endforeach
        </select>
    </div>
</div> --}}

<div class="form-row form-group">
    <label class="col-3 control-label">Categoria/Sub-Categoria</label>
    <div class="col-6">
        <select name="subcategoria_id" class="categoria form-control {{ $errors->has('subcategoria_id') ? 'is-danger' : '' }}" placeholder="Sub-Categoria" value="{{ isset($product) && !old('subcategoria_id') ? $product->subcategoria_id : old('subcategoria_id') }}" required>
            <option value="" selected disabled hidden>-- Seleccione Categoria/Sub-Categoria --</option>
            @foreach($subcategorias as $subcategoria)
            <option value="{{ $subcategoria->id }}" {{ (isset($product) && !old('subcategoria_id') ? $product->subcategoria_id : old('subcategoria_id')) == $subcategoria->id ? 'selected' : '' }}>{{ $subcategoria->division->name }} / {{ $subcategoria->name }}</option>
            @endforeach
        </select>
    </div>
</div>

{{-- <div class="form-row form-group">
    <label class="col-3 control-label">Mayorista</label>
    <div class="col-6">
        <select name="mayorista_id" class="form-control {{ $errors->has('mayorista_id') ? 'is-danger' : '' }}" required placeholder="Mayorista" value="{{ isset($product) && !old('mayorista_id') ? $product->mayorista_id : old('mayorista_id') }}">
            <option selected disabled hidden>-- Seleccione Mayorista --</option>
            @foreach($mayoristas as $mayorista)
            <option value="{{ $mayorista->id }}" {{ (isset($product) && !old('mayorista_id') ? $product->mayorista_id : old('mayorista_id')) == $mayorista->id ? 'selected' : '' }}>{{ $mayorista->name }}</option>
            @endforeach
        </select>
    </div>
</div> --}}

<div class="form-row form-group">
    <label class="col-3 control-label">Imagen</label>
    <div class="col-6">
        <input type="file" name="image" style="min-width:50% !important" id="upload" preview="#image_preview" {{ isset($product) ? '' : 'required' }} accept="image/*">
    </div>
    <div class="col-6 offset-3 d-flex justify-content-center">
        <img src="#" id="image_preview" class="my-1 rounded" style="display: none;">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Puntos</label>
    <div class="col-6">
        <input name="points" type="number" class="form-control {{ $errors->has('points') ? 'is-danger' : '' }}" required placeholder="Puntos" value="{{ isset($product) && !old('points') ? $product->points : old('points') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Precio de Lista</label>
    <div class="col-6">
        <input name="price" type="number" class="form-control {{ $errors->has('price') ? 'is-danger' : '' }}" placeholder="Precio de Lista" value="{{ isset($product) && !old('price') ? $product->price : old('price') }}">
    </div>
</div>



@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.products') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $('.categoria').select2();
    });
</script>
@endpush