@extends('layouts.master')

@section('page-name', 'Crear nuevo Producto')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Añadir Producto
            </div>
            <div class="col-6 d-flex justify-content-end">
                {{-- <a href="{{ route('admin.products.create') }}" class="btn btn-sm btn-primary">Añadir</a> --}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
            @csrf
            @include('products.form')
        </form>
    </div>
</div>

@endsection