@extends('layouts.master')

@section('page-name', 'Precios del Producto')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Precios del Producto
            </div>
            <div class="col-6 d-flex justify-content-end">
                {{-- <a href="{{ route('admin.products.create') }}" class="btn btn-sm btn-primary">Añadir</a> --}}
                <a href="{{ route('admin.products') }}" class="btn btn-sm btn-primary">Volver</a>
            </div>
        </div>
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label class="col-3 control-label">Nombre</label>
                    <div class="col-6">
                        <span class="form-control">{{ $product->name }}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-3 control-label">Descripción</label>
                    <div class="col-6">
                        <span class="form-control">{{ $product->description }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h4>Precios</h4>
            </div>
        </div>

        @foreach ($product->mayoristas as $mayorista)
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="col-3 form-control">{{ $mayorista->name }}</label>
                        <div class="col-6">
                            <label class="form-control">{{ $mayorista->pivot->price ?? '-- sin definir --' }}</label>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <form method="POST" action="{{ route('admin.prices', $product) }}" class="mt-5">
            @csrf

            <div class="row">
                <div class="col-4">
                    <select name="company_id" class="form-control">
                        <option selected disabled hidden>-- Seleccione Mayorista --</option>
                        @foreach($mayoristas as $mayorista)
                        <option value="{{ $mayorista->id }}">{{ $mayorista->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary">Solicitar precio</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection