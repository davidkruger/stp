@extends('layouts.master')

@section('page-name', 'Productos')

@section('content')

<div class="card mb-3">
    
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Productos
            </div>
            
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.products.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
            
        </div>
    </div>
    <div class="card-body">
        @if ($products->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="w-200px">Nombre</th>
                            <th>Categoria</th>
                            <th>Puntos</th>
                            <th>Stock</th>
                            <th>Cantidad</th>
                            <th>Precio de Lista</th>
                            <th class="w-200px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->division->name }}</td>
                            <td>{{ $product->points }}</td>
                            <td>
                            @if ($product->stock==1)
                                <form action="{{ route('admin.stock', $product->id) }}" method="POST" id="si{{ $product->id }}">
                                    @csrf
                                    <input type="hidden" value="0" name="stock" id="stock" form="si{{ $product->id }}">
                                    <button type="submit" form="si{{ $product->id }}" class="btn btn-success" onclick="return confirm('DESEA CAMBIAR EL ESTADO DEL STOCK?');">Si</button>
                                </form>
                            @else
                                <form action="{{ route('admin.stock', $product->id) }}" method="POST" id="no{{ $product->id }}">
                                    @csrf
                                    <input type="hidden" value="1" name="stock" id="stock" form="no{{ $product->id }}">
                                    <button type="submit" form="no{{ $product->id }}" class="btn btn-danger" onclick="return confirm('DESEA CAMBIAR EL ESTADO DEL STOCK?');">No</button>
                                </form>
                            @endif
                            </td>
                            <td>{{ number_format($product->stock_cantidad,0,',','.') }}</td>
                            <td>{{ number_format($product->price,0,',','.') ?? '--' }}</td>
                            <td class="text-center d-flex justify-content-around">
                                @include('components.actions', [
                                    'resource'  => 'products',
                                    'title'     => $product->name,
                                    'record'    => $product->getKey(),
                                    'actions'   => [ 'update', 'delete' ]
                                ])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No se encontraron resultados</h3>
                <p class="text-muted">
                    Puedes agregar items haciendo click en el botón
                    <a href="{{ route('admin.products.create') }}" class="text-custom">
                        <ins>Añadir</ins>
                    </a>
                </p>
            </div>
        @endif
    </div>
</div>

@endsection