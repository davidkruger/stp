@extends('layouts.master')

@section('page-name', 'Asignacion de puntos')

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Tabla de asignación de puntos
            </div>
        </div>
    </div>
    <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="w-200px">Descripcion</th>
                            <th>Puntos</th>
                            <th class="w-200px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($acciones as $accion)
                        <tr>
                            <td>{{ $accion->id }}</td>
                            <td>{{ $accion->description }} @if ($accion->description == "Monto")
                                <small> (Monto a superar para habilitar los puntos extra)</small>
                            @endif
                            </td>
                            @if ($accion->description == "Oportunidad Ganada")
                                <td>Sea asignan los puntos de la oportunidad</td>
                                <td></td>
                            @else
                            <td><input type="number" class="form-control" name="puntos" value="{{ $accion->puntos }}" form="{{ $accion->id }}"></td>
                            <td class="text-center d-flex justify-content-around">
                                <form action="{{ route('admin.acciones.update', $accion->id) }}" method="POST" id="{{ $accion->id }}">
                                    @csrf
                                    <button class="btn btn-success">Guardar</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
</div>
@endsection