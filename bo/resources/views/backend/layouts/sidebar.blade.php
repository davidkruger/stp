<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a href="{{ route('admin.dashboard') }}" class="sidebar-brand d-flex align-items-center justify-content-center">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Samsung <sup>BOM</sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.dashboard' ]) ? 'active' : '' }}">
    <a href="{{ route('admin.dashboard') }}" class="nav-link">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Administración
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link {{ in_array(Route::currentRouteName(), [ 'admin.admins', 'admin.users', 'admin.solicitudes' ]) ? 'show' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapse-users" aria-expanded="true" aria-controls="collapse-user">
      <i class="fas fa-fw fa-cog"></i>
      <span>Usuarios</span>
    </a>
    <div id="collapse-users" class="collapse show" aria-labelledby="heading-users" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Usuarios:</h6>
        <a class="collapse-item {{ in_array(Route::currentRouteName(), [ 'admin.admins' ]) ? 'active' : '' }}" href="{{ route('admin.admins') }}">Administradores</a>
        <a class="collapse-item {{ in_array(Route::currentRouteName(), [ 'admin.users' ]) ? 'active' : '' }}" href="{{ route('admin.users') }}">Mayoristas y Resellers</a>
        <a class="collapse-item noticationLink {{ in_array(Route::currentRouteName(), [ 'admin.solicitudes' ]) ? 'active' : '' }}" href="{{ route('admin.solicitudes') }}">Solicitudes <b>{{\App\Models\User::where('state', 1)->count()}}</b></a>
      </div>
    </div>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Datos
  </div>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.divisions' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.divisions') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Categorias</span></a>
  </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.subcategorias' ]) ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.subcategorias') }}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Sub-Categorias</span></a>
    </li>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.acciones' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.acciones') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Asignacion de puntos</span></a>
  </li>
    
  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.images' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.images') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Imágenes</span></a>
  </li>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.companies' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.companies') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Integrador</span></a>
  </li>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.verticals' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.verticals') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Verticales</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Oportunidades
  </div>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.products' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.products') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Productos</span></a>
  </li>

  <!-- Nav Item - Charts -->
  <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.opportunities' ]) ? 'active' : '' }}">
    @php
        $pendientes = \App\Models\Opportunity::where('protegido', 'pendiente')->count();
    @endphp
    <a class="nav-link" href="{{ route('admin.opportunities') }}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Oportunidades</span>@if ($pendientes>0)
      <b class="badge badge-warning">{{\App\Models\Opportunity::where('protegido', 'pendiente')->count()}} Pendientes</b>
      @endif
    </a>
  </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.leads' ]) ? 'active' : '' }}">
      @php
          $cont_leads = \App\Models\Opportunity::where('state', 'lead')->count();
      @endphp
      <a class="nav-link" href="{{ route('admin.leads') }}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Leads</span>@if ($cont_leads>0)
        <br><b class="badge badge-warning">{{\App\Models\Opportunity::where('state', 'lead')->count()}} Pendientes</b>
        @endif
      </a>
    </li>
   <!-- Divider -->
   <hr class="sidebar-divider">

   <!-- Heading -->
   <div class="sidebar-heading">
     Ranking
   </div>

   <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.rank_puntos' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.rank_puntos') }}"><i class="fas fa-fw fa-chart-area"></i><span>Puntos STP</span></a>
   </li>

   <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.rank_oportunidades' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.rank_oportunidades') }}"><i class="fas fa-fw fa-chart-area"></i><span>Oportunidades Ganadas</span></a>
   </li>

   <li class="nav-item {{ in_array(Route::currentRouteName(), [ 'admin.rank_monto' ]) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.rank_monto') }}"><i class="fas fa-fw fa-chart-area"></i><span>Monto</span></a>
   </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->