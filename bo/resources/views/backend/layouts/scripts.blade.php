<!-- Scripts -->

<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- Core plugin JavaScript -->
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<!-- Page level plugin JavaScript -->
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
{{-- <scriptsrc="asset('vendor/datatables/jquery.dataTables.js')"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.js') }}"></script>--}}
<!-- Custom scripts for all pages -->
<script src="{{ asset('vendor/sb-admin-2/js/sb-admin.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/Only.js') }}"></script>
<script src="{{ asset('assets/backend/js/Preview.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{--<script src="{{ asset('assets/backend/js/DataTables.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 5 ]
                }
            },
            'colvis'
        ]
    } );
} );
</script>--}}



<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.4/axios.min.js" integrity="sha512-lTLt+W7MrmDfKam+r3D2LURu0F47a3QaW5nF0c6Hl0JDZ57ruei+ovbg7BrZ+0bjVJ5YgzsAWE+RreERbpPE1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
   var dataSrc = [];

   var table = $('#dataTable').DataTable({
      'initComplete': function(){
         var api = this.api();

         // Populate a dataset for autocomplete functionality
         // using data from first, second and third columns
         api.cells('tr', [0, 1, 2]).every(function(){
            // Get cell data as plain text
            var data = $('<div>').html(this.data()).text();           
            if(dataSrc.indexOf(data) === -1){ dataSrc.push(data); }
         });
         
         // Sort dataset alphabetically
         dataSrc.sort();
        
         // Initialize Typeahead plug-in
         $('.dataTables_filter input[type="search"]', api.table().container())
            .typeahead({
               source: dataSrc,
               afterSelect: function(value){
                  api.search(value).draw();
               }
            }
         );
      },
      "responsive": true,
       "autoWidth": true,
       dom: 'Bfrtip',
       lengthMenu: [
           [ 10, 25, 50 ],
           ['10 Registros', '25 Registros', '50 Registros' ]
       ],
       buttons: [
           {
               extend: 'copyHtml5',
               text: 'Copiar',
               exportOptions: {
                   columns: [ 0, ':visible' ]
               }
           },
           {
               extend: 'excelHtml5',
               text: 'Exportar en Excel',
               exportOptions: {
                   columns: ':visible'
               }
           },
           {
               extend: 'pdfHtml5',
               text: 'Exportar a PDF',
               exportOptions: {
                   columns: [ 0, 1, 2, 5 ]
               }
           },
           {
               extend: 'colvis',
               text: 'Ver Columnas'
           },
           {
               extend: 'pageLength', 
               text: 'Registros por Pagina'
           }
           
       ]

   });
});
/*   $(document).ready(function() {
   $('#dataTable').DataTable( {
       "responsive": true,
       "autoWidth": true,
       dom: 'Bfrtip',
       lengthMenu: [
           [ 10, 25, 50 ],
           ['10 Registros', '25 Registros', '50 Registros' ]
       ],
       buttons: [
           {
               extend: 'copyHtml5',
               text: 'Copiar',
               exportOptions: {
                   columns: [ 0, ':visible' ]
               }
           },
           {
               extend: 'excelHtml5',
               text: 'Exportar en Excel',
               exportOptions: {
                   columns: ':visible'
               }
           },
           {
               extend: 'pdfHtml5',
               text: 'Exportar a PDF',
               exportOptions: {
                   columns: [ 0, 1, 2, 5 ]
               }
           },
           {
               extend: 'colvis',
               text: 'Ver Columnas'
           },
           {
               extend: 'pageLength', 
               text: 'Registros por Pagina'
           }
           
       ]
   } );
} );*/
</script>
@yield('js')
@stack('scripts')