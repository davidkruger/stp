@extends('layouts.master')

@section('page-name', 'Administradores')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Administradores
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.admins.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($users->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="w-100px">ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            {{-- <th>Tipo</th> --}}
                            <th class="w-200px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->getKey() }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            {{-- <td>{{ $user->type }}</td> --}}
                            <td class="text-center d-flex justify-content-around">
                                @include('components.actions', [
                                    'resource'  => 'admins',
                                    'title'     => $user->name,
                                    'record'    => $user->getKey(),
                                    'actions'   => [ 'update', 'delete' ]
                                ])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No se encontraron resultados</h3>
                <p class="text-muted">
                    Puedes agregar items haciendo click en el botón
                    <a href="{{ route('admin.admins.create') }}" class="text-custom">
                        <ins>Añadir</ins>
                    </a>
                </p>
            </div>
        @endif
    </div>
</div>

@endsection