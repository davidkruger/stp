<div class="form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Nombre" value="{{ isset($user) && !old('name') ? $user->name : old('name') }}">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Email</label>
    <div class="col-6">
        <input name="email" type="email" class="form-control {{ $errors->has('email') ? 'is-danger' : '' }}" required placeholder="Email" value="{{ isset($user) && !old('email') ? $user->email : old('email') }}">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Contraseña</label>
    <div class="col-6">
        <input name="password" type="password" class="form-control {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} placeholder="Contraseña (mínimo 6 caracteres)">
    </div>
</div>

<div class="form-group">
    <label class="col-3 control-label">Confirmar Contraseña</label>
    <div class="col-6">
        <input name="password_confirmation" type="password" class="form-control {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} placeholder="Contraseña (mínimo 6 caracteres)">
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.admins') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>