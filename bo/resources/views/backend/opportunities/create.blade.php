@extends('layouts.master')
@section('content')
<div class="row justify-content-center">
    <div class="col">

                <form method="POST" action="{{ route('admin.opportunities.store') }}" id="enviar">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <label for="">Asignar a Reseller</label>
                        <select name="reseller" id="reseller" class="reseller">
                            <option value="lead">Lead</option>
                            @foreach ($resellers as $res)
                            @if ($res->state == 2)
                                <option value="{{ $res->id }}">{{ $res->company->name }} // {{ $res->name }}</option>
                            @endif
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="row">
                        @include('opportunities.form')
                    </div>
                </form>

    </div>
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('.reseller').select2();
    });
</script>
@endpush