<div class="container">
    <div class="step-app" id="demo">
        <ul class="step-steps">
          <li data-step-target="step1">Cliente Final</li>
          <li data-step-target="step2">Oportunidad</li>
          <li data-step-target="step3">Productos</li>
        </ul>
        <div class="step-content">
          <div class="step-tab-panel" data-step="step1">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Cliente Final</h3>
                </div>
                <div class="card-body">
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Nombre</label>
                        <div class="col-6">
                            <input name="nombre_cliente" type="text" required value="{{ isset($opportunity) && !old('nombre_cliente') ? $opportunity->nombre_cliente : old('nombre_cliente') }}"
                                class="form-control {{ $errors->has('nombre_cliente') ? 'is-danger' : '' }}" placeholder="Nombre Cliente">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Razon Social</label>
                        <div class="col-6">
                            <input name="razon_social_cliente" type="text" required value="{{ isset($opportunity) && !old('razon_social_cliente') ? $opportunity->razon_social_cliente : old('razon_social_cliente') }}"
                                class="form-control {{ $errors->has('razon_social_cliente') ? 'is-danger' : '' }}" placeholder="Razon Social">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Numero de Ruc</label>
                        <div class="col-6">
                            <input name="ruc_cliente" type="text" required value="{{ isset($opportunity) && !old('ruc_cliente') ? $opportunity->ruc_cliente : old('ruc_cliente') }}"
                                class="form-control {{ $errors->has('ruc_cliente') ? 'is-danger' : '' }}" placeholder="Numero de Ruc">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Telefono</label>
                        <div class="col-6">
                            <input name="telefono_cliente" type="text" required value="{{ isset($opportunity) && !old('telefono_cliente') ? $opportunity->name : old('telefono_cliente') }}"
                                class="form-control {{ $errors->has('telefono_cliente') ? 'is-danger' : '' }}" placeholder="Telefono">
                        </div>
                    </div>


                </div>
            </div>
          </div>
          <div class="step-tab-panel" data-step="step2">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Oportunidad</h3>
                </div>
                <div class="card-body">
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Nombre</label>
                        <div class="col-6">
                            <input name="name" type="text" required value="{{ isset($opportunity) && !old('name') ? $opportunity->name : old('name') }}"
                            class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" placeholder="Nombre">
                        </div>
                    </div>

                    <div class="form-row form-group">
                        <label class="col-3 control-label">Vertical del proyecto</label>
                        <div class="col-6">
                            <select name="vertical" class="form-control {{ $errors->has('vertical') ? 'is-danger' : '' }}" required placeholder="Vertical del Proyecto" value="{{ isset($opportunity) && !old('vertical') ? $opportunity->vertical_id : old('vertical') }}">
                                <option value="">-- Elija la vertical del proyecto --</option>
                                @foreach ($verticals as $vertical)
                                <option value="{{ $vertical->id }}">{{ $vertical->code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="form-row form-group">
                    <label class="col-3 control-label">Etapa del proyecto</label>
                    <div class="col-6">
                        <select name="probabilidad_cierre" class="form-control {{ $errors->has('probabilidad_cierre') ? 'is-danger' : '' }}" required placeholder="probabilidad_cierre" value="{{ isset($opportunity) && !old('probabilidad_cierre') ? $opportunity->probabilidad_cierre_id : old('probabilidad_cierre') }}">
                            <option value="">-- Elija la etapa del proyecto --</option>
                            @foreach ($probabilidades as $prob)
                            @if ($prob->porcentaje!=0)
                            <option value="{{ $prob->id }}">{{ $prob->description }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>

    
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Tipo de Oportunidad</label>
                        <div class="col-6">
                            <select name="tipo_oportunidad" onchange="oppPublico(this);" class="form-control {{ $errors->has('tipo_oportunidad') ? 'is-danger' : '' }}" required placeholder="tipo_oportunidad" value="{{ isset($opportunity) && !old('tipo_oportunidad') ? $opportunity->tipo_oportunidad : old('tipo_oportunidad') }}">
                                <option value="publico">Publica</option>
                                <option value="privado" selected>Privada</option>
                            </select>
                        </div>
                    </div>
                    <div id="publico" style="display: none">
                        <div only="tipo_oportunidad=publica">
                            <div class="form-row form-group">
                                <label class="col-3 control-label">Publica</label>
                                <div class="col-6">
                                    <select name="tipo_oportunidad_publica" onchange="tipoPublico(this);" class="form-control {{ $errors->has('tipo_oportunidad') ? 'is-danger' : '' }}" placeholder="tipo_oportunidad" value="{{ isset($opportunity) && !old('tipo_oportunidad') ? $opportunity->tipo_oportunidad : old('tipo_oportunidad') }}">
                                        <option value="pre_pliego" selected>PRE-PLIEGO</option>
                                        <option value="llamado_calle">Llamado en la calle</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <label class="col-3 control-label">Plazo de entrega</label>
                            <div class="col-6">
                                <input name="entrega" type="number" class="form-control {{ $errors->has('entrega') ? 'is-danger' : '' }}" placeholder="24 horas,  48 horas, 72 horas..." value="{{ isset($product) && !old('entrega') ? $product->entrega : old('entrega') }}">
                            </div>
                        </div>
                        <div id="llamado_calle" style="display: none">
                            <div only="tipo_oportunidad_publica=llamado_calle">
                                <div class="form-row form-group">
                                    <label class="col-3 control-label">Id del Llamado</label>
                                    <div class="col-6">
                                        <input name="idllamado" type="text"  value="{{ isset($opportunity) && !old('idllamado') ? $opportunity->idllamado : old('idllamado') }}"
                                        class="form-control {{ $errors->has('idllamado') ? 'is-danger' : '' }}" placeholder="Id del llamado publico">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="pre_pliego">
                            <div only="tipo_oportunidad_publica=pre_pliego">
                                <div class="form-row form-group">
                                    <label class="col-3 control-label">Fecha estimada de la publicación del llamado</label>
                                    <div class="col-6">
                                        <input name="publicacion_llamado" type="date" value="{{ isset($opportunity) && !old('publicacion_llamado') ? $opportunity->publicacion_llamado : old('publicacion_llamado') }}"
                                        class="form-control {{ $errors->has('publicacion_llamado') ? 'is-danger' : '' }}" placeholder="Fecha de publicacion del llamado">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="reseller_id" value="{{ Auth::user()->id }}">
        
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Fecha tentativa de cierre</label>
                        <div class="col-6">
                            <input name="tentative" type="date" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->company_id ? 'readonly disabled' : '' }}
                                class="form-control {{ $errors->has('tentative') ? 'is-danger' : '' }}" placeholder="Fecha de carga de la Oportunidad" value="{{ isset($opportunity) && !old('tentative') ? $opportunity->tentative : old('tentative') }}">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Presupuesto Estimado</label>
                        <div class="col-6">
                            <input type="number" name="presupuesto" placeholder="Presupuesto Estimado" class="form-control">
                        </div>
                    </div>

                    <div class="form-row form-group">
                            <label class="col-3 control-label">Fecha de entrega</label>
                            <div class="col-6">
                                <input name="fentrega" type="date" class="form-control {{ $errors->has('fentrega') ? 'is-danger' : '' }}" placeholder="Fecha de entrega" value="{{ isset($product) && !old('fentrega') ? $product->fentrega : old('fentrega') }}">
                            </div>
                     </div>

                </div>
            </div>
          </div>
          <div class="step-tab-panel" data-step="step3">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Productos</h3>
                </div>
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table table-bordered" id="example" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="w-200px">Nombre</th>
                                    <th>Categoria</th>
                                    <th class="w-200px">SubCat.</th>
                                    <th class="w-200px">Precio de Lista</th>
                                    <th>En Stock</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->division->name }}</td>
                                        <td>{{ $product->subcategoria->name }}</td>
                                        <td>{{ number_format($product->price,0,',','.') }}</td>
                                        <td>
                                        @if ($product->stock==1)
                                            Disponible
                                        @else
                                            No disponible
                                        @endif
                                        </td>
                                        <td><img src="{{ route('storage.get', [ 'file' => $product->image_id ]) }}" width="100"></td>
                                        <td><button class="btn btn-warning" onclick="modal({{ $product->id }}, '{{ $product->name }}');" type="button">Agregar</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="ModalTitle"></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <input name="product_id" id="product_id" type="hidden">
                                    <input type="hidden" value="{{Auth::user()->id.$mytime =date('mdYHis')}}" name="idOpportunity" id="idOpportunity" >
                                    <input name="cantidad" id="cantidad" type="number" class="form-control" placeholder="Cantidad"> <br>
                                    <a style="text-decoration:none;" data-toggle="collapse" href="#collapse" class="btn btn-warning">Solicitar Precio <i class="fas fa-arrow-down"></i></a>
                                    <div id="collapse" class="panel-collapse collapse">
                                        <br>
                                        <input name="precio_sol" id="precio_sol" type="number" class="form-control" placeholder="Precio Solicitado">
                                        <br>
                                        <input name="motivo" id="motivo" type="text" class="form-control" placeholder="Motivo de Precio Solicitado">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                  <button type="button" class="btn btn-success" onclick="addProductOpp();">Agregar a la oportunidad</button>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="step-footer">
          <button data-step-action="prev" class="btn btn-info">Anterior</button>
          <button data-step-action="next" class="btn btn-info">Siguiente</button>
          <button type="submit" form="enviar" class="btn btn-success">Guardar</button>
        </div>
    </div>
    <div class="cartCont">
        <div class="row">
            <div class="col-2">
                <button type="button" class="showCart">
                    <svg xmlns="http://www.w3.org/2000/svg" style="height: 20px" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
                    </svg>
                </button>
                <button type="button" class="hideCart">
                    <svg xmlns="http://www.w3.org/2000/svg" style="height: 20px" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                    </svg>
                </button>
                <ul>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" style="height: 30px" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0" />
                          </svg><br>
                        <span id="cant">0</span> Items
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" style="height: 30px" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 5v2m0 4v2m0 4v2M5 5a2 2 0 00-2 2v3a2 2 0 110 4v3a2 2 0 002 2h14a2 2 0 002-2v-3a2 2 0 110-4V7a2 2 0 00-2-2H5z" />
                          </svg> <br>
                          <span id="puntos">0</span> Puntos
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" style="height: 30px" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                          </svg><br>
                          <span id="precio_lista">0</span> $
                    </li>
                </ul>
            </div>
            <div class="col-10">
                <div class="cartItems" id="cartItems" style="display: none">

                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/jquery.steps@1.1.1/dist/jquery-steps.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
                $('.hideCart').hide();
                var dataSrc = [];

   var table = $('#example').DataTable({
      'initComplete': function(){
         var api = this.api();

         // Populate a dataset for autocomplete functionality
         // using data from first, second and third columns
         api.cells('tr', [0, 1, 2]).every(function(){
            // Get cell data as plain text
            var data = $('<div>').html(this.data()).text();           
            if(dataSrc.indexOf(data) === -1){ dataSrc.push(data); }
         });
         
         // Sort dataset alphabetically
         dataSrc.sort();
        
         // Initialize Typeahead plug-in
         $('.dataTables_filter input[type="search"]', api.table().container())
            .typeahead({
               source: dataSrc,
               afterSelect: function(value){
                  api.search(value).draw();
               }
            }
         );
      }
   });
                $('.showCart').click(function () {
                    $('.cartCont').addClass('active');
                    $('.hideCart').show();
                    $('.showCart').hide();
                    document.getElementById("cartItems").style.display = "block";
                });
                $('.hideCart').click(function () {
                    document.getElementById("cartItems").style.display = "none";
                    $('.cartCont').removeClass('active');
                    $('.hideCart').hide();
                    $('.showCart').show();
                })
        } );
    </script>
    <script>
        function modal(id,name){
            $('#Modal').modal('show');
            ModalTitle.innerHTML=name;
            product_id.value=id;
        }
        function oppPublico(selectObject){
            var value = selectObject.value;
            if (value == 'publico'){
                document.getElementById("publico").style.display = "block";
            }else{
                document.getElementById("publico").style.display = "none";
            }
        }
        function tipoPublico(selectObject){
            var value = selectObject.value;
            if (value == 'pre_pliego'){
                document.getElementById("pre_pliego").style.display = "block";
                document.getElementById("llamado_calle").style.display = "none";
            }else{
                document.getElementById("pre_pliego").style.display = "none";
                document.getElementById("llamado_calle").style.display = "block";
            }
        }
        $('#demo').steps({
            
        });
        function listProducts(products) {
            var contenido = '<div class="row"><div class="col-4">Producto</div><div class="col-2">Cantidad</div><div class="col-2">Precio Lista</div><div class="col-2">Precio Solicitado</div><div class="col-1">  </div></div>';
            var cant = 0;
            var puntos = 0;
            var precio_lista = 0;
/*            for (var i = 2; i < 4; i++) {
                contenido = contenido + '<div class="row">'+products[i].id+'</div>';
            }*/
            Object.keys(products).forEach(function(k){
                contenido = contenido+'<div class="row"><div class="col-4">'+products[k].name+'</div><div class="col-2">'+products[k].cantidad+'</div><div class="col-2">'+products[k].precio_lista+'</div>';
                if (products[k].precio_solicitado!=null){
                    contenido = contenido + '<div class="col-2">'+products[k].precio_solicitado+'</div>';
                }else{
                    contenido = contenido + '<div class="col-2">'+' '+'</div>'
                }
                contenido = contenido+'<button style="background-color:red" type="button" onClick="deleteProduct('+products[k].id+')" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></div>';
                cant = cant+1;
                puntos = puntos+products[k].puntos;
                precio_lista = precio_lista + (products[k].precio_lista * products[k].cantidad);
                //console.log(k + '-' + products[k].name);
            })
            document.getElementById('cartItems').innerHTML = contenido;
            document.getElementById('cant').innerHTML = cant;
            document.getElementById('puntos').innerHTML = puntos;
            document.getElementById('precio_lista').innerHTML = precio_lista;
        };
        function addProductOpp() {
            cantidad = document.querySelector('#cantidad').value;
            
            if (cantidad=="") {
                return alert('Debe ingresar la cantidad');
            }
           axios.post( '{{ url('/backend/addProductOpp') }}',{
                    'product_id':document.querySelector('#product_id').value,
                    'cantidad':document.querySelector('#cantidad').value,
                    'precio_sol':document.querySelector('#precio_sol').value,
                    'motivo':document.querySelector('#motivo').value,
                }).then(function (response) {
                    cantidad.value="";
                    product_id.value="";
                    precio_sol.value="";
                    motivo.value="";
                    $('#Modal').modal('hide');
                    var products = response.data;
                    listProducts(products);
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
        };
        function deleteProduct(item) {

            axios.post( '{{ url('/backend/deleteOppProduct') }}',{
                    'id':item,
                }).then(function (response) {
                    console.log(response);
                    var products = response.data;
                    listProducts(products);
                }).catch(function (error) {
                    console.log(error);
                });
            };
        
    </script>
@endpush