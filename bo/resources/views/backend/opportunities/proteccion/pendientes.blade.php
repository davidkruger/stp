@extends('layouts.master')

@section('page-name', 'Oportunidades')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Oportunidades Pendientes
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.opp_rechazadas') }}" class="btn btn-sm btn-danger"><i class="far fa-thumbs-down"></i> Rechazados</a>
                <a href="{{ route('admin.opp_protegidas') }}" class="btn btn-sm btn-success"><i class="fas fa-thumbs-up"></i> Protegidos</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($opportunities->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Compañia</th>
                            <th>Oportunidad</th>
                            <th>Cliente</th>
                            <th>% Cierre</th>
                            <th>Fecha Cierre</th>
                            <th>Estado</th>
                            <th>Tipo de Oportunidad</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($opportunities as $opportunity)
                            <tr>
                                <th>{{ $opportunity->id }}</th>
                                <th>{{ $opportunity->reseller->name }}</th>
                                <th>{{ $opportunity->reseller->company->name }}</th>
                                <th>{{ $opportunity->name }}</th>
                                <th>{{ $opportunity->nombre_cliente }}</th>
                                <th>{{ $opportunity->probabilidad_cierre->description }}</th>
                                <th>{{ $opportunity->closes }}</th>
                                <th>{{ $opportunity->state }}</th>
                                <th>{{ $opportunity->tipo_oportunidad }}</th>
                                <th><a href="{{ url('backend/proteccion/oportunidad/'.$opportunity->id) }}" class="btn btn-warning">Ver</a></th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No tienes oportunidades pendientes</h3>
            </div>
        @endif
    </div>
</div>

@endsection