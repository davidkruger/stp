@extends('layouts.master')

@section('page-name', 'Oportunidades')

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Oportunidad
            </div>
            <div class="col-6 d-flex justify-content-end">
                @if ($opportunity->protegido == 'pendiente')
                    <form action="{{ route('admin.proteccion_state') }}" method="POST" id="si">
                        @csrf
                        <input type="hidden" form="si" name="protegido" value="si">
                        <input name="opp_id" id="opp_id" type="hidden" form="si" value="{{ $opportunity->id }}">
                        <button type="submit" form="si" class="btn btn-sm btn-success"><i class="fas fa-thumbs-up"></i> Proteger</button>
                    </form>
                    <form action="{{ route('admin.proteccion_state') }}" method="POST" id="no">
                        @csrf
                        <input type="hidden" form="no" name="protegido" value="no">
                        <input name="opp_id" id="opp_id" type="hidden" form="no" value="{{ $opportunity->id }}">
                        <button type="submit" form="no" class="btn btn-sm btn-danger"><i class="far fa-thumbs-down"></i> No Proteger</button>
                    </form>
                @endif
                @if ($opportunity->protegido == 'si')
                    <button type="button" onClick="modal3('{{ $opportunity->name }}',{{ $opportunity->id }});" class="btn btn-sm btn-success"><i class="fas fa-thumbs-up"></i>Negocio Protegido</button>
                    <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="ModalTitle3"></h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <b>Desea retirar la protección de este negocio?</b>
                                <input name="protegido" id="roteger" type="hidden" value="no" form="retirar_proteccion">
                                <input name="opp_id" id="opp_id3" type="hidden" form="retirar_proteccion">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                              <button type="submit" class="btn btn-warning" form="retirar_proteccion">Retirar Protección</button>
                              <form action="{{ url('backend/proteccion/oportunidad') }}" method="POST" id="retirar_proteccion">
                                @csrf
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                @endif
                @if ($opportunity->protegido == 'no')
                    <button type="button" onClick="modal2('{{ $opportunity->name }}',{{ $opportunity->id }});" class="btn btn-sm btn-danger"><i class="far fa-thumbs-down"></i> Negocio sin protección</button>
                    <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="ModalTitle2"></h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <input name="protegido" type="hidden" id="no_proteger" value="si" form="otorgar_proteccion">
                                <b>Desea proteger este negocio?</b>
                                <input name="opp_id" id="opp_id2" type="hidden" form="otorgar_proteccion">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                              <button type="submit" class="btn btn-success" form="otorgar_proteccion">Proteger</button>
                              <form action="{{ url('backend/proteccion/oportunidad') }}" method="POST" id="otorgar_proteccion">
                                @csrf
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="card border-left-{{ [
                    'lead'      => 'primary',
                    'open'      => 'info',
                    'won'       => 'success',
                    'lost'      => 'danger'
                ][$opportunity->state] }}">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col">
                            <h3>{{ $opportunity->name }}</h3>
                        </div>
                        <div>
                            Etapa del negocio: <b class="badge badge-warning">{{ $opportunity->probabilidad_cierre->description }}</b>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                            @if ($opportunity->state == 'lost')
                            <dt class="row"><span class="col-3" style="font-weight: 100">Motivo de la perdida de la oportunidad:</span> <p class="col-9">{{ $opportunity->reason }}</p></dt>
                            @endif
    
                            <h3>Datos de la Oportunidad</h3><br>
                                <dt class="row"><span class="col-3" style="font-weight: 100">Fecha tentativa de cierre:</span> <p class="col-9">{{ $opportunity->tentative }}</p></dt>
                                @if ($opportunity->reseller_id!=null)
                                <dt class="row"><span class="col-3" style="font-weight: 100">Reseller:</span> <p class="col-9">{{ $opportunity->reseller->name }}</p></dt>
                                <dt class="row"><span class="col-3" style="font-weight: 100">Canal Integrador:</span> <p class="col-9">{{ $opportunity->reseller->company->name }}</p></dt>
                                @endif
                                <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad:</span><p class="col-9"> {{ $opportunity->tipo_oportunidad }}</p></dt>
                                @if ($opportunity->tipo_oportunidad=='publico')
                                    @if ($opportunity->tipo_publico=='pre_pliego')
                                    <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad publica:</span><p class="col-9"> PRE-PLIEGO</p></dt>
                                    <dt class="row"><span class="col-3" style="font-weight: 100">Fecha estimada de publicación del llamado:</span><p class="col-9"> {{ $opportunity->pre_pliego_fecha }} </p></dt>
                                    <dt class="row" ><span class="col-3" style="font-weight: 100">Plazo de entrega:</span><p class="col-9"> {{ $opportunity->entrega }}</p></dt>

                                    @else

                                    <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad publica:</span><p class="col-9"> LLAMADO A LA CALLE</p></dt>
                                    <dt class="row"><span class="col-3" style="font-weight: 100">ID del llamado:</span><p class="col-9"> {{ $opportunity->llamado_calle_idllamado }}</p></dt>
                                    <dt class="row" ><span class="col-3" style="font-weight: 100">Plazo de entrega:</span><p class="col-9"> {{ $opportunity->entrega }}</p></dt>
                                    @endif
                                @endif
                                <dt class="row" ><span class="col-3" style="font-weight: 100">Presupuesto estimado:</span><p class="col-9"> {{ number_format($opportunity->presupuesto,0,',','.') }}</p></dt>
                                <dt class="row" ><span class="col-3" style="font-weight: 100">Monto total precio de lista:</span><p class="col-9"> {{ number_format($monto_prod_lista,0,',','.') }}</p></dt>
                                <dt class="row" ><span class="col-3" style="font-weight: 100">Monto total precio solicitado:</span><p class="col-9"> {{ number_format($monto_prod_parcial,0,',','.') }}</p></dt>
                                <dt class="row" ><span class="col-3" style="font-weight: 100">Fecha de entrega:</span><p class="col-9"> {{ $opportunity->fentrega }}</p></dt>
                            <br>
                            <h3>Datos del Cliente Final</h3><br>
                                <dt class="row"><span class="col-3" style="font-weight: 100">Nombre:</span><p class="col-9"> {{ $opportunity->nombre_cliente }}</p></dt>
    
                                <dt class="row"><span class="col-3" style="font-weight: 100">Razon Social:</span><p class="col-9"> {{ $opportunity->razon_social_cliente }}</p></dt>
    
                                <dt class="row"><span class="col-3" style="font-weight: 100">RUC:</span><p class="col-9"> {{ $opportunity->ruc_cliente }}</p></dt>
    
                                <dt class="row"><span class="col-3" style="font-weight: 100">Telefono:</span><p class="col-9"> {{ $opportunity->telefono_cliente }}</p></dt>
    
                            <br><h3>Puntos que se otorgaran al ganar la oportunidad</h3><br>
    
                                <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por productos:</span><p class="col-6"> <b class="badge badge-success">{{ number_format($puntos_prod,0,',','.') }}p.</b></p></dt>
                                <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por estatus de la compañia:</span><p class="col-6"> <b class="badge badge-success">{{ number_format($puntos_estatus,0,',','.') }}p.</b></p></dt>
                                <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por crear la oportunidad:</span><p class="col-6"> <b class="badge badge-success">{{ number_format($puntos_crear,0,',','.') }}p.</b></p></dt>
                                @if ($monto_superar<=$monto_prod_parcial)
                                <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por superar {{ $monto_superar }}$:</span><p class="col-6"> <b class="badge badge-success">{{ number_format($puntos_superar,0,',','.') }}p.</b></p></dt>
                                @else
                                <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por superar {{ $monto_superar }}$:</span><p class="col-6"> <b class="badge badge-danger">{{ number_format($puntos_superar,0,',','.') }}p.</b></p></dt>
                                @endif
                                <dt class="row"> <span class="col-6" style="font-weight: 100">Total de puntos que se le otorgaran al cerrar exitosamente la oportunidad:</span><p class="col-6"> 
                                    @if ($monto_superar<=$monto_prod_parcial)
                                    <b class="badge badge-warning">{{ number_format($puntos_prod + $puntos_estatus + $puntos_crear + $puntos_superar,0,',','.') }}p.</b>
                                    @else 
                                    <b class="badge badge-warning">{{ number_format($puntos_prod + $puntos_estatus + $puntos_crear,0,',','.') }}p.</b>
                                    @endif
                                </p>
                                </dt>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-left-primary mt-4">
                <div class="card-header">
                    <h2>Productos</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($products as $product)
                        <div class="col-12 mb-3">
                            <dl class="row mb-2">
                                <dt class="col-4">Nombre: {{ $product->product->name }}</dt>
    
                                <dt class="col-2">SKU: {{ $product->product->sku }}</dt>
    
                                <dt class="col-2">Stock:@if ($product->product->stock==1)
                                    Disponible
                                @else
                                    No disponible
                                @endif
                                </dt>
                                <dt class="col-2">Precio de Lista: {{ number_format($product->product->price,0,',','.') }}</dt>
    
                                <dt class="col-2">Puntos: {{ number_format($product->product->points,0,',','.') }}</dt>
                            </dl>
                    <div id="detalles{{ $product->id }}" style="display: block">
                            <dl>
                                @if ($product->state=='pendiente')
                                    @if ($product->id_ofertante != Auth::user()->id)
                                    <form action="{{ url('backend/oportunidad/precio/'.$product->id) }}" id="aprobar{{ $product->id }}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{ $product->price_solicitado }}" name="precio_solicitado" form="aprobar{{ $product->id }}">
                                    </form>
                                    <dt><button type="submit" form="aprobar{{ $product->id }}" class="btn btn-primary">Aprobar</button>
                                    @else
                                    <dt><b class="badge badge-danger">Pendiente de aprobación</b>
                                    @endif
                                @else
                                    <dt><b class="badge badge-success">Aprobado</b>
                                @endif
                                <button class="btn btn-warning" style="float: right" type="button" onclick="editar({{ $product->id }});">Asignar precio especial para esta oportunidad</button></dt>
                            </dl>
                            <dl class="row mb-2">
                                @if ($product->state=='pendiente')
                                    <dt class="col-2">Cantidad: <b class="badge badge-primary">{{ $product->quantity }}</b></dt>
                                    <dt class="col-2">Precio Solicitado: <b class="badge badge-primary">{{ $product->price_solicitado }}</b></dt>
                                    <dd class="col-3">Motivo: {{ $product->motivo }}</dt>
                                @else
                                    <dt class="col-2">Cantidad: <b class="badge badge-success">{{ $product->quantity }}</b></dt>
                                    <dt class="col-2">Precio Aprobado: <b class="badge badge-success">{{ $product->price }}</b></dt>
                                @endif
                                
                            </dl>
                    </div>
                    <div id="editable{{ $product->id }}" style="display: none">
                        <dl>
                                <dt><button class="btn btn-success" type="submit" form="precio{{ $product->id }}">Guardar</button> <button onclick="cancelar_edit({{ $product->id }})" class="btn btn-danger">Cancelar</button></dt>
                        </dl>
                    <form action="{{ url('backend/oportunidad/precio/'.$product->id) }}" method="POST" id="precio{{ $product->id }}">
                        @csrf
                        <dl class="row mb-2">
                                <dt class="col-2"><input form="precio{{ $product->id }}" type="number" class="form-control" name="quantity" placeholder="Cantidad" value="{{ $product->quantity }}"></dt>
                                <dt class="col-2"><input form="precio{{ $product->id }}" type="number" class="form-control" name="precio_solicitado" placeholder="Precio asignado" value="{{ $product->price_solicitado }}"></dt>
                                <dt class="col-2"><input form="precio{{ $product->id }}" type="text" class="form-control" name="motivo" placeholder="Motivo de la asignacion de precio"></dt>
                        </dl>
                    </form>
                    </div>
                            <dl>
                                <dt><a style="text-decoration:none;" data-toggle="collapse" href="#collapse{{ $product->id }}" class="">Ver historial de precios solicitados</a></dt>
                            </dl>
                        <div id="collapse{{ $product->id }}" class="panel-collapse collapse">
                            <section class="property_location">
                              <div style="width: 100%">
                                @if (count($product->historiales)>0)
                                @foreach ($product->historiales as $his)
                                    <dl class="row mb-2">
                                        <dt class="col-2">Cantidad: {{ $his->quantity }}</dt>
                                        <dt class="col-2">Precio: {{ $his->price_solicitado }}</dt>
                                        <dt class="col-2">Estado: {{ $his->state }}</dt>
                                    </dl>
                                @endforeach
                                @else
                                <dl>
                                    <dt><b>No ha realizado mas solicitudes para este producto</b></dt>
                                </dl>
                                @endif
                              </div>
                            </section>
                        </div>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card border-left-secondary mt-4">
                <div class="card-header">
                    <h2>Historico de Etapas del Negocio</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <dt class="row"><span class="col-1"> </span><span class="col-2">Fecha</span> <span class="col-4">Etapa del proyecto</span></dt>
                            @php
                                $etapa_count = count($etapas);
                            @endphp
                            @foreach ($etapas as $etapa)
                                <dt class="row"><small class="col-1"><b class=" badge badge-primary">{{ $etapa_count }}</b></small><small class="col-2">{{ date("d/m/Y", strtotime($etapa->created_at)) }}</small><span class="col-4">{{ $etapa->etapa_negocio->description }}</span></dt>
                                @php
                                    $etapa_count = $etapa_count - 1;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    function editar(id){
      document.getElementById("detalles"+id).style.display = "none";
      document.getElementById("editable"+id).style.display = "block";
    }
    function cancelar_edit(id){
      document.getElementById("detalles"+id).style.display = "block";
      document.getElementById("editable"+id).style.display = "none";
    }
    function modal1(name,id){
        $('#Modal1').modal('show');
        ModalTitle1.innerHTML=name;
        opp_id1.value=id;
    }
    function modal2(name,id){
        $('#Modal2').modal('show');
        ModalTitle2.innerHTML=name;
        opp_id2.value=id;
    }
    function modal3(name,id){
        $('#Modal3').modal('show');
        ModalTitle3.innerHTML=name;
        opp_id3.value=id;
    }
  </script>
@endsection