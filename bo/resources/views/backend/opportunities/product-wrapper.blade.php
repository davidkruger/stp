<div class="form-row product-wrapper mb-3 {{ !isset($product) ? 'd-none' : '' }}" {{ !isset($product) ? 'id=new' : '' }}>
    <input type="hidden" name="product[]" value="{{ isset($product) ? $product->id : '' }}">
    <div class="col-2">
        <input type="number" name="quantity[]" class="form-control" placeholder="Cantidad" value="{{ isset($product) ? $product->pivot->quantity : '' }}"></input>
    </div>
    <div class="col-10">
        <select name="products[]" class="form-control">
        <option selected disabled hidden>-- Seleccione Producto --</option>
        @foreach($products as $product_data)
        <option value="{{ $product_data->id }}" {{ isset($product) && $product->id == $product_data->id ? 'selected' : '' }}>{{ $product_data->description }} @if($product_data->stock === 0)[Sin Stock]@else[Con Stock]@endif</option>
        @endforeach
        </select>
    </div>
</div>