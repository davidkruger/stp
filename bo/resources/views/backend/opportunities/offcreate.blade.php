@extends('layouts.master')

@section('page-name', 'Crear nueva Oportunidad')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Añadir Oportunidad
            </div>
            <div class="col-6 d-flex justify-content-end">
                {{-- <a href="{{ route('admin.opportunities.create') }}" class="btn btn-sm btn-primary">Añadir</a> --}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.opportunities.store') }}">
            @csrf
            @include('opportunities.form')
        </form>
    </div>
</div>

@endsection