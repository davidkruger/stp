<div class="form-row form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" required
            value="{{ isset($opportunity) && !old('name') ? $opportunity->name : old('name') }}"
            class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" placeholder="Nombre">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Compañía</label>
    <div class="col-6">
        <select name="company_id" required
            value="{{ isset($opportunity) && !old('company_id') ? $opportunity->company_id : old('company_id') }}"
            class="form-control {{ $errors->has('company_id') ? 'is-danger' : '' }}" placeholder="Compañía">
            <option value="" selected disabled hidden>-- Seleccione Compañía --</option>
            @foreach($companies as $company)
            @if ($company->type !== 'company') @continue @endif
            <option value="{{ $company->id }}" {{ (isset($opportunity) && !old('company_id') ? $opportunity->company_id : old('company_id')) == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Reseller</label>
    <div class="col-6">
        <select name="reseller_id"
            value="{{ isset($opportunity) && !old('reseller_id') ? $opportunity->reseller_id : old('reseller_id') }}"
            class="form-control {{ $errors->has('reseller_id') ? 'is-danger' : '' }}" placeholder="Reseller (opcional)">
            <option value="" selected>Reseller (opcional)</option>
            @foreach($companies as $reseller)
            @if ($reseller->type !== 'reseller') @continue @endif
            <option value="{{ $reseller->id }}" {{ (isset($opportunity) && !old('reseller_id') ? $opportunity->reseller_id : old('reseller_id')) == $reseller->id ? 'selected' : '' }}>{{ $reseller->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Mayorista</label>
    <div class="col-6">
        <select name="mayorista_id"
            value="{{ isset($opportunity) && !old('mayorista_id') ? $opportunity->mayorista_id : old('mayorista_id') }}"
            class="form-control {{ $errors->has('mayorista_id') ? 'is-danger' : '' }}" placeholder="Mayorista">
            <option value="" selected>Mayorista (opcional)</option>
            @foreach($companies as $mayorista)
            @if ($mayorista->type !== 'mayorista') @continue @endif
            <option value="{{ $mayorista->id }}" {{ (isset($opportunity) && !old('mayorista_id') ? $opportunity->mayorista_id : old('mayorista_id')) == $mayorista->id ? 'selected' : '' }}>{{ $mayorista->name }}</option>
            @endforeach
        </select>
    </div>
</div>

@if (isset($opportunity))
<div class="form-row form-group">
    <label class="col-3 control-label">Fecha de carga</label>
    <div class="col-2">
        <input type="date" class="form-control" disabled placeholder="Fecha de carga de la Oportunidad" value="{{ isset($opportunity) ? substr($opportunity->created_at, 0, 10) : '' }}">
    </div>
</div>
@endif

<div class="form-row form-group">
    <label class="col-3 control-label">Fecha tentativa de cierre</label>
    <div class="col-2">
        <input name="tentative" type="date" class="form-control {{ $errors->has('tentative') ? 'is-danger' : '' }}" required placeholder="Fecha de tentativa de cierre" value="{{ isset($opportunity) && !old('tentative') ? $opportunity->tentative : old('tentative') }}">
    </div>
</div>

{{--<div class="form-row form-group">
    <label class="col-3 control-label">Fecha de cierre real</label>
    <div class="col-2">
        <input name="closes" type="date" class="form-control {{ $errors->has('closes') ? 'is-danger' : '' }}" placeholder="Fecha real de cierre de la Oportunidad (opcional)" value="{{ isset($opportunity) && !old('closes') ? $opportunity->closes : old('closes') }}">
    </div>
</div>--}}

@if (isset($opportunity))
<div class="form-row form-group">
    <label class="col-3 control-label">Estado de la Oportunidad (%)</label>
    <div class="col-1 d-flex justify-content-center align-items-center">
        {{ $opportunity->state }}
    </div>
    <div class="col-5">
        <div class="progress" style="height: 100%">
            <div class="progress-bar bg-{{ $opportunity->status > 0 ? 'info' : 'secondary' }}" role="progressbar"
                style="width: {{ $opportunity->status > 0 ? $opportunity->status : 100 }}%;"
                aria-valuenow="{{ $opportunity->status }}" aria-valuemin="0" aria-valuemax="100">{{ $opportunity->status }}%</div>
        </div>
        {{-- <input name="status" type="number" class="form-control {{ $errors->has('status') ? 'is-danger' : '' }}" disabled placeholder="Estado de la Oportunidad (en porcentaje)" value="{{ isset($opportunity) && !old('status') ? $opportunity->status : old('status') }}"> --}}
    </div>
</div>
@endif

<div class="form-row form-group">
    <label class="col-3 control-label">Productos</label>
    <div class="col-6 products-container">
        {{-- verticales --}}
        <div class="form-row">
            <div class="col-2">
                <label class="control-label">Qty</label>
            </div>
            <div class="col-10">
                <label class="control-label">Producto</label>
            </div>
        </div>
        @if(isset($opportunity))
            @foreach($opportunity->products as $product)
                @include('opportunities.product-wrapper', [
                    'product' => $product,
                ])
            @endforeach
        @endif
    </div>
</div>

{{-- empty product wrapper --}}
@include('opportunities.product-wrapper', [ 'product' => null ])

@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.opportunities') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('assets/backend/js/Opportunity.js') }}"></script>
    <script type="text/javascript">new Opportunity({{ isset($opportunity) ? $opportunity->id : '' }});</script>
@endpush