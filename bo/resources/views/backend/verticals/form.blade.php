@if ($errors->any())
<div class="row">
    <div class="col-6 offset-3 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="form-row form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="code" type="text" class="form-control {{ $errors->has('code') ? 'is-danger' : '' }}" required placeholder="Código" value="{{ isset($vertical) && !old('code') ? $vertical->code : old('code') }}">
    </div>
</div>


<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.verticals') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>