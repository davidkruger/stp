@extends('layouts.master')

@section('page-name', 'Editar Vertical')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Editar Vertical
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.verticals.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.verticals.update', $vertical->id) }}">
            @method('PUT')
            @csrf
            @include('verticals.form')
        </form>
    </div>
</div>

@endsection