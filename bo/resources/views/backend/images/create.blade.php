@extends('layouts.master')

@section('page-name', 'Crear nueva Imágen')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Añadir Imágen
            </div>
            <div class="col-6 d-flex justify-content-end">
                {{-- <a href="{{ route('admin.images.create') }}" class="btn btn-sm btn-primary">Añadir</a> --}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.images.store') }}" enctype="multipart/form-data">
            @csrf
            @include('images.form')
        </form>
    </div>
</div>

@endsection