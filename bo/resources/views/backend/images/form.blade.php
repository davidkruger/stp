<div class="form-group">
    <label class="col-3 control-label">Imágen</label>
    <div class="col-6">
        <input name="file" type="file" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Imágen" value="{{ isset($image) && !old('file') ? $image->file : old('file') }}" accept="image/*">
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col-lg-6 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.images') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>