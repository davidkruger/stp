@if (!isset($actions) || (isset($actions) && in_array('show', $actions)))
    <a href= "{{ url('/backend/'.$resource.'/'.$record) }}" class="text-primary" data-toggle="tooltip" data-placement="left" title="Ver Detalle">
        <i class="fas fa-bars"></i><!--
    --></a>
@endif

@if (!isset($actions) || (isset($actions) && in_array('update', $actions)))
    <a href="{{ url('/backend/'.$resource.'/'.$record.'/edit') }}" class="text-success" data-toggle="tooltip" data-placement="top" title="Editar {{ $label ?? '' }}">
        <i class="fas fa-pen"></i><!--
    --></a>
@endif

@if (!isset($actions) || (isset($actions) && in_array('delete', $actions)))
    <a href="{{ url('/backend/'.$resource) }}" class="text-danger delete-record b-0" data-toggle="tooltip" data-placement="right" title="Eliminar {{ $label ?? '' }}" data-record="{{ $record }}" data-record-title="{{ $title }}">
        <i class="fas fa-trash"></i><!--
    --></a>

    <form method="POST" action="/backend/{{ $resource }}/{{ $record }}" id="delete-form-{{ $record }}" class="d-none">
        @method('DELETE')
        @csrf
        <button type="submit"></button>
    </form>
@endif
{{-- acciones para solicitudes --}}
@if (!isset($actions) || (isset($actions) && in_array('aceptar', $actions)))
    <form action="{{ url('/backend/solicitudes/'.$record) }}" id="aceptar{{ $user->id }}" method="POST">
        @csrf
        <input type="hidden" name="state" id="state" value="2">
        <button type="submit" form="aceptar{{ $user->id }}" class="btn btn-success">Aceptar</button>
    </form>
@endif

@if (!isset($actions) || (isset($actions) && in_array('rechazar', $actions)))
    <form action="{{ url('/backend/solicitudes/'.$record) }}" id="rechazar{{ $user->id }}" method="POST">
        @csrf
        <input type="hidden" name="state" id="state" value="3">
        <button type="submit" form="rechazar{{ $user->id }}" class="btn btn-danger">Rechazar</button>
    </form>
@endif