@extends('layouts.master')

@section('page-name', 'Solicitudes para '.$opportunity->name)

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Leads
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.opportunities.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($solicitudes->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Reseller</th>
                            <th>Canal Integrador</th>
                            <th>Fecha de la solicitud</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($solicitudes as $sol)
                        <tr>
                            <td>{{ $sol->reseller->name }}</td>
                            <td>{{ $sol->reseller->company->name }}</td>
                            <td>{{ $sol->created_at }}</td>
                            <td class="text-center d-flex justify-content-around">
                                <button type="submit" form="solicitud{{ $sol->id }}" class="btn btn-success">Otorgar Oportunidad</button>
                                <form action="{{ url('backend/lead/'.$sol->id) }}" method="POST" id="solicitud{{ $sol->id }}">
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No se encontraron resultados</h3>
            </div>
        @endif
    </div>
</div>

@endsection