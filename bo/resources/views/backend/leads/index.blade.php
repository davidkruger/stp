@extends('layouts.master')

@section('page-name', 'Leads')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-table"></i>
                Lista de Leads
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.opportunities.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($opportunities->count())
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Oportunidad</th>
                            <th>Cliente</th>
                            <th>% Cierre</th>
                            <th>Fecha Cierre</th>
                            <th>Tipo Op.</th>
                            <th>Solicitudes</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($opportunities as $opportunity)
                        <tr>
                            <td>{{ $opportunity->getKey() }}</td>
                            <td>{{ $opportunity->name }}</td>
                            <th>{{ $opportunity->nombre_cliente }}</th>
                            <th>{{ $opportunity->probabilidad_cierre->description }}</th>
                            <th>{{ date("d/m/Y", strtotime($opportunity->tentative)) }}</th>
                            <td>{{ $opportunity->tipo_oportunidad }}</td>
                            <td>{{ $opportunity->solicitudes }}</td>
                            <td class="text-center d-flex justify-content-around">
                                <a href="{{ url('backend/oportunidad/'.$opportunity->id) }}" class="btn btn-warning">Ver</a>
                                <a href="{{ url('backend/leads/'.$opportunity->id) }}" class="btn btn-primary">Solicitudes</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="ModalTitle1"></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-6">
                                <label for="no_proteger"><input type="radio" name="protegido" id="no_proteger" value="no" form="proteccion"><i style="color: red" class="fas fa-thumbs-down"></i>No Proteger</label>
                                
                                <input name="opp_id" id="opp_id1" type="hidden" form="proteccion">
                            </div>
                            <div class="col-6">
                                <label for="proteger"><input type="radio" name="protegido" id="proteger" value="si" form="proteccion"><i style="color: green" class="fas fa-thumbs-up"></i>Proteger</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-success" form="proteccion">Guardar</button>
                          <form action="{{ url('backend/proteccion/oportunidad') }}" method="POST" id="proteccion">
                            @csrf
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="ModalTitle2"></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                                <b>Desea otorgar protección a este negocio?</b>
                                <input name="protegido" type="hidden" id="no_proteger" value="si" form="otorgar_proteccion">
                                <input name="opp_id" id="opp_id2" type="hidden" form="otorgar_proteccion">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-success" form="otorgar_proteccion">Proteger</button>
                          <form action="{{ url('backend/proteccion/oportunidad') }}" method="POST" id="otorgar_proteccion">
                            @csrf
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="ModalTitle3"></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <b>Desea retirar la protección de este negocio?</b>
                            <input name="protegido" id="roteger" type="hidden" value="no" form="retirar_proteccion">
                            <input name="opp_id" id="opp_id3" type="hidden" form="retirar_proteccion">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-warning" form="retirar_proteccion">Retirar Protección</button>
                          <form action="{{ url('backend/proteccion/oportunidad') }}" method="POST" id="retirar_proteccion">
                            @csrf
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        @else
            <div class="text-center m-t-30 m-b-30 p-b-10">
                <h2><i class="fas fa-table text-custom"></i></h2>
                <h3>No se encontraron resultados</h3>
                <p class="text-muted">
                    Puedes agregar items haciendo click en el botón
                    <a href="{{ route('admin.opportunities.create') }}" class="text-custom">
                        <ins>Añadir</ins>
                    </a>
                </p>
            </div>
        @endif
    </div>
</div>

@endsection