<div class="vertical-wrapper mb-3 {{ !isset($cVertical) ? 'd-none' : '' }}" {{ !isset($cVertical) ? 'id=new' : '' }}>
    <input type="hidden" name="vertical[]" value="{{ isset($cVertical) ? $cVertical->id : '' }}">
    <select name="verticals[]" class="form-control">
    <option selected disabled hidden>-- Seleccione Vertical --</option>
    @foreach($verticals as $vertical)
    <option value="{{ $vertical->id }}" {{ isset($cVertical) && $cVertical->id == $vertical->id ? 'selected' : '' }}>{{ $vertical->description }}</option>
    @endforeach
    </select>
<div>Eliminar</div>

</div>