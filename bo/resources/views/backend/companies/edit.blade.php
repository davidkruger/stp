@extends('layouts.master')

@section('page-name', 'Editar Integrador')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-user-plus"></i>
                Editar Integrador
            </div>
            <div class="col-6 d-flex justify-content-end">
                <a href="{{ route('admin.companies.create') }}" class="btn btn-sm btn-primary">Añadir</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin.companies.update', $company->id) }}">
            @method('PUT')
            @csrf
            @include('companies.form')
        </form>
    </div>
</div>

@endsection