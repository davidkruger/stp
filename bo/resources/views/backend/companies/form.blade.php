@if ($errors->any())
<div class="row">
    <div class="col-6 offset-3 mb-4">
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                Error
                @foreach ($errors->all() as $error)
                    <div class="text-white-50 small">{{ $error }}</div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<div class="form-row form-group">
{{--<div class="col-3">
    <label class="col-3 control-label">Estado del negocio</label>
    <select  name="estado" class="form-control" placeholder="Estado del negocio">
        <option value="value1">Activo</option>
        <option value="value2" selected>Ganado</option>
        <option value="value3">Perdido</option>
    </select>
</div>--}}
</div>
<div class="form-row form-group">
    <label class="col-3 control-label">RUC</label>
    <div class="col-3">
        <input name="ruc" type="text" class="form-control {{ $errors->has('ruc') ? 'is-danger' : '' }}" required placeholder="RUC" value="{{ isset($company) && !old('ruc') ? $company->ruc : old('ruc') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Nombre</label>
    <div class="col-6">
        <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" required placeholder="Nombre" value="{{ isset($company) && !old('name') ? $company->name : old('name') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Razón Social (SET)</label>
    <div class="col-6">
        <input name="real_name" type="text" class="form-control {{ $errors->has('real_name') ? 'is-danger' : '' }}" required placeholder="Nombre real" value="{{ isset($company) && !old('real_name') ? $company->real_name : old('real_name') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Teléfono</label>
    <div class="col-3">
        <input name="phone" type="text" class="form-control {{ $errors->has('phone') ? 'is-danger' : '' }}" placeholder="Teléfono (opcional)" value="{{ isset($company) && !old('phone') ? $company->phone : old('phone') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Email</label>
    <div class="col-6">
        <input name="email" type="email" class="form-control {{ $errors->has('email') ? 'is-danger' : '' }}" placeholder="Email (opcional)" value="{{ isset($company) && !old('email') ? $company->email : old('email') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Dirección</label>
    <div class="col-6">
        <input name="address" type="text" class="form-control {{ $errors->has('address') ? 'is-danger' : '' }}" placeholder="Dirección (opcional)" value="{{ isset($company) && !old('address') ? $company->address : old('address') }}">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Tipo de Integrador</label>
    <div class="col-3">
        <select name="type" class="form-control {{ $errors->has('type') ? 'is-danger' : '' }}" required placeholder="Tipo de Compañia" value="{{ isset($company) && !old('type') ? $company->type : old('type') }}">
            <option disabled {{ (!isset($company) && !old('type')) ? 'selected' : '' }} hidden>-- Seleccione Tipo de Integrador --</option>
            <option value="reseller" {{ (isset($company) && !old('type') ? $company->type : old('type')) == 'reseller' ? 'selected' : '' }}>Reseller</option>
            <option value="mayorista" {{ (isset($company) && !old('type') ? $company->type : old('type')) == 'mayorista' ? 'selected' : '' }}>Mayorista</option>
    {{--         <option value="company" {{ (isset($company) && !old('type') ? $company->type : old('type')) == 'company' ? 'selected' : '' }}>Integrador</option> --}}
        </select>
    </div>

    <div class="col-3" only="type=reseller">
        <select name="reseller" class="form-control {{ $errors->has('reseller') ? 'is-danger' : '' }}" placeholder="Tipo de Reseller" value="{{ isset($company) && !old('reseller') ? $company->reseller : old('reseller') }}" required>
            <option selected disabled hidden>-- Seleccione Tipo de Reseller --</option>
            <option value="authorized" {{ (isset($company) && !old('reseller') ? $company->reseller : old('reseller')) == 'authorized' ? 'selected' : '' }}>Authorized</option>
            <option value="preferred" {{ (isset($company) && !old('reseller') ? $company->reseller : old('reseller')) == 'preferred' ? 'selected' : '' }}>Preferred</option>
            <option value="priority" {{ (isset($company) && !old('reseller') ? $company->reseller : old('reseller')) == 'priority' ? 'selected' : '' }}>Priority</option>
            <option value="no_qualify" {{ (isset($company) && !old('reseller') ? $company->reseller : old('reseller')) == 'no_qualify' ? 'selected' : '' }}>No Qualify</option>
        </select>
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Logo</label>
    {{-- <div class="col-6">
        <select name="logo_id"
            value="{{ isset($company) && !old('logo_id') ? $company->logo_id : old('logo_id') }}"
            preview="#logo_preview" class="form-control {{ $errors->has('logo_id') ? 'is-danger' : '' }}" placeholder="Logo">
            <option selected>Logo (opcional)</option>
            @foreach($images as $image)
            <option value="{{ $image->id }}" url="{{ route('storage.get', [ 'file' => $image->getKey() ]) }}"
                {{ (isset($company) && !old('logo_id') ? $company->logo_id : old('logo_id')) == $image->id ? 'selected' : '' }}>{{ $image->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-start">
        <label class="fas fa-fw fa-cloud-upload-alt mb-0 fs-20px" for="upload"></label>
        <input type="file" name="logo" class="d-none" id="upload" preview="#logo_preview">
    </div>
    <div class="col-6 offset-3 d-flex justify-content-center">
        <img src="#" id="logo_preview" class="my-1 rounded" style="display: none;">
    </div> --}}
    <div class="col-6">
        <input type="file" name="logo" id="upload" preview="#logo_preview" {{ isset($company) ? '' : 'required' }} accept="image/*">
    </div>
    <div class="col-6 offset-3 d-flex justify-content-center">
        <img src="#" id="logo_preview" class="my-1 rounded" style="display: none;">
    </div>
</div>

<div class="form-row form-group">
    <label class="col-3 control-label">Fecha de Constitución</label>
    <div class="col-3">
        <input name="constitution" type="date" class="form-control {{ $errors->has('constitution') ? 'is-danger' : '' }}" required placeholder="Fecha de Constitución" value="{{ isset($company) && !old('constitution') ? $company->constitution : old('constitution') }}">
    </div>
</div>

<div class="form-row form-group" only="type=reseller">
    <label class="col-3 control-label">Verticales</label>
    <div class="col-9">
        <select name="verticals[]" multiple class="select-form select2" style="width: 100% !important" required>
            @foreach ($verticals as $vertical)
                <option value="{{ $vertical->id }}" {{ (isset($company) && $vertical->selected==1 ? 'selected':'') }}>{{ $vertical->code }}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a href="{{ route('admin.companies') }}" class="btn btn-danger">Cancelar</a>
    </div>
</div>

@push('scripts')

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
        $('.select2').select2();
        });
    </script>
    <script src="{{ asset('assets/backend/js/Company.js') }}"></script>
    <script type="text/javascript">new Company({{ isset($company) ? $company->id : '' }});</script>
@endpush