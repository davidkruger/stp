@extends('layouts.master')

@section('breadcrumb')
    <li class="breadcrumb-item h3 m-0 py-1"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item h3 m-0 py-1 active" aria-current="page">Verticales</li>
@endsection

{{-- @section('page-name', 'Verticales') --}}

@section('content')

    <div class="row justify-content-center">
        @foreach ($verticals as $vertical)
        <div class="col-4">
            <div class="card shadow mb-4">
                <div class="card-header bg-primary">
                    <h5 class="m-0 font-weight-bold text-white">{{ $vertical->code }}</h5>
                </div>
                <div class="card-body py-2">
                    <div class="row">
                        <div class="col-4">
                            <div class="row h-100 align-items-center">
                                <div class="col-12 d-none">
                                    <h5 class="font-weight-bold text-center">{{ $vertical->code }}</h5>
                                </div>
                                <div class="col-12">
                                    <img src="{{ route('storage.get', [ 'file' => $vertical->icon_id ]) }}" class="rounded img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                            <table class="table table-sm table-hover">
                                <thead class="">
                                    <tr>
                                        <th></th>
                                        <th class="text-right">Empresas</th>
                                        <th class="text-right">TAM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Total</td>
                                        <td class="text-right">{{ $vertical->totals->totals[0] ?? 0 }}</td>
                                        <td class="text-right">{{ $vertical->totals->totals[1] ?? 0 }}</td>
                                    </tr>
                                    <tr>
                                        <td>BO</td>
                                        <td class="text-right">{{ $vertical->totals->bo[0] ?? 0 }}</td>
                                        <td class="text-right">{{ $vertical->totals->bo[1] ?? 0 }}</td>
                                    </tr>
                                    <tr>
                                        <td>Leads</td>
                                        <td class="text-right">{{ $vertical->totals->leads[0] ?? 0 }}</td>
                                        <td class="text-right">{{ $vertical->totals->leads[1] ?? 0 }}</td>
                                    </tr>
                                    <tr>
                                        <td>Disponible</td>
                                        <td class="text-right">{{ $vertical->totals->available[0] ?? 0 }}</td>
                                        <td class="text-right">{{ $vertical->totals->available[1] ?? 0 }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection