@extends('layouts.master')

@section('page-name', 'Dashboard')

@section('content')

    <div class="row">

        <div class="col-12 col-sm-4 col-xl-3 mb-4">
            <div class="card border-left-primary shadow h-100">
                <a href="{{ url('backend/leads') }}" class="card-body no-underline text-primary">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col-2 text-center">
                            <i class="fas fa-briefcase fa-3x text-gray-300 hover-primary"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-primary text-uppercase">Leads</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-primary text-right">{{ $count->lead ?? 0 }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-12 col-sm-4 col-xl-3 mb-4">
            <div class="card border-left-info shadow h-100">
                <a href="{{ route('admin.opportunities', [ 'state' => 'open' ]) }}" class="card-body no-underline text-info">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col-2 text-center">
                            <i class="fas fa-clipboard-list fa-3x text-gray-300 hover-info"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-info text-uppercase">Oportunidades en curso</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-info text-right">{{ ($count->open ?? 0)+($count->presented ?? 0) }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-12 col-sm-4 col-xl-3 mb-4">
            <div class="card border-left-success shadow h-100">
                <a href="{{ route('admin.opportunities', [ 'state' => 'won' ]) }}" class="card-body no-underline text-success">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col-2 text-center">
                            <i class="fas fa-dollar-sign fa-3x text-gray-300 hover-success"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-success text-uppercase">Oportunidades Ganadas</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-success text-right">{{ $count->won ?? 0 }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-12 col-sm-4 col-xl-3 mb-4">
            <div class="card border-left-danger shadow h-100">
                <a href="{{ route('admin.opportunities', [ 'state' => 'lost' ]) }}" class="card-body no-underline text-danger">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col-2 text-center">
                            <i class="fas fa-thumbs-down fa-3x text-gray-300 hover-danger"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-danger text-uppercase">Oportunidades Perdidas</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-danger text-right">{{ $count->lost ?? 0 }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>

    <div class="row justify-content-center">

        @foreach ($companies as $company)
            @php
                $data = [
                    'mayorista' => (object)[ 'title' => 'Mayoristas', 'color' => 'info', 'icon' => 'building' ],
                    'reseller'  => (object)[ 'title' => 'Resellers', 'color' => 'warning', 'icon' => 'building' ],
                    'company'   => (object)[ 'title' => 'Empresas', 'color' => 'secondary', 'icon' => 'building' ],
                ][$company->type];
            @endphp
        <div class="col-12 col-sm-3 mb-4">
            <div class="card border-bottom-{{ $data->color }} shadow h-100">
                <a href="{{ route('admin.companies', [ 'type' => $company->type ]) }}" class="card-body no-underline text-{{ $data->color }}">
                    <div class="row no-gutters align-items-center">
                        <div class="col-2 text-center">
                            <i class="fas fa-{{ $data->icon }} fa-3x text-gray-300 hover-{{ $data->color }}"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-{{ $data->color }} text-uppercase">{{ $data->title }}</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-{{ $data->color }} text-right">{{ $company->total ?? 0 }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        @endforeach

    {{--     <div class="col-12 col-sm-4 col-xl-3 mb-4">
            <div class="card border-bottom-primary shadow h-100">
                <a href="{{ route('admin.dashboard.verticals') }}" class="card-body no-underline text-primary">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col-2 text-center">
                            <i class="fas fa-clone fa-3x text-gray-300 hover-primary"></i>
                        </div>
                        <div class="col ml-3">
                            <div class="font-weight-bold text-primary text-uppercase">Verticales</div>
                        </div>
                        <div class="col-2">
                            <div class="h2 mb-0 font-weight-bold text-primary text-right">{{ $count->won ?? 0 }}</div>
                        </div>
                    </div>
                </a>
            </div>
        </div> --}}

    </div>

    <div class="row">
        <div class="col-lg-6 mb-4">

            <div class="card shadow border-left-info mb-4">

                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-info">Oportunidades en Curso mas recientes</h6>
                </div>

                <div class="card-body">

                    @php
                    $open = $opportunities->filter(function($opportunity) {
                        return in_array($opportunity->state, [ 'open' ]);
                    });
                    $cont_open = 0;
                    @endphp
                    @if (count($open) > 0)
                        @foreach ($open as $opportunity)
                            @if ($cont_open<5)
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><a href="{{ url('/backend/oportunidad/'.$opportunity->id) }}"><small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}</a></h4>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><span class="float-right">{{ $opportunity->reseller->name }}</span></h4>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><span>Fecha de creacion: {{ date("d/m/Y", strtotime($opportunity->created_at)) }}</span></h4>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><span class="float-right">Protección:
                                            @switch($opportunity->protegido)
                                                @case('si')
                                                    <i style="color: green" class="fas fa-thumbs-up"></i>
                                                    @break
                                                @case('no')
                                                    <i style="color: red" class="far fa-thumbs-down"></i>
                                                    @break
                                                @default
                                                    <a href="{{ url('backend/oportunidad/'.$opportunity->id) }}"><i style="color: rgb(228, 151, 8)" class="fas fa-exclamation-triangle"></i></a>
                                            @endswitch
                                        </span></h4>
                                    </div>
                                    <div class="col-12">
                                        <div class="progress mb-4">
                                            <div class="progress-bar bg-info font-weight-bold" role="progressbar" style="width: {{ $opportunity->probabilidad_cierre->porcentaje }}%" aria-valuenow="{{ $opportunity->probabilidad_cierre->porcentaje }}" aria-valuemin="0" aria-valuemax="100">{{ $opportunity->probabilidad_cierre->description }}%</div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $cont_open = $cont_open + 1;
                                @endphp
                            @endif
                        @endforeach
                    @else
                        <div class="text-center m-t-30 m-b-30 p-b-10">
                            <em>No hay oportunidades en curso</em>
                        </div>
                    @endif

                </div>

            </div>
            <div class="card shadow border-left-warning mb-4">

                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-warning">Los 3 Mejores Rankeados Este Mes</h6>
                </div>

                <div class="card-body">

                    @if (count($ranks) > 0)
                        @foreach ($ranks as $rank)
                            @if ($rank->position <= 3)
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><small>#{{ $rank->position }}</small></h4>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <h4 class="small font-weight-bold"><span class="float-right">Canal Integrador: {{ $rank->name }}</span></h4>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-4">
                                            <div class="">{{ $rank->puntos }} puntos acumulados este mes</div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="text-center m-t-30 m-b-30 p-b-10">
                            <em>No hay usuarios rankeados</em>
                        </div>
                    @endif

                </div>

            </div>

        </div>

        <div class="col-lg-6 mb-4">

            <div class="card shadow border-left-success mb-4">

                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-success">Oportunidades Ganadas</h6>
                </div>

                <div class="card-body">

                    @php
                    $won = $opportunities->filter(function($opportunity) {
                        return $opportunity->state == 'won';
                    });
                    $cont_won = 0;
                    @endphp
                    @if (count($won) > 0)
                        @foreach ($won as $opportunity)
                            @if ($cont_won<5)
                                <h4 class="small font-weight-bold"><a href="{{ url('/backend/oportunidad/'.$opportunity->id) }}"><small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }} </a><span class="float-right">{{ $opportunity->reseller->name }}</span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-success font-weight-bold" role="progressbar" style="width: 100%" aria-valuenow="{{ $opportunity->status }}" aria-valuemin="0" aria-valuemax="100">Completado!</div>
                                </div>
                                @php
                                $cont_won = $cont_won + 1;
                                @endphp
                            @endif
                        @endforeach
                    @else
                        <div class="text-center m-t-30 m-b-30 p-b-10">
                            <em>No hay oportunidades ganadas</em>
                        </div>
                    @endif

                </div>

            </div>
            <div class="card shadow border-left-danger mb-4">

                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-danger">Oportunidades Perdidas</h6>
                </div>

                <div class="card-body">

                    @php
                    $lost = $opportunities->filter(function($opportunity) {
                        return $opportunity->state == 'lost';
                    });
                    $cont_lost = 0;
                    @endphp
                    @if (count($lost) > 0)
                        @foreach ($lost as $opportunity)
                            @if ($cont_lost<5)
                                <h4 class="small font-weight-bold"><a href="{{ url('/backend/oportunidad/'.$opportunity->id) }}"><small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}</a> <span class="float-right">{{ $opportunity->reseller->name }}</span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-danger font-weight-bold" role="progressbar" style="width: 100%" aria-valuenow="{{ $opportunity->status }}" aria-valuemin="0" aria-valuemax="100">Perdido!</div>
                                </div>
                                @php
                                $cont_lost = $cont_lost + 1;
                                @endphp
                            @endif
                        @endforeach
                    @else
                        <div class="text-center m-t-30 m-b-30 p-b-10">
                            <em>No hay oportunidades ganadas</em>
                        </div>
                    @endif

                </div>

            </div>

        </div>
    </div>

@endsection