@extends('layouts.master')

@section('content')

<div class="row justify-content-center">
    <div class="col">
        <div class="card">
            <div class="card-header">
                Precios de Lista
            </div>

            <div class="card-body">
                <div class="accordion" id="offers-accordion">
                    <form method="POST" action="{{ route('products.update', Auth::user()->company_id) }}">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            @foreach($products as $idx => $product)
                                @include('prices.product', [
                                    'product'   => $product,
                                ])
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col d-flex justify-content-end">
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                        <a href="{{ route('dashboard') }}" class="btn btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
