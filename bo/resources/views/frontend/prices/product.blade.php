<div class="col-12 col-lg-4 mb-3">
    <div class="card border-left-primary">
        <div class="card-header">{{ $product->name }}</div>
        <div class="card-body p-2">
            <div class="row">
                <div class="col-2 d-flex align-items-center">
                    <img src="{{ route('storage.get', [ 'file' => $product->image_id ]) }}" class="rounded img-fluid">
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-12 mb-2">
                            {{ $product->description }}
                        </div>
                        <div class="col-12">

                            <div class="row align-items-center">
                                <label class="col-4 m-0 font-weight-bold">Precio de Lista</label>
                                <div class="col-8">

                                    <input type="hidden" name="products[]" value="{{ $product->id }}">
                                    <input type="number" name="price[]"
                                        value="{{ $product->price }}" required
                                        class="form-control text-right" placeholder="Precio lista">

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>