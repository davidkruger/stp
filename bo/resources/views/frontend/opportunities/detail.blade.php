<div class="row">
    <div class="col-12 mb-3">
        <dl class="row mb-2">
            <dt class="col-2">Compañía:</dt>
            <dd class="col">{{-- $opportunity->company->name --}}</dd>

            <dt class="col-2">Mayorista:</dt>
            <dd class="col">{{-- $opportunity->mayorista!==null?$opportunity->mayorista->name:'' --}}</dd>
        </dl>
        <dl class="row mb-2">
            <dt class="col-2">Fecha tentativa de cierre:</dt>
            <dd class="col-4">{{ $opportunity->tentative }}</dd>

            <dt class="col-2">Reseller:</dt>
            <dd class="col">{{-- $opportunity->reseller!==null?$opportunity->reseller->name:'' --}}</dd>
        </dl>
        <dl class="row mb-2">
            <dt class="col-2">Fecha real de cierre:</dt>
            <dd class="col-4">{{ $opportunity->closes ?? '--' }}</dd>
        </dl>
    </div>
</div>

@if (!isset($withproducts) || $withproducts)
    @if (count($opportunity->products) > 0)

    <div class="row">
        <div class="col">

            <h5>Productos</h5>

            <div class="table-responsive">
                <table class="table table-sm table-borderless table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="w-100px text-center">Cantidad</th>
                            <th>Producto</th>
                            <th>Descripcion</th>
                            <th class="text-right">Precio Lista</th>
                            <th class="text-right">Precio Sugerido</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($opportunity->products as $product)
                        <tr>
                            <td class="text-center">{{ $product->pivot->quantity }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->description }}</td>
                            <td class="text-right">{{ number_format($product->price) }}</td>
                            <td class="text-right">{{ $product->pivot->price !== null ? number_format($product->pivot->price) : '--' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    @else

    <div class="text-center m-t-30 m-b-30 p-b-10">
        <em>Aún no se asignaron productos</em>
    </div>

    @endif
@endif