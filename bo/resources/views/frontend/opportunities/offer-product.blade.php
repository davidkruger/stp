<div class="input-group mb-1">
    <div class="input-group-prepend">
        <span class="input-group-text">{{ $oProduct->opportunityProduct->first()->quantity }}x</span>
        <span class="input-group-text">{{ $oProduct->product->name }}</span>
    </div>
    <span class="form-control">{{ $oProduct->product->description }}</span>
</div>

<div class="input-group mb-3">
    <span class="form-control">{{ $oProduct->price }}</span>
    <div class="input-group-append">
        @if ($oProduct->product->price !== null)
        <span class="input-group-text">Precio Lista: {{ number_format($oProduct->product->price) }}</span>
        @endif
        @if ($oProduct->opportunityProduct->first()->price !== null)
        <span class="input-group-text">Precio Sugerido: {{ number_format($oProduct->opportunityProduct->first()->price) }}</span>
        @endif
    </div>
</div>