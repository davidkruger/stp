@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col">

                <form method="POST" action="{{ route('opportunities.store') }}" id="enviar">
                    @csrf
                    @include('opportunities.form')
                </form>

    </div>
</div>

@endsection
