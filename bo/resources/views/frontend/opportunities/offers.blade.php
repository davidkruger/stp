@foreach ($offers as $idx => $offer)
<div class="row">

    <div class="col-12 col-lg-6">
        <div class="card shadow mb-2 {{ $offer->status !== 'new' ? ( $offer->status == 'rejected' ? 'border-bottom-danger' : 'border-bottom-success' ) : '' }}">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <a href="#offer_{{ $offer->id }}" class="d-block py-2" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="offer_{{ $offer->mayorista_id }}">
                            <h6 class="m-0 font-weight-bold text-{{ $offer->status !== 'new' ? ( $offer->status == 'rejected' ? 'danger' : 'success' ) : 'primary' }}"><small>{{ $offer->created_at }}</small> {{ $offer->mayorista->name }}</h6>
                        </a>
                    </div>
                    <div class="col d-flex justify-content-end align-items-center">
                        {{-- <a href="" class="btn btn-sm btn-secondary">Editar Cotización</a> --}}
                    </div>
                </div>
            </div>

            <div class="collapse {{ $idx == count($offers) - 1 ? 'show' : '' }}" id="offer_{{ $offer->id }}">
                <div class="card-body">

                    @foreach($offer->products as $product)
                        <div class="row">
                            <div class="col-8">
                                {{ $product->name }}
                            </div>
                            <div class="col-4 text-right">
                                {{ number_format($product->pivot->price) }}
                            </div>
                        </div>
                    @endforeach

                </div>

                <div class="card-footer">
                    <div class="row">
                        @if ($offer->status !== 'new')

                        <div class="col">
                            <label class="text-{{ $offer->status == 'rejected' ? 'danger' : 'success' }}">{{ $offer->status == 'rejected' ? 'Rechazada' : 'Aceptada' }}</label>
                        </div>

                        @endif

                        <div class="col d-flex justify-content-end">

                            @if ($offer->status == 'new')

                                @if (in_array($opportunity->state, [ 'open' ]) && $idx == count($offers) - 1)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#rejectOfferModal{{ $offer->id }}">Rechazar Cotización</button>
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#acceptOfferModal{{ $offer->id }}">Aceptar Cotización</button>
                                </div>
                                @endif

                            @else

                            <label class="text-{{ $offer->status == 'rejected' ? 'danger' : 'success' }}">{{ $offer->reason }}</label>

                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!-- Opportunity Modal-->
<div class="modal fade" id="acceptOfferModal{{ $offer->id }}" tabindex="-1" role="dialog" aria-labelledby="acceptOfferModalTitle{{ $offer->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success text-white">
                <h5 class="modal-title" id="acceptOfferModalTitle{{ $offer->id }}">Aceptar Cotización</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.offers.accept', [ $opportunity->id, $offer->id ]) }}">
                @csrf
                @method('PUT')

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estas seguro de aceptar la oferta?</h4>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="reason">Comentarios</label>
                                <textarea name="reason" id="reason" class="form-control" placeholder="Comentarios (opcional)"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Si, aceptar cotización</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Opportunity Modal-->
<div class="modal fade" id="rejectOfferModal{{ $offer->id }}" tabindex="-1" role="dialog" aria-labelledby="rejectOfferModalTitle{{ $offer->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="rejectOfferModalTitle{{ $offer->id }}">Rechazar Cotización</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.offers.reject', [ $opportunity->id, $offer->id ]) }}">
                @csrf
                @method('PUT')

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estas seguro de rechazar la cotización?</h4>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="reason">Comentarios</label>
                                <textarea name="reason" id="reason" class="form-control" placeholder="Comentarios (opcional)"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Si, rechazar cotización</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endforeach