<div class="container">
    <div class="step-app" id="demo">
        <ul class="step-steps">
          <li data-step-target="step1">Cliente Final</li>
          <li data-step-target="step2">Oportunidad</li>
          <li data-step-target="step3">Productos</li>
        </ul>
        <div class="step-content">
          <div class="step-tab-panel" data-step="step1">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Cliente Final</h3>
                </div>
                <div class="card-body">
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Nombre</label>
                        <div class="col-6">
                            <input name="nombre_cliente" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('nombre_cliente') ? $opportunity->nombre_cliente : old('nombre_cliente') }}"
                                class="form-control {{ $errors->has('nombre_cliente') ? 'is-danger' : '' }}" placeholder="Nombre Cliente">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Razon Social</label>
                        <div class="col-6">
                            <input name="razon_social_cliente" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('razon_social_cliente') ? $opportunity->razon_social_cliente : old('razon_social_cliente') }}"
                                class="form-control {{ $errors->has('razon_social_cliente') ? 'is-danger' : '' }}" placeholder="Razon Social">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Numero de Ruc</label>
                        <div class="col-6">
                            <input name="ruc_cliente" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('ruc_cliente') ? $opportunity->ruc_cliente : old('ruc_cliente') }}"
                                class="form-control {{ $errors->has('ruc_cliente') ? 'is-danger' : '' }}" placeholder="Numero de Ruc">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Telefono</label>
                        <div class="col-6">
                            <input name="telefono_cliente" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('telefono_cliente') ? $opportunity->name : old('telefono_cliente') }}"
                                class="form-control {{ $errors->has('telefono_cliente') ? 'is-danger' : '' }}" placeholder="Telefono">
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="step-tab-panel" data-step="step2">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Oportunidad</h3>
                </div>
                <div class="card-body">
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Nombre</label>
                        <div class="col-6">
                            <input name="name" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->company_id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('name') ? $opportunity->name : old('name') }}"
                                class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}" placeholder="Nombre">
                        </div>
                    </div>

                    <div class="form-row form-group">
                        <label class="col-3 control-label">Vertical del proyecto</label>
                        <div class="col-6">
                            <select name="vertical" class="form-control {{ $errors->has('vertical') ? 'is-danger' : '' }}" required placeholder="Vertical del Proyecto" value="{{ isset($opportunity) && !old('vertical') ? $opportunity->vertical_id : old('vertical') }}">
                                <option value="">-- Elija la vertical del proyecto --</option>
                                @foreach ($verticals as $vertical)
                                <option value="{{ $vertical->id }}">{{ $vertical->code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="form-row form-group">
                    <label class="col-3 control-label">Etapa del proyecto</label>
                    <div class="col-6">
                        <select name="probabilidad_cierre" class="form-control {{ $errors->has('probabilidad_cierre') ? 'is-danger' : '' }}" required placeholder="probabilidad_cierre" value="{{ isset($opportunity) && !old('probabilidad_cierre') ? $opportunity->probabilidad_cierre_id : old('probabilidad_cierre') }}">
                            <option value="">-- Elija la etapa del proyecto --</option>
                            @foreach ($probabilidades as $prob)
                            <option value="{{ $prob->id }}">{{ $prob->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

    
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Tipo de Oportunidad</label>
                        <div class="col-6">
                            <select name="tipo_oportunidad" class="form-control {{ $errors->has('tipo_oportunidad') ? 'is-danger' : '' }}" required placeholder="tipo_oportunidad" value="{{ isset($opportunity) && !old('tipo_oportunidad') ? $opportunity->tipo_oportunidad : old('tipo_oportunidad') }}">
                                <option value="publico">Publica</option>
                                <option value="privado">Privada</option>
                            </select>
                        </div>
                    </div>
    
                    <div only="tipo_oportunidad=publica">
                        <div class="form-row form-group">
                            <label class="col-3 control-label">Publica</label>
                            <div class="col-6">
                                <select name="tipo_oportunidad_publica" class="form-control {{ $errors->has('tipo_oportunidad') ? 'is-danger' : '' }}" required placeholder="tipo_oportunidad" value="{{ isset($opportunity) && !old('tipo_oportunidad') ? $opportunity->tipo_oportunidad : old('tipo_oportunidad') }}">
                                    <option value="pre_pliego">PRE-PLIEGO</option>
                                    <option value="llamado_calle">Llamado en la calle</option>
                                </select>
                            </div>
                        </div>
                    </div>
    
                    <div only="tipo_oportunidad_publica=llamado_calle">
                        <div class="form-row form-group">
                            <label class="col-3 control-label">Id del Llamado</label>
                            <div class="col-6">
                                <input name="idllamado" type="text" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('idllamado') ? $opportunity->idllamado : old('idllamado') }}"
                                class="form-control {{ $errors->has('idllamado') ? 'is-danger' : '' }}" placeholder="Id del llamado publico">
                            </div>
                        </div>
                    </div>
    
                    <div only="tipo_oportunidad_publica=pre_pliego">
                        <div class="form-row form-group">
                            <label class="col-3 control-label">Fecha estimada de la publicación del llamado</label>
                            <div class="col-6">
                                <input name="publicacion_llamado" type="date" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('publicacion_llamado') ? $opportunity->publicacion_llamado : old('publicacion_llamado') }}"
                                class="form-control {{ $errors->has('publicacion_llamado') ? 'is-danger' : '' }}" placeholder="Fecha de publicacion del llamado">
                            </div>
                        </div>
                    </div>
    
                    @if (Auth::user()->company->type == 'reseller')
                    <input type="hidden" name="reseller_id" value="{{ Auth::user()->id }}">
                    @else
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Reseller</label>
                        <div class="col-6">
                            <select name="reseller_id" {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->company_id ? 'readonly disabled' : '' }}
                                value="{{ isset($opportunity) && !old('reseller_id') ? $opportunity->reseller_id : old('reseller_id') }}"
                                class="form-control {{ $errors->has('reseller_id') ? 'is-danger' : '' }}" placeholder="Reseller (opcional)">
                                <option value="" selected>Reseller (opcional)</option>
                                @foreach($companies as $reseller)
                                @if ($reseller->type !== 'reseller') @continue @endif
                                <option value="{{ $reseller->id }}" {{ (isset($opportunity) && !old('reseller_id') ? $opportunity->reseller_id : old('reseller_id')) == $reseller->id ? 'selected' : '' }}>{{ $reseller->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
    
                    @if (isset($opportunity))
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Fecha de carga</label>
                        <div class="col-6">
                            <label class="form-control">{{ isset($opportunity) ? $opportunity->created_at : '' }}</label>
                        </div>
                    </div>
                    @endif
    
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Fecha tentativa de cierre</label>
                        <div class="col-6">
                            <input name="tentative" type="date" required {{ isset($opportunity) && $opportunity->createdby !== Auth::user()->company_id ? 'readonly disabled' : '' }}
                                class="form-control {{ $errors->has('tentative') ? 'is-danger' : '' }}" placeholder="Fecha de carga de la Oportunidad" value="{{ isset($opportunity) && !old('tentative') ? $opportunity->tentative : old('tentative') }}">
                        </div>
                    </div>
    
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Presupuesto Estimado</label>
                        <div class="col-6">
                            <input type="number" name="presupuesto" placeholder="Presupuesto Estimado" class="form-control">
                        </div>
                    </div>
    
                    @if (isset($opportunity))
                    <div class="form-row form-group">
                        <label class="col-3 control-label">Estado de la Oportunidad (%)</label>
                        <div class="col-6">
                            <div class="progress" style="height: 100%">
                                <div class="progress-bar bg-{{ $opportunity->status > 0 ? 'info' : 'secondary' }}" role="progressbar"
                                    style="width: {{ $opportunity->status > 0 ? $opportunity->status : 100 }}%;"
                                    aria-valuenow="{{ $opportunity->status }}" aria-valuemin="0" aria-valuemax="100">{{ $opportunity->status }}%</div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
          </div>
          <div class="step-tab-panel" data-step="step3">
            <div class="card fullWidth">
                <div class="card-header">
                    <h3>Productos</h3>
                </div>
                <div class="card-body">
                    <div class="form-row form-group">
                        <div class="col products-container">
                            {{-- products --}}
                            <div class="form-row">
                                <div class="col-6">
                                 <input type="hidden" value="{{Auth::user()->id.$mytime =date('mdYHis')}}" name="idOpportunity" id="idOpportunity" >
                                    <select name="idProductList" id="productList" class="form-control productList" placeholder="Producto">
                                        
                                    </select>
                                </div>
                               
                                <div class="col-2">
                                    <input name="productQty" type="text" id="productQty" value="" class="form-control " placeholder="Cantidad">
                                </div>
                                <div class="col-2">
                                    <input name="priceOffer" id="priceOffer" type="text" value="" class="form-control " placeholder="Precio Solicitado">
                                </div>
                                <div class="col-2">
                                    <button type="button" onclick=" addProductOpp({{Auth::user()->id.$mytime =date('mdYHis')}}); return false;" class="btn btn-success  fullWidth">Agregar</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                   
                                    <th class="w-200px">Nombre</th>
                                    <th>Categoria</th>
                                    <th class="w-200px">Subcategoria</th>
                                    <th class="w-200px">Precio de Lista</th>
                                    <th>Puntos</th>
                                    <th>En Stock</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->division->name }}</td>
                                        <td>{{ $product->subcategoria->name }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->points }}</td>
                                        <td>
                                        @if ($product->stock==1)
                                            Disponible
                                        @else
                                            No disponible
                                        @endif
                                        </td>
                                        <td><img src="{{ route('frontstorage.get', [ 'file' => $product->image_id ]) }}" width="100"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="step-footer">
          <button data-step-action="prev" class="btn btn-info">Anterior</button>
          <button data-step-action="next" class="btn btn-info">Siguiente</button>
          <button {{-- data-step-action="finish" --}} type="submit" class="btn btn-success">Guardar</button>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $('#demo').steps({
            onFinish: function () {
                alert('complete');
            }
        });
        axios.get('/listProducts')
        .then(function (response) {
            var products =response.data;
            for (var i = 0; i < products.length; i++) { 
                if (i==0) {
                    document.getElementById('productList').innerHTML = ' <option value="0">Productos</option><option value="'+products[i].id+'">'+ products[i].name +'</option>'
                }else{
                    document.getElementById('productList').innerHTML = ' <option value="'+products[i].id+'">'+ products[i].name +'</option>'
                }
            }
        });
        function listTable() {
            axios.get('/listOppProducts?opp='+{{$mytime}})
            .then(function (response) {
                var productList =response.data;
                for (var i = 0; i < productList.length; i++) { 
                    document.getElementById('oppProductList').innerHTML = '<td>'+productList[i].nombreProducto+'</td><td>'+productList[i].precioProducto+'</td><td>'+productList[i].quantity+'</td><td>'+productList[i].stock+'</td><td ><i  onclick="deleteOppProduct('+productList[i].id+')" class="fas fa-trash-alt"></i></td>'
                }
            });
        };
        function addProductOpp(item) {

           axios.post( '/addProductOpp',{
                    'idOpp':item,
                    'idProduct':document.querySelector('#productList').value,
                    'cantidad':document.querySelector('#productQty').value,
                    'oferta':document.querySelector('#priceOffer').value,
                }).then(function (response) {
                    listTable() 
                }).catch(function (error) {
                    console.log(error);
                });
        };
        function deleteOppProduct(item) {

            axios.post( '/deleteOppProduct',{
                    'id':item,
                }).then(function (response) {
                    console.log(response);
                    listTable() 
                }).catch(function (error) {
                    console.log(error);
                });
            };
        
    </script>
@endpush