<div class="col-12 col-xl-6">
    <div class="card shadow mb-4">
        <a href="#offer_{{ $offer->company_id }}" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="offer_{{ $offer->company_id }}">
            <h6 class="m-0 font-weight-bold text-primary">{{ $offer->company->name }}</h6>
        </a>

        <div class="collapse show" id="offer_{{ $offer->company_id }}">
            <div class="card-body">

                @foreach($offer->products as $oProduct)
                    @include('opportunities.offer-product')
                @endforeach

            </div>

            <div class="card-footer d-flex justify-content-end">
                <div class="row">
                    <div class="col">
                        <button class="btn btn-info" data-toggle="modal" data-target="#offerModal{{ $offer->company_id }}">Aceptar oferta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Offer Modal-->
<div class="modal fade" id="offerModal{{ $offer->company_id }}" tabindex="-1" role="dialog" aria-labelledby="offerModalTitle{{ $offer->company_id }}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="offerModalTitle{{ $offer->company_id }}">Aceptar oferta?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('opportunities.update', $opportunity->id) }}" id="close-form-{{ $opportunity->id }}">
            @csrf
            @method('PUT')
            <textarea name="close_reason" class="form-control" placeholder="Comentarios sobre el cierre" required></textarea>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-primary" href="{{ route('dashboard') }}" onclick="event.preventDefault(); document.getElementById('close-form-{{ $opportunity->id }}').submit();">Cerrar PO</a>
      </div>
    </div>
  </div>
</div>