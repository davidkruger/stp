@extends('layouts.master')

@section('content')
@if (($opportunity->state=='lead')||(Auth::user()->id==$opportunity->reseller_id)||(Auth::user()->type=='reseller_gerente'||Auth::user()->company_id==$opportunity->reseller->company_id))
<div class="container">
<div class="row justify-content-center">
    <div class="col">
        <div class="card border-left-{{ [
                'lead'      => 'primary',
                'open'      => 'info',
                'won'       => 'success',
                'lost'      => 'danger'
            ][$opportunity->state] }}">
                <form action="{{ url('opportunities/obtener/'.$opportunity->id) }}" method="POST" id="obtener" name="obtener">
                    @csrf
                </form>
            <div class="card-header py-3">
                <div class="row">
                    <div class="col">
                        <h3>{{ $opportunity->name }}</h3>
                    </div>
                    <div>
                       @if ($opportunity->state=='open')
                        Etapa del negocio: <button href="" class="btn btn-secondary" onclick="modal({{ $opportunity->id }},'{{ $opportunity->name }}',{{ $opportunity->probabilidad_cierre_id }});" type="button">{{ $opportunity->probabilidad_cierre->description }}</button>
                       @else
                       Etapa del negocio: <b class="badge badge-secondary">{{ $opportunity->probabilidad_cierre->description }}</b>
                            @if ($opportunity->state=='lead')
                                @if ($solicitar==0)
                                <button class="btn btn-success" type="submit" form="obtener">Solicitar Oportunidad</button>
                                @else
                                <span class="badge badge-success">Solicitud Enviada</span>
                                @endif
                            @endif
                       @endif
                    </div>
                </div>
            </div>
            <div class="card-body">

                        <h3>Datos de la Oportunidad</h3><br>
                            <dt class="row"><span class="col-3" style="font-weight: 100">Fecha tentativa de cierre:</span> <p class="col-9">{{ $opportunity->tentative }}</p></dt>
                            @if ($opportunity->reseller_id!=null)
                                <dt class="row"><span class="col-3" style="font-weight: 100">Reseller:</span> <p class="col-9">{{ $opportunity->reseller->company->name }}</p></dt>
                            @endif
                            <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad:</span><p class="col-9"> {{ $opportunity->tipo_oportunidad }}</p></dt>
                            @if ($opportunity->tipo_oportunidad=='publico')
                                @if ($opportunity->tipo_publico=='pre_pliego')
                                <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad publica:</span><p class="col-9"> PRE-PLIEGO</p></dt>
                                <dt class="row"><span class="col-3" style="font-weight: 100">Fecha estimada de publicación del llamado:</span><p class="col-9"> {{ $opportunity->pre_pliego_fecha }} </p></dt>
                                @else
                                <dt class="row"><span class="col-3" style="font-weight: 100">Tipo de oportunidad publica:</span><p class="col-9"> LLAMADO A LA CALLE</p></dt>
                                <dt class="row"><span class="col-3" style="font-weight: 100">ID del llamado:</span><p class="col-9"> {{ $opportunity->llamado_calle_idllamado }}</p></dt>
                                @endif
                            @endif
                            <dt class="row" ><span class="col-3" style="font-weight: 100">Presupuesto estimado:</span><p class="col-9"> {{ $opportunity->presupuesto }}</p></dt>
                            <dt class="row" ><span class="col-3" style="font-weight: 100">Monto total precio de lista:</span><p class="col-9"> {{ $monto_prod_lista }}</p></dt>
                            <dt class="row" ><span class="col-3" style="font-weight: 100">Monto total precio solicitado:</span><p class="col-9"> {{ $monto_prod_parcial }}</p></dt>
                            <dt class="row" ><span class="col-3" style="font-weight: 100">Plazo de entrega:</span><p class="col-9"> {{ $opportunity->entrega }}</p></dt>
                            <dt class="row" ><span class="col-3" style="font-weight: 100">Fecha de entrega:</span><p class="col-9"> {{ $opportunity->fentrega }}</p></dt>
                        <br>
                        <h3>Datos del Cliente Final</h3><br>
                            <dt class="row"><span class="col-3" style="font-weight: 100">Nombre:</span><p class="col-9"> {{ $opportunity->nombre_cliente }}</p></dt>

                            <dt class="row"><span class="col-3" style="font-weight: 100">Razon Social:</span><p class="col-9"> {{ $opportunity->razon_social_cliente }}</p></dt>

                            <dt class="row"><span class="col-3" style="font-weight: 100">RUC:</span><p class="col-9"> {{ $opportunity->ruc_cliente }}</p></dt>

                            <dt class="row"><span class="col-3" style="font-weight: 100">Telefono:</span><p class="col-9"> {{ $opportunity->telefono_cliente }}</p></dt>

                        <br><h3>Puntos que se otorgaran al ganar la oportunidad</h3><br>

                            <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por productos:</span><p class="col-6"> <b class="badge badge-success">{{ $puntos_prod }}p.</b></p></dt>
                            <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por estatus de la compañia:</span><p class="col-6"> <b class="badge badge-success">{{ $puntos_estatus }}p.</b></p></dt>
                            <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por crear la oportunidad:</span><p class="col-6"> <b class="badge badge-success">{{ $puntos_crear }}p.</b></p></dt>
                            @if ($monto_superar<=$monto_prod_parcial)
                            <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por superar {{ $monto_superar }}$:</span><p class="col-6"> <b class="badge badge-success">{{ $puntos_superar }}p.</b></p></dt>
                            @else
                            <dt class="row"><span class="col-6" style="font-weight: 100">Puntos por superar {{ $monto_superar }}$:</span><p class="col-6"> <b class="badge badge-danger">{{ $puntos_superar }}p.</b></p></dt>
                            @endif
                            <dt class="row"> <span class="col-6" style="font-weight: 100">Total de puntos que se le otorgaran al cerrar exitosamente la oportunidad:</span><p class="col-6"> 
                                @if ($monto_superar<=$monto_prod_parcial)
                                <b class="badge badge-warning">{{ $puntos_prod + $puntos_estatus + $puntos_crear + $puntos_superar }}p.</b>
                                @else 
                                <b class="badge badge-warning">{{ $puntos_prod + $puntos_estatus + $puntos_crear }}p.</b>
                                @endif
                            </p>
                            </dt>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-left-primary mt-4">
            <div class="card-header">
                <h2>Productos</h2>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach ($products as $product)
                    <div class="col-12 mb-3">
                        <dl class="row mb-2">
                            <dt class="col-4">Nombre: {{ $product->product->name }}</dt>

                            <dt class="col-2">SKU: {{ $product->product->sku }}</dt>

                            <dt class="col-2">Stock:@if ($product->product->stock==1)
                                Disponible
                            @else
                                No disponible
                            @endif
                            </dt>
                            <dt class="col-2">Precio de Lista: {{ $product->product->price }}</dt>

                            <dt class="col-2">Puntos: {{ $product->product->points }}</dt>
                        </dl>
                <div id="detalles{{ $product->id }}" style="display: block">
                        <dl>
                            @if ($product->state=='pendiente')
                                @if ($product->id_ofertante != Auth::user()->id)
                                @else
                                <dt><b class="badge badge-danger">Pendiente de aprobación</b>
                                @endif
                            @else
                                <dt><b class="badge badge-success">Aprobado</b>
                            @endif
                            @if ($opportunity->state=='open')
                                <button class="btn btn-warning" style="float: right" type="button" onclick="editar({{ $product->id }});">Solicitar precio especial para este proyecto</button></dt>
                            @endif
                        </dl>
                        <dl class="row mb-2">
                            @if ($product->state=='pendiente')
                                <dt class="col-2">Cantidad: <b class="badge badge-primary">{{ $product->quantity }}</b></dt>
                                <dt class="col-2">Precio Solicitado: <b class="badge badge-primary">{{ $product->price_solicitado }}</b></dt>
                                <dd class="col-3">Motivo: {{ $product->motivo }}</dt>
                            @else
                                <dt class="col-2">Cantidad: <b class="badge badge-success">{{ $product->quantity }}</b></dt>
                                <dt class="col-2">Precio Aprobado: <b class="badge badge-success">{{ $product->price }}</b></dt>
                            @endif
                            
                        </dl>
                </div>
                <div id="editable{{ $product->id }}" style="display: none">
                    <dl>
                            <dt><button class="btn btn-success" type="submit" form="precio{{ $product->id }}">Guardar</button> <button onclick="cancelar_edit({{ $product->id }})" class="btn btn-danger">Cancelar</button></dt>
                    </dl>
                <form action="{{ url('opportunities/precio_solicitado/'.$product->id) }}" method="POST" id="precio{{ $product->id }}">
                    @csrf
                    <dl class="row mb-2">
                            <dt class="col-2"><input form="precio{{ $product->id }}" type="number" class="form-control" name="quantity" placeholder="Cantidad" value="{{ $product->quantity }}"></dt>
                            <dt class="col-2"><input form="precio{{ $product->id }}" type="number" class="form-control" name="precio_solicitado" placeholder="Precio solicitado" value="{{ $product->price_solicitado }}"></dt>
                            <dt class="col-2"><input form="precio{{ $product->id }}" type="text" class="form-control" name="motivo" placeholder="Motivo de la solicitud"></dt>
                    </dl>
                </form>
                </div>
                        <dl>
                            <dt><a style="text-decoration:none;" data-toggle="collapse" href="#collapse{{ $product->id }}" class="">Ver historial de precios solicitados</a></dt>
                        </dl>
                    <div id="collapse{{ $product->id }}" class="panel-collapse collapse">
                        <section class="property_location">
                          <div style="width: 100%">
                            @if (count($product->historiales)>0)
                            @foreach ($product->historiales as $his)
                                <dl class="row mb-2">
                                    <dt class="col-2">Cantidad: {{ $his->quantity }}</dt>
                                    <dt class="col-2">Precio: {{ $his->price_solicitado }}</dt>
                                    <dt class="col-2">Estado: {{ $his->state }}</dt>
                                </dl>
                            @endforeach
                            @else
                            <dl>
                                <dt><b>No ha realizado mas solicitudes para este producto</b></dt>
                            </dl>
                            @endif
                            
                          </div>
                        </section>
                    </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
            <!--form action="{{ url('opportunities/cerrar/'.$opportunity->id) }}" id="cerrar" method="POST">
            @csrf
            @if ($cerrar==1)
            <button class="btn btn-success" title="Para cerrar la oportunidad debe tener todos los precios con el estado Aprobado y la etapa del negocio debe ser 0%-Perdido (en cuyo caso no se asignaran los puntos de la oportunidad) o 100%-BO Won" disabled>Cerrar Oportunidad</button>
            @else
            <button type="submit" form="cerrar" class="btn btn-success" title="Para cerrar la oportunidad debe tener todos los precios con el estado Aprobado y la etapa del negocio debe ser 0%-Perdido (en cuyo caso no se asignaran los puntos de la oportunidad) o 100%-BO Won">Cerrar Oportunidad</button>
            @endif
            </form-->
    </div>
</div>
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ModalTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input name="opp_id" id="opp_id" type="hidden" form="actualizar">
            <select onchange="cerrarOpp(this);" name="pro_cierre" id="pro_cierre" class="form-control" form="actualizar">
                @foreach ($probabilidades as $pro)
                @if ( $pro->id==7 )
                    @if ($cerrar==0)
                    <option value="{{ $pro->id }}">{{ $pro->description }}</option>                        
                    @else
                    <option value="{{ $pro->id }}" disabled title="Todos los precios solicitados deben estar aprobados para ganar la oportunidad">{{ $pro->description }}</option>  
                    @endif
                @else
                    <option value="{{ $pro->id }}">{{ $pro->description }}</option>
                @endif
                @endforeach
            </select>
            <br>
            <div id="cerrarOpp" style="display: none">
                <div class="form-check">
                    <label for="cerrar">Por favor cuentanos el motivo de la perdida del negocio</label>
                    <input class="form-control" type="text" id="cerrar" name="cerrar" form="actualizar">
                </div>
            </div>
            <form action="{{ url('opportunities/estado') }}" method="POST" id="actualizar">
                @csrf
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" form="actualizar">Actualizar</button>
        </div>
      </div>
    </div>
</div>
</div>
@else
    
@endif
@endsection
@section('js')
<script>
    function cerrarOpp(selectObject){
        var value = selectObject.value;
        if (value == '8'){
            document.getElementById("cerrarOpp").style.display = "block";
        }else{
            document.getElementById("cerrarOpp").style.display = "none";
        }
    }
    function editar(id){
      document.getElementById("detalles"+id).style.display = "none";
      document.getElementById("editable"+id).style.display = "block";
    }
  </script>
  <script>
    function cancelar_edit(id){
      document.getElementById("detalles"+id).style.display = "block";
      document.getElementById("editable"+id).style.display = "none";
    }
      function modal(id,name,prob_cierre){
          $('#Modal').modal('show');
          ModalTitle.innerHTML=name;
          opp_id.value=id;
          document.getElementById("pro_cierre").value = prob_cierre;
      }
  
      function actualizar() {
          if (document.getElementById("pro_cierre").value == '8'){
              return confirm('ATENCIÓN, ESTA ACCIÓN IMPLICA CERRAR LA OPORTUNIDAD CON EL ESTADO "Oportunidad Perdida" ¿Seguro que desea continuar?');
          }else{
              return confirm('DESEA ACTUALIZAR LA ETAPA DEL PROYECTO?');
          }
      }
  </script>
@endsection