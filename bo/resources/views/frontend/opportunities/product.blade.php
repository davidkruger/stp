<div class="input-group mb-1">
    <div class="input-group-prepend">
        <span class="input-group-text">{{ $product->pivot->quantity }}x</span>
        <span class="input-group-text">{{ $product->name }}</span>
    </div>
    <span class="form-control">{{ $product->description }}</span>
</div>

<div class="input-group mb-3">
    <input type="hidden" name="products[]" value="{{ $product->id }}">
    <input type="number" name="price[]"
        value="{{ $product->price }}" required
        class="form-control" placeholder="Precio por unidad">
    <div class="input-group-append">
        @if ($product->price !== null)
        <span class="input-group-text">Precio Lista: {{ number_format($product->price) }}</span>
        @endif
        @if ($product->pivot->price !== null)
        <span class="input-group-text">Precio Sugerido: {{ number_format($product->pivot->price) }}</span>
        @endif
    </div>
</div>