@extends('layouts.master')

@section('content')

<div class="row justify-content-center">
    <div class="col">
        <div class="card border-left-{{ [
                'lead'      => 'primary',
                'open'      => 'info',
                'won'       => 'success',
                'lost'      => 'danger'
            ][$opportunity->state] }}">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <h6 class="m-0 font-weight-bold text-{{ [
                                'lead'      => 'primary',
                                'open'      => 'info',
                                'won'       => 'success',
                                'lost'      => 'danger'
                            ][$opportunity->state] }}">
                            <small class="mr-3">[{{ $opportunity->id }}]</small>
                            {{ $opportunity->name }}
                        </h6>
                    </div>
                    <div class="col d-flex justify-content-end">
                        <div class="row">
                            <div class="col">
                                {{-- mayorista puede obtener leads sin mayorista --}}
                                {{-- reseller puede obtener si hay mayorista asignado y no hay reseller --}}
                                @if ($opportunity->state == 'lead' &&
                                    ((Auth::user()->company->type == 'mayorista' && $opportunity->mayorista_id == null) ||
                                    (Auth::user()->company->type == 'reseller' && $opportunity->mayorista_id !== null && $opportunity->reseller_id == null)))
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#obtenerModal">Obtener Oportunidad</button>
                                @endif

                                {{-- mayoristas y resellers pueden rechazar OP si son los asignados --}}
                             {{--    @if (in_array($opportunity->state, [ 'lead', 'open' ]) && (
                                    $opportunity->mayorista_id == Auth::user()->company_id ||
                                    $opportunity->reseller_id == Auth::user()->company_id))
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#leadModal">Liberar Oportunidad</button>
                                @endif --}}

                                {{-- mayorista puede realizar ofertas si es el asignado y la op es lead|open --}}
                                @if (Auth::user()->company->type == 'mayorista' &&
                                    in_array($opportunity->state, [ 'lead', 'open' ]) &&
                                    $opportunity->mayorista_id == Auth::user()->company_id)
                                <a href="{{ route('opportunities.offers.create', $opportunity) }}" class="btn btn-primary">Nueva Cotización</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="accordion" id="offers-accordion">

                    @include('opportunities.detail', [ 'opportunity' => $opportunity ])

                    @if (count($offers) > 0)
                        <div class="row">
                            <div class="col">
                                <h5>Mis Cotizaciones</h5>
                            </div>
                        </div>
                        @include('opportunities.my-offers', [ 'offers' => $offers ])

                    @elseif (Auth::user()->type = 'reseller')
                        <div class="row">
                            <div class="col">
                                <h5>Cotizaciones</h5>
                            </div>
                        </div>

                        @if (count($opportunity->offers) > 0)
                            @include('opportunities.offers', [ 'offers' => $opportunity->offers ])
                        @else

                            <div class="m-t-30 m-b-30 p-b-10">
                                <em>Aún no hay cotizaciones</em>
                            </div>

                        @endif

                    @endif
                </div>
            </div>

            @if (in_array($opportunity->state, [ 'open', 'won', 'lost' ]))
            <div class="card-footer">
                @if (in_array($opportunity->state, [ 'won', 'lost' ]))
                <div class="row">
                    <div class="col-2 d-flex align-items-center">
                        <h3>Resultado</h3>
                    </div>
                    <div class="col d-flex align-items-center">
                        <h4 class="text-{{ [
                            'won'       => 'success',
                            'lost'      => 'danger',
                        ][$opportunity->state] }}">{{ [
                            'won'       => 'Ganada',
                            'lost'      => 'Perdida',
                        ][$opportunity->state] }}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col">{{ $opportunity->reason }}</div>
                </div>
                @else
                    @if (Auth::user()->company->type == 'reseller' || $opportunity->createdby == Auth::user()->company_id)
                        @if ($opportunity->status >= 25)
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#winModal">Marcar como Ganada</button>
                        @endif
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#loseModal">Marcar como Perdida</button>
                    @endif
                @endif
            </div>
            @endif
        </div>
    </div>
</div>

@if ($opportunity->state == 'lead' &&
    ((Auth::user()->company->type == 'mayorista' && $opportunity->mayorista_id == null) ||
    (Auth::user()->company->type == 'reseller' && $opportunity->mayorista_id !== null && $opportunity->reseller_id == null)))
<div class="modal fade" id="obtenerModal" tabindex="-1" role="dialog" aria-labelledby="opportunityModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title" id="opportunityModalTitle">Obtener Oportunidad</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.update', $opportunity->id) }}">
                @csrf
                @method('PUT')

                @if (Auth::user()->company->type == 'mayorista' && $opportunity->mayorista_id == null)
                <input type="hidden" name="mayorista_id" value="{{ Auth::user()->company_id }}">
                <input type="hidden" name="state" value="{{ $opportunity->reseller !== null ? 'open' : 'lead' }}">
                @elseif (Auth::user()->company->type == 'reseller' && $opportunity->mayorista_id !== null && $opportunity->reseller_id == null)
                <input type="hidden" name="reseller_id" value="{{ Auth::user()->company_id }}">
                <input type="hidden" name="state" value="{{ $opportunity->mayorista !== null ? 'open' : 'lead' }}">
                @endif

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estas seguro de obtener esta Oportunidad?</h4>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Si, obtener</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endif

@if (in_array($opportunity->state, [ 'lead', 'open' ]) && (
    $opportunity->mayorista_id == Auth::user()->company_id ||
    $opportunity->reseller_id == Auth::user()->company_id))
<div class="modal fade" id="leadModal" tabindex="-1" role="dialog" aria-labelledby="leadModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="leadModalTitle">Liberar Oportunidad ?</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.update', $opportunity->id) }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="state" value="lead">

                @if ($opportunity->mayorista_id == Auth::user()->company_id)
                <input type="hidden" name="mayorista_id" value="">
                @elseif ($opportunity->reseller_id == Auth::user()->company_id)
                <input type="hidden" name="reseller_id" value="">
                @endif

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estás seguro de abandonar la Oportunidad?</h4>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Si, abandonar oportunidad</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endif

<div class="modal fade" id="winModal" tabindex="-1" role="dialog" aria-labelledby="winModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success text-white">
                <h5 class="modal-title" id="winModalTitle">Oportunidad Ganada</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.update', $opportunity->id) }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="state" value="won">
                <input type="hidden" name="status" value="100">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estás seguro de marcar la Oportunidad como Ganada?</h4>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="reason">Comentarios</label>
                                <textarea name="reason" id="reason" class="form-control" placeholder="Comentarios del cierre" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Si, ganar oportunidad</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="loseModal" tabindex="-1" role="dialog" aria-labelledby="loseModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="loseModalTitle">Oportunidad Perdida</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form method="POST" action="{{ route('opportunities.update', $opportunity->id) }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="state" value="lost">
                <input type="hidden" name="status" value="0">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mb-3 text-center">
                            <h4>Estás seguro de marcar la Oportunidad como Perdida?</h4>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="reason">Comentarios</label>
                                <textarea name="reason" id="reason" class="form-control" placeholder="Comentarios de la perdida" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Si, perder oportunidad</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
