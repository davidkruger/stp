@extends('layouts.master')

@section('content')

<div class="row justify-content-center">
    <div class="col">
        <div class="card border-left-primary">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <i class="fas fa-user-plus"></i>
                        {{ __('Editar Oportunidad') }}
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route('opportunities.update', $opportunity) }}">
                    @csrf
                    @method('PUT')
                    @include('opportunities.form')
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
