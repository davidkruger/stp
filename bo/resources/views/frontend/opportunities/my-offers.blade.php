@foreach ($offers as $idx => $offer)
<div class="row">

    <div class="col-12 col-lg-6">
        <div class="card shadow mb-2 {{ $offer->status !== 'new' ? ( $offer->status == 'rejected' ? 'border-bottom-danger' : 'border-bottom-success' ) : '' }}">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <a href="#offer_{{ $offer->id }}" class="d-block py-2" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="offer_{{ $offer->mayorista_id }}">
                            <h6 class="m-0 font-weight-bold text-{{ $offer->status !== 'new' ? ( $offer->status == 'rejected' ? 'danger' : 'success' ) : 'primary' }}">{{ $offer->created_at }}</h6>
                        </a>
                    </div>
                    <div class="col d-flex justify-content-end align-items-center">
                        {{-- <a href="" class="btn btn-sm btn-secondary">Editar Cotización</a> --}}
                    </div>
                </div>
            </div>

            <div class="collapse {{ $idx == count($offers) - 1 ? 'show' : '' }}" id="offer_{{ $offer->id }}">
                <div class="card-body">

                    @foreach($offer->products as $product)
                        <div class="row">
                            <div class="col-8">
                                {{ $product->name }}
                            </div>
                            <div class="col-4 text-right">
                                {{ number_format($product->pivot->price) }}
                            </div>
                        </div>
                    @endforeach

                </div>

                @if ($offer->status !== 'new')
                <div class="card-footer">
                    <div class="row">

                        <div class="col">
                            <label class="text-{{ $offer->status == 'rejected' ? 'danger' : 'success' }}">{{ $offer->status == 'rejected' ? 'Rechazada' : 'Aceptada' }}</label>
                        </div>

                        <div class="col d-flex justify-content-end">

                            <label class="text-{{ $offer->status == 'rejected' ? 'danger' : 'success' }}">{{ $offer->reason }}</label>

                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
</div>

</div>
@endforeach