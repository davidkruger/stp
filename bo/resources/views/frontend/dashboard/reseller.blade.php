@extends('layouts.master')

@section('content')
<div class="row justify-content-center mb-4">
    <div class="col-12 col-md-12">
        <a>
            <img class="img-responsive" src="{{ url('/assets/img/header.jpg') }}" style="width:100%; " alt=""/>
        </a>
    </div>
    <div class="col-12 col-md-12">
        <div class="card border-left-primary rankPosition">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <h4>Tu posicion en el ranking de Samsung Te Premia: #{{ $rank_position }} con {{ $rank_puntos }} puntos acumulados este mes</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header bg-primary text-white py-3">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <h6 class="m-0 font-weight-bold">{{ __('Leads disponibles') }}</h6>
                    </div>
                    <div class="col mr-auto d-flex justify-content-end">
                        {{-- <a href="{{ route('opportunities.create') }}" class="btn btn-sm btn-primary">Crear nueva Oportunidad</a> --}}
                    </div>
                </div>
            </div>

            <div class="card-body bg-light">

                @if (count($leads) > 0)

                    <div class="row">
                        <div class="col-3">
                            <h5 class="font-weight-bold">Nombre</h5>
                        </div>
                        <div class="col-3">
                            <h5 class="font-weight-bold">Puntos</h5>
                        </div>
                        <div class="col-3">
                            <h5 class="font-weight-bold">Monto</h5>
                        </div>
                    </div>
                    {{-- Reseller opportunities --}}
                    @foreach($leads as $opportunity)
                        @include('dashboard.reseller.lead')
                    @endforeach

                @else

                    <div class="text-center m-t-30 m-b-30 p-b-10">
                        <em>No hay Leads disponibles</em>
                    </div>

                @endif
            </div>

        </div>
    </div>
</div>

<div class="row justify-content-center mb-4">
    <div class="col">
        <div class="card border-left-primary">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-9 d-flex align-items-center">

                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold active" id="pills-open-tab"
                                    data-toggle="pill" href="#pills-open" role="tab" aria-controls="pills-open"
                                    aria-selected="true">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-clipboard-list"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades en curso') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold" id="pills-won-tab"
                                    data-toggle="pill" href="#pills-won" role="tab" aria-controls="pills-won"
                                    aria-selected="false">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-dollar-sign"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades ganadas') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold" id="pills-lost-tab"
                                    data-toggle="pill" href="#pills-lost" role="tab" aria-controls="pills-lost"]
                                    aria-selected="false">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-thumbs-down"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades perdidas') }}</span>
                                </a>
                            </li>
                        </ul>

                        {{-- <h6 class="m-0 font-weight-bold text-info">{{ __('Oportunidades en curso') }}</h6> --}}
                    </div>
                    <div class="col-3 mr-auto d-flex justify-content-end">
                        <a href="{{ route('opportunities.create') }}" class="btn btn-lg btn-primary btn-icon-split font-weight-bold p-3">
                            <span class="icon d-flex align-items-center">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Crear nueva Oportunidad</span>
                        </a>
                        {{-- <a href="{{ route('opportunities.create') }}" class="btn btn-sm btn-primary">Crear nueva Oportunidad</a> --}}
                    </div>
                </div>
            </div>

            <div class="card-body bg-light">

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-open" role="tabpanel" aria-labelledby="pills-open-tab">

                        @php
                        $open = $opportunities->filter(function($opportunity) {
                            return
                                // state open
                                in_array($opportunity->state, [ 'open' ]) ||
                                // lead & reseller=this
                                $opportunity->state == 'lead' && $opportunity->reseller_id == Auth::user()->company_id;
                        });
                        @endphp
                        @if (count($open) > 0)

                            <div class="row">
                                <div class="col-3">
                                    <h5 class="font-weight-bold">Nombre</h5>
                                </div>
                                <div class="col-1">
                                    <h5 class="font-weight-bold">Fecha de Creación/Cierre</h5>
                                </div>
                                <div class="col-1">
                                    <h5 class="font-weight-bold">Puntos</h5>
                                </div>
                                <div class="col-1">
                                    <h5 class="font-weight-bold">Monto Parcial</h5>
                                </div>
                                @if (Auth::user()->type == 'reseller_gerente')
                                <div class="col-1">
                                    <h5 class="font-weight-bold">Usuario</h5>
                                </div>
                                @endif
                                <div class="col-3 text-center">
                                    <h5 class="font-weight-bold">Etapa del Proyecto</h5>
                                </div>
                                <div class="col-2 text-center">
                                    <h5 class="font-weight-bold"></h5>
                                </div>
                            </div>

                            {{-- Reseller opportunities --}}
                            @foreach($open as $opportunity)
                                @include('dashboard.reseller.open')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades en curso</em>
                            </div>

                        @endif

                    </div>
                    <div class="tab-pane fade" id="pills-won" role="tabpanel" aria-labelledby="pills-won-tab">

                        @php
                        $won = $opportunities->filter(function($opportunity) {
                            return $opportunity->state == 'won';
                        });
                        @endphp
                        @if (count($won) > 0)

                        <div class="row">
                            <div class="col-3">
                                <h5 class="font-weight-bold">Nombre</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Fecha de Creación/Cierre</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Puntos</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Monto Final</h5>
                            </div>
                            @if (Auth::user()->type == 'reseller_gerente')
                            <div class="col-1">
                                <h5 class="font-weight-bold">Usuario</h5>
                            </div>
                            @endif
                            <div class="col-3 text-center">
                                <h5 class="font-weight-bold">Etapa del Proyecto</h5>
                            </div>
                            <div class="col-2 text-center">
                                <h5 class="font-weight-bold"></h5>
                            </div>
                        </div>

                            {{-- Reseller opportunities --}}
                            @foreach($won as $opportunity)
                                @include('dashboard.reseller.won')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades ganadas</em>
                            </div>

                        @endif

                    </div>
                    <div class="tab-pane fade" id="pills-lost" role="tabpanel" aria-labelledby="pills-lost-tab">

                        @php
                        $lost = $opportunities->filter(function($opportunity) {
                            return in_array($opportunity->state, [ 'lost' ]);
                        });
                        @endphp
                        @if (count($lost) > 0)

                        <div class="row">
                            <div class="col-3">
                                <h5 class="font-weight-bold">Nombre</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Fecha de Creación/Cierre</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Puntos</h5>
                            </div>
                            <div class="col-1">
                                <h5 class="font-weight-bold">Monto Parcial</h5>
                            </div>
                            @if (Auth::user()->type == 'reseller_gerente')
                            <div class="col-1">
                                <h5 class="font-weight-bold">Usuario</h5>
                            </div>
                            @endif
                            <div class="col-3 text-center">
                                <h5 class="font-weight-bold">Etapa del Proyecto</h5>
                            </div>
                            <div class="col-2 text-center">
                                <h5 class="font-weight-bold"></h5>
                            </div>
                        </div>

                            {{-- Reseller opportunities --}}
                            @foreach($lost as $opportunity)
                                @include('dashboard.reseller.lost')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades perdidas</em>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="ModalTitle"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <input name="opp_id" id="opp_id" type="hidden" form="actualizar">
                <select name="pro_cierre" onchange="cerrarOpp(this);" id="pro_cierre" class="form-control" form="actualizar">
                    @foreach ($probabilidades as $pro)
                        @if ($pro->id!=7)
                        <option value="{{ $pro->id }}">{{ $pro->description }}</option>
                        @endif
                    @endforeach
                </select>
                <br>
                <div id="cerrarOpp" style="display: none">
                    <div class="form-check">
                        <label for="cerrar">Por favor cuentanos el motivo de la perdida del negocio</label>
                        <input class="form-control" type="text" id="cerrar" name="cerrar" form="actualizar">
                    </div>
                </div>
                <form action="{{ url('opportunities/estado') }}" method="POST" id="actualizar">
                    @csrf
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-success" form="actualizar">Actualizar</button>
            </div>
          </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    function cerrarOpp(selectObject){
        var value = selectObject.value;
        if (value == '8'){
            document.getElementById("cerrarOpp").style.display = "block";
        }else{
            document.getElementById("cerrarOpp").style.display = "none";
        }
    }
    function modal(id,name,prob_cierre){
        $('#Modal').modal('show');
        ModalTitle.innerHTML=name;
        opp_id.value=id;
        document.getElementById("pro_cierre").value = prob_cierre;
    }

    function actualizar() {
        if (document.getElementById("pro_cierre").value == '8'){
            return confirm('ATENCIÓN, ESTA ACCIÓN IMPLICA CERRAR LA OPORTUNIDAD CON EL ESTADO "Oportunidad Perdida" ¿Seguro que desea continuar?');
        }else{
            return confirm('DESEA ACTUALIZAR LA ETAPA DEL PROYECTO?')
        }
    }
</script>
@endpush