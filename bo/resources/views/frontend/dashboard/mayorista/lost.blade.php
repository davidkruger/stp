<div class="card m-0 my-2 shadow">
    <div class="card-body p-3">

        <div class="row align-items-center">
            <div class="col-6">
                <small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}
            </div>
            <div class="col-4">
                {{ $opportunity->reseller->name }}
            </div>
            <div class="col-2 d-flex justify-content-end align-items-center">
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-outline-primary btn-icon-split">
                        <span class="icon d-flex align-items-center">
                            <i class="fas fa-eye"></i>
                        </span>
                        <span class="text">Detalles</span>
                    </a>
                    {{-- <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-secondary">Detalles</a> --}}
                </div>
            </div>
        </div>

    </div>
</div>