<div class="card m-0 my-2 shadow">
    <div class="card-body p-3">

        <div class="row align-items-center">
            <div class="col-2">
                <small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}
            </div>
            <div class="col-2">
                {{ $opportunity->reseller !== null ? $opportunity->reseller->name : '--' }}
            </div>
            <div class="col-2" style="text-align: center">
                {{ $opportunity->puntos !== null ? $opportunity->puntos : '--' }}
            </div>
            <div class="col-2" style="text-align: center">
                {{ $opportunity->monto_total !== null ? $opportunity->monto_total : '--' }}
            </div>
            <div class="col-2 text-center">
                <div class="progress w-100">
                    <div class="progress-bar bg-{{ $opportunity->status > 0 ? 'primary' : 'secondary' }}" role="progressbar"
                        style="width: {{ $opportunity->status > 0 ? $opportunity->status : 100 }}%;"
                        aria-valuenow="{{ $opportunity->status }}" aria-valuemin="0" aria-valuemax="100">{{ $opportunity->status }}%</div>
                </div>
            </div>
            <div class="col d-flex justify-content-end">
                <div class="btn-group btn-group-sm" role="group">
                    @if ($opportunity->createdby == Auth::user()->company_id)
                    <a href="{{ route('opportunities.edit', [ $opportunity->id ]) }}" class="btn btn-outline-secondary btn-icon-split">
                        <span class="icon d-flex align-items-center">
                            <i class="fas fa-edit"></i>
                        </span>
                        {{-- <span class="text">Editar</span> --}}
                    </a>
                    @endif

                {{--     <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-outline-primary btn-icon-split">
                        <span class="icon d-flex align-items-center">
                            <i class="fas fa-eye"></i>
                        </span>
                        {{-- <span class="text">Ver detalle</span> --}}
                {{--     </a> --}}

                    @if (count($opportunity->offers()->fromMayorista(Auth::user()->company_id)->get()) > 0)
                    <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-outline-primary btn-icon-split">
                        <span class="icon d-flex align-items-center">
                            <i class="fas fa-money-bill-alt"></i>
                        </span>
                        <span class="text">Mis cotizaciones</span>
                    </a>
                    @else
                    <a href="{{ route('opportunities.show', $opportunity) }}" class="btn btn-outline-primary btn-icon-split">
                        <span class="icon d-flex align-items-center">
                            <i class="fas fa-money-bill-alt"></i>
                        </span>
                        <span class="text">Ver</span>
                    </a>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>