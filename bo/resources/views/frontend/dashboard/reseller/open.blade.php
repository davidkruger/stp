<div class="card m-0 my-2 shadow">
    <div class="card-body p-3">

        <div class="row align-items-center">
            <div class="col-3">
                <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" title="Ver Detalles del Proyecto">
                <small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}
                </a>
            </div>
            <div class="col-1">
                {{ date("d/m/Y", strtotime($opportunity->created_at)) }} <br>
                {{ date("d/m/Y", strtotime($opportunity->tentative)) }}
            </div>
            <div class="col-1">
                {{ number_format($opportunity->puntos,0,',','.') }}
            </div>
            <div class="col-1">
                {{ number_format($opportunity->monto,0,',','.') }}
            </div>
            @if (Auth::user()->type == 'reseller_gerente')
            <div class="col-1">
                {{ $opportunity->reseller->name }}
            </div>
            @endif
            <div class="col-3 d-flex align-items-center">
                <div class="progress w-100">
                    <div class="progress-bar bg-{{ $opportunity->probabilidad_cierre->porcentaje > 0 ? 'primary' : 'secondary' }}" role="progressbar"
                        style="width: {{ $opportunity->probabilidad_cierre->porcentaje > 0 ? $opportunity->probabilidad_cierre->porcentaje : 100 }}%;"
                        aria-valuenow="{{ $opportunity->probabilidad_cierre->porcentaje }}" aria-valuemin="0" aria-valuemax="100">{{ $opportunity->probabilidad_cierre->porcentaje }}%</div>
                </div>
            </div>
            <div class="col-1 text-center">
               <button href="" class="btn btn-secondary" title="Click para cambiar la etapa del Proyecto" onclick="modal({{ $opportunity->id }},'{{ $opportunity->name }}',{{ $opportunity->probabilidad_cierre_id }});" type="button">{{ $opportunity->probabilidad_cierre->nombre }}</button>
            </div>
            <div class="col-1">
                @switch($opportunity->protegido)
                @case('si')
                   <p title="Negocio Protegido"><i style="color: green" class="fas fa-thumbs-up"></i></p>
                    @break
                @case('no')
                    <p title="Negocio Sin Protección"><i style="color: red" class="far fa-thumbs-down"></i></p>
                    @break
                @default
                    <p title="Negocio Pendiente de Protección"><i style="color: orange" class="fas fa-exclamation-triangle"></i></p> 
                @endswitch
            </div>
        </div>

    </div>
</div>