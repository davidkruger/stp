<div class="card m-0 my-2 shadow">
    <div class="card-body p-3">

        <div class="row align-items-center">
            <div class="col-3">
                <small>[{{ $opportunity->id }}]</small> {{ $opportunity->name }}
            </div>

            <div class="col-3">
                {{ $opportunity->puntos }}
            </div>
            <div class="col-3">
                {{ $opportunity->monto }}
            </div>
            <div class="col-2 d-flex justify-content-end">
                <div class="row">
                    <div class="col">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-outline-primary btn-icon-split">
                                <span class="icon d-flex align-items-center">
                                    <i class="fas fa-eye"></i>
                                </span>
                                <span class="text">Ver detalles</span>
                            </a>
                            {{-- <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-primary">Obtener</a> --}}
                            {{-- <a href="{{ route('opportunities.show', [ $opportunity->id ]) }}" class="btn btn-primary">Ver detalles</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>