@extends('layouts.master')

@section('content')


<div class="row justify-content-center mb-4">
    <div class="col-12 col-md-12 mb-4">
        <a>
            <img class="img-responsive" src="{{ url('/assets/img/header.jpg') }}" style="width:100%; " alt=""/>
        </a>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header bg-primary text-white py-3">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <h6 class="m-0 font-weight-bold">{{ __('Leads disponibles') }}</h6>
                    </div>
                    <div class="col mr-auto d-flex justify-content-end">
                        {{-- <a href="{{ route('opportunities.create') }}" class="btn btn-sm btn-primary">Crear nueva Oportunidad</a> --}}
                    </div>
                </div>
            </div>

            <div class="card-body bg-light">

                @php
                // solo leads sin mayorista
                $leads = $leads->filter(function($lead) {
                    return $lead->mayorista_id === null;
                });
                @endphp
                @if (count($leads) > 0)

                    <div class="row">
                        <div class="col-2">
                            <h5 class="font-weight-bold">Nombre</h5>
                        </div>
                        <div class="col-2">
                            <h5 class="font-weight-bold">Compañía</h5>
                        </div>
                        <div class="col-2">
                            <h5 class="font-weight-bold">Reseller</h5>
                        </div>
                        <div class="col-2">
                            <h5 class="font-weight-bold">Puntos</h5>
                        </div>
                        <div class="col-2">
                            <h5 class="font-weight-bold">Monto</h5>
                        </div>
                    </div>
                    {{-- Reseller opportunities --}}
                    @foreach($leads as $opportunity)
                        @include('dashboard.mayorista.lead')
                    @endforeach

                @else

                    <div class="text-center m-t-30 m-b-30 p-b-10">
                        <em>No hay Leads disponibles</em>
                    </div>

                @endif
            </div>

        </div>
    </div>
</div>


<div class="row justify-content-center mb-4">
    <div class="col">
        <div class="card border-left-primary">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col d-flex align-items-center">

                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold active" id="pills-open-tab"
                                    data-toggle="pill" href="#pills-open" role="tab" aria-controls="pills-open"
                                    aria-selected="true">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-clipboard-list"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades en curso') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold" id="pills-won-tab"
                                    data-toggle="pill" href="#pills-won" role="tab" aria-controls="pills-won"
                                    aria-selected="false">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-dollar-sign"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades ganadas') }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn-icon-split p-3 mr-2 font-weight-bold" id="pills-lost-tab"
                                    data-toggle="pill" href="#pills-lost" role="tab" aria-controls="pills-lost"]
                                    aria-selected="false">
                                    <span class="icon d-flex align-items-center">
                                        <i class="fas fa-thumbs-down"></i>
                                    </span>
                                    <span class="text">{{ __('Oportunidades perdidas') }}</span>
                                </a>
                            </li>
                        </ul>

                        {{-- <h6 class="m-0 font-weight-bold text-info">{{ __('Oportunidades en curso') }}</h6> --}}
                    </div>
                    <div class="col mr-auto d-flex justify-content-end">
                        <a href="{{ route('opportunities.create') }}" class="btn btn-lg btn-primary btn-icon-split font-weight-bold p-3">
                            <span class="icon d-flex align-items-center">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Crear nueva Oportunidad</span>
                        </a>
                        {{-- <a href="{{ route('opportunities.create') }}" class="btn btn-lg btn-primary font-weight-bold p-3">Crear nueva Oportunidad</a> --}}
                    </div>
                </div>
            </div>

            <div class="card-body bg-light">

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-open" role="tabpanel" aria-labelledby="pills-open-tab">

                        @php
                        $open = $opportunities->filter(function($opportunity) {
                            return
                                // state open
                                in_array($opportunity->state, [ 'open' ]) ||
                                // lead & mayorista=this
                                $opportunity->state == 'lead' && $opportunity->mayorista_id == Auth::user()->company_id;
                        });
                        @endphp
                        @if (count($open) > 0)

                            <div class="row">
                                <div class="col-2">
                                    <h5 class="font-weight-bold">Nombre</h5>
                                </div>
                                <div class="col-2">
                                    <h5 class="font-weight-bold">Reseller</h5>
                                </div>
                                <div class="col-2 text-center">
                                    <h5 class="font-weight-bold">Puntos</h5>
                                </div>
                                <div class="col-2 text-center">
                                    <h5 class="font-weight-bold">Monto</h5>
                                </div>
                                <div class="col-2 text-center">
                                    <h5 class="font-weight-bold">Estado</h5>
                                </div>
                            </div>

                            {{-- Open opportunities --}}
                            @foreach($open as $opportunity)
                                @include('dashboard.mayorista.open')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades en curso</em>
                            </div>

                        @endif

                    </div>
                    <div class="tab-pane fade" id="pills-won" role="tabpanel" aria-labelledby="pills-won-tab">

                        @php
                        $won = $opportunities->filter(function($opportunity) {
                            return $opportunity->state == 'won';
                        });
                        @endphp
                        @if (count($won) > 0)

                            <div class="row">
                                <div class="col-6">
                                    <h5 class="font-weight-bold">Nombre</h5>
                                </div>
                                <div class="col-4">
                                    <h5 class="font-weight-bold">Compañía</h5>
                                </div>
                            </div>

                            {{-- Open opportunities --}}
                            @foreach($won as $opportunity)
                                @include('dashboard.mayorista.won')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades ganadas</em>
                            </div>

                        @endif

                    </div>
                    <div class="tab-pane fade" id="pills-lost" role="tabpanel" aria-labelledby="pills-lost-tab">

                        @php
                        $lost = $opportunities->filter(function($opportunity) {
                            return in_array($opportunity->state, [ 'lost' ]);
                        });
                        @endphp
                        @if (count($lost) > 0)

                            <div class="row">
                                <div class="col-6">
                                    <h5 class="font-weight-bold">Nombre</h5>
                                </div>
                                <div class="col-4">
                                    <h5 class="font-weight-bold">Compañía</h5>
                                </div>
                            </div>

                            {{-- Open opportunities --}}
                            @foreach($lost as $opportunity)
                                @include('dashboard.mayorista/lost')
                            @endforeach

                        @else

                            <div class="text-center m-t-30 m-b-30 p-b-10">
                                <em>No tienes oportunidades perdidas</em>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection