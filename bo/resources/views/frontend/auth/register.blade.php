@extends('layouts.app')

@section('body-class', 'bg-gradient-primary')

@section('app')

<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
            <div class="row">

                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>

                <div class="col-lg-7">
                    <div class="p-5">

                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Crea tu cuenta!</h1>
                        </div>

                        <form class="user" method="POST" action="{{ route('register_client') }}">
                            @csrf

                            <div class="form-group row">
                            <div class="col-sm-12 mb-6 mb-sm-0">
                            <input type="text" class="form-control form-control-user {{ $errors->has('name') ? 'is-danger' : '' }}" id="name" name="name" value="{{ isset($user) && !old('name') ? $user->name : old('name') }}" placeholder="Nombre completo">
                            </div>
                            </div>

                            <div class="form-group">
                            <input type="email" class="form-control form-control-user {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ isset($user) && !old('email') ? $user->email : old('email') }}" placeholder="Email">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user {{ $errors->has('company') ? 'is-danger' : '' }}" name="company" value="{{ isset($user) && !old('company') ? $user->company : old('company') }}" placeholder="Ingrese el nombre de su compañia">
                        {{--         <select name="company_id" class="form-control {{ $errors->has('company_id') ? 'is-danger' : '' }}" required placeholder="Compañía" value="{{ isset($user) && !old('company_id') ? $user->company_id : old('company_id') }}">
                                        <option selected disabled hidden>-- Seleccione su Compañía --</option>
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}" {{ (isset($user) && !old('company_id') ? $user->company_id : old('company_id')) == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                                    @endforeach
                                    
                                </select> --}}
                            </div>

                            <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="password" class="form-control form-control-user {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} name="password" id="password" placeholder="Contraseña (mínimo 6 caracteres)">
                            </div>
                            <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user {{ $errors->has('password') ? 'is-danger' : '' }}" {{ isset($user) ? '' : 'required' }} name="password_confirmation" id="password_confirmation"placeholder="Contraseña (mínimo 6 caracteres)">
                            </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-user btn-block">
                            Registrarse
                            </button>

                            <hr>
                        </form>

                        <hr>
                        <div class="text-center">
                        <a class="small" href="{{ url('/') }}">Ya tienes una cuenta? Ingresa aquí!</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container d-none">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
