@extends('layouts.app')

@section('body-class', 'bg-gradient-primary')

@section('app')

<div class="container">
    <!-- Outer Row -->    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">

                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>

                        <div class="col-lg-6">
                            <div class="p-5">

                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Portal de Oportunidades</h1><br>
                                    <h2>Acceso Reseller</h2>
                                </div>

                                <form class="user" method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group">
                                        <input name="email" type="email" value="{{ old('email') }}"
                                            required autocomplete="email" autofocus
                                            class="form-control form-control-user @error('email') is-invalid @enderror"
                                            aria-describedby="emailHelp" placeholder="{{ __('E-Mail Address') }}">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <input name="password" type="password"
                                            required autocomplete="current-password"
                                            class="form-control form-control-user @error('password') is-invalid @enderror"
                                            placeholder="Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-user btn-block">{{ __('Login') }}</button>

                                    <hr>

                                </form>

                                <hr>


                                <div class="text-center">
                                    <a class="small" href="{{ url('register_client') }}">{{ __('Registrate') }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3" style="text-align: right"><a style="text-align: right !important; color: white" href="{{ url('/backend/login') }}">--> Acceso Samsung</a></div>
        </div>
    </div>
</div>

@endsection
