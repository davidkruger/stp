@extends('layouts.app')

@section('body-class', 'bg-gradient-primary')

@section('app')

<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
            <div class="row">

                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>

                <div class="col-lg-7">
                    <div class="p-5">

                        <div class="text-center">
                            <h1 class="h4">Tu cuenta ha sido creada!</h1>
                        </div>
                        <div>
                            <h2>Durante las proximas 48 horas tu cuenta sera dada de alta y podras acceder al sistema!!</h2>
                        </div>
                        <div>

                        </div>
                        <div>
                            <a href="{{ url('/') }}"><- Volver</a>
                        </div>
                        <hr>

                        <div class="text-center">
                            <a class="small" href="{{ url('/') }}">Ya tienes una cuenta? Ingresa aquí!</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container d-none">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
