@extends('layouts.app')

@section('app')
@if (Auth::user()->state === 2)
    <!-- Page Wrapper -->
    <div id="main-wrapper">
        <!-- Topbar -->
        @include('layouts.topbar')
        <!-- Begin Page Contents -->
        <div class="container-fluid pt-4">
            <!-- Main Content -->
            @yield('content')
        <!-- End Page Contents -->
        </div>
    <!-- End of Page Wrapper -->
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
@else
    <h2>Su cuenta esta desabilitada, por favor vuelva a intentar mas tarde o pongase en contacto con el administrador del sistema</h2>
    <form method="POST" action="{{ route('logout') }}" id="logout-form">@csrf
        <button type="submit" class="btn btn-primary"><-Volver</button>
    </form>
@endif
@endsection