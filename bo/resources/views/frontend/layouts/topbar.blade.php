<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-primary topbar">
    <!-- Navbar - Brand -->
    <a href="{{ url('/') }}" class="navbar-brand d-flex align-items-center justify-content-center">
        <div class="navbar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="navbar-brand-text mx-3 d-none d-lg-block">Samsung <sup>BOM</sup></div>
    </a>
    <a class="navbar-brand" href="{{ route('dashboard') }}">Dashboard</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav mr-auto">

            @if (Auth::user()->type == 'mayorista')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('products') }}">Precios de Lista</a>
            </li>
            @endif

            {{--
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
            --}}
        </ul>

        <ul class="navbar-nav ml-auto">
            <!-- Nav Item - Alerts -->
            {{-- @include('layouts.alerts') --}}
            <!-- Nav Item - Messages -->
            {{-- @include('layouts.messages') --}}
            <!-- divider -->
            {{-- <div class="topbar-divider d-none d-sm-block"></div> --}}
            <!-- Nav Item - User Information -->
            @include('layouts.user')
        </ul>

        <form class="form-inline my-2 my-lg-0 d-none">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>

</nav>


<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel">Cerrar Sesión?</h5>
        <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
        <div class="modal-body">Estas seguro de finalizar tu sesión?</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Si, salir</a>
        <form method="POST" action="{{ route('logout') }}" class="d-none" id="logout-form">@csrf</form>
      </div>
    </div>
  </div>
</div>