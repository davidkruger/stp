@extends('layouts.master')

@section('content')

<div class="row justify-content-center">
    <div class="col">
        <div class="card border-left-primary">
            <div class="card-header"><small>[{{ $opportunity->code }}]</small> {{ $opportunity->name }}</div>

            <div class="card-body">

                @include('opportunities.detail', [
                    'opportunity'   => $opportunity,
                    'withproducts'  => false
                ])

                <form method="POST" action="{{ route('opportunities.offers.store', $opportunity) }}">
                    @csrf

                    <div class="row">
                        <div class="col">

                            <div class="table-responsive">
                                <table class="table table-sm table-borderless table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="w-100px text-center">Cantidad</th>
                                            <th>Producto</th>
                                            <th>Descripcion</th>
                                            <th class="text-right">Precio Lista</th>
                                            <th class="text-right">Precio Sugerido</th>
                                            <th class="text-right">Precio Ofertado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($opportunity->products as $product)
                                        <input type="hidden" name="products[]" value="{{ $product->id }}">
                                        <tr>
                                            <td class="text-center">{{ $product->pivot->quantity }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td class="text-right">{{ number_format($product->price) }}</td>
                                            <td class="text-right">{{ $product->pivot->price !== null ? number_format($product->pivot->price) : '--' }}</td>
                                            <td>
                                                @if ($product->mayorista_id == Auth::user()->company_id)
                                                <input type="number" name="price[]" required class="form-control form-control-sm text-right" placeholder="Precio ofrecido">
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col d-flex justify-content-end">
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <a href="{{ route('dashboard') }}" class="btn btn-secondary">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection
