@extends('layouts.master')

@section('content')

<div class="row justify-content-center">
    <div class="col">
        <div class="card border-left-primary">
            <div class="card-header"><small>[{{ $opportunity->code }}]</small> {{ $opportunity->name }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('opportunities.offers.update', [ $opportunity, $offer->company_id ]) }}">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col">
                            @foreach ($offer->products as $oProduct)
                                @include('offers.product-edit')
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <div class="col d-flex justify-content-end">
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <a href="{{ route('dashboard') }}" class="btn btn-secondary">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection
