<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\File;
use App\Models\Vertical;
use Faker\Generator as Faker;

$factory->define(Vertical::class, function (Faker $faker) {
    $icon = File::inRandomOrder()->first();
    return [
        'code'          => $faker->unique()->word,
        'description'   => $faker->text(16),
        'icon_id'       => $icon->id,
    ];
});
