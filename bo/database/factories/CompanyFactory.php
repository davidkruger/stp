<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use App\Models\File;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    $name = $faker->company;
    $logo = File::inRandomOrder()->first();
    $type = Arr::random([ 'mayorista', 'reseller', 'company' ]);
    $reseller = $type == 'reseller' ? Arr::random([ 'authorized', 'preferred', 'priority' ]) : null;
    return [
        'ruc'       => $faker->iban('PY', '', 8),
        'name'      => $name,
        'real_name' => $name.' '.$faker->companySuffix,
        'phone'     => $faker->phoneNumber,
        'email'     => $faker->email,
        'address'   => $faker->streetAddress,
        'branches'  => $faker->numberBetween(1, 5),
        'avg_branches'  => $faker->numberBetween(1, 5),
        'logo_id'   => $logo->id,
        'constitution'  => $faker->date,
        'type'      => $type,
        'reseller'  => $reseller,
    ];
});
