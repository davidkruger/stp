<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use App\Models\Division;
use App\Models\File;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $mayorista = Company::mayoristas()->inRandomOrder()->first();
    $division = Division::inRandomOrder()->first();
    $image = File::inRandomOrder()->first();
    return [
        'mayorista_id'  => $mayorista->id,
        'name'          => ucfirst($faker->words(3, true)),
        'description'   => $faker->words(10, true),
        'division_id'   => $division->id,
        'image_id'      => $image->id,
        'price'         => $faker->numberBetween(100000, 5000000),
        'points'        => $faker->numberBetween(1, 20),
    ];
});
