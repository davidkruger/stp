<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\File;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(File::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'type'  => 'image',
        'url'   => 'images/'.$faker->image('storage/app/images', 320, 240, null, false)
    ];
});
