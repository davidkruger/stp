<?php

use Illuminate\Database\Seeder;

class DemoSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call([
            // create Files
            FilesSeeder::class,
            // create Divisions
            DivisionsSeeder::class,
            // create Verticals
            VerticalsSeeder::class,
            // create Companies
            CompaniesSeeder::class,
            // create Products
            ProductsSeeder::class,
        ]);
    }
}
