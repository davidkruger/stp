<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // create 5 example files
        factory(Product::class, 15)->create()->each(function($product) {
            //
        });
    }
}
