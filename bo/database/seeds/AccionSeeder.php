<?php

use Illuminate\Database\Seeder;
use App\Models\Accion;

class AccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accion = new Accion;
        $accion->description = "Crear";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Monto";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Puntos por superar monto";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Resseller Authorized";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Resseller Preferred";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Resseller Priority";
        $accion->puntos = 0;
        $accion->save();
        
        $accion = new Accion;
        $accion->description = "Resseller Not Qualify";
        $accion->puntos = 0;
        $accion->save();

        $accion = new Accion;
        $accion->description = "Oportunidad Ganada";
        $accion->puntos = 0;
        $accion->save();
    }
}
