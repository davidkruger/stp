<?php

use App\Models\Vertical;
use Illuminate\Database\Seeder;

class VerticalsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // create 5 example files
        factory(Vertical::class, 10)->create();
    }
}
