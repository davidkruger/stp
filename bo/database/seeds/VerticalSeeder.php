<?php

use Illuminate\Database\Seeder;
use App\Models\Vertical;

class VerticalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Vertical;
        $item->description = "FINANZAS";
        $item->code = "FINANZAS";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "EDUCACION";
        $item->code = "EDUCACION";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "SALA DE CONTROL";
        $item->code = "SALA DE CONTROL";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "RETAIL";
        $item->code = "RETAIL";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "ENTRETENMIENTO";
        $item->code = "ENTRETENMIENTO";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "C-STORE";
        $item->code = "C-STORE";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "QSR";
        $item->code = "QSR";
        $item->icon_id = 2;
        $item->save();

        $item = new Vertical;
        $item->description = "CORPORATIVO";
        $item->code = "CORPORATIVO";
        $item->icon_id = 2;
        $item->save();
    }
}
