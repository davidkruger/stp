<?php

use App\Models\User;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MayoristaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::where('type', 'mayorista')->first();
        $user = new User;
        $user->name = "Bonus";
        $user->email = "bonus@samsung.com";
        $user->password = Hash::make('123456');
        $user->type = "mayorista";
        $user->state = 2;
        $user->company_id = $company->id;
        $user->save();

        $company = Company::where('type', 'reseller')->first();
        $user = new User;
        $user->name = "Gerente Diviser";
        $user->email = "gerente@diviser.com";
        $user->password = Hash::make('123456');
        $user->type = "reseller_gerente";
        $user->state = 2;
        $user->company_id = $company->id;
        $user->save();

        $user = new User;
        $user->name = "Usuario Diviser";
        $user->email = "usuario1@diviser.com";
        $user->password = Hash::make('123456');
        $user->type = "reseller";
        $user->state = 2;
        $user->company_id = $company->id;
        $user->save();
    }
}
