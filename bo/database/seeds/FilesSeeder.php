<?php

use App\Models\File;
use Illuminate\Database\Seeder;

class FilesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // create 2 example files
        factory(File::class, 5)->create();
    }
}
