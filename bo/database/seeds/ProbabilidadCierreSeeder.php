<?php

use App\Models\ProbabilidadCierre;
use Illuminate\Database\Seeder;

class ProbabilidadCierreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new ProbabilidadCierre;
        $item->description = "10% - Opportunity";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "25% - Proposal Sent";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "35% - RFQ / RFP ";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "50% - Under negotiation";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "75% - Final proposal sent";
        $item->save();
        
        $item = new ProbabilidadCierre;
        $item->description = "85% - Contract Signing";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "100% - BO Won";
        $item->save();

        $item = new ProbabilidadCierre;
        $item->description = "0% - Perdida";
        $item->save();
    }
}
