<?php

use App\Models\Division;
use Illuminate\Database\Seeder;

class DivisionsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // create 5 example files
        //factory(Division::class, 10)->create();

        $item = new Division;
        $item->name = "TV";
        $item->save();

        $item = new Division;
        $item->name = "SmarthPhone";
        $item->save();

        $item = new Division;
        $item->name = "Tablet";
        $item->save();

        $item = new Division;
        $item->name = "Notebook";
        $item->save();

        $item = new Division;
        $item->name = "Monitor";
        $item->save();

    }
}
