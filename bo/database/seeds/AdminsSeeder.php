<?php

use App\Models\Samsung;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // get a random password
  /*      $password = str_random(16);
        // create user
        $root = new Samsung;
        // set root account data with a random password
        $root->fill([
            'name'      => 'Administrator',
            'email'     => 'root@samsung.com.py',
            'password'  => Hash::make($password),
            'type'      => 'samsung',
        ]);
        // save root account
        $root->save();
*/
        $password = 123456789;
        $root = new Samsung;
        $root->fill([
            'name'      => 'Administrator',
            'email'     => 'admin@samsung.com',
            'password'  => Hash::make($password),
            'type'      => 'samsung',
        ]);
        $root->save();

        $root = new Samsung;
        $root->fill([
            'name'      => 'Alejandro',
            'email'     => 'alejandro@samsung.com',
            'password'  => Hash::make($password),
            'type'      => 'samsung',
        ]);
        $root->save();

        $root = new Samsung;
        $root->fill([
            'name'      => 'Bernardo',
            'email'     => 'bernardo@samsung.com',
            'password'  => Hash::make($password),
            'type'      => 'samsung',
        ]);
        $root->save();

        $root = new Samsung;
        // set root account data with a random password
        $root->fill([
            'name'      => 'Gerardo',
            'email'     => 'gerardo@samsung.com',
            'password'  => Hash::make($password),
            'type'      => 'samsung',
        ]);
        $root->save();
    }
}
