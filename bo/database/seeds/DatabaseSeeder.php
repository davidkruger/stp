<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(AdminsSeeder::class);
        $this->call(FilesSeeder::class);
        $this->call(DivisionsSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(ProbabilidadCierreSeeder::class);
        $this->call(VerticalSeeder::class);
        $this->call(AccionSeeder::class);
        $this->call(MayoristaSeeder::class);
    }
}
