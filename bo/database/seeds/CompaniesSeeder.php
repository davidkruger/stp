<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // create 5 example files
/*        $retry = false;
        do {
            $types = [];
            factory(Company::class, 10)->create()->each(function($company) {
                //
                if (!isset($types[$company->type])) $types[$company->type] = 0;
                $types[$company->type]++;
            });
            foreach ($types as $type => $qty)
                if ($qty == 0) $retry = true;
        } while ($retry);*/
        $item = new Company;
        $item->ruc = "2221119-2";
        $item->name = "Diviser";
        $item->real_name = "Diviser S.A.";
        $item->phone = "0951 222 333";
        $item->email = "admin@diviser.com";
        $item->address = "Braslia casi españa nro. 558";
        $item->logo_id = 4;
        $item->constitution = "2020-01-01";
        $item->type = "reseller";
        $item->reseller = "authorized";
        $item->save();

        $item = new Company;
        $item->ruc = "2221119-1";
        $item->name = "Bonus";
        $item->real_name = "Bonus SRL";
        $item->phone = "0951 222 333";
        $item->email = "admin@bonus.com";
        $item->address = "Braslia casi españa nro. 559";
        $item->logo_id = 4;
        $item->constitution = "2020-01-01";
        $item->type = "mayorista";
        $item->save();
    }
}
