<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_opportunity_item');
            $table->integer('quantity');
            $table->integer('price_solicitado');
            $table->unsignedBigInteger('id_ofertante')->nullable();
            $table->enum('state', [ 'pendiente', 'aprobado' ])->default('pendiente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_ofertas');
    }
}
