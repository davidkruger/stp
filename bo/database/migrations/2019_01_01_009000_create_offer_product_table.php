<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateOfferProductTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('offer_product', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('opportunity_product_id');
                $table->foreign('opportunity_product_id')->references('id')->on('opportunity_product');
//            $table->primary([ 'offer_id', 'product_id' ]);
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('offer_product');
    }
}