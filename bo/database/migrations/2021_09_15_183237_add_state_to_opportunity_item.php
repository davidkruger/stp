<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToOpportunityItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opportunity_item', function (Blueprint $table) {
            $table->enum('state', [ 'pendiente', 'aprobado' ])->default('pendiente');
            $table->unsignedBigInteger('id_ofertante')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opportunity_item', function (Blueprint $table) {
            //
        });
    }
}
