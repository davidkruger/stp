<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateProductsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('products', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mayorista_id')->nullable()->comment('Mayorista al cual pertenece el Producto');
                $table->foreign('mayorista_id')->references('id')->on('companies');
            $table->string('name')->comment('Nombre producto');
            $table->string('description')->comment('Descripcion producto');
            $table->unsignedBigInteger('division_id')->comment('Tipo de Producto-Categoria');
                $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedBigInteger('subcategoria_id')->comment('Sub Categoria');
                $table->foreign('subcategoria_id')->references('id')->on('sub_categorias');
            $table->unsignedBigInteger('image_id')->comment('Imégen del Producto');
                $table->foreign('image_id')->references('id')->on('files');
            $table->unsignedBigInteger('price_reference')->nullable()->comment('Precio Samsung de referencia');
            $table->unsignedBigInteger('price')->nullable()->comment('Precio lista del Producto');
            $table->unsignedBigInteger('points');
        //    $table->integer('stock_cantidad')->nullable();
        //    $table->boolean('stock')->nullable();
        //    $table->string('sku', 120)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }
}