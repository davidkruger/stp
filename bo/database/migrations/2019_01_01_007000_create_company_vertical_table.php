<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateCompanyVerticalTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('company_vertical', function(SamsungBlueprint $table) {
            $table->unsignedBigInteger('company_id');
                $table->foreign('company_id')->references('id')->on('companies');
            $table->unsignedBigInteger('vertical_id');
                $table->foreign('vertical_id')->references('id')->on('verticals');
            $table->primary([ 'company_id', 'vertical_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('company_vertical');
    }
}