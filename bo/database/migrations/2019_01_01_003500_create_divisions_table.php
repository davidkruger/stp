<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateDivisionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('divisions', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('divisions');
    }
}