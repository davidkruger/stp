<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateSubCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('sub_categorias', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('division_id')->comment('Tipo de Producto-Categoria');
                $table->foreign('division_id')->references('id')->on('divisions');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categorias');
    }
}
