<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateOffersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('offers', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('opportunity_id')->comment('Oportunidad a la cual pertenece la Oferta');
                $table->foreign('opportunity_id')->references('id')->on('opportunities');
            $table->unsignedBigInteger('mayorista_id')->comment('Mayorista que realiza la Oferta');
                $table->foreign('mayorista_id')->references('id')->on('companies');
            $table->enum('status', [ 'new', 'accepted', 'rejected' ])->default('new')->comment('Estado de la oferta');
            $table->string('reason')->nullable()->comment('Motivo del rechazo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('offers');
    }
}