<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPuntosToPuntajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('puntajes', function (Blueprint $table) {
            $table->unsignedInteger('puntos_productos')->nullable();
            $table->unsignedInteger('puntos_estatus')->nullable();
            $table->unsignedInteger('puntos_crear')->nullable();
            $table->unsignedInteger('puntos_superar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('puntajes', function (Blueprint $table) {
            //
        });
    }
}
