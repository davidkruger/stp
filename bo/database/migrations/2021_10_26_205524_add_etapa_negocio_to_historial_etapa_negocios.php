<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEtapaNegocioToHistorialEtapaNegocios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historial_etapa_negocios', function (Blueprint $table) {
            $table->unsignedBigInteger('etapa_id');
            $table->foreign('etapa_id')->references('id')->on('probabilidad_cierres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historial_etapa_negocios', function (Blueprint $table) {
            //
        });
    }
}
