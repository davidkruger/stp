<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateOpportunityProductTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('opportunity_product', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('opportunity_id');
            $table->unsignedBigInteger('product_id');
                $table->foreign('product_id')->references('id')->on('products');
           // $table->primary([ 'opportunity_id', 'product_id' ]);

            $table->Integer('quantity')->comment('Cantidad del Producto requerido por el Reseller');
            $table->unsignedInteger('price')->nullable()->comment('Precio propuesto por el Reseller');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('opportunity_product');
    }
}