<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateOpportunitiesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('opportunities', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Nombre descriptivo de la Oportunidad');
            $table->unsignedBigInteger('products_list_id')->unique()->comment('el id que van a tener los opportunity_products asociados a esta oportunidad');
            $table->unsignedBigInteger('reseller_id')->nullable()->comment('Reseller asignado a la Oportunidad, ej GG');
                $table->foreign('reseller_id')->references('id')->on('users');
            $table->unsignedBigInteger('mayorista_id')->nullable()->comment('Mayorista que realiza ofrece las cotizaciones, ej Bonus');
                $table->foreign('mayorista_id')->references('id')->on('users');
            $table->date('tentative')->comment('Fecha tentativa de cierre');
            $table->date('closes')->nullable()->comment('Fecha de cierre real');
            $table->unsignedTinyInteger('status')
                ->default(0)->comment('Status de la Oportunidad (en porcentaje)');
            $table->enum('state', [ 'lead', 'open', 'won', 'lost' ])
                ->default('lead')->comment('Estado de la Oportunidad en texto');
            $table->string('reason')->nullable()->comment('Detalle de ganado/perdido');
            $table->unsignedBigInteger('createdby')->nullable()->comment('Compañía desde la cual se creo la Oportunidad');
                $table->foreign('createdby')->references('id')->on('users');
        //    $table->integer('puntos')->nullable();
        //    $table->integer('puntos_extra')->nullable();
        //    $table->integer('monto_total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('opportunities');
    }
}