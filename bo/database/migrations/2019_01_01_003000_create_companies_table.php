<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreateCompaniesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // get schema builder
        $schema = DB::getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            // return SamsungBlueprint
            return new SamsungBlueprint($table, $callback);
        });

        // create table
        $schema->create('companies', function(SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->string('ruc')->unique();
            $table->string('name')->comment('Nombre ficticio');
            $table->string('real_name')->comment('Nombre en la SET');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->integer('branches')->nullable()->comment('Cantidad de sucursales');
            $table->integer('avg_branches')->nullable()->comment('Promedio de pantallas por Sucursal');
            $table->unsignedBigInteger('logo_id')->nullable();
                $table->foreign('logo_id')->references('id')->on('files');
            $table->date('constitution')->comment('Fecha de Constitución');
            $table->enum('type', [ 'mayorista', 'reseller', 'company' ]);
            $table->enum('reseller', [ 'authorized', 'preferred', 'priority', 'not_qualify' ])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('companies');
    }
}
