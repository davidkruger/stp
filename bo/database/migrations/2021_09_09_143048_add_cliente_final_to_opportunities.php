<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClienteFinalToOpportunities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opportunities', function (Blueprint $table) {
            $table->string('nombre_cliente', 120)->nullable();
            $table->string('razon_social_cliente', 120)->nullable();
            $table->string('ruc_cliente', 20)->nullable();
            $table->string('telefono_cliente', 15)->nullable();
            $table->unsignedBigInteger('vertical_id', )->nullable();
                $table->foreign('vertical_id')->references('id')->on('verticals');
            $table->longText('descripcion')->nullable();
            $table->unsignedBigInteger('probabilidad_cierre_id', )->nullable();
                $table->foreign('probabilidad_cierre_id')->references('id')->on('probabilidad_cierres');
            $table->enum('tipo_oportunidad', [ 'privado', 'publico' ])->nullable();
            $table->enum('tipo_publico', [ 'pre_pliego', 'llamado_calle' ])->nullable();
            $table->string('llamado_calle_idllamado', 50)->nullable();
            $table->string('pre-pliego_fecha', 50)->nullable();
            $table->integer('presupuesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opportunities', function (Blueprint $table) {
            //
        });
    }
}
