<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

use App\Blueprints\SamsungBlueprint;

class CreatePuntajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            // get schema builder
            $schema = DB::getSchemaBuilder();

            // replace blueprint
            $schema->blueprintResolver(function($table, $callback) {
                // return SamsungBlueprint
                return new SamsungBlueprint($table, $callback);
            });
        
            // create table
            $schema->create('puntajes', function (SamsungBlueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
            $table->integer('puntos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntajes');
    }
}
