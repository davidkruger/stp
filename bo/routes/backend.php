<?php

/*
|--------------------------------------------------------------------------
| Private
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace'     => 'Backend',
    'prefix'        => 'backend',
    'middleware'    => [ 'web' ],
], function() {
    // backend login endpoints
    Route::get('login', 'Auth\LoginController@showLoginForm');
    Route::post('login', 'Auth\LoginController@login')->name('backend.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('backend.logout');
});

Route::group([
    'namespace'     => 'Backend',
    'prefix'        => 'backend',
    'middleware'    => [ 'web', 'auth:admin' ],
], function() {
    // name prefix
    $name_prefix = [ 'as' => 'admin' ];

    //
    Route::get('/',                     'DashboardController@index')->name('backend');
    Route::get('/dashboard',            'DashboardController@dashboard')->name('admin.dashboard');
    Route::get('/dashboard/verticals',  'DashboardController@verticals')->name('admin.dashboard.verticals');

    // users
    Route::resource('admins',   'AdminsController', $name_prefix)->name('index', 'admin.admins')->parameters([ 'admins' => 'samsung' ]);
    Route::resource('users',    'UsersController', $name_prefix)->name('index', 'admin.users');
    Route::get('solicitudes',    'UsersController@solicitudes')->name('admin.solicitudes');
    Route::post('solicitudes/{id}',    'UsersController@solicitud')->name('admin.solicitud');
    Route::get('rechazados',    'UsersController@rechazados')->name('admin.rechazados');
    
    //
    Route::resource('divisions',        'DivisionsController', $name_prefix)->name('index', 'admin.divisions');
    Route::resource('subcategorias',        'SubCategoriaController', $name_prefix)->name('index', 'admin.subcategorias');
    Route::resource('images',           'ImagesController', $name_prefix)->name('index', 'admin.images')->parameters([ 'files' => 'image' ]);
    Route::resource('products',         'ProductsController', $name_prefix)->name('index', 'admin.products');
    Route::post('stock/{id}', 'ProductsController@stock')->name('admin.stock');
    Route::resource('companies',        'CompaniesController', $name_prefix)->name('index', 'admin.companies');
    Route::resource('verticals',        'VerticalsController', $name_prefix)->name('index', 'admin.verticals');
    Route::resource('opportunities',    'OpportunitiesController', $name_prefix)->name('index', 'admin.opportunities');
    Route::resource('offers',           'OffersController', $name_prefix)->name('index', 'admin.offers');

    // private storage
    Route::get('storage/{file}',        'StorageController@get')->name('storage.get');
    //acciones que otorgan puntos
    Route::get('acciones', 'AccionController@index')->name('admin.acciones');
    Route::post('acciones/update/{id}', 'AccionController@update')->name('admin.acciones.update');

    Route::get('proteccion', 'ProtectController@oportunidades_pendientes')->name('admin.proteccion');
    Route::get('leads', 'OpportunitiesController@leads')->name('admin.leads');
    Route::get('leads/{id}', 'OpportunitiesController@lead')->name('admin.lead');
    Route::post('lead/{id}', 'OpportunitiesController@otorgar_lead')->name('admin.otorgar_lead');
    Route::get('oportunidad/{id}', 'ProtectController@oportunidad')->name('admin.proteccionShow');
    Route::post('proteccion/oportunidad', 'ProtectController@proteccion')->name('admin.proteccion_state');
    Route::get('opp_protegidas', 'ProtectController@opp_protegidas')->name('admin.opp_protegidas');
    Route::get('opp_rechazadas', 'ProtectController@opp_rechazadas')->name('admin.opp_rechazadas');

    //ranks
    Route::get('rank_puntos', 'RankingController@rank_puntos')->name('admin.rank_puntos');
    Route::get('rank_puntos/periodo', 'RankingController@rank_puntos_periodo')->name('admin.rank_puntos_periodo');

    Route::get('rank_oportunidades', 'RankingController@rank_opp')->name('admin.rank_oportunidades');
    Route::get('rank_oportunidades/periodo', 'RankingController@rank_oportunidades_periodo')->name('admin.rank_oportunidades_periodo');
    
    Route::get('rank_monto', 'RankingController@rank_monto')->name('admin.rank_monto');
    Route::get('rank_monto/periodo', 'RankingController@rank_monto_periodo')->name('admin.rank_monto_periodo');

    Route::post('oportunidad/precio/{id}', 'OpportunitiesController@precio_asignado')->name('admin.oportunidad_precio');
  
    Route::post('/addProductOpp',  'OpportunitiesController@addProductOpp');
    Route::post('/deleteOppProduct', 'OpportunitiesController@deleteOppProduct');
    Route::delete('/opportunities/{id}', 'OpportunitiesController@destroy')->name('opportunities.destroy');

});