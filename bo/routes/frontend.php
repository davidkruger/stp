<?php

/*
|--------------------------------------------------------------------------
| Public with authentication needed
|--------------------------------------------------------------------------
*/
Route::group([
    'namespace'     => 'Frontend',
], function() {
    Route::get('/register_client', 'UserController@create');
    Route::post('/register_client', 'UserController@store')->name('register_client');
});
Route::group([
    'namespace'     => 'Frontend',
    'middleware'    => [ 'auth' ],
], function() {
    // dashboard
    Route::get('/dashboard',            'DashboardController@index')->name('dashboard');

    Route::resource('opportunities',            'OpportunitiesController', [
        'only'  => [ 'show', 'create', 'edit', 'update', 'store' ],
        'names' => [ 'index' => 'opportunities.show' ]
    ]);
    Route::resource('opportunities.offers',     'OffersController', [
        'only'  => [ 'show', 'create', 'store', 'update', 'edit', 'destroy' ],
        'names' => [ 'index' => 'opportunities.offers' ]
    ]);
    Route::put('opportunities/{opportunity}/offers/{offer}/accept', 'OffersController@accept')->name('opportunities.offers.accept');
    Route::put('opportunities/{opportunity}/offers/{offer}/reject', 'OffersController@reject')->name('opportunities.offers.reject');
    Route::resource('products',                 'PricesController', [
        'only'  => [ 'index', 'update' ],
        'names' => [ 'index' => 'products' ]
    ]);

    //json
    Route::get('/listProducts',            'OpportunitiesController@listProducts');
    Route::get('/listOppProducts',            'OpportunitiesController@listOppProducts');
    Route::post('/addProductOpp',  'OpportunitiesController@addProductOpp');
    Route::post('/deleteOppProduct', 'OpportunitiesController@deleteOppProduct');

    // private storage
    Route::get('storage/{file}',        'StorageController@get')->name('frontstorage.get');
    Route::post('opportunities/precio_solicitado/{id}', 'OpportunitiesController@precio_solicitado');
    Route::post('opportunities/aprobado/{id}', 'OpportunitiesController@aprobado');
    Route::post('opportunities/cerrar/{id}', 'OpportunitiesController@cerrar');
    Route::post('opportunities/perder/{id}', 'OpportunitiesController@perder');
    Route::post('opportunities/estado', 'DashboardController@actualizar');
    Route::post('opportunities/mail', 'OpportunitiesController@mail');
    Route::post('opportunities/obtener/{id}', 'OpportunitiesController@obtener');
});