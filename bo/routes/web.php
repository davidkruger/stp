<?php

/*
|--------------------------------------------------------------------------
| Public
|--------------------------------------------------------------------------
*/
Route::group([
    'namespace'     => 'Frontend'
], function() {
    //
 //   Route::get('/',         'SiteController@index');
    Route::get('/',         function(){
        return redirect('/login');
    });
    Route::get('/home',     'SiteController@index');
    // Route::get('/contact',  'SiteController@contact');
    // web login endpoints
    Auth::routes();
});
