<?php

namespace App\Models;

use App\Models\Company;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vertical extends Model {
    use SoftDeletes;
    use HasValidationRules;

    protected $fillable = [
        'code',
        'description',
        'icon_id',
    ];

    protected static $createRules = [
        'code'          => [ 'required' ],
        'icon'          => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
    ];

    protected static $updateRules = [
        'code'          => [ 'required' ],
        'icon'          => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
    ];

    public function icon() {
        return $this->belongsTo(File::class);
    }

    public function companies() {
        return  $this->belongsToMany(Company::class);
    }
}