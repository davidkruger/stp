<?php

namespace App\Models;

use App\Traits\HasValidationRules;
use App\Models\Opportunity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Puntaje extends Model
{
    use SoftDeletes;
    use HasValidationRules;

    public function opportunity() {
        // Compañía de la Oportunidad, cliente final, ej: Itau
        return $this->belongsTo(Opportunity::class);
    }
}
