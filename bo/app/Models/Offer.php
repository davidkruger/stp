<?php

namespace App\Models;

use App\Models\Company;
use App\Models\Opportunity;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Offer extends Model {
    use SoftDeletes;
    use HasValidationRules;

    public $fillable = [
        'opportunity_id',
        'mayorista_id',
        'status',
        'reason',
    ];

    protected static $createRules = [
        'opportunity_id'    => [ 'required' ],
        'mayorista_id'      => [ 'required' ],
    ];

    protected static $updateRules = [
        'status'    => [ 'required' ],
        'reason'    => [ 'sometimes', 'nullable' ],
    ];

    public function opportunity() {
        // Oportunidad a la cual pertenece la Oferta
        return $this->belongsTo(Opportunity::class);
    }

    public function mayorista() {
        // Mayorista que realiza la Oferta
        return $this->belongsTo(Company::class);
    }

    public function products() {
        // Productos de la Oferta
        return $this->belongsToMany(Product::class)
            // precio ofrecido desde pivot
            ->withPivot([ 'price' ])
            ->withTimestamps();
    }

    public function scopeFromMayorista($query, $mayorista_id) {
        return $query->where('mayorista_id', $mayorista_id);
    }
}