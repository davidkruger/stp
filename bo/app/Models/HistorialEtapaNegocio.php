<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProbabilidadCierre;
use App\Models\User;
use App\Models\Opportunity;

class HistorialEtapaNegocio extends Model
{
    public function etapa_negocio() {
        return $this->belongsTo(ProbabilidadCierre::class, 'etapa_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function opportunity() {
        return $this->belongsTo(Opportunity::class);
    }
}
