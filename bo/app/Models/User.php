<?php

namespace App\Models;

use App\Models\Base\User as BaseUser;
use App\Models\Scopes\UserScope;

class User extends BaseUser {
    public static function boot() {
        parent::boot();
        static::addGlobalScope(new UserScope);
    }
}