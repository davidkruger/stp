<?php

namespace App\Models;

use App\Models\Offer;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class OfferProduct extends Pivot {
    use SoftDeletes;
    use HasValidationRules;

    public $pivotParent = Offer::class;

    public $fillable = [
        'offer_id',
        'product_id',
        'price',
    ];

    protected static $createRules = [
        'offer_id'      => [ 'required', 'numeric' ],
        'product_id'    => [ 'required', 'numeric' ],
        'price'         => [ 'required', 'numeric', 'min:0' ],
    ];

    protected static $updateRules = [
        'price' => [ 'required', 'numeric', 'min:0' ],
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}