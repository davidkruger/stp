<?php

namespace App\Models\Base;

use App\Models\Company;
use App\Models\Opportunity;
use App\Models\Puntaje;
use App\Traits\HasValidationRules;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Validator;

abstract class User extends Authenticatable {
    use Notifiable;
    use SoftDeletes;
    use HasValidationRules;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
        'company',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $passwordRules = [
        'password'  => [ 'required', 'confirmed', 'min:6' ],
    ];

    protected static $createRules = [
        'name'      => [ 'required', 'min:2' ],
        'email'     => [ 'required', 'email', 'unique:users' ],
        'password'  => [ 'required', 'min:6' ],
        'type'      => [ 'required' ],
        'company_id'=> [ 'sometimes', 'nullable' ],
    ];

    protected static $updateRules = [
        'name'      => [ 'required', 'min:2' ],
        'email'     => [ 'required', 'email', 'unique:users,email,{id}' ],
        'password'  => [ 'sometimes', 'nullable', 'min:6' ],
        'type'      => [ 'required' ],
        'company_id'=> [ 'sometimes', 'nullable' ],
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function opportunities() {
        return $this->hasMany(Opportunity::class, 'reseller_id');
    }

    public function puntajes() {
        return $this->hasMany(Puntaje::class);
    }

   /* protected function beforeSave(Validator $validator) {
        // validate company for type=reseller|mayorista
        if (in_array($this->type, [ 'reseller', 'mayorista' ]) && $this->company_id == null)
            // add field validation error
            $validator->errors()->add('company_id', 'Company must be specified');
    }*/
}