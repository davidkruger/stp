<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistorialOferta extends Model
{
    public function ofertante() {
        return $this->belongsTo(User::class, 'id_ofertante');
    }
}
