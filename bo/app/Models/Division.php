<?php

namespace App\Models;

use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model {
    use SoftDeletes;
    use HasValidationRules;

    protected $fillable = [ 'name' ];

    protected static $createRules = [
        'name'  => [ 'required', 'min:2' ],
    ];

    protected static $updateRules = [
        'name'  => [ 'required', 'min:2' ],
    ];
}