<?php

namespace App\Models;

use App\Models\Base\User as BaseUser;
use App\Models\Scopes\SamsungScope;

class Samsung extends BaseUser {
    protected $guard = 'admin';

    public static function boot() {
        parent::boot();
        static::addGlobalScope(new SamsungScope);
    }
}