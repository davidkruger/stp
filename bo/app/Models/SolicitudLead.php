<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SolicitudLead extends Model
{
    public function reseller() {
        return $this->belongsTo(User::class, 'reseller_id');
    }
}
