<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class UserScope implements Scope {
    public function apply(Builder $builder, Model $model) {
        // filter Mayorista|Reseller
        $builder->whereIn('type', [ 'mayorista', 'reseller','reseller_gerente' ]);
    }
}