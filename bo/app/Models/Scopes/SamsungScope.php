<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SamsungScope implements Scope {
    public function apply(Builder $builder, Model $model) {
        // filter Samsung
        $builder->whereIn('type', [ 'samsung' ]);
    }
}