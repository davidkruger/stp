<?php

namespace App\Models;

use App\Models\Offer;
use App\Models\Opportunity;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Validator;

class Company extends Model {
    use SoftDeletes;
    use HasValidationRules;

    protected $fillable = [
        'ruc',
        'name',
        'real_name',
        'phone',
        'email',
        'address',
        'branches',
        'avg_branches',
        'logo_id',
        'constitution',
        'type',
        'reseller',
    ];

    protected static $createRules = [
        'ruc'       => [ 'required', 'min:5' ],
        'name'      => [ 'required', 'min:2' ],
        'real_name' => [ 'required', 'min:2' ],
        'phone'     => [ 'nullable' ],
        'email'     => [ 'nullable', 'email' ],
        'address'   => [ 'nullable', 'min:10' ],
        'branches'  => [ 'sometimes', 'nullable', 'numeric' ],
        'avg_branches'  => [ 'sometimes', 'nullable', 'numeric' ],
        'logo_id'   => [ 'sometimes', 'nullable' ],
        'logo'      => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
        'constitution'  => [ 'required', 'date', 'before:now' ],
        'type'      => [ 'required' ],
        'reseller'  => [ 'sometimes', 'nullable' ],
    ];

    protected static $updateRules = [
        'ruc'       => [ 'required', 'min:5' ],
        'name'      => [ 'required', 'min:2' ],
        'real_name' => [ 'required', 'min:2' ],
        'phone'     => [ 'sometimes', 'nullable' ],
        'email'     => [ 'sometimes', 'nullable', 'email' ],
        'address'   => [ 'sometimes', 'nullable', 'min:10' ],
        'branches'  => [ 'required', 'numeric' ],
        'avg_branches'  => [ 'required', 'numeric' ],
        'logo_id'   => [ 'sometimes', 'nullable' ],
        'logo'      => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
        'constitution'  => [ 'required', 'date', 'before:now' ],
        'type'      => [ 'required' ],
        'reseller'  => [ 'sometimes', 'nullable' ],
    ];

    protected function beforeSave(Validator $validator) {
        // validate reseller for type=reseller
        if ($this->type == 'reseller' && $this->reseller == null)
            // add field validation error
            $validator->errors()->add('reseller', 'Reseller type must be specified');
    }

    public function logo() {
        return $this->belongsTo(File::class);
    }

    public function reseller(){
        return $this->hasMany(User::class);
    }
    public function verticals() {
        return $this->belongsToMany(Vertical::class);
    }

    public function opportunities() {
        return $this->hasMany(Opportunity::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }

    public function offers() {
        return $this->hasMany(Offer::class);
    }

    public function scopeCompanies($query) {
        return $query->where('type', 'company');
    }

    public function scopeMayoristas($query) {
        return $query->where('type', 'mayorista');
    }

    public function scopeResellers($query) {
        return $query->where('type', 'reseller');
    }

    public function scopeTypeIn($query, $types) {
        return $query->whereIn('type', $types);
    }
}