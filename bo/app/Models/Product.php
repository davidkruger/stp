<?php

namespace App\Models;

use App\Models\OfferProduct;
use App\Models\Company;
use App\Models\Division;
use App\Models\File;
use App\Models\SubCategoria;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {
    use SoftDeletes;
    use HasValidationRules;

    public $fillable = [
        'name',
        'description',
        'division_id',
        'subcategoria_id',
        'image_id',
        'price',
        'points',
        'stock_cantidad',
        'stock',
        'sku'
    ];

    protected static $createRules = [
        'name'          => [ 'required', 'min:5' ],
        'description'   => [ 'required' ],
        'subcategoria_id'   => [ 'required' ],
        'image_id'      => [ 'required' ],
        'image'         => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
        'price'         => [ 'sometimes', 'nullable', 'numeric', 'min:1' ],
        'points'        => [ 'required', 'numeric', 'min:1' ],
        'stock_cantidad'  => [ 'required' ],
        'stock'  => [ 'required' ],
        'sku'  => [ 'required' ],
    ];

    protected static $updateRules = [
        'name'          => [ 'required', 'min:5' ],
        'description'   => [ 'required' ],
        'subcategoria_id'   => [ 'required' ],
        'image_id'      => [ 'required' ],
        'image'         => [ 'sometimes', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048' ],
        'price'         => [ 'sometimes', 'nullable', 'numeric', 'min:1' ],
        'points'        => [ 'required', 'numeric', 'min:1' ],
        'stock_cantidad'  => [ 'required' ],
        'stock'  => [ 'required' ],
        'sku'  => [ 'required' ],
    ];

    public function mayorista() {
        return $this->belongsTo(Company::class);
    }

    public function subcategoria() {
        return $this->belongsTo(SubCategoria::class);
    }

    public function division() {
        return $this->belongsTo(Division::class);
    }

    public function image() {
        return $this->belongsTo(File::class);
    }

    public function scopeFromMayorista($query, $mayorista) {
        return $query->where('mayorista_id', $mayorista);
    }
}