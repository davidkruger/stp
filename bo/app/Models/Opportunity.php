<?php

namespace App\Models;

use App\Models\OpportunityProduct;
use App\Models\OpportunityItem;
use App\Traits\HasValidationRules;
use App\Traits\HasCreatedUpdatedBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Opportunity extends Model {
    use SoftDeletes;
    use HasValidationRules;
    // use HasCreatedUpdatedBy;

    public $fillable = [
        'name',
        'reseller_id',
        'mayorista_id',
        'tentative',
        'closes',
       'status',
        'state',
        'reason',
        'createdby',
        'entrega',
        'fentrega',
    ];

    protected static $createRules = [
        'name'          => [ 'required', 'min:2' ],
        'reseller_id'   => [ 'sometimes', 'nullable' ],
        'mayorista_id'  => [ 'sometimes', 'nullable' ],
        'tentative'     => [ 'required', 'date' ],
        'closes'        => [ 'sometimes', 'nullable', 'date'],
        'status'        => [ 'sometimes', 'nullable', 'numeric', 'min:0', 'max:100' ],
        'state'         => [ 'sometimes' ],
        'createdby'     => [ 'sometimes', 'nullable' ],
        'entrega'     => [ 'sometimes', 'nullable' ],
        'fentrega'     => [ 'required', 'date' ],
    ];

    protected static $updateRules = [
        'name'          => [ 'required', 'min:2' ],
        'reseller_id'   => [ 'sometimes', 'nullable' ],
        'mayorista_id'  => [ 'sometimes', 'nullable' ],
        'tentative'     => [ 'required', 'date' ],
        'closes'        => [ 'sometimes', 'nullable', 'date'],
        'status'        => [ 'sometimes', 'nullable', 'numeric', 'min:0', 'max:100' ],
        'state'         => [ 'sometimes' ],
        'reason'        => [ 'sometimes', 'nullable' ],
        'entrega'     => [ 'sometimes', 'nullable' ],
        'fentrega'     => [ 'required', 'date' ],
    ];

    public function company() {
        // Compañía de la Oportunidad, cliente final, ej: Itau
        return $this->belongsTo(Company::class);
    }

    public function vertical() {
        // Compañía de la Oportunidad, cliente final, ej: Itau
        return $this->belongsTo(Vertical::class);
    }

    public function reseller() {
        return $this->belongsTo(User::class, 'reseller_id');
    }

    public function probabilidad_cierre() {
        return $this->belongsTo(ProbabilidadCierre::class);
    }

  //  public function reseller() {
        // Reseller asignado a la Oportunidad, ej GG
    //    return $this->belongsTo(Company::class);
   // }

    public function mayorista() {
        // Mayorista seleccionado por el Reseller, ej Bonus
        return $this->belongsTo(Company::class);
    }

    public function products() {
        // Productos asignados a la Oportunidad
        return $this->belongsToMany(Product::class)
            // Cantidad y Precio desde el pivot
            ->withPivot([ 'quantity', 'price' ])
            ->withTimestamps();
    }

    public function items() {
        return $this->hasMany(OpportunityItem::class, 'opp_id');
    }

    public function offers() {
        // Ofertas (cotizaciones) realizadas por el Mayorista
        return $this->hasMany(Offer::class);
    }

    public function scopeFromCompany($query, $company) {
        // filtramos Oportunidades donde la Compañia esta asignada
        return $query->where('company_id', $company);
    }

    public function scopeFromReseller($query, $reseller) {
        // filtramos Oportunidades donde el reseller esta asignado
        return $query->where('reseller_id', $reseller);
    }

    public function scopeFromMayorista($query, $mayorista) {
        // filtramos Oportunidades donde el Mayorista esta asignado
        return $query->where('mayorista_id', $mayorista);
    }

    public function scopeIsLead($query) {
        return $this->hasState('lead');
        return $query->where('state', 'lead');
    }

    public function scopeHasState($query, $state) {
        return $query->where('state', $state);
    }

    public function scopeOportDescending($query){
        return $query->orderBy('id','DESC');
    }   
    
}