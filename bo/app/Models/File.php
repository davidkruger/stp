<?php

namespace App\Models;

use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model {
    use SoftDeletes;
    use HasValidationRules;

    const STORAGE_DIRECTORY = 'images';

    protected $fillable = [ 'name', 'type', 'url' ];

    public static $uploadRules = [
        'file'  => [ 'required', 'image', 'mimes:jpeg,jpg,png,gix', 'max:2048' ],
    ];

    protected static $createRules = [
        'name'  => [ 'required', 'min:3' ],
        'type'  => [ 'required' ],
        'url'   => [ 'required', 'min:3' ],
    ];

    protected static $updateRules = [
        'name'  => [ 'required', 'min:3' ],
        'type'  => [ 'required' ],
        'url'   => [ 'required', 'min:3' ],
    ];

    public function scopeImages($query) {
        return $query->where('type', '=', 'image');
    }

    public static function upload($request, $file, $controller) {
        // get uploaded file
        $file = $request->file($file);
        // validate
        if (!$file->isValid()) {
            // append errors
            $controller->validator->errors()->add($file, $file->getErrorMessage());
            // exit with validation exception
            $controller->throwValidationException($request, $controller->validator);
        }
        // save file
        $path = $file->store(self::STORAGE_DIRECTORY);
        // create resource
        return new File([
            'name'  => $file->getClientOriginalName(),
            'type'  => 'image',
            'url'   => $path
        ]);
    }
}