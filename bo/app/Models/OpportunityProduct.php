<?php

namespace App\Models;

use App\Models\Opportunity;
use App\Models\Product;
use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpportunityProduct extends Pivot {
    use SoftDeletes;
    use HasValidationRules;

    public $pivotParent = Opportunity::class;

    protected $foreignKey = 'opportunity_id';
    protected $relatedKey = 'product_id';

    public $fillable = [
        'quantity',
        'price',
    ];

    protected static $createRules = [
        'quantity'  => [ 'required', 'numeric', 'min:0' ],
        'price'     => [ 'sometimes', 'nullable', 'numeric', 'min:0' ],
    ];

    protected static $updateRules = [
        'quantity'  => [ 'required', 'numeric', 'min:0' ],
        'price'     => [ 'sometimes', 'nullable', 'numeric', 'min:0' ],
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
    
    public function offerProduct() {
        return $this->hasMany(OfferProduct::class);
    }
}