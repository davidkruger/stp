<?php

namespace App\Models;

use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accion extends Model
{
    use SoftDeletes;
    use HasValidationRules;
}
