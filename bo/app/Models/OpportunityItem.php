<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpportunityItem extends Model
{
    protected $table = 'opportunity_item';
    public function product() {
        return $this->belongsTo(Product::class);
    }
    
    public function offerProduct() {
        return $this->hasMany(OfferProduct::class);
    }

    public function historiales() {
        return $this->hasMany(HistorialOferta::class, 'id_opportunity_item');
    }
}
