<?php

namespace App\Models;

use App\Traits\HasValidationRules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoria extends Model
{
    use SoftDeletes;
    use HasValidationRules;

    protected $fillable = [ 'name', 'division_id' ];

    protected static $createRules = [
        'name'  => [ 'required', 'min:2' ],
        'division_id' => [ 'required' ],
    ];

    protected static $updateRules = [
        'name'  => [ 'required', 'min:2' ],
        'division_id' => [ 'required' ],
    ];

    public function division(){
        return $this->belongsTo('App\Models\Division', 'division_id');
    }
}
