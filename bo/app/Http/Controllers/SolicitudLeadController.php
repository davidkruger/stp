<?php

namespace App\Http\Controllers;

use App\SolicitudLead;
use Illuminate\Http\Request;

class SolicitudLeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SolicitudLead  $solicitudLead
     * @return \Illuminate\Http\Response
     */
    public function show(SolicitudLead $solicitudLead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SolicitudLead  $solicitudLead
     * @return \Illuminate\Http\Response
     */
    public function edit(SolicitudLead $solicitudLead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SolicitudLead  $solicitudLead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SolicitudLead $solicitudLead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SolicitudLead  $solicitudLead
     * @return \Illuminate\Http\Response
     */
    public function destroy(SolicitudLead $solicitudLead)
    {
        //
    }
}
