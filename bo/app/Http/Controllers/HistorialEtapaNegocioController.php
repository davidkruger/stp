<?php

namespace App\Http\Controllers;

use App\HistorialEtapaNegocio;
use Illuminate\Http\Request;

class HistorialEtapaNegocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HistorialEtapaNegocio  $historialEtapaNegocio
     * @return \Illuminate\Http\Response
     */
    public function show(HistorialEtapaNegocio $historialEtapaNegocio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HistorialEtapaNegocio  $historialEtapaNegocio
     * @return \Illuminate\Http\Response
     */
    public function edit(HistorialEtapaNegocio $historialEtapaNegocio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HistorialEtapaNegocio  $historialEtapaNegocio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HistorialEtapaNegocio $historialEtapaNegocio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HistorialEtapaNegocio  $historialEtapaNegocio
     * @return \Illuminate\Http\Response
     */
    public function destroy(HistorialEtapaNegocio $historialEtapaNegocio)
    {
        //
    }
}
