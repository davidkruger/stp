<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\ProbabilidadCierre;
use App\Models\OpportunityItem;
use App\Models\Product;
use App\Models\Opportunity;
use App\Models\HistorialEtapaNegocio;
use App\Models\Accion;
use App\Mail\OppNoProtegida;
use App\Mail\OppProtegida;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class ProtectController extends BackendController
{
    public function oportunidades_pendientes(){
        $opportunities = Opportunity::where('protegido', 'pendiente')->get();

        return view('opportunities.proteccion.pendientes', compact('opportunities'));
    }
    public function opp_protegidas(){
        $opportunities = Opportunity::where('protegido', 'si')->get();

        return view('opportunities.index', compact('opportunities'));
    }
    public function opp_rechazadas(){
        $opportunities = Opportunity::where('protegido', 'no')->get();

        return view('opportunities.index', compact('opportunities'));
    }

    public function oportunidad($id){
        $opportunity = Opportunity::find($id);
        $puntos = $this->puntos($opportunity->id);
        $puntos_prod = $puntos['puntos_prod'];
        $puntos_estatus = $puntos['puntos_estatus'];
        $monto_prod_lista = $puntos['monto_prod_lista'];
        $monto_prod_parcial = $puntos['monto_prod_parcial'];
        $cerrar = 0;
        $probabilidades = ProbabilidadCierre::all();
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
   
        if ($opportunity->probabilidad_cierre_id != 8 && $opportunity->probabilidad_cierre_id != 7) {
           $cerrar = 1;
        }
        $etapas = HistorialEtapaNegocio::where('opportunity_id',$id)->get();
        $etapas = $etapas->sortByDesc('id');


           $puntos_crear = $puntos['puntos_crear'];
           $monto_superar = $puntos['monto_superar'];
           $puntos_superar = $puntos['puntos_superar'];

           return view('opportunities.proteccion.show', compact('probabilidades','opportunity', 'products', 'cerrar','puntos_estatus',
                        'monto_prod_parcial','puntos_prod','monto_prod_lista','puntos_crear','monto_superar','puntos_superar','etapas'));
    }

    public function proteccion(Request $request){
        $oportunidad = Opportunity::find($request->opp_id);
        $oportunidad->protegido = $request->protegido;
        $oportunidad->save();
        $correomt = "giovanni@capitanlot.com";
        if ($request->protegido == 'si') {
            
            //dd($oportunidad->reseller);
            Mail::to($correomt)->send(new OppProtegida($oportunidad));
        }elseif ($request->protegido == 'no') {
            Mail::to($correomt)->send(new OppNoProtegida($oportunidad));
        }
        return redirect('/backend/oportunidad/'.$request->opp_id);
    }

    public function puntos($id){
        $puntos = [];
        $opportunity = Opportunity::find($id);
        $puntos_prod = 0;
        $puntos_estatus = 0;
        $monto_prod_lista = 0;
        $monto_prod_parcial = 0;
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
        $prods = Product::all();
        foreach ($products as $keypro => $product) {
            foreach ($prods as $key => $prod) {
                if ($product->product_id==$prod->id) {
                    $puntos_prod = $puntos_prod + ($prod->points * $product->quantity);
                    $monto_prod_lista = $monto_prod_lista + ($prod->price * $product->quantity);
                    if ($product->state=='pendiente') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price_solicitado * $product->quantity);
                    }elseif ($product->state=='aprobado') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price * $product->quantity);
                    }
                }
            }
        }
        $reseller = $opportunity->reseller;
        if ($reseller) {
            switch ($reseller->company->reseller) {
                case 'authorized':
                    $status_reseller = Accion::where('description','Resseller Authorized')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'preferred':
                    $status_reseller = Accion::where('description','Resseller Preferred')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'priority':
                    $status_reseller = Accion::where('description','Resseller Priority')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'not_qualify':
                    $status_reseller = Accion::where('description','Resseller Not Qualify')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
            }
        }else {
            $puntos_estatus = 0;
        }
        $puntos_crear = Accion::where('description','Crear')->first();
        $monto_superar = Accion::where('description','Monto')->first();
        $puntos_superar = Accion::where('description','Puntos por superar monto')->first();
        $puntos_extra = 0;
        $total_puntos = $puntos_crear->puntos + $puntos_estatus + $puntos_prod;
        if ($monto_prod_parcial >= $monto_superar->puntos) {
            $total_puntos = $total_puntos + $puntos_superar->puntos;
            $puntos_extra = 1;
        }

        $puntos['puntos_crear'] = $puntos_crear->puntos;
        $puntos['monto_superar'] = $monto_superar->puntos;
        $puntos['puntos_superar'] = $puntos_superar->puntos;
        $puntos['puntos_estatus'] = $puntos_estatus;
        $puntos['puntos_prod'] = $puntos_prod;
        $puntos['monto_prod_lista'] = $monto_prod_lista;
        $puntos['monto_prod_parcial'] = $monto_prod_parcial;
        $puntos['total_puntos'] = $total_puntos;
        $puntos['puntos_extra'] = $puntos_extra;
        
        return $puntos;
    }
}
