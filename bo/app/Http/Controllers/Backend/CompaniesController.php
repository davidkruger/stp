<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Company;
use App\Models\CompanyVertical;
use App\Models\File;
use App\Models\Vertical;
use Illuminate\Http\Request;

class CompaniesController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // filter by type
        if (($type = request('type')) !== null) $companies = Company::typeIn(explode(',', $type))->get();
        // load all companies
        else $companies = Company::all();
        // show a list of objects
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // load images
        $images = File::images()->get();
        $verticals = Vertical::all();
        // show create form
        return view('companies.create', compact('images', 'verticals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // create resource
        $company = new Company;
    //    dd($company);
        // fill resource with request data
        $company->fill($request->only( Company::createRules(true) ));
        // check new uploaded logo
        if ($request->has('logo')) {
            // upload image
            $image = File::upload($request, 'logo', $this);
            // save resource
            if (!$image->save())
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($image->errors())
                    ->withInput();
            // set uploaded logo into company
            $company->logo_id = $image->id;
        }
        // save resource
        if (!$company->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($company->errors())
                ->withInput();
        // check verticals
        if (isset($request->verticals))
            // sync verticals
            $company->verticals()->sync($request->verticals);
        // redirect to list
        return redirect()->route('admin.companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company) {
        // redirect to list
        return redirect()->route('admin.companies');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company) {
        // load images
        $images = File::images()->get();
        $verticals = Vertical::all();
        $comp_vert = CompanyVertical::where('company_id',$company->id)->pluck('vertical_id');
        foreach ($verticals as $key => $vertical) {
            $verticals[$key]->selected = 0;
            foreach ($comp_vert as $key_c => $c) {
                if ($vertical->id == $c) {
                    $verticals[$key]->selected = 1;
                }
            }
        }
        // show edit form
        return view('companies.edit', compact('company', 'images', 'verticals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company) {
        // update object
        if (!$company->update($request->only( Company::updateRules($company->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($company->errors())
                ->withInput();
        // check verticals
        if (isset($request->verticals))
            // sync verticals
            $company->verticals()->sync($request->verticals);
        // update type on users
        foreach($company->users as $user)
            // update user type
            $user->update([ 'type' => $company->type ]);
        // redirect to list
        return redirect()->route('admin.companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.companies');
    }
}
