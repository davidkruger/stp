<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // si el state es 2, el usuario esta activo
        $users = User::where('state',2)->get();
        // show a list of objects
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // load companies
        $companies = Company::typeIn(['reseller' ])->get();
        // show create form
        return view('users.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate request (password confirmation)
        $this->validate($request, User::$passwordRules);
        // create resource
        $user = new User;
        // fill resource with request data
        $user->fill($request->only( User::createRules(true) ));
        // crypt password
        $user->password = bcrypt($user->password);
        // check if company was selected
        if ($user->company !== null)
            // set type based on Company
            $user->type = $request->type;
            $user->state = $request->state;
            $user->company_id = $request->company_id;
            // save resource
        if (!$user->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($user->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.users');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        // redirect to list
        return redirect()->route('admin.users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        // load companies
        $companies = Company::all();
        // show edit form
        return view('users.edit', compact('user', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        // check for password change
        if ($request->has('password') && $request->password !== null)
            // validate password change (password confirmation)
            $this->validate($request, User::$passwordRules);
        else
            // remove password attribute from request to prevent null assignment
            $request->request->remove('password');
        // update resource with request data
        $user->fill($request->only( User::updateRules($user->id, true) ));
            $user->type = $request->type;
            $user->state = $request->state;
            $user->company_id = $request->company_id;
        // check for password change
        if ($request->has('password'))
            // crypt password
            $user->password = bcrypt($user->password);
        // save resource
        if (!$user->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($user->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        // check current user
        if (Auth::user()->id == $user->id) return back();
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.users');
    }
    public function solicitudes(){
        // si el state es 1, el usuario solicito que den de alta su cuenta
        $users = User::where('state',1)->get();
        $companies = Company::typeIn(['reseller' ])->get();
        // show a list of objects
        return view('users.solicitudes', compact('users','companies'));
    }
    public function solicitud(Request $request, $id){
        $user = User::findOrFail($id);
        $user->state = $request->input('state');
        $user->company_id = $request->input('company_id');
        $user->save();
        return redirect('/backend/solicitudes');
    }

    public function rechazados(){
        $users = User::where('state',3)->get();
        return view('users.rechazados', compact('users'));
    }
}
