<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Vertical;
use App\Models\File;
use Illuminate\Http\Request;

class VerticalsController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $verticals = Vertical::all();
        // show a list of objects
        return view('verticals.index', compact('verticals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('verticals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // create resource
        $vertical = new Vertical;
        $vertical->code = $request->code;
        $vertical->save();
        return redirect()->route('admin.verticals');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function show(Vertical $vertical) {
        // redirect to list
        return redirect()->route('admin.verticals');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function edit(Vertical $vertical) {
        // load images
        $images = File::images()->get();
        // show edit form
        return view('verticals.edit', compact('vertical', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vertical $vertical) {
        // update object
        if (!$vertical->update($request->only( Vertical::updateRules($vertical->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($vertical->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.verticals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vertical  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vertical $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.verticals');
    }
}
