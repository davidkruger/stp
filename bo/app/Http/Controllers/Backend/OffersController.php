<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Company;
use App\Models\Opportunity;
// use App\Models\OpportunityProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OffersController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $opportunities = Opportunity::with('offers')->with('offers.products')->get();
        dd($opportunities);
        // show a list of objects
        return view('opportunities.index', compact('opportunities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // load Resellers & Mayoristas
        $resellers = Company::resellers()->get();
        $mayoristas = Company::mayoristas()->get();
        $products = Product::all();
        // show create form
        return view('opportunities.create', compact('resellers', 'mayoristas', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // create resource
        $opportunity = new Opportunity;
        // fill resource with request data
        $opportunity->fill($request->only( Opportunity::createRules(true) ));
        // Oppotunity always starts at current date
        $opportunity->starts = date('Y-m-d');
        // Opportunity always starts at 10%
        $opportunity->status = 10;
        // save resource
        if (!$opportunity->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($opportunity->errors())
                ->withInput();
        // check products
        if (isset($request->products)) {
            // foreach products to sync
            $products = [];
            foreach ($request->products as $idx => $product)
                // get extra columns
                $products[$product] = [
                    'quantity'  => $request->quantity[$idx],
                    'price'     => $request->price[$idx],
                ];
            // sync products
            $opportunity->products()->sync($products);
        }
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function show(Opportunity $opportunity) {
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function edit(Opportunity $opportunity) {
        // reload with products
        $opportunity = Opportunity::with('products')->findOrFail($opportunity->id);
        // load Resellers & Mayoristas
        $resellers = Company::resellers()->get();
        $mayoristas = Company::mayoristas()->get();
        $products = Product::all();
        // show edit form
        return view('opportunities.edit', compact('opportunity', 'resellers', 'mayoristas', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opportunity $opportunity) {
        // update object
        if (!$opportunity->update($request->only( Opportunity::updateRules($opportunity->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($opportunity->errors())
                ->withInput();
        // check products
        if (isset($request->products)) {
            // foreach products to sync
            $products = [];
            foreach ($request->products as $idx => $product)
                // get extra columns
                $products[$product] = [
                    'quantity'  => $request->quantity[$idx],
                    'price'     => $request->price[$idx],
                ];
            // sync products
            $opportunity->products()->sync($products);
        }
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Opportunity  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opportunity $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.opportunities');
    }
}
