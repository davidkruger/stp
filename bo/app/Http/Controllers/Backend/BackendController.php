<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class BackendController extends Controller {
    public function __construct() {
        // rewrite base path for views
        view()->addLocation(resource_path('views/backend/'));
    }
}