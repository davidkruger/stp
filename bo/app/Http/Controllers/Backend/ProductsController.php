<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Company;
use App\Models\Division;
use App\Models\File;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\SubCategoria;
use Illuminate\Http\Request;

class ProductsController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $products = Product::all();
        // show a list of objects
        return view('products.index', compact('products'));

    }

    public function prices(Product $product) {
        // reload with mayoristas prices
        $product = Product::with('mayoristas')->find($product->id);
        // get all mayoristas
        $mayoristas = Company::mayoristas()->get();
        // show a list of objects
        return view('products.prices', compact('product', 'mayoristas'));
    }

    public function request(Request $request, Product $product) {
        // create resource
        $price = new ProductPrice;
        // fill data
        $price->fill([
            'product_id'    => $product->id,
            'company_id'    => $request->company_id
        ]);
        // save resource
        if (!$price->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($price->errors());
        // redirect to product prices
        return redirect()->route('admin.prices', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // load divisions & images
        $divisions = Division::all();
        $subcategorias = SubCategoria::orderBy('division_id', 'ASC')->get();
        $mayoristas = Company::mayoristas()->get();
        $images = File::images()->get();
        // show create form
        return view('products.create', compact('divisions', 'mayoristas', 'images','subcategorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //dd($request);
        $subcategoria = SubCategoria::find($request->input('subcategoria_id'));
        // create resource
        $product = new Product;
        // fill resource with request data
        $product->fill($request->only( Product::createRules(true) ));
        // check new uploaded image
        if ($request->has('image')) {
            // upload image
            $image = File::upload($request, 'image', $this);
            // save resource
            if (!$image->save())
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($image->errors())
                    ->withInput();
            // set uploaded image into resource
            $product->image_id = $image->id;
        }
        $product->division_id = $subcategoria->division->id;
        //dd($product);
        // save resource
        if (!$product->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($product->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {
        // redirect to list
        return redirect()->route('admin.products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) {
        // load divisions & images
        $divisions = Division::all();
        $subcategorias = SubCategoria::all();
        $mayoristas = Company::mayoristas()->get();
        $images = File::images()->get();
        // show edit form
        return view('products.edit', compact('product', 'divisions', 'mayoristas', 'images', 'subcategorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product) {
        // update object
        if (!$product->update($request->only( Product::updateRules($product->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($product->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.products');
    }

    public function stock(Request $request, $id){
        $product = Product::find($id);
        $product->stock = $request->stock;
        $product->save();
        return redirect('/backend/products');
    }
}
