<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\SubCategoria;
use Illuminate\Http\Request;
use App\Models\Division;

class SubCategoriaController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subs = Subcategoria::all();
        return view('subcategorias.index', compact('subs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisiones = Division::all();
        return view('subcategorias.create', compact('divisiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sub = new SubCategoria;
        // fill resource with request data
        $sub->fill($request->only( SubCategoria::createRules(true) ));
        // save resource
        if (!$sub->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($sub->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.subcategorias');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategoria  $subCategoria
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategoria $subCategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategoria  $subCategoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategoria = SubCategoria::find($id);
        $divisiones = Division::all();
        return view('subcategorias.edit', compact('divisiones','subCategoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategoria  $subCategoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategoria $subCategoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategoria  $subCategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategoria $subCategoria)
    {
        //
    }
}
