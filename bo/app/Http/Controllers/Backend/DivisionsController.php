<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Division;
use App\Models\SubCategoria;
use App\Models\File;
use Illuminate\Http\Request;

class DivisionsController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $divisions = Division::all();
        // show a list of objects
        return view('divisions.index', compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // load images
        $images = File::images()->get();
        // show create form
        return view('divisions.create', compact('images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // create resource
        $division = new Division;
        // fill resource with request data
        $division->fill($request->only( Division::createRules(true) ));
        // save resource
        if (!$division->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($division->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.divisions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Division $division) {
        // redirect to list
        $subs = SubCategoria::where('division_id', $division->id)->get();
        return view('subcategorias.index', compact('subs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function edit(Division $division) {
        // show edit form
        return view('divisions.edit', compact('division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Division $division) {
        // update object
        if (!$division->update($request->only( Division::updateRules($division->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($division->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.divisions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Division  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::find($id);
        
        $division->delete();
        return redirect('/backend/divisions');
    }
}
