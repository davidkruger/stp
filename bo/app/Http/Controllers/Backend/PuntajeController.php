<?php

namespace App\Http\Controllers;

use App\puntaje;
use Illuminate\Http\Request;

class PuntajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\puntaje  $puntaje
     * @return \Illuminate\Http\Response
     */
    public function show(puntaje $puntaje)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\puntaje  $puntaje
     * @return \Illuminate\Http\Response
     */
    public function edit(puntaje $puntaje)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\puntaje  $puntaje
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, puntaje $puntaje)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\puntaje  $puntaje
     * @return \Illuminate\Http\Response
     */
    public function destroy(puntaje $puntaje)
    {
        //
    }
}
