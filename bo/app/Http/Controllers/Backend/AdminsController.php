<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Samsung;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminsController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $users = Samsung::all();
        // show a list of objects
        return view('admins.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // show create form
        return view('admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // create resource
        $admin = new Samsung;
        // fill resource with request data
        $admin->fill($request->only( Samsung::createRules(true) ));
        // crypt password before saving
        $admin->password = bcrypt($admin->password);
        // set user type fixed to samsung (admins)
        $admin->type = 'samsung';
        // save resource
        if (!$admin->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($admin->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.admins');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Samsung  $samsung
     * @return \Illuminate\Http\Response
     */
    public function show(Samsung $samsung) {
        // redirect to list
        return redirect()->route('admin.admins');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Samsung  $samsung
     * @return \Illuminate\Http\Response
     */
    public function edit(Samsung $samsung) {
        // show edit form
        return view('admins.edit', [ 'user' => $samsung ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Samsung  $samsung
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Samsung $samsung) {
        // check for password change
        if ($request->has('password') && $request->password !== null)
            // validate password change (password confirmation)
            $this->validate($request, Samsung::$passwordRules);
        else
            // remove password attribute from request to prevent null assignment
            $request->request->remove('password');
        // update resource with request data
        $samsung->fill($request->only( Samsung::updateRules($samsung->id, true) ));
        // check for password change
        if ($request->has('password'))
            // crypt password
            $samsung->password = bcrypt($samsung->password);
        // check if company was selected
        if ($samsung->company !== null)
            // update type based on Company
            $samsung->type = $samsung->company->type;
        // save resource
        if (!$samsung->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($samsung->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.admins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Samsung  $samsung
     * @return \Illuminate\Http\Response
     */
    public function destroy(Samsung $samsung) {
        // check current user or root account
        if (Auth::user()->id == $samsung->id || $samsung->id == 1) return back();
        // delete object
        $samsung->delete();
        // redirect to list
        return redirect()->route('admin.admins');
    }
}
