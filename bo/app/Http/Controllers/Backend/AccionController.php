<?php

namespace App\Http\Controllers\Backend;

use App\Models\Accion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccionController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acciones = Accion::all();
        return view('puntajes.acciones.index', compact('acciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Accion  $accion
     * @return \Illuminate\Http\Response
     */
    public function show(Accion $accion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Accion  $accion
     * @return \Illuminate\Http\Response
     */
    public function edit(Accion $accion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Accion  $accion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $accion = Accion::find($id);
        $accion->puntos = $request->puntos;
        $accion->save();
        return redirect('/backend/acciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Accion  $accion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accion $accion)
    {
        //
    }
}
