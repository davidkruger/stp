<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\File;
use Illuminate\Http\Request;

class ImagesController extends BackendController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch all objects
        $images = File::images()->get();
        // show a list of objects
        return view('images.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // show create form
        return view('images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate request
        $this->validate($request, File::$uploadRules);
        // upload file
        $image = File::upload($request, 'file', $this);
        // save resource
        if (!$image->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($image->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.images');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\File  $image
     * @return \Illuminate\Http\Response
     */
    public function show(File $image) {
        // redirect to list
        return redirect()->route('admin.images');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\File  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(File $image) {
        // redirect to images
        return redirect()->route('admin.images');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $image) {
        // update object
        if (!$image->update($request->only( File::updateRules($image->id, true) )))
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($image->errors())
                ->withInput();
        // redirect to list
        return redirect()->route('admin.images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $image) {
        // delete object
        $image->delete();
        // redirect to list
        return redirect()->route('admin.images');
    }
}
