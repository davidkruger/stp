<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class StorageController extends BackendController {
    public function get(File $file) {
        // get file
        $storage = Storage::get($file->url);
        // create a response with file data
        $response = new Response($storage, 200);
        // append file mime type as content type
        $response->header('Content-Type', Storage::mimeType($file->url));
        // return response data
        return $response;
    }
}
