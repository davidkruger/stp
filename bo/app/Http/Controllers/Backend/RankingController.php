<?php

namespace App\Http\Controllers\Backend;

use App\Models\Company;
use App\Models\User;
use App\Http\Controllers\Backend\BackendController;

use Illuminate\Http\Request;

class RankingController extends BackendController
{
    public function rank_puntos(){
        $fecha = date('Y-m');
        $companies = $this->ranking_puntos($fecha);
        return view('ranking.puntos', compact('companies','fecha'));
    }

    public function ranking_puntos($fecha){
        $companies = Company::where('type', 'reseller')->get();
        $users = User::all();
        foreach ($companies as $key_comp => $comp) {
            $companies[$key_comp]->monto = $companies[$key_comp]->monto + 0;
            foreach ($comp->users as $key_user => $user) {
                foreach ($user->puntajes as $key_punt => $puntaje) {
                    $fecha_punt = substr($puntaje->created_at, 0, 7);
                    if ($fecha == $fecha_punt ) {
                        $companies[$key_comp]->puntos = $companies[$key_comp]->puntos + $puntaje->puntos;
                    }
                }
            }
        }

        $companies = $companies->sortByDesc('puntos');
        $position = 0;
        foreach ($companies as $key => $comp) {
            $position = $position + 1;
            $companies[$key]->position = $position;
        }
        return $companies;
    }

    public function rank_puntos_periodo(Request $request)
    {
        $fecha = $request->ano.'-'.$request->mes;
        $companies = $this->ranking_puntos($fecha);
        return view('ranking.puntos', compact('companies','fecha'));
    }

    public function rank_opp(){
        $fecha = date('Y-m');
        $companies = $this->ranking_opp($fecha);
        return view('ranking.oportunidades', compact('companies','fecha'));
    }

    public function ranking_opp($fecha){
        $companies = Company::where('type', 'reseller')->get();
        $users = User::all();
        foreach ($companies as $key_comp => $comp) {
            $companies[$key_comp]->opp_cant = $companies[$key_comp]->opp_cant + 0;
            foreach ($comp->users as $key_user => $user) {
                foreach ($user->puntajes as $key_punt => $puntaje) {
                    $fecha_punt = substr($puntaje->created_at, 0, 7);
                    if ($fecha == $fecha_punt ) {
                        $companies[$key_comp]->opp_cant = $companies[$key_comp]->opp_cant + 1;
                    }
                }
            }
        }
        $companies = $companies->sortByDesc('opp_cant');
        $position = 0;
        foreach ($companies as $key => $comp) {
            $position = $position + 1;
            $companies[$key]->position = $position;
        }
        return $companies;
    }

    public function rank_oportunidades_periodo(Request $request)
    {
        $fecha = $request->ano.'-'.$request->mes;
        $companies = $this->ranking_opp($fecha);
        return view('ranking.oportunidades', compact('companies','fecha'));
    }

    public function rank_monto(){
        $fecha = date('Y-m');
        $companies = $this->ranking_monto($fecha);
        return view('ranking.monto', compact('companies','fecha'));
    }

    public function ranking_monto($fecha){
        $companies = Company::where('type', 'reseller')->get();
        $users = User::all();
        foreach ($companies as $key_comp => $comp) {
            $companies[$key_comp]->monto = $companies[$key_comp]->monto + 0;
            foreach ($comp->users as $key_user => $user) {
                foreach ($user->puntajes as $key_punt => $puntaje) {
                    $fecha_punt = substr($puntaje->created_at, 0, 7);
                    if ($fecha == $fecha_punt ) {
                        foreach ($puntaje->opportunity->items as $item){
                            $companies[$key_comp]->monto = $companies[$key_comp]->monto + ($item->price*$item->quantity);
                        }
                    }
                }
            }
        }
        $companies = $companies->sortByDesc('monto');
        $position = 0;
        foreach ($companies as $key => $comp) {
            $position = $position + 1;
            $companies[$key]->position = $position;
        }
        return $companies;
    }

    public function rank_monto_periodo(Request $request)
    {
        $fecha = $request->ano.'-'.$request->mes;
        $companies = $this->ranking_opp($fecha);
        return view('ranking.monto', compact('companies','fecha'));
    }
}
