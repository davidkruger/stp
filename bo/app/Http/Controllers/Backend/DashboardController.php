<?php

namespace App\Http\Controllers\Backend;

use App\Models\Company;
use App\Models\Opportunity;
use App\Models\Vertical;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends BackendController {
    public function __construct() {
        //
        parent::__construct();
        // force login
        $this->middleware('auth:admin');
    }

    public function index() {
        // redirect to dashboard
        return redirect()->route('admin.dashboard');
    }

    public function dashboard() {

        $fecha = date('Y-m');
        $ranks = $this->ranking_position($fecha);
        // load all opportunities
        $opportunities = Opportunity::all();
        // calculate counts
        $count = (object)[];
        foreach ($opportunities as $opportunity) {
            // init counter for state
            if (!isset($count->{$opportunity->state})) $count->{$opportunity->state} = 0;
            // increment counter
            $count->{$opportunity->state}++;
        }
        $opportunities = $opportunities->sortByDesc('created_at');
        //dd($count, $opportunities);
        // load companies count
        $companies = Company::groupBy('type')->select('type', DB::raw('count(*) as total'))->get();
        // show dashboard view
        return view('dashboard.index', compact('opportunities', 'count', 'companies','ranks'));
    }

    public function ranking_position($fecha){
        $companies = Company::where('type', 'reseller')->get();
        $users = User::all();
        foreach ($companies as $key_comp => $comp) {
            $companies[$key_comp]->puntos = $companies[$key_comp]->puntos + 0;
            foreach ($comp->users as $key_user => $user) {
                foreach ($user->puntajes as $key_punt => $puntaje) {
                    $fecha_punt = substr($puntaje->created_at, 0, 7);
                    if ($fecha == $fecha_punt ) {
                        $companies[$key_comp]->puntos = $companies[$key_comp]->puntos + $puntaje->puntos;
                    }
                }
            }
        }

        $companies = $companies->sortByDesc('puntos');
        $position = 0;
        foreach ($companies as $key => $comp) {
            $position = $position + 1;
            $companies[$key]->position = $position;
        }
        return $companies;
    }

    public function verticals() {
        // load verticales
        $verticals = Vertical::with('companies.users.opportunities')->get();
        dd($verticals);
        // foreach verticals
        foreach ($verticals as $vertical) {
            // counters [ empresas , pantallas ]
            $vertical->totals = (object)[
                'totals'    => [ 0, 0 ],
                'bo'        => [ 0, 0 ],
                'leads'     => [ 0, 0 ],
                'available' => [ 0, 0 ],
            ];
            // foreach companies
            foreach ($vertical->companies as $company) {
                // add empresas counter
                $vertical->totals->totals[0]++;
                $vertical->totals->totals[1] += $company->branches * $company->avg_branches;
                // foreach opportunities
                foreach ($company->opportunities as $opportunity) {
                    // check OP status
                    if ($opportunity->state == 'lead') {
                        // add product qty
                        $vertical->totals->leads[0]++;
                    } elseif ($opportunity->state == 'open') {
                        // add product qty
                        $vertical->totals->bo[0]++;
                    }
                    // foreach opportunity products
                    foreach ($opportunity->products as $product) {
                        // check OP status
                        if ($opportunity->state == 'lead') {
                            // add product qty
                            $vertical->totals->leads[1] += $product->pivot->quantity;
                        } elseif ($opportunity->state == 'open') {
                            // add product qty
                            $vertical->totals->bo[1] += $product->pivot->quantity;
                        }
                    }
                }
            }
            // calculate available
            $vertical->totals->available[0] = $vertical->totals->totals[0] - $vertical->totals->bo[0] - $vertical->totals->leads[0];
            $vertical->totals->available[1] = $vertical->totals->totals[1] - $vertical->totals->bo[1] - $vertical->totals->leads[1];
        }
        // show dashboard view
        return view('dashboard/verticals', compact('verticals'));
    }
}