<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Backend\BackendController;

class LoginController extends BackendController {
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->middleware('guest:admin')->except('logout');
    }

    public function redirectTo() {
        return route('backend');
    }

    // public function login(Request $request) {
    //     // validate data
    //     $this->validate($request, [
    //         'email'     => 'required|email',
    //         'password'  => 'required|min:6'
    //     ]);

    //     // attempt login
    //     if (Auth::guard('admin')->attempt($request)) {
    //         //
    //     }
    // }

    protected function guard() {
        // return admin guard
        return Auth::guard('admin');
    }
}