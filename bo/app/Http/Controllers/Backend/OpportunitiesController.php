<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BackendController;
use App\Models\Company;
use App\Models\Opportunity;
use App\Models\Product;
use App\Models\Accion;
use App\Models\Vertical;
use App\Models\OpportunityItem;
use App\Models\ProbabilidadCierre;
use App\Models\OpportunityProduct;
use App\Models\HistorialOferta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SolicitudLead;
use App\Mail\OportunidadAsignada;
use App\Models\HistorialEtapaNegocio;


class OpportunitiesController extends BackendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (($state = request('state')) !== null){
            $opportunities = Opportunity::where('state',$state)->get();
        }else{
            //$opportunities = Opportunity::all();   
            $opportunities = Opportunity::OportDescending()->get();
            
        }
        foreach ($opportunities as $key_opp => $opp) {
            $opportunities[$key_opp]->puntos = $this->puntos($opp->id);
            
        }
        // show a list of objects
        return view('opportunities.index', compact('opportunities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create() {

        \Session::forget('opp_pro');
        // load Resellers & Mayoristas
        $companies = Company::all();
        $products = Product::all();
        $verticals = Vertical::all();
        $probabilidades = ProbabilidadCierre::all();
        $type = ['reseller','reseller_gerente'];
        $resellers = User::whereIn('type',$type)->get();
        // show create form
        if (!\Session::has('opp_pro')) \Session::put('opp_pro', array());
        return view('opportunities.create', compact('companies', 'products','verticals','probabilidades','resellers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function deleteOppProduct(Request $request)
    {
        $product = Product::find($request->id);

        $opp_pro = \Session::get('opp_pro');
        unset($opp_pro[$product->id]);
        \Session::put('opp_pro', $opp_pro);
        return $opp_pro;
    }

    public function addProductOpp(Request $request)
    {
        $id = $request->product_id;
        $cantidad = $request->cantidad;
        $pre_sol = $request->precio_sol;
        $motivo = $request->motivo;
        $opp_pro = \Session::get('opp_pro');
        if ($opp_pro) {
            foreach ($opp_pro as $key => $value) {
                if ($value->id==$id) {
                    $cantidad = $cantidad + $value->cantidad;
                }
            }
        }
        $pro = Product::find($id);
        $product = new \stdClass;
        $product->id = $id;
        $product->name = $pro->name;
        $product->cantidad = $cantidad;
        $product->motivo = $motivo;
        $product->puntos = $cantidad * $pro->points;
        $product->precio_solicitado = $pre_sol;
        $product->precio_lista = $pro->price;

        $opp_pro[$product->id] = $product;

        \Session::put('opp_pro', $opp_pro);

        return $opp_pro;
    }


    public function store(Request $request) {
        $opp_pro = \Session::get('opp_pro');
        $opportunity = new Opportunity;
        $opportunity->nombre_cliente = $request->nombre_cliente;
        $opportunity->razon_social_cliente = $request->razon_social_cliente;
        $opportunity->ruc_cliente = $request->ruc_cliente;
        $opportunity->telefono_cliente = $request->telefono_cliente;
        $opportunity->name = $request->name;
        $opportunity->entrega = $request->entrega;
        $opportunity->fentrega = $request->fentrega;
        $opportunity->vertical_id = $request->vertical;
      //  $opportunity->descripcion = $request->description;
        $opportunity->probabilidad_cierre_id = $request->probabilidad_cierre;
        $opportunity->tipo_oportunidad = $request->tipo_oportunidad;
        if ($request->tipo_oportunidad=='publico') {
            $opportunity->tipo_publico = $request->tipo_oportunidad_publica;
            if ($request->tipo_oportunidad_publica=='pre_pliego') {
                $opportunity->pre_pliego_fecha = $request->publicacion_llamado;
            }else {
                $opportunity->llamado_calle_idllamado = $request->idllamado;
            }
        }
        if ($request->reseller == 'lead') {
            $opportunity->state = 'lead';
        }else {
            $opportunity->reseller_id = $request->reseller;
            $opportunity->state = 'open';
        }
        $opportunity->tentative = $request->tentative;

        $opportunity->presupuesto = $request->presupuesto;
        $opportunity->save();
        $opp_id = $opportunity->id;
        foreach ($opp_pro as $key => $pro) {
            $n_pro = new OpportunityItem;
            $n_pro->opp_id = $opp_id;
            $n_pro->product_id = $pro->id;
            $n_pro->quantity = $pro->cantidad;
            if ($pro->precio_solicitado == null) {
                $n_pro->price = $pro->precio_lista;
                $n_pro->state = 'aprobado';
            }else {
                
                $n_pro->price_solicitado = $pro->precio_solicitado;
                $n_pro->price = $pro->precio_lista;
                $n_pro->motivo = $pro->motivo;
                $n_pro->state = 'aprobado';
            }
            $n_pro->id_ofertante = Auth::user()->id;
            $n_pro->save();
        }
        $h_etapa = new HistorialEtapaNegocio;
        $h_etapa->opportunity_id = $opp_id;
        $h_etapa->user_id = Auth::user()->id;
        $h_etapa->etapa_id = $request->probabilidad_cierre;
        $h_etapa->save();

        if ($request->reseller != 'lead') {
            Mail::to($opportunity->reseller)->send(new OportunidadAsignada($opportunity));
        }
        // redirect to list
        return redirect()->route('admin.proteccionShow', $opportunity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function show(Opportunity $opportunity) {
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function puntos($id){
        $puntos = [];
        $opportunity = Opportunity::find($id);
        $puntos_prod = 0;
        $puntos_estatus = 0;
        $monto_prod_lista = 0;
        $monto_prod_parcial = 0;
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
        $prods = Product::all();
        foreach ($products as $keypro => $product) {
            foreach ($prods as $key => $prod) {
                if ($product->product_id==$prod->id) {
                    $puntos_prod = $puntos_prod + ($prod->points * $product->quantity);
                    $monto_prod_lista = $monto_prod_lista + ($prod->price * $product->quantity);
                    if ($product->state=='pendiente') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price_solicitado * $product->quantity);
                    }elseif ($product->state=='aprobado') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price * $product->quantity);
                    }
                }
            }
        }
        $reseller = $opportunity->reseller;
        if ($reseller) {
            switch ($reseller->company->reseller) {
                case 'authorized':
                    $status_reseller = Accion::where('description','Resseller Authorized')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'preferred':
                    $status_reseller = Accion::where('description','Resseller Preferred')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'priority':
                    $status_reseller = Accion::where('description','Resseller Priority')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'not_qualify':
                    $status_reseller = Accion::where('description','Resseller Not Qualify')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
            }
        }else {
            $puntos_estatus = 0;
        }
        $puntos_crear = Accion::where('description','Crear')->first();
        $monto_superar = Accion::where('description','Monto')->first();
        $puntos_superar = Accion::where('description','Puntos por superar monto')->first();
        $puntos_extra = 0;
        $total_puntos = $puntos_crear->puntos + $puntos_estatus + $puntos_prod;
        if ($monto_prod_parcial >= $monto_superar->puntos) {
            $total_puntos = $total_puntos + $puntos_superar->puntos;
            $puntos_extra = 1;
        }

        $puntos['puntos_crear'] = $puntos_crear->puntos;
        $puntos['monto_superar'] = $monto_superar->puntos;
        $puntos['puntos_superar'] = $puntos_superar->puntos;
        $puntos['puntos_estatus'] = $puntos_estatus;
        $puntos['puntos_prod'] = $puntos_prod;
        $puntos['monto_prod_lista'] = $monto_prod_lista;
        $puntos['monto_prod_parcial'] = $monto_prod_parcial;
        $puntos['total_puntos'] = $total_puntos;
        $puntos['puntos_extra'] = $puntos_extra;
        
        return $puntos;
    }

    public function edit(Opportunity $opportunity) {
        $puntos = $this->puntos($opportunity->id);
        $puntos_prod = $puntos['puntos_prod'];
        $puntos_estatus = $puntos['puntos_estatus'];
        $monto_prod_lista = $puntos['monto_prod_lista'];
        $monto_prod_parcial = $puntos['monto_prod_parcial'];
        $cerrar = 0;
        $probabilidades = ProbabilidadCierre::all();
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
   
        if ($opportunity->probabilidad_cierre_id != 8 && $opportunity->probabilidad_cierre_id != 7) {
           $cerrar = 1;
        }
           $puntos_crear = $puntos['puntos_crear'];
           $monto_superar = $puntos['monto_superar'];
           $puntos_superar = $puntos['puntos_superar'];

           return view('opportunities.edit', compact('probabilidades','opportunity', 'products', 'cerrar','puntos_estatus','monto_prod_parcial','puntos_prod','monto_prod_lista','puntos_crear','monto_superar',
                       'puntos_superar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opportunity $opportunity) {
        // update values
        $opportunity->fill($request->only( Opportunity::updateRules($opportunity->id, true) ));

        $puntos = 0;
        $costo = 0;
        if (isset($request->products)) {
            foreach ($request->products as $key => $value) {
                $prod = Product::find($value);
                $puntos = $puntos + ($request->quantity[$key]*$prod->points);
                $costo = $costo + ($request->quantity[$key]*$prod->price);
            }
        }
        $opportunity->puntos = $puntos;
        $opportunity->monto_total = $costo;

        // update object
        if (!$opportunity->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($opportunity->errors())
                ->withInput();
        // check products
        if (isset($request->products)) {
            // foreach products to sync
            $products = [];
            foreach ($request->products as $idx => $product)
                // get extra columns
                $products[$product] = [
                    'quantity'  => $request->quantity[$idx],
                ];
            // sync products
            $opportunity->products()->sync($products);
            // check current status == 10, and set status to 25%
            if ($opportunity->status == 10) if (!$opportunity->update([ 'status' => 25 ]))
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($opportunity->errors())
                    ->withInput();
        }
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Opportunity  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opportunity $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('admin.opportunities');
    }

    public function precio_asignado($id, Request $request){
        $producto = OpportunityItem::find($id);
        $old_pro = new HistorialOferta;
        $old_pro->id_opportunity_item = $id;
        $old_pro->quantity = $producto->quantity;
        if ($producto->price_solicitado == null) {
            $old_pro->price_solicitado = $producto->price;
            $old_pro->id_ofertante = Auth::user()->id;
        }else {
            $old_pro->price_solicitado = $producto->price_solicitado;
            $old_pro->id_ofertante = $producto->id_ofertante;
            $old_pro->motivo = $producto->motivo;
        }
        $old_pro->state = $producto->state;
        $old_pro->save();

        $producto->motivo = $request->motivo;
        $producto->price_solicitado = $request->precio_solicitado;
        $producto->price = $request->precio_solicitado;
        $producto->state = 'aprobado';
        $producto->id_ofertante = Auth::user()->id;
        $producto->save();
        return redirect(url('backend/oportunidad/'.$producto->opp_id));
    }

    public function leads(){
        $opportunities = Opportunity::where('state', 'lead')->get();
        foreach ($opportunities as $key => $opp) {
            $solicitudes = SolicitudLead::where('opportunity_id', $opp->id)->count();
            $opportunities[$key]->solicitudes = $solicitudes;
        }
        return view('leads.index', compact('opportunities'));
    }

    public function lead($id){
        $opportunity = Opportunity::find($id);
        $solicitudes = SolicitudLead::where('opportunity_id', $id)->get();
        return view('leads.show', compact('opportunity','solicitudes'));
    }

    public function otorgar_lead($id){
        $solicitud = SolicitudLead::find($id);
        $opportunity = Opportunity::find($solicitud->opportunity_id);
        $opportunity->reseller_id = $solicitud->reseller_id;
        $opportunity->state = 'open';
        $opportunity->save();

        $solicitud->state = 'aprobado';
        $solicitud->save();
        $solicitudes = SolicitudLead::where('opportunity_id', $opportunity->id)->where('state','pendiente')->get();
        foreach ($solicitudes as $key => $sol) {
            $sol->state = 'rechazado';
            $sol->save();
        }
        Mail::to($solicitud->reseller)->send(new OportunidadAsignada($opportunity));
        return redirect(url('backend/oportunidad/'.$opportunity->id));
    }

}
