<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\FrontendController;
use App\Models\Offer;
use App\Models\OfferProduct;
use App\Models\Opportunity;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OffersController extends FrontendController {
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Opportunity $opportunity) {
        // show create form
        return view('offers.create', compact('opportunity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Opportunity $opportunity) {
        // create resource
        $offer = new Offer;
        // fill resource with request data
        $offer->fill([
            'opportunity_id'    => $opportunity->id,
            'mayorista_id'      => Auth::user()->company_id
        ]);
        // save resource
        if (!$offer->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($offer->errors())
                ->withInput();
        // foreach products to sync
        foreach ($request->products as $idx => $product) {
            // load product data
            $product = Product::findOrFail($product);
            // ignore if product isnt from mayorista
            if ($product->mayorista_id !== Auth::user()->company_id) continue;
            // create OfferProduct resource
            $oProduct = new OfferProduct;
            // set attributes
            $oProduct->fill([
                'offer_id'      => $offer->id,
                'product_id'    => $request->products[$idx],
                'price'         => $request->price[$idx],
            ]);
            // save resource
            if (!$oProduct->save())
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($oProduct->errors());
        }
        // change opportunity status to 50%
        if (!$opportunity->update([ 'status' => 50 ]))
            return redirect()
                ->back()
                ->withErrors($opportunity->errors());
        // redirect to list
        return redirect()->route('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Opportunity $opportunity, int $offer_id) {
        // load offer
        $offer = $opportunity->offers()->currentUser()->first();
        // validate against param
        if ($offer->company_id !== $offer_id) return abort(404);
        // dd([
        //     'offer'     => $offer,
        //     'products'  => $offer->products,
        //     'product'   => $offer->products[0]->product,
        //     'opProduct' => $offer->products[0]->opportunityProduct->first(),
        // ]);
        // show offer details
        return view('offers.edit', compact('opportunity', 'offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opportunity $opportunity, int $offer_id) {
        // load offer
        $offer = $opportunity->offers()->currentUser()->first();
        // foreach products to sync
        $products = [];
        foreach ($request->products as $idx => $product_id) {
            // get OfferProduct to update
            $oProduct = OfferProduct::where('opportunity_id', '=', $offer->opportunity_id)
                                 ->where('company_id', '=', $offer->company_id)
                                 ->where('product_id', '=', $product_id)
                                 ->first();
            // fill params
            $oProduct->fill([
                'price' => $request->price[$idx]
            ]);
            // save resource
            if (!$oProduct->save())
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($oProduct->errors());
        }
        // redirect to list
        return redirect()->route('dashboard');
    }

    public function accept(Request $request, Opportunity $opportunity, Offer $offer) {
        // update offer
        if (!$offer->update([
            'status'    => 'accepted',
            'reason'    => $request->reason,
        ]))
            return redirect()
                ->back()
                ->withErrors($offer->errors());
        // // update opportunity
        // if (!$opportunity->update([
        //     // 'status'        => 50,
        //     // 'close_reason'  => $request->close_reason,
        // ]))
        //     return redirect()
        //         ->back()
        //         ->withErrors($opportunity->errors());
        // return to dashboard
        return redirect()->route('dashboard');
    }

    public function reject(Request $request, Opportunity $opportunity, Offer $offer) {
        // update offer
        if (!$offer->update([
            'status'    => 'rejected',
            'reason'    => $request->reason,
        ]))
            return redirect()
                ->back()
                ->withErrors($offer->errors());
        // return to dashboard
        return redirect()->route('opportunities.show', $opportunity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('dashboard');
    }
}
