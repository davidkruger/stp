<?php

namespace App\Http\Controllers\Frontend;

class SiteController extends FrontendController {
    public function index() {
        return view('site/index');
    }

    // public function contact() {
    //     return view('site/contact');
    // }
}