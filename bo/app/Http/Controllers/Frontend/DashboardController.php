<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Opportunity;
use App\Models\OpportunityItem;
use App\Models\Accion;
use App\Models\Product;
use App\Models\Company;
use App\Models\User;
use App\Models\Puntaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ProbabilidadCierre;
use App\Models\HistorialEtapaNegocio;

class DashboardController extends FrontendController {
    public function index() {
        // get all opportunities
        $leads = Opportunity::where('state', 'lead')->get();
        $opportunities = [];
        $probabilidades = ProbabilidadCierre::all();
        $fecha = date('Y-m');
        $ranks = $this->ranking_position($fecha);
        $rank_position = 0;
        $rank_puntos = 0;
        switch (Auth::user()->type) {
            case 'reseller':
                foreach ($ranks as $key => $rank) {
                    if ($rank->id == Auth::user()->company->id) {
                        $rank_position = $rank->position;
                        $rank_puntos = $rank->puntos;
                    }
                }
                $opportunities = Opportunity::where('reseller_id',Auth::user()->id)->with('offers')->get();
                $type = 'reseller';
                break;
            case 'reseller_gerente':
                foreach ($ranks as $key => $rank) {
                    if ($rank->id == Auth::user()->company->id) {
                        $rank_position = $rank->position;
                        $rank_puntos = $rank->puntos;
                    }
                }
                $users = User::where('company_id',Auth::user()->company_id)->get();
                $users_id = [''];
                foreach ($users as $key => $user) {
                    $users_id[$key] = $user->id;
                }
                $opportunities = Opportunity::whereIn('reseller_id', $users_id)
                    ->with('offers')->get();
                $type = 'reseller';
                break;
            case 'mayorista':
                $opportunities = Opportunity::all();
                $type = 'mayorista';
                break;
        }
        foreach ($opportunities as $key_opp => $opp) {
            $opportunities[$key_opp]->cerrar = 0;
            $items = OpportunityItem::where('opp_id', $opp->id)->get();
            foreach ($items as $key_item => $item) {
                if ($item->state=='pendiente'){
                    $opportunities[$key_opp]->cerrar = 1;
                }
            }
            $puntos = $this->puntos($opp->id);
            $opportunities[$key_opp]->puntos = $puntos['total_puntos'];
            $opportunities[$key_opp]->monto = $puntos['monto_prod_parcial'];
        }
        foreach ($leads as $key_lead => $lead) {

            $puntos = $this->puntos($lead->id);
            $leads[$key_lead]->puntos = $puntos['total_puntos'];
            $leads[$key_lead]->monto = $puntos['monto_prod_lista'];
        }
        $opportunities = $opportunities->sortByDesc('id');
        // load dashboard view by user type
        return view('dashboard.'.$type, compact('leads', 'opportunities','probabilidades','rank_position','rank_puntos'));
    }

    public function ranking_position($fecha){
        $companies = Company::where('type', 'reseller')->get();
        $users = User::all();
        foreach ($companies as $key_comp => $comp) {
            $companies[$key_comp]->puntos = $companies[$key_comp]->puntos + 0;
            foreach ($comp->users as $key_user => $user) {
                foreach ($user->puntajes as $key_punt => $puntaje) {
                    $fecha_punt = substr($puntaje->created_at, 0, 7);
                    if ($fecha == $fecha_punt ) {
                        $companies[$key_comp]->puntos = $companies[$key_comp]->puntos + $puntaje->puntos;
                    }
                }
            }
        }

        $companies = $companies->sortByDesc('puntos');
        $position = 0;
        foreach ($companies as $key => $comp) {
            $position = $position + 1;
            $companies[$key]->position = $position;
        }
        return $companies;
    }

    public function actualizar(Request $request){
        $opp = Opportunity::find($request->opp_id);
        if ($request->pro_cierre==8) {
            $opp->state = 'lost';
            $opp->reason = $request->cerrar;
        }
        if ($request->pro_cierre==7) {
            $opp->state = 'won';
            $id = $opp->id;
            $puntos = $this->puntos($id);
            $puntaje = new Puntaje;
            $puntaje->user_id = $opp->reseller_id;
            $puntaje->puntos = $puntos['total_puntos'];
            $puntaje->puntos_productos = $puntos['puntos_prod'];
            $puntaje->puntos_estatus = $puntos['puntos_estatus'];
            $puntaje->puntos_crear = $puntos['puntos_crear'];
            if ($puntos['puntos_extra']==1) {
                $puntaje->puntos_superar = $puntos['puntos_superar'];
            }
            $puntaje->opportunity_id = $id;
            $puntaje->save();
        }
            $opp->probabilidad_cierre_id= $request->pro_cierre;
        $opp->save();


        $h_etapa = new HistorialEtapaNegocio;
        $h_etapa->opportunity_id = $opp->id;
        $h_etapa->user_id = Auth::user()->id;
        $h_etapa->etapa_id = $request->pro_cierre;
        $h_etapa->save();

        return redirect()->back();
        //return redirect('/dashboard');
    }

    public function puntos($id){
        $puntos = [];
        $opportunity = Opportunity::find($id);
        $puntos_prod = 0;
        $puntos_estatus = 0;
        $monto_prod_lista = 0;
        $monto_prod_parcial = 0;
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
        $prods = Product::all();
        foreach ($products as $keypro => $product) {
            foreach ($prods as $key => $prod) {
                if ($product->product_id==$prod->id) {
                    $puntos_prod = $puntos_prod + ($prod->points * $product->quantity);
                    $monto_prod_lista = $monto_prod_lista + ($prod->price * $product->quantity);
                    if ($product->state=='pendiente') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price_solicitado * $product->quantity);
                    }elseif ($product->state=='aprobado') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price * $product->quantity);
                    }
                }
            }
        }
        $reseller = $opportunity->reseller;
        if ($reseller) {
            switch ($reseller->company->reseller) {
                case 'authorized':
                    $status_reseller = Accion::where('description','Resseller Authorized')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'preferred':
                    $status_reseller = Accion::where('description','Resseller Preferred')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'priority':
                    $status_reseller = Accion::where('description','Resseller Priority')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'not_qualify':
                    $status_reseller = Accion::where('description','Resseller Not Qualify')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
            }
        }else {
            $puntos_estatus = 0;
        }
        $puntos_crear = Accion::where('description','Crear')->first();
        $monto_superar = Accion::where('description','Monto')->first();
        $puntos_superar = Accion::where('description','Puntos por superar monto')->first();
        $puntos_extra = 0;
        $total_puntos = $puntos_crear->puntos + $puntos_estatus + $puntos_prod;
        if ($monto_prod_parcial >= $monto_superar->puntos) {
            $total_puntos = $total_puntos + $puntos_superar->puntos;
            $puntos_extra = 1;
        }

        $puntos['puntos_crear'] = $puntos_crear->puntos;
        $puntos['monto_superar'] = $monto_superar->puntos;
        $puntos['puntos_superar'] = $puntos_superar->puntos;
        $puntos['puntos_estatus'] = $puntos_estatus;
        $puntos['puntos_prod'] = $puntos_prod;
        $puntos['monto_prod_lista'] = $monto_prod_lista;
        $puntos['monto_prod_parcial'] = $monto_prod_parcial;
        $puntos['total_puntos'] = $total_puntos;
        $puntos['puntos_extra'] = $puntos_extra;
        
        return $puntos;
    }
}