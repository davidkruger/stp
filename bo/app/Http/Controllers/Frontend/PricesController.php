<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\FrontendController;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PricesController extends FrontendController {
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // fetch
        $products = Product::fromMayorista(Auth::user()->company_id)->get();
        // show create form
        return view('prices.index', compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        // foreach products to update price
       // dd($request);
        $products = [];
        foreach ($request->products as $idx => $product_id) {
            // get Product to update
            $product = Product::findOrFail($product_id);
            // fill params
            $product->fill([
                'price' => $request->price[$idx]
            ]);
            // save resource
         //   dd($product);
            if (!$product->save())
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($product->errors());
        }
        // redirect to list
        return redirect()->route('products');
    }
}
