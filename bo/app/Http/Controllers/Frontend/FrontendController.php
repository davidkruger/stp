<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class FrontendController extends Controller {
    public function __construct() {
        // add base path for frontend views
        view()->addLocation(resource_path('views/frontend/'));
    }
}