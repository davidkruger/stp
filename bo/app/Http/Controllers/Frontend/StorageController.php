<?php

namespace App\Http\Controllers\Frontend;

use App\Models\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class StorageController extends FrontendController {
    public function get(File $file) {
        // get file
        $storage = Storage::get($file->url);
        // create a response with file data
        $response = new Response($storage, 200);
        // append file mime type as content type
        $response->header('Content-Type', Storage::mimeType($file->url));
        // return response data
        return $response;
    }
}
