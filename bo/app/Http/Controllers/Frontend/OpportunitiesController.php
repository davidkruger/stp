<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\FrontendController;
use App\Models\HistorialOferta;
use App\Models\Company;
use App\Models\Opportunity;
use App\Models\OpportunityProduct;
use App\Models\OpportunityItem;
use App\Models\Accion;
use App\Models\Puntaje;
use App\Models\User;
use App\Models\Vertical;
use App\Models\ProbabilidadCierre;
// use App\Models\OpportunityProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\oppcreated;
use App\Models\SolicitudLead;
use App\Models\HistorialEtapaNegocio;

class OpportunitiesController extends FrontendController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index() {
        // fetch all objects
        $opportunities = Opportunity::all();
        // show a list of objects
        return view('opportunities.index', compact('opportunities'));
    }
    public function listProducts(Request $request) {
        //if (!$request->ajax()) return redirect('/');
        $products = Product::all();
        return  $products;
       
    }

    public function listOppProducts(Request $request) {
        //if (!$request->ajax()) return redirect('/');
        $oppProducts = OpportunityProduct::all();
        foreach ($oppProducts as $key => $value) {
            $oppProducts[$key]->nombreProducto = $value->product->name;
            $oppProducts[$key]->precioProducto = $value->product->price;
            $oppProducts[$key]->stock = $value->product->stock_cantidad;
        }
        return  $oppProducts;
       
    }
 
    public function deleteOppProduct(Request $request)
    {
        $product = Product::find($request->id);

        $opp_pro = \Session::get('opp_pro');
        unset($opp_pro[$product->id]);
        \Session::put('opp_pro', $opp_pro);
        return $opp_pro;
    }

    public function addProductOpp(Request $request)
    {
        $id = $request->product_id;
        $cantidad = $request->cantidad;
        $pre_sol = $request->precio_sol;
        $motivo = $request->motivo;
        $opp_pro = \Session::get('opp_pro');
        if ($opp_pro) {
            foreach ($opp_pro as $key => $value) {
                if ($value->id==$id) {
                    $cantidad = $cantidad + $value->cantidad;
                }
            }
        }
        $pro = Product::find($id);
        $product = new \stdClass;
        $product->id = $id;
        $product->name = $pro->name;
        $product->cantidad = $cantidad;
        $product->motivo = $motivo;
        $product->puntos = $cantidad * $pro->points;
        $product->precio_solicitado = $pre_sol;
        $product->precio_lista = $pro->price;

        $opp_pro[$product->id] = $product;

        \Session::put('opp_pro', $opp_pro);

        return $opp_pro;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        \Session::forget('opp_pro');
        // load Resellers & Mayoristas
        $companies = Company::all();
        $products = Product::all();
        $verticals = Vertical::all();
        $probabilidades = ProbabilidadCierre::all();
        // show create form
        if (!\Session::has('opp_pro')) \Session::put('opp_pro', array());
        return view('opportunities.create', compact('companies', 'products','verticals','probabilidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $opp_pro = \Session::get('opp_pro');
        $opportunity = new Opportunity;
        $opportunity->nombre_cliente = $request->nombre_cliente;
        $opportunity->razon_social_cliente = $request->razon_social_cliente;
        $opportunity->ruc_cliente = $request->ruc_cliente;
        $opportunity->telefono_cliente = $request->telefono_cliente;
        $opportunity->name = $request->name;
        $opportunity->vertical_id = $request->vertical;
      //  $opportunity->descripcion = $request->description;
        $opportunity->probabilidad_cierre_id = $request->probabilidad_cierre;
        $opportunity->tipo_oportunidad = $request->tipo_oportunidad;
        if ($request->tipo_oportunidad=='publico') {
            $opportunity->tipo_publico = $request->tipo_oportunidad_publica;
            if ($request->tipo_oportunidad_publica=='pre_pliego') {
                $opportunity->pre_pliego_fecha = $request->publicacion_llamado;
            }else {
                $opportunity->llamado_calle_idllamado = $request->idllamado;
            }
        }
        $opportunity->state = 'open';
        $opportunity->tentative = $request->tentative;
      //  $opportunity->products_list_id = $request->idOpportunity;
        $opportunity->reseller_id = Auth::user()->id;

        $opportunity->presupuesto = $request->presupuesto;
        $opportunity->save();
        $opp_id = $opportunity->id;
        foreach ($opp_pro as $key => $pro) {
            $n_pro = new OpportunityItem;
            $n_pro->opp_id = $opp_id;
            $n_pro->product_id = $pro->id;
            $n_pro->quantity = $pro->cantidad;
            if ($pro->precio_solicitado == null) {
                $n_pro->price = $pro->precio_lista;
                $n_pro->state = 'aprobado';
            }else {
                $n_pro->price_solicitado = $pro->precio_solicitado;
                $n_pro->motivo = $pro->motivo;
                $n_pro->state = 'pendiente';
            }
            $n_pro->id_ofertante = Auth::user()->id;
            $n_pro->save();
        }
        $h_etapa = new HistorialEtapaNegocio;
        $h_etapa->opportunity_id = $opp_id;
        $h_etapa->user_id = Auth::user()->id;
        $h_etapa->etapa_id = $request->probabilidad_cierre;
        $h_etapa->save();
        Mail::to(Auth::user())->send(new oppcreated($opportunity));
        // redirect to list
        return redirect()->route('opportunities.show', $opportunity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function puntos($id){
        $puntos = [];
        $opportunity = Opportunity::find($id);
        $puntos_prod = 0;
        $puntos_estatus = 0;
        $monto_prod_lista = 0;
        $monto_prod_parcial = 0;
        $products = OpportunityItem::where('opp_id', $opportunity->id)->get();
        $prods = Product::all();
        foreach ($products as $keypro => $product) {
            foreach ($prods as $key => $prod) {
                if ($product->product_id==$prod->id) {
                    $puntos_prod = $puntos_prod + ($prod->points * $product->quantity);
                    $monto_prod_lista = $monto_prod_lista + ($prod->price * $product->quantity);
                    if ($product->state=='pendiente') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price_solicitado * $product->quantity);
                    }elseif ($product->state=='aprobado') {
                        $monto_prod_parcial = $monto_prod_parcial + ($product->price * $product->quantity);
                    }
                }
            }
        }
        $reseller = $opportunity->reseller;
        if ($reseller) {
            switch ($reseller->company->reseller) {
                case 'authorized':
                    $status_reseller = Accion::where('description','Resseller Authorized')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'preferred':
                    $status_reseller = Accion::where('description','Resseller Preferred')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'priority':
                    $status_reseller = Accion::where('description','Resseller Priority')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
                case 'not_qualify':
                    $status_reseller = Accion::where('description','Resseller Not Qualify')->first();
                    $puntos_estatus = $status_reseller->puntos;
                    break;
            }
        }else {
            $puntos_estatus = 0;
        }
        $puntos_crear = Accion::where('description','Crear')->first();
        $monto_superar = Accion::where('description','Monto')->first();
        $puntos_superar = Accion::where('description','Puntos por superar monto')->first();
        $puntos_extra = 0;
        $total_puntos = $puntos_crear->puntos + $puntos_estatus + $puntos_prod;
        if ($monto_prod_parcial >= $monto_superar->puntos) {
            $total_puntos = $total_puntos + $puntos_superar->puntos;
            $puntos_extra = 1;
        }

        $puntos['puntos_crear'] = $puntos_crear->puntos;
        $puntos['monto_superar'] = $monto_superar->puntos;
        $puntos['puntos_superar'] = $puntos_superar->puntos;
        $puntos['puntos_estatus'] = $puntos_estatus;
        $puntos['puntos_prod'] = $puntos_prod;
        $puntos['monto_prod_lista'] = $monto_prod_lista;
        $puntos['monto_prod_parcial'] = $monto_prod_parcial;
        $puntos['total_puntos'] = $total_puntos;
        $puntos['puntos_extra'] = $puntos_extra;
        
        return $puntos;
    }
    public function show(Opportunity $opportunity) {
     $puntos = $this->puntos($opportunity->id);
     $puntos_prod = $puntos['puntos_prod'];
     $puntos_estatus = $puntos['puntos_estatus'];
     $monto_prod_lista = $puntos['monto_prod_lista'];
     $monto_prod_parcial = $puntos['monto_prod_parcial'];
     $cerrar = 0;
     $solicitar = 0;
     $probabilidades = ProbabilidadCierre::all();
     if ($opportunity->state=='lead') {
         $solicitudes = SolicitudLead::where('opportunity_id',$opportunity->id)->where('reseller_id', Auth::user()->id)->get();
        if (count($solicitudes)>0) {
            $solicitar = 1;
        }
     }
     $products = OpportunityItem::where('opp_id', $opportunity->id)->get();

        foreach ($products as $key => $pro) {
            if ($pro->state=='pendiente') {
                $cerrar = 1;
            }
        }

        $puntos_crear = $puntos['puntos_crear'];
        $monto_superar = $puntos['monto_superar'];
        $puntos_superar = $puntos['puntos_superar'];
        switch (Auth::user()->company->type) {
            case 'mayorista':
                // already assigned op
                if ($opportunity->mayorista !== null)
                    // accessing to opportunity of another mayorista
                    if ($opportunity->mayorista_id !== Auth::user()->company_id) abort(403);
                break;
            case 'reseller':
                if ($opportunity->reseller !== null)
                    // accessing to opportunity of another reseller
            //        if ($opportunity->reseller_id !== Auth::user()->company_id) abort(403);
                break;
        }
        return view('opportunities.show', compact('probabilidades','opportunity', 'products', 'cerrar','puntos_estatus'
        ,'monto_prod_parcial','puntos_prod','monto_prod_lista','puntos_crear','monto_superar','puntos_superar','solicitar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function edit(Opportunity $opportunity) {
        // reload with products
        $opportunity = Opportunity::with('products')->findOrFail($opportunity->id);
        // load Resellers & Mayoristas
        $companies = Company::all();
        $products = Product::all();
        // show edit form
        return view('opportunities.edit', compact('opportunity', 'companies', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opportunity $opportunity) {
        // update values
        $opportunity->fill($request->only( Opportunity::updateRules($opportunity->id, true) ));
        $puntos = 0;
        $costo = 0;
      //  dd($request);
        if (isset($request->products)) {
            foreach ($request->products as $key => $value) {
                $prod = Product::find($value);
                $puntos = $puntos + ($request->quantity[$key]*$prod->points);
                $costo = $costo + ($request->quantity[$key]*$prod->price);
            }
        }
        // create resource
        $opportunity->puntos = $puntos;
        $opportunity->monto_total = $costo;
        // update object
        if (!$opportunity->save())
            // redirect with errors
            return redirect()
                ->back()
                ->withErrors($opportunity->errors())
                ->withInput();
        // check products
        if (isset($request->products)) {
            // foreach products to sync
            $products = [];
            foreach ($request->products as $idx => $product)
                // get extra columns
                $products[$product] = [
                    'quantity'  => $request->quantity[$idx],
                ];
            // sync products
            $opportunity->products()->sync($products);
            // check current status == 10, and set status to 25%
            if ($opportunity->status == 10) if (!$opportunity->update([ 'status' => 25 ]))
                // redirect with errors
                return redirect()
                    ->back()
                    ->withErrors($opportunity->errors())
                    ->withInput();
        }
        // redirect to list
        if ($opportunity->state=="won") {
            $accion = Accion::where('description', 'Oportunidad Ganada')->first();
            $puntaje = new Puntaje;
            $puntaje->puntos = $opportunity->puntos;
            $puntaje->user_id = Auth::user()->id;
            $puntaje->accion_id = $accion->id;
            $puntaje->opportunity_id = $opportunity->id;
            if (!$puntaje->save()) {
                return 'Error: no se asignaron los puntos, por favor contacte al administrador del sistema para mas información';
            }
            
        }
        return redirect()->route('opportunities.show', $opportunity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Opportunity  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opportunity $user) {
        // delete object
        $user->delete();
        // redirect to list
        return redirect()->route('opportunities');
    }
    
    public function precio_solicitado($id, Request $request){
        $producto = OpportunityItem::find($id);
        $old_pro = new HistorialOferta;
        $old_pro->id_opportunity_item = $id;
        $old_pro->quantity = $producto->quantity;
        if ($producto->price_solicitado == null) {
            $old_pro->price_solicitado = $producto->price;
            $old_pro->id_ofertante = Auth::user()->id;
        }else {
            $old_pro->price_solicitado = $producto->price_solicitado;
            $old_pro->id_ofertante = $producto->id_ofertante;
            $old_pro->motivo = $producto->motivo;
        }
        $old_pro->state = $producto->state;
        $old_pro->save();

        $producto->quantity = $request->quantity;
        $producto->motivo = $request->motivo;
        $producto->price_solicitado = $request->precio_solicitado;
        $producto->state = 'pendiente';
        $producto->id_ofertante = Auth::user()->id;
        $producto->save();
        return redirect('/opportunities/'.$producto->opp_id);
    }

    public function aprobado($id){
        $producto = OpportunityItem::find($id);
        $producto->price = $producto->price_solicitado;
        $producto->state = 'aprobado';
        $producto->save();
        return redirect('/opportunities/'.$producto->opp_id);
    }

    public function cerrar($id){
        $opp = Opportunity::find($id);
        if ($opp->probabilidad_cierre_id == 7) {
            $opp->state = 'won';
            $puntos = $this->puntos($id);
            $puntaje = new Puntaje;
            $puntaje->user_id = $opp->reseller_id;
            $puntaje->puntos = $puntos['total_puntos'];
            $puntaje->puntos_productos = $puntos['puntos_prod'];
            $puntaje->puntos_estatus = $puntos['puntos_estatus'];
            $puntaje->puntos_crear = $puntos['puntos_crear'];
            if ($puntos['puntos_extra']==1) {
                $puntaje->puntos_superar = $puntos['puntos_superar'];
            }
            $puntaje->opportunity_id = $id;
            $puntaje->save();
        }
        if ($opp->probabilidad_cierre_id == 8) {
            $opp->state = 'lost';
        }
        $opp->save();
        return redirect('/opportunities/'.$id);
    }

    public function mail(Request $request){
        $opp = Opportunity::find($request->id);
        $user = Auth::user();
        Mail::to($user)->send(new oppcreated($opp));
        return redirect('/opportunities/'.$request->id);
    }

    public function obtener($id){
        $solicitud = new SolicitudLead;
        $solicitud->opportunity_id = $id;
        $solicitud->reseller_id = Auth::user()->id;
        $solicitud->state = 'pendiente';
        $solicitud->save();

        return redirect('/opportunities/'.$id);
    }

/*    public function obtener($id){
        $opp = Opportunity::find($id);
        $opp->state = 'open';
        $opp->reseller_id = Auth::user()->id;
        $opp->save();
        return redirect('/opportunities/'.$id);
    }*/
}
