<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            // accessing to admin page
            case 'admin':
                // authenticated as admin
                if (Auth::guard($guard)->check())
                    // redirect to backend
                    return redirect()->route('backend');
                break;
            // normal user
            default:
                // authenticated as normal user
                if (Auth::guard($guard)->check())
                    // redirect to user dashboard
                    return redirect('/dashboard');
        }

        return $next($request);
    }
}
