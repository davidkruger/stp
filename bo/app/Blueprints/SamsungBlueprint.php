<?php

namespace App\Blueprints;

use Illuminate\Database\Connection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Grammars\Grammar;

class SamsungBlueprint extends Blueprint {
    public function build(Connection $connection, Grammar $grammar) {
        // always add created/updated/deleted
        $this->timestamps();
        $this->softDeletes();
        // return parent builder
        return parent::build($connection, $grammar);
    }

    public function createdUpdatedBy() {
        $this->unsignedBigInteger('createdby')->comment('Usuario que creo el registro');
            $this->foreign('createdby')->references('id')->on('users');
        $this->unsignedBigInteger('updatedby')->comment('Usuario que actualizo el registro');
            $this->foreign('updatedby')->references('id')->on('users');
    }
}