<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Concerns\GuardsAttributes;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Illuminate\Support\Facades\Validator as Validator_Factory;
use Illuminate\Validation\Validator;

trait HasCreatedUpdatedBy {
    use HasEvents;

    public static function boot() {
        //
        parent::boot();
        // capture creating
        self::creating(function($model) {
            // set createdby value
            $model->createdby = Auth::user()->getId();
            // continue execution
            return true;
        });
        // capture saving
        self::saving(function($model) {
            // set updatedby value
            $model->updatedby = Auth::user()->getId();
            // continue execution
            return true;
        });
    }
}