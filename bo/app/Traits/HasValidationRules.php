<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Concerns\GuardsAttributes;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Illuminate\Support\Facades\Validator as Validator_Factory;
use Illuminate\Validation\Validator;

trait HasValidationRules {
    use HasEvents;

    /**
     * Validation rules for creating a new resourse
     */
    public static final function createRules(bool $onlykeys = false) {
        // get rules
        $rules = static::$createRules ?? [];
        // return keys or rules
        return $onlykeys ? array_keys($rules) : $rules;
    }

    /**
     * Validation rules for updating a new resourse
     */
    public static final function updateRules($id, bool $onlykeys = false) {
        // get rules
        $rules = static::$updateRules ?? [];
        // foreach rules
        foreach ($rules as $idx => $rule)
            // check if is array of rules
            if (gettype($rule) == 'array')
                // foreach rules array
                foreach ($rule as $rule_idx => $rule_value)
                    // replace {id}
                    $rules[$idx][$rule_idx] = str_replace('{id}', $id, $rule_value);
            else
                // replace {id}
                $rules[$idx] = str_replace('{id}', $id, $rule);
        // return keys or rules
        return $onlykeys ? array_keys($rules) : $rules;
    }

    public static function boot() {
        //
        parent::boot();
        // capture creating
        self::creating(function($model) {
            // create a Validator with createRules
            $validator = Validator_Factory::make($model->getAllAttributes(), static::createRules());
            // check if rules fails
            if ($validator->fails()) {
                // save errors
                $model->validation_errors = $validator->errors();
                // return false
                return false;
            }
            // execute beforeSave
            $model->beforeSave($validator);
            // save errors
            $model->validation_errors = $validator->errors()->any() ? $validator->errors() : null;
            // return validator result
            return !$validator->errors()->any();
        });
        // capture updating
        self::updating(function($model) {
            // create a Validator with updateRules
            $validator = Validator_Factory::make($model->getAllAttributes(), static::updateRules($model->id));
            // check if rules fails
            if ($validator->fails()) {
                // save errors
                $model->validation_errors = $validator->errors();
                // return false
                return false;
            }
            // execute beforeSave
            $model->beforeSave($validator);
            // save errors
            $model->validation_errors = $validator->errors()->any() ? $validator->errors() : null;
            // return validator result
            return !$validator->errors()->any();
        });
    }

    /**
     * Errors generated from Validator or custom beforeSave validations
     */
    private $validation_errors;
    public final function errors() {
        // return saved errors
        return $this->validation_errors;
    }

    /**
     * Custom validations beforeSave
     */
    protected function beforeSave(Validator $validator) {}

    /**
     * Returns all resource attributes,
     * appends null to inexistent ones
     */
    private function getAllAttributes() {
        // get current attributes
        $attributes = $this->getAttributes();
        // foreach fillable columns
        foreach ($this->getFillable() as $column)
            // check if isn't in attributes yet
            if (!array_key_exists($column, $attributes))
                // append with null value
                $attributes[$column] = null;
        // return attributes
        return $attributes;
    }
}