<?php

namespace App\Traits;

use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey as CoenJacobs_HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Exception;

trait HasCompositePrimaryKey {
    use CoenJacobs_HasCompositePrimaryKey;

    protected function setKeysForSaveQuery(Builder $query) {
        foreach ($this->getKeyName() as $key) {
            if ( ! isset($this->$key))
                throw new Exception(__METHOD__ . 'Missing part of the primary key: ' . $key);
            // FIX: Use original key value
            $query->where($key, '=', $this->original[$key]);
        }
        return $query;
    }
}